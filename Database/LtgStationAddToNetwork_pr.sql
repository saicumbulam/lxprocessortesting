IF EXISTS (SELECT * FROM sysobjects WHERE name = 'LtgStationAddToNetwork_pr')
BEGIN
	DROP PROCEDURE LtgStationAddToNetwork_pr	
END

GO

CREATE PROC LtgStationAddToNetwork_pr
@StationId varchar(10),
@NetworkId nchar(10)
AS

/* LtgStationAddToNetwork_pr.sql */

BEGIN 

INSERT INTO [dbo].[LtgNetworkSensor]
           ([LtgNetworkID]
           ,[Station_ID]
           ,[LastModifiedBy]
           ,[LastModifiedTime])
     VALUES
           (@NetworkId
		   ,@StationId
		   ,'LtgStationAddToNetwork_pr'
		   ,GETUTCDATE())

END

GO

IF EXISTS (SELECT * FROM sysobjects WHERE name = 'LtgStationAddToNetwork_pr')
BEGIN
	GRANT EXEC ON LtgStationAddToNetwork_pr TO webaccess
	PRINT 'LtgStationAddToNetwork_pr Added'
END
ELSE
BEGIN
	PRINT 'Error'
END
