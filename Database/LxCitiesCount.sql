DECLARE @id int
	,@latitude float
	,@longitude float
	,@location geography
	,@sum bigint
	,@points int
	,@i int
	,@north float
	,@south float
	,@east float 
	,@west float
	,@pp geography

DECLARE dbCursor CURSOR FOR  
SELECT	Id
		,Latitude
		,Longitude
FROM	Cities 

OPEN dbCursor   
FETCH NEXT FROM dbCursor INTO @id, @latitude, @longitude   

WHILE @@FETCH_STATUS = 0   
BEGIN
	-- Create a circle/buffer from point with 10 mile radius and 0.1 mile tolerance   
	SET @location = geography::Point(@latitude, @longitude, 4326).BufferWithTolerance(16093.44, 160.9344, 0)

	--loop through all the points in the polygon and grab the min and max to create a bounding box
	SET @points = @location.STNumPoints();

	SET @i = 1
	SET @north = -90
	SET	@south = 90
	SET	@east = -180
	SET @west = 180

	WHILE (@i <= @points)
	BEGIN

		SET @pp = @location.STPointN(@i)	

		IF (@pp.Lat > @north) SET @north = @pp.Lat
		IF (@pp.Lat < @south) SET @south = @pp.Lat
		IF (@pp.Long > @east) SET @east = @pp.Long
		IF (@pp.Long < @west) SET @west = @pp.Long

		SET @i = @i + 1
	END

	-- grab the data in the bounding boxes from all the tables
	SET @sum = 0

	SELECT	@sum = @sum + COUNT(Lightning_Time_String) 
	FROM	LtgFlash_History201501
	WHERE	Latitude >= @south
	AND		Latitude <= @north
	AND		Longitude >= @west
	AND		Longitude <= @east

	SELECT	@sum = @sum + COUNT(Lightning_Time_String) 
	FROM	LtgFlash_History201502
	WHERE	Latitude >= @south
	AND		Latitude <= @north
	AND		Longitude >= @west
	AND		Longitude <= @east

	SELECT	@sum = @sum + COUNT(Lightning_Time_String) 
	FROM	LtgFlash_History201503
	WHERE	Latitude >= @south
	AND		Latitude <= @north
	AND		Longitude >= @west
	AND		Longitude <= @east
	
	SELECT	@sum = @sum + COUNT(Lightning_Time_String) 
	FROM	LtgFlash_History201504
	WHERE	Latitude >= @south
	AND		Latitude <= @north
	AND		Longitude >= @west
	AND		Longitude <= @east

	SELECT	@sum = @sum + COUNT(Lightning_Time_String) 
	FROM	LtgFlash_History201505
	WHERE	Latitude >= @south
	AND		Latitude <= @north
	AND		Longitude >= @west
	AND		Longitude <= @east

	SELECT	@sum = @sum + COUNT(Lightning_Time_String) 
	FROM	LtgFlash_History201506
	WHERE	Latitude >= @south
	AND		Latitude <= @north
	AND		Longitude >= @west
	AND		Longitude <= @east

	SELECT	@sum = @sum + COUNT(Lightning_Time_String) 
	FROM	LtgFlash_History201507
	WHERE	Latitude >= @south
	AND		Latitude <= @north
	AND		Longitude >= @west
	AND		Longitude <= @east

	SELECT	@sum = @sum + COUNT(Lightning_Time_String) 
	FROM	LtgFlash_History201508
	WHERE	Latitude >= @south
	AND		Latitude <= @north
	AND		Longitude >= @west
	AND		Longitude <= @east

	SELECT	@sum = @sum + COUNT(Lightning_Time_String) 
	FROM	LtgFlash_History201509
	WHERE	Latitude >= @south
	AND		Latitude <= @north
	AND		Longitude >= @west
	AND		Longitude <= @east

	SELECT	@sum = @sum + COUNT(Lightning_Time_String) 
	FROM	LtgFlash_History201510
	WHERE	Latitude >= @south
	AND		Latitude <= @north
	AND		Longitude >= @west
	AND		Longitude <= @east

	SELECT	@sum = @sum + COUNT(Lightning_Time_String) 
	FROM	LtgFlash_History201511
	WHERE	Latitude >= @south
	AND		Latitude <= @north
	AND		Longitude >= @west
	AND		Longitude <= @east

	SELECT	@sum = @sum + COUNT(Lightning_Time_String) 
	FROM	LtgFlash_History201512
	WHERE	Latitude >= @south
	AND		Latitude <= @north
	AND		Longitude >= @west
	AND		Longitude <= @east

	UPDATE	Cities
	SET		LxSum = @sum
	WHERE	Id = @id

    FETCH NEXT FROM dbCursor INTO @id, @latitude, @longitude 
END   

CLOSE dbCursor   
DEALLOCATE dbCursor

SELECT * FROM Cities