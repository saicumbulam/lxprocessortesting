IF EXISTS (SELECT * FROM sysobjects WHERE name = 'LtgStationRemoveFromNetwork_pr')
BEGIN
	DROP PROCEDURE LtgStationRemoveFromNetwork_pr	
END

GO

CREATE PROC LtgStationRemoveFromNetwork_pr
@StationId varchar(10),
@NetworkId nchar(10)
AS

/* LtgStationRemoveFromNetwork_pr.sql */

BEGIN 

DELETE FROM [dbo].[LtgNetworkSensor]
WHERE Station_ID = @StationId
AND LtgNetworkID = @NetworkId

END

GO

IF EXISTS (SELECT * FROM sysobjects WHERE name = 'LtgStationRemoveFromNetwork_pr')
BEGIN
	GRANT EXEC ON LtgStationRemoveFromNetwork_pr TO webaccess
	PRINT 'LtgStationRemoveFromNetwork_pr Added'
END
ELSE
BEGIN
	PRINT 'Error'
END
