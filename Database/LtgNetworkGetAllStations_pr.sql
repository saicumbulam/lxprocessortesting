IF EXISTS (SELECT * FROM sysobjects WHERE name = 'LtgNetworkGetAllStations_pr')
BEGIN
	DROP PROCEDURE LtgNetworkGetAllStations_pr	
END

GO

CREATE PROC LtgNetworkGetAllStations_pr
@NetworkId nchar(10)
AS

/* LtgNetworkGetAllStations_pr.sql */

BEGIN 

	SELECT	Station_ID
	FROM	dbo.LtgNetworkSensor WITH (NOLOCK)
	WHERE	LtgNetworkID = @NetworkId

END

GO

IF EXISTS (SELECT * FROM sysobjects WHERE name = 'LtgNetworkGetAllStations_pr')
BEGIN
	GRANT EXEC ON LtgNetworkGetAllStations_pr TO webaccess
	PRINT 'LtgNetworkGetAllStations_pr Added'
END
ELSE
BEGIN
	PRINT 'Error'
END
