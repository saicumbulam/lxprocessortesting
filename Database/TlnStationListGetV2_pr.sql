IF EXISTS (SELECT * FROM sysobjects WHERE name = 'TlnStationListGetV2_pr')
BEGIN
	DROP PROCEDURE TlnStationListGetV2_pr	
END

GO

CREATE PROC TlnStationListGetV2_pr

AS

/* TlnStationListGetV2_pr.sql */

BEGIN 

SELECT  LTGStatList.Station_ID AS Id
		,LTGStatList.IP_Address AS Ip
		,StationCalibration.LastCalibrationDate AS LastCalibration
		,LastLogData.LTRemote_FWVersion AS OutdoorFirmware
		,LtgSensorStatistics.PacketRatePercent AS PacketPercent
		,LtgSensorStatistics.TimeLastSuccessfulPacketReceived AS LastPacketUtc
		,LtgQCFlags.QCFlag AS QCFlag
		,LTGStatList.Latitude AS Latitude
		,LTGStatList.Longitude AS Longitude
		,LtgNetworkSensor.LtgNetworkID as NetworkId
		,LtgNetworkList.LtgNetworkName as NetworkName
		,LtgNetworkList.[Description] as NetworkDescription
FROM	dbo.LTGStatList
			LEFT OUTER JOIN (
					SELECT	Station_ID
					,MAX	(CalibrationDate) AS LastCalibrationDate
					FROM	Lightning.dbo.LtgCalibration
					GROUP BY Station_ID) AS StationCalibration
				ON LTGStatList.Station_ID = StationCalibration.Station_ID
			LEFT OUTER JOIN Lightning.dbo.LastLogData
				ON LTGStatList.Station_ID = LastLogData.Station_ID
			LEFT OUTER JOIN Lightning.dbo.LtgSensorStatistics
				ON LTGStatList.Station_ID = LtgSensorStatistics.SensorId
            LEFT OUTER JOIN Lightning.dbo.LtgQCFlags
			    ON Lightning.dbo.LtgQCFlags.Station_ID = LTGStatList.Station_ID
			LEFT OUTER JOIN Lightning.dbo.LtgNetworkSensor 
				ON Lightning.dbo.LtgNetworkSensor.Station_ID = LTGStatlist.Station_ID
			LEFT OUTER JOIN Lightning.dbo.LtgNetworkList 
				ON Lightning.dbo.LtgNetworkSensor.LtgNetworkID = Lightning.dbo.LtgNetworkList.LtgNetworkID
ORDER BY LTGStatList.Station_ID, LtgNetworkSensor.LtgNetworkID 

END

GO

IF EXISTS (SELECT * FROM sysobjects WHERE name = 'TlnStationListGetV2_pr')
BEGIN
	GRANT EXEC ON TlnStationListGetV2_pr TO webaccess
	PRINT 'TlnStationListGetV2_pr Added'
END
ELSE
BEGIN
	PRINT 'Error'
END
