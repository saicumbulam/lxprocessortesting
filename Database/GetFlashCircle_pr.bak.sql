USE [TLNDM]
GO

/****** Object:  StoredProcedure [dbo].[GetFlashCircle_pr]    Script Date: 06/04/2015 13:53:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







CREATE PROCEDURE [dbo].[GetFlashCircle_pr]
		 @Longitude			decimal(9,6)
		,@Latitude			decimal(9,6)
		,@Radius			int = 300
		,@DateTimeFrom		bigint
		,@DateTimeTo		bigint
		,@StrokeType		INT = NULL
	/*

	Date Breakdown:
	YYYY MM DD HH Q
	2012 09 18 02 1

	exec GetFlashCircle_pr 31.35, 34.90,  300000,  20121010124, 20121010234, 0

	*/
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @StartTime		DATETIME
	DECLARE @EndTime		DATETIME
	DECLARE @RecordCount	INT
	DECLARE @ProcName		VARCHAR(50)
		
	SET @ProcName= OBJECT_NAME(@@PROCID)
	SET @StartTime = GETUTCDATE()
	
	--- Get partiotions based on the inpuit date range ----
	DECLARE @MonthFrom  SMALLINT
	DECLARE @MonthTo	SMALLINT
	DECLARE @DateFromSDT  SMALLDATETIME
	DECLARE @DateToSDT    SMALLDATETIME
	DECLARE @Location GEOGRAPHY
	
	SELECT @Location = dbo.ConvertToGeography_fn(@Longitude,@Latitude)
	SELECT @DateFromSDT = dbo.ConvertToSmallDateTime_fn(@DateTimeFrom)		
	SELECT @DateToSDT	= dbo.ConvertToSmallDateTime_fn(@DateTimeTo)
										
	EXEC [dbo].[GetHPTSegmentSuffixes_pr]
		@HPTBaseName			= 'HPT_Flash',
		@DateFrom				= @DateFromSDT,
		@DateTo					= @DateToSDT,
		@FirstSegmentSuffix		= @MonthTo  OUTPUT,
		@SecondSegmentSuffix	= @MonthFrom  OUTPUT

	--- end get partiotions --------

	--- Extract
	DECLARE @sqlcmd NVARCHAR(MAX)
	SET @sqlcmd = '
	SELECT 
		EFV.[FlashID],
		EFV.[LightningTimeString],
		EFV.[Latitude],
		EFV.[Longitude],
		EFV.[Height],
		EFV.[StrokeType],
		EFV.[Amplitude],
		EFV.[StrokeSolution],
		EFV.[Confidence],
		FFA.[NbrOfSensors] AS [NumOfSensor],
		FFA.[NbrOfPortions] AS [Multiplicity]
	FROM [dbo].[Export_Flash_vw] EFV WITH (NOLOCK)
	LEFT OUTER JOIN [dbo].[Fact_FlashAggrts] FFA
	ON EFV.FlashID = FFA.FlashID
	WHERE EFV.[FlashID] IN
	(
		SELECT [FlashID] FROM Fact_Flash ff WITH (NOLOCK)
		JOIN dim_spatial_geopoint ds WITH (NOLOCK)
		ON ff.spatial_key = ds.spatial_key
		WHERE Geopoint.STDistance(' + '''' +  CAST(@Location AS VARCHAR(255)) + '''' +  ') <  ' + CAST(@Radius AS VARCHAR(15)) + ' 
		AND ff.Date_Key BETWEEN ' + CAST(@DateTimeFrom  AS VARCHAR(15)) + ' AND ' +  CAST(@DateTimeTo AS VARCHAR(15)) +	'
	)' + ' 
	AND EFV.PartitionSegmentCode in (' + CAST(@MonthTo AS VARCHAR(2))+ ',' +  CAST(@MonthFrom AS VARCHAR(2)) +')'

	IF @StrokeType IS NOT NULL
	BEGIN
		SET @sqlcmd += '
	AND EFV.StrokeType = ' + CAST(@StrokeType AS VARCHAR(5))
	END
	EXEC (@sqlcmd) 
	PRINT @sqlcmd
	SELECT @RecordCount = @@ROWCOUNT
	SET @EndTime = getutcdate()

	EXEC dbo.InsertRequest_pr
		@RequestTS			= @StartTime,
		@CompletionTS		= @EndTime,
		@TimeFrom			= @DateTimeFrom,
		@TimeTo				= @DateTimeTo,
		@RequestTypeID		= 2,
		@radius				= @Radius,
		@CenterLongitude	= @Longitude,
		@CenterLatitude		= @Latitude,
		@RecordCount		= @RecordCount,
		@UserID				= @ProcName
END



GO


