IF EXISTS (SELECT * FROM sysobjects WHERE name = 'LtgNetworkCreate_pr')
BEGIN
	DROP PROCEDURE LtgNetworkCreate_pr	
END

GO

CREATE PROC LtgNetworkCreate_pr
@Id nchar(10),
@Name varchar(50),
@Desc varchar(250) = NULL

AS
/* LtgNetworkCreate_pr.sql */

BEGIN 
	INSERT INTO [dbo].[LtgNetworkList]
           ([LtgNetworkID]
           ,[LtgNetworkName]
           ,[Description]
           ,[LastModifiedBy]
           ,[LastModifiedTime])
     VALUES
           (@Id
           ,@Name
           ,@Desc
           ,'LtgNetworkCreate_pr'
           ,GETUTCDATE())

END

GO

IF EXISTS (SELECT * FROM sysobjects WHERE name = 'LtgNetworkCreate_pr')
BEGIN
	GRANT EXEC ON LtgNetworkCreate_pr TO webaccess
	PRINT 'LtgNetworkCreate_pr Added'
END
ELSE
BEGIN
	PRINT 'Error'
END
