USE [TLNDM]
GO

/****** Object:  StoredProcedure [dbo].[GetFlashPortionCircle_pr]    Script Date: 06/04/2015 13:53:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[GetFlashPortionCircle_pr]
		 @Longitude			decimal(9,6)
		,@Latitude			decimal(9,6)
		,@Radius			int = 300
		,@DateTimeFrom		bigint
		,@DateTimeTo		bigint
	    ,@StrokeType		INT = NULL
	/*

	Date Breakdown:
	YYYY MM DD HH Q
	2012 09 18 02 1

	exec GetFlashPortionCircle_pr 31.35, 34.90,  300000,  20121010124, 20121010234, 0

	*/
	as
	begin
		
			declare @StartTime		datetime
			declare @EndTime		datetime
			declare @RecordCount	int
			declare @ProcName		varchar(50)
			
			set @ProcName= OBJECT_NAME(@@PROCID)
			set @StartTime = getutcdate()


		--- Get partiotions based on the inpuit date range ----
			declare @MonthFrom  smallint
			declare @MonthTo	smallint

			declare @DateFromSDT  smalldatetime
			declare @DateToSDT    smalldatetime

			declare @Location geography
			select @Location = dbo.ConvertToGeography_fn(@Longitude,@Latitude)

			select @DateFromSDT = dbo.ConvertToSmallDateTime_fn(@DateTimeFrom)
			set @DateFromSDT = dateadd(dd,-1,@DateFromSDT)		
			select @DateToSDT	= dbo.ConvertToSmallDateTime_fn(@DateTimeTo)
			
									
			exec [dbo].[GetHPTSegmentSuffixes_pr]
			  @HPTBaseName			= 'HPT_FlashPortion',
			  @DateFrom				= @DateFromSDT,
			  @DateTo				= @DateToSDT,
			  @FirstSegmentSuffix	= @MonthTo  OUTPUT,
			  @SecondSegmentSuffix	= @MonthFrom  OUTPUT

   		--- end get partiotions --------


	--- Extract
	declare @sqlcmd nvarchar(max)
	set @sqlcmd = 
	'
	SELECT [FlashPortionID]
		  ,[LightningTimeString]
		  ,[Latitude]
		  ,[Longitude]
		  ,[Height]
		  ,[StrokeType]
		  ,[Amplitude]
		  ,[StrokeSolution]
		  ,[Confidence]
	  FROM [dbo].[Export_FlashPortion_vw] with(nolock)
	  where flashportionid in
	(
	--- Search
		select [FlashPortionID] from  Fact_FlashPortion ff with(nolock)
		join dim_spatial_geopoint ds with(nolock)
		on ff.spatial_key = ds.spatial_key
		where Geopoint.STDistance(' + '''' +  cast(@Location as varchar(255)) + '''' +  ') <  ' + cast(@Radius as varchar(15)) +
		' and ff.Date_Key between  ' + cast(@DateTimeFrom  as varchar(15)) +  ' and  ' +  cast(@DateTimeTo as varchar(15))  +
	') ' +
		'--- Navigate to required partitions
		 and PartitionSegmentCode in (' + cast(@MonthTo as varchar(2))+ ',' +  cast(@MonthFrom as varchar(2)) +')' 

	if @StrokeType IS NOT NULL 
	BEGIN 
		SET @sqlcmd += '
		and [StrokeType] = ' + CAST(@StrokeType AS VARCHAR(5)) 
		end 
		exec (@sqlcmd) 
		print (@sqlcmd)
			select @RecordCount = @@ROWCOUNT
			set @EndTime = getutcdate()

			exec dbo.InsertRequest_pr
			 @RequestTS			= @StartTime
			,@CompletionTS		= @EndTime
			,@TimeFrom			= @DateTimeFrom
			,@TimeTo			= @DateTimeTo
			,@RequestTypeID		= 2
			,@radius			= @Radius
			,@CenterLongitude	= @Longitude
			,@CenterLatitude	= @Latitude
			,@RecordCount		= @RecordCount
			,@UserID			= @ProcName
	end

	




GO


