
SELECT	LTGStatList.Station_ID
	,LTGStatList.Latitude
	,LTGStatList.Longitude
	,StatisticsDataHistory.SensorId
	,AVG(StatisticsDataHistory.MedianHFThreshold) AS MedianHFThresholdAvg
	,LtgSensorDetectionStatistics.LastFlashTime
	,LtgSensorDetectionStatistics.SensorID
FROM	LTGStatList LEFT OUTER JOIN LtgSensorDetectionStatistics 
			LEFT OUTER JOIN StatisticsDataHistory 
			ON LtgSensorDetectionStatistics.SensorID = Lightning.dbo.StatisticsDataHistory.SensorId
		ON LTGStatList.Station_ID = LtgSensorDetectionStatistics.SensorID
WHERE	StatisticsDataHistory.[Time] > DATEADD(day, -30, GETUTCDATE())
GROUP BY LTGStatList.Station_ID
		,LTGStatList.Latitude
		,LTGStatList.Longitude
		,StatisticsDataHistory.SensorId
		,LtgSensorDetectionStatistics.LastFlashTime
		,LtgSensorDetectionStatistics.SensorID
ORDER BY LTGStatList.Station_ID
  

  /*
Missing Index Details from SQLQuery13.sql - Dev-MsSql-Snp.dev.awscorp.com\Prod_MsSql_03.master (sa (53))
The Query Processor estimates that implementing the following index could improve the query cost by 94.3913%.
*/


/*
USE [Lightning]
GO
CREATE NONCLUSTERED INDEX [StatisticsDataHistory_IX1]
ON [dbo].[StatisticsDataHistory] ([Time])
INCLUDE ([SensorId],[MedianHFThreshold])
GO
*/
