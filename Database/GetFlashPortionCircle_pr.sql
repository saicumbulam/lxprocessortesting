IF EXISTS (SELECT * FROM sysobjects WHERE name = 'GetFlashPortionCircle_pr')
BEGIN
	DROP PROCEDURE GetFlashPortionCircle_pr	
END

GO

CREATE PROCEDURE [dbo].[GetFlashPortionCircle_pr]
	@Longitude decimal(9,5)
	,@Latitude decimal(9,5)
	,@Radius int = 300
	,@DateTimeFrom bigint
	,@DateTimeTo bigint
	,@StrokeType int = NULL

/*

Date Breakdown:
YYYY MM DD HH Q
2012 09 18 02 1

exec GetFlashPortionCircle_pr -80.854690, 35.243190,    16000,       20140625153, 20140625232, 0

*/
AS
BEGIN
       
	declare @StartTime         datetime
	declare @EndTime           datetime
	declare @RecordCount int
	declare @ProcName          varchar(50)
              
	set @ProcName= OBJECT_NAME(@@PROCID)
	set @StartTime = getutcdate()


	--- Get partitions based on the input date range ----
	declare @MonthFrom  smallint
	declare @MonthTo     smallint

	declare @DateFromSDT  smalldatetime
	declare @DateToSDT    smalldatetime

	declare @Location geography
	
	SET @Location = geography::Point(@Latitude,@Longitude, 4326)
	SET @Location = @Location.BufferWithTolerance(@Radius, 0.1, 0)
                 
	select @DateFromSDT = dbo.ConvertToSmallDateTime_fn(@DateTimeFrom)
	set @DateFromSDT = dateadd(dd,-1,@DateFromSDT)         
	select @DateToSDT    = dbo.ConvertToSmallDateTime_fn(@DateTimeTo)
                                                       
	exec [dbo].[GetHPTSegmentSuffixes_pr]
		@HPTBaseName = 'HPT_FlashPortion',
		@DateFrom = @DateFromSDT,
		@DateTo = @DateToSDT,
		@FirstSegmentSuffix = @MonthTo  OUTPUT,
		@SecondSegmentSuffix = @MonthFrom  OUTPUT

	--- end get partiotions --------


    --- Extract
    declare @sqlcmd nvarchar(max)
    set @sqlcmd = 
    '
    SELECT [FlashPortionID]
            ,[LightningTimeString]
            ,[Latitude]
            ,[Longitude]
            ,[Height]
            ,[StrokeType]
            ,[Amplitude]
            ,[StrokeSolution]
            ,[Confidence]
        FROM [dbo].[Export_FlashPortion_vw] with(nolock)
        where flashportionid in
    (
    --- Search
            select [FlashPortionID] from  Fact_FlashPortion ff with(nolock)
            join dim_spatial_geopoint ds with(nolock, INDEX (ix_dim_spatial_geopoint_geopoint) )
            on ff.spatial_key = ds.spatial_key
            where Geopoint.STIntersects(geography::Parse(' + '''' +  @Location.ToString() + '''' +  ')) = 1  '       +
            ' and ff.Date_Key between  ' + cast(@DateTimeFrom  as varchar(15)) +  ' and  ' +  cast(@DateTimeTo as varchar(15))  +
    ') ' +
            '--- Navigate to required partitions
            and PartitionSegmentCode in (' + cast(@MonthTo as varchar(2))+ ',' +  cast(@MonthFrom as varchar(2)) +') ' 
			  
	IF (@StrokeType <> NULL)
	BEGIN 
		SET @sqlcmd = @sqlcmd + ' and StrokeType = ' + cast(@StrokeType as varchar(5)) + ''''
	END

    --print (@sqlcmd)
    exec (@sqlcmd) 

              
              
    select @RecordCount = @@ROWCOUNT
    set @EndTime = getutcdate()

    exec dbo.InsertRequest_pr
    @RequestTS = @StartTime
    ,@CompletionTS             = @EndTime
    ,@TimeFrom                 = @DateTimeFrom
    ,@TimeTo                   = @DateTimeTo
    ,@RequestTypeID            = 2
    ,@radius                   = @Radius
    ,@CenterLongitude    = @Longitude
    ,@CenterLatitude     = @Latitude
    ,@RecordCount        = @RecordCount
    ,@UserID                   = @ProcName
              
END

GO

IF EXISTS (SELECT * FROM sysobjects WHERE name = 'GetFlashPortionCircle_pr')
BEGIN
	GRANT EXEC ON GetFlashPortionCircle_pr TO webaccess
	PRINT 'GetFlashPortionCircle_pr Added'
END
ELSE
BEGIN
	PRINT 'Error'
END
