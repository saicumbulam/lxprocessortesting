USE [Lightning]
GO

BEGIN TRANSACTION

IF EXISTS (
            SELECT * FROM dbo.sysobjects 
            WHERE   id = object_id(N'[lxdf].[PartnerGet]') AND
                    OBJECTPROPERTY(id, N'IsProcedure') = 1)

    DROP PROCEDURE [lxdf].[PartnerGet]
GO

CREATE PROCEDURE [lxdf].[PartnerGet]
	@partnerId varchar(50) = NULL,
	@onlyActivePartners bit = 1,
	@feedTypeId int = 1
AS
	SET NOCOUNT ON
	
	SELECT	p.PartnerId,
			p.PartnerName,
			p.IsActive,
			p.ResponseFormatTypeId,
			p.ResponsePackageTypeId,
			p.IsCloudToGroundEnabled,
			p.IsInCloudEnabled,
			p.BoundingArea,
			p.NetworkTypeId,
			p.Metadata,
			p.IsInclusionBoundingArea
	FROM	lxdf.Partner p with(nolock)
	WHERE	p.PartnerId = ISNULL(@partnerId, p.PartnerId)
	AND		p.IsActive = CASE @onlyActivePartners 
							WHEN 1 THEN 1 
							ELSE p.IsActive 
							END
  AND p.PartnerId in ( SELECT PartnerId from lxdf.PartnerFeedType where FeedTypeId = @feedTypeId)	 
  ORDER BY p.PartnerName

  GO

IF EXISTS (
            SELECT * FROM dbo.sysobjects 
            WHERE   id = object_id(N'[bizops].[CreatePartner]') AND
                    OBJECTPROPERTY(id, N'IsProcedure') = 1)

    DROP PROCEDURE [bizops].[CreatePartner]
GO

CREATE PROCEDURE [bizops].[CreatePartner]
	@partnerName varchar(100),				-- To set partners name
	@isCloudToGroundEnabled bit,			-- To set CG
	@isInCloudEnabled bit,					-- To set IC
	@partnerId varchar(50) = NULL OUTPUT,	-- If not provided, new GUID created
	@responseFormatTypeId tinyint = NULL,	-- Only needed for v1 partner
	@responsePackageTypeId tinyint = NULL,	-- Only needed for v1 partner
	@boundingArea varchar(200) = NULL,		-- In WKT (Well Known Text) format: 'POLYGON((lon1 lat1, lon2 lat2, lon3 lat3, lon4 lat4, lon1 lat1))'
	@sInclusionBoundingArea bit = 1,		-- inlcude or exclude flashes within the bounding box
	@ipAddress varchar(39) = NULL,			-- Optional partner IP
	@lastModifiedBy varchar(100) = NULL,	-- Will default to currently logged in user
	@networkTypeId int,						-- Type of Network
	@displayResults bit = 1,				-- Will output new row(s)
	@metadata bit = 0						-- Metadata
AS
	---
	--- This SP will create a new partner row and optionally add
	--- an IP address for this partner to their whitelist.
	---
	SET NOCOUNT ON
	SET XACT_ABORT ON
	
	IF @responsePackageTypeId = 3 AND @responseFormatTypeId != 2
	BEGIN
		PRINT 'ERROR: Invalid @responseFormatTypeId and @responsePackageTypeId combination. When @responsePackageTypeId=3 then @responseFormatTypeId must equal 2.'
		RETURN -1
	END
	
	IF @partnerId IS NULL
	BEGIN
		SET @partnerId = newid()
	END

	BEGIN TRAN
	
	INSERT INTO lxdf.[Partner]
           (PartnerId
           ,PartnerName
           ,IsActive
           ,ResponseFormatTypeId
           ,ResponsePackageTypeId
           ,IsCloudToGroundEnabled
           ,IsInCloudEnabled
           ,BoundingArea
		   ,IsInclusionBoundingArea
           ,NetworkTypeId
           ,Metadata
           ,LastModifiedDateUtc
           ,LastModifiedBy)
     VALUES
           (@partnerId
           ,@partnerName
           ,1
           ,@responseFormatTypeId
           ,@responsePackageTypeId
           ,@isCloudToGroundEnabled
           ,@isInCloudEnabled
           ,@boundingArea
		   ,@sInclusionBoundingArea
           ,@networkTypeId
           ,@metadata
           ,GETUTCDATE()
           ,ISNULL(@lastModifiedBy, SUSER_SNAME()))

	DECLARE @ipReturnCode int		   
	EXEC @ipReturnCode = bizops.AddPartnerIpWhitelist
			@partnerId=@partnerId,
			@ipAddress=@ipAddress,
			@lastModifiedBy=@lastModifiedBy,
			@displayResults=0
		
	if (@ipReturnCode = -1)
	BEGIN
		ROLLBACK
		PRINT 'Transaction has been rolled back. Failure creating IP Whitelist.' 
		RETURN -1
	END
	ELSE
	BEGIN
		COMMIT
		PRINT 'Transaction committed successfully.'
		
		IF @displayResults = 1
		BEGIN
			SELECT * FROM lxdf.[Partner] with(nolock) WHERE PartnerId = @partnerId
			SELECT * FROM lxdf.PartnerIpWhitelist with(nolock) WHERE PartnerId = @partnerId
		END		
	END
GO

COMMIT

--DECLARE @RC int
--DECLARE @partnerName varchar(100) = '!WeatherZone'
--DECLARE @isCloudToGroundEnabled bit = 1
--DECLARE @isInCloudEnabled bit = 1
--DECLARE @partnerId varchar(50) = NULL
--DECLARE @responseFormatTypeId tinyint = 1
--DECLARE @responsePackageTypeId tinyint = 1
--DECLARE @boundingArea varchar(200) = 'POLYGON ((90 5, 90 -60, 180 -60, 180 5, 90 5))'
--DECLARE @sInclusionBoundingArea bit = 0
--DECLARE @ipAddress varchar(39) = null
--DECLARE @lastModifiedBy varchar(100) = null
--DECLARE @networkTypeId int = 1
--DECLARE @displayResults bit = 1
--DECLARE @metadata bit = 1,

---- TODO: Set parameter values here.

--EXECUTE @RC = [bizops].[CreatePartner] 
--   @partnerName
--  ,@isCloudToGroundEnabled
--  ,@isInCloudEnabled
--  ,@partnerId OUTPUT
--  ,@responseFormatTypeId
--  ,@responsePackageTypeId
--  ,@boundingArea
--  ,@sInclusionBoundingArea
--  ,@ipAddress
--  ,@lastModifiedBy
--  ,@networkTypeId
--  ,@displayResults
--  ,@metadata
--GO

--INSERT INTO lxdf.PartnerFeedType VALUES('BEB0F5DF-AE02-48E7-AC17-3609A4FCFD62', 1, GETUTCDATE(), 'AWSCORP\jdearing')