IF EXISTS (SELECT * FROM sysobjects WHERE name = 'LtgNetworkUpdate_pr')
BEGIN
	DROP PROCEDURE LtgNetworkUpdate_pr	
END

GO

CREATE PROC LtgNetworkUpdate_pr
@Id nchar(10),
@Name varchar(50),
@Desc varchar(250) = NULL

AS

/* LtgNetworkUpdate_pr.sql */

BEGIN 

UPDATE	dbo.LtgNetworkList
SET		LtgNetworkName = @Name
		,[Description] = @Desc
		,LastModifiedBy = 'LtgNetworkUpdate_pr'
		,LastModifiedTime = GETUTCDATE()
WHERE	LtgNetworkID = @Id			  

SELECT [LtgNetworkID]
		,[LtgNetworkName]
		,[Description]
FROM	[dbo].[LtgNetworkList] WITH (NOLOCK)
WHERE	[LtgNetworkID] = @Id


END

GO

IF EXISTS (SELECT * FROM sysobjects WHERE name = 'LtgNetworkUpdate_pr')
BEGIN
	GRANT EXEC ON LtgNetworkUpdate_pr TO webaccess
	PRINT 'LtgNetworkUpdate_pr Added'
END
ELSE
BEGIN
	PRINT 'Error'
END
