IF EXISTS (SELECT * FROM sysobjects WHERE name = 'LtgNetworkDelete_pr')
BEGIN
	DROP PROCEDURE LtgNetworkDelete_pr	
END

GO

CREATE PROC LtgNetworkDelete_pr
@Id nchar(10)

AS
/* LtgNetworkDelete_pr.sql */

BEGIN 

DELETE FROM dbo.LtgNetworkSensor
WHERE LtgNetworkID = @Id

DELETE FROM dbo.LtgNetworkList
WHERE LtgNetworkID = @Id

END

GO

IF EXISTS (SELECT * FROM sysobjects WHERE name = 'LtgNetworkDelete_pr')
BEGIN
	GRANT EXEC ON LtgNetworkDelete_pr TO webaccess
	PRINT 'LtgNetworkDelete_pr Added'
END
ELSE
BEGIN
	PRINT 'Error'
END
