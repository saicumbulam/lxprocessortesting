IF EXISTS (SELECT * FROM sysobjects WHERE name = 'LtgGetStationData_pr')
BEGIN
	DROP PROCEDURE LtgGetStationData_pr	
END

GO

CREATE PROC LtgGetStationData_pr

AS

/* LtgGetStationData_pr.sql */

BEGIN 

SELECT	LTGStatList.Station_ID
		,LTGStatList.Sensor_Version 
		,LTGStatList.Antenna_Version
		,LTGStatList.Latitude
		,LTGStatList.Longitude
   		,LTGStatList.Altitude
		,ISNULL(LTGQCFlags.QCFlag, 100) AS QCFlag
		,LtgNetworkID
 FROM	dbo.LTGStatList 
			LEFT OUTER JOIN dbo.LtgQCFlags
				ON LTGStatList.Station_ID = LTGQCFlags.Station_ID
			LEFT OUTER JOIN dbo.LtgNetworkSensor
				ON LTGStatList.Station_ID = LtgNetworkSensor.Station_ID
ORDER BY LTGStatList.Station_ID, LtgNetworkSensor.LtgNetworkID 

END

GO

IF EXISTS (SELECT * FROM sysobjects WHERE name = 'LtgGetStationData_pr')
BEGIN
	GRANT EXEC ON LtgGetStationData_pr TO webaccess
	GRANT EXEC ON LtgGetStationData_pr TO appaccess
	PRINT 'LtgGetStationData_pr Added'
END
ELSE
BEGIN
	PRINT 'Error'
END
