USE [Lightning]
GO

INSERT INTO [dbo].[LtgNetworkList]
           ([LtgNetworkID]
           ,[LtgNetworkName]
           ,[Description]
           ,[LastModifiedBy]
           ,[LastModifiedTime])
     VALUES
           (N'WWLLN-Push'
           ,'WWLLN-Push'
           ,'Sensors pushed to WWLLN'
           ,SYSTEM_USER
           ,GETUTCDATE())
GO

INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'ADDSB', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'AKSUK', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'AKSYL', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'ANCHR', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'AUK02', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'AUK10', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'AUK11', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'AUK12', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'AUK16', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'AUK19', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'AUK22', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'AUK24', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'AUK25', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'AUK28', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'AUK29', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'AUK32', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'AUK34', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'AUK35', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'AUK37', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'AUK39', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'AUK43', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'AUK44', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'AUK47', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'AUK48', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'AUK49', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'AUK52', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'AUK60', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'AUK62', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'AUK63', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'AUK64', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'AUK65', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'AUK66', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'AUK69', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'AUK70', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'AUK71', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'AUK72', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'AUK73', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'AUK75', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'AUK76', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'AUK77', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'AUK79', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'AUK81', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'AUK83', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'AUK87', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'AUK88', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'AWSHQ5', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'BANGU', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'BFFLD', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'BGTNM', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'BHRLS', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'BJMBR', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'BLGRD', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'BLKHS', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'BNDRS', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'BNGLR', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'BNNBR', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'BNNEY', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'BOBOD', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'BOFAA', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'BOKEE', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'BRGNN', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'BRNDS', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'BRS10', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'BRS12', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'BRS13', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'BRS14', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'BRS18', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'BRS20', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'BRS22', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'BRS28', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'BRS29', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'BRS34', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'BRS35', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'BRS38', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'BRS44', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'BRS49', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'BRS50', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'BRS54', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'BRS55', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'BURSA', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'CANAK', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'CHCNZ', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'CHILE', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'CHNGM', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'CHNSL', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'CLRNC', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'CMNRM', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'CPNHG', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'CPSMB', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'CTDLV', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'DACSC', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'DAKAR', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'DATKA', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'DBRGR', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'DMPLS', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'ELKNV', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'ELLSW', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'ELYNV', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'EVRGL', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'FINCA', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'FLGTR', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'FRZRL', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'GRCMR', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'GRNCN', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'GRNPK', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'GRZGR', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'GTMCT', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'GTNRL', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'GULF1', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'GURUE', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'HANAL', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'HILO1', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'HMBLD', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'HNLLQ', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'INHSS', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'ISTNB', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'IZMIR', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'JNKPR', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'JSPRS', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'JUNEU', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'KCOSE', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'KHWSR', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'KLKTB', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'KNTNN', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'KONYA', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'L2KAB', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'L2RBT', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'LBRVL', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'LFYTN', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'LKSVL', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'LMPRM', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'LMSSD', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'LNGKW', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'LNGWM', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'LRCAY', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'LSKMR', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'LVNPG', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'MAD02', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'MDRLK', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'MDWSK', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'MIDVO1', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'MNZNL', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'MOSTA', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'MPTMZ', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'MRDLP', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'MRRBL', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'MRSJH', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'MUZAC', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'MYRDG', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'MZMB2', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'MZMB5', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'MZMB7', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'NDJMN', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'NIAMY', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'NIMET', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'NIVCL', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'NNGVL', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'NOIDA', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'NPMSN', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'NRBNT', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'NTLSD', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'NWLLS', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'OGDOU', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'ORA03', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'ORA06', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'ORA07', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'ORA08', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'ORA09', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'OUESS', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'PCRCL', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'PHILL', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'PHNMP', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'PMBMZ', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'PMPST', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'PNMSP', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'PNRDG', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'PNTNR', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'PRCSH', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'PRHCZ', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'PRTCR', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'PRTMR', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'PRTNC', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'PVMXL', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'PWNCK', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'QTCTO', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'RDNTH', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'RDSCH', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'REN01', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'REN04', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'RISRM', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'RYKJV', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'SBNSD', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'SGMSO', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'SHNYN', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'SKAMB', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'SKBHJ', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'SKBHO', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'SKBIK', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'SKCOI', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'SKCTK', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'SKHYD', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'SKJAI', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'SKJLD', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'SKLEH', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'SKLKN', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'SKNAG', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'SKPTN', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'SKUDP', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'SKUDU', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'SKVSA', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'SLNJK', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'SMBKK', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'SMPLV', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'SMWLL', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'SNTDR', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'SP026', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'SRBYS', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'STHMB', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'STHPD', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'STMNS', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'STNLN', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'STTDP', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'THMSD', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'TMPCO', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'TNKFR', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'TNRFE', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'TPMRC', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'TRNPL', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'TSM01', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'TTMHK', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'TWNSV', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'UEC01', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'UEC07', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'UEC13', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'UEC14', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'UGNPK', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'UKASK', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'UKKPY', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'UKRHV', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'UKSML', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'UKSRN', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'ULNBT', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'UNLSK', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'USHRL', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'VIC03', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'VRZNB', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'WLDHF', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'WLDWL', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'WNDHK', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'WNGRT', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'YANGN', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'YNDCM', SYSTEM_USER, GETUTCDATE())
GO
INSERT INTO LtgNetworkSensor ([LtgNetworkID] , [Station_ID], [LastModifiedBy], [LastModifiedTime]) VALUES('WWLLN-Push', 'ZGREB', SYSTEM_USER, GETUTCDATE())
