/****** Script for SelectTopNRows command from SSMS  ******/
SELECT	Stroke_Type AS StrokeType
		,Lightning_Time_String AS LightningTime
		,Latitude
		,Longitude
		,Amplitude
		,Height
		,SUBSTRING(Stroke_Solution, CHARINDEX('"ns":', Stroke_Solution, 0) + 5, CHARINDEX(',', Stroke_Solution, CHARINDEX('"ns":', Stroke_Solution, 0) + 5) - (CHARINDEX('"ns":', Stroke_Solution, 0) + 5)) AS NumberStations
		,PortionCount.Multi AS Multiplicity
FROM	[LxArchive2015].[dbo].[LtgFlashPortions_History201507] WITH (NOLOCK),
			(SELECT	FlashGuid
					,COUNT(LtgFlashPortions_History_ID) AS Multi
			FROM	[LxArchive2015].[dbo].[LtgFlashPortions_History201507] WITH (NOLOCK)
			WHERE	Lightning_Time >= '7/26/2015 15:00' AND Lightning_Time < '7/26/2015 20:02'
			GROUP BY FlashGUID) AS PortionCount
WHERE	LtgFlashPortions_History201507.FlashGUID = PortionCount.FlashGuid
AND		Lightning_Time >= '7/26/2015 15:00' AND Lightning_Time < '7/26/2015 20:00'
ORDER BY Lightning_Time_String

