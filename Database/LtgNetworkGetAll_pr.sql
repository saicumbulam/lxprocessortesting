IF EXISTS (SELECT * FROM sysobjects WHERE name = 'LtgNetworkGetAll_pr')
BEGIN
	DROP PROCEDURE LtgNetworkGetAll_pr	
END

GO

CREATE PROC LtgNetworkGetAll_pr

AS

/* LtgNetworkGetAll_pr.sql */

BEGIN 

	SELECT [LtgNetworkID]
		,[LtgNetworkName]
		,[Description]
	FROM [dbo].[LtgNetworkList] WITH (NOLOCK)
	ORDER BY [LtgNetworkName]

END

GO

IF EXISTS (SELECT * FROM sysobjects WHERE name = 'LtgNetworkGetAll_pr')
BEGIN
	GRANT EXEC ON LtgNetworkGetAll_pr TO webaccess
	PRINT 'LtgNetworkGetAll_pr Added'
END
ELSE
BEGIN
	PRINT 'Error'
END
