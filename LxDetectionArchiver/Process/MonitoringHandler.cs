﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

using Newtonsoft.Json.Linq;

using Aws.Core.Data.Common.Operations;
using Aws.Core.Utilities;

using LxDetectionArchiver.Common;
using LxDetectionArchiver.Interfaces;

namespace LxDetectionArchiver.Process
{
    public class MonitoringHandler : IRunnable
    {
        private readonly string _monitorHmtl;
        private string _systemType;
        private readonly Uri _datafeedStatusUrl;
        private DateTime _prevDatafeedFlashProcessedUtc;
        private DateTime? _nextCheckTime;
        private readonly DateTime _startTime;
        private readonly List<IStatusProvider> _statusProviders;

        public MonitoringHandler(List<IStatusProvider> statusProviders, Uri datafeedStatusUrl, string systemType)
        {
            _statusProviders = statusProviders;
            _datafeedStatusUrl = datafeedStatusUrl;
            _systemType = systemType;

            _startTime = DateTime.UtcNow;
            _prevDatafeedFlashProcessedUtc = _startTime;
            _nextCheckTime = null;

            try
            {
                _monitorHmtl = File.ReadAllText(AppUtils.AppLocation + @"\Monitor.html", Encoding.UTF8);
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventId.Process.MonitoringHandler.LoadMonitorFile, "Unable to load monitor html file", ex);
                _monitorHmtl = "Unavailable";
            }
        }

        //public MonitoringHandler(DatabaseWriter databaseWriter, DatabaseWriter retryDatabaseWriter, FileReader fileReader, FileCleaner fileCleaner, AwsS3FileArchiver awsS3FileArchiver, 
        //    string systemType, string invalidFlashFolder, string invalidPortionFolder, string failedFlashFolder, string failedPortionFolder, string pickupFolder, Uri datafeedStatusUrl)
        //{   
            //_databaseWriter = databaseWriter;
            //_retryDatabaseWriter = retryDatabaseWriter;
            //_fileReader = fileReader;
            //_fileCleaner = fileCleaner;
            //_awsS3FileArchiver = awsS3FileArchiver;
            //_systemType = systemType;
            //_invalidFlashFolder = invalidFlashFolder;
            //_invalidPortionFolder = invalidPortionFolder;
            //_failedFlashFolder = failedFlashFolder;
            //_failedPortionFolder = failedPortionFolder;
            //_pickupFolder = pickupFolder;
            //_datafeedStatusUrl = datafeedStatusUrl;

            //_startTime = DateTime.UtcNow;
            //_prevDatafeedFlashProcessedUtc = _startTime;
            //_nextCheckTime = null;

            //try
            //{
            //    _monitorHmtl = File.ReadAllText(AppUtils.AppLocation + @"\Monitor.html", Encoding.UTF8);
            //}
            //catch (Exception ex)
            //{
            //    EventManager.LogError(EventId.Process.MonitoringHandler.LoadMonitorFile, "Unable to load monitor html file", ex);
            //    _monitorHmtl = "Unavailable";
            //}
        //}

        public void Start()
        {
            try
            {
                OpsCommandHttpEndpointSvc.Start((cmd) => MonitorStatusCheck(cmd));
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventId.Process.MonitoringHandler.StartOpsHttp, "Unable to start ecv http endpoint", ex);
            }
        }

        public void Stop()
        {
            try
            {
                OpsCommandHttpEndpointSvc.Stop();
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventId.Process.MonitoringHandler.StopOpsHttp, "Unable to stop ecv http endpoint", ex);
            }
        }

        private string MonitorStatusCheck(string cmd)
        {
            var statusData = GetStatusData();

            if (cmd.Trim().Equals("PRTG", StringComparison.InvariantCultureIgnoreCase))
            {
                return GeneratePrtgStatus(statusData);
                //response = GeneratePrtgStatus(_invalidFlashFolder, _invalidPortionFolder, _failedFlashFolder, _failedPortionFolder, _pickupFolder, lastDatafeedFlashProcessedUtc, 
                //    _awsS3FileArchiver);
            }
            else
            {
                return GenerateHtmlStatus(statusData);
                //return GenerateHtmlStatus(_systemType, utcNow, _monitorHmtl, _invalidFlashFolder, _invalidPortionFolder, _failedFlashFolder, _failedPortionFolder, 
                //    _pickupFolder, lastDatafeedFlashProcessedUtc, _nextCheckTime, _awsS3FileArchiver);
            }

        }

        //private string GeneratePrtgStatus(string invalidFlashFolder, string invalidPortionFolder, string failedFlashFolder, string failedPortionFolder, string pickupFileFolder, DateTime lastDatafeedFlashProcessedUtc, AwsS3FileArchiver awsS3FileArchiver)
        private string GeneratePrtgStatus(IEnumerable<KeyValuePair<string, string>> statusData)
        {
            var j = new JObject();

            foreach (var status in statusData)
            {
                j.Add(status.Key, status.Value);
            }
            return j.ToString();
        }

        private const string Success = "<span style=\"color: green;\">SUCCESS</span>";
        private const string Failure = "<span style=\"color: red;\">FAIL</span>";
        private const string NA = "-";
        //private string GenerateHtmlStatus(string systemType, DateTime utcNow, string monitorHmtl, string invalidFlashFolder, string invalidPortionFolder, string failedFlashFolder, 
        //    string failedPortionFolder, string pickupFolder, DateTime lastDatafeedFlashProcessedUtc, DateTime? nextCheckTime, AwsS3FileArchiver awsS3FileArchiver)
        private string GenerateHtmlStatus(IEnumerable<KeyValuePair<string, string>> statusData)
        {
            string html = null;

            try
            {
                var sd = statusData as KeyValuePair<string, string>[] ?? statusData.ToArray();
                html = string.Format(_monitorHmtl,
                    AppUtils.GetVersion(), //0
                    DateTime.UtcNow.ToString(new CultureInfo("en-US")),

                    sd.FirstOrDefault(s => s.Key.Equals("isAgeLastFlashSuccess")).Value ?? Failure,
                    sd.FirstOrDefault(s => s.Key.Equals("ageLastFlashWrittenToDbSeconds")).Value ?? NA,

                    sd.FirstOrDefault(s => s.Key.Equals("isAgeProcessedSuccess")).Value ?? Failure,
                    sd.FirstOrDefault(s => s.Key.Equals("ageLastFileReadSeconds")).Value ?? NA,

                    sd.FirstOrDefault(s => s.Key.Equals("isS3AgeProcessedSuccess")).Value ?? Failure,
                    sd.FirstOrDefault(s => s.Key.Equals("s3AgeLastFileReadSeconds")).Value ?? NA,

                    sd.FirstOrDefault(s => s.Key.Equals("isAgeArchivedSuccess")).Value ?? Failure,
                    sd.FirstOrDefault(s => s.Key.Equals("ageLastFileArchivedSeconds")).Value ?? NA,

                    sd.FirstOrDefault(s => s.Key.Equals("isAgeLastCleanedSuccess")).Value ?? Failure,
                    sd.FirstOrDefault(s => s.Key.Equals("ageLastFileCleanupMinutes")).Value ?? NA,

                    sd.FirstOrDefault(s => s.Key.Equals("isInvalidFileCountSuccess")).Value ?? Failure,
                    sd.FirstOrDefault(s => s.Key.Equals("invalidFileCount")).Value ?? NA,

                    sd.FirstOrDefault(s => s.Key.Equals("isS3InvalidFileCountSuccess")).Value ?? Failure,
                    sd.FirstOrDefault(s => s.Key.Equals("s3InvalidFileCount")).Value ?? NA,

                    sd.FirstOrDefault(s => s.Key.Equals("isLastS3ArchiveSuccess")).Value ?? Failure,
                    sd.FirstOrDefault(s => s.Key.Equals("ageLastS3ArchiveSeconds")).Value ?? NA,


                    sd.FirstOrDefault(s => s.Key.Equals("databaseWriterQueueCount")).Value ?? NA,
                    sd.FirstOrDefault(s => s.Key.Equals("databaseWriterActiveTasks")).Value ?? NA,
                    sd.FirstOrDefault(s => s.Key.Equals("databaseWriterMaxTasks")).Value ?? NA,
                    sd.FirstOrDefault(s => s.Key.Equals("databaseWriterInsertRatePerMinute")).Value ?? NA,



                    sd.FirstOrDefault(s => s.Key.Equals("retryDatabaseWriterQueueCount")).Value ?? NA,
                    sd.FirstOrDefault(s => s.Key.Equals("retryDatabaseWriterActiveTasks")).Value ?? NA,
                    sd.FirstOrDefault(s => s.Key.Equals("retryDatabaseWriterMaxTasks")).Value ?? NA,
                    sd.FirstOrDefault(s => s.Key.Equals("retryDatabaseWriterInsertRatePerMinute")).Value ?? NA,



                    sd.FirstOrDefault(s => s.Key.Equals("pickupFileCount")).Value ?? NA,
                    sd.FirstOrDefault(s => s.Key.Equals("failedFileCount")).Value ?? NA,
                    sd.FirstOrDefault(s => s.Key.Equals("ageLastDatafeedFlashProcessedSeconds")).Value ?? NA,
                    sd.FirstOrDefault(s => s.Key.Equals("s3PickupFileCount")).Value ?? NA,
                    sd.FirstOrDefault(s => s.Key.Equals("s3FailedFileCount")).Value ?? NA
                    
                    );
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventId.Process.MonitoringHandler.BuildMonitorHtml, "Unable to build monitor html file", ex);
            }

            return html;
        }

        private int GetFileCount(string folder)
        {
            var count = 0;

            if (!string.IsNullOrWhiteSpace(folder))
            {
                try
                {
                    var di = new DirectoryInfo(folder);
                    if (di.Exists)
                    {
                        var fi = di.GetFiles("*", SearchOption.TopDirectoryOnly);
                        count = fi.Length;
                    }
                }
                catch (Exception ex)
                {
                    EventManager.LogError(EventId.Process.MonitoringHandler.GetFileCount, "Unable to get file count", ex);
                }
            }

            return count;
        }

        private static readonly int HttpTimeoutMilliSeconds = Config.DatafeedStatusHttpTimeoutSeconds * 1000;
        private DateTime GetDatafeedStatus(Uri datafeedStatusUrl)
        {
            var lastFlashProcessed = DateTime.MinValue;

            try
            {
                var wc = new WebClientWithTimeout(HttpTimeoutMilliSeconds);
                wc.DownloadString(datafeedStatusUrl);
                
                var body = wc.DownloadString(datafeedStatusUrl);

                if (!string.IsNullOrWhiteSpace(body))
                    lastFlashProcessed = ParseResponse(body);
                else
                    EventManager.LogError(EventId.Process.MonitoringHandler.GetDatafeedStatus, $"{datafeedStatusUrl} returned an empty body");
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventId.Process.MonitoringHandler.GetDatafeedStatus, $"Error calling {datafeedStatusUrl}", ex);
            }

            return lastFlashProcessed;
        }

        private DateTime ParseResponse(string body)
        {
            var lastFlashProcessed = DateTime.MinValue;

            try
            {
                var jo = JObject.Parse(body);
                JToken jt;
                if (jo.TryGetValue("lastFlashProcessedUtc", out jt))
                {
                    lastFlashProcessed = jt.ToObject<DateTime>();
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventId.Process.MonitoringHandler.GetDatafeedStatus, "Error processing datafeed status body", ex);
            }

            return lastFlashProcessed;
        }

        private class WebClientWithTimeout : WebClient
        {

            private readonly int _timeout;
            public WebClientWithTimeout(int timeoutMilliSeconds)
            {
                _timeout = timeoutMilliSeconds;
            }
            
            protected override WebRequest GetWebRequest(Uri uri)
            {
                var w = base.GetWebRequest(uri);
                w.Timeout = _timeout;
                return w;
            }
        }

        private IEnumerable<KeyValuePair<string, string>> GetStatusData()
        {
            var statusData = new List<KeyValuePair<string, string>>();
            foreach (var statusProvider in _statusProviders)
            {
                statusData.AddRange(statusProvider.GetMonitorigStatus());
            }


            var utcNow = DateTime.UtcNow;

            var lastDatafeedFlashProcessedUtc = GetDatafeedStatus(_datafeedStatusUrl);
            if (lastDatafeedFlashProcessedUtc == DateTime.MinValue)
                lastDatafeedFlashProcessedUtc = _startTime;

            //nextCheckTime serves as a grace period for the archiver to respond after a long period of inactivity
            if (_nextCheckTime.HasValue && utcNow > _nextCheckTime)
                _nextCheckTime = null;

            if (!_nextCheckTime.HasValue)
            {
                var lastFlashDelta = lastDatafeedFlashProcessedUtc - _prevDatafeedFlashProcessedUtc;
                if (lastFlashDelta.TotalSeconds > Config.MonitorLastFlashDbAgeSecondsMaximum)
                    _nextCheckTime = lastDatafeedFlashProcessedUtc.AddSeconds(Config.MonitorLastFlashDbAgeSecondsMaximum);
            }



            var ageLastDatafeedFlashProcessed = Math.Round((utcNow - lastDatafeedFlashProcessedUtc).TotalSeconds, 1, MidpointRounding.AwayFromZero);
            statusData.Add(new KeyValuePair<string, string>("ageLastDatafeedFlashProcessedSeconds", ageLastDatafeedFlashProcessed.ToString(CultureInfo.InvariantCulture)));


            var isAgeLastFlashSuccess = false;
            if (_nextCheckTime.HasValue && _nextCheckTime.Value > utcNow)
                isAgeLastFlashSuccess = true;
            else
            {
                var lastFlashDate = ConvertStatusValueToDateTime(statusData.FirstOrDefault(s => s.Key.Equals("lastFlashWrittenToDbTimestampUtc")).Value);
                isAgeLastFlashSuccess = lastFlashDate >= lastDatafeedFlashProcessedUtc.AddSeconds(-Config.MonitorLastFlashDbAgeSecondsMaximum);
            }
            statusData.Add(new KeyValuePair<string, string>("isAgeLastFlashSuccess", isAgeLastFlashSuccess ? Success : Failure));


            var isAgeProcessedSuccess = false;
            var lastFileProcessedTimestamp = ConvertStatusValueToDateTime(statusData.FirstOrDefault(s => s.Key.Equals("lastFileProcessedTimestampUtc")).Value);
            if (_nextCheckTime.HasValue && _nextCheckTime.Value > utcNow)
                isAgeProcessedSuccess = true;
            else
            {
                isAgeProcessedSuccess = (lastFileProcessedTimestamp >= lastDatafeedFlashProcessedUtc.AddSeconds(-Config.MonitorLastFileProcessedAgeSecondsMaximum));
            }
            statusData.Add(new KeyValuePair<string, string>("isAgeProcessedSuccess", isAgeProcessedSuccess ? Success : Failure));


            var isS3AgeProcessedSuccess = false;
            var s3LastFileProcessedTimestamp = ConvertStatusValueToDateTime(statusData.FirstOrDefault(s => s.Key.Equals("s3LastFileProcessedTimestampUtc")).Value);
            if (_nextCheckTime.HasValue && _nextCheckTime.Value > utcNow)
                isS3AgeProcessedSuccess = true;
            else
            {
                isS3AgeProcessedSuccess = (s3LastFileProcessedTimestamp >= lastDatafeedFlashProcessedUtc.AddSeconds(-Config.MonitorLastFileProcessedAgeSecondsMaximum));
            }
            statusData.Add(new KeyValuePair<string, string>("isS3AgeProcessedSuccess", isS3AgeProcessedSuccess ? Success : Failure));


            var isAgeArchivedSuccess = false;
            var lastFileArchivedTimestamp = ConvertStatusValueToDateTime(statusData.FirstOrDefault(s => s.Key.Equals("lastFileArchivedTimestampUtc")).Value);
            if (_nextCheckTime.HasValue && _nextCheckTime.Value > utcNow)
                isAgeArchivedSuccess = true;
            else
                isAgeArchivedSuccess = (lastFileArchivedTimestamp >= lastDatafeedFlashProcessedUtc.AddSeconds(-Config.MonitorLastFileArchivedAgeSecondsMaximum));
            statusData.Add(new KeyValuePair<string, string>("isAgeArchivedSuccess", isAgeArchivedSuccess ? Success : Failure));



            var ageLastClean = Convert.ToInt32(statusData.FirstOrDefault(s => s.Key.Equals("ageLastFileCleanupMinutes")).Value);
            statusData.Add(new KeyValuePair<string, string>("isAgeLastCleanedSuccess", (ageLastClean <= Config.MonitorLastCleanupAgeMinutesMaximum) ? Success : Failure));



            var invalidFileCount = Convert.ToInt32(statusData.FirstOrDefault(s => s.Key.Equals("invalidFileCount")).Value);
            statusData.Add(new KeyValuePair<string, string>("isInvalidFileCountSuccess", (invalidFileCount <= Config.MonitorInvalidFileCountMaximum) ? Success : Failure));

            var s3InvalidFileCount = Convert.ToInt32(statusData.FirstOrDefault(s => s.Key.Equals("s3InvalidFileCount")).Value);
            statusData.Add(new KeyValuePair<string, string>("isS3InvalidFileCountSuccess", (s3InvalidFileCount <= Config.MonitorInvalidFileCountMaximum) ? Success : Failure));

            var ageLastS3Archive = Convert.ToInt32(statusData.FirstOrDefault(s => s.Key.Equals("ageLastS3ArchiveSeconds")).Value);
            statusData.Add(new KeyValuePair<string, string>("isLastS3ArchiveSuccess", (ageLastS3Archive <= Config.MonitorLastS3ArchivepAgeSecondsMaximum) ? Success : Failure));

            _prevDatafeedFlashProcessedUtc = lastDatafeedFlashProcessedUtc;

            return statusData;
        }

        private DateTime ConvertStatusValueToDateTime(string timestamp)
        {
            var lastFlashDate = DateTime.MinValue;
            if (!string.IsNullOrWhiteSpace(timestamp))
            {
                DateTime.TryParse(timestamp, out lastFlashDate);
            }

            return lastFlashDate;
        }
    }
}
