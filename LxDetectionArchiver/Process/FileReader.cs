﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading;
using Newtonsoft.Json; 
using Aws.Core.Utilities; 
using LxCommon.Models;
using LxCommon.Utilities; 
using LxDetectionArchiver.Common;

namespace LxDetectionArchiver.Process
{
    public class FileReader
    {
        private Timer _importTimer;
        private DirectoryInfo _pickupFolder;
        private DirectoryInfo _archiveFolder;
        private int _importIntervalSeconds;
        private string _filePattern;
        private DatabaseWriter _databaseWriter;
        private bool _isPortionReader;
        private string _etlConn;
        private int _maxBrokerEndpointCount;

        private const double MaxFailurePercent = 0.05;
        private const string BadDatePath =  "Unknown\\";

        private DateTime _lastFileProcessed;
        private DateTime _lastFileArchived;
        private DateTime _lastS3Flush;


        public FileReader(string pickupFolder, int importIntervalSeconds, string archiveFolder, string filePattern, DatabaseWriter databaseWriter, bool isPortionReader, 
            string etlConn, int maxBrokerEndpointCount)
        {
            _pickupFolder = new DirectoryInfo(pickupFolder);
            _archiveFolder = null;
            if (archiveFolder != null)
            {
                _archiveFolder = FileHelper.CreateDirectory(archiveFolder);
            }
            _importIntervalSeconds = importIntervalSeconds;
            _filePattern = filePattern;
            _databaseWriter = databaseWriter;
            _isPortionReader = isPortionReader;
            _etlConn = etlConn;
            _maxBrokerEndpointCount = maxBrokerEndpointCount;
            _lastFileProcessed = DateTime.UtcNow;
            _lastFileArchived = DateTime.UtcNow;
            _lastS3Flush = DateTime.UtcNow;
        }

        public void Start()
        {
            _importTimer = new Timer(Process, null, TimeSpan.FromSeconds(0), TimeSpan.FromMilliseconds(-1));
        }

        public void Stop()
        {
            if (_importTimer != null)
            {
                _importTimer.Dispose();
            }
        }

        private void Process(object state)
        {
            //EventManager.LogInfo("File Reader Process is called");
            if (Config.EnableDbArchive)
            {
                //EventManager.LogInfo("EnableDBArchive is true");
                // Database and S3
                if (_databaseWriter != null)
                {
                    //EventManager.LogInfo("databaseWriter object is set to true");
                    EventManager.LogInfo(string.Format("Database count : {0}, Second Condition : {1}", _databaseWriter.Count, IsDbBrokerReady(_etlConn, _maxBrokerEndpointCount)));

                    if (_databaseWriter.Count < 1 && IsDbBrokerReady(_etlConn, _maxBrokerEndpointCount))
                    {
                        //EventManager.LogInfo("Condition 1");
                        FileInfo fileInfo = GetOldestFile(_pickupFolder, _filePattern);
                        if (fileInfo != null)
                        {
                            //EventManager.LogInfo("Reading file");
                            bool success = ReadFile(fileInfo, _databaseWriter, _isPortionReader);

                            if (success)
                            {
                                _lastFileProcessed = FileHelper.GetFileTimeFromName(fileInfo.Name, _filePattern);
                                ArchiveFile(fileInfo, _archiveFolder, _filePattern);
                            }
                        }
                    }
                }
            }
            else
            {
                // Archive only
                FileInfo fileInfo = GetOldestFile(_pickupFolder, _filePattern);
                if (fileInfo != null)
                {
                    ArchiveFile(fileInfo, _archiveFolder, _filePattern);
                }
            }

            if (_importTimer != null)
            {
                _importTimer.Change(TimeSpan.FromSeconds(_importIntervalSeconds), TimeSpan.FromMilliseconds(-1));
            }
        }

        private bool IsDbBrokerReady(string etlConn, int maxBrokerEndpointCount)
        {
            bool isReady = true;
            using (SqlConnection conn = new SqlConnection(etlConn))
            {
                using (SqlCommand cmd = new SqlCommand("GetPortionBrokerEndpointCounts_pr", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        conn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                       
                        while (reader.Read())
                        {
                            
                            int stateCount = reader.GetInt32(1);
                            EventManager.LogInfo(string.Format("{0} - {1}", stateCount, maxBrokerEndpointCount));
                            if (stateCount >= maxBrokerEndpointCount)
                            {
                                isReady = false;
                                break;
                            }
                        }
                       
                    }
                    catch (Exception ex)
                    {
                        EventManager.LogError(EventId.Process.FileDbImporter.IsDbBrokerReady, "Error getting broker endpoint counts", ex);
                        isReady = false;
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }
            
            return isReady;
        }

        private FileInfo GetOldestFile(DirectoryInfo pickupFolder, string filePattern)
        {
            FileInfo[] fi;
            FileInfo oldestFile = null;

            try
            {
                fi = pickupFolder.GetFiles(filePattern, SearchOption.AllDirectories);

                if (fi.Length != 0)
                {
                    oldestFile = (from f in fi orderby f.CreationTime ascending select f).First();
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventId.Process.FileDbImporter.FindFiles, string.Format("Unable to find oldest file in directory: {0}", pickupFolder.FullName), ex);
            }

            return oldestFile;
        }

        private bool ReadFile(FileInfo fileInfo, DatabaseWriter databaseWriter, bool isPortionReader)
        {
            //EventManager.LogInfo("ReadFile method is called");
            bool success = true;
            int total = 0;
            int failures = 0;
            try
            {
                using (StreamReader reader = new StreamReader(fileInfo.FullName))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        try
                        {
                            Portion p = null;
                            Flash f = null;
                            if (isPortionReader)
                            {
                                p = JsonConvert.DeserializeObject<Portion>(line);
                            }
                            else
                            {
                                f = JsonConvert.DeserializeObject<Flash>(line);
                            }

                            Tuple<Flash, Portion> fp = new Tuple<Flash, Portion>(f, p);
                            ///EventManager.LogInfo("Add method called");
                            databaseWriter.Add(fp);
                        }
                        catch (Exception ex)
                        {
                            failures++;
                            EventManager.LogError(EventId.Process.FileDbImporter.Deserialize, string.Format("Unable deserialize to flash: {0}", line), ex);
                        }
                        total++;
                    }
                }

                if (total > 0 && failures > 0)
                {
                    double perc = (double)failures / total;
                    success = perc <= MaxFailurePercent;
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventId.Process.FileDbImporter.ReadFile, string.Format("Unable to read file: {0}", fileInfo.FullName), ex);
                success = false;
            }

            return success;
        }

        private void ArchiveFile(FileInfo fileInfo, DirectoryInfo archiveFolder, string filePattern)
        {
            if (archiveFolder != null)
            {
                DateTime dt = FileHelper.GetFileTimeFromName(fileInfo.Name, filePattern);
                string path = BadDatePath;
                if (dt != DateTime.MinValue)
                {
                    path = string.Format("{0:D4}\\{1:D2}\\{2:D2}\\{3:D2}\\", dt.Year, dt.Month, dt.Day, dt.Hour);
                }

                path = Path.Combine(archiveFolder.FullName, path);

                FileHelper.CreateDirectory(path);

                path = Path.Combine(path, fileInfo.Name);

                try
                {
                    if (File.Exists(path))
                    {
                        string appendText = File.ReadAllText(fileInfo.FullName);
                        File.AppendAllText(path, appendText);
                        fileInfo.Delete();
                    }
                    else
                    {
                        fileInfo.MoveTo(path);
                    }
                    
                    _lastFileArchived = dt;
                }
                catch (Exception ex)
                {
                    EventManager.LogError(EventId.Process.FileDbImporter.MoveFile, string.Format("Unable to move file from: {0} to: {1}", fileInfo.FullName, path), ex);
                }
            }
            else
            {
                try
                {
                    fileInfo.Delete();
                    _lastFileArchived = FileHelper.GetFileTimeFromName(fileInfo.FullName, filePattern);
                }
                catch (Exception ex)
                {
                    EventManager.LogError(EventId.Process.FileDbImporter.DeleteFile, string.Format("Unable to delete file: {0}", fileInfo.FullName), ex);
                }
            }
        }

        public DateTime LastFileProcessed
        {
            get { return _lastFileProcessed; }
        }

        public DateTime LastFileArchived
        {
            get { return _lastFileArchived; }
        }

        public int GetFileCount()
        {
            var count = 0;

            if (!string.IsNullOrWhiteSpace(_pickupFolder?.FullName))
            {
                try
                {
                    if (_pickupFolder.Exists)
                    {
                        var fi = _pickupFolder.GetFiles("*", SearchOption.TopDirectoryOnly);
                        count = fi.Length;
                    }
                }
                catch (Exception ex)
                {
                    EventManager.LogError(EventId.Process.MonitoringHandler.GetFileCount, "Unable to get file count", ex);
                }
            }

            return count;
        }
    }
}
