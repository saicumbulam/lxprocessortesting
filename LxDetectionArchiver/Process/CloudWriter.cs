﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Aws.Core.Utilities;
using En.Ods.Lib.Common.Dynamo;
using En.Ods.Lib.Common.Interface;
using En.Ods.Lib.Common.Serialization;
using LxCommon.Models;
using LxCommon.Utilities;
using LxDetectionArchiver.Common;

namespace LxDetectionArchiver.Process
{
    public class CloudWriter : IDisposable
    {
        private readonly Counters _counters;
        private readonly ICloudStorage _storage;

        private readonly ConcurrentDictionary<string, LinkedList<string>> _fileTracker;   //<quadkey, List<local path>>
        private readonly ReaderWriterLockSlim _fileTrackerLock;

        private readonly DirectoryInfo _stagingFolder;
        private readonly DirectoryInfo _transferFolder;

        private readonly string _bucketName;
        private readonly string _pathPrefix;
        private readonly int _maxFileSizeByte;
        private readonly int _maxConcurrentFileUploads;
        

        private const int MaxLockTries = 5;
        private const int MaxLockWaitMillseconds = 2;

        private const string JsonFileExtension = "json";

        private bool _disposed;
        private bool _started;
        private CancellationTokenSource _cancellationTokenSource;

        private const string FilesUploadedCounterName = "FilesUploaded";

        public CloudWriter(string bucketName)
        {
            if (string.IsNullOrWhiteSpace(bucketName)) throw new ArgumentNullException(nameof(bucketName));

            _bucketName = bucketName;
            _pathPrefix = Config.S3PathPrefix;
            _storage = new S3Factory().GetStorage(new JsonOdsValue());

            _maxFileSizeByte = (Config.S3MaxFileSizeMB * 1024) * 1024;
            _maxConcurrentFileUploads = Config.S3MaxConcurrentFileUploads;

            _stagingFolder = FileHelper.CreateDirectory(Config.S3StagingFolder);
            _transferFolder = FileHelper.CreateDirectory(Config.S3TransferFolder);

            _fileTracker = new ConcurrentDictionary<string, LinkedList<string>>();
            _fileTrackerLock = new ReaderWriterLockSlim();

            _counters = new Counters();

            _started = false;
        }

        public void Start()
        {
            EventManager.LogInfo($"CloudWriter.Start() - Starting");

            if (_cancellationTokenSource?.Token != null)
            {
                _cancellationTokenSource.Cancel();
                _cancellationTokenSource.Dispose();
                _cancellationTokenSource = null;
            }

            _cancellationTokenSource = new CancellationTokenSource();

            ResetCounters();

            _started = true;

            EventManager.LogInfo($"CloudWriter.Start() - Started");
        }

        public void Stop()
        {
            EventManager.LogInfo($"CloudWriter.Start() - Stopping");

            _cancellationTokenSource.Cancel();

            _started = false;

            EventManager.LogInfo($"CloudWriter.Start() - Stopped");
        }

        public void StoreFlashes(IReadOnlyDictionary<string, List<Tuple<Flash, string>>> flashes)
        {
            _cancellationTokenSource?.Token.ThrowIfCancellationRequested();

            if (flashes == null || !flashes.Any()) return;

            EventManager.LogDebug($"CloudWriter.StoreFlashes() - Storing flashes. Count: {flashes.Count}, QuadKeys: {flashes.Keys.Count()}");

            foreach (var quadKey in flashes.Keys)
            {
                _cancellationTokenSource?.Token.ThrowIfCancellationRequested();

                var flashesForQuadKey = flashes[quadKey];

                EventManager.LogDebug($"CloudWriter.StoreFlashes() - Storing flashes for quadKey. QuadKey: {quadKey}, Count: {flashesForQuadKey.Count}");

                if (flashesForQuadKey != null && flashesForQuadKey.Any())
                {
                    EventManager.LogDebug($"CloudWriter.StoreFlashes() - Start writing flashes for quadKey. Flash Count: {flashes.Count}, QuadKey: {quadKey}");
                    StageFlashes(quadKey, flashesForQuadKey);
                    EventManager.LogDebug($"CloudWriter.StoreFlashes() - Finished writing flashes for quadKey. Flash Count: {flashes.Count}, QuadKey: {quadKey}");
                }
            }
            EventManager.LogDebug($"CloudWriter.StoreFlashes() - Finished writing flash data to staging folder. Folder: {_stagingFolder}");
        }

        private void StageFlashes(string quadKey, IReadOnlyCollection<Tuple<Flash, string>> flashes)
        {
            _cancellationTokenSource?.Token.ThrowIfCancellationRequested();

            if (flashes == null || !flashes.Any()) return;

            EventManager.LogDebug($"CloudWriter.StageFlashes() - Staging flashes for quadKey. QuadKey: {quadKey}, Count: {flashes.Count}");

            var firstFlash = flashes.FirstOrDefault();

            if (firstFlash?.Item1 == null || string.IsNullOrWhiteSpace(firstFlash.Item2)) return;

            string filePath;
            var fileStream = default(StreamWriter);

            try
            {
                filePath = GetOutputFileName(quadKey, firstFlash.Item1);
                fileStream = File.AppendText(filePath);

                EventManager.LogDebug($"CloudWriter.StageFlashes() - Start writing flashes. Path: {filePath}, QuadKey: {quadKey}");


                foreach (var flashData in flashes)
                {
                    _cancellationTokenSource?.Token.ThrowIfCancellationRequested();

                    var flash = flashData.Item1;
                    var rawFlash = flashData.Item2;

                    var isFlashAndFileMatch = IsFlashValidForFile(quadKey, flash, filePath);
                    var isFileMaxed = (fileStream.BaseStream.Length >= _maxFileSizeByte);

                    if (!isFlashAndFileMatch || isFileMaxed)
                    {
                        EventManager.LogDebug(isFileMaxed
                                            ? $"CloudWriter.StageFlashes() - Max file size exceeded. Creating new file. File: {filePath} "
                                            : $"CloudWriter.StageFlashes() - Output file does not match flash date. Creating new file. File: {filePath} Flash Timestamp: {flash.TimeStamp}"
                                         );
                        fileStream.Flush();
                        fileStream.Close();
                        fileStream.Dispose();

                        if (isFileMaxed) MoveTrackedFileToTransferFolder(quadKey, filePath);

                        filePath = GetOutputFileName(quadKey, flash, true);
                        fileStream = File.AppendText(filePath);
                        EventManager.LogDebug($"CloudWriter.StageFlashes() - New file created for quadKey. File: {filePath}, QuadKey: {quadKey}");
                    }

                    fileStream.WriteLine(rawFlash);
                    fileStream.Flush();
                }
            }
            finally
            {
                fileStream?.Flush();
                fileStream?.Close();
                fileStream?.Dispose();
            }

            EventManager.LogDebug($"CloudWriter.StageFlashes() - Finished writing flashes. Path: {filePath}, QuadKey: {quadKey}");
        }

        private void MoveTrackedFileToTransferFolder(string quadKey, string filePath)
        {
            _cancellationTokenSource?.Token.ThrowIfCancellationRequested();

            if (string.IsNullOrWhiteSpace(quadKey)) throw new ArgumentNullException(nameof(quadKey));
            if (string.IsNullOrWhiteSpace(filePath)) throw new ArgumentNullException(nameof(filePath));

            EventManager.LogDebug($"CloudWriter.MoveTrackedFileToTransferFolder() - Processing move tracked file request. QuadKey: {quadKey}, File: {filePath}");

            var moveFile = false;
            var readLockHeld = false;
            var writeLockHeld = false;

            try
            {
                var tries = 0;

                while (!(readLockHeld = _fileTrackerLock.TryEnterReadLock(MaxLockWaitMillseconds)) && tries < MaxLockTries)
                    tries++;

                if (readLockHeld)
                {
                    moveFile = _fileTracker.ContainsKey(quadKey) && _fileTracker[quadKey].Contains(filePath);
                    _fileTrackerLock.ExitReadLock();
                    readLockHeld = false;
                }
                else
                {
                    EventManager.LogWarning($"CloudWriter.MoveTrackedFileToTransferFolder() - Failed to obtain read lock. QuadKey: {quadKey}, File: {filePath}");
                }

                if (moveFile)
                {
                    EventManager.LogDebug($"CloudWriter.MoveTrackedFileToTransferFolder() - Moving tracked file to transfer folder. QuadKey: {quadKey}, File: {filePath}");

                    var destinationFile = MoveFileToTransferFolder(new FileInfo(filePath));

                    if (!string.IsNullOrWhiteSpace(destinationFile) && File.Exists(destinationFile))
                    {
                        tries = 0;
                        while (!(writeLockHeld = _fileTrackerLock.TryEnterWriteLock(MaxLockWaitMillseconds)) && tries < MaxLockTries)
                            tries++;

                        if (writeLockHeld)
                        {
                            _fileTracker[quadKey].Remove(filePath);
                            _fileTrackerLock.ExitWriteLock();
                            writeLockHeld = false;
                        }
                        else
                        {
                            EventManager.LogWarning($"CloudWriter.MoveTrackedFileToTransferFolder() - Failed to obtain write lock. QuadKey: {quadKey}, File: {filePath}");
                        }
                    }
                    else
                    {
                        EventManager.LogError($"CloudWriter.MoveTrackedFileToTransferFolder() - Failed to move file to transfer folder. QuadKey: {quadKey}, File: {filePath}, Destination: {destinationFile}");
                    }
                }
                else
                {
                    EventManager.LogWarning($"CloudWriter.MoveTrackedFileToTransferFolder() - File tracker does not contain either the request quadKey or filePath. QuadKey: {quadKey}, File: {filePath}");
                }
            }
            finally
            {
                if (readLockHeld) _fileTrackerLock.ExitReadLock();
                if (writeLockHeld) _fileTrackerLock.ExitWriteLock();
            }
        }

        private string MoveFileToTransferFolder(FileInfo file)
        {
            _cancellationTokenSource?.Token.ThrowIfCancellationRequested();

            if (file == null) throw new ArgumentNullException(nameof(file));

            var destinationFile = Path.Combine(_transferFolder.FullName, Path.GetFileName(file.FullName));

            if (file.Exists)
            {
                EventManager.LogDebug($"CloudWriter.MoveFileToTransferFolder() - Moving file to transfer folder. File: {file.FullName}, Destination: {destinationFile}");
                file.MoveTo(destinationFile);

                if (!File.Exists(destinationFile))
                {
                    EventManager.LogError($"CloudWriter.MoveFileToTransferFolder() - Failed to move file to transfer folder. File: {file.FullName}, Destination: {destinationFile}");
                    destinationFile = string.Empty;
                }

                //if (File.Exists(file.FullName))
                //    EventManager.LogWarning($"CloudWriter.MoveFileToTransferFolder() - Failed to remove source file. File: {file.FullName}, Destination: {destinationFile}");
            }
            else
            {
                EventManager.LogWarning($"CloudWriter.MoveFileToTransferFolder() - Unable to move file to transfer folder as source file does not exist. File: {file.FullName}");
                destinationFile = string.Empty;
            }

            return destinationFile;
        }

        private string GetOutputFileName(string quadKey, Flash flash, bool createNew = false)
        {
            _cancellationTokenSource?.Token.ThrowIfCancellationRequested();

            var path = string.Empty;
            var quadKeyEntryExists = false;

            var readLockHeld = false;
            var writeLockHeld = false;

            var tries = 0;

            try
            {
                while (!(readLockHeld = _fileTrackerLock.TryEnterReadLock(MaxLockWaitMillseconds)) && tries < MaxLockTries)
                    tries++;

                if (readLockHeld)
                {
                    quadKeyEntryExists = _fileTracker.ContainsKey(quadKey);
                    _fileTrackerLock.ExitReadLock();
                    readLockHeld = false;
                }
                else
                {
                    EventManager.LogWarning($"CloudWriter.GetOutputFileName() - Failed to obtain read lock. QuadKey: {quadKey}");
                }

                if (!quadKeyEntryExists)
                {
                    EventManager.LogDebug($"CloudWriter.GetOutputFileName() - Adding quadKey entry to file tracker. QuadKey: {quadKey}");

                    tries = 0;
                    while (!(writeLockHeld = _fileTrackerLock.TryEnterWriteLock(MaxLockWaitMillseconds)) && tries < MaxLockTries)
                        tries++;

                    if (writeLockHeld)
                    {
                        var fileNames = new LinkedList<string>();
                        quadKeyEntryExists = _fileTracker.AddOrUpdate(quadKey, fileNames, (key, existingValue) => fileNames) != null;
                        createNew = true;

                        _fileTrackerLock.ExitWriteLock();
                        writeLockHeld = false;

                        EventManager.LogDebug($"CloudWriter.GetOutputFileName() - quadKey entry added to file tracker. QuadKey: {quadKey}, Added: {quadKeyEntryExists}");
                    }
                    else
                    {
                        EventManager.LogWarning($"CloudWriter.GetOutputFileName() - Failed to obtain write lock. QuadKey: {quadKey}");
                    }
                }

                if (!quadKeyEntryExists)
                {
                    EventManager.LogError($"CloudWriter.GetOutputFileName() - Unable to add quadKey to file tracker. QuadKey: {quadKey}");
                    throw new InvalidOperationException("Failed to add quadKey to file tracker");
                }

                if (createNew)
                {
                    // remove colons from date --- colons can't be used in file names
                    var datestring = flash.TimeStamp.Replace(":", "-");
                    path = Path.Combine(_stagingFolder.FullName, $"{quadKey}-{datestring}.{JsonFileExtension}").Trim();
                    EventManager.LogDebug($"CloudWriter.GetOutputFileName() - Creating new file. QuadKey: {quadKey}, File: {path}");
                    File.Create(path).Close();

                    tries = 0;
                    while (!(writeLockHeld = _fileTrackerLock.TryEnterWriteLock(MaxLockWaitMillseconds)) && tries < MaxLockTries)
                        tries++;

                    if (writeLockHeld)
                    {
                        _fileTracker[quadKey].AddFirst(path);

                        _fileTrackerLock.ExitWriteLock();
                        writeLockHeld = false;
                    }
                    else
                    {
                        EventManager.LogWarning($"CloudWriter.GetOutputFileName() - Failed to obtain second write lock. QuadKey: {quadKey}");
                    }
                }
                else
                {
                    tries = 0;
                    while (!(readLockHeld = _fileTrackerLock.TryEnterReadLock(MaxLockWaitMillseconds)) && tries < MaxLockTries)
                        tries++;

                    if (readLockHeld)
                    {
                        path = _fileTracker[quadKey]?.First?.Value;

                        _fileTrackerLock.ExitReadLock();
                        readLockHeld = false;
                    }
                    else
                    {
                        EventManager.LogWarning($"CloudWriter.GetOutputFileName() - Failed to obtain second read lock. QuadKey: {quadKey}, File: {path}");
                    }

                    if (!IsFlashValidForFile(quadKey, flash, path))
                    {
                        EventManager.LogDebug($"CloudWriter.GetOutputFileName() - Flash date for quadKey does not match file. Calling GetOutputFilePath recursively. QuadKey: {quadKey}, File: {path}, Flash Date: {flash.TimeStamp}");
                        path = GetOutputFileName(quadKey, flash, true);
                    }
                }
            }
            finally 
            {
                if (readLockHeld) _fileTrackerLock.ExitReadLock();
                if (writeLockHeld) _fileTrackerLock.ExitWriteLock();
            }

            return path;
        }

        private bool IsFlashValidForFile(string quadKey, Flash flash, string existingFile)
        {
            var isMatch = false;

            var existingFileInfo = new FileInfo(existingFile);

            try
            {
                var flashDate = DateTime.Parse(flash.TimeStamp);

                //format: quadkey-YYYY-MM-DDTHH-MM-SS.json
                //eample: 31000-2016-04-06T23-59-59.999076000.json
                var existingFileDateInfo = existingFileInfo.Name.Replace($"{quadKey}-", string.Empty)
                                                                .Replace(existingFileInfo.Extension, string.Empty)
                                                                .Substring(0, 10);
                //now: 2016-04-06
                var existingFileDate = DateTime.Parse(existingFileDateInfo);

                isMatch = existingFileDate.Date.Equals(flashDate.Date);
            }
            catch (Exception)
            {
                EventManager.LogError($"CloudWriter.IsFlashValidForFile() - Caught exception while comparing Flash and File date: File: {existingFile}, Flash: ({flash.TimeStamp})");
            }

            return isMatch;
        }

        private string GetCloudPath(string localFile)
        {
            if (string.IsNullOrWhiteSpace(localFile)) throw new ArgumentNullException(nameof(localFile));

            var fileName = Path.GetFileName(localFile);   //quadKey-year-month-dayThour-month-second.json
            if (string.IsNullOrWhiteSpace(fileName)) throw new ArgumentNullException(nameof(localFile));

            // 03223-2016-09-27T21-37-49.json
            // {quadKey}-{datestring}.{JsonFileExtension}
            var fileNameComponents = fileName.Split('-');
            var quadKey = fileNameComponents[0];
            var year = fileNameComponents[1];
            var month = fileNameComponents[2].TrimStart('0'); // remove leading zero
            var cloudPath = $"{quadKey}/{year}/{month}/{fileName}";

            if (!string.IsNullOrWhiteSpace(_pathPrefix)) cloudPath = $"{_pathPrefix}/{cloudPath}";

            return cloudPath;
        }


        #region Cleanup Tasks
        public void FlushToCloud()
        {
            _cancellationTokenSource?.Token.ThrowIfCancellationRequested();

            EventManager.LogInfo(EventId.Process.CloudWriter.Flush, "CloudWriter.FlushToCloud() - Writing files to cloud");

            try
            {
                var tasks = new List<Task>();
                var files = _transferFolder.GetFiles().Where(f => f.Exists && !f.IsReadOnly && f.Length > 0);

                foreach (var fileInfo in files.OrderByDescending(f => f.Length))
                {
                    _cancellationTokenSource?.Token.ThrowIfCancellationRequested();

                    EventManager.LogDebug($"CloudWriter.FlushToCloud() - Start processing transfer file {fileInfo.Name}");

                    var localFile = fileInfo;

                    var t = Task.Run(async () =>
                    {
                        var pathOnS3 = string.Empty;

                        try
                        {
                            pathOnS3 = GetCloudPath(localFile.FullName);

                            EventManager.LogDebug($"CloudWriter.FlushToCloud() - Start uploading file to cloud storage. File: {localFile.FullName}, Cloud Path: {pathOnS3}, Bucket: {_bucketName}");
                            using (var fileStream = new FileStream(localFile.FullName, FileMode.Open, FileAccess.Read, FileShare.Read, 81920, FileOptions.Asynchronous))
                            {
                                await _storage.PutObjectAsync(_bucketName, pathOnS3, fileStream);
                            }

                            EventManager.LogDebug($"CloudWriter.FlushToCloud() - Finished uploading file to cloud storage. File: {localFile.FullName}, Cloud Path: {pathOnS3}, Bucket: {_bucketName}");

                            localFile.Delete();
                            _counters.IncrementCounter(FilesUploadedCounterName, 1);

                            EventManager.LogInfo(EventId.Process.CloudWriter.Flush, $"CloudWriter.FlushToCloud() - File transferred to cloud. File: {localFile.FullName}, Cloud Path: {pathOnS3}, Bucket: {_bucketName}");
                        }
                        catch (Exception ex)
                        {
                            EventManager.LogError(EventId.Process.CloudWriter.Flush, $"CloudWriter.FlushToCloud() - Caught exception while upload file to cloud. File: {localFile.FullName}, Cloud Path: {pathOnS3}, Bucket: {_bucketName}, Error: {ex.Message}", ex);
                        }
                    }, _cancellationTokenSource?.Token ?? CancellationToken.None);

                    tasks.Add(t);

                    if (tasks.Count >= _maxConcurrentFileUploads)
                    {
                        Task.WaitAll(tasks.ToArray<Task>());
                        tasks.Clear();
                    }
                }

                if (tasks.Any())
                {
                    Task.WaitAll(tasks.ToArray<Task>());
                    tasks.Clear();
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventId.Process.CloudWriter.Flush, $"CloudWriter.FlushToCloud() - Caught exception while flush files to cloud. Error: {ex.Message}", ex);
            }
            finally
            {

            }
        }

        public void ProcessTrackedFiles()
        {
            _cancellationTokenSource?.Token.ThrowIfCancellationRequested();

            var writeLockHeld = false;

            try
            {
                var trackedFiles = default(KeyValuePair<string, LinkedList<string>> []);

                if (_fileTracker.Any())
                {
                    var tries = 0;
                    while (!(writeLockHeld = _fileTrackerLock.TryEnterWriteLock(MaxLockWaitMillseconds)) && tries < MaxLockTries)
                        tries++;

                    if (writeLockHeld)
                    {
                        trackedFiles = _fileTracker.ToArray();
                        _fileTracker.Clear();

                        _fileTrackerLock.ExitWriteLock();
                        writeLockHeld = false;
                    }
                    else
                    {
                        EventManager.LogWarning($"CloudWriter.ProcessTrackedFiles() - Failed to obtain write lock. Tracked files will not be processed.");
                    }
                }

                if (trackedFiles != null && trackedFiles.Any())
                {
                    EventManager.LogDebug($"CloudWriter.ProcessTrackedFiles() - Moving tracked files to transfer folder. Tracked File Count: {trackedFiles.LongLength}, Transfer Folder: {_transferFolder.FullName}");
                    foreach (var kvPair in trackedFiles)
                    {
                        var quadKey = kvPair.Key;
                        var fileList = kvPair.Value;

                        EventManager.LogDebug($"CloudWriter.ProcessTrackedFiles() - Processing tracked files for quadKey. QuadKey: {quadKey}, File Count: {fileList.Count}");

                        foreach (var file in fileList)
                        {
                            _cancellationTokenSource?.Token.ThrowIfCancellationRequested();
                            EventManager.LogDebug($"CloudWriter.ProcessTrackedFiles() - Processing tracked files for quadKey. QuadKey: {quadKey}, File Count: {fileList.Count}");
                            MoveFileToTransferFolder(new FileInfo(file));
                        }
                    }
                }
                else
                {
                    EventManager.LogInfo($"CloudWriter.ProcessTrackedFiles() - No tracked files to process. Tracked File Count: {trackedFiles?.LongLength ?? 0}");
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError($"CloudWriter.ProcessTrackedFiles() - Caught exception while processing tracked files. Error: {ex.Message}", ex);
            }
            finally
            {
                if (writeLockHeld) _fileTrackerLock.ExitWriteLock();

            }
        }

        public void ProcessNonTrackedFiles()
        {
            _cancellationTokenSource?.Token.ThrowIfCancellationRequested();

            var writeLockHeld = false;

            try
            {
                var trackedFiles = new List<string>();
                var untrackedFileTimeToLiveHours = Config.S3UntrackedFileTimeToLiveHours;
                var expiryTime = DateTime.UtcNow.AddHours(-Math.Abs(untrackedFileTimeToLiveHours));

                if (_fileTracker.Any())
                {
                    var tries = 0;
                    while (!(writeLockHeld = _fileTrackerLock.TryEnterWriteLock(MaxLockWaitMillseconds)) && tries < MaxLockTries)
                        tries++;

                    if (writeLockHeld)
                    {
                        trackedFiles.AddRange(_fileTracker.ToArray().SelectMany(f => f.Value.AsEnumerable()));

                        _fileTrackerLock.ExitWriteLock();
                        writeLockHeld = false;
                    }
                    else
                    {
                        EventManager.LogWarning($"CloudWriter.ProcessNonTrackedFiles() - Failed to obtain write lock. Tracked files will not be processed.");
                    }
                }

                var files = _stagingFolder.GetFiles().Where(f => f.Exists && f.LastWriteTimeUtc < expiryTime && !trackedFiles.Contains(f.FullName));
                foreach (var file in files)
                {
                    _cancellationTokenSource?.Token.ThrowIfCancellationRequested();
                    MoveFileToTransferFolder(file);
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError($"CloudWriter.ProcessNonTrackedFiles() - Caught exception while processing non-tracked files. Error: {ex.Message}", ex);
            }
            finally
            {
                if (writeLockHeld) _fileTrackerLock.ExitWriteLock();
            }
        }

        public void ResetCounters()
        {
            _cancellationTokenSource?.Token.ThrowIfCancellationRequested();
            EventManager.LogInfo($"CloudWriter.ResetCounters() - Resetting counters.");
            _counters.ResetCounters();
        }
        #endregion Cleanup Tasks



        #region Counters

        public ulong GetFilesUploadedToCloudCount()
        {
            return _counters.GetCounterValue(FilesUploadedCounterName);
        }
        #endregion Counters

        #region IDisposable
        public void Dispose()
        {
            if (_disposed) return;

            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected void Dispose(bool disposing)
        {
            if (_disposed) throw new ObjectDisposedException(null);

            try
            {
                if (disposing)
                {
                    if (_started) Stop();

                    _storage?.Dispose();
                    _fileTrackerLock?.Dispose();
                    _counters?.Dispose();
                    _cancellationTokenSource?.Dispose();
                }
            }
            finally
            {
                _disposed = true;
            }
        }
        #endregion IDisposable
    }
}
