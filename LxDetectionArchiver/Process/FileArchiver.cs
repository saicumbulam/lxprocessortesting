﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading;
using LxCommon;
using Newtonsoft.Json; 
using Aws.Core.Utilities; 
using LxCommon.Models;
using LxCommon.Utilities; 
using LxDetectionArchiver.Common;

namespace LxDetectionArchiver.Process
{
    public class FileArchiver
    {
        private Timer _importTimer;
        private DirectoryInfo _pickupFolder;
        private DirectoryInfo _archiveFolder;
        private string _filePattern;
        private int _importIntervalSeconds;
        private const string BadDatePath =  "Unknown\\";
        private DateTime _lastFileArchived;

        public FileArchiver(string pickupFolder, int importIntervalSeconds, string archiveFolder, string filePattern)
        {
            _pickupFolder = new DirectoryInfo(pickupFolder);
            _archiveFolder = null;
            if (archiveFolder != null)
            {
                _archiveFolder = FileHelper.CreateDirectory(archiveFolder);
            }
            _filePattern = filePattern;
            _importIntervalSeconds = importIntervalSeconds;
            _lastFileArchived = DateTime.UtcNow;
        }

        public void Start()
        {
            _importTimer = new Timer(Process, null, TimeSpan.FromSeconds(0), TimeSpan.FromMilliseconds(-1));
        }

        public void Stop()
        {
            if (_importTimer != null)
            {
                _importTimer.Dispose();
            }
        }

        private void Process(object state)
        {
            FileInfo fileInfo = GetOldestFile(_pickupFolder, _filePattern);
            if (fileInfo != null)
            {
                ArchiveFile(fileInfo, _archiveFolder, _filePattern);
            }

            if (_importTimer != null)
            {
                _importTimer.Change(TimeSpan.FromSeconds(_importIntervalSeconds), TimeSpan.FromMilliseconds(-1));
            }
        }

        private FileInfo GetOldestFile(DirectoryInfo pickupFolder, string filePattern)
        {
            FileInfo[] fi;
            FileInfo oldestFile = null;

            try
            {
                fi = pickupFolder.GetFiles(filePattern, SearchOption.AllDirectories);

                if (fi.Length != 0)
                {
                    oldestFile = (from f in fi orderby f.CreationTime ascending select f).First();
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventId.Process.FileDbImporter.FindFiles, string.Format("Unable to find oldest file in directory: {0}", pickupFolder.FullName), ex);
            }

            return oldestFile;
        }

        private void ArchiveFile(FileInfo fileInfo, DirectoryInfo archiveFolder, string filePattern)
        {
            if (archiveFolder != null)
            {
                DateTime dt = FileHelper.GetFileTimeFromName(fileInfo.Name, filePattern);
                string path = BadDatePath;
                if (dt != DateTime.MinValue)
                {
                    path = string.Format("{0:D4}\\{1:D2}\\{2:D2}\\{3:D2}\\", dt.Year, dt.Month, dt.Day, dt.Hour);
                }

                path = Path.Combine(archiveFolder.FullName, path);

                FileHelper.CreateDirectory(path);

                path = Path.Combine(path, fileInfo.Name);

                try
                {
                    if (File.Exists(path))
                    {
                        string appendText = File.ReadAllText(fileInfo.FullName);
                        File.AppendAllText(path, appendText);
                        fileInfo.Delete();
                    }
                    else
                    {
                        fileInfo.MoveTo(path);
                    }
                    
                    _lastFileArchived = dt;
                }
                catch (Exception ex)
                {
                    EventManager.LogError(EventId.Process.FileDbImporter.MoveFile, string.Format("Unable to move file from: {0} to: {1}", fileInfo.FullName, path), ex);
                }
            }
            else
            {
                try
                {
                    fileInfo.Delete();
                    _lastFileArchived = FileHelper.GetFileTimeFromName(fileInfo.FullName, filePattern);
                }
                catch (Exception ex)
                {
                    EventManager.LogError(EventId.Process.FileDbImporter.DeleteFile, string.Format("Unable to delete file: {0}", fileInfo.FullName), ex);
                }
            }
        }

        public DateTime LastFileArchived
        {
            get { return _lastFileArchived; }
        }

        public int GetFileCount()
        {
            var count = 0;

            if (!string.IsNullOrWhiteSpace(_pickupFolder?.FullName))
            {
                try
                {
                    if (_pickupFolder.Exists)
                    {
                        var fi = _pickupFolder.GetFiles("*", SearchOption.TopDirectoryOnly);
                        count = fi.Length;
                    }
                }
                catch (Exception ex)
                {
                    EventManager.LogError(EventId.Process.MonitoringHandler.GetFileCount, "Unable to get file count", ex);
                }
            }

            return count;
        }
    }
}
