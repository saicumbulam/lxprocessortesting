﻿using System;
using System.IO;
using System.Linq;
using System.Threading;

using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;

using Aws.Core.Utilities;

using LxCommon.Utilities;

using LxDetectionArchiver.Common;

namespace LxDetectionArchiver.Process
{
    public class AwsS3FileArchiver
    {
        private readonly DirectoryInfo _pickupPath;
        private readonly string _bucketName;
        private readonly string _accessKey;
        private readonly string _secretKey;
        private readonly int _importIntervalSeconds;
        private readonly string _filePattern;
        private readonly TransferUtilityConfig _transferUtilityConfig;
        private readonly RegionEndpoint _regionEndpoint;

        private Timer _importTimer;
        private const ushort EventManagerOffset = EventId.Process.AwsS3FileArchiver;

        private DateTime _lastFileArchived;

        public AwsS3FileArchiver(string pickupPath, string bucketName, string filePattern, int importIntervalSeconds, int numberUploadThreads, string accessKey, string secretKey, string region)
        {
            Guard.ArgumentIsNotNull(pickupPath, "pickupPath");
            _pickupPath = new DirectoryInfo(pickupPath);

            Guard.ArgumentIsNotNull(bucketName, "bucketName");
            _bucketName = bucketName;

            Guard.ArgumentIsNotNull(filePattern, "filePattern");
            _filePattern = filePattern;

            Guard.ArgumentIsInRange(importIntervalSeconds, "importIntervalSeconds", 1, 60);
            _importIntervalSeconds = importIntervalSeconds;

            Guard.ArgumentIsInRange(numberUploadThreads, "numberUploadThreads", 1, 50);
             _transferUtilityConfig = new TransferUtilityConfig()
            {
                ConcurrentServiceRequests = numberUploadThreads,
            };

            Guard.ArgumentIsNotNull(accessKey, "accessKey");
            _accessKey = accessKey;

            Guard.ArgumentIsNotNull(secretKey, "secretKey");
            _secretKey = secretKey;

            _regionEndpoint = RegionEndpoint.GetBySystemName(region);
            Guard.ArgumentIsNotNull(_regionEndpoint, "region");

            if (!CreateS3Bucket(bucketName, accessKey, secretKey, _regionEndpoint, S3Region.US, S3CannedACL.Private))
            {
                throw new ArgumentException("unable to create s3 bucket");
            }

            _lastFileArchived = DateTime.UtcNow;
        }

        private bool CreateS3Bucket(string bucketName, string accessKey, string secretKey, RegionEndpoint regionEndpoint, S3Region s3region, S3CannedACL acl)
        {
            AmazonS3Client client = new AmazonS3Client(accessKey, secretKey, regionEndpoint);
            PutBucketRequest request = new PutBucketRequest
            {
                BucketName = bucketName,
                BucketRegion = s3region,
                CannedACL = acl,
            };

            bool success = false;

            // Issue call
            try
            {
                PutBucketResponse response = client.PutBucket(request);
                success = (response.HttpStatusCode == System.Net.HttpStatusCode.OK);
            }
            catch (AmazonS3Exception awsEx)
            {
                EventManager.LogError(EventManagerOffset + 1,
                                      string.Format(
                                          "Error creating bucket {0} in AWS. Aws Error Code = {1}, Aws Request Id = {2}, Aws Error Message = {3}",
                                          bucketName, awsEx.ErrorCode, awsEx.RequestId, awsEx.Message), awsEx);
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventManagerOffset + 10, string.Format("Error creating bucket {0} in AWS.", bucketName), ex);
            }

            return success;
        }

        public void Start()
        {
            _importTimer = new Timer(Process, null, TimeSpan.FromSeconds(0), TimeSpan.FromMilliseconds(-1));
        }

        public void Stop()
        {
            if (_importTimer != null)
            {
                _importTimer.Dispose();
            }
        }

        private void Process(object state)
        {
            IOrderedEnumerable<FileInfo> files = GetFilesOldestFirst(_pickupPath, _filePattern);
            if (files != null)
            {
                foreach (FileInfo file in files)
                {
                    string s3filepath = Path.Combine(Config.S3PickupFolder, Path.GetFileName(file.FullName));
                    File.Copy(file.FullName, s3filepath);

                    MoveFileToAwsS3(file, _filePattern, _bucketName, _transferUtilityConfig, _accessKey, _secretKey, _regionEndpoint);    
                }
            }
           

            if (_importTimer != null)
            {
                _importTimer.Change(TimeSpan.FromSeconds(_importIntervalSeconds), TimeSpan.FromMilliseconds(-1));
            }
        }

        private IOrderedEnumerable<FileInfo> GetFilesOldestFirst(DirectoryInfo pickupFolder, string filePattern)
        {
            FileInfo[] fi;
            IOrderedEnumerable<FileInfo> files = null;
            try
            {
                fi = pickupFolder.GetFiles(filePattern, SearchOption.AllDirectories);

                if (fi.Length != 0)
                {
                    files = (from f in fi orderby f.CreationTime ascending select f);
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventManagerOffset, string.Format("Unable to find oldest file in directory: {0}", pickupFolder.FullName), ex);
            }

            return files;
        }

        private void MoveFileToAwsS3(FileInfo fileInfo, string filePattern, string bucketName, TransferUtilityConfig transferUtilityConfig, string accessKey, string secretKey, RegionEndpoint region)
        {
            DateTime fileTime = FileHelper.GetFileTimeFromName(fileInfo.Name, filePattern);
            string s3Path = BuildPartialS3Path(fileTime);
            s3Path = bucketName + s3Path;
            try
            {
                //EventManager.LogInfo(string.Format("Archiving file {0} to {1} in AWS.", fileInfo.FullName, s3Path));

                using (TransferUtility fileTransferUtility = new TransferUtility(accessKey, secretKey, region, transferUtilityConfig))
                {
                    fileTransferUtility.Upload(fileInfo.FullName, s3Path);
                }
                DeleteFile(fileInfo);
                _lastFileArchived = fileTime;
            }
            catch (AmazonS3Exception awsEx)
            {
                EventManager.LogError(EventManagerOffset + 1,
                                      string.Format(
                                          "Error archiving file {0} to {1} in AWS. Aws Error Code = {2}, Aws Request Id = {3}, Aws Error Message = {4}",
                                          fileInfo.FullName, s3Path, awsEx.ErrorCode, awsEx.RequestId, awsEx.Message), awsEx);
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventManagerOffset + 10, string.Format("Error archiving file {0} to {1} in AWS.", fileInfo.FullName, s3Path), ex);
            }
        }

        private string BuildPartialS3Path(DateTime fileTime)
        {
            return fileTime.ToString("/yyyy/MM/dd/HH");
        }

        private void DeleteFile(FileInfo fileInfo)
        {
            try
            {
                //EventManager.LogInfo(EventManagerOffset + 3, string.Format("AwsS3FileArchiver archived file: {0}", fileInfo.FullName));
                fileInfo.Delete();
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventManagerOffset + 4, string.Format("Error deleting file: {0}", fileInfo.FullName), ex);
            }
        }

        public DateTime LastFileArchived
        {
            get { return _lastFileArchived; }
        }

        public int GetFileCount()
        {
            var count = 0;

            if (!string.IsNullOrWhiteSpace(_pickupPath?.FullName))
            {
                try
                {
                    if (_pickupPath.Exists)
                    {
                        var fi = _pickupPath.GetFiles("*", SearchOption.TopDirectoryOnly);
                        count = fi.Length;
                    }
                }
                catch (Exception ex)
                {
                    EventManager.LogError(EventId.Process.MonitoringHandler.GetFileCount, "Unable to get file count", ex);
                }
            }

            return count;
        }
    }
}
