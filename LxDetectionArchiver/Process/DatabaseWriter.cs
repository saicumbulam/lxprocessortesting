﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using LxCommon.TcpCommunication;
using Microsoft.SqlServer.Types;

using Aws.Core.Utilities;

using LxCommon;
using LxCommon.DataStructures;
using LxCommon.Models;
using LxCommon.Utilities;

using LxDetectionArchiver.Common;

namespace LxDetectionArchiver.Process
{
    public class DatabaseWriter : ParallelQueueProcessor<Tuple<Flash, Portion>>
    {
        private string _etlConn;
        private string _edwConn;
        private FlashDataLogger _failedFlashLogger;
        private FlashDataLogger _failedPortionLogger;
        private int _maxQueueSize;
        private DateTime _lastFlash;
        private bool _isTlnSystem;
        private readonly RateCalculator _insertRate = new RateCalculator(5);
        public DatabaseWriter(string etlConn, string edwConn, FlashDataLogger failedFlashLogger, FlashDataLogger failedPortionLogger, int maxQueueSize, ManualResetEventSlim stopEvent, 
            int maxActiveWorkItems, bool isTlnSystem) : 
            base(stopEvent, maxActiveWorkItems)
        {
            EventManager.LogInfo("Database Writer Initialized");
            _etlConn = etlConn;
            _edwConn = edwConn;
            _failedFlashLogger = failedFlashLogger;
            _failedPortionLogger = failedPortionLogger;
            _maxQueueSize = maxQueueSize;
            _isTlnSystem = isTlnSystem;
            _lastFlash = DateTime.UtcNow;
        }

        /// <summary>
        /// Adds a flash/portions to the archiving queue and also checks the size of the queue and dumps excess flashes. Use this method instead of Add
        /// </summary>
        /// <param name="fp">Flash/Portion to be added the archive queue</param>
        public override void Add(Tuple<Flash, Portion> fp)
        {
            if (_queue.Count > _maxQueueSize)
            {
                EventManager.LogWarning(EventId.Process.DbWriterProcessor.QueueToLarge, "DatabaseWriter queue greater than max size, dumping flashes from queue");
                while (_queue.Count > _maxQueueSize)
                {
                    Tuple<Flash, Portion> fpLog;
                    if (_queue.TryDequeue(out fpLog))
                    {
                        if (fpLog.Item1 != null && _failedFlashLogger != null)
                        {
                            _failedFlashLogger.WriteFlash(fpLog.Item1, LtgTimeUtils.GetUtcDateTimeFromString(fpLog.Item1.TimeStamp));
                        }
                        else if (fp.Item2 != null && fp.Item2.FlashId.HasValue && _failedPortionLogger != null)
                        {
                            _failedPortionLogger.WritePortion(fpLog.Item2, fp.Item2.FlashId.Value, LtgTimeUtils.GetUtcDateTimeFromString(fp.Item2.TimeStamp));
                        }
                    }
                }
            }

            base.Add(fp);
        }

        protected override void DoWork(Tuple<Flash, Portion> fp)
        {
            //EventManager.LogInfo("DoWork called");
            if (fp.Item1 != null)
            {
                //if we already have an id this means just the aggregation failed
                if (fp.Item1.Id.HasValue)
                {
                    //EventManager.LogInfo("InsertFlashAggregateData called");
                    InsertFlashAggregateData(fp.Item1);
                }
                else
                {
                    //EventManager.LogInfo("InsertFlash called");
                    InsertFlash(fp.Item1);
                }
            }
            else if (fp.Item2 != null && fp.Item2.FlashId.HasValue)
            {
                //EventManager.LogInfo("InsertPortions called");
                InsertPortions(fp.Item2.FlashId.Value, new List<Portion> { fp.Item2 });
            }
        }

        private void InsertFlash(Flash flash)
        {
            Guid flashId = Guid.NewGuid();
            using (SqlConnection conn = new SqlConnection(_etlConn))
            {
                using (SqlCommand cmd = new SqlCommand("InsertStageFlash_pr", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    DateTime ft = CreateFlashParameters(flashId, flash, cmd);

                    try
                    {
                        conn.Open();
                        if (_isTlnSystem)
                        {
                            flash.Id = (long)cmd.ExecuteScalar(); 
                            InsertFlashAggregateData(flash);
                        }
                        else
                        {
                            cmd.ExecuteNonQuery();
                        }
                        conn.Close();
                        InsertPortions(flashId, flash.Portions);
                        _lastFlash = ft;
                        _insertRate.AddSample(DateTime.UtcNow);
                    }
                    catch (Exception ex)
                    {
                        EventManager.LogError(EventId.Process.DbWriterProcessor.InsertFlash, "Error inserting flash", ex);

                        if (_failedFlashLogger != null)
                        {
                            _failedFlashLogger.WriteFlash(flash, LtgTimeUtils.GetUtcDateTimeFromString(flash.TimeStamp));
                        }
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }
        }

        private DateTime CreateFlashParameters(Guid flashId, Flash flash, SqlCommand cmd)
        {
            DateTime ft = LtgTimeUtils.GetUtcDateTimeFromString(flash.TimeStamp);

            cmd.Parameters.AddWithValue("@FlashGUID", flashId);
            cmd.Parameters.AddWithValue("@LightningTime", ft);
            cmd.Parameters.AddWithValue("@LightningTimeString", flash.TimeStamp);
            cmd.Parameters.AddWithValue("@Latitude", flash.Latitude);
            cmd.Parameters.AddWithValue("@Longitude", flash.Longitude);
            cmd.Parameters.AddWithValue("@Height", flash.Height);
            cmd.Parameters.AddWithValue("@StrokeType", LtgConstants.ConvertWwllnTypeToTlnType(flash.Type));
            cmd.Parameters.AddWithValue("@Amplitude", flash.Amplitude);
            //for the stroke solution we are stuffing in our extended data
            cmd.Parameters.AddWithValue("@StrokeSolution", flash.GetMetaDataDbString());
            cmd.Parameters.AddWithValue("@Confidence", flash.Confidence);

            cmd.Parameters.Add(new SqlParameter("@GeoPoint", CreatePoint(flash.Latitude, flash.Longitude)) { UdtTypeName = "Geography" });

            return ft;
        }

        private void InsertPortions(Guid flashId, List<Portion> portions)
        {
            if (portions != null && portions.Count > 0)
            {
                using (SqlConnection conn = new SqlConnection(_etlConn))
                {
                    foreach (Portion portion in portions)
                    {
                        using (SqlCommand cmd = new SqlCommand("InsertStageFlashPortion_pr", conn))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            CreatePortionParameters(flashId, portion, cmd);
                            try
                            {
                                conn.Open();
                                cmd.ExecuteNonQuery();
                                _insertRate.AddSample(DateTime.UtcNow);
                            }
                            catch (Exception ex)
                            {
                                EventManager.LogError(EventId.Process.DbWriterProcessor.InsertPortions, "Error inserting portion", ex);
                                if (_failedPortionLogger != null)
                                {
                                    _failedPortionLogger.WritePortion(portion, flashId, LtgTimeUtils.GetUtcDateTimeFromString(portion.TimeStamp));
                                }
                            }
                            finally
                            {
                                conn.Close();
                            }
                        }
                    }
                }
            }
        }

        private void CreatePortionParameters(Guid flashId, Portion portion, SqlCommand cmd)
        {
            cmd.Parameters.AddWithValue("@FlashGUID", flashId);
            cmd.Parameters.AddWithValue("@LightningTime", LtgTimeUtils.GetUtcDateTimeFromString(portion.TimeStamp));
            cmd.Parameters.AddWithValue("@LightningTimeString", portion.TimeStamp);
            cmd.Parameters.AddWithValue("@Latitude", portion.Latitude);
            cmd.Parameters.AddWithValue("@Longitude", portion.Longitude);
            cmd.Parameters.AddWithValue("@Height", portion.Height);
            cmd.Parameters.AddWithValue("@StrokeType", LtgConstants.ConvertWwllnTypeToTlnType(portion.Type));
            cmd.Parameters.AddWithValue("@Amplitude", portion.Amplitude);
            //for the stroke solution we are stuffing in our extended data
            cmd.Parameters.AddWithValue("@StrokeSolution", portion.GetMetaDataDbString());
            cmd.Parameters.AddWithValue("@Confidence", portion.Confidence);

            cmd.Parameters.Add(new SqlParameter("@GeoPoint", CreatePoint(portion.Latitude, portion.Longitude)) { UdtTypeName = "Geography" });
        }

        public const int Wgs84SpatialRefId = 4326;
        private static SqlGeography CreatePoint(double lat, double lon)
        {
            SqlGeography geo = null;
            try
            {
                SqlGeographyBuilder b = new SqlGeographyBuilder();
                b.SetSrid(Wgs84SpatialRefId);
                b.BeginGeography(OpenGisGeographyType.Point);
                b.BeginFigure(lat, lon);
                b.EndFigure();
                b.EndGeography();
                geo = b.ConstructedGeography;
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventId.Process.DbWriterProcessor.CreatePoint, "Error creating SqlGeography type", ex);
            }
            return geo;
        }

        private void InsertFlashAggregateData(Flash f)
        {
            if (f.Id.HasValue && f.Portions != null && f.Portions.Count > 0)
            {
                using (SqlConnection conn = new SqlConnection(_edwConn))
                {
                    using (SqlCommand cmd = new SqlCommand("InsertFactFlashAggrts_pr", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@FlashID", f.Id.Value);
                        cmd.Parameters.AddWithValue("@NbrOfPortions", f.Portions.Count);
                        cmd.Parameters.AddWithValue("@NbrOfSensors", f.NumberSensors);

                        //EventManager.LogInfo(EventId.Process.DbWriterProcessor.InsertMetaData, string.Format("flash: {0} multiplicity: {1} stationCount: {2}", flashId, portions.Count, GetUniqueStationCount(portions)));

                        try
                        {
                            conn.Open();
                            cmd.ExecuteNonQuery();
                            _insertRate.AddSample(DateTime.UtcNow);
                        }
                        catch (Exception ex)
                        {
                            EventManager.LogError(EventId.Process.DbWriterProcessor.InsertFlashAggregateData, "Error inserting flash meta data", ex);

                            if (_failedFlashLogger != null)
                            {
                                _failedFlashLogger.WriteFlash(f, LtgTimeUtils.GetUtcDateTimeFromString(f.TimeStamp));
                            }
                        }
                        finally
                        {
                            conn.Close();
                        }
                    }
                }
            }
        }

        protected override void Shutdown()
        {
            if (_queue.Count > 0 && _failedFlashLogger != null)
            {
                EventManager.LogWarning(EventId.Process.DbWriterProcessor.Shutdown, "Shutdown initiated, writing queued entries to file");

                Tuple<Flash, Portion> fpLog;
                if (_queue.TryDequeue(out fpLog))
                {
                    if (fpLog.Item1 != null && _failedFlashLogger != null)
                        {
                            _failedFlashLogger.WriteFlash(fpLog.Item1, LtgTimeUtils.GetUtcDateTimeFromString(fpLog.Item1.TimeStamp));
                        }
                        else if (fpLog.Item2 != null && fpLog.Item2.FlashId.HasValue && _failedPortionLogger != null)
                        {
                            _failedPortionLogger.WritePortion(fpLog.Item2, fpLog.Item2.FlashId.Value, LtgTimeUtils.GetUtcDateTimeFromString(fpLog.Item2.TimeStamp));
                        }
                }
            }
        }

        public DateTime LastFlash
        {
            get { return _lastFlash; }
        }

        public double GetInsertRatePerMinute(DateTime now)
        {
            return _insertRate.GetRatePerSecond(now) * 60;
        }

        public int GetFileCount()
        {
            return _failedFlashLogger.GetFileCount() + _failedPortionLogger.GetFileCount();
        }
    }
}
