﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using LxCommon.Models;
using LxCommon.Utilities;
using LxDetectionArchiver.Common;
using Newtonsoft.Json;

namespace LxDetectionArchiver.Process
{
    public class FileProcessor
    {
        private readonly Counters _counters;
        private readonly CancellationToken _cancellationToken;

        private const string FlashesProcessedCounterName = "FlashesProcessed";
        private const string FlashesValidCounterName = "FlashesValid";
        private const string FlashesInvalidCounterName = "FlashesInvalid";

        public FileProcessor(CancellationToken cancellationToken)
        {
            _cancellationToken = cancellationToken;

            _counters = new Counters();

            _counters.ResetCounters();

            ScheduledTask.Create(DateTime.UtcNow.AddDays(1).Date, _counters.ResetCounters, _cancellationToken);
        }

        public Dictionary<string, List<Tuple<Flash, string>>> GetVaidFlashes(FileInfo file)
        {
            var lines = new List<string>();
            var validFlashes = new Dictionary<string, List<Tuple<Flash, string>>>();
            var invalidFlashes = new List<string>();

            try
            {
                if (file.Exists && file.Length > 0)
                {
                    using (var fileStream = new FileStream(file.FullName, FileMode.Open, FileAccess.Read, FileShare.None))
                    {
                        fileStream.Seek(0, SeekOrigin.Begin);

                        lines.AddRange(fileStream.Lines(_cancellationToken));

                        fileStream.Close();
                    }
                }

                if (lines.Any())
                {
                    foreach (var line in lines)
                    {
                        _cancellationToken.ThrowIfCancellationRequested();
                        var flash = default(Flash);
                        var quadKey = string.Empty;

                        try { flash = JsonConvert.DeserializeObject<Flash>(line); }
                        catch (Exception) { /* Ignore xception */ }

                        if (flash != null)
                            quadKey = TileSystem.LatLongToQuadKey(flash.Latitude, flash.Longitude, 5);

                        if (!string.IsNullOrWhiteSpace(quadKey))
                        {
                            if (!validFlashes.ContainsKey(quadKey))
                                validFlashes.Add(quadKey, new List<Tuple<Flash, string>>());

                            validFlashes[quadKey].Add(new Tuple<Flash, string>(flash, line));
                        }
                        else
                        {
                            invalidFlashes.Add(line);
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (validFlashes.Any()) _counters.IncrementCounter(FlashesValidCounterName, validFlashes.Count);
                if (invalidFlashes.Any()) _counters.IncrementCounter(FlashesInvalidCounterName, invalidFlashes.Count);
                _counters.IncrementCounter(FlashesProcessedCounterName, invalidFlashes.Count + validFlashes.Count);
            }

            return validFlashes;
        }

        public ulong GetFlashesProcessedCount()
        {
            return _counters.GetCounterValue(FlashesProcessedCounterName);
        }

        public ulong GetFlashesValidCount()
        {
            return _counters.GetCounterValue(FlashesValidCounterName);
        }

        public ulong GetFlashesInvalidCount()
        {
            return _counters.GetCounterValue(FlashesInvalidCounterName);
        }
    }
}
