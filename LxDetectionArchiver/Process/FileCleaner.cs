﻿using System;
using System.IO;
using System.Threading;
using Aws.Core.Utilities;
using LxCommon.Utilities;
using LxDetectionArchiver.Common;

namespace LxDetectionArchiver.Process
{
    public class FileCleaner
    {
        private string _archiveFolder;
        private int _cleanupIntervalMinutes;
        private string _filePattern;
        private int _archiveTimeToLiveHours;
        private Timer _cleanupTimer;
        private DateTime _lastRun;

        public FileCleaner(string archiveFolder, string filePattern, int cleanupIntervalMinutes, int archiveTimeToLiveHours)
        {
            _archiveFolder = archiveFolder;
            _cleanupIntervalMinutes = cleanupIntervalMinutes;
            _filePattern = filePattern;
            _archiveTimeToLiveHours = archiveTimeToLiveHours;
            _lastRun = DateTime.UtcNow;
        }

        public void Start()
        {
            _cleanupTimer = new Timer(Process, null, TimeSpan.FromSeconds(30), TimeSpan.FromMilliseconds(-1));
        }

        public void Stop()
        {
            if (_cleanupTimer != null)
            {
                _cleanupTimer.Dispose();
            }
        }

        private void Process(object state)
        {

            DirectoryInfo root = null;
            DateTime utcNow = DateTime.UtcNow;
            EventManager.LogInfo(EventId.Process.FileCleaner.Start, string.Format("File cleaner started for {0} will delete files older than {1} hours", _archiveFolder, _archiveTimeToLiveHours));
            try
            {
                root = new DirectoryInfo(_archiveFolder);
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventId.Process.FileCleaner.FindDirectories, string.Format("Unable to access directory: {0}", _archiveFolder), ex);
            }

            if (root != null && root.Exists)
            {
                CrawlDirectory(root, _filePattern, _archiveTimeToLiveHours, utcNow);
            }

            if (_cleanupTimer != null)
            {
                _cleanupTimer.Change(TimeSpan.FromMinutes(_cleanupIntervalMinutes), TimeSpan.FromMilliseconds(-1));
            }
            
            EventManager.LogInfo(EventId.Process.FileCleaner.Finished, "File cleaner finished");
            _lastRun = DateTime.UtcNow;
        }

        private void CrawlDirectory(DirectoryInfo root, string filePattern, int archiveTimeToLiveHours, DateTime utcNow)
        {
            if (root.Exists)
            {
                FileInfo[] fi = null;
                try
                {
                    fi = root.GetFiles(filePattern, SearchOption.TopDirectoryOnly);
                }
                catch (Exception ex)
                {
                    EventManager.LogError(EventId.Process.FileCleaner.FindFiles, string.Format("Unable to find files in directory: {0}", root.FullName), ex);
                }

                if (fi != null && fi.Length > 0)
                {
                    ProcessFiles(fi, filePattern, archiveTimeToLiveHours, utcNow);
                }

                DirectoryInfo[] subFolders = null;
                try
                {
                    subFolders = root.GetDirectories();
                }
                catch (Exception ex)
                {
                    EventManager.LogError(EventId.Process.FileCleaner.FindFiles, string.Format("Unable to find folders in directory: {0}", root.FullName), ex);
                }

                if (subFolders != null && subFolders.Length > 0)
                {
                    foreach (DirectoryInfo directoryInfo in subFolders)
                    {
                        CrawlDirectory(directoryInfo, filePattern, archiveTimeToLiveHours, utcNow);

                        try
                        {
                            FileSystemInfo[] infos = directoryInfo.GetFileSystemInfos();
                            if (infos.Length == 0)
                            {
                                directoryInfo.Delete();
                            }
                        }
                        catch (Exception ex)
                        {
                            EventManager.LogError(EventId.Process.FileCleaner.DeleteDirectory, string.Format("Unable to delete directory: {0}", directoryInfo.FullName), ex);
                        }
                    }
                }
            }   
        }

        private void ProcessFiles(FileInfo[] fi, string filePattern, int archiveTimeToLiveHours, DateTime utcNow)
        {
            foreach (FileInfo fileInfo in fi)
            {
                DateTime dt = FileHelper.GetFileTimeFromName(fileInfo.Name, filePattern);
                if (dt != DateTime.MinValue)
                {
                    TimeSpan ts = utcNow - dt;
                    if (ts.TotalHours > archiveTimeToLiveHours)
                    {
                        try
                        {
                            fileInfo.Delete();
                        }
                        catch (Exception ex)
                        {
                            EventManager.LogError(EventId.Process.FileCleaner.ProcessFiles, string.Format("Unable to delete file: {0}", fileInfo.FullName), ex);
                        }
                    }
                }
            }
        }

        public DateTime LastRun
        {
            get { return _lastRun; }
        }
    }
}
