﻿using System;
using System.IO;

namespace LxDetectionArchiver.Process
{
    public class FileReadyEventArgs : EventArgs
    {
        private readonly FileInfo _fileInfo;

        public FileReadyEventArgs(FileInfo file)
        {
            if (file == null) throw new ArgumentNullException(nameof(file));
            if (!file.Exists) throw new FileNotFoundException("File does not exist", file.FullName);

            _fileInfo = file;
        }

        public FileReadyEventArgs(string directory, string name)
        {
            _fileInfo = new FileInfo(Path.Combine(directory, name));
        }

        /// <summary>
        /// Gets the name of the file.
        /// </summary>
        /// 
        /// <returns>
        /// The name of the file.
        /// </returns>
        /// <filterpriority>1</filterpriority>
        public string Name
        {
            get { return _fileInfo.Name; }
        }

        public string DirectoryName
        {
            get { return _fileInfo.DirectoryName; }
        }

        /// <summary>
        /// Represents the fully qualified path of the directory or file.
        /// </summary>
        /// <exception cref="T:System.IO.PathTooLongException">The fully qualified path is 260 or more characters.</exception>
        public string FullName
        {
            get { return _fileInfo.FullName; }
        }

        public FileInfo File
        {
            get { return _fileInfo; }
        }

        public DirectoryInfo Directory
        {
            get { return _fileInfo.Directory; }
        }
    }
}
