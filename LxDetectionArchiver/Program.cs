﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using Aws.Core.Utilities;
using LxDetectionArchiver.Common;
using LxDetectionArchiver.Interfaces;
using LxDetectionArchiver.Process;

namespace LxDetectionArchiver
{
    public class Program
    {
        private static Program _program;

        private static void Main()
        {
            var servicesToRun = new ServiceBase[] 
            { 
                new WinService() 
            };
            ServiceBase.Run(servicesToRun);
        }

        public static void Start()
        {
            EventManager.Init();

            var currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += OnUnhandledException;

            _program = new Program();

            _program.Begin();
        }

        public static void Stop()
        {
            _program?.End();

            EventManager.ShutDown();
        }

        private static void OnUnhandledException(object sender, UnhandledExceptionEventArgs args)
        {
            var ex = (Exception)args.ExceptionObject;
            EventManager.LogError(EventId.Program.Global, "Global unhandled exception", ex);
            Stop();
        }


        private readonly List<IRunnable> _runnables;

        public Program()
        {
            var dbArchiver = new DbArchiver();
            var cloudArchiver = new CloudArchiver();
            var monitoringHandler = new MonitoringHandler(new List<IStatusProvider>
                {
                    dbArchiver,
                    cloudArchiver
                }
                , Config.DatafeedStatusUrl
                , Config.SystemType
            );

            _runnables = new List<IRunnable>()
            {
                monitoringHandler,
                dbArchiver,
                cloudArchiver
            };
        }

        private void Begin()
        {
            try
            {
                foreach (var runnable in _runnables)
                    runnable.Start();
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventId.Program.Global, $"Program.Begin() - Caught exception starting runnables. Count: {_runnables.Count}, Error: {ex.Message}", ex);
            }
        }

        private void End()
        {
            for(var i = _runnables.Count-1; i >= 0; i--)
                _runnables[i].Stop();
        }
    }
}
