﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Aws.Core.Utilities;
using LxCommon.Utilities;
using LxDetectionArchiver.Common;
using LxDetectionArchiver.Interfaces;
using LxDetectionArchiver.Process;

namespace LxDetectionArchiver
{
    public class LocalArchiver : IRunnable, IStatusProvider
    {
        private readonly FileArchiver _fileArchiver;
        private readonly ManualResetEventSlim _stopEvent;
        private readonly FileCleaner _fileCleaner;

        public LocalArchiver()
        {
            _stopEvent = new ManualResetEventSlim(false);

            var isTlnSystem = Config.SystemType.ToUpper().Equals("TLN");

            _fileArchiver = new FileArchiver(Config.PickupFolder, Config.ImportIntervalSeconds, Config.ArchiveFolder, Config.FilePattern);


            _fileCleaner = null;
            if (Config.EnableCleanup)
            {
                _fileCleaner = new FileCleaner(Config.ArchiveFolder, Config.FilePattern, Config.CleanupIntervalMinutes, Config.ArchiveTimeToLiveHours);
                _fileCleaner.Start();
            }
        }

        public void Start()
        {
            EventManager.LogInfo("File Archiver is called");
            _fileArchiver.Start();
        }

        public void Stop()
        {
            _stopEvent.Set();
            _fileArchiver.Stop();
            _fileCleaner?.Stop();
        }

        public List<KeyValuePair<string, string>> GetMonitorigStatus()
        {
            var monitoringStatus = new List<KeyValuePair<string, string>>();

            var utcNow = DateTime.UtcNow;
            var lastArchivedFileTimestamp = DateTime.MinValue;
            var ageArchived = 0.00;
            var pickupFileCount = 0;
            if (_fileArchiver != null)
            {
                lastArchivedFileTimestamp = _fileArchiver.LastFileArchived;
                ageArchived = Math.Round((utcNow - _fileArchiver.LastFileArchived).TotalSeconds, 0, MidpointRounding.AwayFromZero);
                pickupFileCount = _fileArchiver.GetFileCount();
            }
            monitoringStatus.Add(new KeyValuePair<string, string>("lastFileArchivedTimestampUtc", lastArchivedFileTimestamp.ToString(CultureInfo.InvariantCulture)));
            monitoringStatus.Add(new KeyValuePair<string, string>("ageLastFileArchivedSeconds", ageArchived.ToString(CultureInfo.InvariantCulture)));
            monitoringStatus.Add(new KeyValuePair<string, string>("pickupFileCount", pickupFileCount.ToString(CultureInfo.InvariantCulture)));

            var lastCleanedTimestamp = DateTime.MinValue;
            var ageLastClean = 0.00;
            if (_fileCleaner != null)
            {
                lastCleanedTimestamp = _fileCleaner.LastRun;
                ageLastClean = Math.Round((utcNow - lastCleanedTimestamp).TotalMinutes, 1, MidpointRounding.AwayFromZero);
            }
            monitoringStatus.Add(new KeyValuePair<string, string>("lastFileCleanupTimestamp", lastCleanedTimestamp.ToString(CultureInfo.InvariantCulture)));
            monitoringStatus.Add(new KeyValuePair<string, string>("ageLastFileCleanupMinutes", ageLastClean.ToString(CultureInfo.InvariantCulture)));

            return monitoringStatus;
        }
    }
}
