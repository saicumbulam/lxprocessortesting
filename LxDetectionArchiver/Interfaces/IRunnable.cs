﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aws.Core.Data.Manager;

namespace LxDetectionArchiver.Interfaces
{
    public interface IRunnable
    {
        void Start();
        void Stop();
    }
}
