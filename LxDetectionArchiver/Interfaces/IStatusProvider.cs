﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LxDetectionArchiver.Common;

namespace LxDetectionArchiver.Interfaces
{
    public interface IStatusProvider
    {
        List<KeyValuePair<string, string>> GetMonitorigStatus();
    }
}
