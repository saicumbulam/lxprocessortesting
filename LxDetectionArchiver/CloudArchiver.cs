﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using Aws.Core.Utilities;
using LxCommon.Utilities;
using LxDetectionArchiver.Common;
using LxDetectionArchiver.Interfaces;
using LxDetectionArchiver.Process;
using System.Threading.Tasks;

namespace LxDetectionArchiver
{
    public class CloudArchiver : IRunnable, IStatusProvider, IDisposable
    {
        private readonly Counters _counters;
        private readonly FileWatcher _fileWatcher;
        private readonly CloudWriter _cloudWriter;
        private readonly ConcurrentDictionary<string, FileInfo> _fileTracker;

        private readonly ReaderWriterLockSlim _fileTrackerLock;

        private readonly DirectoryInfo _pickupFolder;
        private readonly DirectoryInfo _archiveFolder;
        private readonly DirectoryInfo _failedFlashFolder;
        private readonly DirectoryInfo _invalidFlashFolder;

        private readonly string _filePattern;

        private const int MaxLockTries = 5;
        private const int MaxLockWaitMillseconds = 2;

        private FileProcessor _fileProcessor;

        private bool _disposed;
        private bool _started;
        private CancellationTokenSource _cancellationTokenSource;

        private DateTime _lastFileProcesedDateTimeUtc;


        private Task _recurringFailedFileCleanupTask;
        private Task _recurringInvalidFileCleanupTask;
        private Task _recurringCloudWriterFlushTask;
        private Task _recurringCloudWriterProcessNonTrackedFilesTask;
        private Task _dailyCounterResetTask;
        private Task _dailyArchiveCleanupTask;
        private Task _dailyProcessTrackedFilesTask;

        private const string FilesProcessedCounterName = "FilesProcessed";
        private const string FilesArchivedCounterName = "FilesArchived";

        private const string UnknownPath = "Unknown";


        public CloudArchiver()
        {
            EventManager.LogDebug($"CloudArchiver - S3 enabled: {Config.S3Enabled}");


            if (!Config.S3Enabled) return;

            _disposed = false;

            _filePattern = Config.FilePattern;

            _fileTracker = new ConcurrentDictionary<string, FileInfo>();

            _fileTrackerLock = new ReaderWriterLockSlim();

            _counters = new Counters();

            _pickupFolder = FileHelper.CreateDirectory(Config.S3PickupFolder);
            _archiveFolder = FileHelper.CreateDirectory(Config.S3ArchiveFolder);
            _failedFlashFolder = FileHelper.CreateDirectory(Config.S3FailedFlashFolder);
            _invalidFlashFolder = FileHelper.CreateDirectory(Config.S3InvalidFlashFolder);

            _cloudWriter = new CloudWriter(Config.S3Bucket);
            _fileWatcher = new FileWatcher(Config.S3PickupFolder, Config.FilePattern);


            _started = false;
        }

        public void Start()
        {
            EventManager.LogInfo($"CloudArchiver.Start() - Starting");

            if (_cancellationTokenSource?.Token != null)
            {
                _cancellationTokenSource.Cancel();
                _cancellationTokenSource.Dispose();
            }

            _cancellationTokenSource = new CancellationTokenSource();

            _fileProcessor = new FileProcessor(_cancellationTokenSource.Token);

            ResetCounters();

            _cloudWriter?.Start();


            CloudWriterProcessNonTrackedFiles();
            

            _recurringCloudWriterFlushTask = RecurringTask.Create(TimeSpan.Zero, TimeSpan.FromHours(Config.S3FlushFrequencyHours), CloudWriterFlush, _cancellationTokenSource.Token);

            _recurringCloudWriterProcessNonTrackedFilesTask = RecurringTask.Create(TimeSpan.Zero, TimeSpan.FromHours(Config.S3UntrackedFileFrequencyHours), CloudWriterProcessNonTrackedFiles, _cancellationTokenSource.Token);

            _recurringInvalidFileCleanupTask = RecurringTask.Create(TimeSpan.Zero, TimeSpan.FromHours(Config.S3InvalidFlashFileCleanUpFrequencyHours), CleanupInvalidFiles, _cancellationTokenSource.Token);
            _recurringFailedFileCleanupTask = RecurringTask.Create(TimeSpan.Zero, TimeSpan.FromHours(Config.S3FailedFlashFileCleanUpFrequencyHours), CleanupFailedFiles, _cancellationTokenSource.Token);


            _dailyCounterResetTask = RecurringScheduledTask.CreateDaily(DateTime.UtcNow.AddDays(1).Date, ResetCounters, _cancellationTokenSource.Token);
            _dailyArchiveCleanupTask = RecurringScheduledTask.CreateDaily(DateTime.UtcNow.AddDays(1).Date, CleanupArchiveFolder, _cancellationTokenSource.Token);

            _dailyProcessTrackedFilesTask = RecurringScheduledTask.CreateDaily(DateTime.UtcNow.AddDays(1).Date, CloudWriterProcessTrackedFiles, _cancellationTokenSource.Token);


            if (_fileWatcher != null)
            {
                _fileWatcher.FileReady += OnFileReady;
                _fileWatcher.Start();
            }

            _started = true;

            EventManager.LogInfo($"CloudArchiver.Start() - Started");
        }

        public void Stop()
        {
            EventManager.LogInfo($"CloudArchiver.Stop() - Stopping");

            if (_fileWatcher != null)
            {
                _fileWatcher.Stop();
                _fileWatcher.FileReady -= OnFileReady;
            }

            _cancellationTokenSource.Cancel();

            _cloudWriter?.Stop();

            _fileProcessor = null;

            _started = false;

            EventManager.LogInfo($"CloudArchiver.Stop() - Stopped");
        }

        private void OnFileReady(object sender, FileReadyEventArgs e)
        {
            _cancellationTokenSource?.Token.ThrowIfCancellationRequested();

            if (e?.File == null || !e.File.Exists) return;

            EventManager.LogDebug($"CloudArchiver.OnFileReady() - Event raised. File: {e.FullName}");

            _lastFileProcesedDateTimeUtc = DateTime.UtcNow;

            var readLockHeld = false;
            var writeLockHeld = false;

            try
            {
                var invalidFile = false;
                var doesNotContainKey = false;

                var processFile = false;
                var tries = 0;

                while (!(readLockHeld = _fileTrackerLock.TryEnterReadLock(MaxLockWaitMillseconds)) && tries < MaxLockTries)
                    tries++;

                if (readLockHeld)
                {
                    doesNotContainKey = _fileTracker.IsEmpty || !_fileTracker.ContainsKey(e.FullName);

                    _fileTrackerLock.ExitReadLock();
                    readLockHeld = false;
                }
                else
                {
                    EventManager.LogWarning($"CloudArchiver.OnFileReady() - Failed to obtain read lock. Moving file to failed file folder. File: {e.FullName}");
                    MoveToFailedFlashFolder(e.File);
                }

                if (doesNotContainKey)
                {
                    tries = 0;

                    while (!(writeLockHeld = _fileTrackerLock.TryEnterWriteLock(MaxLockWaitMillseconds)) && tries < MaxLockTries)
                        tries++;

                    if (writeLockHeld)
                    {
                        _fileTracker.AddOrUpdate(e.FullName, e.File, (key, existingValue) => e.File);
                        processFile = true;

                        _fileTrackerLock.ExitWriteLock();
                        writeLockHeld = false;
                    }
                    else
                    {
                        EventManager.LogWarning($"CloudArchiver.OnFileReady() - Failed to obtain write lock. Moving file to failed file folder. File: {e.FullName}");
                    }
                }

                if (processFile)
                {
                    _cancellationTokenSource?.Token.ThrowIfCancellationRequested();
                    EventManager.LogDebug($"CloudArchiver.OnFileReady() - Processing file. File: {e.FullName}");

                    var flashes = _fileProcessor.GetVaidFlashes(e.File);

                    _cancellationTokenSource?.Token.ThrowIfCancellationRequested();

                    if (flashes != null && flashes.Any())
                    {
                        EventManager.LogDebug($"CloudArchiver.OnFileReady() - Storing flashes. Count: {flashes.Count}");
                        _cloudWriter.StoreFlashes(flashes);
                    }
                    else
                    {
                        EventManager.LogWarning($"CloudArchiver.OnFileReady() - No flashes returned. Moving file to invalid flash folder. File: {e.FullName}");
                        MoveToInvalidFlashFolder(e.File);
                        invalidFile = true;
                    }

                    tries = 0;
                    while (!(writeLockHeld = _fileTrackerLock.TryEnterWriteLock(MaxLockWaitMillseconds)) && tries < MaxLockTries)
                        tries++;

                    if (writeLockHeld)
                    {
                        FileInfo junkFile;
                        _fileTracker.TryRemove(e.FullName, out junkFile);

                        _fileTrackerLock.ExitWriteLock();
                        writeLockHeld = false;
                    }
                    else
                    {
                        EventManager.LogWarning($"CloudArchiver.OnFileReady() - Failed to obtain second write lock. File: {e.FullName}");
                    }

                    if (!invalidFile) MoveToArchiveFolder(e.File);

                    _counters.IncrementCounter(FilesProcessedCounterName, 1);
                }
                else
                {
                    EventManager.LogDebug($"CloudArchiver.OnFileReady() - Skipping file as it's already in progress. File: {e.FullName}");
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError($"CloudArchiver.OnFileReady() - Caught exception while processing file. Moving file to failed flash folder. File: {e.FullName}, Error: {ex.Message}", ex);

                if (e?.File != null && e.File.Exists)
                    MoveToFailedFlashFolder(e.File);
            }
            finally
            {
                if (readLockHeld) _fileTrackerLock.ExitReadLock();
                if (writeLockHeld) _fileTrackerLock.ExitWriteLock();
            }
        }
        
        private void MoveToFailedFlashFolder(FileInfo failedFile)
        {
            if (failedFile != null && failedFile.Exists)
            {
                try
                {
                    var destinationFile = Path.Combine(_failedFlashFolder.FullName, failedFile.Name);
                    failedFile.MoveTo(destinationFile);
                    EventManager.LogInfo($"CloudArchiver.MoveToFailedFlashFolder() - Moved file to failed flash folder. File: {failedFile.FullName}, Destination: {destinationFile}");
                }
                catch (Exception ex)
                {
                    EventManager.LogError($"CloudArchiver.MoveToFailedFlashFolder() - Caught exception while moving file to failed file folder. File: {failedFile.FullName}, Failed File Folder: {_failedFlashFolder.FullName}, Error: {ex.Message}", ex);
                }
            }
        }

        private void MoveToInvalidFlashFolder(FileInfo invalidFile)
        {
            if (invalidFile != null && invalidFile.Exists)
            {
                try
                {
                    var destinationFile = Path.Combine(_invalidFlashFolder.FullName, invalidFile.Name);
                    invalidFile.MoveTo(destinationFile);
                    EventManager.LogInfo($"CloudArchiver.MoveToInvalidFlashFolder() - Moved file to invalid flash folder. File: {invalidFile.FullName}, Destination: {destinationFile}");
                }
                catch (Exception ex)
                {
                    EventManager.LogError($"CloudArchiver.MoveToInvalidFlashFolder() - Caught exception while moving file to invalid file folder. File: {invalidFile.FullName}, Invalid File Folder: {_invalidFlashFolder.FullName}, Error: {ex.Message}", ex);
                }
            }
        }

        private void MoveToArchiveFolder(FileInfo sourceFile)
        {
            if (sourceFile != null && sourceFile.Exists)
            {
                var destinationFile = UnknownPath;
                var originalSourceFile = sourceFile.FullName;

                try
                {
                    EventManager.LogDebug($"CloudArchiver.MoveToArchiveFolder() - Archiving file. Source: {sourceFile.FullName}, Destination Folder: {_archiveFolder.FullName}");

                    var dt = FileHelper.GetFileTimeFromName(sourceFile.Name, Config.FilePattern);
                    if (dt > DateTime.MinValue && dt < DateTime.MaxValue)
                        destinationFile = Path.Combine(Path.Combine(Path.Combine($"{dt.Year:D4}", $"{dt.Month:D2}"), $"{dt.Day:D2}"), $"{dt.Hour:D2}");

                    destinationFile = Path.Combine(_archiveFolder.FullName, destinationFile);

                    FileHelper.CreateDirectory(destinationFile);

                    destinationFile = Path.Combine(destinationFile, sourceFile.Name);

                    if (File.Exists(destinationFile))
                    {
                        EventManager.LogDebug($"CloudArchiver.MoveToArchiveFolder() - Appending data to existing file. Source: {originalSourceFile}, Destination: {destinationFile}");

                        var appendText = File.ReadAllText(sourceFile.FullName);
                        File.AppendAllText(destinationFile, appendText);
                        sourceFile.Delete();
                    }
                    else
                    {
                        EventManager.LogDebug($"CloudArchiver.MoveToArchiveFolder() - Moving file to archive folder. Source: {originalSourceFile}, Destination: {destinationFile}");
                        sourceFile.MoveTo(destinationFile);
                    }

                    _counters.IncrementCounter(FilesArchivedCounterName, 1);
                }
                catch (Exception ex)
                {
                    EventManager.LogError($"CloudArchiver.MoveToArchiveFolder() - Failed to archive file due to exception. Source: {originalSourceFile}, Destination: {destinationFile}", ex);
                }

                if (!File.Exists(destinationFile))
                {
                    EventManager.LogError($"CloudArchiver.MoveToArchiveFolder() - Unable to archive file. Source file not moved to destination. Source: {originalSourceFile}, Destination: {destinationFile}");
                }

                if (File.Exists(originalSourceFile))
                {
                    EventManager.LogDebug($"CloudArchiver.MoveToArchiveFolder() - Original source file still exists. Deleting. Source: {originalSourceFile}");

                    try { File.Delete(originalSourceFile); }
                    catch (Exception ex)
                    {
                        EventManager.LogError($"CloudArchiver.MoveToArchiveFolder() - Unable to delete file. File: {originalSourceFile}, Error: {ex.Message}", ex);
                    }
                }
            }
            else
            {
                EventManager.LogWarning($"CloudArchiver.MoveToArchiveFolder() - Unable to archive file as source file did not exist. Source: {sourceFile?.FullName}");
            }
        }

        #region Background Jobs

        private void ResetCounters()
        {
            _cancellationTokenSource?.Token.ThrowIfCancellationRequested();

            EventManager.LogInfo($"CloudArchiver.ResetCounters() - Resetting counters.");
            _counters.ResetCounters();
            _cloudWriter.ResetCounters();
        }

        private void CleanupInvalidFiles()
        {
            _cancellationTokenSource?.Token.ThrowIfCancellationRequested();

            EventManager.LogInfo($"CloudArchiver.CleanupInvalidFiles() - Cleaning up Invalid Flashes folder. Folder: {_invalidFlashFolder.FullName}");

            var invalidFileTimeToLiveHours = Config.S3InvalidFlashFileTimeToLiveHours;
            var expiryTime = DateTime.UtcNow.AddHours(-Math.Abs(invalidFileTimeToLiveHours));

            foreach (var invalidFlashFile in _invalidFlashFolder.GetFiles("*", SearchOption.AllDirectories).Where(f => f.Exists && f.LastWriteTimeUtc < expiryTime))
            {
                _cancellationTokenSource?.Token.ThrowIfCancellationRequested();

                EventManager.LogDebug($"CloudArchiver.CleanupInvalidFiles() - Removing expired invalid flash file. File: {invalidFlashFile.FullName}, Timestamp: {invalidFlashFile.LastWriteTimeUtc}, Time To Live (Hours): {invalidFileTimeToLiveHours}");
                if (invalidFlashFile.Exists) invalidFlashFile.Delete();
            }
        }

        private void CleanupFailedFiles()
        {
            _cancellationTokenSource?.Token.ThrowIfCancellationRequested();

            EventManager.LogInfo($"CloudArchiver.CleanupArchiveFolder() - Cleaning up Failed Flashes folder. Folder: {_failedFlashFolder.FullName}");

            foreach (var failedFlashFile in _failedFlashFolder.GetFiles(_filePattern, SearchOption.AllDirectories).Where(f => f.Exists))
            {
                _cancellationTokenSource?.Token.ThrowIfCancellationRequested();

                if (failedFlashFile.Length > 0)
                {
                    var destinationFile = Path.Combine(_pickupFolder.FullName, failedFlashFile.Name);
                    failedFlashFile.MoveTo(destinationFile);
                    EventManager.LogDebug($"CloudArchiver.CleanupFailedFiles() - Moved failed flash file to pickup folder for reprocessing. File: {failedFlashFile.FullName}, Destination: {destinationFile}");
                }
                else
                {
                    var destinationFile = Path.Combine(_pickupFolder.FullName, failedFlashFile.Name);
                    failedFlashFile.MoveTo(destinationFile);
                    EventManager.LogWarning($"CloudArchiver.CleanupFailedFiles() - Moved zero length failed flash file to invalid flash folder. File: {failedFlashFile.FullName}, Destination: {destinationFile}");
                }
            }
        }

        private void CleanupArchiveFolder()
        {
            _cancellationTokenSource?.Token.ThrowIfCancellationRequested();

            EventManager.LogInfo($"CloudArchiver.CleanupArchiveFolder() - Cleaning up Archive folder. Folder: {_archiveFolder.FullName}");

            var archiveTimeToLiveDays = Config.S3ArchiveTimeToLiveDays;
            var expiryTime = DateTime.UtcNow.AddDays(-Math.Abs(archiveTimeToLiveDays));

            foreach (var archivedFile in _failedFlashFolder.GetFiles(_filePattern, SearchOption.AllDirectories).Where(f => f.Exists && f.LastWriteTimeUtc < expiryTime))
            {
                _cancellationTokenSource?.Token.ThrowIfCancellationRequested();

                EventManager.LogDebug($"CloudArchiver.CleanupArchiveFolder() - Removing expired archive file. File: {archivedFile.FullName}, Timestamp: {archivedFile.LastWriteTimeUtc}, Time To Live (Days): {archiveTimeToLiveDays}");
                if (archivedFile.Exists) archivedFile.Delete();
            }
        }

        private void CloudWriterFlush()
        {
            _cancellationTokenSource?.Token.ThrowIfCancellationRequested();

            EventManager.LogInfo($"CloudArchiver.CloudWriterFlush() - Flushing CloudWriter.");
            _cloudWriter.FlushToCloud();

        }

        private void CloudWriterProcessTrackedFiles()
        {
            _cancellationTokenSource?.Token.ThrowIfCancellationRequested();

            EventManager.LogInfo($"CloudArchiver.CloudWriterProcessTrackedFiles() - Processing tracked CloudWriter files.");
            _cloudWriter.ProcessTrackedFiles();
        }

        private void CloudWriterProcessNonTrackedFiles()
        {
            _cancellationTokenSource?.Token.ThrowIfCancellationRequested();

            EventManager.LogInfo($"CloudArchiver.CloudWriterProcessNonTrackedFiles() - Processing non-tracked CloudWriter files.");
            _cloudWriter.ProcessNonTrackedFiles();
        }

        #endregion Background Tasks

        #region Counters/Status
        public List<KeyValuePair<string, string>> GetMonitorigStatus()
        {
            var monitoringStatus = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("cloudArchiverInvalidFlashesCount",GetInvalidFlashesCount().ToString(CultureInfo.InvariantCulture)),
                new KeyValuePair<string, string>("cloudArchiverValidFlashesCount",GetValidFlashesCount().ToString(CultureInfo.InvariantCulture)),
                new KeyValuePair<string, string>("cloudArchiverFlashesProcessedCount",GetFlashesProcessedCount().ToString(CultureInfo.InvariantCulture)),
                new KeyValuePair<string, string>("cloudArchiverFilesProcessedCount",GetFilesProcessedCount().ToString(CultureInfo.InvariantCulture)),
                new KeyValuePair<string, string>("cloudArchiverFailedFileCount",GetFailedFileCount().ToString(CultureInfo.InvariantCulture)),
                new KeyValuePair<string, string>("cloudArchiverFilesArchivedCount",GetFilesArchivedCount().ToString(CultureInfo.InvariantCulture)),
                new KeyValuePair<string, string>("cloudArchiverInvalidFileCount",GetInvalidFileCount().ToString(CultureInfo.InvariantCulture)),
                new KeyValuePair<string, string>("cloudArchiverLastFileProcessedTimestampUtc",GetLastFileProcessedTimestampUtc().ToString(CultureInfo.InvariantCulture)),
                new KeyValuePair<string, string>("cloudArchiverAgeLastFileReadSeconds",GetSecondsSinceLastFileProcessed().ToString(CultureInfo.InvariantCulture))
            };

            return monitoringStatus;
        }

        #region FileProcessor Counts
        public ulong GetInvalidFlashesCount()
        {
            return _fileProcessor.GetFlashesInvalidCount();
        }

        public ulong GetValidFlashesCount()
        {
            return _fileProcessor.GetFlashesValidCount();
        }

        public ulong GetFlashesProcessedCount()
        {
            return _fileProcessor.GetFlashesProcessedCount();
        }
        #endregion FileProcessor Counts

        public ulong GetFilesProcessedCount()
        {
            return _counters.GetCounterValue(FilesProcessedCounterName);
        }

        public long GetFailedFileCount()
        {
            return _failedFlashFolder.GetFiles("*", SearchOption.AllDirectories).LongLength;
        }

        public ulong GetFilesArchivedCount()
        {
            return _counters.GetCounterValue(FilesArchivedCounterName);
        }

        public long GetInvalidFileCount()
        {
            return _invalidFlashFolder.GetFiles("*", SearchOption.AllDirectories).LongLength;
        }

        public DateTime GetLastFileProcessedTimestampUtc()
        {
            return _lastFileProcesedDateTimeUtc;
        }

        public long GetSecondsSinceLastFileProcessed()
        {
            return Convert.ToInt64(Math.Round((DateTime.UtcNow - GetLastFileProcessedTimestampUtc()).TotalSeconds, 0, MidpointRounding.AwayFromZero));
        }

        #endregion Counters/Status

        #region IDisposable
        public void Dispose()
        {
            if (_disposed) return;

            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected void Dispose(bool disposing)
        {
            if (_disposed) throw new ObjectDisposedException(null);

            try
            {
                if (disposing)
                {
                    if (_started) Stop();

                    _counters?.Dispose();
                    _fileTrackerLock?.Dispose();
                    _fileWatcher?.Dispose();

                    _recurringFailedFileCleanupTask?.Dispose();
                    _recurringInvalidFileCleanupTask?.Dispose();
                    _recurringCloudWriterProcessNonTrackedFilesTask?.Dispose();
                    _recurringCloudWriterFlushTask?.Dispose();
                    _dailyArchiveCleanupTask?.Dispose();
                    _dailyCounterResetTask?.Dispose();
                    _dailyProcessTrackedFilesTask?.Dispose();
                    _cancellationTokenSource?.Dispose();
                }
            }
            finally
            {
                _disposed = true;
            }
        }
        #endregion IDisposable
    }
}
