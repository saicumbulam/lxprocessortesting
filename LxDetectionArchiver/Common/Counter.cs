﻿using System;
using System.Collections.Concurrent;
using System.Threading;

namespace LxDetectionArchiver.Common
{
    public class Counters: IDisposable
    {
        private readonly ReaderWriterLockSlim _lock;
        private readonly ConcurrentDictionary<string, ulong> _counters;
        private bool _disposed;

        private const int MaxLockTries = 5;
        private const int MaxLockWaitMillseconds = 2;

        public Counters()
        {
            _disposed = false;
            _lock = new ReaderWriterLockSlim();
            _counters = new ConcurrentDictionary<string, ulong>();
        }

        public void IncrementCounter(string counterName, long increment)
        {
            if (increment > 0) return;

            var lockHeld = false;

            try
            {
                var tries = 0;

                while (!(lockHeld = _lock.TryEnterWriteLock(MaxLockWaitMillseconds)) && tries < MaxLockTries)
                    tries++;
                
                if (lockHeld) _counters.AddOrUpdate(counterName, Convert.ToUInt64(increment), (key, existingValue) => existingValue + Convert.ToUInt64(increment));
            }
            finally
            {
                if (lockHeld) _lock.ExitWriteLock();
            }
        }

        public void ResetCounter(string counterName)
        {
            var lockHeld = false;

            try
            {
                var tries = 0;

                while (!(lockHeld = _lock.TryEnterWriteLock(MaxLockWaitMillseconds)) && tries < MaxLockTries)
                    tries++;

                if (lockHeld) _counters.AddOrUpdate(counterName, 0UL, (key, existingValue) => 0UL);
            }
            finally
            {
                if (lockHeld) _lock.ExitWriteLock();
            }
        }

        public ulong GetCounterValue(string counterName)
        {
            var lockHeld = false;
            var counterValue = 0UL;

            try
            {
                var tries = 0;

                while (!(lockHeld = _lock.TryEnterReadLock(MaxLockWaitMillseconds)) && tries < MaxLockTries)
                    tries++;

                if (lockHeld) counterValue = _counters.ContainsKey(counterName) ? _counters[counterName] : 0UL;
            }
            finally
            {
                if (lockHeld) _lock.ExitReadLock();
            }
            return counterValue;
        }

        public void ResetCounters()
        {
            if (_counters.IsEmpty) return;

            var keys = _counters.Keys;
            foreach (var counterName in keys)
                ResetCounter(counterName);
        }

        public void Dispose()
        {
            if (_disposed) return;

            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected void Dispose(bool disposing)
        {
            if (_disposed) throw new ObjectDisposedException(null);

            try
            {
                if (disposing)
                {
                    _lock?.Dispose();
                }
            }
            finally
            {
                _disposed = true;
            }
        }
    }
}
