﻿using System;
using Aws.Core.Utilities.Config;

namespace LxDetectionArchiver.Common
{
    public class Config : AppConfig
    {
        static Config()
        {
        }

        public static string SystemType
        {
            get { return Get("SystemType", "TLN"); }
        }

        public static bool EnableDbArchive
        {
            get { return Get("EnableDbArchive", true); }
        }

        public static bool EnableArchive 
        {
            get { return Get("EnableArchive", true); }
        }

        public static string ArchiveFolder
        {
            get { return Get("ArchiveFolder", @"E:\EN\Archive\LxDetectionArchive"); }
        }

        public static string PickupFolder
        {
            get { return Get("PickupFolder", @"E:\EN\Archive\LxDatafeed\EnGlnNetwork\Pickup"); }
        }

        public static int ImportIntervalSeconds
        {
            get { return Get("ImportIntervalSeconds", 5); }
        }

        public static string FilePattern
        {
            get { return Get("FilePattern", "flash-*.txt"); }
        }

        public static int MaxDbWriterProcessorTasks
        {
            get { return Get("MaxDbWriterProcessorTasks", 5); }
        }

        public static bool EnableDbFailureLogging
        {
            get { return Get("EnableDbFailureLogging", false); }
        }

        public static string FailedFlashFolder
        {
            get { return Get("FailedFlashFolder", @"E:\EN\Archive\LxDetectionArchiver\Failed Flashes"); }
        }

        public static string FailedFlashFileName
        {
            get { return Get("FailedFlashFileName", "Failed Flashes {0}.json"); }
        }

        public static string FailedPortionFolder
        {
            get { return Get("FailedPortionFolder", @"E:\EN\Archive\LxDetectionArchiver\Failed Portions"); }
        }

        public static string FailedPortionFileName
        {
            get { return Get("FailedPortionFileName", "Failed Portions {0}.json"); }
        }

        public static bool EnableRetryDbFailures
        {
            get { return Get("EnableRetryDbFailures", true); }
        }

        public static int MaxRetryDbWriterProcessorTasks
        {
            get { return Get("MaxRetryDbWriterProcessorTasks", 2); }
        }

        public static int RetryDbFailuresIntervalMinutes
        {
            get { return Get("RetryDbFailuresIntervalMinutes", 240); }
        }

        public static string InvalidFlashFolder
        {
            get { return Get("InvalidFlashFolder", @"E:\EN\Archive\LxDetectionArchiver\Invalid Flashes"); }
        }

        public static string InvalidFlashFileName
        {
            get { return Get("InvalidFlashFileName", "Invalid Flashes {0}.json"); }
        }

        public static string InvalidPortionFolder
        {
            get { return Get("InvalidPortionFolder", @"E:\EN\Archive\LxDetectionArchiver\Invalid Portions"); }
        }

        public static string InvalidPortionFileName
        {
            get { return Get("InvalidPortionFileName", "Invalid Portions {0}.json"); }
        }

        public static bool EnableCleanup
        {
            get { return Get("EnableCleanup", true); }
        }

        public static int CleanupIntervalMinutes
        {
            get { return Get("CleanupIntervalMinutes", 30); }
        }

        public static int ArchiveTimeToLiveHours
        {
            get { return Get("ArchiveTimeToLiveHours", 24); }
        }

        public static int MaxDbWriterQueueSize
        {
            get { return Get("MaxDbWriterQueueSize", 100000); }
        }

        public static int MonitorLastFlashDbAgeSecondsMaximum
        {
            get { return Get("MonitorLastFlashDbAgeSecondsMaximum", 240); }
        }

        public static int MonitorLastFileProcessedAgeSecondsMaximum
        {
            get { return Get("MonitorLastFileProcessedAgeSecondsMaximum", 180); }
        }

        public static int MonitorLastFileArchivedAgeSecondsMaximum
        {
            get { return Get("MonitorLastFileArchivedAgeSecondsMaximum", 180); }
        }

        public static int MonitorLastCleanupAgeMinutesMaximum
        {
            get { return Get("CleanupIntervalMinutes", 30) + 2; }
        }

        public static int MonitorInvalidFileCountMaximum
        {
            get { return Get("MonitorInvalidFileCountMaximum", 1); }
        }

        public static int MonitorLastS3ArchivepAgeSecondsMaximum 
        {
            get { return Get("MonitorLastS3ArchivepAgeSecondsMaximum", 120); }
        }

        public static bool IsTlnSystem
        {
            get 
            { 
                return (Get("SystemType", "TLS") == "TLN"); 
            }
        }

        public static Uri DatafeedStatusUrl
        {
            get
            {
                string url = Get("DatafeedStatusUrl", "http://localhost:15555/OpsCommand/prtg");
                Uri uri;
                if (!Uri.TryCreate(url, UriKind.RelativeOrAbsolute, out uri))
                {
                    uri = new Uri("http://localhost:15555/OpsCommand/prtg");
                }
                return uri;
            }
        }

        public static int DatafeedStatusHttpTimeoutSeconds
        {
            get { return Get("DatafeedStatusHttpTimeoutSeconds", 5); }
        }

        public static int FailedFlashMinutesPerFile
        {
            get { return Get("FailedFlashMinutesPerFile", 1); }
        }
        
        public static int FailedPortionMinutesPerFile
        {
            get { return Get("FailedPortionMinutesPerFile", 1); }
        }

        public static int InvalidFlashMinutesPerFile
        {
            get { return Get("InvalidFlashMinutesPerFile", 1); }
        }
        public static int InvalidPortionMinutesPerFile
        {
            get { return Get("InvalidPortionMinutesPerFile", 1); }
        }

        public static int MaxBrokerEndpointCount
        {
            get { return Get("MaxBrokerEndpointCount", 5000); }
        }



        public static bool S3Enabled
        {
            get { return Get("S3Enabled", true); }
        }

        public static string S3Bucket
        {
            get { return Get<string>("S3Bucket", null); }
        }

        public static string S3PathPrefix
        {
            get { return Get<string>("S3PathPrefix", null); }
        }

        public static int S3MaxFileSizeMB
        {
            get { return Get("S3MaxFileSizeMB", 100); }
        }

        public static string S3PickupFolder
        {
            get { return Get("S3PickupFolder", @"E:\En\Archive\LxDatafeed\EnGlnNetwork\S3"); }
        }

        public static string S3StagingFolder
        {
            get { return Get("S3StagingFolder", @"E:\EN\Archive\LxDetectionArchiver\CloudArchiver\Staging"); }
        }

        public static string S3TransferFolder
        {
            get { return Get("S3TransferFolder", @"E:\EN\Archive\LxDetectionArchiver\CloudArchiver\Transfer"); }
        }

        public static string S3FailedFlashFolder
        {
            get { return Get("S3FailedFlashFolder", @"E:\EN\Archive\LxDetectionArchiver\CloudArchiver\FailedFlashes"); }
        }

        public static string S3InvalidFlashFolder
        {
            get { return Get("S3InvalidFlashFolder", @"E:\EN\Archive\LxDetectionArchiver\CloudArchiver\InvalidFlashes"); }
        }

        public static string S3ArchiveFolder
        {
            get { return Get("S3ArchiveFolder", @"E:\EN\Archive\LxDetectionArchiver\S3Archive"); }
        }

        public static int S3MaxConcurrentFileUploads
        {
            get { return Get("S3MaxConcurrentFileUploads", 10); }
        }

        public static int S3FailedFlashFileCleanUpFrequencyHours
        {
            get { return Get("S3FailedFlashFileCleanUpFrequencyHours", 12); }
        }

        public static int S3InvalidFlashFileCleanUpFrequencyHours
        {
            get { return Get("S3InvalidFlashFileCleanUpFrequencyHours", 12); }
        }

        public static int S3InvalidFlashFileTimeToLiveHours
        {
            get { return Get("S3InvalidFlashFileTimeToLiveHours", 12); }
        }

        public static int S3ArchiveTimeToLiveDays
        {
            get { return Get("S3ArchiveTimeToLiveDays", 2); }
        }

        public static int S3FlushFrequencyHours
        {
            get { return Get("S3FlushFrequencyHours", 1); }
        }

        public static int S3UntrackedFileTimeToLiveHours
        {
            get { return Get("S3UntrackedFileTimeToLiveHours", 2); }
        }

        public static int S3UntrackedFileFrequencyHours
        {
            get { return Get("S3UntrackedFileFrequencyHours", 1); }
        }
    }
}
