﻿using System;

namespace LxDetectionArchiver.Common
{
    public static class Guard
    {
        public static void ArgumentIsNotNull(object value, string argument)
        {
            if (value == null)
            {
                throw new ArgumentNullException(argument);
            }
        }

        public static void ArgumentIsInRange(int value, string argument, int min, int max)
        {
            if (value < min || value > max)
            {
                throw new ArgumentOutOfRangeException(argument, string.Format("{0} must be >= {1} and <= {2}", argument, min, max));
            }
        }
    }
}
