﻿namespace LxDetectionArchiver.Common
{
    public static class EventId
    {
        public static class Program
        {
            public const ushort Global = 1800;   
        }

        public static class Common
        {
            public static class Config
            {
                public const ushort CreatingTextWriter = 1810;
            }
        }

        public static class Process
        {
            public static class FileDbImporter
            {
                public const ushort CreateDirectory = 1820;
                public const ushort Deserialize = 1821;
                public const ushort ReadFile = 1822;
                public const ushort DeleteFile = 1823;
                public const ushort MoveFile = 1824;
                public const ushort FindFiles = 1825;
                public const ushort IsDbBrokerReady = 1826; 
            }

            public static class DbWriterProcessor
            {
                public const ushort InsertFlash = 1830;
                public const ushort InsertPortions = 1831;
                public const ushort CreatePoint = 1832;
                public const ushort QueueToLarge = 1833;
                public const ushort Shutdown = 1834;
                public const ushort InsertFlashAggregateData = 1835;
            }

            public static class FileCleaner
            {
                public const ushort FindDirectories = 1840;
                public const ushort FindFiles = 1841;
                public const ushort ProcessFiles = 1842;
                public const ushort DeleteDirectory = 1843;
                public const ushort Start = 1844;
                public const ushort Finished = 1845;
            }

            public static class MonitoringHandler
            {
                public const ushort StartOpsHttp = 1850;
                public const ushort StopOpsHttp = 1851;
                public const ushort GetFileCount = 1852;
                public const ushort LoadMonitorFile = 1853;
                public const ushort BuildMonitorHtml = 1854;
                public const ushort GetDatafeedStatus = 1855;
            }

            public const ushort AwsS3FileArchiver = 1860;

            public static class CloudWriter
            {
                public const ushort ProcessFile = 1870;
                public const ushort Flush = 1871;
            }
        }
        
    }
}
