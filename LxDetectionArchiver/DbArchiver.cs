﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Threading;
using Aws.Core.Utilities;
using LxCommon.Utilities;
using LxDetectionArchiver.Common;
using LxDetectionArchiver.Interfaces;
using LxDetectionArchiver.Process;

namespace LxDetectionArchiver
{
    public class DbArchiver : IRunnable, IStatusProvider
    {
        private readonly FileReader _fileReader;
        
        private readonly DatabaseWriter _databaseWriter;
        
        private readonly ManualResetEventSlim _stopEvent;
        private readonly FileCleaner _fileCleaner;
        

        private readonly FileReader _retryFlashFileReader;
        private readonly FileReader _retryPortionFileReader;
        private readonly DatabaseWriter _retryDatabaseWriter;

        

        public DbArchiver()
        {
            _stopEvent = new ManualResetEventSlim(false);

            var isTlnSystem = Config.SystemType.ToUpper().Equals("TLN");

            FlashDataLogger failedFlashLogger = null;
            FlashDataLogger failedPortionLogger = null;

            EventManager.LogInfo("Checking DB Archive Flags");
            if (Config.EnableDbArchive)
            {
                EventManager.LogInfo("EnableDbArchive is set to true");
                if (Config.EnableDbFailureLogging)
                {
                    EventManager.LogInfo("EnableDbFailureLogging is set to true");
                    failedFlashLogger = new FlashDataLogger(Config.FailedFlashFolder, Config.FailedFlashFileName, isTlnSystem, Config.FailedFlashMinutesPerFile);
                    failedPortionLogger = new FlashDataLogger(Config.FailedPortionFolder, Config.FailedPortionFileName, isTlnSystem, Config.FailedPortionMinutesPerFile);
                }

                EventManager.LogInfo("Writing to database");
                _databaseWriter = new DatabaseWriter(ConfigurationManager.ConnectionStrings["LightningEtl"].ConnectionString, ConfigurationManager.ConnectionStrings["LightningEdw"].ConnectionString,
                    failedFlashLogger, failedPortionLogger, Config.MaxDbWriterQueueSize, _stopEvent, Config.MaxDbWriterProcessorTasks, Config.IsTlnSystem);

                _retryFlashFileReader = null;
                _retryPortionFileReader = null;
                _retryDatabaseWriter = null;
                if (Config.EnableRetryDbFailures)
                {
                    EventManager.LogInfo("EnableRetryDbFailures is set to true");
                    var invalidFlashLogger = new FlashDataLogger(Config.InvalidFlashFolder, Config.InvalidFlashFileName, isTlnSystem, Config.InvalidFlashMinutesPerFile);
                    var invalidPortionLogger = new FlashDataLogger(Config.InvalidPortionFolder, Config.InvalidPortionFileName, isTlnSystem, Config.InvalidPortionMinutesPerFile);
                    _retryDatabaseWriter = new DatabaseWriter(ConfigurationManager.ConnectionStrings["LightningEtl"].ConnectionString, ConfigurationManager.ConnectionStrings["LightningEdw"].ConnectionString,
                        invalidFlashLogger, invalidPortionLogger, Config.MaxDbWriterQueueSize, _stopEvent, Config.MaxRetryDbWriterProcessorTasks, Config.IsTlnSystem);

                    _retryFlashFileReader = new FileReader(Config.FailedFlashFolder, Config.RetryDbFailuresIntervalMinutes * 60, null, string.Format(Config.FailedFlashFileName, "*"), _retryDatabaseWriter, false, ConfigurationManager.ConnectionStrings["LightningEtl"].ConnectionString, Config.MaxBrokerEndpointCount);
                    _retryPortionFileReader = new FileReader(Config.FailedPortionFolder, Config.RetryDbFailuresIntervalMinutes * 60, null, string.Format(Config.FailedPortionFileName, "*"), _retryDatabaseWriter, true, ConfigurationManager.ConnectionStrings["LightningEtl"].ConnectionString, Config.MaxBrokerEndpointCount);
                }
            }
            

            _fileReader = new FileReader(Config.PickupFolder, Config.ImportIntervalSeconds, Config.ArchiveFolder, Config.FilePattern, _databaseWriter, false,
                ConfigurationManager.ConnectionStrings["LightningEtl"].ConnectionString, Config.MaxBrokerEndpointCount);

            _fileCleaner = null;
            if (Config.EnableCleanup)
            {
                _fileCleaner = new FileCleaner(Config.ArchiveFolder, Config.FilePattern, Config.CleanupIntervalMinutes, Config.ArchiveTimeToLiveHours);
                _fileCleaner.Start();
            }

        }

        public void Start()
        {
            EventManager.LogInfo("File Reader is called");
            _fileReader.Start();

            _retryFlashFileReader?.Start();
            _retryPortionFileReader?.Start();

        }

        public void Stop()
        {
            _stopEvent.Set();
            _fileReader.Stop();

            _retryFlashFileReader?.Stop();
            _retryPortionFileReader?.Stop();
            _fileCleaner?.Stop();
            
        }

        public List<KeyValuePair<string, string>> GetMonitorigStatus()
        {
            var monitoringStatus = new List<KeyValuePair<string, string>>();

            var utcNow = DateTime.UtcNow;
            var dbLastFlash = DateTime.MinValue;
            var dbAgeLastFlash = 0.00;
            var dbQueueCount = 0;
            var dbActiveTasks = 0;
            var dbMaxTasks = 0;
            var dbInsertRate = 0.00;
            if (_databaseWriter != null)
            {
                dbLastFlash = _databaseWriter.LastFlash;
                dbAgeLastFlash = Math.Round((utcNow - dbLastFlash).TotalSeconds, 0, MidpointRounding.AwayFromZero);
                dbQueueCount = _databaseWriter.Count;
                dbActiveTasks = _databaseWriter.ActiveCommands;
                dbMaxTasks = _databaseWriter.MaxActiveWorkItems;
                dbInsertRate = _databaseWriter.GetInsertRatePerMinute(DateTime.UtcNow);
            }
            monitoringStatus.Add(new KeyValuePair<string, string>("lastFlashWrittenToDbTimestampUtc", dbLastFlash.ToString(CultureInfo.InvariantCulture)));
            monitoringStatus.Add(new KeyValuePair<string, string>("ageLastFlashWrittenToDbSeconds", dbAgeLastFlash.ToString(CultureInfo.InvariantCulture)));
            monitoringStatus.Add(new KeyValuePair<string, string>("databaseWriterQueueCount", dbQueueCount.ToString()));
            monitoringStatus.Add(new KeyValuePair<string, string>("databaseWriterActiveTasks", dbActiveTasks.ToString()));
            monitoringStatus.Add(new KeyValuePair<string, string>("databaseWriterMaxTasks", dbMaxTasks.ToString()));
            monitoringStatus.Add(new KeyValuePair<string, string>("databaseWriterInsertRatePerMinute", dbInsertRate.ToString(CultureInfo.InvariantCulture)));

            var retryDbQueueCount = 0;
            var retryInsertRate = 0.00;
            var invalidFileCount = 0;
            var retryActiveTasks = 0;
            var retryMaxTasks = 0;
            if (_retryDatabaseWriter != null)
            {
                retryDbQueueCount = _retryDatabaseWriter.Count;
                retryInsertRate = _retryDatabaseWriter.GetInsertRatePerMinute(DateTime.UtcNow);
                invalidFileCount = _retryDatabaseWriter.GetFileCount();
                retryActiveTasks = _retryDatabaseWriter.ActiveCommands;
                retryMaxTasks = _retryDatabaseWriter.MaxActiveWorkItems;
            }
            monitoringStatus.Add(new KeyValuePair<string, string>("retryDatabaseWriterQueueCount", retryDbQueueCount.ToString()));
            monitoringStatus.Add(new KeyValuePair<string, string>("retryDatabaseWriterInsertRatePerMinute", retryInsertRate.ToString(CultureInfo.InvariantCulture)));
            monitoringStatus.Add(new KeyValuePair<string, string>("invalidFileCount", invalidFileCount.ToString(CultureInfo.InvariantCulture)));
            monitoringStatus.Add(new KeyValuePair<string, string>("retryDatabaseWriterActiveTasks", retryActiveTasks.ToString(CultureInfo.InvariantCulture)));
            monitoringStatus.Add(new KeyValuePair<string, string>("retryDatabaseWriterMaxTasks", retryMaxTasks.ToString(CultureInfo.InvariantCulture)));



            var lastFileTimestamp = DateTime.MinValue;
            var lastArchivedFileTimestamp = DateTime.MinValue;
            var ageProcessed = 0.00;
            var ageArchived = 0.00;
            var pickupFileCount = 0;
            if (_fileReader != null)
            {
                lastFileTimestamp = _fileReader.LastFileProcessed;
                lastArchivedFileTimestamp = _fileReader.LastFileArchived;
                ageProcessed = Math.Round((utcNow - _fileReader.LastFileProcessed).TotalSeconds, 0, MidpointRounding.AwayFromZero);
                ageArchived = Math.Round((utcNow - _fileReader.LastFileArchived).TotalSeconds, 0, MidpointRounding.AwayFromZero);
                pickupFileCount = _fileReader.GetFileCount();
            }
            monitoringStatus.Add(new KeyValuePair<string, string>("lastFileProcessedTimestampUtc", lastFileTimestamp.ToString(CultureInfo.InvariantCulture)));
            monitoringStatus.Add(new KeyValuePair<string, string>("lastFileArchivedTimestampUtc", lastArchivedFileTimestamp.ToString(CultureInfo.InvariantCulture)));
            monitoringStatus.Add(new KeyValuePair<string, string>("ageLastFileReadSeconds", ageProcessed.ToString(CultureInfo.InvariantCulture)));
            monitoringStatus.Add(new KeyValuePair<string, string>("ageLastFileArchivedSeconds", ageArchived.ToString(CultureInfo.InvariantCulture)));
            monitoringStatus.Add(new KeyValuePair<string, string>("pickupFileCount", pickupFileCount.ToString(CultureInfo.InvariantCulture)));




            var lastCleanedTimestamp = DateTime.MinValue;
            var ageLastClean = 0.00;
            if (_fileCleaner != null)
            {
                lastCleanedTimestamp = _fileCleaner.LastRun;
                ageLastClean = Math.Round((utcNow - lastCleanedTimestamp).TotalMinutes, 1, MidpointRounding.AwayFromZero);
            }
            monitoringStatus.Add(new KeyValuePair<string, string>("lastFileCleanupTimestamp", lastCleanedTimestamp.ToString(CultureInfo.InvariantCulture)));
            monitoringStatus.Add(new KeyValuePair<string, string>("ageLastFileCleanupMinutes", ageLastClean.ToString(CultureInfo.InvariantCulture)));


            
            var failedFileCount = (_retryFlashFileReader?.GetFileCount() ?? 0) + (_retryPortionFileReader?.GetFileCount() ?? 0);
            monitoringStatus.Add(new KeyValuePair<string, string>("failedFileCount", failedFileCount.ToString(CultureInfo.InvariantCulture)));

            return monitoringStatus;
        }
    }
}
