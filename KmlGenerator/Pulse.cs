﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KmlGenerator
{
    public enum PulseType { Cg, Ic}

    public class Pulse
    {
        public DateTime TimeStamp { get; set; }

        public string TimeString { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public PulseType Type { get; set; }

        public double Height { get; set; }

        public double Amplitude { get; set; }

        public string StrokeSolution { get; set; }

    }
}
