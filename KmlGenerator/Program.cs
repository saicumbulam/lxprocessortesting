﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace KmlGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Run(async () =>
            {
                string sql = "SELECT Lightning_Time, Lightning_Time_String, Latitude, Longitude, Stroke_Type, Height, Amplitude, Stroke_Solution "
                    + "FROM  [dbo].[LtgFlashPortions_History201602] WITH (NOLOCK) "
                    + "WHERE Lightning_Time >= '2/4/2016 13:35' "
                    + "AND Lightning_Time <= '2/4/2016 14:00' "
                    + "AND Latitude >= 25.655 "
                    + "AND Latitude <= 31.924 "
                    + "AND Longitude >= -91.161 "
                    + "AND Longitude <= -81.903 ";
                    //+ "AND LEFT(Stroke_Solution, 10) != '{\"ee\":null'";

                string connection = ConfigurationManager.ConnectionStrings["Lightning-Rd"].ConnectionString;
                List<Pulse> pulses = await LoadData(connection, sql);

                string fileName = string.Format("Pulses-{0}.kml", DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss"));
                
                await WritePulsesKml(pulses, fileName);

            }).Wait();

            
        }

       

        private static async Task<List<Pulse>> LoadData(string connection, string sql)
        {
            List<Pulse> pulses = null;
            using (SqlConnection conn = new SqlConnection(connection))
            {
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    cmd.CommandType = CommandType.Text;

                    try
                    {
                        await conn.OpenAsync();
                        using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                        {
                            if (reader.HasRows)
                            {
                                pulses = new List<Pulse>();
                                while (await reader.ReadAsync())
                                {
                                    Pulse p = ParsePulsesRow(reader);
                                    if (p != null) pulses.Add(p);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error: " + ex.Message);
                    }
                }
            }

            return pulses;
        }

        private static Pulse ParsePulsesRow(SqlDataReader reader)
        {
            return new Pulse
            {
                TimeStamp = DateTime.SpecifyKind(reader.GetFieldValue<DateTime>(0), DateTimeKind.Utc),
                TimeString = reader.GetFieldValue<string>(1),
                Latitude = reader.GetFieldValue<double>(2),
                Longitude = reader.GetFieldValue<double>(3),
                Type = reader.GetFieldValue<int>(4) == 0 ? PulseType.Cg : PulseType.Ic,
                Height = reader.GetFieldValue<double>(5),
                Amplitude = reader.GetFieldValue<double>(6),
                StrokeSolution = reader.GetFieldValue<string>(7)
            };
        }

        private static async Task WritePulsesKml(List<Pulse> pulses, string fileName)
        {
            using (FileStream fileStream = new FileStream(fileName, FileMode.Create, FileAccess.Write))
            {
                XmlWriterSettings settings = new XmlWriterSettings {Async = true, Indent = true, Encoding = Encoding.UTF8};
                using (XmlWriter xmlWriter = XmlWriter.Create(fileStream, settings))
                {
                    await WriteHeaderAsync(xmlWriter);
                    await WriteStylesAsync(xmlWriter);
                    foreach (Pulse pulse in pulses)
                    {
                        await WritePulseAsync(xmlWriter, pulse);
                    }
                    await WriteEndHeaderAsync(xmlWriter);
                }
            }   
        }

        private static async Task WritePulseAsync(XmlWriter xmlWriter, Pulse pulse)
        {
            await xmlWriter.WriteStartElementAsync(null, "Placemark", null);

            await xmlWriter.WriteElementStringAsync(null, "description", null, string.Format("{0} ({1}, {2})", pulse.TimeString, pulse.Latitude, pulse.Longitude));

            await xmlWriter.WriteStartElementAsync(null, "TimeStamp", null);

            await xmlWriter.WriteElementStringAsync(null, "when", null, pulse.TimeStamp.ToString("yyyy-MM-ddTHH:mm:ssZ"));

            await xmlWriter.WriteEndElementAsync();//</TimeStamp>

            await xmlWriter.WriteElementStringAsync(null, "styleUrl", null, pulse.Type == PulseType.Ic ? "ic" : "cg");

            await xmlWriter.WriteStartElementAsync(null, "Point", null);

            await xmlWriter.WriteElementStringAsync(null, "coordinates", null, string.Format("{0},{1},0", pulse.Longitude, pulse.Latitude));

            await xmlWriter.WriteEndElementAsync();//</Point>

            await xmlWriter.WriteEndElementAsync();//</Placemark>
        }

        private static async Task WriteStylesAsync(XmlWriter xmlWriter)
        {
            await xmlWriter.WriteStartElementAsync(null, "Style", null);
            await xmlWriter.WriteAttributeStringAsync(null, "id", null, "cg");
            await xmlWriter.WriteStartElementAsync(null, "IconStyle", null);
            await xmlWriter.WriteStartElementAsync(null, "Icon", null);
            await xmlWriter.WriteElementStringAsync(null, "href", null, "files/ltg-cg.png");
            await xmlWriter.WriteEndElementAsync(); 
            await xmlWriter.WriteEndElementAsync(); 
            await xmlWriter.WriteEndElementAsync();
            await xmlWriter.WriteStartElementAsync(null, "Style", null);
            await xmlWriter.WriteAttributeStringAsync(null, "id", null, "ic");
            await xmlWriter.WriteStartElementAsync(null, "IconStyle", null);
            await xmlWriter.WriteStartElementAsync(null, "Icon", null);
            await xmlWriter.WriteElementStringAsync(null, "href", null, "files/ltg-ic.png");
            await xmlWriter.WriteEndElementAsync();
            await xmlWriter.WriteEndElementAsync();
            await xmlWriter.WriteEndElementAsync();
        }

        private static async Task WriteHeaderAsync(XmlWriter xmlWriter)
        {
            await xmlWriter.WriteStartDocumentAsync();
            await xmlWriter.WriteStartElementAsync(null, "kml", "http://www.opengis.net/kml/2.2");
            await xmlWriter.WriteStartElementAsync(null, "Document", null);
            await xmlWriter.WriteElementStringAsync(null, "name", null, "EnTln Pulses");
        }

        private static async Task WriteEndHeaderAsync(XmlWriter xmlWriter)
        {
            await xmlWriter.WriteEndElementAsync(); //Document
            await xmlWriter.WriteEndElementAsync(); // KML
            await xmlWriter.WriteEndDocumentAsync();
        }
    }
}
