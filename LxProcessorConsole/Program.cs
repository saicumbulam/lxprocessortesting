﻿using System;
using System.Runtime;
using System.Threading;

namespace LxProcessorConsole
{
    class Program
    {
        private static void Main(string[] args)
        {
            //InitThreadPool();
            //GCSettings.LatencyMode = GCLatencyMode.SustainedLowLatency;
            LxProcessor.Program.Start();
            Console.WriteLine("Press \'q\' to quit the program.");
            while (Console.Read() != 'q') { }

            LxProcessor.Program.Stop();
        }



        //private const int MinimumClrThreads = 1;   //use this value to control the threads 
        //private const int MaximumClrThreads = 1;   //use this value to control the threads 

    }
}
