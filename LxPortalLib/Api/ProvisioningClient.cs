﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Aws.Core.Utilities;
using LxPortalLib.Models;

namespace LxPortalLib.Api
{
    public class ProvisioningClient
    {
        private const ushort EventManagerOffset = 10000;

        private static readonly WebRequestHandler Handler = new WebRequestHandler
        {
            ReadWriteTimeout = Config.ApiReadWriteTimeout
        };

        public static bool Delete(string id)
        {
            bool isSuccess = false;
            if (!string.IsNullOrEmpty(id))
            {
                HttpClient client = new HttpClient(Handler);
                client.BaseAddress = Config.ApiBaseAddress;
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                try
                {
                    Task<HttpResponseMessage> resp = client.DeleteAsync(string.Format("api/provisioning/Partner/{0}", id));
                    resp.Wait(Config.ApiReadWriteTimeout);
                    if (resp.IsCompleted && resp.Result.StatusCode == HttpStatusCode.NoContent)
                    {
                        isSuccess = true;
                    }
                }
                catch (Exception ex)
                {
                    EventManager.LogError(EventManagerOffset,
                                            string.Format("Unable to delete partner {0}. Reason: {1}", id, ex.Message));
                }
            }
            return isSuccess;
        }

        public static ProvisioningPartner Post(ProvisioningPartner provisioningPartner)
        {
            ProvisioningPartner result = null;

            PartnerRecord partner = new PartnerRecord
            {
                PartnerName = provisioningPartner.PartnerName,
                IsActive = provisioningPartner.IsActive,
                BoundingArea = FormatBoundingArea(provisioningPartner.BoundingArea),
                Metadata = provisioningPartner.Metadata,
                FeedType = provisioningPartner.FeedTypeList
            };

            SetPartner(partner, provisioningPartner.PackageName, provisioningPartner.FormatType);

            HttpClient client = new HttpClient(Handler);
            client.BaseAddress = Config.ApiBaseAddress;
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            try
            {
                Task<HttpResponseMessage> resp = client.PostAsJsonAsync(string.Format("api/provisioning/Partner"), partner);
                resp.Wait(Config.ApiReadWriteTimeout);
                if (resp.IsCompleted && resp.Result.StatusCode == HttpStatusCode.OK)
                {
                    provisioningPartner.PartnerId = resp.Result.Content.ReadAsAsync<ResponseContainer<string>>().Result.r;
                    result = provisioningPartner;
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventManagerOffset, string.Format("Unable to add partner {0}. Reason: {1}", partner.PartnerId, ex.Message));
            }
            return result;
        }

        public static ProvisioningPartner Put(ProvisioningPartner provisioningPartner)
        {
            ProvisioningPartner result = null;

            PartnerRecord partner = new PartnerRecord
            {
                PartnerId = provisioningPartner.PartnerId,
                PartnerName = provisioningPartner.PartnerName,
                IsActive = provisioningPartner.IsActive,
                BoundingArea = FormatBoundingArea(provisioningPartner.BoundingArea),
                Metadata = provisioningPartner.Metadata,
                IpAddress = provisioningPartner.IpAddress
                //FeedType = provisioningPartner.FeedTypeList
            };

            SetPartner(partner, provisioningPartner.PackageName, provisioningPartner.FormatType);

            if (!string.IsNullOrWhiteSpace(partner.PartnerId))
            {
                HttpClient client = new HttpClient(Handler);
                client.BaseAddress = Config.ApiBaseAddress;
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                try
                {
                    Task<HttpResponseMessage> resp = client.PutAsJsonAsync(string.Format("api/provisioning/Partner"), partner);
                    resp.Wait(Config.ApiReadWriteTimeout);
                    if (resp.IsCompleted && resp.Result.StatusCode == HttpStatusCode.OK)
                    {
                        result = provisioningPartner;
                    }
                }
                catch (Exception ex)
                {
                    EventManager.LogError(EventManagerOffset, string.Format("Unable to update partner {0}. Reason: {1}", partner.PartnerId, ex.Message));
                }
            }
            return result;
        }

        private static void SetPartner(PartnerRecord partner, Provisioning.PackageName packageName, Provisioning.FormatType formatType)
        {
            if (packageName == Provisioning.PackageName.TlnCg)
            {
                partner.NetworkTypeId = 1;
                partner.IsCloudToGroundEnabled = true;
                partner.IsInCloudEnabled = false;
            }
            else if (packageName == Provisioning.PackageName.GlnCg)
            {
                partner.NetworkTypeId = 2;
                partner.IsCloudToGroundEnabled = true;
                partner.IsInCloudEnabled = false;
            }
            if (packageName == Provisioning.PackageName.TlnCgIc)
            {
                partner.NetworkTypeId = 1;
                partner.IsCloudToGroundEnabled = true;
                partner.IsInCloudEnabled = true;
            }
            else if (packageName == Provisioning.PackageName.GlnCgIc) //GlnCgIc
            {
                partner.NetworkTypeId = 2;
                partner.IsCloudToGroundEnabled = true;
                partner.IsInCloudEnabled = true;
            }

            if (formatType == Provisioning.FormatType.AsciiFlash)
            {
                partner.ResponseFormatTypeId = 1;
                partner.ResponsePackageTypeId = 1;
            }
            else if (formatType == Provisioning.FormatType.AsciiPulse)
            {
                partner.ResponseFormatTypeId = 1;
                partner.ResponsePackageTypeId = 2;
            }
            else if (formatType == Provisioning.FormatType.BinaryFlash)
            {
                partner.ResponseFormatTypeId = 2;
                partner.ResponsePackageTypeId = 1;
            }
            else if (formatType == Provisioning.FormatType.BinaryPulse)
            {
                partner.ResponseFormatTypeId = 2;
                partner.ResponsePackageTypeId = 2;
            }
            else // BinaryCombo
            {
                partner.ResponseFormatTypeId = 2;
                partner.ResponsePackageTypeId = 3;
            }
        }

        public static ResponseContainer<List<ProvisioningPartner>> GetAll(Provisioning.FeedType feedType)
        {
            ResponseContainer<List<PartnerRecord>> responseContainer = null;
            ResponseContainer<List<ProvisioningPartner>> result = new ResponseContainer<List<ProvisioningPartner>>();

            HttpClient client = new HttpClient(Handler);
            client.BaseAddress = Config.ApiBaseAddress;
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            try
            {
                int fType;
                if (feedType == Provisioning.FeedType.LxDetectionFeed)
                {
                    fType = 1;
                }
                else 
                {
                    fType = 2; 
                }

                HttpResponseMessage resp = client.GetAsync(string.Format("api/provisioning/Partner?feedType={0}", fType)).Result;
                if (resp.IsSuccessStatusCode)
                {
                    responseContainer = resp.Content.ReadAsAsync<ResponseContainer<List<PartnerRecord>>>().Result;
                    result.i = responseContainer.i;
                    result.c = responseContainer.c;
                    result.e = responseContainer.e;

                    if (responseContainer.r != null)
                    {
                        List<ProvisioningPartner> provisioningPartners = new List<ProvisioningPartner>();
                        foreach (var partnerRecord in responseContainer.r)
                        {
                            ProvisioningPartner provisioningPartner = new ProvisioningPartner(partnerRecord);
                            provisioningPartners.Add(provisioningPartner);
                        }
                        result.r = provisioningPartners;
                    }
                }
                if (responseContainer == null)
                {
                    result = new ResponseContainer<List<ProvisioningPartner>>();
                    result.i = Guid.NewGuid().ToString();
                    result.c = 500;
                    result.e = "Failed to get list of Partners.";
                    result.r = null;
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventManagerOffset, "Unable to get partner list" + ex.Message);
                result = new ResponseContainer<List<ProvisioningPartner>>();
                result.i = Guid.NewGuid().ToString();
                result.c = 500;
                result.e = "Unable to get partner list" + ex.Message;
                result.r = null;
            }

            return result;
        }

        public static ResponseContainer<ProvisioningPartner> Get(string id)
        {
            ResponseContainer<PartnerRecord> responseContainer = null;
            ResponseContainer<ProvisioningPartner> result = new ResponseContainer<ProvisioningPartner>();

            if (!string.IsNullOrWhiteSpace(id))
            {
                HttpClient client = new HttpClient(Handler);
                client.BaseAddress = Config.ApiBaseAddress;
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                try
                {
                    HttpResponseMessage resp = client.GetAsync(string.Format("api/provisioning/Partner?partnerId={0}", id)).Result;
                    if (resp.IsSuccessStatusCode)
                    {
                        responseContainer = resp.Content.ReadAsAsync<ResponseContainer<PartnerRecord>>().Result;
                        result.i = responseContainer.i;
                        result.c = responseContainer.c;
                        result.e = responseContainer.e;

                        if (responseContainer.r != null)
                        {
                            ProvisioningPartner provisioningPartner = new ProvisioningPartner(responseContainer.r);
                            result.r = provisioningPartner;
                        }
                    }
                    if (responseContainer == null)
                    {
                        result = new ResponseContainer<ProvisioningPartner>();
                        result.i = Guid.NewGuid().ToString();
                        result.c = 500;
                        result.e = string.Format("Failed to get partner {0}.", id);
                        result.r = null;
                    }
                }
                catch (Exception ex)
                {
                    EventManager.LogError(EventManagerOffset, string.Format("Unable to get partner {0}. Reason: {1}", id, ex.Message));
                    result = new ResponseContainer<ProvisioningPartner>();
                    result.i = Guid.NewGuid().ToString();
                    result.c = 500;
                    result.e = string.Format("Unable to get partner {0}. Reason: {1}", id, ex.Message);
                    result.r = null;
                }
            }

            return result;
        }

        public static ResponseContainer<List<EventLogRecord>> GetEventLog(string id, int epochStart = -1, int epochEnd = -1)
        {
            ResponseContainer<List<EventLogRecord>> responseContainer = new ResponseContainer<List<EventLogRecord>>();

            if (!string.IsNullOrWhiteSpace(id))
            {
                HttpClient client = new HttpClient(Handler);
                client.BaseAddress = Config.ApiBaseAddress;
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                try
                {
                    HttpResponseMessage resp;
                    if (epochStart == -1 || epochEnd == -1)
                    {
                        resp = client.GetAsync(string.Format("api/provisioning/EventLog?partnerId={0}", id)).Result;
                    }
                    else
                    {
                        resp = client.GetAsync(string.Format("api/provisioning/EventLog?partnerId={0}&epochStart={1}&epochEnd={2}", id, epochStart, epochEnd)).Result;
                    }
                    if (resp.IsSuccessStatusCode)
                        responseContainer = resp.Content.ReadAsAsync<ResponseContainer<List<EventLogRecord>>>().Result;
                }
                catch (Exception ex)
                {
                    EventManager.LogError(EventManagerOffset, string.Format("Unable to get event log list for partner {0}. Reason: {1}", id, ex.Message));
                }
            }

            return responseContainer;
        }

        private static string FormatBoundingArea(List<double> boundingArea)
        {
            string result = null;

            if (boundingArea.Count == 4)
            {
                double n = boundingArea[0];
                double w = boundingArea[1];
                double s = boundingArea[2];
                double e = boundingArea[3];

                result = string.Format("{0},{1};{2},{3}", n, w, s, e); // "n,w;s,e"
            }

            return result;
        }
    }
}
