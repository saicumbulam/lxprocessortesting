﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using Aws.Core.Utilities;
using LxPortalLib.Models;

namespace LxPortalLib.Api
{
    public class StationNetworkClient
    {
        private const ushort EventManagerOffset = 3000;

        private static readonly WebRequestHandler Handler = new WebRequestHandler
        {
            ReadWriteTimeout = Config.ApiReadWriteTimeout
        };

        public static ResponseContainer<List<Network>> GetStationNetworks()
        {
            ResponseContainer<List<Network>> result = null;

            HttpClient client = new HttpClient(Handler);
            client.BaseAddress = Config.ApiBaseAddress;
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            try
            {
                HttpResponseMessage resp = client.GetAsync("api/stationnetworks").Result;
                if (resp.IsSuccessStatusCode)
                {
                    result = resp.Content.ReadAsAsync<ResponseContainer<List<Network>>>().Result;
                }
                if (result == null)
                {
                    result = new ResponseContainer<List<Network>>();
                    result.i = Guid.NewGuid().ToString();
                    result.c = (int) resp.StatusCode;
                    result.e = "Failed to get list of station networks.";
                    result.r = null;
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventManagerOffset, "Unable to get station networks" + ex.Message);
                result = new ResponseContainer<List<Network>>();
                result.i = Guid.NewGuid().ToString();
                result.c = 500;
                result.e = "Unable to get station networks. " + ex.Message;
                result.r = null;
            }

            return result;
        }
    }
}
