﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using Aws.Core.Utilities;
using LxPortalLib.Models;
using LxPortalLib.Common;

namespace LxPortalLib.Api
{
    public class StormClient
    {
        private const ushort EventManagerOffset = 10000;

        private static readonly WebRequestHandler Handler = new WebRequestHandler
        {
            ReadWriteTimeout = Config.ApiReadWriteTimeout
        };

        //obselete TLS/TLN Release 2.0
        public static ResponseContainer<List<StormRecord>> GetBoxStormData(DateTime start, DateTime end, decimal ullat, decimal ullon,
                                                        decimal lrlat, decimal lrlon)
        {
            ResponseContainer<List<StormRecord>> responseContainer = new ResponseContainer<List<StormRecord>>();

            if (start < end && ullat > lrlat && ullon < lrlon)
            {
                HttpClient client = new HttpClient(Handler);
                client.BaseAddress = Config.ApiBaseAddress;
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                try
                {
                    string baseurl = string.Empty; 
                    if (Config.SystemType.ToUpper() == "TLN")
                    {
                        baseurl = string.Format("api/v2/storm");
                    }
                    else
                    {
                        baseurl = string.Format("api/storm");
                    }

                    string parameters =
                        string.Format("?start={0}&end={1}&ullat={2}&ullon={3}&lrlat={4}&lrlon={5}",
                                      Uri.EscapeUriString(start.ToString("s")),
                                      Uri.EscapeUriString(end.ToString("s")),
                                      Uri.EscapeUriString(ullat.ToString(CultureInfo.InvariantCulture)),
                                      Uri.EscapeUriString(ullon.ToString(CultureInfo.InvariantCulture)),
                                      Uri.EscapeUriString(lrlat.ToString(CultureInfo.InvariantCulture)),
                                      Uri.EscapeUriString(lrlon.ToString(CultureInfo.InvariantCulture)));

                    string sturl = baseurl + parameters;

                    HttpResponseMessage resp = client.GetAsync(sturl).Result;
                    if (resp.IsSuccessStatusCode)
                    {
                        if (Config.SystemType.ToUpper() == "TLN")
                        {
                            responseContainer = resp.Content.ReadAsAsync<ResponseContainer<List<StormRecord>>>().Result;
                        }
                        else
                        {
                            responseContainer.c = (int)resp.StatusCode;
                            responseContainer.r = resp.Content.ReadAsAsync<List<StormRecord>>().Result;
                        }
                    }
                    else if (resp.StatusCode < HttpStatusCode.InternalServerError)
                    {
                        responseContainer.c = (int)resp.StatusCode;
                        responseContainer.r = new List<StormRecord>();
                        responseContainer.e = "Failed to get Storm report data. ";
                    }
                    else
                    {
                        responseContainer.c = (int)resp.StatusCode;
                        responseContainer.r = new List<StormRecord>();
                        responseContainer.e = "Failed to get Storm report data. ";
                    }
                }
                catch (Exception ex)
                {
                    responseContainer.c = 500;
                    responseContainer.e = "Error occured in inner call: " + ex;
                    EventManager.LogError(EventManagerOffset, "Unable to get list of Storm Data" + ex.Message);
                }

            }
            return responseContainer;
        }


        //using new Pulse LSR
        public static List<LocalStormRecord> GetBoxPulseStormData(DateTime start, DateTime end, decimal ullat, decimal ullon, decimal lrlat, decimal lrlon)
        {
            List<LocalStormRecord> lsrData = new List<LocalStormRecord>();
            StormReport getLsrData = new StormReport();

            HttpClient client = new HttpClient(Handler);
            client.BaseAddress = Config.EarthNetworksPulse; 
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            try
            {
                string hmacAuthUrl = GetAuthenticatedLsrUrl(start, end, ullat, ullon, lrlat, lrlon);

                HttpResponseMessage resp = client.GetAsync(hmacAuthUrl).Result;
                if (resp.IsSuccessStatusCode)
                    getLsrData = resp.Content.ReadAsAsync<StormReport>().Result;
                    lsrData = getLsrData.Result; 
            }
            catch(Exception ex)
            {
                //log error 
            }

            return lsrData;
        }


        private static string GetAuthenticatedLsrUrl(DateTime start, DateTime end, decimal ullat, decimal ullon, decimal lrlat, decimal lrlon)
        {
            string lsrUrl = string.Empty;

            //convert start/end time to epoch 
            long epochStartTime = Utilities.ConvertToEpoch(start);
            long epochEndTime = Utilities.ConvertToEpoch(end);

            string evenTypes = "hail,tstm%20wnd%20gst,tornado,tstm%20wnd%20dmg,water%20spout"; //hardcoded need to talk to PC about list of eventypes 

            var queryParameters = new Dictionary<string, string>
            {
                {"startDateTimeUtc", epochStartTime.ToString()},
                {"endDateTimeUtc", epochEndTime.ToString()},
                {"topLeftLatitude", ullat.ToString()},
                {"topLeftLongitude", ullon.ToString()},
                {"bottomRightLatitude", lrlat.ToString()},
                {"bottomRightLongitude", lrlon.ToString()},
                {"eventTypes", evenTypes},
                {"verbose", "true"},
            };

            //build the string 
            StringBuilder uriSb = new StringBuilder();
            uriSb.Append(Config.EarthNetworksPulse + "data/lsr/v1/query");
            uriSb.Append("?");
            bool first = true;
            foreach (var kvp in queryParameters)
            {
                if (first)
                {
                    first = false;
                }
                else
                {
                    uriSb.Append('&');
                }
                uriSb.Append(HttpUtility.UrlEncode(kvp.Key));
                uriSb.Append('=');
                uriSb.Append(HttpUtility.UrlEncode(kvp.Value));
            }

            //get hmac auth uri 
            lsrUrl = HmacAuthentication.AddHmacAuth(Config.PulseGuidSecretKey,
                                             new Uri(uriSb.ToString()),
                                             Config.PulseKey);


            return lsrUrl;
        }

    }
}
