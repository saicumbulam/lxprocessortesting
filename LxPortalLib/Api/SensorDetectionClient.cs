﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Aws.Core.Utilities;
using LxPortalLib.Common;
using LxPortalLib.Models;

namespace LxPortalLib.Api
{
    public class SensorDetectionClient
    {
        private const ushort EventManagerOffset = 3000;



        private static readonly WebRequestHandler Handler = new WebRequestHandler
        {
            ReadWriteTimeout = Config.ApiReadWriteTimeout 
        }; 


        public static SensorDetection Get(string id, DateTime? start, DateTime? end)
        {
            SensorDetection dtc = null; 
            if (!string.IsNullOrWhiteSpace(id))
            {
                HttpClient client = new HttpClient(Handler); 
                client.BaseAddress = Config.ApiBaseAddress; 

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                try
                {
                    string url = string.Format("api/sensordetection/{0}", id);
                    if (start.HasValue || end.HasValue)
                    {
                        if (start.HasValue)
                            start = Utilities.NormalizeDateTimeToUtc(start.Value);

                        if (end.HasValue)
                            end = Utilities.NormalizeDateTimeToUtc(end.Value);


                       
                        string parameters = string.Format("?start={0}&end={1}",
                                                          (start.HasValue)
                                                              ? Uri.EscapeUriString(start.Value.ToString("s"))
                                                              : string.Empty,
                                                          (end.HasValue)
                                                              ? Uri.EscapeUriString(end.Value.ToString("s"))
                                                              : string.Empty);

                        url = url + parameters;
                    }
                    HttpResponseMessage resp = client.GetAsync(url).Result;
                    if (resp.IsSuccessStatusCode)
                        dtc = resp.Content.ReadAsAsync<SensorDetection>().Result;

                    else if(resp.StatusCode < HttpStatusCode.InternalServerError)
                    {
                        dtc = new SensorDetection {Id = id}; 
                    }
                    else
                    {
                        dtc = null; 
                    }
                }
                catch (Exception ex)
                {
                    EventManager.LogError(EventManagerOffset, "Unable to get list of sensor detections" + ex.Message);
                    dtc = null; 
                }
            }
            return dtc; 
        }

    }
}
