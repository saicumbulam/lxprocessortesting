﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Aws.Core.Utilities;
using LxPortalLib.Models;

namespace LxPortalLib.Api
{
    public class SensorStatusClient
    {
        private const ushort EventManagerOffset = 3000;

        private static readonly WebRequestHandler Handler = new WebRequestHandler
        {
            ReadWriteTimeout = Config.ApiReadWriteTimeout
        };

        /// <summary>
        /// To get the most recent sensor status for a particular sensor
        /// </summary>
        /// <param name="id">sensor id</param>
        /// <returns>The return object will contain the sensor status. A null object will be returned if there is a failure</returns>
        public static CurrentSensorStatus Get(string id)
        {
            CurrentSensorStatus status = null;
            if (!string.IsNullOrWhiteSpace(id))
            {
                HttpClient client = new HttpClient(Handler);
                client.BaseAddress = Config.ApiBaseAddress;

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                try
                {
                    HttpResponseMessage resp = client.GetAsync(string.Format("api/currentsensorstatus/{0}", id)).Result;
                    if (resp.IsSuccessStatusCode)
                        status = resp.Content.ReadAsAsync<CurrentSensorStatus>().Result;

                    else if (resp.StatusCode < HttpStatusCode.InternalServerError)
                    {
                        status = new CurrentSensorStatus {Id = id};
                    }
                    else
                    {
                        resp = null; 
                    }
                }
                catch (Exception ex)
                {
                    EventManager.LogError(EventManagerOffset, "Unable to get current sensor status" + ex.Message);
                    status = null;
                }
            }

            return status;
        }
    }
}
