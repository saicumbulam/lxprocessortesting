﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Collections.Generic; 

using Aws.Core.Utilities;
using LxPortal.Lib.Common; 
using LxPortal.Lib.Models; 

namespace LxPortal.Lib.Api
{
    public class SensorFlashQueryClient
    {
        private const ushort EventManagerOffset = 3000;  
        private static readonly WebRequestHandler Handler = new WebRequestHandler
        {
            ReadWriteTimeout = Config.ApiReadWriteTimeout
        }; 
        
        public static List<FlashRecord> Get(DateTime? start, DateTime? end, StrokeType? type, decimal? ullat, decimal? ullon, decimal? lrlat, decimal? lrlon)
        {
            List<FlashRecord> flash = null; //return a null if failure
            if (start.HasValue || end.HasValue || type.HasValue || ullat.HasValue || ullon.HasValue || lrlat.HasValue || lrlon.HasValue)// all params can't be empty for valid request
            {
                try
                {
                    HttpClient client = new HttpClient(Handler);
                    client.BaseAddress = Config.ApiBaseAddress; 

                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));


                    string url = string.Format("api/flashquery");
                    if (start.HasValue)
                        start = Utilities.NormalizeDateTimeToUtc(start.Value);

                    if (end.HasValue)
                        end = Utilities.NormalizeDateTimeToUtc(end.Value); //check dates if they're utc format

                    //concatenate parameters 
                    string parameters =
                        string.Format("?start={0}&end={1}&type={2}", //"&ullat={3}&ullon={4}&lrlat={5}&lrlon={6}", 
                        (start.HasValue) ? Uri.EscapeUriString(start.Value.ToString("s")) : string.Empty, 
                        (end.HasValue) ? Uri.EscapeUriString(end.Value.ToString("s")): string.Empty, 
                        (type.HasValue) ? Enum.Parse(typeof(StrokeType), type.ToString()): string.Empty);

                    url = url + parameters;

                    HttpResponseMessage resp = client.GetAsync(url).Result;
                    if (resp.IsSuccessStatusCode)
                        //flash = null; 
                    flash = resp.Content.ReadAsAsync<FlashRecord>().Result; 

                    else if (resp.StatusCode < HttpStatusCode.InternalServerError)
                    {
                        flash = new List<FlashRecord>() { };
                    }
                    else
                    {
                        flash = null;
                    }

                }
                catch (Exception ex)
                {
                    EventManager.LogError(EventManagerOffset, "Unable to get flash query list" + ex.Message); 
                }
            }
            return flash; 
        }

    }
}
