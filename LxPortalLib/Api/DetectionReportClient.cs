﻿using System;
using System.Globalization;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Collections.Generic;
using Aws.Core.Utilities;
using LxPortalLib.Common;
using LxPortalLib.Models;

namespace LxPortalLib.Api
{
    public class DetectionReportClient 
    {
        private const ushort EventManagerOffset = 10000;

        private static readonly WebRequestHandler Handler = new WebRequestHandler
        {
            ReadWriteTimeout = Config.ApiReadWriteTimeout
        };

        //Summer 2014 TLN updates: 
        //1. API v2 (i.e. api/v2/{controller}) - method signatures changed and uses ResponseContainer<T> 
        //2. lxVersion param added - enables UI to use different endpoints to compare detection data 

        public static ResponseContainer<List<FlashRecord>> GetCircleDetectionsInMiles(DateTime start, DateTime end, string type, decimal lat, decimal lon, int radiusMiles, LayerType layer, string lxVersion)
        {
            int radiusMeters = Utilities.ConvertMilesToMeters(radiusMiles);
            return GetCircleDetections(start, end, type, lat, lon, radiusMeters, layer, lxVersion);
        }

        public static ResponseContainer<List<FlashRecord>> GetCircleDetections(DateTime start, DateTime end, string type, decimal lat, decimal lon, int radius, LayerType layer, string lxVersion)
        {
            ResponseContainer<List<FlashRecord>> responseContainer = new ResponseContainer<List<FlashRecord>> { i = Guid.NewGuid().ToString(), r = null };

            HttpClient client = new HttpClient(Handler);
            if (!string.IsNullOrEmpty(lxVersion))
            {
                client.BaseAddress = (lxVersion != "base") ? Config.ApiCompareToBaseAddress : Config.ApiBaseAddress;
            }
            else
            {
                client.BaseAddress = Config.ApiBaseAddress;
            }
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            
            try
            {
                //use latest version 
                string baseurl = string.Empty; 
                if (Config.SystemType.ToUpper() == "TLN")
                {
                    baseurl = string.Format(((int)layer == 1) ? "api/v2/flash" : "api/v2/flashportion");
                }
                else
                {
                    baseurl = string.Format(((int)layer == 1) ? "api/flash" : "api/flashportion");
                }

                start = Utilities.NormalizeDateTimeToUtc(start);
                end = Utilities.NormalizeDateTimeToUtc(end);
                string strokeType = string.Empty;
                string parameters = null;

                if (type != "both")
                {
                    strokeType = (type == "ic") ? ((int)StrokeType.IntraCloud).ToString(CultureInfo.InvariantCulture)
                                            : ((int)StrokeType.CloudToGround).ToString(CultureInfo.InvariantCulture);

                    parameters = string.Format("?start={0}&end={1}&lat={2}&lon={3}&type={4}&radius={5}",
                           Uri.EscapeUriString(start.ToString("s")),
                           Uri.EscapeUriString(end.ToString("s")),
                           Uri.EscapeUriString((lat.ToString(CultureInfo.InvariantCulture))),
                           Uri.EscapeUriString((lon.ToString(CultureInfo.InvariantCulture))),
                           Uri.EscapeUriString(strokeType),
                           Uri.EscapeUriString((radius.ToString(CultureInfo.InvariantCulture))));
                }
                else
                {
                    parameters = string.Format("?start={0}&end={1}&lat={2}&lon={3}&type={4}&radius={5}",
                           Uri.EscapeUriString(start.ToString("s")),
                           Uri.EscapeUriString(end.ToString("s")),
                           Uri.EscapeUriString((lat.ToString(CultureInfo.InvariantCulture))),
                           Uri.EscapeUriString((lon.ToString(CultureInfo.InvariantCulture))),
                           Uri.EscapeUriString(((int)StrokeType.None).ToString(CultureInfo.InvariantCulture)),
                           Uri.EscapeUriString((radius.ToString(CultureInfo.InvariantCulture))));
                }



                string cgurl = baseurl + parameters;

                HttpResponseMessage resp = client.GetAsync(cgurl).Result;

                if (resp.IsSuccessStatusCode)
                {
                    if (Config.SystemType.ToUpper() == "TLN")
                    {
                        responseContainer = resp.Content.ReadAsAsync<ResponseContainer<List<FlashRecord>>>().Result;
                    }
                    else
                    {
                        responseContainer.c = (int)resp.StatusCode;
                        responseContainer.r = resp.Content.ReadAsAsync<List<FlashRecord>>().Result;
                    }
                }
                else if (resp.StatusCode < HttpStatusCode.InternalServerError)
                {
                    responseContainer = resp.Content.ReadAsAsync<ResponseContainer<List<FlashRecord>>>().Result;
                }
                else
                {
                    responseContainer.c = (int)resp.StatusCode;
                    responseContainer.r = new List<FlashRecord>();
                    responseContainer.e = "Failed to get Detection data.";
                }

            }
            catch (Exception ex)
            {
                EventManager.LogError(EventManagerOffset, "Unable to get flash query list" + ex.Message);
            }
            return responseContainer;
        }

        public static ResponseContainer<List<FlashRecord>> GetBoxDetections(DateTime start, DateTime end, string type, decimal ullat, decimal ullon, decimal lrlat, decimal lrlon, LayerType layer, string lxVersion)
        {
            ResponseContainer<List<FlashRecord>> responseContainer = new ResponseContainer<List<FlashRecord>> { i = Guid.NewGuid().ToString(), r = null };

            HttpClient client = new HttpClient(Handler);
            if (!string.IsNullOrEmpty(lxVersion))
            {
                client.BaseAddress = (lxVersion != "base") ? Config.ApiCompareToBaseAddress : Config.ApiBaseAddress; 
            }
            else
            {
                client.BaseAddress = Config.ApiBaseAddress;
            }

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            try
            {
                string baseurl = string.Empty;
                if (Config.SystemType.ToUpper() == "TLN")
                {
                    baseurl = string.Format(((int)layer == 1) ? "api/v2/flash" : "api/v2/flashportion");
                }
                else
                {
                    baseurl = string.Format(((int)layer == 1) ? "api/flash" : "api/flashportion");
                }

                start = Utilities.NormalizeDateTimeToUtc(start);
                end = Utilities.NormalizeDateTimeToUtc(end);
                string strokeType = string.Empty;
                string parameters = null;

                if (type != "both")
                {
                    strokeType = (type == "ic") ? ((int)StrokeType.IntraCloud).ToString(CultureInfo.InvariantCulture)
                                                : ((int)StrokeType.CloudToGround).ToString(CultureInfo.InvariantCulture);

                    parameters =
                        string.Format("?start={0}&end={1}&ullat={2}&ullon={3}&lrlat={4}&lrlon={5}&type={6}",
                                      Uri.EscapeUriString(start.ToString("s")),
                                      Uri.EscapeUriString(end.ToString("s")),
                                      Uri.EscapeUriString((ullat.ToString(CultureInfo.InvariantCulture))),
                                      Uri.EscapeUriString((ullon.ToString(CultureInfo.InvariantCulture))),
                                      Uri.EscapeUriString((lrlat.ToString(CultureInfo.InvariantCulture))),
                                      Uri.EscapeUriString((lrlon.ToString(CultureInfo.InvariantCulture))),
                                      Uri.EscapeUriString(strokeType));
                }
                else
                {
                    parameters =
                         string.Format("?start={0}&end={1}&ullat={2}&ullon={3}&lrlat={4}&lrlon={5}&type={6}",
                                       Uri.EscapeUriString(start.ToString("s")),
                                       Uri.EscapeUriString(end.ToString("s")),
                                       Uri.EscapeUriString((ullat.ToString(CultureInfo.InvariantCulture))),
                                       Uri.EscapeUriString((ullon.ToString(CultureInfo.InvariantCulture))),
                                       Uri.EscapeUriString((lrlat.ToString(CultureInfo.InvariantCulture))),
                                       Uri.EscapeUriString((lrlon.ToString(CultureInfo.InvariantCulture))),
                                       Uri.EscapeUriString(((int)StrokeType.None).ToString(CultureInfo.InvariantCulture)));
                }

                string cgurl = baseurl + parameters;

                //HttpResponseMessage resp = client.GetAsync(cgurl).Result;
                HttpResponseMessage resp = client.GetAsync(cgurl).Result;
                if (resp.IsSuccessStatusCode)
                {
                    if (Config.SystemType.ToUpper() == "TLN")
                    {
                        responseContainer = resp.Content.ReadAsAsync<ResponseContainer<List<FlashRecord>>>().Result;
                    }
                    else
                    {
                        responseContainer.c = (int) resp.StatusCode; 
                        responseContainer.r = resp.Content.ReadAsAsync<List<FlashRecord>>().Result; 
                    }
                }
                else if (resp.StatusCode < HttpStatusCode.InternalServerError)
                {
                    responseContainer = resp.Content.ReadAsAsync<ResponseContainer<List<FlashRecord>>>().Result;
                }
                else
                {
                    responseContainer.c = (int)resp.StatusCode;
                    responseContainer.r = new List<FlashRecord>();
                    responseContainer.e = "Failed to get Detection data.";
                }
            }
            catch (Exception ex)
            {
                responseContainer.c = 500;
                responseContainer.e = "Error occured in inner call: " + ex;
                EventManager.LogError(EventManagerOffset, "Unable to get flash query list" + ex.Message);
            }
            return responseContainer;
        }
    

    }
}

