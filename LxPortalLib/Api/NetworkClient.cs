﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

using LxPortalLib.Models;

namespace LxPortalLib.Api
{
    public class NetworkClient
    {
        private static readonly WebRequestHandler Handler = new WebRequestHandler
        {
            ReadWriteTimeout = Config.ApiReadWriteTimeout
        };

        public static async Task<bool> DeleteAsync(string id, bool useLocalDb)
        {
            bool isSuccess = false;
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentException("id cannot be null or empty");
            }

            HttpClient client = new HttpClient(Handler);
            client.BaseAddress = useLocalDb ? Config.ApiBaseAddress : Config.ApiRedundantAddress;
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage resp = await client.DeleteAsync(string.Format("api/network/{0}", id));
            if (resp.IsSuccessStatusCode)
            {
                ResponseContainer<Network> rc = await resp.Content.ReadAsAsync<ResponseContainer<Network>>();
                isSuccess = rc.IsSuccessStatusCode;
                if (!isSuccess)
                {
                    throw new Exception(rc.e);
                }
            }
         
            return isSuccess;
        }

        public static async Task<Network> AddAsync(Network network, bool useLocalDb)
        {
            if (network == null)
            {
                throw new ArgumentNullException("network");
            }
            if (string.IsNullOrWhiteSpace(network.Id))
            {
                throw new ArgumentException("network.Id cannot be null or empty");
            }
            if (string.IsNullOrWhiteSpace(network.Name))
            {
                throw new ArgumentException("network.Name cannot be null or empty");
            }

            Network value = null;
            HttpClient client = new HttpClient(Handler);
            client.BaseAddress = useLocalDb ? Config.ApiBaseAddress : Config.ApiRedundantAddress;
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage resp = await client.PostAsJsonAsync("api/network", network);
            
            if (resp.IsSuccessStatusCode)
            {
                ResponseContainer<Network> rc = await resp.Content.ReadAsAsync<ResponseContainer<Network>>();
                if (rc.IsSuccessStatusCode)
                {
                    value = rc.r;
                }
                else
                {
                    throw new Exception(rc.e);
                }
            }

            return value;
        }

        public static async Task<bool> UpdateAsync(Network network, bool useLocalDb)
        {
            if (network == null)
            {
                throw new ArgumentNullException("network");
            }
            if (string.IsNullOrWhiteSpace(network.Id))
            {
                throw new ArgumentException("network.Id cannot be null or empty");
            }
            if (string.IsNullOrWhiteSpace(network.Name))
            {
                throw new ArgumentException("network.Name cannot be null or empty");
            }

            bool success = false;
            
            HttpClient client = new HttpClient(Handler);
            client.BaseAddress = useLocalDb ? Config.ApiBaseAddress : Config.ApiRedundantAddress;
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage resp = await client.PutAsJsonAsync(string.Format("api/network/{0}", network.Id), network);
            if (resp.IsSuccessStatusCode)
            {
                ResponseContainer<Network> rc = await resp.Content.ReadAsAsync<ResponseContainer<Network>>();
                if (rc.IsSuccessStatusCode)
                {
                    success = true;
                }
                else
                {
                    throw new Exception(rc.e);
                }
            }
            return success;
        }

        public static async Task<ResponseContainer<IEnumerable<Network>>> GetAll()
        {
            ResponseContainer<IEnumerable<Network>> responseContainer = null;

            HttpClient client = new HttpClient(Handler);
            client.BaseAddress = Config.ApiBaseAddress;
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage resp = await client.GetAsync("api/network");
            if (resp.IsSuccessStatusCode)
            {
                responseContainer = await resp.Content.ReadAsAsync<ResponseContainer<IEnumerable<Network>>>();
            }

            return responseContainer;
        }
    }
}
