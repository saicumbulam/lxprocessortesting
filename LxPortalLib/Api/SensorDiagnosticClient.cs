﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Aws.Core.Utilities;
using LxPortalLib.Common;
using LxPortalLib.Models;

namespace LxPortalLib.Api
{
    public class SensorDiagnosticClient
    {
        private const ushort EventManagerOffset = 3000;

        private static readonly WebRequestHandler Handler = new WebRequestHandler
        {
            ReadWriteTimeout = Config.ApiReadWriteTimeout
        };

        /// <summary>
        /// To get the most current diagnostic data for a particular sensor
        /// </summary>
        /// <param name="id">sensor id</param>
        /// <returns>Return object holding the most current diagnostic if data existed in database. If funciton will return null if there is a failure</returns>
        public static SensorDiagnostics Get(string id, DateTime? start, DateTime? end)
        {
            SensorDiagnostics diag = null;
            if (!string.IsNullOrWhiteSpace(id))
            {
                HttpClient client = new HttpClient(Handler);
                client.BaseAddress = Config.ApiBaseAddress;

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                try
                {
                    string url = string.Format("api/sensordiagnostic/{0}", id);
                    if (start.HasValue || end.HasValue)
                    {
                        if (start.HasValue)
                            start = Utilities.NormalizeDateTimeToUtc(start.Value);

                        if (end.HasValue)
                            end = Utilities.NormalizeDateTimeToUtc(end.Value);

                        string parameters = string.Format("?start={0}&end={1}",
                            (start.HasValue) ? Uri.EscapeUriString(start.Value.ToString("s")) : string.Empty,
                            (end.HasValue) ? Uri.EscapeUriString(end.Value.ToString("s")) : string.Empty);

                        url = url + parameters;
                    }
                    HttpResponseMessage resp = client.GetAsync(url).Result;
                    if (resp.IsSuccessStatusCode)
                        diag = resp.Content.ReadAsAsync<SensorDiagnostics>().Result;

                    else if (resp.StatusCode < HttpStatusCode.InternalServerError)
                    {
                        diag = new SensorDiagnostics { Id = id};
                    }
                    else
                    {
                        diag = null; 
                    }
                }
                catch (Exception ex)
                {
                    EventManager.LogError(EventManagerOffset, "Unable to get current sensor diagnsotic" + ex.Message);
                    diag = null;
                }
            }

            return diag;
        }
    }
}
