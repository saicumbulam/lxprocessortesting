﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Aws.Core.Utilities;
using System.Globalization;
using LxPortalLib.Common;
using LxPortalLib.Models;

namespace LxPortalLib.Api
{
    public class CellAlertClient
    {
        private const ushort EventManagerOffset = 10000;

        private static readonly WebRequestHandler Handler = new WebRequestHandler
        {
            ReadWriteTimeout = Config.ApiReadWriteTimeout
        }; 

        //haven't fully unit tested the output of API before hitting lib
        //make sure that api returns list 
        public static ResponseContainer<List<CellAlertRecord>> GetBoxCellAlert(DateTime start, DateTime end, string alertTypeList,
                                                            decimal ullat, decimal ullon, decimal lrlat, decimal lrlon)
        {
            ResponseContainer<List<CellAlertRecord>> responseContainer = new ResponseContainer<List<CellAlertRecord>>();

            if (start < end && ullat > lrlat && ullon < lrlon)
            {
                HttpClient client = new HttpClient(Handler);
                client.BaseAddress = Config.ApiCTABaseAddress;
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                try
                {
                    string baseurl = string.Empty; 
                    if (Config.SystemType.ToUpper() == "TLN")
                    {
                        baseurl = string.Format("api/v2/cellalert");
                    }
                    else
                    {
                        baseurl = string.Format("api/cellalert");
                    }


                    start = Utilities.NormalizeDateTimeToUtc(start);
                    end = Utilities.NormalizeDateTimeToUtc(end);

                    string parameters =
                        string.Format("?start={0}&end={1}&alerttypelist={2}&ullat={3}&ullon={4}&lrlat={5}&lrlon={6}",
                                      Uri.EscapeUriString(start.ToString("s")),
                                      Uri.EscapeUriString(end.ToString("s")),
                                      Uri.EscapeUriString(alertTypeList.ToString(CultureInfo.InvariantCulture)),
                                      Uri.EscapeUriString(ullat.ToString(CultureInfo.InvariantCulture)),
                                      Uri.EscapeUriString(ullon.ToString(CultureInfo.InvariantCulture)),
                                      Uri.EscapeUriString(lrlat.ToString(CultureInfo.InvariantCulture)),
                                      Uri.EscapeUriString(lrlon.ToString(CultureInfo.InvariantCulture)));

                    string caurl = baseurl + parameters;

                    HttpResponseMessage resp = client.GetAsync(caurl).Result;
                    if (resp.IsSuccessStatusCode)
                    {
                        if (Config.SystemType.ToUpper() == "TLN")
                        {
                            responseContainer = resp.Content.ReadAsAsync<ResponseContainer<List<CellAlertRecord>>>().Result;
                        }
                        else
                        {
                            responseContainer.r = resp.Content.ReadAsAsync<List<CellAlertRecord>>().Result; 
                        }
                    }
                    else if (resp.StatusCode < HttpStatusCode.InternalServerError)
                    {
                        responseContainer = resp.Content.ReadAsAsync<ResponseContainer<List<CellAlertRecord>>>().Result;
                    }
                    else
                    {
                        responseContainer.c = (int)resp.StatusCode;
                        responseContainer.r = new List<CellAlertRecord>();
                        responseContainer.e = "Failed to get Cell Alert data.";
                    }
                }
                catch (Exception ex)
                {
                    responseContainer.c = 500;
                    responseContainer.e = "Error occured in inner call: " + ex;
                    EventManager.LogError(EventManagerOffset, "Unable to get list of Cell Alerts" + ex.Message);
                }

            }
            return responseContainer;
        }
    }
}
