﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Aws.Core.Utilities;
using LxPortalLib.Common;
using LxPortalLib.Models;

namespace LxPortalLib.Api
{
    public class SensorPacketClient
    {
        private const ushort EventManagerOffset = 3000;

        private static readonly WebRequestHandler Handler = new WebRequestHandler
        {
            ReadWriteTimeout = Config.ApiReadWriteTimeout
        };

        /// <summary>
        /// To get the most sensor packet data list for a particular sensor
        /// </summary>
        /// <param name="id">sensor id</param>
        /// <param name="start">start time</param>
        /// <param name="end">end time</param>
        /// <returns>The return object will contain the list of sensor packet data. A null object will be returned if there is a failure</returns>
        public static List<PacketRecord> Get(string id, DateTime? start, DateTime? end)
        {
            List<PacketRecord> packets = null;
            if (!string.IsNullOrWhiteSpace(id))
            {
                HttpClient client = new HttpClient(Handler);
                client.BaseAddress = Config.ApiBaseAddress;

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                try
                {
                    string url = string.Format("api/sensorpackethistory/{0}", id);
                    if (start.HasValue || end.HasValue)
                    {
                        if (start.HasValue)
                            start = Utilities.NormalizeDateTimeToUtc(start.Value);

                        if (end.HasValue)
                            end = Utilities.NormalizeDateTimeToUtc(end.Value);
                        
                        string parameters = string.Format("?start={0}&end={1}", 
                            (start.HasValue) ? Uri.EscapeUriString(start.Value.ToString("s")) : string.Empty,
                            (end.HasValue) ? Uri.EscapeUriString(end.Value.ToString("s")) : string.Empty);

                        url = url + parameters;
                    }

                    HttpResponseMessage resp = client.GetAsync(url).Result;

                    if (resp.IsSuccessStatusCode)
                        packets = resp.Content.ReadAsAsync<List<PacketRecord>>().Result;

                    else if( resp.StatusCode < HttpStatusCode.InternalServerError)
                    {
                        packets = new List<PacketRecord>();
                    }
                    else
                    {
                        resp = null; 
                    }
                }
                catch (Exception ex)
                {
                    EventManager.LogError(EventManagerOffset, "Unable to get current sensor status" + ex.Message);
                    packets = null;
                }
            }

            return packets;
        }
    }
}
