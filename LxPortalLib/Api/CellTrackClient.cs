﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Aws.Core.Utilities;
using LxPortalLib.Common;
using LxPortalLib.Models;

namespace LxPortalLib.Api
{
    public class CellTrackClient
    {
        private const ushort EventManagerOffset = 10000;

        private static readonly WebRequestHandler Handler = new WebRequestHandler
        {
            ReadWriteTimeout = Config.ApiReadWriteTimeout
        };

        public static ResponseContainer<List<CellTrackRecord>> GetBoxCellTrack(DateTime start, DateTime end, decimal ullat, decimal ullon,
                                                            decimal lrlat, decimal lrlon)
        {
            ResponseContainer<List<CellTrackRecord>> responseContainer = new ResponseContainer<List<CellTrackRecord>>();

            if (start < end && ullat > lrlat && ullon < lrlon)
            {
                HttpClient client = new HttpClient(Handler);
                client.BaseAddress = Config.ApiCTABaseAddress;
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                try
                {
                    string baseurl = string.Empty;
                    if (Config.SystemType.ToUpper() == "TLN")
                    {
                        baseurl = string.Format("api/v2/celltrack");
                    }
                    else
                    {
                        baseurl = string.Format("api/celltrack");
                    }
                        
                    start = Utilities.NormalizeDateTimeToUtc(start);
                    end = Utilities.NormalizeDateTimeToUtc(end);

                    string parameters =
                        string.Format("?start={0}&end={1}&ullat={2}&ullon={3}&lrlat={4}&lrlon={5}",
                                      Uri.EscapeUriString(start.ToString("s")),
                                      Uri.EscapeUriString(end.ToString("s")),

                                      Uri.EscapeUriString(ullat.ToString(CultureInfo.InvariantCulture)),
                                      Uri.EscapeUriString(ullon.ToString(CultureInfo.InvariantCulture)),
                                      Uri.EscapeUriString(lrlat.ToString(CultureInfo.InvariantCulture)),
                                      Uri.EscapeUriString(lrlon.ToString(CultureInfo.InvariantCulture)));

                    string cturl = baseurl + parameters;

                    HttpResponseMessage resp = client.GetAsync(cturl).Result;
                    if (resp.IsSuccessStatusCode)
                    {
                        if (Config.SystemType.ToUpper() == "TLN")
                        {
                            responseContainer = resp.Content.ReadAsAsync<ResponseContainer<List<CellTrackRecord>>>().Result;
                        }
                        else
                        {
                            responseContainer.r = resp.Content.ReadAsAsync<List<CellTrackRecord>>().Result;
                        }
                    }
                    else if (resp.StatusCode < HttpStatusCode.InternalServerError)
                    {
                        responseContainer = resp.Content.ReadAsAsync<ResponseContainer<List<CellTrackRecord>>>().Result;
                    }
                    else
                    {
                        responseContainer.c = (int)resp.StatusCode;
                        responseContainer.r = new List<CellTrackRecord>();
                        responseContainer.e = "Failed to get Cell Tracks data.";
                    }
                }
                catch (Exception ex)
                {
                    responseContainer.c = 500;
                    responseContainer.e = "Error occured in inner call: " + ex;
                    EventManager.LogError(EventManagerOffset, "Unable to get list of Cell Tracks" + ex.Message);
                }

            }
            return responseContainer;
        }

    }
}
