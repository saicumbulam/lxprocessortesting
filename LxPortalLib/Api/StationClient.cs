﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Aws.Core.Utilities;
using LxPortalLib.Models;

namespace LxPortalLib.Api
{
    public class StationClient
    {
        private const ushort EventManagerOffset = 3000;

        private static readonly WebRequestHandler Handler = new WebRequestHandler
        {
            ReadWriteTimeout = Config.ApiReadWriteTimeout
        };

        public static bool Delete(string id, bool useLocalDB)
        {
            bool isSuccess = false;
            if (!string.IsNullOrEmpty(id))
            {
                HttpClient client = new HttpClient(Handler);
                client.BaseAddress = useLocalDB ? Config.ApiBaseAddress : Config.ApiRedundantAddress;
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                try
                {
                    Task<HttpResponseMessage> resp = client.DeleteAsync(string.Format("api/stations/{0}", id));
                    resp.Wait(Config.ApiReadWriteTimeout);
                    if (resp.IsCompleted && resp.Result.StatusCode == HttpStatusCode.NoContent)
                    {
                        isSuccess = true;
                    }
                }
                catch (Exception ex)
                {
                    EventManager.LogError(EventManagerOffset,
                                            string.Format(
                                                "Unable to delete station {0} from {1} database. Reason: {2}",
                                                id, useLocalDB ? "local" : "remote", ex.Message));
                }
            }
            return isSuccess;
        }

        public static Station Post(Station station, bool useLocalDB)
        {
            if (!(string.IsNullOrEmpty(station.Name) || string.IsNullOrEmpty(station.SerialNumber)))
            {
                HttpClient client = new HttpClient(Handler);
                client.BaseAddress = useLocalDB ? Config.ApiBaseAddress : Config.ApiRedundantAddress;
                if (client.BaseAddress != null)
                {
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    try
                    {
                        Task<HttpResponseMessage> resp = client.PostAsJsonAsync("api/stations", station);
                        resp.Wait(Config.ApiReadWriteTimeout);
                        if (resp.IsCompleted && resp.Result.StatusCode == HttpStatusCode.Created)
                        {
                            string[] tokens = resp.Result.Headers.Location.AbsoluteUri.Split('/');
                            station.Id = tokens[tokens.Length - 1];
                        }
                        else
                        {
                            station = null;
                        }
                    }
                    catch (Exception ex)
                    {
                        EventManager.LogError(EventManagerOffset,
                                              string.Format("Unable to post station {0} for {1} database. Reason: {2}",
                                                            (station == null) ? "" : station.Id,
                                                            useLocalDB ? "local" : "remote", ex.Message));
                        station = null;
                    }
                }
            }
            return station;
        }

        public static HttpResponseMessage Put(Station station, bool useLocalDB)
        {
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.InternalServerError);
            if (!string.IsNullOrWhiteSpace(station.Id))
            {
                HttpClient client = new HttpClient(Handler);
                client.BaseAddress = useLocalDB ? Config.ApiBaseAddress : Config.ApiRedundantAddress;

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                try
                {
                    Task<HttpResponseMessage> resp = client.PutAsJsonAsync(string.Format("api/stations/{0}", station.Id), station);
                    resp.Wait(Config.ApiReadWriteTimeout);
                    if (resp.IsCompleted && resp.Result.StatusCode == HttpStatusCode.OK)
                    {
                        result = resp.Result;
                    }
                }
                catch (Exception ex)
                {
                    EventManager.LogError(EventManagerOffset, string.Format("Unable to update station {0} for {1} database. Reason: {2}", station.Id, useLocalDB ? "local" : "remote", ex.Message));
                }
            }
            return result;
        }

        //version 1
        public static List<Station> GetAll()
        {
            List<Station> stations = null;
            HttpClient client = new HttpClient(Handler);
            client.BaseAddress = Config.ApiBaseAddress;
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            try
            {
                HttpResponseMessage resp = client.GetAsync("api/stations").Result;
                if (resp.IsSuccessStatusCode)
                    stations = resp.Content.ReadAsAsync<List<Station>>().Result;
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventManagerOffset, "Unable to get station list" + ex.Message);
            }

            return stations;
        }

        //version 2
        public static ResponseContainer<List<Station>> GetAllv2()
        {
            ResponseContainer<List<Station>> responseContainer = new ResponseContainer<List<Station>>();

            HttpClient client = new HttpClient(Handler);
            client.BaseAddress = Config.ApiBaseAddress; 
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            try
            {
                string baseurl = string.Empty;
                if (Config.SystemType.ToUpper() == "TLN")
                {
                    baseurl = string.Format("api/v2/stations");
                }
                else
                {
                    baseurl = string.Format("api/stations");
                }

                HttpResponseMessage resp = client.GetAsync(baseurl).Result; 
                if (resp.IsSuccessStatusCode)
                {
                    if (Config.SystemType.ToUpper() == "TLN")
                    {
                        responseContainer = resp.Content.ReadAsAsync<ResponseContainer<List<Station>>>().Result; 
                    }
                    else
                    {
                        responseContainer.c = (int)resp.StatusCode;
                        responseContainer.r = resp.Content.ReadAsAsync<List<Station>>().Result; 
                    }
                }
                else if (resp.StatusCode < HttpStatusCode.InternalServerError)
                {
                    responseContainer = resp.Content.ReadAsAsync<ResponseContainer<List<Station>>>().Result; 
                }
                else
                {
                    responseContainer.c = (int) resp.StatusCode; 
                    responseContainer.r = new List<Station>();
                    responseContainer.e = "Failed to get Stations data."; 
                }
            }
            catch(Exception ex)
            {
                responseContainer.c = 500;
                responseContainer.e = "Error occured in inner call: " + ex;
                EventManager.LogError(EventManagerOffset, "Unable to get list of Stations" + ex.Message);
            }

            return responseContainer; 
        }

        public static Station Get(string id, bool useLocalDB)
        {
            Station station = null;
            if (!string.IsNullOrWhiteSpace(id))
            {
                HttpClient client = new HttpClient(Handler);
                client.BaseAddress = useLocalDB ? Config.ApiBaseAddress : Config.ApiRedundantAddress;
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                if (client.BaseAddress != null)
                {
                    try
                    {
                        HttpResponseMessage resp = client.GetAsync(string.Format("api/stations/{0}", id)).Result;
                        if (resp.IsSuccessStatusCode)
                            station = resp.Content.ReadAsAsync<Station>().Result;
                    }
                    catch (Exception ex)
                    {
                        EventManager.LogError(EventManagerOffset, string.Format("Unable to get station {0} for {1} database. Reason: {2}", id, useLocalDB?"local":"remote", ex.Message));
                    }
                }
            }

            return station;
        }
    }
}
