﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Aws.Core.Utilities;
using LxPortalLib.Common;
using LxPortalLib.Models;

namespace LxPortalLib.Api
{
    public class NwsAlertClient
    {
        private const ushort EventManagerOffset = 3000;

        private static readonly WebRequestHandler Handler = new WebRequestHandler
        {
            ReadWriteTimeout = Config.ApiReadWriteTimeout
        };

        public static ResponseContainer<List<NwsAlert>> GetNwsAlerts(DateTime start, DateTime end)
        {
            ResponseContainer<List<NwsAlert>> responseContainer = new ResponseContainer<List<NwsAlert>> { i = Guid.NewGuid().ToString(), r = null };
            
            HttpClient client = new HttpClient(Handler);
            client.BaseAddress = Config.ApiBaseAddress; 
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            try
            {
                string baseurl = string.Empty;
                if (Config.SystemType.ToUpper() == "TLN")
                {
                    baseurl = string.Format("api/v2/nwsalert");
                }
                else
                {
                    baseurl = string.Format("api/nwsalert");
                }

                start = Utilities.NormalizeDateTimeToUtc(start);
                end = Utilities.NormalizeDateTimeToUtc(end);

                string parameters = string.Format("?start={0}&end={1}",
                                                  Uri.EscapeUriString(start.ToString("s")),
                                                  Uri.EscapeUriString(end.ToString("s")));

                string nwsurl = baseurl + parameters;

                HttpResponseMessage resp = client.GetAsync(nwsurl).Result;
                if (resp.IsSuccessStatusCode)
                {
                    if (Config.SystemType.ToUpper() == "TLN")
                    {
                        responseContainer = resp.Content.ReadAsAsync<ResponseContainer<List<NwsAlert>>>().Result;
                    }
                    else
                    {
                        responseContainer.c = (int)resp.StatusCode;
                        responseContainer.r = resp.Content.ReadAsAsync<List<NwsAlert>>().Result; 
                    }
                }
                else if (resp.StatusCode < HttpStatusCode.InternalServerError)
                {
                    responseContainer = resp.Content.ReadAsAsync<ResponseContainer<List<NwsAlert>>>().Result;
                }
                else
                {
                    responseContainer.c = (int)resp.StatusCode;
                    responseContainer.r = new List<NwsAlert>();
                    responseContainer.e = "Failed to get Nws alert data. ";
                }
            }
            catch (Exception ex)
            {
                responseContainer.c = 500;
                responseContainer.e = "Error occured in inner call: " + ex;
                EventManager.LogError(EventManagerOffset, "Unable to get NwsAlerts list" + ex.Message);
            }

            return responseContainer; 
        }
    }
}
