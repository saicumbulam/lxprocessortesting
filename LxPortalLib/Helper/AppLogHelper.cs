﻿using System;
using Aws.Core.Utilities;

namespace LxPortalLib.Helper
{
    public class AppLogHelper
    {
        private static bool _isInitialized = false;
        private static readonly object LockInit = new object();

        public static void LogMessage(string message, Exception exp)
        {
            if (!_isInitialized)
            {
                lock (LockInit)
                {
                    if (!_isInitialized)
                    {
                        _isInitialized = true;
                        AppLogger.Init();
                    }
                }
            }
            if (exp != null)
                AppLogger.LogError(message, exp); 
            else
                AppLogger.Log(message);
        }
    }
}
