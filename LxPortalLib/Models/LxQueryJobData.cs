﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace LxPortalLib.Models
{
    public class LxQueryJobData
    {
        private DateTime _dateSubmitted;
        private double? _latitude;
        private double? _longitude;

        private double? _minLatitude;
        private double? _minLongitude;

        private double? _maxLatitude;
        private double? _maxLongitude;

        public LxQueryJobData()
        {
            // Set default values using DefaultValue attribute
            foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(this))
            {
                var myDefaultValueAttribute = (DefaultValueAttribute)property.Attributes[typeof(DefaultValueAttribute)];

                if (myDefaultValueAttribute != null)
                    property.SetValue(this, myDefaultValueAttribute.Value);
            }
        }


        public string JobId { get; set; }

        public DateTime SubmittedDateTime
        {
            get { return _dateSubmitted; }
            set { _dateSubmitted = value.Kind != DateTimeKind.Utc ? DateTime.SpecifyKind(value, DateTimeKind.Utc) : value; }
        }

        [DefaultValue(false)]
        public bool Resubmitted { get; set; }
        public DateTime ResubmittedDateTime
        {
            get { return _dateSubmitted; }
            set { _dateSubmitted = value.Kind != DateTimeKind.Utc ? DateTime.SpecifyKind(value, DateTimeKind.Utc) : value; }
        }

        [DefaultValue(LxQueryType.Circle)]
        public LxQueryType QueryType { get; set; }
        [DefaultValue(StrokeType.None)]
        public StrokeType StrokeType { get; set; }
        [DefaultValue(LxType.Both)]
        public LxType LxType { get; set; }

        public DateTime Start { get; set; }
        public DateTime End { get; set; }


        public int? Radius { get; set; }
        [DefaultValue(RadiusType.Miles)]
        public RadiusType RadiusType { get; set; }

        public double? Latitude
        {
            get { return _latitude; }
            set { _latitude = value.HasValue ? Math.Round(value.Value, 5, MidpointRounding.ToEven) : (double?) null; }
        }

        public double? Longitude
        {
            get { return _longitude; }
            set { _longitude = value.HasValue ? Math.Round(value.Value, 5, MidpointRounding.ToEven) : (double?)null; }
        }


        public double? LatitudeMax
        {
            get { return _maxLatitude; }
            set { _maxLatitude = value.HasValue ? Math.Round(value.Value, 5, MidpointRounding.ToEven) : (double?)null; }
        }

        public double? LatitudeMin
        {
            get { return _minLatitude; }
            set { _minLatitude = value.HasValue ? Math.Round(value.Value, 5, MidpointRounding.ToEven) : (double?)null; }
        }

        public double? LongitudeMax
        {
            get { return _maxLongitude; }
            set { _maxLongitude = value.HasValue ? Math.Round(value.Value, 5, MidpointRounding.ToEven) : (double?)null; }
        }

        public double? LongitudeMin
        {
            get { return _minLongitude; }
            set { _minLongitude = value.HasValue ? Math.Round(value.Value, 5, MidpointRounding.ToEven) : (double?)null; }
        }

        [DefaultValue(FileOutputType.Json)]
        public FileOutputType OutputType { get; set; }

        public string RecipientEmailAddress { get; set; }

        public bool? SendNotifications { get; set; }

        public bool? SendProgressNotifications { get; set; }

        public List<string> QuadKey { get; set; }

        [DefaultValue(LxOutputVersion.V3)]
        public LxOutputVersion OutputVersion { get; set; }

        [DefaultValue(true)]
        public bool OutputCompressed { get; set; }
    }

    public enum LxQueryType { BoundingBox = 0, Circle = 1, QuadKey = 2 }
    public enum LxType { Flash = 0, Portion = 1, Both = 2 }
    public enum RadiusType { Meters = 0, Miles = 1 }
    public enum FileOutputType { Json = 0, Csv = 1, Kmz = 2 }
    public enum LxOutputVersion { V2 = 0, V3 = 1 }

    //[AttributeUsage(AttributeTargets.Property)]
    //public class DateTimeKindAttribute : Attribute
    //{
    //    private readonly DateTimeKind _kind;

    //    public DateTimeKindAttribute(DateTimeKind kind)
    //    {
    //        _kind = kind;
    //    }

    //    public DateTimeKind DateTimeKind
    //    {
    //        get { return _kind; }
    //    }
    //}
}
