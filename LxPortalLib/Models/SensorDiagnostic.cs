﻿using System;
using System.Collections.Generic;

namespace LxPortalLib.Models
{
    public class SensorDiagnostics
    {
        public string Id { get; set; }
        public List<SensorDiagnosticData> DiagnosticList { get; set; } 
    }

    public class SensorDiagnosticData
    {
        public DateTime? TimestampUtc { get; set; }
        public string Value { get; set; }
    }
}
