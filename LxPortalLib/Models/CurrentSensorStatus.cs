﻿namespace LxPortalLib.Models
{
    public class CurrentSensorStatus
    {
        public PacketRecord Packet { get; set; }
        public GpsRecord Gps { get; set; }
        public HardwareRecord Hardware { get; set; }
        public DetectionRecordData Detection { get; set; }
        public string Id { get; set; }
        
    }
}
