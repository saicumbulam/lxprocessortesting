﻿using System;
using Newtonsoft.Json;

namespace LxPortalLib.Models
{
    public class Station
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string SerialNumber { get; set; }
        public DateTime? InstallDate { get; set; }
        public string IndoorFirmware { get; set; }
        public string Ip { get; set; }
        public DateTime? FirstCallHome { get; set; }
        public DateTime? LastCallHome { get; set; }
        public DateTime? LastCalibration { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string Province { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string PocName { get; set; }
        public string PocPhone { get; set; }
        public string PocEmail { get; set; }
        public int? SendLogRateMinutes { get; set; }
        public int? SendGpsRateMinutes { get; set; }
        public int? SendAntennaGainRateMinutes { get; set; }
        public int? AntennaMode { get; set; }
        public int? AntennaAttenuation { get; set; }
        public string PrimaryDeliveryIp { get; set; }
        public string SecondaryDeliveryIp { get; set; }
        public string OutdoorFirmware { get; set; }
        public int Status { get; set; }
        public int? PacketPercent { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public DateTime? LastPacketUtc { get; set; }
        public string Note { get; set; }
        public bool IsEnable { get; set; }
        public string NetworkId { get; set; }
        public string NetworkDescription { get; set; }
        public decimal? BackUpBatteryVol { get; set; }
        public decimal? MemBackUpVoltage { get; set; }
        [JsonIgnore]
        public int? Displayer { get; set; }
        [JsonIgnore]
        public int? WindSensor { get; set; }
        [JsonIgnore]
        public string AntennaHwRev { get; set; }
        [JsonIgnore]
        public string AntennaSerial { get; set; }
        [JsonIgnore]
        public int? RemoteSerial { get; set; }
    }
}
