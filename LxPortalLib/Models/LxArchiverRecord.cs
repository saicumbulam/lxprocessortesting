﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LxCommon;

namespace LxPortalLib.Models
{

    public class LxArchiverRecord
    {
        public Guid? FlashGUID { get; set; } 
        public string TimeUtc { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public double Height { get; set; }
        public FlashType Type { get; set; }
        public double Amplitude { get; set; }
        public int Confidence { get; set; }
        public int NumberSensors { get; set; }
        public string Version { get; set; }
        public double MinLatitude { get; set; }
        public double MaxLatitude { get; set; }
        public double MinLongitude { get; set; }
        public double MaxLongitude { get; set; }
        public string Source { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public decimal DurationSeconds { get; set; }
        public int IcMultiplicity { get; set; }
        public int CgMultiplicity { get; set; }

        public List<FlashPortionRecord> Portions { get; set; }
    }
}
