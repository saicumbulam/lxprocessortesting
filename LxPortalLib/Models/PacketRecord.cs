﻿using System;

namespace LxPortalLib.Models
{
    public class PacketRecord
    {
        public DateTime? TimestampUtc { get; set; }
        public DateTime? LastPacketUtc { get; set; }
        public int? ReceivedPackets { get; set; }
        public int? PacketRate { get; set; }
        public decimal? TotalPacketSizeKilobytes { get; set; }
        public int? MinPacketSizeBytes { get; set; }
        public int? MaxPacketSizeBytes { get; set; }
        public DateTime? FirstPacketUtc { get; set; }
        public int? Status { get; set; }
        public int? EmptyPackets { get; set; }
        public int? MinPacketLatency { get; set; }
        public int? MaxPacketLatency { get; set; }
        public int? MedianPacketLatency { get; set; }
        public int? MinLFThreshold { get; set; }
        public int? MaxLFThreshold { get; set; }
        public int? MedianLFThreshold { get; set; }
        public int? MinHFThreshold { get; set; }
        public int? MaxHFThreshold { get; set; }
        public int? MedianHFThreshold { get; set; }
    }
}
