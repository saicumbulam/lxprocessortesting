﻿using System;
using System.Collections.Generic;

namespace LxPortalLib.Models
{
    public enum StrokeType { CloudToGround = 0, IntraCloud = 1, None}

    public enum LayerType   { Flash = 1, FlashPortion = 2}

    public class FlashRecord
    {
        public long Id { get; set; }
        public DateTime TimeUtc { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public double Height { get; set; }
        public StrokeType? Type { get ; set; }
        public string StrokeSolution { get; set; }
        public double Amplitude { get; set; }
        public int Confidence { get; set; }
    }
}
