﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LxCommon.Models;
using LxCommon;

namespace LxPortalLib.Models
{

    public class FlashPortionRecord
    {
        public Guid? FlashGUID { get; set; }
        public Guid? FlashID { get; set; }
        public string TimeUtc { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public double Height { get; set; }
        public FlashType? Type { get; set; }
        public string StrokeSolution { get; set; }
        public double Amplitude { get; set; }
        public int Confidence { get; set; }
        public string Version { get; set; }
        public ErrorEllipse ErrorEllipse { get; set; }
        public string Offsets { get; set; }
        public Dictionary<string, double> StationOffsets { get; set; }
    }
}
