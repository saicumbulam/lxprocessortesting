﻿using System;
using System.Net;

using Newtonsoft.Json;

namespace LxPortalLib.Models
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Parameter | AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class VerbosePropertyAttribute : Attribute
    {
        public string PropertyName { get; set; }
        public VerbosePropertyAttribute(string propertyName)
        {
            this.PropertyName = propertyName;
        }
    }

    public class ResponseContainer<T>
    {
        [JsonProperty("Id")]
        public string i { get; set; }
        [JsonProperty("Code")]
        public int c { get; set; }
        [JsonProperty("ErrorMessage")]
        public string e { get; set; }
        [JsonProperty("Result")]
        public T r { get; set; }

        internal HttpStatusCode GetCorrespondingStatusCode()
        {
            object val = Enum.ToObject(typeof(HttpStatusCode), c);
            if (!Enum.IsDefined(typeof(HttpStatusCode), val))
            {
                return HttpStatusCode.InternalServerError;
            }
            return (HttpStatusCode)val;
        }

        [JsonIgnore]
        public bool IsSuccessStatusCode
        {
            get
            {
                HttpStatusCode hsc = GetCorrespondingStatusCode();
                return (hsc >= HttpStatusCode.OK && hsc <= (HttpStatusCode) 299);
            }
        }
    }
}
