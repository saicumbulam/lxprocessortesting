﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LxPortalLib.Models
{
    public class EventLogRecord
    {
        public string RemoteIpAddress {get; set; }
        public string EventText { get; set; }
        public DateTime LastModifiedDateUtc { get; set; }
    }
}
