﻿namespace LxPortalLib.Models
{
    public class Network
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
