﻿using System;

namespace LxPortalLib.Models
{
    public class GpsRecord
    {
        public DateTime? LastUpdateUtc { get; set; }
        public int? VisibleSatellites { get; set; }
        public int? TrackedSatellites { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        public decimal? Altitude { get; set; }
        public DateTime? SensorTimeUtc { get; set; }
    }
}
