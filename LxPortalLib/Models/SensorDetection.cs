﻿using System;
using System.Collections.Generic;

namespace LxPortalLib.Models
{
    //get a list of detection records
    public class SensorDetection
    {
        public string Id { get; set; }
        public List<DetectionRecordData> DetectionList { get; set; }
    }


    //single detection record 
    public class DetectionRecordData
    {
        public DateTime? LastDetectionUtc { get; set; }
        public decimal? LastLatitude { get; set; }
        public decimal? LastLongitude { get; set; }
        public int? TotalLast24Hours { get; set; }
        public DateTime? TimestampUtc { get; set; }
        public decimal? LastPeakCurrent { get; set; }
    }
}
