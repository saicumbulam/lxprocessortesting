﻿using System;

namespace LxPortalLib.Models
{
    public class CellTrackRecord
    {
        public string CellId { get; set; }
        public DateTime CellTimeUtc { get; set; }
        public string TrackLocation { get; set; }
        public string TrackCentroid { get; set; }
        public string CellLocation { get; set; }
        public string CellCentroid { get; set; }
        public decimal Direction { get; set; }
        public decimal InCloudFlashRate { get; set; }
        public decimal CloudGroundFlashRate { get; set; }
        public decimal InCloudFlashRateChange { get; set; }
        public decimal CloudGroudFlashRateChange { get; set; }
        public decimal Area { get; set; }
        public decimal Speed { get; set; }
        public string Type { get; set; }
    }
}
