﻿using System;
using System.Collections.Generic;

namespace LxPortalLib.Models
{

    public class StormReport
    {
        public string Id { get; set; }
        public int Code { get; set; }
        public string ErrorMessage { get; set; }
        public List<LocalStormRecord> Result { get; set; }
    }
    public class LocalStormRecord
    {
        public string City { get; set; }
        public string Comments { get; set; }
        public string County { get; set; }
        public string EventDetail { get; set; }
        public string EventSource { get; set; }
        public string GeoHash { get; set; }
        public string Id { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public int MessageTimeUtc { get; set; }
        public string MessageTimeUtcStr { get; set; }
        public int ReportTimeUtc { get; set; }
        public string ReportTimeUtcStr { get; set; }
        public string State { get; set; }
        public string WeatherEvent { get; set; }
        public string WmoHeader { get; set; }
    }


    ////obselete
    public class StormRecord
    {

        public int LsrId { get; set; }
        public DateTime UtcTime { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public string Event { get; set; }
        public string Magnitude { get; set; }
        public string Source { get; set; }
        public string Remarks { get; set; }
        public string Unit { get; set; }
    }
}
