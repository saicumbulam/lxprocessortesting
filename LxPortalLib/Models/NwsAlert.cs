﻿using System;

namespace LxPortalLib.Models
{
    public class NwsAlert
    {
        public DateTime PostTimeUtc { get; set; }
        public DateTime ExpireTimeUtc { get; set; }
        public string Location { get; set; }
        public string Description { get; set; }
        public int DirectionDeg { get; set; }
        public int SpeedKts { get; set; }
        public string Type { get; set; }
        public long VtecId { get; set; }
    }
}
