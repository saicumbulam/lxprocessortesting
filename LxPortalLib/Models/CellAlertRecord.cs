﻿using System;

namespace LxPortalLib.Models
{
    public class CellAlertRecord
    {
        public string CellId { get; set; }
        public DateTime CellTimeUtc { get; set; }
        public string AlertLocation { get; set; }
        public string AlertCentroid { get; set; }
        public string CellLocation { get; set; }
        public string CellCentroid { get; set; }
        public decimal Direction { get; set; }
        public decimal InCloudFlashRate { get; set; }
        public decimal CloudGroundFlashRate { get; set; }
        public decimal InCloudFlashRateChange { get; set; }
        public decimal CloudGroudFlashRateChange { get; set; }
        public decimal Area { get; set; }
        public decimal Speed { get; set; }
        public int Level { get; set; }
        public int Threshold { get; set; }
        public DateTime StartUtc { get; set; }
        public DateTime EndUtc { get; set; }
    }
}
