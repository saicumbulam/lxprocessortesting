﻿using System;
using Aws.Core.Utilities.Config;

namespace LxPortalLib
{
    public class Config : AppConfig
    {
        public static Uri ApiBaseAddress
        {
            get { return new Uri(Get("EarthNetworks.Lightning.Api.BaseAddress", "http://localhost:8080/")); } //DEV-LXUTIL-01
        }

        public static Uri ApiCompareToBaseAddress
        {
            get
            {
                string temp = Get("EarthNetworks.Lightning.Api.CompareToBaseAddress", string.Empty);
                return string.IsNullOrEmpty(temp) ? null : new Uri(temp);
            }
        }

        public static Uri ApiCTABaseAddress
        {
            get { return new Uri(Get("EarthNetworks.Lightning.Api.CTABaseAddress", "http://localhost:8080/")); } //DEV-LXUTIL-01
        }


        public static Uri ApiRedundantAddress
        {
            get
            {
                string temp = Get("EarthNetworks.Lightning.Api.RedundantAddress", string.Empty);
                return string.IsNullOrEmpty(temp) ? null : new Uri(temp);
            }
        }

        public static int ApiReadWriteTimeout
        {
            get { return Get("EarthNetworks.Lightning.Api.ReadWriteTimeout", 500000); }
        }

        public static int PacketPercentThreshold
        {
            get { return Get("LxPortal.Lib.Common.PacketPercentThreshold", 75); }
        }

        public static int CallHomeAgeThresholdMinutes
        {
            get { return Get("LxPortal.Lib.Common.CallHomeAgeThresholdMinutes", 120); }
        }

        public static int DetectionReportMaxRadiusInMeters
        {
            get { return Get("LxPortal.Api.Models.DetectionReportMaxRadiusInMeters", 804672); }
        }

        public static int DetectionReportMinRadiusInMeters
        {
            get { return Get("LxPortal.Api.Models.DetectionReportMinRadiusInMeters", 10000); }
        }

        public static string SystemType
        {
            get { return Get<string>("LxPortal.Api.Controller.SystemType", ""); }
        }

        //PULSE 
        /// <summary>
        /// For TLN this portion is call EN Pulse 
        /// </summary>
        public static Uri EarthNetworksPulse
        {
            get { return new Uri(Get("EarthNetworks.Pulse", "http://pulse.staging.enqa.co/")); }
        }

        public static string PulseGuidSecretKey
        {
            get { return Get("EarthNetworks.PulseGuidSecretKey", "cffe7617549349289adf6e0a8c4460e8"); }
        }

        public static string  PulseKey
        {
            get { return Get("EarthNetworks.PulseKey", "InternalV1V1"); }
        }

    }
}
