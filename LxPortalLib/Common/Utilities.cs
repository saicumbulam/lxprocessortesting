﻿using System;
using System.Globalization;

namespace LxPortalLib.Common
{
    public class Utilities
    {
        public static DateTime NormalizeDateTimeToUtc(DateTime value)
        {
            if (value.Kind != DateTimeKind.Utc)
                value = DateTime.SpecifyKind(value, DateTimeKind.Utc);

            return value;
        }

        public static long GetDateKey(DateTime date)
        {
            string minKey = string.Empty;
            if (date.Minute >= 0 && date.Minute <= 14)
                minKey = "1";
            else if (date.Minute >= 15 && date.Minute <= 29)
                minKey = "2";
            else if (date.Minute >= 30 && date.Minute <= 44)
                minKey = "3";
            else if (date.Minute >= 45 && date.Minute <= 59)
                minKey = "4";

            string k = string.Format("{0}{1}{2}{3}{4}", 
                date.Year, 
                date.Month.ToString(CultureInfo.InvariantCulture).PadLeft(2, '0'), 
                date.Day.ToString(CultureInfo.InvariantCulture).PadLeft(2, '0'), 
                date.Hour.ToString(CultureInfo.InvariantCulture).PadLeft(2, '0'), 
                minKey);

            long key;

            long.TryParse(k, out key);
            
            return key;
        }
        
        public static int GetSensorStatus(DateTime? lastCallHomeUtc, int? packetRate, bool sensorEnabled)
        {

            return GetSensorStatus(lastCallHomeUtc, packetRate, sensorEnabled,  DateTime.UtcNow, Config.PacketPercentThreshold, Config.CallHomeAgeThresholdMinutes);
        }

        public static int GetSensorStatus(DateTime? lastCallHomeUtc, int? packetRate, bool sensorEnabled, DateTime currentUtcTime, int packetRateThreshold, int callHomeThresholdMinutes)
        {
            int status = 0;

            if (!sensorEnabled)
            {
                status = 5;
            }
            else if (lastCallHomeUtc.HasValue)
            {
                TimeSpan ts = currentUtcTime - lastCallHomeUtc.Value;

                if (ts.TotalMinutes < callHomeThresholdMinutes)
                {
                    if (packetRate.HasValue && packetRate >= Config.PacketPercentThreshold)
                        status = 1;
                    else
                        status = 3;
                }
                else
                {
                    if (packetRate.HasValue && packetRate >= Config.PacketPercentThreshold)
                        status = 2;
                    else
                        status = 4;
                }
            }

            return status;
        }

        public static int ConvertMilesToMeters(int miles)
        {
            return (int)(Math.Round(miles * 1609.344, 0, MidpointRounding.AwayFromZero));
        }

        public static DateTime ConvertFromEpoch(int epochValue)
        {
            DateTime time = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return time.AddSeconds(epochValue);
        }

        public static long ConvertToEpoch(DateTime dateValue)
        {
            DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            return Convert.ToInt64((dateValue.ToUniversalTime() - epoch).TotalSeconds);
        }
    }
}
