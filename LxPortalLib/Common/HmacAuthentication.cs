﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace LxPortalLib.Common
{
    public class HmacAuthentication
    {
        const string HmacTs = "timestamp";
        const string HmacHash = "hash";
        const string HmacId = "authid";
        readonly static DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        const char Delimiter = '\n';

        /// <summary>
        /// Compute the Base64-encoded, SHA256 hash of the given data string with a particular secret key
        /// </summary>
        /// <param name="secretKey">secret key used to hash data</param>
        /// <param name="data">data to be hashed</param>
        /// <returns>hashed data, base64 encoded and then url encoded</returns>
        internal static string HashDataWithKey(HMACSHA256 mac, string data)
        {
            var hashBytes = mac.ComputeHash(Encoding.UTF8.GetBytes(data));
            return Convert.ToBase64String(hashBytes);
        }

        internal static long ToUnixTime(DateTime date)
        {
            return Convert.ToInt64((date.ToUniversalTime() - epoch).TotalSeconds);
        }
        internal static DateTime FromUnixTime(long unixTime)
        {
            return epoch.AddSeconds(unixTime);
        }
        private static void AddTimeStamp(IDictionary<string, string> queryParameters)
        {
            String hmacTs;
            if (!queryParameters.TryGetValue(HmacTs, out hmacTs))
            {
                hmacTs = ToUnixTime(DateTime.Now).ToString();
                queryParameters[HmacTs] = hmacTs;
            }
        }
        private static string AssembleHashData(string verb, string urlPathComponent, string postData, IDictionary<string, string> queryParameters)
        {
            StringBuilder data = new StringBuilder();
            data.Append(verb.ToUpperInvariant());
            data.Append(Delimiter);
            data.Append(urlPathComponent);
            data.Append(Delimiter);
            data.Append(postData);
            data.Append(Delimiter);
            string hmacTs;
            if (!queryParameters.TryGetValue(HmacTs, out hmacTs))
            {
                hmacTs = String.Empty;
            }
            data.Append(hmacTs);
            foreach (var kvp in queryParameters.Where(k => k.Key != HmacTs && k.Key != HmacHash && k.Key != HmacId).OrderBy(k => k.Key))
            {
                data.Append(Delimiter);
                data.Append(kvp.Key);
                data.Append(Delimiter);
                data.Append(kvp.Value);
            }
            return data.ToString();
        }

        public static void AddAuthenticationParameters(HMACSHA256 mac, string keyId, string verb, string urlPathComponent, string postData, IDictionary<string, string>
            queryParameters)
        {
            queryParameters[HmacId] = keyId;
            AddTimeStamp(queryParameters);
            queryParameters[HmacHash] = HashDataWithKey(mac, AssembleHashData(verb, urlPathComponent, postData, queryParameters));
        }

        public static string AddHmacAuth(HMACSHA256 mac, Uri original, string keyId, string verb, string postData)
        {
            string path = "/" + original.GetComponents(UriComponents.Path, UriFormat.SafeUnescaped);

            Dictionary<string, string> queryParameters = new Dictionary<string, string>();
            string query = original.GetComponents(UriComponents.Query, UriFormat.UriEscaped);

            foreach (var i in query.Split('&'))
            {
                var keyval = i.Split('=');
                if (keyval.Length == 2)
                {
                    queryParameters[keyval[0]] = HttpUtility.UrlDecode(keyval[1]);
                }
            }

            AddAuthenticationParameters(mac, keyId, verb, path, postData, queryParameters);
            StringBuilder uriBuilder = new StringBuilder();
            var authBase = original.GetComponents(UriComponents.SchemeAndServer, UriFormat.SafeUnescaped);
            uriBuilder.Append(authBase);
            uriBuilder.Append(path);
            uriBuilder.Append('?');
            bool first = true;
            foreach (var kvp in queryParameters)
            {
                if (first)
                {
                    first = false;
                }
                else
                {
                    uriBuilder.Append('&');
                }
                uriBuilder.Append(HttpUtility.UrlEncode(kvp.Key));
                uriBuilder.Append('=');
                uriBuilder.Append(HttpUtility.UrlEncode(kvp.Value));
            }

            return uriBuilder.ToString();
        }

        public static string AddHmacAuth(string secretKey, Uri original, string keyId, string verb = "GET", string postData = null)
        {
            return AddHmacAuth(new HMACSHA256(Encoding.UTF8.GetBytes(secretKey)), original, keyId, verb, postData ?? string.Empty);
        }
    }
}
