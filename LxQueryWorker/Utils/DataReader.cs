﻿using System;
using System.Collections.Generic;
using LxCommon.Models;
using LxCommon.Utilities;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.Common;
using Newtonsoft.Json;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Aws.Core.Utilities;
using LxCommon;

namespace LxQueryWorker.Utils
{
    internal class DataReader
    {
        private const string WwllnSource = "wwlln";
        private const string TlnSource = "tln";
        private string _jobId;

        public DataReader(string jobId)
        {
            _jobId = jobId;
        }

        public List<Flash> Read(string quadKey, DateTime start, DateTime end)
        {
            var result = new List<Flash>();

            double maxLatitude, maxLongitude, minLatitude, minLongitude;

            // get bounding box from quadkey
            TileSystem.QuadKeyToBoundingBox(quadKey, out maxLatitude, out maxLongitude, out minLatitude, out minLongitude);

            var startTimeString = start.ToString("yyyy-MM-dd HH:mm:ss.fff");
            var endTimeString = end.ToString("yyyy-MM-dd HH:mm:ss.fff");
            var query = CreateQueryStatement(startTimeString, endTimeString, 0, minLatitude, maxLatitude, minLongitude, maxLongitude);
            var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Lightning"].ConnectionString);
            var dr = default(SqlDataReader);

            var portions = new Dictionary<Guid, List<Portion>>();
            try
            {
                conn.Open();
                using (var comm = new SqlCommand("SET ARITHABORT ON", conn))
                {
                    comm.ExecuteNonQuery();
                }

                var command = new SqlCommand(query, conn)
                {
                    CommandTimeout = 7200 //2 hour timeout
                };

                try
                {
                    dr = command.ExecuteReader();
                }
                catch (SqlException ex)
                {
                    Console.WriteLine($"{DateTime.Now}: [ERROR] SQL command failed: {ex}");
                    AppLogger.LogError($"[{_jobId}] {DateTime.Now}: [ERROR] SQL command failed", ex);
                }

                if (dr != null && dr.HasRows)
                {
                    // Process records from DB
                    Console.WriteLine($"{DateTime.Now}: Start processing DB records for time time range: {startTimeString} - {endTimeString}");
                    AppLogger.LogDebug($"[{_jobId}] {DateTime.Now}: Start processing DB records for time time range: {startTimeString} - {endTimeString}");
                    portions = ProcessDbRecords(dr);
                    Console.WriteLine($"{DateTime.Now}: End processing DB records for time time range: {startTimeString} - {endTimeString}");
                    AppLogger.LogDebug($"[{_jobId}] {DateTime.Now}: End processing DB records for time time range: {startTimeString} - {endTimeString}");
                }

                dr?.Close();
                dr = null;
                conn?.Close();
                conn = null;

                AppLogger.LogDebug($"[{_jobId}] Portions count= {portions.Count}");
                if (portions.Count > 0)
                {
                    var flashes = ProcessFlashData(portions);
                    result = flashes[quadKey].OrderBy(x => x.TimeStamp).ToList();
                    AppLogger.LogDebug($"[{_jobId}] Flashes count = {result.Count}, quad key {quadKey}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"{DateTime.Now}: [ERROR] Caught exception while processing data for time time range: {startTimeString} - {endTimeString}: {ex}");
                AppLogger.LogError($"[{_jobId}] {DateTime.Now}: [ERROR] Caught exception while processing data for time time range: {startTimeString} - {endTimeString}", ex);
            }
            finally
            {
                dr?.Close();
                conn?.Close();

                //Console.WriteLine($"{DateTime.Now}: Processing time: {DateTime.Now.Subtract(intervalStartTime).TotalSeconds} seconds");
            }
            return result;
        }

        private Dictionary<Guid, List<Portion>> ProcessDbRecords(DbDataReader dr)
        {
            Console.WriteLine($"{DateTime.Now}: Start processing DB data");
            AppLogger.LogDebug($"[{_jobId}] {DateTime.Now}: Start processing DB data");

            Console.WriteLine($"{DateTime.Now}: Start caching field names");
            AppLogger.LogDebug($"[{_jobId}] {DateTime.Now}: Start caching field names");
            var columnNames = new Dictionary<string, int>();
            for (var i = 0; i < dr.FieldCount; i++)
            {
                var fieldName = dr.GetName(i);
                columnNames.Add(fieldName, dr.GetOrdinal(fieldName));
            }
            Console.WriteLine($"{DateTime.Now}: End caching field names");
            AppLogger.LogDebug($"[{_jobId}] {DateTime.Now}: End caching field names");

            //read through queried data
            Console.WriteLine($"{DateTime.Now}: Start processing records");
            AppLogger.LogDebug($"[{_jobId}] {DateTime.Now}: Start processing records");
            var portions = new Dictionary<Guid, List<Portion>>();
            var recordsProcessed = (long)0;
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    var data = (IDataRecord) dr;

                    var portion = CreatePortion(data, columnNames);
                    if (portion.FlashGUID.HasValue)
                    {
                        var flashGuid = portion.FlashGUID.Value;

                        if (!portions.ContainsKey(flashGuid))
                        {
                            portions.Add(flashGuid, new List<Portion>());
                        }

                        portions[flashGuid].Add(portion);
                    }
                    Console.Write(".");
                    recordsProcessed++;
                }
                Console.WriteLine("");
            }
            else
            {
                Console.WriteLine($"{DateTime.Now}: No records were returned!");
                AppLogger.LogDebug($"[{_jobId}] {DateTime.Now}: No records were returned!");
            }

            Console.WriteLine($"{DateTime.Now}: Start sorting records");
            AppLogger.LogDebug($"[{_jobId}] {DateTime.Now}: Start sorting records");
            foreach (var kvPair in portions)
            {
                portions[kvPair.Key].Sort((x, y) => LtgTimeUtils.GetUtcDateTimeFromString(x.TimeStamp)
                                                    .CompareTo(LtgTimeUtils.GetUtcDateTimeFromString(y.TimeStamp)));
            }
            Console.WriteLine($"{DateTime.Now}: End sorting records");
            Console.WriteLine($"{DateTime.Now}: End processing records: {recordsProcessed}");
            Console.WriteLine($"{DateTime.Now}: End processing DB data");
            AppLogger.LogDebug($"[{_jobId}] {DateTime.Now}: End sorting records");
            AppLogger.LogDebug($"[{_jobId}] {DateTime.Now}: End processing records: {recordsProcessed}");
            AppLogger.LogDebug($"[{_jobId}] {DateTime.Now}: End processing DB data");
            return portions;
        }

        private Dictionary<string, List<Flash>> ProcessFlashData(Dictionary<Guid, List<Portion>> portions)
        {
            Console.WriteLine($"{DateTime.Now}: Start processing flash data");
            AppLogger.LogDebug($"[{_jobId}] {DateTime.Now}: Start processing flash data");
            var flashesProcessed = (long)0;
            var flashes = new Dictionary<string, List<Flash>>();

            // create flashes based on portion lists
            foreach (var kvPair in portions)
            {
                var createdFlash = CreateFlash(kvPair.Value);
                var quadkey = GetQuadKey(createdFlash);

                if (!flashes.ContainsKey(quadkey))
                    flashes.Add(quadkey, new List<Flash>());

                createdFlash.Portions = kvPair.Value;

                flashes[quadkey].Add(createdFlash);
                flashesProcessed++;
            }

            Console.WriteLine($"{DateTime.Now}: End processing flash data: {flashesProcessed}");
            AppLogger.LogDebug($"[{_jobId}] {DateTime.Now}: End processing flash data: {flashesProcessed}");

            return flashes;
        }

        //returns the quad key of a Flash
        private string GetQuadKey(Flash flash)
        {
            var quadkey = "";
            var LevelOfDetail = 5;

            var latitude = flash.Latitude; var longitude = flash.Longitude;

            var pixelX = 0;
            var pixelY = 0;
            TileSystem.LatLongToPixelXY(latitude, longitude, out pixelX, out pixelY, LevelOfDetail);

            var tileX = 0;
            var tileY = 0;
            TileSystem.PixelXYToTileXY(pixelX, pixelY, out tileX, out tileY);

            quadkey = TileSystem.TileXYToQuadKey(tileX, tileY, LevelOfDetail);

            return quadkey;
        }

        private string CreateQueryStatement(string start, string end, int yearmonth, double minLatitude, double maxLatitude, double minLongitude, double maxLongitude)
        {
            var query =
                "SELECT Flash.FlashID ,Flash.FlashGUID ,Flash.Lightning_Time ,Flash.Lightning_Time_String ,Flash.Latitude" +
                ",Flash.Longitude ,Flash.Height ,Flash.Stroke_Type ,Flash.Amplitude ,Flash.Stroke_Solution" +
                ",Flash.Confidence ,Flash.LastModifiedTime ,Flash.LastModifiedBy" +
                ",Portions.FlashPortionGUID AS FlashPortions_FlashPortionGUID ,Portions.Lightning_Time_String AS FlashPortions_Lightning_Time_String" +
                ",Portions.Latitude AS FlashPortions_Latitude ,Portions.Longitude AS FlashPortions_Longitude,Portions.Height AS FlashPortions_Height, Portions.Stroke_Solution AS FlashPortions_Stroke_Solution" +
                ",Portions.Confidence AS FlashPortions_Confidence, Portions.LastModifiedTime AS FlashPortions_LastModifiedTime ,Portions.Amplitude AS FlashPortions_Amplitude " +
                ",Portions.LastModifiedBy AS FlashPortions_LastModifiedBy ,Portions.[Offsets] AS FlashPortions_Offsets ,Portions.Stroke_Type AS FlashPortions_Stroke_Type " +
                " FROM dbo.LtgFlash AS Flash WITH (NOLOCK) INNER JOIN dbo.LtgFlashPortions as Portions WITH (NOLOCK)" +
                " ON Flash.FlashGUID = Portions.FlashGUID" +
                $" WHERE Flash.Lightning_Time >= \'{start}\'" +
                $" AND Flash.Lightning_Time < \'{end}\'" +
                $" AND Flash.Latitude >= {minLatitude}" +
                $" AND Flash.Latitude < {maxLatitude}" +
                $" AND Flash.Longitude >= {minLongitude}" +
                $" AND Flash.Longitude < {maxLongitude}";
            //" ORDER BY Flash.Lightning_Time ASC;"

            AppLogger.LogDebug($"[{_jobId}] DB query statement: \"{query}\"");
            return query;
        }

        //create Portion object
        private Portion CreatePortion(IDataRecord data, Dictionary<string, int> columnNames)
        {
            var portion = new Portion
            {
                FlashGUID = data.GetGuid(columnNames["FlashGUID"]),
                FlashId = data.GetGuid(columnNames["FlashPortions_FlashPortionGUID"]),
                TimeStamp = data.GetString(columnNames["FlashPortions_Lightning_Time_String"]),
                Latitude = data.GetDouble(columnNames["FlashPortions_Latitude"]),
                Longitude = data.GetDouble(columnNames["FlashPortions_Longitude"]),
                Amplitude = data.GetDouble(columnNames["FlashPortions_Amplitude"]),
                Height = data.GetDouble(columnNames["FlashPortions_Height"]),
                Solution = data.GetString(columnNames["FlashPortions_Stroke_Solution"]),
                Confidence = data.GetInt32(columnNames["FlashPortions_Confidence"]),
                Offsets = data.GetString(columnNames["FlashPortions_Offsets"]),
                Type = (FlashType)data.GetInt32(columnNames["FlashPortions_Stroke_Type"])
            };
            portion.TimeStamp = portion.TimeStamp.Trim();

            // deserialize if Solution is json
            if (!string.IsNullOrWhiteSpace(portion.Solution)
                 && portion.Solution.Contains("ee")
                 && portion.Solution.Contains("v")
                 && portion.Solution.Contains("so"))
            {
                try
                {
                    var ped = JsonConvert.DeserializeObject<PortionExtendedData>(portion.Solution);
                    portion.ErrorEllipse = ped.ErrorEllipse;
                    portion.StationOffsets = ped.StationOffsets;
                    portion.Version = ped.Version;
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"{DateTime.Now}: [WARN] CreatePortion caught exception processing 'solution' property: {ex}");
                    AppLogger.LogError($"[{_jobId}] {DateTime.Now}: [WARN] CreatePortion caught exception processing 'solution' property", ex);
                }
            }

            // Remove Solution & Offsets (if needed)
            portion.Solution = null;
            if (string.IsNullOrWhiteSpace(portion.Offsets)) portion.Offsets = null;

            return portion;
        }

        //create Flash object
        private Flash CreateFlash(List<Portion> portions)
        {
            var toReturn = new Flash
            {
                Latitude = 0,
                Longitude = 0,
                Height = 0,
                CgMultiplicity = 0,
                IcMultiplicity = 0,
                DurationSeconds = 0,
                Type = FlashType.FlashTypeIC
            };


            Portion brightestIc = null;
            Portion brightestCg = null;
            var uniqueSensors = new HashSet<string>();
            Portion startPortion = null;
            Portion endPortion = null;

            if (portions != null)
            {
                if (portions.Count == 1 && ((Portion)portions[0]).Type == FlashType.FlashTypeGlobalCG)
                {
                    brightestCg = ((Portion)portions[0]);
                    toReturn.Type = FlashType.FlashTypeGlobalCG;
                    if (brightestCg.Offsets != null && brightestCg.Offsets.Length > 0)
                    {
                        var OffsetList = brightestCg.Offsets.Split(',');

                        foreach (var offset in OffsetList)
                        {
                            if (offset != null && offset.Length > 0)
                            {
                                var l = offset.IndexOf("=");
                                var id = offset;

                                if (l > 0)
                                {
                                    id = offset.Substring(0, l);
                                }

                                uniqueSensors.Add(id);
                            }
                        }
                    }
                    else if (brightestCg.StationOffsets != null)
                    {
                        foreach (var offset in brightestCg.StationOffsets)
                        {
                            uniqueSensors.Add(offset.Key);
                        }
                    }

                    toReturn.CgMultiplicity = 1;
                    toReturn.Source = WwllnSource;
                    startPortion = brightestCg;
                    endPortion = brightestCg;
                    toReturn.MinLatitude = brightestCg.Latitude;
                    toReturn.MaxLatitude = brightestCg.Latitude;
                    toReturn.MinLongitude = brightestCg.Longitude;
                    toReturn.MaxLongitude = brightestCg.Longitude;
                }
                else
                {
                    var initialized = false;
                    var count = 0;
                    while (count < portions.Count)
                    {
                        var portion = portions[count] as Portion;

                        if (portion != null)
                        {
                            if (!initialized)
                            {
                                startPortion = portion;
                                endPortion = portion;
                                toReturn.MinLatitude = portion.Latitude;
                                toReturn.MaxLatitude = portion.Latitude;
                                toReturn.MinLongitude = portion.Longitude;
                                toReturn.MaxLongitude = portion.Longitude;
                                toReturn.Version = portion.Version;
                                initialized = true;
                                toReturn.Source = TlnSource;
                            }
                            else
                            {
                                if (portion.TimeStamp.CompareTo(startPortion.TimeStamp) < 0) startPortion = portion;
                                if (portion.TimeStamp.CompareTo(endPortion.TimeStamp) > 0) endPortion = portion;
                                if (portion.Latitude < toReturn.MinLatitude) toReturn.MinLatitude = portion.Latitude;
                                if (portion.Latitude > toReturn.MaxLatitude) toReturn.MaxLatitude = portion.Latitude;
                                if (portion.Longitude < toReturn.MinLongitude) toReturn.MinLongitude = portion.Longitude;
                                if (portion.Longitude > toReturn.MaxLongitude) toReturn.MaxLongitude = portion.Longitude;
                            }

                            if (portion.Type == FlashType.FlashTypeCG)
                            {
                                portion.Height = 0;
                                toReturn.Type = FlashType.FlashTypeCG;
                                if (brightestCg == null || (Math.Abs(portion.Amplitude) > Math.Abs(brightestCg.Amplitude)))
                                {
                                    brightestCg = portion;
                                }
                                toReturn.CgMultiplicity++;
                            }
                            else if (portion.Type == FlashType.FlashTypeIC)
                            {
                                if (portion.Height < 0)
                                {
                                    portion.Height *= -1;
                                }

                                if (brightestIc == null || (Math.Abs(portion.Amplitude) > Math.Abs(brightestIc.Amplitude)))
                                {
                                    brightestIc = portion;
                                }

                                toReturn.IcMultiplicity++;
                            }

                            if (portion.Offsets != null)
                            {
                                var OffsetList = portion.Offsets.Split(',');

                                foreach (var offset in OffsetList)
                                {
                                    if (!string.IsNullOrWhiteSpace(offset))
                                    {
                                        var l = offset.IndexOf("=", StringComparison.Ordinal);
                                        var id = offset;

                                        if (l > 0)
                                        {
                                            id = offset.Substring(0, l);
                                        }

                                        uniqueSensors.Add(id);
                                    }
                                }
                            }
                            else if (portion.StationOffsets != null)
                            {
                                foreach (var offset in portion.StationOffsets)
                                {
                                    uniqueSensors.Add(offset.Key);
                                }
                            }
                        }

                        count++;
                    }
                }

                if (startPortion != null)
                {
                    if (toReturn.IcMultiplicity > 1 || toReturn.CgMultiplicity > 1)
                    {
                        toReturn.StartTime = startPortion.TimeStamp;
                        toReturn.EndTime = endPortion.TimeStamp;

                        var x = Convert.ToDateTime(toReturn.EndTime) - Convert.ToDateTime(toReturn.StartTime);
                        toReturn.DurationSeconds = (decimal)x.Ticks / TimeSpan.TicksPerSecond;
                    }
                    else
                    {
                        toReturn.StartTime = startPortion.TimeStamp;
                        toReturn.EndTime = endPortion.TimeStamp;
                    }
                }

                toReturn.NumberSensors = uniqueSensors.Count;

                if ((toReturn.Type == FlashType.FlashTypeCG || toReturn.Type == FlashType.FlashTypeGlobalCG) && brightestCg != null)
                {
                    toReturn.Latitude = brightestCg.Latitude;
                    toReturn.Longitude = brightestCg.Longitude;
                    toReturn.Height = 0;
                    toReturn.Amplitude = brightestCg.Amplitude;
                    toReturn.TimeStamp = brightestCg.TimeStamp;
                    toReturn.Confidence = brightestCg.Confidence;

                }
                else if (toReturn.Type == FlashType.FlashTypeIC && brightestIc != null)
                {
                    toReturn.Latitude = brightestIc.Latitude;
                    toReturn.Longitude = brightestIc.Longitude;
                    toReturn.Height = brightestIc.Height;
                    toReturn.Amplitude = brightestIc.Amplitude;
                    toReturn.TimeStamp = brightestIc.TimeStamp;
                    toReturn.Confidence = brightestIc.Confidence;
                }
            }

            return toReturn;
        }
    }
}
