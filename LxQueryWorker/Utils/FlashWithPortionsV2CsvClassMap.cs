﻿using CsvHelper.Configuration;
using CsvHelper.TypeConversion;
using LxCommon;
using LxQueryWorker.Model;

namespace LxQueryWorker.Utils
{
    public sealed class FlashWithPortionsV2CsvClassMap : CsvClassMap<FlashV2>
    {
        private const string DateTimeFormat = "yyyy-MM-ddTHH:mm:ss.FFFFFFF";

        public FlashWithPortionsV2CsvClassMap()
        {
            Map(f => f.FlashType).Name("flashType").TypeConverter<EnumConverter<FlashType>>();
            Map(f => f.Time).Name("time").TypeConverter<DateTimeConverter>().TypeConverterOption(DateTimeFormat);
            Map(f => f.Latitude).Name("latitude");
            Map(f => f.Longitude).Name("longitude");
            Map(f => f.PeakCurrent).Name("peakcurrent").Default(null);
            Map(f => f.IcHeight).Name("icheight").Default(null);
            Map(f => f.NumberSensors).Name("numbersensors").Default(null);
            Map(f => f.Multiplicity).Name("multiplicity").Default(null);
            References<PortionV2CsvClassMap>(f => f.Portions).Prefix("portion.");
        }
    }
}
