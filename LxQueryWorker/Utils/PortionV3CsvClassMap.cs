﻿using CsvHelper.Configuration;
using CsvHelper.TypeConversion;
using LxCommon;
using LxQueryWorker.Model;

namespace LxQueryWorker.Utils
{
    public sealed class PortionV3CsvClassMap : CsvClassMap<PortionV3>
    {

        private const string DateTimeFormat = "yyyy-MM-ddTHH:mm:ss.FFFFFFF";
        //2015-11-21T00:29:01.164035773
        public PortionV3CsvClassMap()
        {
            Map(f => f.Type).Name("type").TypeConverter<EnumConverter<FlashType>>();
            Map(f => f.Timestamp).Name("timestamp").TypeConverter<DateTimeConverter>().TypeConverterOption(DateTimeFormat);
            Map(f => f.Latitude).Name("latitude");
            Map(f => f.Longitude).Name("longitude");
            Map(f => f.Amplitude).Name("peakcurrent").Default(null);
            Map(f => f.Height).Name("icheight").Default(null);
            Map(f => f.NumberSensors).Name("numbersensors").Default(null);
            References<ErrorEllipseV3ClassMap>(f => f.ErrorEllipse);
            
            //Map(f => f.ErrorEllipse.MajorAxis).Name("majoraxis");
            //Map(f => f.ErrorEllipse.MinorAxis).Name("minoraxis");
            //Map(f => f.ErrorEllipse.MajorAxisBearing).Name("bearing");
        }
    }

}
