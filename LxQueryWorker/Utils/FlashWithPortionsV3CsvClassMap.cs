﻿using CsvHelper.Configuration;
using CsvHelper.TypeConversion;
using LxCommon;
using LxQueryWorker.Model;

namespace LxQueryWorker.Utils
{
    public sealed class FlashWithPortionsV3CsvClassMap : CsvClassMap<FlashV3>
    {

        private const string DateTimeFormat = "yyyy-MM-ddTHH:mm:ss.FFFFFFF";
        //2015-11-21T00:29:01.164035773
        public FlashWithPortionsV3CsvClassMap()
        {
            Map(f => f.Type).Name("type").TypeConverter<EnumConverter<FlashType>>();
            Map(f => f.Timestamp).Name("timestamp").TypeConverter<DateTimeConverter>().TypeConverterOption(DateTimeFormat);
            Map(f => f.Latitude).Name("latitude");
            Map(f => f.Longitude).Name("longitude");
            Map(f => f.PeakCurrent).Name("peakcurrent").Default(null);
            Map(f => f.Height).Name("icheight").Default(null);
            Map(f => f.NumberSensors).Name("numbersensors").Default(null);
            Map(f => f.IcMultiplicity).Name("icmultiplicity").Default(null);
            Map(f => f.CgMultiplicity).Name("cgmultiplicity").Default(null);
            Map(f => f.StartTime).Name("starttime").TypeConverter<DateTimeConverter>().TypeConverterOption(DateTimeFormat);
            Map(f => f.EndTime).Name("endtime").TypeConverter<DateTimeConverter>().TypeConverterOption(DateTimeFormat);
            Map(f => f.DurationSeconds).Name("duration");
            Map(f => f.MaxLatitude).Name("ullatitude");
            Map(f => f.MaxLongitude).Name("ullongitude");
            Map(f => f.MinLatitude).Name("lrlatitude");
            Map(f => f.MinLongitude).Name("lrlongitude");
            References<PortionV3CsvClassMap>(f => f.Portions).Prefix("portion.");
        }
    }
}
