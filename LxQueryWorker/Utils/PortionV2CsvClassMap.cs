﻿using CsvHelper.Configuration;
using CsvHelper.TypeConversion;
using LxCommon;
using LxQueryWorker.Model;

namespace LxQueryWorker.Utils
{
    public sealed class PortionV2CsvClassMap : CsvClassMap<PortionV2>
    {
        private const string DateTimeFormat = "yyyy-MM-ddTHH:mm:ss.FFFFFFF";

        public PortionV2CsvClassMap()
        {
            Map(f => f.PulseType).Name("pulseType").TypeConverter<EnumConverter<FlashType>>();
            Map(f => f.Time).Name("time").TypeConverter<DateTimeConverter>().TypeConverterOption(DateTimeFormat);
            Map(f => f.Latitude).Name("latitude");
            Map(f => f.Longitude).Name("longitude");
            Map(f => f.PeakCurrent).Name("peakcurrent").Default(null);
            Map(f => f.IcHeight).Name("icheight").Default(null);
            Map(f => f.NumberOfSensors).Name("numberofsensors").Default(null);
            Map(f => f.Multiplicity).Name("multiplicity").Default(null);
        }
    }
}
