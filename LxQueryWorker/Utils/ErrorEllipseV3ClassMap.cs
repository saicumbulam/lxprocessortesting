﻿using CsvHelper.Configuration;
using LxQueryWorker.Model;

namespace LxQueryWorker.Utils
{
    public sealed class ErrorEllipseV3ClassMap : CsvClassMap<ErrorEllipseV3>
    {
        public ErrorEllipseV3ClassMap()
        {
            Map(ee => ee.MajorAxis).Name("majoraxis").Default("");
            Map(ee => ee.MinorAxis).Name("minoraxis").Default("");
            Map(ee => ee.MajorAxisBearing).Name("bearing").Default("");
        }
    }
}
