﻿using System;
using CsvHelper.TypeConversion;

namespace LxQueryWorker.Utils
{
    public class EnumConverter<T> : DefaultTypeConverter where T : struct
    {
        public override string ConvertToString(TypeConverterOptions options, object value)
        {
            T result;
            if (Enum.TryParse(value.ToString(), out result))
            {
                return (Convert.ToInt32(result)).ToString();
            }
            throw new InvalidCastException($"Invalid value to EnumConverter. Type: {typeof(T)} Value: {value}");
        }
    }
}
