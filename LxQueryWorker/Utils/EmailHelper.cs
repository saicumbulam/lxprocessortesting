﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Aws.Core.Utilities;
using En.Ods.Lib.Common.Dynamo;
using En.Ods.Lib.Common.Interface;
using En.Ods.Lib.Common.Serialization;
using LxPortalLib.Models;
using LxQueryWorker.Model;

namespace LxQueryWorker.Utils
{
    internal class EmailHelper
    {
        private static readonly IEmailSender Sender;

        static EmailHelper()
        {
            ServicePointManager.DefaultConnectionLimit = 1000;

            Sender = new SesFactory().GetEmailSender(new JsonOdsValue());
        }

        public static async Task SendEmail(LxQueryJobData lxQueryJobData, EmailTemplate template)
        {
            try
            {
                var result = await Sender.SendMessageAsync(template.FromAddress,
                                                 template.Subject,
                                                 template.MessageBody,
                                                 true,
                                                 lxQueryJobData.RecipientEmailAddress.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries)
                                                );
                if (result != null && result["Success"].IsBool)
                {
                    var requestId = result["RequestId"].AsString;
                    var messageId = result["MessageId"].AsString;

                    if (result["Success"].AsBool)
                        AppLogger.LogMessage($"[{lxQueryJobData.JobId}] EmailHelper.SendEmail() - Succeeded sending email. Subject: {template.Subject}, To: {lxQueryJobData.RecipientEmailAddress}, RequestId: {requestId}, MessageId: {messageId}");
                    else
                        AppLogger.LogError($"[{lxQueryJobData.JobId}] EmailHelper.SendEmail() - Failed sending email. Subject: {template.Subject}, To: {lxQueryJobData.RecipientEmailAddress}, RequestId: {requestId}, MessageId: {messageId}", null);
                }
                else
                    AppLogger.LogError($"[{lxQueryJobData.JobId}] EmailHelper.SendEmail() - Unknown result sending email. Subject: {template.Subject}, To: {lxQueryJobData.RecipientEmailAddress}", null);
            }
            catch (Exception ex)
            {
                AppLogger.LogError($"[{lxQueryJobData?.JobId ?? "Unknown"}] EmailHelper.SendEmail() - Error sending email. Subject: {template?.Subject ?? "Unknown"}, To: {lxQueryJobData?.RecipientEmailAddress ?? "Unknown"}, Error: {ex.Message}", ex);
                throw;
            }
        }
    }
}
