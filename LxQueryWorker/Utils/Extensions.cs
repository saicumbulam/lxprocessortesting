﻿using System.Threading.Tasks;
using System.Xml;

namespace LxQueryWorker.Utils
{
    public static class Extensions
    {
        public static Task WriteStartElementAsync(this XmlWriter xmlWriter, string localName)
        {
            return xmlWriter.WriteStartElementAsync(null, localName, null);
        }

        public static Task WriteElementStringAsync(this XmlWriter xmlWriter, string localName, string value)
        {
            return xmlWriter.WriteElementStringAsync(null, localName, null, value);
        }
    }
}
