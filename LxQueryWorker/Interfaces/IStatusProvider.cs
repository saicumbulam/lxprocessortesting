﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LxQueryWorker.Common;

namespace LxQueryWorker.Interfaces
{
    public interface IStatusProvider
    {
        List<KeyValuePair<string, string>> GetMonitorigStatus();
    }
}
