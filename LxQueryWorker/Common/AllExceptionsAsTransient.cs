﻿using System;
using Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;

namespace LxQueryWorker.Common
{
    public class AllExceptionsAsTransient : ITransientErrorDetectionStrategy
    {
        public bool IsTransient(Exception ex)
        {
            return true;
        }
    }

    public class AllExceptionsAsTransientAsync : ITransientErrorDetectionStrategy
    {
        public bool IsTransient(Exception ex)
        {
            return !(ex is OperationCanceledException);
        }
    }
}
