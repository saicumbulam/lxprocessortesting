﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LxQueryWorker.Common
{
    public static class EventId
    {
        public static class Program
        {
            public const ushort Global = 1800;
        }

        public static class Process
        {
            public static class MonitoringHandler
            {
                public const ushort StartOpsHttp = 1850;
                public const ushort StopOpsHttp = 1851;
                public const ushort GetFileCount = 1852;
                public const ushort LoadMonitorFile = 1853;
                public const ushort BuildMonitorHtml = 1854;
                public const ushort GetDatafeedStatus = 1855;
            }
        }
    }
}
