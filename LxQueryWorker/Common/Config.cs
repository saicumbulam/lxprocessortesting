﻿using Aws.Core.Utilities.Config;

namespace LxQueryWorker.Common
{
    public class Config : AppConfig
    {
        public static string S3LxDataBucket
        {
            get { return Get<string>("S3LxDataBucket", null); }
        }

        public static string S3LxQueryOutputBucket
        {
            get { return Get<string>("S3LxQueryOutputBucket", null); }
        }

        public static string AwsSnsLxQueryNotifierTopic
        {
            get { return Get<string>("AwsSnsLxQueryNotifierTopic", null); }
        }

        public static string AwsSqsLxQueryQueue
        {
            get { return Get<string>("AwsSqsLxQueryQueue", null); }
        }

        public static string AwsSnsLxQueryJobProcessorTopic
        {
            get { return Get<string>("AwsSnsLxQueryJobProcessorTopic", null); }
        }

        public static string AwsSqsLxQueryJobProcessorQueue
        {
            get { return Get<string>("AwsSqsLxQueryJobProcessorQueue", null); }
        }

        public static string EmailFromAddress
        {
            get { return Get<string>("EmailFromAddress", null); }
        }

        public static string KMZImageFolder
        {
            get { return Get<string>("KMZImageFolder", null); }
        }

        public static string JobFolder
        {
            get { return Get<string>("JobFolder", null); }
        }

        public static int MaxOutputFileSize
        {
            get { return Get<int>("MaxOutputFileSize.Default", 104857600); }
        }

        public static int MaxOutputFileSizeJson
        {
            get { return Get<int>("MaxOutputFileSize.JSON", MaxOutputFileSize); }
        }

        public static int MaxOutputFileSizeCsv
        {
            get { return Get<int>("MaxOutputFileSize.CSV", MaxOutputFileSize); }
        }

        public static int MaxKmlPlacemarks
        {
            get { return Get<int>("MaxKmlPlacemarks", 10000); }
        }

        public static int PresignedUrlExpirationDays
        {
            get { return Get<int>("PresignedUrlExpirationDays", 7); }
        }

        public static bool StoreResultsInS3
        {
            get { return Get<bool>("StoreResultsInS3", true); }
        }

        public static int MinutesBetweenProgressNotifications
        {
            get { return Get<int>("MinutesBetweenProgressNotifications", 5); }
        }

        public static int MonitorCountResetHours
        {
            get { return Get<int>("MonitorCountResetHours", 24); }
        }

        public static string JobStartedSubject
        {
            get { return Get<string>("JobStartedSubject", null); }
        }

        public static string JobInProgressSubject
        {
            get { return Get<string>("JobInProgressSubject", null); }
        }

        public static string JobCompletedSubject
        {
            get { return Get<string>("JobCompletedSubject", null); }
        }

        public static string JobFailedSubject
        {
            get { return Get<string>("JobFailedSubject", null); }
        }

        public static string JobInvalidSubject
        {
            get { return Get<string>("JobInvalidSubject", null); }
        }

        public static string CacheFolder
        {
            get { return Get<string>("CacheFolder", null); }
        }

        public static int MaxLineReaderTasks
        {
            get { return Get<int>("MaxLineReaderTasks", 10); }
        }

        public static int MaxFileDownloadTasks
        {
            get { return Get <int> ("MaxFileDownloadTasks", 10); }
        }

        public static int MaxFileProcessorTasks
        {
            get { return Get<int>("MaxFileProcessorTasks", 10); }
        }

        public static int MaxFileDownloadRetries
        {
            get { return Get<int>("MaxFileDownloadRetries", 5); }
        }

        public static int FileDownloadRetryInitialIntervalSeconds
        {
            get { return Get<int>("FileDownloadRetryInitialIntervalSeconds", 5); }
        }

        public static int FileDownloadRetryIncrementSeconds
        {
            get { return Get<int>("FileDownloadRetryIncrementSeconds", 10); }
        }

        public static bool SaveProcessedArchiveFiles
        {
            get { return Get<bool>("SaveProcessedArchiveFiles", false); }
        }

        public static int FlashBatchSize
        {
            get { return Get<int>("FlashBatchSize.Default", 20); }
        }

        public static int FlashBatchSizeCsv
        {
            get { return Get<int>("FlashBatchSize.Csv", FlashBatchSize); }
        }

        public static int FlashBatchSizeJson
        {
            get { return Get<int>("FlashBatchSize.Json", FlashBatchSize); }
        }

        public static int MaxConcurrentjobs
        {
            get { return Get<int>("MaxConcurrentjobs", 2); }
        }

        public static int QueueReadDelaySeconds
        {
            get { return Get<int>("QueueReadDelaySeconds", 30); }
        }

        public static int JobNotificationCleanUpFrequencyMinutes
        {
            get { return Get<int>("JobNotificationCleanUpFrequencyMinutes", 5); }
        }
    }
}