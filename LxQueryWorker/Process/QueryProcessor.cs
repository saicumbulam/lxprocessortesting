﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using Aws.Core.Utilities;
using Aws.Core.Utilities.Spatial;
using CsvHelper;
using En.Ods.Lib.Common.Dynamo;
using En.Ods.Lib.Common.Interface;
using En.Ods.Lib.Common.Serialization;
using LxCommon;
using LxCommon.Models;
using LxCommon.Utilities;
using LxPortalLib.Models;
using LxQueryWorker.Common;
using LxQueryWorker.Model;
using LxQueryWorker.Utils;
using Newtonsoft.Json;
using System.Xml;
using System.Text;
using System.Threading.Tasks;
using LxQueryWorker.MailT4Templates;
using System.IO.Compression;
using System.Net;
using System.Threading;
using Timer = System.Timers.Timer;
using Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;
using MoreLinq;

namespace LxQueryWorker.Process
{
    public class QueryProcessor
    {
        private readonly ICloudStorage _storage;
        private readonly string _s3LxDataBucket;
        private readonly string _s3LxQueryOutputBucket;
        private const int LevelOfDetail = 5;
        private int _invalidQueryCount = 0;
        private int _failedQueryCount = 0;
        private int _successQueryCount = 0;
        private int _totalQueryCount = 0;
        private DateTime _lastQueryProcessedUtc;
        private readonly int _progressIntervalMinutes;

        private readonly int _maxFileDownloadTasks;
        private readonly int _maxLineReaderTasks;
        private readonly int _maxFileProcessorTasks;

        private readonly int _maxOutputFileSizeJson;
        private readonly int _maxOutputFileSizeCsv;

        private readonly int _maxKmlPlacemarks;

        private readonly int _maxFileDownloadRetries;
        private readonly int _fileDownloadRetryInitialIntervalSeconds;
        private readonly int _fileDownloadRetryIncrementSeconds;

        private const int DefaultCopyBufferSize = 81920; // using the buffer size defined in Stream.cs as _DefaultCopyBufferSize
        private const string JsonFileExtension = "json";
        private const string CsvFileExtension = "csv";
        private const string KmzFileExtension = "kmz";
        private const string KmlFileExtension = "kml";

        private readonly int _flashBatchSizeJson;
        private readonly int _flashBatchSizeCsv;

        private readonly string _jobFolder;

        static QueryProcessor()
        {
            ServicePointManager.DefaultConnectionLimit = 1000;
        }

        public QueryProcessor(string s3LxDataBucket, string s3LxQueryOutputBucket)
        {
            if (string.IsNullOrWhiteSpace(s3LxDataBucket))
                throw new ArgumentNullException(nameof(s3LxDataBucket));

            if (string.IsNullOrWhiteSpace(s3LxQueryOutputBucket))
                throw new ArgumentNullException(nameof(s3LxQueryOutputBucket));

            _s3LxDataBucket = s3LxDataBucket;
            _s3LxQueryOutputBucket = s3LxQueryOutputBucket;
            _lastQueryProcessedUtc = DateTime.UtcNow;
            _progressIntervalMinutes = Config.MinutesBetweenProgressNotifications;
          
            _maxLineReaderTasks = Config.MaxLineReaderTasks;
            _maxFileDownloadTasks = Config.MaxFileDownloadTasks;
            _maxFileProcessorTasks = Config.MaxFileProcessorTasks;

            _maxOutputFileSizeJson = Config.MaxOutputFileSizeJson;
            _maxOutputFileSizeCsv = Config.MaxOutputFileSizeCsv;

            _maxKmlPlacemarks = Config.MaxKmlPlacemarks;

            _maxFileDownloadRetries = Config.MaxFileDownloadRetries;
            _fileDownloadRetryInitialIntervalSeconds = Config.FileDownloadRetryInitialIntervalSeconds;
            _fileDownloadRetryIncrementSeconds = Config.FileDownloadRetryIncrementSeconds;

            _flashBatchSizeJson = Config.FlashBatchSizeJson;
            _flashBatchSizeCsv = Config.FlashBatchSizeCsv;

            _jobFolder = Config.JobFolder;

            _storage = new S3Factory().GetStorage(new JsonOdsValue());
        }

        public void Start()
        {
            PurgeJobFolderRoot();
        }

        public void Stop()
        {

        }

        public async Task<bool> Process(LxQueryJobData lxQueryJobData, CancellationToken ct, bool keepLocal = false)
        {
            if (lxQueryJobData == null)
            {
                _invalidQueryCount++;
                throw new ArgumentNullException(nameof(lxQueryJobData));
            }

            ct.ThrowIfCancellationRequested();

            _totalQueryCount++;

            var progressEmailTimer = new Timer
            {
                Interval = TimeSpan.FromMinutes(_progressIntervalMinutes).TotalMilliseconds
            };
            progressEmailTimer.Elapsed += (sender, e) => { SendProgressEmail(lxQueryJobData).GetAwaiter().GetResult(); };

            if (lxQueryJobData.SendProgressNotifications.HasValue && lxQueryJobData.SendProgressNotifications.Value)
            {
                progressEmailTimer.AutoReset = true;
                progressEmailTimer.Enabled = true;
            }
            else
            {
                progressEmailTimer.AutoReset = false;
                progressEmailTimer.Enabled = false;
            }


            /*
            The LxQueryWorker should allow for 3 different LX types to be requested (UI will need to be updated to reflect these):
            •	Pulse only
            •	Flash only
            •	Pulse and Flash combined

            The LxQueryWorker should allow for 3 different output formats: JSON (already does this), CSV, and KMZ. These output formats are dependent on the LX type requested:
            •	Pulse only
                o	JSON
                o	CSV
                o	KMZ
            •	Flash only
                o	JSON
                o	CSV
                o	KMZ
            •	Pulse and Flash Combined
                o	JSON
            */

            // Validate request combination
            if (lxQueryJobData.LxType == LxType.Both && lxQueryJobData.OutputType != FileOutputType.Json)
            {
                _invalidQueryCount++;
                if (lxQueryJobData.SendNotifications.HasValue && lxQueryJobData.SendNotifications.Value)
                {
                    await SendFailureEmail(lxQueryJobData, false, "Cannot request combined Pulse and Flash as anything other JSON file type");
                }
                DisposeProgressTimer(progressEmailTimer, lxQueryJobData.JobId);
                var ex =  new InvalidOperationException($"[{lxQueryJobData.JobId}] Cannot request combined Pulse and Flash as anything other JSON file type");
                AppLogger.LogError($"[{lxQueryJobData.JobId}] Failed to process job due to invalid request combination. LxType: {lxQueryJobData.LxType}, OutputType: {lxQueryJobData.OutputType}", ex);
                throw ex;
            }

            // Ensure End date is greater than Start date
            if (lxQueryJobData.Start == lxQueryJobData.End)
            {
                lxQueryJobData.End = lxQueryJobData.Start == lxQueryJobData.Start.Date
                                     ? lxQueryJobData.End.AddDays(1).AddMilliseconds(-1)
                                     : lxQueryJobData.End.AddMilliseconds(1);
            }
            else if (lxQueryJobData.Start > lxQueryJobData.End)
            {
                _invalidQueryCount++;
                if (lxQueryJobData.SendNotifications.HasValue && lxQueryJobData.SendNotifications.Value)
                {
                    await SendFailureEmail(lxQueryJobData, false, "EndDate must be greater than StartDate");
                }
                DisposeProgressTimer(progressEmailTimer, lxQueryJobData.JobId);
                var ex = new InvalidOperationException($"[{lxQueryJobData.JobId}] EndDate must be greater than StartDate");
                AppLogger.LogError($"[{lxQueryJobData.JobId}] Failed to process job due to invalid request combination. LxType: {lxQueryJobData.LxType}, OutputType: {lxQueryJobData.OutputType}", ex);
                throw ex;
            }

            var flashesCount = 0L;
            var portionsCount = 0L;

            try
            {
                // Create temp job folder
                var jobFolderPath = GetJobFolder(lxQueryJobData.JobId);

                // Compute quad keys covering the query area
                var quadKeys = GetQuadKeys(lxQueryJobData);
                AppLogger.LogMessage($"[{lxQueryJobData.JobId}] quad keys = {string.Join(", ", quadKeys)}");

                var sameYear = lxQueryJobData.Start.AddDays(-1).Year == lxQueryJobData.End.AddDays(1).Year;
                var sameMonth = lxQueryJobData.Start.AddDays(-1).Month == lxQueryJobData.End.AddDays(1).Month;

                var pathDateLimiter = string.Empty;
                if (sameYear)
                {
                    pathDateLimiter = $"/{lxQueryJobData.Start.Year}";
                    if (sameMonth && !(lxQueryJobData.Start.Year == 2017 && lxQueryJobData.Start.Month == 1)) // if Jan 2017, do not set limiter due to month path issue. this is a temporary fix
                    {
                        pathDateLimiter += $"/{lxQueryJobData.Start.Month}";
                    }
                }

                var outputFileList = new List<string>();
                var kmzFilesPlacemarkCounts = new Dictionary<string, long>();

                var outputIndex = 0;
                foreach (var quadKey in quadKeys)
                {
                    ct.ThrowIfCancellationRequested();
                    
                    var flashes = new List<Flash>();
                    // If today data is requested, get from database
                    if (lxQueryJobData.End.Date >= DateTime.UtcNow.Date)
                    {
                        AppLogger.LogMessage($"[{lxQueryJobData.JobId}] Getting Lx data from DB. Query start: {lxQueryJobData.Start}, query end: {lxQueryJobData.End}, quad key = {quadKey}");
                        List<Flash> dbFlashes;
                        if (lxQueryJobData.Start >= DateTime.UtcNow.Date)
                        {
                            dbFlashes = GetDbData(lxQueryJobData.JobId, quadKey, lxQueryJobData.Start, lxQueryJobData.End, lxQueryJobData);
                        }
                        else
                        {
                            dbFlashes = GetDbData(lxQueryJobData.JobId, quadKey, DateTime.UtcNow.Date, lxQueryJobData.End, lxQueryJobData);
                        }
                        AppLogger.LogMessage($"[{lxQueryJobData.JobId}] Getting Lx data from DB completed. quad key = {quadKey}");
                        flashes.AddRange(dbFlashes);
                    }
                    // Get data from S3 if older data needed
                    if (lxQueryJobData.Start.Date < DateTime.UtcNow.Date)
                    {
                        var pathPrefix = $"{quadKey}{pathDateLimiter}";
                        AppLogger.LogDebug($"[{lxQueryJobData.JobId}] Getting Lx data from S3. Query start date: {lxQueryJobData.Start}, query end date: {lxQueryJobData.End}, quad key = {quadKey}");
                        var cloudFlashes = await GetCloudData(jobFolderPath, pathPrefix, lxQueryJobData, ct);
                        AppLogger.LogDebug($"[{lxQueryJobData.JobId}] Getting Lx data from S3 completed.");
                        flashes.AddRange(cloudFlashes);
                    }
                    
                    if (flashes?.Count > 0)
                    {
                        if (lxQueryJobData.LxType != LxType.Portion) flashesCount += flashes.Count;

                        var generatedOutout = new GeneratedOutput();
                        switch (lxQueryJobData.OutputType)
                        {
                            case FileOutputType.Csv:
                                {
                                    generatedOutout = await CreateCsvOutputFile(flashes, jobFolderPath, lxQueryJobData, outputIndex, ct);
                                    portionsCount += generatedOutout.Counter;
                                    break;
                                }

                            case FileOutputType.Kmz:
                                {
                                    generatedOutout = await CreateKmzFile(flashes, jobFolderPath, lxQueryJobData, outputIndex, kmzFilesPlacemarkCounts, ct);
                                    portionsCount += generatedOutout.Counter;
                                    break;
                                }

                            case FileOutputType.Json:
                                {
                                    generatedOutout = await CreateJsonOutputFile(flashes, jobFolderPath, lxQueryJobData, outputIndex, ct);
                                    portionsCount += generatedOutout.Counter;
                                    break;
                                }
                        }

                        outputIndex = generatedOutout.CurrentIndex;

                        foreach (var fileName in generatedOutout.Files.Where(fileName => !string.IsNullOrWhiteSpace(fileName) && !outputFileList.Contains(fileName)))
                        {
                            outputFileList.Add(fileName);
                        }
                    }
                }

                if (lxQueryJobData.OutputType == FileOutputType.Kmz)
                {
                    foreach (var outputFile in outputFileList)
                    {
                        FinalizeKmzFile(outputFile);
                    }
                }

                // Upload files to S3
                if (Config.StoreResultsInS3)
                {
                    var outputUrlList = new ConcurrentBag<string>();
                    var fileUploadTasks = new List<Task>();

                    foreach (var outputFileSrc in outputFileList)
                    {
                        ct.ThrowIfCancellationRequested();

                        if (fileUploadTasks.Count >= _maxFileDownloadTasks)
                        {
                            await Task.WhenAll(fileUploadTasks.ToArray<Task>());
                            fileUploadTasks.Clear();
                        }

                        var outputFile = outputFileSrc;
                        var t = Task.Run(async () =>
                        {
                            ct.ThrowIfCancellationRequested();

                            var fileInfo = new FileInfo(outputFile);
                            if (fileInfo.Exists)
                            {
                                var pathOnS3 = $"{lxQueryJobData.JobId}/{fileInfo.Name}";

                                AppLogger.LogDebug($"[{lxQueryJobData.JobId}] Uploading generated file to cloud. File: {fileInfo.Name}, Destination: {pathOnS3}");

                                var retryStrategy = new Incremental(_maxFileDownloadRetries,
                                                                    TimeSpan.FromSeconds(_fileDownloadRetryInitialIntervalSeconds),
                                                                    TimeSpan.FromSeconds(_fileDownloadRetryIncrementSeconds));
                                var retryPolicy = new RetryPolicy<AllExceptionsAsTransientAsync>(retryStrategy);

                                retryPolicy.Retrying += (sender, e) =>
                                {
                                    var jobId = lxQueryJobData.JobId;
                                    var of = outputFile;
                                    AppLogger.LogWarning($"[{jobId}] Retry upload file to cloud: {of}, Count: {e.CurrentRetryCount}, Delay: {e.Delay}, Destination: {pathOnS3}", e.LastException);
                                };

                                await retryPolicy.ExecuteAsync(async () =>
                                {
                                    ct.ThrowIfCancellationRequested();

                                    using (var fileStream = new FileStream(fileInfo.FullName, FileMode.Open, FileAccess.Read, FileShare.Read, DefaultCopyBufferSize, FileOptions.Asynchronous))
                                    {
                                        string mimeType = null;
                                        switch (lxQueryJobData.OutputType)
                                        {
                                            //case FileOutputType.Csv:
                                            //    mimeType = "text/csv";
                                            //    break;
                                            case FileOutputType.Kmz:
                                                mimeType = "application/vnd.google-earth.kmz";
                                                break;
                                                //case FileOutputType.Json:
                                                //    mimeType = "application/json";
                                                //    break;
                                        }
                                        await _storage.PutObjectAsync(_s3LxQueryOutputBucket, pathOnS3, fileStream, mimeType).ConfigureAwait(false);
                                    }

                                    ct.ThrowIfCancellationRequested();

                                    var signedUrl = await _storage.GetPresignedUrlAsync(_s3LxQueryOutputBucket, pathOnS3, Config.PresignedUrlExpirationDays * 1440).ConfigureAwait(false);
                                    outputUrlList.Add(signedUrl);
                                    
                                }, ct);
                            }
                        }, ct);

                        fileUploadTasks.Add(t);
                    }
                    await Task.WhenAll(fileUploadTasks.ToArray<Task>());
                    fileUploadTasks.Clear();

                    AppLogger.LogDebug($"[{lxQueryJobData.JobId}] Presigned Urls: {string.Join(", ", outputUrlList)}");

                    if (lxQueryJobData.SendNotifications.HasValue
                        && lxQueryJobData.SendNotifications.Value)
                    {
                        await SendCompletionEmail(lxQueryJobData, outputUrlList.ToList(), flashesCount, portionsCount);
                    }

                    // Delete temp job folder
                    Directory.Delete(jobFolderPath, true);

                    _lastQueryProcessedUtc = DateTime.UtcNow;
                    _successQueryCount++;
                }
                DisposeProgressTimer(progressEmailTimer, lxQueryJobData.JobId);
            }
            catch (Exception ex)
            {
                _failedQueryCount++;
                if (lxQueryJobData.SendNotifications.HasValue
                    && lxQueryJobData.SendNotifications.Value)
                {
                    await SendFailureEmail(lxQueryJobData, true, ex.Message);
                }
                DisposeProgressTimer(progressEmailTimer, lxQueryJobData.JobId);
                AppLogger.LogError($"[{lxQueryJobData.JobId}] Error in processing job due to exception: {ex.Message}", ex);
                throw;
            }
            return true;
        }

        public List<Flash> GetDbData(string jobId, string quadKey, DateTime start, DateTime end, LxQueryJobData lxQueryJobData)
        {
            var reader = new DataReader(jobId);
            var result = reader.Read(quadKey, start, end);
            var validFlash = result.Where(x => ValidateFlash(x, lxQueryJobData)).ToList();
            AppLogger.LogMessage($"[{jobId}] Valid flashes count = {validFlash.Count}, quad key {quadKey}");

            return validFlash;
        }

        public void PurgeJobFolder(string jobId)
        {
            if (string.IsNullOrWhiteSpace(jobId)) throw new ArgumentNullException(nameof(jobId));

            var jobFolderPath = Path.Combine(_jobFolder, jobId);
            if (Directory.Exists(jobFolderPath))
                Directory.Delete(jobFolderPath, true);
        }

        private void PurgeJobFolderRoot()
        {
            if (Directory.Exists(_jobFolder))
                Directory.Delete(_jobFolder, true);
        }

        private string GetJobFolder(string jobId, bool createNew = true)
        {
            if (string.IsNullOrWhiteSpace(jobId)) throw new ArgumentNullException(nameof(jobId));

            var jobFolderPath = Path.Combine(_jobFolder, jobId);
            if (createNew && Directory.Exists(jobFolderPath)) Directory.Delete(jobFolderPath, true);
            if (createNew) Directory.CreateDirectory(jobFolderPath);

            return jobFolderPath;
        }

        private async Task<List<Flash>> GetCloudData(string jobFolder, string pathPrefix, LxQueryJobData lxQueryJobData, CancellationToken ct)
        {
            var tasks = new List<Task>();
            var cloudStorageKeys = new List<ICloudStorageKey>();
            var validFlashes = new ConcurrentBag<Flash>();

            await _storage.GetKeysAsync(_s3LxDataBucket, pathPrefix, ct).ForEach(
                cloudStorageKey =>
                {
                    ct.ThrowIfCancellationRequested();

                    // Check file name to reduce file reads
                    var s3Path = cloudStorageKey.Path;
                    if (s3Path.Substring(s3Path.LastIndexOf('/') + 1).Length < 25)
                    {
                        return;
                    }
                    var timeStamp = s3Path.Substring(s3Path.LastIndexOf('/') + 1).Substring(6, 19);

                    // Replace time '-' with ':'
                    var arr = timeStamp.ToCharArray();
                        arr[13] = ':';
                        arr[16] = ':';
                        var dt = Convert.ToDateTime(new string(arr));

                    // 1 day buffer
                    if (dt.Date < lxQueryJobData.Start.Date.AddDays(-1) || dt.Date > lxQueryJobData.End.Date.AddDays(1))
                    {
                        // S3 file too old or too new
                        AppLogger.LogDebug($"[{lxQueryJobData.JobId}] Skipping cloud file {cloudStorageKey.Path} due to being too old or new. Start: {lxQueryJobData.Start.Date.AddDays(-1)}, End: {lxQueryJobData.End.Date.AddDays(1)}");
                    }
                    else
                    {
                        AppLogger.LogDebug($"[{lxQueryJobData.JobId}] Adding cloud file {cloudStorageKey.Path} to cloudStorageKeys");
                        cloudStorageKeys.Add(cloudStorageKey);
                    }
                }
            );

            if(cloudStorageKeys.Any())
            { 
                foreach (var cloudStorageKey in cloudStorageKeys)
                {
                    ct.ThrowIfCancellationRequested();

                    if (tasks.Count >= _maxFileDownloadTasks)
                    {
                        await Task.WhenAll(tasks.ToArray<Task>());
                        tasks.Clear();
                    }

                    var csk = cloudStorageKey;

                    var task = Task.Run(async () =>
                    {
                        ct.ThrowIfCancellationRequested();

                        AppLogger.LogDebug($"[{lxQueryJobData.JobId}] Downloading cloud file: {csk.Path}");

                        var outputFile = new FileInfo(Path.Combine(jobFolder, $"{csk.Path.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar)}"));
                        if (!outputFile.Directory.Exists) outputFile.Directory.Create();

                        var retryStrategy = new Incremental(_maxFileDownloadRetries,
                                                            TimeSpan.FromSeconds(_fileDownloadRetryInitialIntervalSeconds),
                                                            TimeSpan.FromSeconds(_fileDownloadRetryIncrementSeconds));
                        var retryPolicy = new RetryPolicy<AllExceptionsAsTransientAsync>(retryStrategy);

                        retryPolicy.Retrying += (sender, e) =>
                        {
                            AppLogger.LogWarning($"[{lxQueryJobData.JobId}] Retry downloading cloud file: {csk.Path}, Count: {e.CurrentRetryCount}, Delay: {e.Delay}, Output File: {outputFile}", e.LastException);
                        };
                    
                        await retryPolicy.ExecuteAsync(async () =>
                        {
                            ct.ThrowIfCancellationRequested();

                            using (var obj = await _storage.GetObjectAsync(_s3LxDataBucket, csk.Path).ConfigureAwait(false))
                            {
                                ct.ThrowIfCancellationRequested();

                                using (var fileStream = new FileStream(outputFile.FullName, FileMode.Create, FileAccess.Write, FileShare.Read, DefaultCopyBufferSize, FileOptions.Asynchronous))
                                {
                                    AppLogger.LogDebug($"[{lxQueryJobData.JobId}] Storing cloud file to local disk. Cloud file: {csk.Path}, Local file {outputFile.Name}");
                                    await obj.Stream.CopyToAsync(fileStream, DefaultCopyBufferSize, ct).ConfigureAwait(false);
                                    fileStream.Flush(true);
                                    fileStream.Close();
                                }
                            }

                            var downloadedFile = new FileInfo(outputFile.FullName);
                            AppLogger.LogDebug($"[{lxQueryJobData.JobId}] Finished storing cloud file to local disk. Cloud file: {csk.Path}, Local file {downloadedFile.Name}, Local file size: {downloadedFile.Length}");

                        }, ct);

                    }, ct);

                    tasks.Add(task);
                }
                await Task.WhenAll(tasks.ToArray<Task>());
                tasks.Clear();

                foreach (var cloudStorageKey in cloudStorageKeys.OrderBy(i => i.Path.Substring(i.Path.LastIndexOf("/", StringComparison.Ordinal) + 1)))
                {
                    ct.ThrowIfCancellationRequested();

                    if (tasks.Count >= _maxFileProcessorTasks)
                    {
                        await Task.WhenAll(tasks.ToArray<Task>());
                        tasks.Clear();
                    }

                    var csk = cloudStorageKey;

                    var task = Task.Run(async () =>
                    {
                        ct.ThrowIfCancellationRequested();

                        AppLogger.LogDebug($"[{lxQueryJobData.JobId}] Processing cloud file: {csk.Path}");

                        var flashes = new ConcurrentBag<Flash>();
                        var linesRead = 0;

                        var outputFile = new FileInfo(Path.Combine(jobFolder, $"{csk.Path.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar)}"));

                        using (var fileStream = new FileStream(outputFile.FullName, FileMode.Open, FileAccess.Read, FileShare.Read, DefaultCopyBufferSize, FileOptions.Asynchronous))
                        {
                            fileStream.Seek(0, SeekOrigin.Begin);

                            using (var reader = new StreamReader(fileStream))
                            {
                                var lineReaderTasks = new List<Task>();

                                while (!reader.EndOfStream)
                                {
                                    ct.ThrowIfCancellationRequested();

                                    if (lineReaderTasks.Count >= _maxLineReaderTasks)
                                    {
                                        await Task.WhenAll(lineReaderTasks.ToArray<Task>());
                                        lineReaderTasks.Clear();
                                    }

                                    var line = await reader.ReadLineAsync();
                                    linesRead++;

                                    var lineReaderTask = Task.Run(() =>
                                    {
                                        var flash = default(Flash);
                                        if (IsValidLine(lxQueryJobData, line, outputFile, out flash))
                                            flashes.Add(flash);

                                    }, ct);

                                    lineReaderTasks.Add(lineReaderTask);
                                }

                                await Task.WhenAll(lineReaderTasks.ToArray<Task>());
                                lineReaderTasks.Clear();
                            }
                            fileStream.Close();
                        }

                        AppLogger.LogMessage($"[{lxQueryJobData.JobId}] Finished processing cloud file. File: {csk.Path}, Lines Read: {linesRead}, Valid flashes: {flashes.Count}, Invalid flashes: {linesRead - flashes.Count}");

                        foreach (var flash in flashes)
                        {
                            validFlashes.Add(flash);
                        }
                    }, ct);

                    tasks.Add(task);
                }
                await Task.WhenAll(tasks.ToArray<Task>());
                tasks.Clear();
            }
            //foreach (var flash in validFlashes)
            //{
            //    if (flashes.Find(f => f.TimeStamp.Equals(flash.TimeStamp)
            //                          && f.Type.Equals(flash.Type)
            //                          && f.Source.Equals(flash.Source)
            //                          && f.DurationSeconds.Equals(flash.DurationSeconds)
            //                          && f.Latitude.Equals(flash.Latitude)
            //                          && f.Longitude.Equals(flash.Longitude)
            //                          && f.MaxLatitude.Equals(flash.MaxLatitude)
            //                          && f.MinLatitude.Equals(flash.MinLatitude)
            //                          && f.MaxLongitude.Equals(flash.MaxLongitude)
            //                          && f.MinLongitude.Equals(flash.MinLongitude)
            //                          && f.Height.Equals(flash.Height)
            //                          && f.NumberSensors.Equals(flash.NumberSensors)
            //        ) == null)
            //    {
            //        flashes.Add(flash);
            //    }
            //}

            var dirInfo = new DirectoryInfo(Path.Combine(jobFolder, $"{pathPrefix.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar)}"));
            if (!Config.SaveProcessedArchiveFiles && dirInfo.Exists) dirInfo.Delete(true);
            
            var initialCount = validFlashes.Count;
            var result = new List<Flash>();

            if (validFlashes.Any()) result = validFlashes.OrderBy(f => f.TimeStamp).AsParallel().Distinct().ToList();
            //2016-09-16T01:25:24.139155112
            AppLogger.LogDebug($"[{lxQueryJobData.JobId}] Finished retrieving cloud data. Path prefix: {pathPrefix}, Valid flashes: {initialCount}, Distinct valid flashes: {result.Count}");

            return result;
        }


        #region Output file generators
        private async Task<GeneratedOutput> CreateCsvOutputFile(IReadOnlyCollection<Flash> flashes, string jobFolder, LxQueryJobData lxQueryJobData, int outputIndex, CancellationToken ct)
        {
            ct.ThrowIfCancellationRequested();

            var outputFileList = new List<string>();
            var outputFile = GenerateOutputFileName(lxQueryJobData, jobFolder, CsvFileExtension, ref outputIndex);
            //var lastNotificationSent = DateTime.MinValue;
            var portionsCount = 0L;

            if (flashes.Count > 0)
            {
                foreach (var flashBatch in flashes.Batch(_flashBatchSizeCsv))
                {
                    ct.ThrowIfCancellationRequested();

                    //if (lxQueryJobData.SendProgressNotifications != null
                    //    && lxQueryJobData.SendNotifications != null
                    //    && lxQueryJobData.SendNotifications.Value
                    //    && lxQueryJobData.SendProgressNotifications.Value
                    //    && lastNotificationSent.AddMinutes(Config.MinutesBetweenProgressNotifications) <= DateTime.UtcNow)
                    //{
                    //    lastNotificationSent = DateTime.UtcNow;
                    //    await SendProgressEmail(lxQueryJobData);
                    //}

                    var fileInfo = new FileInfo(outputFile);
                    var needsHeader = !fileInfo.Exists;
                    if (fileInfo.Exists && fileInfo.Length > _maxOutputFileSizeCsv)
                    {
                        outputIndex++;
                        outputFile = GenerateOutputFileName(lxQueryJobData, jobFolder, CsvFileExtension, ref outputIndex);
                        fileInfo = new FileInfo(outputFile);
                        needsHeader = true;
                    }

                    // Create/Open CSV and add data
                    using (var txtWriter = fileInfo.AppendText())
                    {
                        var csvWriter = new CsvWriter(txtWriter);
                        csvWriter.Configuration.AllowComments = false;
                        csvWriter.Configuration.IgnoreHeaderWhiteSpace = true;
                        csvWriter.Configuration.IsHeaderCaseSensitive = true;
                        csvWriter.Configuration.SkipEmptyRecords = true;
                        csvWriter.Configuration.TrimHeaders = true;
                        csvWriter.Configuration.TrimFields = true;
                        csvWriter.Configuration.HasHeaderRecord = needsHeader;
                        csvWriter.Configuration.BufferSize = DefaultCopyBufferSize;

                        switch (lxQueryJobData.OutputVersion)
                        {
                            case LxOutputVersion.V2:
                                {
                                    var flashesV2 = flashBatch.Select(x => new FlashV2(x));
                                    switch (lxQueryJobData.LxType)
                                    {
                                        case LxType.Both:
                                            {
                                                csvWriter.Configuration.RegisterClassMap<FlashWithPortionsV2CsvClassMap>();
                                                csvWriter.WriteRecords(flashesV2);
                                                portionsCount += flashesV2.Select(flash => flash.Portions).SelectMany(x => x).Count();
                                                break;
                                            }

                                        case LxType.Flash:
                                            {
                                                flashesV2.ForEach(flash => flash.Portions = null);
                                                csvWriter.Configuration.RegisterClassMap<FlashV2CsvClassMap>();
                                                csvWriter.WriteRecords(flashesV2);
                                                break;
                                            }

                                        case LxType.Portion:
                                            {
                                                csvWriter.Configuration.RegisterClassMap<PortionV2CsvClassMap>();
                                                csvWriter.WriteRecords(flashesV2.Select(flash => flash.Portions).SelectMany(x => x));
                                                portionsCount += flashesV2.Select(flash => flash.Portions).SelectMany(x => x).Count();
                                                break;
                                            }
                                    }
                                    break;
                                }
                            case LxOutputVersion.V3:
                            default:
                                {
                                    var flashesV3 = flashBatch.Select(x => new FlashV3(x));

                                    switch (lxQueryJobData.LxType)
                                    {
                                        case LxType.Both:
                                            {
                                                csvWriter.Configuration.RegisterClassMap<FlashWithPortionsV3CsvClassMap>();
                                                csvWriter.WriteRecords(flashesV3);
                                                portionsCount += flashesV3.Select(flash => flash.Portions).SelectMany(x => x).Count();
                                                break;
                                            }

                                        case LxType.Flash:
                                            {
                                                flashesV3.ForEach(flash => flash.Portions = null);
                                                csvWriter.Configuration.RegisterClassMap<FlashV3CsvClassMap>();
                                                csvWriter.WriteRecords(flashesV3);
                                                break;
                                            }

                                        case LxType.Portion:
                                            {
                                                csvWriter.Configuration.RegisterClassMap<PortionV3CsvClassMap>();
                                                csvWriter.WriteRecords(flashesV3.Select(flash => flash.Portions).SelectMany(x => x));
                                                portionsCount += flashesV3.Select(flash => flash.Portions).SelectMany(x => x).Count();
                                                break;
                                            }
                                    }
                                    break;
                                }
                        }

                        if (!outputFileList.Contains(fileInfo.FullName))
                            outputFileList.Add(fileInfo.FullName);
                    }
                }
            }

            return new GeneratedOutput(outputFileList, outputIndex, portionsCount);
        }

        private async Task<GeneratedOutput> CreateJsonOutputFile(IReadOnlyCollection<Flash> flashes, string jobFolder, LxQueryJobData lxQueryJobData, int outputIndex, CancellationToken ct)
        {
            switch (lxQueryJobData.OutputVersion)
            {
                case LxOutputVersion.V2:
                    return await CreateJsonOutputFileV2(flashes, jobFolder, lxQueryJobData, outputIndex, ct);
                case LxOutputVersion.V3:
                default:
                    return await CreateJsonOutputFileV3(flashes, jobFolder, lxQueryJobData, outputIndex, ct);
            }
        }

        private async Task<GeneratedOutput> CreateJsonOutputFileV2(IReadOnlyCollection<Flash> flashes, string jobFolder, LxQueryJobData lxQueryJobData, int outputIndex, CancellationToken ct)
        {
            ct.ThrowIfCancellationRequested();

            var portionsCount = 0L;

            var outputFileList = new List<string>();

            if (flashes?.Count > 0)
            {
                var outputFile = GenerateOutputFileName(lxQueryJobData, jobFolder, JsonFileExtension, ref outputIndex);
                //var lastNotificationSent = DateTime.MinValue;

                var fileInfo = new FileInfo(outputFile);
                var fileWriter = default(StreamWriter);

                try
                {
                    fileWriter = fileInfo.AppendText();
                    fileWriter.AutoFlush = true;

                    foreach (var flashBatch in flashes.Batch(_flashBatchSizeJson).Select(batch => batch.Select(x => new FlashV2(x))))
                    {
                        ct.ThrowIfCancellationRequested();

                        //if (lxQueryJobData.SendProgressNotifications != null
                        //    && lxQueryJobData.SendNotifications != null
                        //    && lxQueryJobData.SendNotifications.Value
                        //    && lxQueryJobData.SendProgressNotifications.Value
                        //    && lastNotificationSent.AddMinutes(Config.MinutesBetweenProgressNotifications) <= DateTime.UtcNow)
                        //{
                        //    lastNotificationSent = DateTime.UtcNow;
                        //    await SendProgressEmail(lxQueryJobData);
                        //}

                        var data = string.Empty;

                        switch (lxQueryJobData.LxType)
                        {
                            case LxType.Both:
                                {
                                    data = string.Join(Environment.NewLine, flashBatch.Select(JsonConvert.SerializeObject));
                                    portionsCount += flashBatch.Select(flash => flash.Portions).SelectMany(x => x).Count();
                                    break;
                                }

                            case LxType.Flash:
                                {
                                    flashBatch.ForEach(flash => flash.Portions = null);
                                    data = string.Join(Environment.NewLine, flashBatch.Select(JsonConvert.SerializeObject));
                                    break;
                                }

                            case LxType.Portion:
                                {
                                    data = string.Join(Environment.NewLine, flashBatch.Select(flash => flash.Portions).SelectMany(x => x).Select(JsonConvert.SerializeObject));
                                    portionsCount += flashBatch.Select(flash => flash.Portions).SelectMany(x => x).Count();
                                    break;
                                }
                        }

                        if (fileInfo.Exists && (fileInfo.Length + data.Length + Environment.NewLine.Length) >= _maxOutputFileSizeJson)
                        {
                            fileWriter.Close();
                            fileWriter.Dispose();

                            outputIndex++;
                            outputFile = GenerateOutputFileName(lxQueryJobData, jobFolder, JsonFileExtension, ref outputIndex);
                            fileInfo = new FileInfo(outputFile);

                            fileWriter = fileInfo.AppendText();
                            fileWriter.AutoFlush = true;
                        }

                        fileWriter.Write(data);
                        fileWriter.Write(Environment.NewLine);

                        if (!outputFileList.Contains(fileInfo.FullName))
                            outputFileList.Add(fileInfo.FullName);
                    }
                }
                finally
                {
                    fileWriter.Close();
                    fileWriter.Dispose();
                }
            }

            return new GeneratedOutput(outputFileList, outputIndex, portionsCount);
        }

        private async Task<GeneratedOutput> CreateJsonOutputFileV3(IReadOnlyCollection<Flash> flashes, string jobFolder, LxQueryJobData lxQueryJobData, int outputIndex, CancellationToken ct)
        {
            ct.ThrowIfCancellationRequested();

            var portionsCount = 0L;

            var outputFileList = new List<string>();

            if (flashes?.Count > 0)
            {
                var outputFile = GenerateOutputFileName(lxQueryJobData, jobFolder, JsonFileExtension, ref outputIndex);
                //var lastNotificationSent = DateTime.MinValue;

                var fileInfo = new FileInfo(outputFile);
                var fileWriter = default(StreamWriter);

                try
                {
                    fileWriter = fileInfo.AppendText();
                    fileWriter.AutoFlush = true;

                    foreach (var flashBatch in flashes.Batch(_flashBatchSizeJson).Select(batch => batch.Select(x => new FlashV3(x))))
                    {
                        ct.ThrowIfCancellationRequested();

                        //if (lxQueryJobData.SendProgressNotifications != null
                        //    && lxQueryJobData.SendNotifications != null
                        //    && lxQueryJobData.SendNotifications.Value
                        //    && lxQueryJobData.SendProgressNotifications.Value
                        //    && lastNotificationSent.AddMinutes(Config.MinutesBetweenProgressNotifications) <= DateTime.UtcNow)
                        //{
                        //    lastNotificationSent = DateTime.UtcNow;
                        //    await SendProgressEmail(lxQueryJobData);
                        //}

                        var data = string.Empty;

                        switch (lxQueryJobData.LxType)
                        {
                            case LxType.Both:
                                {
                                    data = string.Join(Environment.NewLine, flashBatch.Select(JsonConvert.SerializeObject));
                                    portionsCount += flashBatch.Select(flash => flash.Portions).SelectMany(x => x).Count();
                                    break;
                                }

                            case LxType.Flash:
                                {
                                    var flashBatchList = flashBatch.ToList();
                                    flashBatchList.ForEach(flash => flash.Portions = null);
                                    data = string.Join(Environment.NewLine, flashBatchList.Select(JsonConvert.SerializeObject));
                                    break;
                                }

                            case LxType.Portion:
                                {
                                    data = string.Join(Environment.NewLine,flashBatch.Select(flash => flash.Portions).SelectMany(x => x).Select(JsonConvert.SerializeObject));
                                    portionsCount += flashBatch.Select(flash => flash.Portions).SelectMany(x => x).Count();
                                    break;
                                }
                        }

                        if (fileInfo.Exists && (fileInfo.Length + data.Length + Environment.NewLine.Length) >= _maxOutputFileSizeJson)
                        {
                            fileWriter.Close();
                            fileWriter.Dispose();

                            outputIndex++;
                            outputFile = GenerateOutputFileName(lxQueryJobData, jobFolder, JsonFileExtension, ref outputIndex);
                            fileInfo = new FileInfo(outputFile);

                            fileWriter = fileInfo.AppendText();
                            fileWriter.AutoFlush = true;
                        }

                        fileWriter.Write(data);
                        fileWriter.Write(Environment.NewLine);

                        if (!outputFileList.Contains(fileInfo.FullName))
                            outputFileList.Add(fileInfo.FullName);
                    }
                }
                finally
                {
                    fileWriter.Close();
                    fileWriter.Dispose();
                }
            }

            return new GeneratedOutput(outputFileList, outputIndex, portionsCount);
        }

        private async Task<GeneratedOutput> CreateKmzFile(IReadOnlyCollection<Flash> flashes, string jobFolder, LxQueryJobData lxQueryJobData, int outputIndex, Dictionary<string, long> kmzFilesPlacemarkCounts, CancellationToken ct)
        {
            ct.ThrowIfCancellationRequested();

            var portionsCount = 0L;

            var xmlWriterSettings = new XmlWriterSettings
            {
                Indent = false,
                ConformanceLevel = ConformanceLevel.Fragment,
                OmitXmlDeclaration = true,
                CloseOutput = true,
            };

            //var lastNotificationSent = DateTime.MinValue;

            var outputFileList = new List<string>();

            var outputFile = GenerateOutputFileName(lxQueryJobData, jobFolder, KmzFileExtension, ref outputIndex);
            var outputFileInfo = new FileInfo(outputFile);
            if (!kmzFilesPlacemarkCounts.ContainsKey(outputFile))
                kmzFilesPlacemarkCounts.Add(outputFile, 0L);

            if (flashes?.Count > 0)
            {
                var placeMarksAdded = 0L;

                if ((kmzFilesPlacemarkCounts[outputFile] + placeMarksAdded) >= _maxKmlPlacemarks)
                {
                    outputIndex++;
                    outputFile = GenerateOutputFileName(lxQueryJobData, jobFolder, KmzFileExtension, ref outputIndex);
                    outputFileInfo = new FileInfo(outputFile);
                    if (!kmzFilesPlacemarkCounts.ContainsKey(outputFile))
                        kmzFilesPlacemarkCounts.Add(outputFile, 0L);
                }

                ZipArchive archive = null;
                XmlWriter kmzWriter = null;

                try
                {
                    var kmzFile = outputFileInfo.Open(FileMode.OpenOrCreate);
                    archive = new ZipArchive(kmzFile, ZipArchiveMode.Update);

                    var entryName = outputFileInfo.Name.Replace(KmzFileExtension, KmlFileExtension);
                    ZipArchiveEntry kmlEntry = null;
                    if (archive.Entries.Count == 0)
                    {
                        AddImagesToKmz(archive, ct);
                        kmlEntry = archive.CreateEntry(entryName);
                        using (var streamWriter = new StreamWriter(kmlEntry.Open()))
                        {
                            streamWriter.Write(CreatePrimaryHeadersforKmz(lxQueryJobData).ToString());
                            streamWriter.Close();
                        }
                    }
                    else
                    {
                        kmlEntry = archive.GetEntry(entryName);
                    }

                    var entryStream = kmlEntry.Open();
                    entryStream.Seek(0, SeekOrigin.End);

                    kmzWriter = XmlWriter.Create(entryStream, xmlWriterSettings);

                    outputFileInfo = new FileInfo(outputFile);

                    var entriesWritten = 0;

                    foreach (var srcFlash in flashes)
                    {
                        ct.ThrowIfCancellationRequested();

                        //if (lxQueryJobData.SendProgressNotifications != null
                        //    && lxQueryJobData.SendNotifications != null
                        //    && lxQueryJobData.SendNotifications.Value
                        //    && lxQueryJobData.SendProgressNotifications.Value
                        //    && lastNotificationSent.AddMinutes(Config.MinutesBetweenProgressNotifications) <= DateTime.UtcNow)
                        //{
                        //    lastNotificationSent = DateTime.UtcNow;
                        //    await SendProgressEmail(lxQueryJobData);
                        //}

                        if ((kmzFilesPlacemarkCounts[outputFile] + placeMarksAdded) >= _maxKmlPlacemarks)
                        {
                            kmzFilesPlacemarkCounts[outputFile] += placeMarksAdded;
                            placeMarksAdded = 0;

                            #region Close existing KMZ file
                            kmzWriter.Close();
                            kmzWriter.Dispose();
                            archive.Dispose();

                            if (!outputFileList.Contains(outputFileInfo.FullName))
                                outputFileList.Add(outputFileInfo.FullName);

                            #endregion Close existing KMZ file

                            #region Open new KMZ file

                            outputIndex++;
                            outputFile = GenerateOutputFileName(lxQueryJobData, jobFolder, KmzFileExtension, ref outputIndex);
                            outputFileInfo = new FileInfo(outputFile);
                            if (!kmzFilesPlacemarkCounts.ContainsKey(outputFile))
                                kmzFilesPlacemarkCounts.Add(outputFile, 0L);
                            entryName = outputFileInfo.Name.Replace(KmzFileExtension, KmlFileExtension);

                            kmzFile = outputFileInfo.Open(FileMode.CreateNew);
                            archive = new ZipArchive(kmzFile, ZipArchiveMode.Update);

                            AddImagesToKmz(archive, ct);
                            kmlEntry = archive.CreateEntry(entryName);
                            using (var streamWriter = new StreamWriter(kmlEntry.Open()))
                            {
                                streamWriter.Write(CreatePrimaryHeadersforKmz(lxQueryJobData).ToString());
                                streamWriter.Close();
                            }

                            entryStream = kmlEntry.Open();
                            entryStream.Seek(0, SeekOrigin.End);

                            kmzWriter = XmlWriter.Create(entryStream, xmlWriterSettings);
                            outputFileInfo = new FileInfo(outputFile);

                            #endregion Open new KMZ file
                        }

                        #region Write flash to KMZ
                        
                        switch (lxQueryJobData.OutputVersion)
                        {
                            #region V2
                            case LxOutputVersion.V2:
                                {
                                    var flashV2 = new FlashV2(srcFlash);
                                    switch (lxQueryJobData.LxType)
                                    {
                                        case LxType.Flash:
                                            {
                                                WriteKmlContentV2(flashV2, null, kmzWriter, ct);
                                                entriesWritten++;
                                                placeMarksAdded++;
                                                break;
                                            }

                                        case LxType.Portion:
                                            {
                                                var portions = flashV2.Portions;
                                                portionsCount += portions.Count;
                                                foreach (var srcPortion in portions)
                                                {
                                                    if ((kmzFilesPlacemarkCounts[outputFile] + placeMarksAdded) >= _maxKmlPlacemarks)
                                                    {
                                                        kmzFilesPlacemarkCounts[outputFile] += placeMarksAdded;
                                                        placeMarksAdded = 0;

                                                        #region Close existing KMZ file
                                                        kmzWriter.Close();
                                                        kmzWriter.Dispose();
                                                        archive.Dispose();

                                                        if (!outputFileList.Contains(outputFileInfo.FullName))
                                                            outputFileList.Add(outputFileInfo.FullName);

                                                        #endregion Close existing KMZ file

                                                        #region Open new KMZ file

                                                        outputIndex++;
                                                        outputFile = GenerateOutputFileName(lxQueryJobData, jobFolder, KmzFileExtension, ref outputIndex);
                                                        outputFileInfo = new FileInfo(outputFile);
                                                        if (!kmzFilesPlacemarkCounts.ContainsKey(outputFile))
                                                            kmzFilesPlacemarkCounts.Add(outputFile, 0L);
                                                        entryName = outputFileInfo.Name.Replace(KmzFileExtension, KmlFileExtension);

                                                        kmzFile = outputFileInfo.Open(FileMode.CreateNew);
                                                        archive = new ZipArchive(kmzFile, ZipArchiveMode.Update);

                                                        AddImagesToKmz(archive, ct);
                                                        kmlEntry = archive.CreateEntry(entryName);
                                                        using (var streamWriter = new StreamWriter(kmlEntry.Open()))
                                                        {
                                                            streamWriter.Write(CreatePrimaryHeadersforKmz(lxQueryJobData).ToString());
                                                            streamWriter.Close();
                                                        }

                                                        entryStream = kmlEntry.Open();
                                                        entryStream.Seek(0, SeekOrigin.End);

                                                        kmzWriter = XmlWriter.Create(entryStream, xmlWriterSettings);
                                                        outputFileInfo = new FileInfo(outputFile);

                                                        #endregion Open new KMZ file
                                                    }

                                                    WriteKmlContentV2(null, srcPortion, kmzWriter, ct);
                                                    entriesWritten++;
                                                    placeMarksAdded++;
                                                }
                                                break;
                                            }
                                    }
                                    break;
                                }
                            #endregion V2

                            #region V3
                            case LxOutputVersion.V3:
                            default:
                                {
                                    var flashV3 = new FlashV3(srcFlash);
                                    switch (lxQueryJobData.LxType)
                                    {
                                        case LxType.Flash:
                                            {
                                                WriteKmlContentV3(flashV3, null, kmzWriter, ct);
                                                entriesWritten++;
                                                placeMarksAdded++;
                                                break;
                                            }

                                        case LxType.Portion:
                                            {
                                                var portions = flashV3.Portions;
                                                portionsCount += portions.Count;
                                                foreach (var srcPortion in portions)
                                                {
                                                    if ((kmzFilesPlacemarkCounts[outputFile] + placeMarksAdded) >= _maxKmlPlacemarks)
                                                    {
                                                        kmzFilesPlacemarkCounts[outputFile] += placeMarksAdded;
                                                        placeMarksAdded = 0;

                                                        #region Close existing KMZ file
                                                        kmzWriter.Close();
                                                        kmzWriter.Dispose();
                                                        archive.Dispose();

                                                        if (!outputFileList.Contains(outputFileInfo.FullName))
                                                            outputFileList.Add(outputFileInfo.FullName);

                                                        #endregion Close existing KMZ file

                                                        #region Open new KMZ file

                                                        outputIndex++;
                                                        outputFile = GenerateOutputFileName(lxQueryJobData, jobFolder, KmzFileExtension, ref outputIndex);
                                                        outputFileInfo = new FileInfo(outputFile);
                                                        if (!kmzFilesPlacemarkCounts.ContainsKey(outputFile))
                                                            kmzFilesPlacemarkCounts.Add(outputFile, 0L);
                                                        entryName = outputFileInfo.Name.Replace(KmzFileExtension, KmlFileExtension);

                                                        kmzFile = outputFileInfo.Open(FileMode.CreateNew);
                                                        archive = new ZipArchive(kmzFile, ZipArchiveMode.Update);

                                                        AddImagesToKmz(archive, ct);
                                                        kmlEntry = archive.CreateEntry(entryName);
                                                        using (var streamWriter = new StreamWriter(kmlEntry.Open()))
                                                        {
                                                            streamWriter.Write(CreatePrimaryHeadersforKmz(lxQueryJobData).ToString());
                                                            streamWriter.Close();
                                                        }

                                                        entryStream = kmlEntry.Open();
                                                        entryStream.Seek(0, SeekOrigin.End);

                                                        kmzWriter = XmlWriter.Create(entryStream, xmlWriterSettings);
                                                        outputFileInfo = new FileInfo(outputFile);

                                                        #endregion Open new KMZ file
                                                    }

                                                    WriteKmlContentV3(null, srcPortion, kmzWriter, ct);
                                                    entriesWritten++;
                                                    placeMarksAdded++;
                                                }
                                                break;
                                            }
                                    }
                                    break;
                                }
                                #endregion V3
                        }
                        #endregion Write flash to KMZ
                    }
                }
                finally
                {
                    try
                    {
                        kmzFilesPlacemarkCounts[outputFile] += placeMarksAdded;

                        #region Close KMZ file
                        kmzWriter.Close();
                        kmzWriter.Dispose();
                        archive.Dispose();
                        #endregion Close KMZ file
                    }
                    catch (Exception)
                    {
                        /* Consume the exception */
                    }
                }

                if (!outputFileList.Contains(outputFileInfo.FullName))
                    outputFileList.Add(outputFileInfo.FullName);
            }

            return new GeneratedOutput(outputFileList, outputIndex, portionsCount);
        }

        private static string GenerateOutputFileName(LxQueryJobData lxQueryJobData, string jobFolder, string fileExtension, ref int outputIndex)
        {
            var strokeType = lxQueryJobData.StrokeType == StrokeType.None ? "Both" : lxQueryJobData.StrokeType.ToString();
            var outputFilePrefix = $"{lxQueryJobData.Start.Year:D4}{lxQueryJobData.Start.Month:D2}{lxQueryJobData.Start.Day:D2}" + $"-{lxQueryJobData.End.Year:D4}{lxQueryJobData.End.Month:D2}{lxQueryJobData.End.Day:D2}" + $"-lxversion.{lxQueryJobData.OutputVersion}" + $"-lxtype.{lxQueryJobData.LxType}" + $"-stroketype.{strokeType}";
            var outputFile = Path.Combine(jobFolder, $"{outputFilePrefix}-{outputIndex}.{fileExtension}");

            return outputFile;
        }
        #endregion Output file generators


        #region KMZ helper methods
        private static void AddImagesToKmz(ZipArchive archive, CancellationToken ct)
        {
            var imgFolder = Directory.GetFiles(Config.KMZImageFolder);
            foreach (var imageFileInfo in imgFolder.Where(file => !file.Contains("contour")).Select(file => new FileInfo(file)))
            {
                ct.ThrowIfCancellationRequested();

                using (var stream = imageFileInfo.Open(FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    var zipEntry = archive.CreateEntry($"{imageFileInfo.Directory.Name.ToLowerInvariant()}{Path.AltDirectorySeparatorChar}{imageFileInfo.Name.ToLowerInvariant()}", CompressionLevel.Optimal);

                    var entryLastWriteTime = imageFileInfo.LastWriteTime;
                    if (entryLastWriteTime.Year < 1980 || entryLastWriteTime.Year > 2107)
                        entryLastWriteTime = new DateTime(1980, 1, 1, 0, 0, 0);
                    zipEntry.LastWriteTime = entryLastWriteTime;

                    using (var dest = zipEntry.Open())
                    {
                        stream.CopyTo(dest, DefaultCopyBufferSize);
                        dest.Close();
                    }
                    stream.Flush(true);
                    stream.Close();
                }
            }
        }

        private static void FinalizeKmzFile(string kmzFilePath)
        {
            const string outputFileExtension = "kmz";
            const string entryFileExtension = "kml";
            const string kmlFooter = "\r\n  </Document>\r\n</Folder>\r\n</kml>";

            var kmzFileInfo = new FileInfo(kmzFilePath);
            var entryName = kmzFileInfo.Name.Replace(outputFileExtension, entryFileExtension);

            using (var kmzFile = kmzFileInfo.Open(FileMode.OpenOrCreate))
            {
                using (var archive = new ZipArchive(kmzFile, ZipArchiveMode.Update))
                {
                    var kmlEntry = archive.GetEntry(entryName);

                    using (var streamWriter = new StreamWriter(kmlEntry.Open()))
                    {
                        streamWriter.BaseStream.Seek(0, SeekOrigin.End);
                        streamWriter.Write(kmlFooter);
                        streamWriter.Close();
                    }
                }
            }
        }

        private static void WriteKmlContentV2(FlashV2 flash, PortionV2 portion, XmlWriter writer, CancellationToken ct)
        {
            ct.ThrowIfCancellationRequested();

            var mapTipData = new Dictionary<string, string>();
            var itemType = flash?.FlashType ?? portion.PulseType;

            var styleUrl = string.Empty;
            var recordType = string.Empty;

            var amplitude = flash != null ? Convert.ToDouble(flash.PeakCurrent?.ToString("0.00")) : portion.PeakCurrent;

            switch (itemType)
            {
                case FlashType.FlashTypeGlobalCG:
                case FlashType.FlashTypeCG:
                    {
                        recordType = "CloudToGround";
                        styleUrl = amplitude >= 0 ? "#cg_positive" : "#cg_negative";
                        break;
                    }

                case FlashType.FlashTypeIC:
                case FlashType.FlashTypeICNarrowBipolar:
                case FlashType.FlashTypeGlobalIC:
                    {
                        recordType = "In Cloud";
                        styleUrl = "#ic";
                        break;
                    }
            }

            #region Create Description Grid

            mapTipData["Latitude"] = flash?.Latitude.ToString(CultureInfo.InvariantCulture) ?? portion.Latitude.ToString(CultureInfo.InvariantCulture);
            mapTipData["Longitude"] = flash?.Longitude.ToString(CultureInfo.InvariantCulture) ?? portion.Longitude.ToString(CultureInfo.InvariantCulture);
            mapTipData["UTC Time"] = flash?.Time.ToString("MM/dd/yyyy HH:mm UTC") ?? portion.Time.ToString("MM/dd/yyyy HH:mm UTC");
            mapTipData["Height"] = flash?.IcHeight.ToString("0.00") ?? portion.IcHeight.ToString("0.00");
            mapTipData["Amplitude"] = flash != null ? flash.PeakCurrent?.ToString("0.00") : portion.PeakCurrent.ToString("0.00");
            mapTipData["Type"] = recordType;

            #endregion


            //create Placemark
            writer.WriteStartElement("Placemark");
            writer.WriteElementString("visibility", "0");

            // description
            writer.WriteElementString("description", GetMaptipText(mapTipData));
            // </description>

            //point
            writer.WriteStartElement("Point");
            writer.WriteElementString("coordinates", flash?.Longitude + "," + flash?.Latitude + ",0" ?? portion?.Longitude + "," + portion?.Latitude + ",0");
            writer.WriteEndElement();
            // </point>

            // TimeStamp
            writer.WriteStartElement("TimeSpan");
            writer.WriteElementString("begin", flash?.Time.ToString("s") ?? portion.Time.ToString("s"));
            writer.WriteElementString("end", flash?.Time.AddMinutes(6).ToString("s") ?? portion.Time.AddMinutes(6).ToString("s"));
            writer.WriteEndElement();
            // </TimeStamp>

            // Style
            writer.WriteElementString("styleUrl", styleUrl);

            writer.WriteEndElement(); //Placemark
        }

        private static void WriteKmlContentV3(FlashV3 flash, PortionV3 portion, XmlWriter writer, CancellationToken ct)
        {
            ct.ThrowIfCancellationRequested();

            var mapTipData = new Dictionary<string, string>();
            var itemType = flash?.Type ?? portion.Type;

            var styleUrl = string.Empty;
            var recordType = string.Empty;

            var amplitude = flash != null ? Convert.ToDouble(flash.PeakCurrent?.ToString("0.00")) : portion.Amplitude;

            switch (itemType)
            {
                case FlashType.FlashTypeGlobalCG:
                case FlashType.FlashTypeCG:
                    {
                        recordType = "CloudToGround";
                        styleUrl = amplitude >= 0 ? "#cg_positive" : "#cg_negative";
                        break;
                    }

                case FlashType.FlashTypeIC:
                case FlashType.FlashTypeICNarrowBipolar:
                case FlashType.FlashTypeGlobalIC:
                    {
                        recordType = "In Cloud";
                        styleUrl = "#ic";
                        break;
                    }
            }

            #region Create Description Grid

            mapTipData["Latitude"] = flash?.Latitude.ToString(CultureInfo.InvariantCulture) ?? portion.Latitude.ToString(CultureInfo.InvariantCulture);
            mapTipData["Longitude"] = flash?.Longitude.ToString(CultureInfo.InvariantCulture) ?? portion.Longitude.ToString(CultureInfo.InvariantCulture);
            mapTipData["UTC Time"] = flash?.Timestamp.ToString("MM/dd/yyyy HH:mm UTC") ?? portion.Timestamp.ToString("MM/dd/yyyy HH:mm UTC");
            mapTipData["Height"] = flash != null ? flash.Height?.ToString("0.00") : portion.Height.ToString("0.00");
            mapTipData["Amplitude"] = flash != null ? flash.PeakCurrent?.ToString("0.00") : portion.Amplitude.ToString("0.00");
            mapTipData["Type"] = recordType;

            #endregion

            //create Placemark
            writer.WriteStartElement("Placemark");
            writer.WriteElementString("visibility", "0");

            //description
            writer.WriteElementString("description", GetMaptipText(mapTipData));
            // </description>

            //point
            writer.WriteStartElement("Point");
            writer.WriteElementString("coordinates", flash != null ? flash.Longitude + "," + flash.Latitude + ",0" : portion.Longitude + "," + portion.Latitude + ",0");
            writer.WriteEndElement();
            // </point>

            // TimeStamp
            writer.WriteStartElement("TimeSpan");
            writer.WriteElementString("begin", flash?.Timestamp.ToString("s") ?? portion.Timestamp.ToString("s"));
            writer.WriteElementString("end", flash?.Timestamp.AddMinutes(6).ToString("s") ?? portion.Timestamp.AddMinutes(6).ToString("s"));
            writer.WriteEndElement();
            // </TimeStamp>

            // Style
            writer.WriteElementString("styleUrl", styleUrl);

            writer.WriteEndElement(); //Placemark
        }

        private static StringBuilder CreatePrimaryHeadersforKmz(LxQueryJobData lxQueryJobData)
        {
            var layerType = string.Empty;
            switch (lxQueryJobData.LxType)
            {
                case LxType.Flash:
                    {
                        layerType = "Flashes";
                        break;
                    }
                case LxType.Portion:
                    {
                        layerType = "Strokes";
                        break;
                    }
            }

            var sb = new StringBuilder();

            sb.Append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n");
            sb.Append("<kml xmlns=\"http://www.opengis.net/kml/2.2\">\r\n");
            sb.Append("<Folder>\r\n  <name>TLS Detection Report</name>\r\n");
            sb.Append($"  <description>KML powered by Earth Networks Total Lightning System.\r\nCopyright {DateTime.UtcNow.Year}, Earth Networks, Inc. All rights reserved.\r\n</description>\r\n");
            sb.Append("  <Document>\r\n");
            sb.Append("    <Style id=\"ic\"><IconStyle><scale>1</scale><Icon><href>images/ltg-ic.png</href></Icon></IconStyle></Style>\r\n");
            sb.Append("    <Style id=\"cg_positive\"><IconStyle><scale>1</scale><Icon><href>images/ltg-cg-positive.png</href></Icon></IconStyle></Style>\r\n");
            sb.Append("    <Style id=\"cg_negative\"><IconStyle><scale>1</scale><Icon><href>images/ltg-cg-negative.png</href></Icon></IconStyle></Style>\r\n");
            sb.AppendFormat("    <name>Lx {0}</name>\r\n", layerType);

            return sb;
        }

        private static string GetMaptipText(Dictionary<string, string> mapTipData)
        {
            var sbDesc = new StringBuilder();
            sbDesc.Append("<div id='maptip_wrapper' style='background-color:#f2dcdb;border:1px solid #953735'>");
            sbDesc.Append("<table width='100%' cellpadding='2'  cellspacing='0' style='border-collapse: collapse;background-color: #f2dcdb;'>");
            sbDesc.Append("<tr><td align='center' style='padding-bottom:3px'>");
            sbDesc.Append("<table style='width:100%;border-collapse:collapse;border: 1px solid white' cellpadding='3' cellspacing='0'>");

            var k = 0;

            foreach (var data in mapTipData)
            {
                if (k % 2 == 0)
                {
                    sbDesc.Append("<tr><td style='width:45%; background-color:#e8d0d0;border: 1px  solid white;padding-left :4px;' align='left'>" + ((data.Key == "Blank") ? "&nbsp;" : data.Key) + "</td><td  align='left' style='width:55%;background-color:#e8d0d0;border: 1px  solid white;padding-left :4px;'>" + ((data.Key == "Blank") ? ("<a id='lightningdetails' href='" + data.Value + "' target='_blank' title='Details'>Details</a>") : data.Value) + "</td></tr>");
                }
                else
                {
                    sbDesc.Append("<tr><td style='width:45%;background-color: #f4e9e9;border: 1px  solid white;padding-left :4px;' align='left'>" + ((data.Key == "Blank") ? "&nbsp;" : data.Key) + "</td><td align='left' style='width:55%;background-color: #f4e9e9;border: 1px  solid white;padding-left :4px;'>" + ((data.Key == "Blank") ? ("<a id='lightningdetails' href='" + data.Value + "' target='_blank' title='Details'>Details</a>") : data.Value) + "</td></tr>");
                }
                k++;
            }
            sbDesc.Append("</table></td></tr>");
            sbDesc.Append("</table></div>");

            return sbDesc.ToString();
        }
        #endregion KMZ helper methods


        #region Content validators
        private static bool IsValidLine(LxQueryJobData lxQueryJobData, string line, FileInfo outputFile, out Flash flash)
        {
            var isValid = false;
            flash = default(Flash);

            // Check time for each line
            try
            {
                flash = JsonConvert.DeserializeObject<Flash>(line);

                if (flash != null)
                {
                    // Check if flash matches query criteria
                    if (ValidateFlash(flash, lxQueryJobData))
                    {
                        if (lxQueryJobData.LxType == LxType.Portion &&
                            lxQueryJobData.StrokeType == StrokeType.CloudToGround)
                        {
                            flash.Portions?.RemoveAll(p => p.Type.Equals(FlashType.FlashTypeIC)
                                                            || p.Type.Equals(FlashType.FlashTypeGlobalIC)
                                                            || p.Type.Equals(FlashType.FlashTypeICNarrowBipolar));

                            if (flash.Portions != null && flash.Portions.Count > 0) isValid = true;
                        }
                        else if (lxQueryJobData.LxType == LxType.Portion &&
                                 lxQueryJobData.StrokeType == StrokeType.IntraCloud)
                        {
                            if (flash.Type == FlashType.FlashTypeCG ||
                                flash.Type == FlashType.FlashTypeGlobalCG)
                            {
                                flash.Portions?.RemoveAll(p => p.Type.Equals(FlashType.FlashTypeCG)
                                                                || p.Type.Equals(FlashType.FlashTypeGlobalCG));
                            }
                            if (flash.Portions != null && flash.Portions.Count > 0) isValid = true;
                        }
                        else
                        {
                            isValid = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogError($"[{lxQueryJobData.JobId}] Error processing flash from file: {outputFile.Name}, line: {line} due to exception: {ex.Message}", ex);
            }

            return isValid;
        }

        private static bool ValidateFlash(Flash flash, LxQueryJobData lxQueryJobData)
        {
            var isValid = false;

            // Ensure flash type is the same type requested
            if (lxQueryJobData.StrokeType == StrokeType.CloudToGround)
            {
                if (lxQueryJobData.LxType == LxType.Flash)
                {
                    isValid = (flash.Type == FlashType.FlashTypeCG || flash.Type == FlashType.FlashTypeGlobalCG);
                }
                else if (lxQueryJobData.LxType == LxType.Portion)
                {
                    if (flash?.Portions?.Find(p => p.Type.Equals(FlashType.FlashTypeCG)
                                        || p.Type.Equals(FlashType.FlashTypeGlobalCG)) == null)
                        isValid = false;
                    else
                        isValid = true;
                }
                else
                    isValid = true;
            }
            else if (lxQueryJobData.StrokeType == StrokeType.IntraCloud)
            {
                if (lxQueryJobData.LxType == LxType.Flash)
                {
                    isValid = (flash.Type == FlashType.FlashTypeIC || flash.Type == FlashType.FlashTypeGlobalIC);
                }
                else if (lxQueryJobData.LxType == LxType.Portion)
                {
                    if (flash?.Portions?.Find(p => p.Type.Equals(FlashType.FlashTypeIC)
                                        || p.Type.Equals(FlashType.FlashTypeGlobalIC)) == null)
                        isValid = false;
                    else
                        isValid = true;
                }
                else
                    isValid = true;
            }
            else
                isValid = true;

            if (!isValid) return false;



            var flashTimestamp = Convert.ToDateTime(flash.TimeStamp);

            // Ensure timestamp is valid
            isValid &= flashTimestamp >= lxQueryJobData.Start && flashTimestamp <= lxQueryJobData.End;
            if (!isValid) return false;


            // Only check location if time range and flash type are valid since
            // since this is an expensive operation
            switch (lxQueryJobData.QueryType)
            {
                case LxQueryType.QuadKey:
                {
                    var bBoxes = lxQueryJobData.QuadKey.Select(GetBoundingBoxByQuadKey).ToList();
                    isValid  = bBoxes.Any(bBox => bBox.Contains((float) flash.Latitude, (float) flash.Longitude));
                    break;
                }
                case LxQueryType.Circle:
                {
                    var bBox = GetBoundingBoxByRadius((float) lxQueryJobData.Latitude.Value
                                                        , (float) lxQueryJobData.Longitude.Value
                                                        , lxQueryJobData.Radius.Value
                                                        , lxQueryJobData.RadiusType);
                    var radiusInMiles = lxQueryJobData.RadiusType == RadiusType.Meters
                                        ? ConvertMetersToMiles(lxQueryJobData.Radius.Value)
                                        : lxQueryJobData.Radius.Value;
                    isValid = bBox.GetDistance((float) flash.Latitude, (float) flash.Longitude) <= radiusInMiles;
                    break;
                }
                case LxQueryType.BoundingBox:
                {
                    var bBox = new BoundingBox((float) lxQueryJobData.LatitudeMin.Value
                                                , (float) lxQueryJobData.LongitudeMin.Value
                                                , (float) lxQueryJobData.LatitudeMax.Value
                                                , (float) lxQueryJobData.LongitudeMax.Value);
                    isValid = bBox.Contains((float) flash.Latitude, (float) flash.Longitude);
                    break;
                }
            }

            return isValid;
        }
        #endregion Content validators


        #region Location converters
        private static BoundingBox GetBoundingBoxByRadius(float latitude, float longitude, int radius, RadiusType radiusType)
        {
            return new BoundingBox(latitude, longitude, radiusType == RadiusType.Meters ? ConvertMetersToMiles(radius) : radius);
        }

        private static BoundingBox GetBoundingBoxByQuadKey(string quadKey)
        {
            double maxLatitude, maxLongitude, minLatitude, minLongitude;

            TileSystem.QuadKeyToBoundingBox(quadKey, out maxLatitude, out maxLongitude, out minLatitude, out minLongitude);

            return new BoundingBox((float)minLatitude , (float)minLongitude , (float)maxLatitude , (float)maxLongitude);
        }

        private static List<string> GetQuadKeys(LxQueryJobData lxQueryJobData)
        {
            var quadKeys = new List<string>();

            if (lxQueryJobData.QueryType == LxQueryType.QuadKey)
            {
                quadKeys.AddRange(lxQueryJobData.QuadKey);
            }
            else
            {
                BoundingBox bBox;

                if (lxQueryJobData.QueryType == LxQueryType.Circle)
                {
                    bBox = new BoundingBox((float)lxQueryJobData.Latitude.Value
                                           , (float)lxQueryJobData.Longitude.Value
                                           , lxQueryJobData.RadiusType == RadiusType.Meters
                                             ? ConvertMetersToMiles(lxQueryJobData.Radius.Value)
                                             : lxQueryJobData.Radius.Value);
                }
                else
                {
                    bBox = new BoundingBox((float)lxQueryJobData.LatitudeMin.Value
                                           , (float)lxQueryJobData.LongitudeMin.Value
                                           , (float)lxQueryJobData.LatitudeMax.Value
                                           , (float)lxQueryJobData.LongitudeMax.Value);
                }
                quadKeys.AddRange(TileSystem.BoundingBoxToQuadKeys(bBox.LatitudeMax, bBox.LongitudeMax, bBox.LatitudeMin, bBox.LongitudeMin));
            }

            return quadKeys;
        }

        private static float ConvertMetersToMiles(int meters)
        {
            return (float)(meters / 1609.344);
        }
        #endregion Location converters


        #region progress notification methods
        private static async Task SendProgressEmail(LxQueryJobData lxQueryJobData)
        {
            if (lxQueryJobData?.SendNotifications == null || !lxQueryJobData.SendNotifications.Value || lxQueryJobData.SendProgressNotifications == null || !lxQueryJobData.SendProgressNotifications.Value)
                return;

            var t4Template = new JobProgressUpdate
            {
                _lxQueryJobData = lxQueryJobData,
                _emailSubject = Config.JobInProgressSubject
            };

            // send progress email
            var template = new EmailTemplate()
            {
                FromAddress = Config.EmailFromAddress,
                MessageBody = t4Template.TransformText(),
                Subject = Config.JobInProgressSubject
            };

            await SendEmail(lxQueryJobData, template);
        }

        private static async Task SendCompletionEmail(LxQueryJobData lxQueryJobData, List<string> outputUrlList, long flashesCount, long portionsCount)
        {
            if (lxQueryJobData.SendNotifications == null || !lxQueryJobData.SendNotifications.Value)
                return;

            var t4Template = new JobCompletion
            {
                _lxQueryJobData = lxQueryJobData,
                _signedUrls = outputUrlList,
                _emailSubject = Config.JobCompletedSubject,
                _flashesCount = flashesCount,
                _portionsCount = portionsCount
            };

            var template = new EmailTemplate()
            {
                FromAddress = Config.EmailFromAddress,
                MessageBody = t4Template.TransformText(),
                Subject = Config.JobCompletedSubject
            };

            await SendEmail(lxQueryJobData, template);
        }

        private static async Task SendFailureEmail(LxQueryJobData lxQueryJobData, bool isFailure, string message)
        {
            if (lxQueryJobData.SendNotifications == null || !lxQueryJobData.SendNotifications.Value)
                return;

            var t4Template = new JobFailed
            {
                _lxQueryJobData = lxQueryJobData,
                _emailSubject = isFailure ? Config.JobFailedSubject : Config.JobInvalidSubject,
                _failureMessage = message
            };

            var template = new EmailTemplate()
            {
                FromAddress = Config.EmailFromAddress,
                MessageBody = t4Template.TransformText(),
                Subject = t4Template._emailSubject
            };

            await SendEmail(lxQueryJobData, template);
        }

        private static async Task SendEmail(LxQueryJobData lxQueryJobData, EmailTemplate template)
        {
            try
            {
                await EmailHelper.SendEmail(lxQueryJobData, template);
            }
            catch (Exception ex)
            {
                AppLogger.LogError($"[{lxQueryJobData?.JobId ?? "Unknown"}] Error sending email. Subject: {template?.Subject ?? "Unknown"}, To: {lxQueryJobData?.RecipientEmailAddress ?? "Unknown"}, Error: {ex.Message}", ex);
                throw;
            }
        }

        private static void DisposeProgressTimer(Timer progressEmailTimer, string jobId)
        {
            try
            {
                if (progressEmailTimer == null) return;

                progressEmailTimer.AutoReset = false;
                progressEmailTimer.Enabled = false;
                progressEmailTimer.Dispose();
            }
            catch (Exception ex)
            {
                AppLogger.LogError($"[{jobId}] Caught exception while disposing of progress timer. Error: {ex.Message}", ex);
            }
        }
        #endregion progress notification methods


        #region ECV counters
        public int FailedQueryCount
        {
            get { return _failedQueryCount; }
            set { _failedQueryCount = value; }
        }

        public int InvalidQueryCount
        {
            get { return _invalidQueryCount; }
            set { _invalidQueryCount = value; }
        }

        public int ReceivedQueryCount
        {
            get { return _totalQueryCount; }
            set { _totalQueryCount = value; }
        }

        public int SuccessQueryCount
        {
            get { return _successQueryCount; }
            set { _successQueryCount = value; }
        }

        public DateTime LastQueryProcessedUTC
        {
            get { return _lastQueryProcessedUtc; }
        }
        #endregion ECV counters

        private class GeneratedOutput
        {
            public GeneratedOutput()
            {
            }

            public GeneratedOutput(List<string> files, int index, long count)
            {
                Files = files;
                CurrentIndex = index;
                Counter = count;
            }

            public List<string> Files { get; set; }
            public int CurrentIndex { get; set; }
            public long Counter { get; set; } 
        }
    }
}
