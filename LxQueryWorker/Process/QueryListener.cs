﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System.IO;
using System.Net;
using System.Threading;
using Aws.Core.Utilities;
using En.Ods.Lib.Common.Dynamo;
using En.Ods.Lib.Common.Interface;
using En.Ods.Lib.Common.Serialization;
using LxPortalLib.Models;
using LxQueryWorker.Common;
using LxQueryWorker.Model;
using LxQueryWorker.Interfaces;
using LxQueryWorker.MailT4Templates;

using Newtonsoft.Json;
using System.Threading.Tasks;
using LxQueryWorker.Utils;

namespace LxQueryWorker.Process
{
    public class QueryListener : IStatusProvider
    {
        private readonly string _topicName;
        private readonly string _queueName;

        private ICloudQueue _sqsQueue;
        private ICloudTopic _snsTopic;
        private IEmailSender _emailSender;

        
        
        private bool _shuttingDown;
        private DateTime _lastQueryReceived, _lastMessagesReceivedUtc, _startTime;
        private readonly ConcurrentDictionary<string, RunningJob> _runningJobs;
        private readonly int _maxConcurrentJobs;
        private readonly TimeSpan _queueReadDelay;

        private readonly QueryProcessor _queryProcessor;
        private readonly JobProcessorNotifier _jobProcessorNotifier;

        static QueryListener()
        {
            ServicePointManager.DefaultConnectionLimit = 1000;
        }

        public QueryListener(string topicName, string queueName)
        {
            _topicName = topicName;
            _queueName = queueName;

            _shuttingDown = false;

            _sqsQueue = null;
            _snsTopic = null;
            _emailSender = null;

            _startTime = DateTime.Now;
            _lastMessagesReceivedUtc = DateTime.UtcNow;
            _lastQueryReceived = DateTime.UtcNow;

            _queueReadDelay = TimeSpan.FromSeconds(Config.QueueReadDelaySeconds);

            _maxConcurrentJobs = Config.MaxConcurrentjobs;
            _runningJobs = new ConcurrentDictionary<string, RunningJob>();

            _jobProcessorNotifier = new JobProcessorNotifier(Config.AwsSnsLxQueryJobProcessorTopic, Config.AwsSqsLxQueryJobProcessorQueue);

            _queryProcessor = new QueryProcessor(Config.S3LxDataBucket, Config.S3LxQueryOutputBucket);
        }

        public void Start()
        {
            _shuttingDown = false;

            try
            {
                // Will use credentials in .config
                var emptyCredentials = new JsonOdsValue();

                _emailSender = new SesFactory().GetEmailSender(emptyCredentials);

                _snsTopic = new SnsFactory().GetNotifier(emptyCredentials).GetTopic(_topicName);

                if (_snsTopic != null)
                {
                    var dynamicQueue = (IDynamicQueueFromNotifier) new SqsFactory().GetQueueing(emptyCredentials);
                    _sqsQueue = dynamicQueue.GetCleanQueueConnectedToTopicAsync(_queueName, _snsTopic, 500).GetAwaiter().GetResult();
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogError($"QueryListener.Start() - Exception occurred while initializing objects. Error: {ex.Message}", ex);
            }

            var errMsg = string.Empty;

            if (_jobProcessorNotifier == null)
                errMsg = string.Join("; ", errMsg, $"QueryListener.Start() - Failed to create JobProcessorNotifier.");

            if (_queryProcessor == null)
                errMsg = string.Join("; ", errMsg, $"QueryListener.Start() - Failed to create QueryProcessor.");

            if (_emailSender == null)
                errMsg = string.Join("; ", errMsg, "QueryListener.Start() - Failed to retrieve email sender.");

            if (_snsTopic == null)
                errMsg = string.Join("; ", errMsg, $"QueryListener.Start() - Failed to retrieve SNS topic. Topic: {_topicName}");

            if (_sqsQueue == null)
                errMsg = string.Join("; ", errMsg, $"QueryListener.Start() - Failed to retrieve SQS queue. Queue: {_queueName}, Topic: {_topicName}");

            if (!string.IsNullOrWhiteSpace(errMsg)) throw new InvalidOperationException(errMsg);

            try { _queryProcessor.Start(); }
            catch (Exception ex)
            {
                AppLogger.LogError($"QueryListener.Start() - Caught exception starting query processor notifier. Error: {ex.Message}", ex);
                throw;
            }
            
            try { _jobProcessorNotifier.Start(); }
            catch (Exception ex)
            {
                AppLogger.LogError($"QueryListener.Start() - Caught exception starting job processor notifier. Error: {ex.Message}", ex);
                throw;
            }

            ProcessPreviousRunningJobsAsync().GetAwaiter().GetResult();

            ReadMessageQueue();
        }

        public void Stop()
        {
            _shuttingDown = true;

            _sqsQueue.Dispose();
            _sqsQueue = null;

            try { _queryProcessor.Stop(); }
            catch (Exception ex)
            {
                AppLogger.LogWarning($"QueryListener.Stop() - Exception occurred while stopping query processor. Error: {ex.Message}", ex);
            }
            

            try { _jobProcessorNotifier.Stop(); }
            catch (Exception ex)
            {
                AppLogger.LogWarning($"QueryListener.Stop() - Exception occurred while stopping job processor notifier. Error: {ex.Message}", ex);
            }
        }


        private async Task ProcessPreviousRunningJobsAsync()
        {
            try
            {
                AppLogger.LogDebug($"QueryListener.ProcessPreviousRunningJobsAsync() - Start calling GetUncompletedJobNotificationsAsync");
                var uncompletedJobs = await _jobProcessorNotifier.GetUncompletedJobNotificationsAsync();
                AppLogger.LogDebug($"QueryListener.ProcessPreviousRunningJobsAsync() - Finished calling GetUncompletedJobNotificationsAsync. Count: {uncompletedJobs.Count}");

                if (uncompletedJobs != null && uncompletedJobs.Any())
                {
                    foreach (var uncompletedJob in uncompletedJobs)
                    {
                        AppLogger.LogDebug($"[{uncompletedJob.Item2.JobId}] QueryListener.ProcessPreviousRunningJobsAsync() - Processing uncompleted job. Job Id: {uncompletedJob.Item2.JobId}, Job Details: {uncompletedJob.Item1.MessageBody}");

                        LxQueryJobData lxQueryJobData = null;
                        try { lxQueryJobData = JsonConvert.DeserializeObject<LxQueryJobData>(uncompletedJob.Item2.MessageBody); }
                        catch (Exception) { /*Ignore any exceptions*/ }

                        AppLogger.LogDebug($"[{uncompletedJob.Item2.JobId}] QueryListener.ProcessPreviousRunningJobsAsync() - Cleaning up uncompleted job. Message Id: {uncompletedJob.Item1.MessageId}, Details: {uncompletedJob.Item2.MessageBody}");
                        await _jobProcessorNotifier.CancelJobStartedNotificationAsync(uncompletedJob.Item1);
                        
                        if (lxQueryJobData != null) await ResubmitJobAsync(lxQueryJobData, uncompletedJob.Item2.MessageId, uncompletedJob.Item2.MessageBody);
                    }
                }
            }
            catch (Exception ex)
            {
                if (!_shuttingDown)
                {
                    AppLogger.LogError($"QueryListener.ProcessPreviousRunningJobsAsync() - Caught exception. Error: {ex.Message}", ex);
                }
            }
        }

        private async Task ResubmitJobAsync(LxQueryJobData lxQueryJobData, string messageId, string messageBody)
        {
            if (lxQueryJobData != null)
            {
                _queryProcessor.PurgeJobFolder(lxQueryJobData.JobId);
                
                AppLogger.LogDebug($"[{lxQueryJobData.JobId}] QueryListener.ResubmitJob() - Resubmitting job. Message Id: {messageId}, Details: {messageBody}");

                lxQueryJobData.Resubmitted = true;
                lxQueryJobData.ResubmittedDateTime = DateTime.UtcNow;

                AppLogger.LogMessage($"[{messageId}] QueryListener.ProcessPreviousRunningJobsAsync() - Resubmiting uncompleted job. Job Id: {lxQueryJobData.JobId}, Submitted Date: {lxQueryJobData.SubmittedDateTime}, Details: {messageBody}");
                await _snsTopic.SendNotificationAsync(JsonConvert.SerializeObject(lxQueryJobData));
            }
        }



        private async Task ReadMessageQueue()
        {
            var cloudMessages = new List<ICloudMessage>(Config.MaxConcurrentjobs);

            try
            {
                AppLogger.LogMessage("QueryListener.ReadMessageQueue() - Processing new jobs");

                #region Process new jobs
                while (true)
                {
                    if (_shuttingDown)
                    {
                        break;
                    }

                    cloudMessages.Clear();

                    if (_startTime.AddHours(Config.MonitorCountResetHours) <= DateTime.Now)
                    {
                        _queryProcessor.ReceivedQueryCount = 0;
                        _queryProcessor.InvalidQueryCount = 0;
                        _queryProcessor.FailedQueryCount = 0;
                        _queryProcessor.SuccessQueryCount = 0;
                        _startTime = DateTime.Now;
                    }

                    WaitOnRunningTasksIfMaxed();
                    FinalizeCompletedJobs();

                    try
                    {
                        var maxMessages = Config.MaxConcurrentjobs <= 10 ? Config.MaxConcurrentjobs : 10;
                        cloudMessages.AddRange(await _sqsQueue.ReceiveMessagesAsync(maxMessages, 60, 20));

                        if (cloudMessages.Any())
                        {
                            AppLogger.LogMessage($"QueryListener.ReadMessageQueue() - SQS messages received. Count: {cloudMessages.Count}");

                            _lastMessagesReceivedUtc = DateTime.UtcNow;
                            _lastQueryReceived = await ProcessMessages(cloudMessages);
                        }
                    }
                    catch (Exception ex)
                    {
                        AppLogger.LogError($"QueryListener.ReadMessageQueue() - Caught exception processing job queue. Error: {ex.Message}", ex);
                    }

                    await Task.Delay(_queueReadDelay);
                }
                #endregion Process new jobs
            }
            catch (Exception ex)
            {
                if (!_shuttingDown)
                {
                    AppLogger.LogError($"QueryListener.ReadMessageQueue() - Caught exception while processing. Error: {ex.Message}", ex);
                }
            }
        }


        private void WaitOnRunningTasksIfMaxed()
        {
            if (_runningJobs.Count < _maxConcurrentJobs) return;
            AppLogger.LogMessage($"QueryListener.WaitOnRunningTasksIfMaxed() - Runnings jobs reached max limit. Waiting on runnings jobs to complete. Running Jobs: {_runningJobs.Count}, Max Jobs: {_maxConcurrentJobs}");

            WaitOnRunningTasks();
        }

        private void WaitOnRunningTasks()
        {
            if (!_runningJobs.Any())
            {
                AppLogger.LogDebug($"QueryListener.WaitOnRunningTasksIfMaxed() - No running jobs. Running jobs: {_runningJobs.Count}");
                return;
            }

            AppLogger.LogMessage($"QueryListener.WaitOnRunningTasksIfMaxed() - Allow running jobs to complete. Running jobs: {_runningJobs.Count}");

            try
            {
                var runningJobs = _runningJobs.Select(job => job.Value.Job).ToArray();
                Task.WaitAll(runningJobs);
            }
            catch (AggregateException /*aex*/)
            {
                /* Ignore these since we will be inspecting each task individually */
            }

            AppLogger.LogDebug($"QueryListener.WaitOnRunningTasksIfMaxed() - All running jobs completed. Running jobs: {_runningJobs.Count}");
        }

        private void FinalizeCompletedJobs()
        {
            if (_runningJobs.Any())
            { 
                AppLogger.LogDebug($"QueryListener.FinalizeCompletedJobs() - Finalizing completed jobs.");

                var jobIds = _runningJobs.Keys;
                foreach (var jobId in jobIds)
                {
                    RunningJob runningJob;
                    if (_runningJobs[jobId]?.Job != null && _runningJobs[jobId].Job.IsCompleted && _runningJobs.TryRemove(jobId, out runningJob))
                    {
                        LogJobStatus(runningJob);

                        try {  runningJob.Job.Dispose(); }
                        catch(Exception) { /* Ignore */ }
                    }
                }

                AppLogger.LogDebug($"QueryListener.FinalizeCompletedJobs() - All completed jobs finalized.");
            }
        }

        private static void LogJobStatus(RunningJob runningJob)
        {
            switch (runningJob.Job.Status)
            {
                case TaskStatus.Canceled:
                    AppLogger.LogMessage($"[{runningJob.JobDetails.JobId}] QueryListener.LogJobStatus() - Job was canceled");
                    break;

                case TaskStatus.Faulted:
                    AppLogger.LogError($"[{runningJob.JobDetails.JobId}] QueryListener.LogJobStatus() - Job failed due to exception. Error: {runningJob.Job.Exception?.Message ?? "Unknown"}", runningJob.Job.Exception?.Flatten());
                    break;

                case TaskStatus.RanToCompletion:
                    AppLogger.LogMessage($"[{runningJob.JobDetails.JobId}] QueryListener.LogJobStatus() - Job completed successfully");
                    break;
                default:
                    AppLogger.LogMessage($"[{runningJob.JobDetails.JobId}] QueryListener.LogJobStatus() - Unknown job status");
                    break;
            }
        }

        private async Task<DateTime> ProcessMessages(IList<ICloudMessage> messages)
        {
            if (messages == null) throw new ArgumentNullException(nameof(messages));

            var last = DateTime.MinValue;

            foreach (var cloudMessage in messages)
            {
                last = DateTime.Now;

                WaitOnRunningTasksIfMaxed();
                FinalizeCompletedJobs();

                AppLogger.LogDebug($"[{cloudMessage.MessageId}] QueryListener.ProcessMessages() - Processing message. Deleting from queue. Queue: {_queueName}, Details: {cloudMessage.MessageBody}");
                await _sqsQueue.DeleteMessagesAsync(new List<string> { cloudMessage.ReceiptHandle } );

                if (!string.IsNullOrWhiteSpace(cloudMessage.MessageBody))
                {
                    AppLogger.LogMessage($"[{cloudMessage.MessageId}] QueryListener.ProcessMessages() - Processing message.");

                    LxQueryJobData lxQueryJobData = null;
                    try { lxQueryJobData = JsonConvert.DeserializeObject<LxQueryJobData>(cloudMessage.MessageBody); }
                    catch(Exception) { /*Ignore any exceptions*/ }

                    if (!string.IsNullOrWhiteSpace(lxQueryJobData?.JobId))
                    {
                        if (_runningJobs.ContainsKey(lxQueryJobData.JobId))
                        {
                            AppLogger.LogMessage($"[{cloudMessage.MessageId}] QueryListener.ProcessMessages() - Unable to create job as a job with the same id already exists. Job Id: {lxQueryJobData.JobId}, Details: {cloudMessage.MessageBody}");
                        }
                        else
                        {
                            if (!await _jobProcessorNotifier.NotifyJobStartedAsync(cloudMessage, lxQueryJobData))
                            {
                                AppLogger.LogError($"[{lxQueryJobData.JobId}] QueryListener.ProcessMessages() - Failed to send job start notification. Resubmitting job. Message Id: {cloudMessage.MessageId}, Job Details: {cloudMessage.MessageBody}", null);
                                await ResubmitJobAsync(lxQueryJobData, cloudMessage.MessageId, cloudMessage.MessageBody);

                                continue;
                            }

                            AppLogger.LogMessage($"[{cloudMessage.MessageId}] QueryListener.ProcessMessages() - Creating job. Job Id: {lxQueryJobData.JobId}, Job Details: {cloudMessage.MessageBody}");

                            var newJob = default(RunningJob);
                            var cancelJob = false;
                            try
                            {
                                var ctx = new CancellationTokenSource();
                                var job = Task.Run(async () =>
                                {
                                    try
                                    {
                                        AppLogger.LogDebug($"[{lxQueryJobData.JobId}] QueryListener.ProcessMessages() - Start processing job");
                                        if (await ProcessJobRequest(lxQueryJobData, ctx.Token))
                                            AppLogger.LogMessage($"[{lxQueryJobData.JobId}] QueryListener.ProcessMessages() - Job succeeded. Job Details: {cloudMessage.MessageBody}");
                                        else
                                            AppLogger.LogMessage($"[{lxQueryJobData.JobId}] QueryListener.ProcessMessages() - Job failed. Job Details: {cloudMessage.MessageBody}");
                                    }
                                    finally 
                                    {
                                        if (!await _jobProcessorNotifier.NotifyJobCompletedAsync(lxQueryJobData))
                                            AppLogger.LogWarning($"[{lxQueryJobData.JobId}] QueryListener.ProcessMessages() - Failed to send job completed notification. Job will eventually be resubmitted.", null);
                                    }


                                }, CancellationToken.None);
                                newJob = new RunningJob(lxQueryJobData, job, ctx);
                            }
                            catch (Exception ex)
                            {
                                AppLogger.LogError($"[{lxQueryJobData.JobId}] QueryListener.ProcessMessages() - Failed to create job. Job Details: {cloudMessage.MessageBody}, Error: {ex.Message}", ex);
                            }

                            if (newJob != null)
                            {
                                try
                                {
                                    var addedJob = _runningJobs.AddOrUpdate(lxQueryJobData.JobId, newJob, (jobId, existingJob) => newJob);
                                    if (addedJob == null)
                                    {
                                        AppLogger.LogError($"[{lxQueryJobData.JobId}] QueryListener.ProcessMessages() - Failed to add new job to list of running jobs. Canceling job. Job Details: {cloudMessage.MessageBody}", null);
                                        cancelJob = true;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    AppLogger.LogError($"[{lxQueryJobData.JobId}] QueryListener.ProcessMessages() - Error occurred while adding new job to list of running jobs. Canceling job. Job Details: {cloudMessage.MessageBody}, Error: {ex.Message}", ex);
                                    cancelJob = true;
                                }

                                if (cancelJob)
                                {
                                    try { newJob.CancellationTokenSource.Cancel(); }
                                    catch (Exception ex) { AppLogger.LogError($"[{lxQueryJobData.JobId}] QueryListener.ProcessMessages() - Failed to cancel job. Job Details: {cloudMessage.MessageBody}, Error: {ex.Message}", ex); }
                                }
                            }
                        }
                    }
                    else
                    {
                        AppLogger.LogError($"QueryListener.ProcessMessages() - Failed to process message as it was unknown. Message will not be resubmitted. Message: {cloudMessage}", null);

                        // This message type doesn't exist
                        // This code needs to be implemented
                        /*
                        JobCancellationRequest jobCancellationRequest = null;
                        try { jobCancellationRequest = JsonConvert.DeserializeObject<JobCancellationRequest>(cloudMessage.MessageBody); }
                        catch(Exception) {  }
                        
                        CancelJob(jobCancellationRequest.JobId);
                        */
                    }
                }
                else
                    AppLogger.LogError($"QueryListener.ProcessMessages() - Failed to process message as it was blank. Message: {cloudMessage}", null);
            }

            return last;
        }

        private async Task<bool> ProcessJobRequest(LxQueryJobData lxQueryJobData, CancellationToken ct)
        {
            if (lxQueryJobData == null) throw new ArgumentNullException(nameof(lxQueryJobData));

            var jobSucceeded = false;

            try
            {
                AppLogger.LogDebug($"[{lxQueryJobData.JobId}] QueryListener.ProcessJobRequest() - Job details: {JsonConvert.SerializeObject(lxQueryJobData)}");

                if (lxQueryJobData.SendNotifications != null && lxQueryJobData.SendNotifications.Value)
                {
                    var t4Template = new JobSubmission
                    {
                        _lxQueryJobData = lxQueryJobData,
                        _emailSubject = Config.JobStartedSubject
                    };

                    var template = new EmailTemplate()
                    {
                        FromAddress = Config.EmailFromAddress,
                        MessageBody = t4Template.TransformText(),
                        Subject = t4Template._emailSubject
                    };

                    try
                    {
                        await EmailHelper.SendEmail(lxQueryJobData, template);
                    }
                    catch (Exception ex)
                    {
                        AppLogger.LogError($"[{lxQueryJobData.JobId}] QueryListener.ProcessJobRequest() - Error sending email. Subject: {template?.Subject ?? "Unknown"}, To: {lxQueryJobData.RecipientEmailAddress}, Error: {ex.Message}", ex);
                        throw;
                    }
                }

                jobSucceeded = await _queryProcessor.Process(lxQueryJobData, ct);

            }
            catch (Exception ex)  
            {
                AppLogger.LogError($"[{lxQueryJobData.JobId}] QueryListener.ProcessJobRequest() - Error processing job request. Error: {ex.Message}", ex);
            }

            return jobSucceeded;
        }

        private void CancelJob(string jobId)
        {
            RunningJob runningJob;
            if (_runningJobs.TryGetValue(jobId, out runningJob) && !runningJob.Job.IsCompleted)
            {
                runningJob.CancellationTokenSource.Cancel();
            }
        }

        public List<KeyValuePair<string, string>> GetMonitorigStatus()
        {
            var monitoringStatus = new List<KeyValuePair<string, string>>();

            var utcNow = DateTime.UtcNow;
            var lastQueryReceivedTimestamp = DateTime.MinValue;
            var lastQueryProcessedTimestamp = DateTime.MinValue;
            var invalidCount = 0;
            var failedCount = 0;
            var receivedCount = 0;
            var successCount = 0;
            var ageReceived = 0.0;
            var ageProcessed = 0.0;

            invalidCount = _queryProcessor.InvalidQueryCount;
            failedCount = _queryProcessor.FailedQueryCount;
            receivedCount = _queryProcessor.ReceivedQueryCount;
            successCount = _queryProcessor.SuccessQueryCount;

            lastQueryReceivedTimestamp = _lastMessagesReceivedUtc;
            lastQueryProcessedTimestamp = _queryProcessor.LastQueryProcessedUTC;
            ageReceived = Math.Round((utcNow - lastQueryReceivedTimestamp).TotalSeconds, 0, MidpointRounding.AwayFromZero);
            ageProcessed = Math.Round((utcNow - lastQueryProcessedTimestamp).TotalSeconds, 0, MidpointRounding.AwayFromZero);

            monitoringStatus.Add(new KeyValuePair<string, string>("invalidQueryCount", invalidCount.ToString(CultureInfo.InvariantCulture)));
            monitoringStatus.Add(new KeyValuePair<string, string>("failedQueryCount", failedCount.ToString(CultureInfo.InvariantCulture)));
            monitoringStatus.Add(new KeyValuePair<string, string>("receivedQueryCount", receivedCount.ToString(CultureInfo.InvariantCulture)));
            monitoringStatus.Add(new KeyValuePair<string, string>("successQueryCount", successCount.ToString(CultureInfo.InvariantCulture)));
            monitoringStatus.Add(new KeyValuePair<string, string>("lastQueryReceivedTimestampUtc", lastQueryReceivedTimestamp.ToString(CultureInfo.InvariantCulture)));
            monitoringStatus.Add(new KeyValuePair<string, string>("lastQueryProcessedTimestampUtc", lastQueryProcessedTimestamp.ToString(CultureInfo.InvariantCulture)));
            monitoringStatus.Add(new KeyValuePair<string, string>("ageLastQueryReceivedSeconds", ageReceived.ToString(CultureInfo.InvariantCulture)));
            monitoringStatus.Add(new KeyValuePair<string, string>("ageLastQueryProcessedSeconds", ageProcessed.ToString(CultureInfo.InvariantCulture)));

            return monitoringStatus;
        }


        private class RunningJob
        {
            public RunningJob(LxQueryJobData jobDetails, Task job, CancellationTokenSource ctx)
            {
                Job = job;
                CancellationTokenSource = ctx;
                JobDetails = jobDetails;
            }

            public CancellationTokenSource CancellationTokenSource { get; private set; }

            public Task Job { get; private set; }

            public LxQueryJobData JobDetails { get; private set; } 
        }
    }
}
