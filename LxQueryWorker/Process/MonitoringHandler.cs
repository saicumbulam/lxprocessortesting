﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

using Newtonsoft.Json.Linq;

using Aws.Core.Data.Common.Operations;
using Aws.Core.Utilities;

using LxQueryWorker.Common;
using LxQueryWorker.Interfaces;

namespace LxQueryWorker.Process
{
    public class MonitoringHandler
    {
        private readonly string _monitorHmtl;
        private readonly DateTime _startTime;
        private readonly IStatusProvider _statusProvider;

        public MonitoringHandler(IStatusProvider statusProvider)
        {
            _statusProvider = statusProvider;

            _startTime = DateTime.UtcNow;

            try
            {
                _monitorHmtl = File.ReadAllText(AppUtils.AppLocation + @"\Monitor.html", Encoding.UTF8);
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventId.Process.MonitoringHandler.LoadMonitorFile, "Unable to load monitor html file", ex);
                _monitorHmtl = "Unavailable";
            }
        }

        public void Start()
        {
            try
            {
                OpsCommandHttpEndpointSvc.Start((cmd) => MonitorStatusCheck(cmd));
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventId.Process.MonitoringHandler.StartOpsHttp, "Unable to start ecv http endpoint", ex);
            }
        }

        public void Stop()
        {
            try
            {
                OpsCommandHttpEndpointSvc.Stop();
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventId.Process.MonitoringHandler.StopOpsHttp, "Unable to stop ecv http endpoint", ex);
            }
        }


        private string MonitorStatusCheck(string cmd)
        {
            var statusData = GetStatusData();

            if (cmd.Trim().Equals("PRTG", StringComparison.InvariantCultureIgnoreCase))
            {
                return GeneratePrtgStatus(statusData);
            }
            else
            {
                return GenerateHtmlStatus(statusData);
            }

        }

        private string GeneratePrtgStatus(IEnumerable<KeyValuePair<string, string>> statusData)
        {
            var j = new JObject();

            foreach (var status in statusData)
            {
                j.Add(status.Key, status.Value);
            }
            return j.ToString();
        }

        private const string NA = "-";
        private string GenerateHtmlStatus(IEnumerable<KeyValuePair<string, string>> statusData)
        {
            string html = null;

            try
            {
                var sd = statusData as KeyValuePair<string, string>[] ?? statusData.ToArray();
                html = string.Format(_monitorHmtl,
                    AppUtils.GetVersion(), //0
                    DateTime.UtcNow.ToString(new CultureInfo("en-US")),

                    sd.FirstOrDefault(s => s.Key.Equals("lastQueryReceivedTimestampUtc")).Value ?? NA,
                    sd.FirstOrDefault(s => s.Key.Equals("ageLastQueryReceivedSeconds")).Value ?? NA,

                    sd.FirstOrDefault(s => s.Key.Equals("lastQueryProcessedTimestampUtc")).Value ?? NA,
                    sd.FirstOrDefault(s => s.Key.Equals("ageLastQueryProcessedSeconds")).Value ?? NA,

                    sd.FirstOrDefault(s => s.Key.Equals("receivedQueryCount")).Value ?? NA,
                    sd.FirstOrDefault(s => s.Key.Equals("invalidQueryCount")).Value ?? NA,
                    sd.FirstOrDefault(s => s.Key.Equals("failedQueryCount")).Value ?? NA,
                    sd.FirstOrDefault(s => s.Key.Equals("successQueryCount")).Value ?? NA
                    );
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventId.Process.MonitoringHandler.BuildMonitorHtml, "Unable to build monitor html file", ex);
            }

            return html;
        }

        private IEnumerable<KeyValuePair<string, string>> GetStatusData()
        {
            var statusData = new List<KeyValuePair<string, string>>();
                statusData.AddRange(_statusProvider.GetMonitorigStatus());

            return statusData;
        }

    }
}
