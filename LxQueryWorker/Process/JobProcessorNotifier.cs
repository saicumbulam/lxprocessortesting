﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Aws.Core.Utilities;
using En.Ods.Lib.Common.Dynamo;
using En.Ods.Lib.Common.Interface;
using En.Ods.Lib.Common.Serialization;
using LxPortalLib.Models;
using LxQueryWorker.Model;
using Newtonsoft.Json;
using LxQueryWorker.Common;
using MoreLinq;

namespace LxQueryWorker.Process
{
    public class JobProcessorNotifier
    {
        private readonly string _topicName;
        private readonly string _queueName;
        private ICloudQueue _sqsQueue;
        private bool _isRunning;
        private System.Threading.Timer _jobNotificationCleanupTimer;

        public JobProcessorNotifier(string topicName, string queueName)
        {
            _topicName = topicName;
            _queueName = queueName;
        }

        public void Start()
        {
            Init();

            StartTimer();

            _isRunning = true;
        }

        private void Init()
        {
            var emptyCredentials = new JsonOdsValue();

            var snsTopic = default(ICloudTopic);
            try
            {
                snsTopic = new SnsFactory().GetNotifier(emptyCredentials).GetTopic(_topicName);
            }
            catch (Exception ex)
            {
                AppLogger.LogError($"JobProcessorNotifier.Init() - Error retrieving SNS topic notifier. Error: {ex.Message}", ex);
            }

            if (snsTopic == null)
                throw new InvalidOperationException($"JobProcessorNotifier.Init() - Failed to retrieve SNS topic notifier. Topic: {_topicName}");

            try
            {
                var dynamicQueue = (IDynamicQueueFromNotifier)new SqsFactory().GetQueueing(emptyCredentials);
                _sqsQueue = dynamicQueue.GetCleanQueueConnectedToTopicAsync(_queueName, snsTopic, 999).GetAwaiter().GetResult();
            }
            catch (Exception ex)
            {
                AppLogger.LogError($"JobProcessorNotifier.Init() - Error retrieving SQS queue. Queue: {_queueName}, Topic: {_topicName}, Error: {ex.Message}", ex);
            }

            try { snsTopic.Dispose(); }
            catch (Exception ex) { AppLogger.LogError($"JobProcessorNotifier.Init() - Error disposing SNS topic notifier.  Topic: {_topicName}, Error: {ex.Message}", ex); }
            snsTopic = null;

            if (_sqsQueue == null)
                throw new InvalidOperationException($"JobProcessorNotifier.Init() - Failed to retrieve SQS queue. Queue: {_queueName}, Topic: {_topicName}");
        }

        public void Stop()
        {
            StopTimer();

            try { _sqsQueue?.Dispose(); }
            catch (Exception ex) { AppLogger.LogError($"JobProcessorNotifier.Dispose() - Error disposing SQS queue.  Queue: {_queueName}, Error: {ex.Message}", ex); }

            _sqsQueue = null;
            _isRunning = false;
        }

        public async Task<bool> NotifyJobStartedAsync(ICloudMessage message, LxQueryJobData lxQueryJobData)
        {
            if (!_isRunning || _sqsQueue == null)
            {
                AppLogger.LogWarning($"[{lxQueryJobData.JobId}] JobProcessorNotifier.NotifyJobStarted() - Unable to send Job Started notification as JobNotifier is not running. Queue: {_queueName}", null);
                return false;
            }

            var result = true;
            try
            {
                var jobStarted = new JobStarted
                {
                    JobId = lxQueryJobData.JobId,
                    StartDateTime = lxQueryJobData.SubmittedDateTime,
                    ReceiptHandle = message.ReceiptHandle,
                    MessageId = message.MessageId,
                    MessageBody = message.MessageBody
                };

                AppLogger.LogDebug($"[{lxQueryJobData.JobId}] JobProcessorNotifier.NotifyJobStarted() - Sending Job Started notification. Queue: {_queueName}");
                await _sqsQueue.SendMessagesAsync(0, new List<string> { JsonConvert.SerializeObject(jobStarted) });
            }
            catch (Exception ex)
            {
                AppLogger.LogError($"[{lxQueryJobData.JobId}] JobProcessorNotifier.NotifyJobStarted() - Error sending Job Started notification. Queue: {_queueName}, Error: {ex.Message}", ex);
                result = false;
            }
            return result;
        }

        public async Task<bool> NotifyJobCompletedAsync(LxQueryJobData lxQueryJobData)
        {
            if (!_isRunning || _sqsQueue == null)
            {
                AppLogger.LogWarning($"[{lxQueryJobData.JobId}] JobProcessorNotifier.NotifyJobCompleted() - Unable to send Job Completed notification as JobNotifier is not running. Queue: {_queueName}", null);
                return false;
            }

            var result = true;
            try
            {
                var jobCompleted = new JobCompleted
                {
                    CompletedDateTime = DateTime.UtcNow,
                    JobDetails = lxQueryJobData
                };

                AppLogger.LogDebug($"[{lxQueryJobData.JobId}] JobProcessorNotifier.NotifyJobCompleted() - Sending Job Completed notification. Queue: {_queueName}");
                await _sqsQueue.SendMessagesAsync(0, new List<string> {JsonConvert.SerializeObject(jobCompleted)});
            }
            catch (Exception ex)
            {
                AppLogger.LogError($"[{lxQueryJobData?.JobId}] JobProcessorNotifier.NotifyJobCompleted() - Error sending Job Completed notification. Queue: {_queueName}, Error: {ex.Message}", ex);
                result = false;
            }
            return result;
        }

        private async Task<bool> DeleteCompletedJobNotificationsAsync()
        {
            var result = true;
            var receiptHandles = new List<string>();

            try
            {
                var jobNotifications = await GetJobNotifications();

                AppLogger.LogDebug($"JobProcessorNotifier.DeleteCompletedJobNotificationsAsync() - Retrieved job notifications. Queue: {_queueName}, Jobs Started: {jobNotifications.JobsStarted.Count}, Jobs Completed: {jobNotifications.JobsCompleted.Count}, Unknown Message Types: {jobNotifications.UnknownMessageTypes.Count}");

                // Add all duplicate JobStarted notifications to receiptHandles except 1
                foreach (var jobStartedDuplicate in jobNotifications.JobsStarted.GroupBy(js => js.Item2.JobId).Where(jobStartedDuplicate => jobStartedDuplicate.Count() > 1))
                {
                    AppLogger.LogDebug($"JobProcessorNotifier.DeleteCompletedJobNotificationsAsync() - Found duplicate JobStarted notifications for job. Removing duplicates. Job Id: {jobStartedDuplicate.Key}, Duplicate Count: {jobStartedDuplicate.Count()}");
                    receiptHandles.AddRange(jobStartedDuplicate.Select(s => s.Item2.ReceiptHandle).Skip(1));
                }

                // Remove all JobStarted notifications that have been added to receiptHandles
                if (receiptHandles.Any()) jobNotifications.JobsStarted.RemoveAll(s => receiptHandles.Contains(s.Item2.ReceiptHandle));


                // Add all duplicate JobCompleted notifications to receiptHandles except 1
                foreach (var jobCompletedDuplicate in jobNotifications.JobsCompleted.GroupBy(js => js.Item2.JobDetails.JobId).Where(jobCompletedDuplicate => jobCompletedDuplicate.Count() > 1))
                {
                    AppLogger.LogDebug($"JobProcessorNotifier.DeleteCompletedJobNotificationsAsync() - Found duplicate JobCompleted notifications for job. Removing duplicates. Job Id: {jobCompletedDuplicate.Key}, Duplicate Count: {jobCompletedDuplicate.Count()}");
                    receiptHandles.AddRange(jobCompletedDuplicate.Select(s => s.Item1.ReceiptHandle).Skip(1));
                }

                // Remove all JobStarted notifications that have been added to receiptHandles
                if (receiptHandles.Any()) jobNotifications.JobsCompleted.RemoveAll(s => receiptHandles.Contains(s.Item1.ReceiptHandle));

                AppLogger.LogDebug($"JobProcessorNotifier.DeleteCompletedJobNotificationsAsync() - Filtered out duplicate job notifications. Jobs Started: {jobNotifications.JobsStarted.Count}, Jobs Completed: {jobNotifications.JobsCompleted.Count}, Unknown Message Types: {jobNotifications.UnknownMessageTypes.Count}");


                // Get all JobStarted notifications that have a matching JobCompleted notification
                receiptHandles.AddRange(jobNotifications.JobsStarted.Join(jobNotifications.JobsCompleted, f => f.Item2.JobId, s => s.Item2.JobDetails.JobId, (first, second) => first.Item1.ReceiptHandle));
                
                // Get all JobCompleted notifications that have a matching JobStarted notification
                receiptHandles.AddRange(jobNotifications.JobsCompleted.Join(jobNotifications.JobsStarted, f => f.Item2.JobDetails.JobId, s => s.Item2.JobId, (first, second) => first.Item1.ReceiptHandle));

                // Get all JobCompleted notifications that do not have a matching JobStarted notification
                // TODO: Revist how to handle these potentially orphaned notifications. Issue here is if server 1 is reading from the queue at the same time server 2 is reading from the queue, a JobStart notification can be hidden.
                //receiptHandles.AddRange(jobNotifications.JobsCompleted.Where(jc => jobNotifications.JobsStarted.All(js => jc.Item2.CompletedDateTime.AddHours(2) <= DateTime.UtcNow && !string.Equals(js.Item2.JobId, jc.Item2.JobDetails.JobId, StringComparison.InvariantCultureIgnoreCase))).Select(jc => jc.Item1.ReceiptHandle));

                // Get all UnknownMessageTypes
                receiptHandles.AddRange(jobNotifications.UnknownMessageTypes.Select(j => j.ReceiptHandle));

                // Filter out any duplicate entries in receiptHandles
                receiptHandles = receiptHandles.Distinct().ToList();

                AppLogger.LogDebug($"JobProcessorNotifier.DeleteCompletedJobNotificationsAsync() - Identified job notifications to be deleted. Receipt Handles: {receiptHandles.Count}");
            }
            catch (Exception ex)
            {
                AppLogger.LogError($"JobProcessorNotifier.DeleteCompletedJobNotificationsAsync() - Error retrieving job notifications. Queue: {_queueName}, Error: {ex.Message}", ex);
                result = false;
            }

            try
            {
                if (result && receiptHandles.Any())
                {
                    AppLogger.LogDebug($"JobProcessorNotifier.DeleteCompletedJobNotificationsAsync() - Deleting messages. Queue: {_queueName}, Message Count: {receiptHandles.Count}");
                    await _sqsQueue.DeleteMessagesAsync(receiptHandles);
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogError($"JobProcessorNotifier.DeleteCompletedJobNotificationsAsync() - Error deleting completed job notifications. Queue: {_queueName}, Error: {ex.Message}", ex);
                result = false;
            }

            return result;
        }

        /// <summary>
        /// Gets a list of JobStarted notifications that do not have a
        /// matching JobCompleted notification.
        /// The method returns as list of Tuple pair.
        /// Item1=ICloudMessage - this is the original job notification message.
        /// Item2=JobStarted - <seealso cref="LxQueryWorker.Model.JobStarted" />.
        /// </summary>
        public async Task<List<Tuple<ICloudMessage, JobStarted>>> GetUncompletedJobNotificationsAsync()
        {
            var jobNotifications = await GetJobNotifications();

            AppLogger.LogDebug($"JobProcessorNotifier.GetUncompletedJobNotificationsAsync() - Finished calling GetJobNotifications. Jobs Started: {jobNotifications.JobsStarted.Count}, Jobs Completed: {jobNotifications.JobsCompleted.Count}, Unknown Messages: {jobNotifications.UnknownMessageTypes.Count}");

            var uncompletedJobs = jobNotifications.JobsStarted.Where(js => !jobNotifications.JobsCompleted.Any(jc => jc.Item2.JobDetails.JobId.Equals(js.Item2.JobId, StringComparison.InvariantCultureIgnoreCase))).ToList();

            return uncompletedJobs;
        }

        public async Task<bool> CancelJobStartedNotificationAsync(ICloudMessage jobStartedMessage)
        {
            try
            {
                await _sqsQueue.DeleteMessagesAsync(new List<string> { jobStartedMessage.ReceiptHandle });
            }
            catch (Exception ex)
            {
                AppLogger.LogError($"JobProcessorNotifier.CancelJobStartedNotificationAsync() - Error deleting job start notification. Queue: {_queueName}, Message Body: {jobStartedMessage.MessageBody}, Error: {ex.Message}", ex);
                return false;
            }
            return true;
        }

        private async Task<JobNotifications> GetJobNotifications()
        {
            var jobNotifications = new JobNotifications();

            try
            {
                var messages = new List<ICloudMessage>();

                while (true)
                {
                    var msgs = await _sqsQueue.ReceiveMessagesAsync(10, 5);
                    if (msgs == null || !msgs.Any()) break;
                    messages.AddRange(msgs);
                }
                
                if (messages.Any())
                {
                    AppLogger.LogDebug($"JobProcessorNotifier.GetJobNotifications() - Processing messages. Count: {messages.Count}");

                    foreach (var message in messages)
                    {
                        AppLogger.LogDebug($"JobProcessorNotifier.GetJobNotifications() - Processing message. Details: {message.MessageBody}");

                        var jobStarted = default(JobStarted);
                        var jobCompleted = default(JobCompleted);

                        try { jobStarted = JsonConvert.DeserializeObject<JobStarted>(message.MessageBody); }
                        catch (Exception) { /*Ignore any exceptions*/ }

                        if (jobStarted == null || (string.IsNullOrWhiteSpace(jobStarted?.JobId)
                                                   && string.IsNullOrWhiteSpace(jobStarted.MessageBody)
                                                   && string.IsNullOrWhiteSpace(jobStarted.ReceiptHandle)))
                        {
                            try { jobCompleted = JsonConvert.DeserializeObject<JobCompleted>(message.MessageBody); }
                            catch (Exception) { /*Ignore any exceptions*/ }
                        }

                        if (!string.IsNullOrWhiteSpace(jobStarted?.JobId) && !string.IsNullOrWhiteSpace(jobStarted.MessageBody) && !string.IsNullOrWhiteSpace(jobStarted.ReceiptHandle))
                        {
                            AppLogger.LogDebug($"JobProcessorNotifier.GetJobNotifications() - Adding JobStarted. Details: {message.MessageBody}");
                            jobNotifications.JobsStarted.Add(new Tuple<ICloudMessage, JobStarted>(message, jobStarted));
                        }
                        else if (!string.IsNullOrWhiteSpace(jobCompleted?.JobDetails?.JobId))
                        {
                            AppLogger.LogDebug($"JobProcessorNotifier.GetJobNotifications() - Adding JobCompleted. Details: {message.MessageBody}");
                            jobNotifications.JobsCompleted.Add(new Tuple<ICloudMessage, JobCompleted>(message, jobCompleted));
                        }
                        else
                        {
                            AppLogger.LogDebug($"JobProcessorNotifier.GetJobNotifications() - Adding UnknownMessageType. Details: {message.MessageBody}");
                            jobNotifications.UnknownMessageTypes.Add(message);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogError($"JobProcessorNotifier.GetJobNotifications() - Error retrieving job notifications. Queue: {_queueName}, Error: {ex.Message}", ex);
            }

            return jobNotifications;
        }

        private void StartTimer()
        {
            if (_jobNotificationCleanupTimer != null) StopTimer();

            _jobNotificationCleanupTimer = new System.Threading.Timer(async (e) =>
            {
                AppLogger.LogMessage($"JobProcessorNotifier.JobNotificationsTimer() - Completed job notifications clean up started.");

                var result = await DeleteCompletedJobNotificationsAsync();

                AppLogger.LogMessage($"JobProcessorNotifier.JobNotificationsTimer() - Completed job notifications clean up finished. Success: {result}");

            }, null, (int)TimeSpan.FromMinutes(1).TotalMilliseconds, (int)TimeSpan.FromMinutes(Config.JobNotificationCleanUpFrequencyMinutes).TotalMilliseconds);
        }

        private void StopTimer()
        {
            try
            {
                _jobNotificationCleanupTimer?.Dispose();
            }
            catch (Exception ex)
            {
                AppLogger.LogError($"JobProcessorNotifier.StopTimer() - Error disposing job notification cleanup timer. Error: {ex.Message}", ex);
            }
            
            _jobNotificationCleanupTimer = null;
        }

        private class JobNotifications
        {
            public JobNotifications()
            {
                JobsStarted = new List<Tuple<ICloudMessage, JobStarted>>();
                JobsCompleted = new List<Tuple<ICloudMessage, JobCompleted>>();
                UnknownMessageTypes = new List<ICloudMessage>();
            }

            public List<Tuple<ICloudMessage, JobStarted>> JobsStarted { get; set; }
            public List<Tuple<ICloudMessage, JobCompleted>> JobsCompleted { get; set; }
            public List<ICloudMessage> UnknownMessageTypes { get; set; }
        }
    }
}
