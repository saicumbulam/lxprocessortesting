﻿using System;
using LxCommon;
using LxCommon.Models;
using Newtonsoft.Json;

namespace LxQueryWorker.Model
{
    public class PortionV2
    {
        public PortionV2()
        {

        }

        public PortionV2(Portion src, Flash parent)
        {
            PulseType = src.Type == FlashType.FlashTypeGlobalCG ? FlashType.FlashTypeCG : src.Type;
            Time = DateTime.Parse(src.TimeStamp);
            Longitude = src.Longitude;
            Latitude = src.Latitude;
            IcHeight = (src.Type == FlashType.FlashTypeGlobalIC
                       || src.Type == FlashType.FlashTypeIC
                       || src.Type == FlashType.FlashTypeICNarrowBipolar)
                        ?  src.Height
                        : 0;
            PeakCurrent = src.Amplitude;
            NumberOfSensors = src.StationOffsets?.Count ?? 0;
            Multiplicity = (src.Type == FlashType.FlashTypeGlobalIC
                           || src.Type == FlashType.FlashTypeIC
                           || src.Type == FlashType.FlashTypeICNarrowBipolar)
                            ? parent.IcMultiplicity
                            : parent.CgMultiplicity;
        }

        [JsonProperty(PropertyName = "pulseType")]
        public FlashType PulseType { get; set; }

        [JsonProperty(PropertyName = "time")]
        public DateTime Time { get; set; }

        [JsonProperty(PropertyName = "latitude")]
        public double Latitude { get; set; }

        [JsonProperty(PropertyName = "longitude")]
        public double Longitude { get; set; }

        [JsonProperty(PropertyName = "peakCurrent")]
        public double PeakCurrent { get; set; }

        [JsonProperty(PropertyName = "icHeight")]
        public double IcHeight { get; set; }

        [JsonProperty(PropertyName = "numberOfSensors")]
        public int NumberOfSensors { get; set; }

        [JsonProperty(PropertyName = "multiplicity")]
        public int? Multiplicity { get; set; }
    }
}
