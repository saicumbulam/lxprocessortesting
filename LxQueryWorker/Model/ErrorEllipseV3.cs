﻿using Newtonsoft.Json;

namespace LxQueryWorker.Model
{
    [JsonObject("ee")]
    public class ErrorEllipseV3
    {
        [JsonProperty(PropertyName = "maj")]
        public double? MajorAxis { get; set; }

        [JsonProperty(PropertyName = "min")]
        public double? MinorAxis { get; set; }

        [JsonProperty(PropertyName = "b")]
        public double? MajorAxisBearing { get; set; }

        public string ToJsonString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
