﻿using System;

namespace LxQueryWorker.Model
{
    public class JobStarted
    {
        /// <summary>
        /// JobId from LxQueryJobData
        /// </summary>
        public string JobId { get; set; }

        /// <summary>
        /// MessageBody from original job submission (ICloudMessage).
        /// Will contain a serialized LxQueryJobData object.
        /// </summary>
        public string MessageBody { get; set; }

        /// <summary>
        /// MessageId from original job submission (ICloudMessage).
        /// </summary>
        public string MessageId { get; set; }


        /// <summary>
        /// MessageBodyReceiptHandle from original job submission (ICloudMessage).
        /// </summary>
        public string ReceiptHandle { get; set; }

        /// <summary>
        /// SubmittedDateTime from LxQueryJobData
        /// </summary>
        public DateTime StartDateTime { get; set; }
    }
}
