﻿using System;
using LxPortalLib.Models;

namespace LxQueryWorker.Model
{
    public class JobCompleted
    {
        public LxQueryJobData JobDetails { get; set; }
        public DateTime CompletedDateTime { get; set; }
    }
}
