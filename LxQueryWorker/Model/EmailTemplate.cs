﻿namespace LxQueryWorker.Model
{
    internal class EmailTemplate
    {
        public string FromAddress { get; set; }
        public string Subject { get; set; }
        public string MessageBody { get; set; }
    }
}
