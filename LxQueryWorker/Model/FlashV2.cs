﻿using System;
using System.Collections.Generic;
using LxCommon;
using LxCommon.Models;
using Newtonsoft.Json;

namespace LxQueryWorker.Model
{
    public class FlashV2
    {
        public FlashV2()
        {

        }

        public FlashV2(Flash src)
        {
            FlashType = src.Type == FlashType.FlashTypeGlobalCG ? FlashType.FlashTypeCG : src.Type;
            Time = DateTime.Parse(src.TimeStamp);
            Longitude = src.Longitude;
            Latitude = src.Latitude;
            IcHeight = (src.Type == FlashType.FlashTypeGlobalIC
                       || src.Type == FlashType.FlashTypeIC
                       || src.Type == FlashType.FlashTypeICNarrowBipolar)
                        ? src.Height
                        : 0;
            PeakCurrent = src.Amplitude;
            NumberSensors = src.NumberSensors;
            Multiplicity = (src.Type == FlashType.FlashTypeGlobalIC
                           || src.Type == FlashType.FlashTypeIC
                           || src.Type == FlashType.FlashTypeICNarrowBipolar)
                            ? src.IcMultiplicity
                            : src.CgMultiplicity;

            foreach (var portion in src.Portions)
            {
                if (Portions == null) Portions = new List<PortionV2>();
                Portions.Add(new PortionV2(portion, src));
            }
        }

        [JsonProperty(PropertyName = "flashType")]
        public FlashType FlashType { get; set; }

        [JsonProperty(PropertyName = "time")]
        public DateTime Time { get; set; }

        [JsonProperty(PropertyName = "latitude")]
        public double Latitude { get; set; }

        [JsonProperty(PropertyName = "longitude")]
        public double Longitude { get; set; }

        [JsonProperty(PropertyName = "peakCurrent")]
        public double? PeakCurrent { get; set; }

        [JsonProperty(PropertyName = "icHeight")]
        public double IcHeight { get; set; }

        [JsonProperty(PropertyName = "numberSensors")]
        public int? NumberSensors { get; set; }

        [JsonProperty(PropertyName = "multiplicity")]
        public int? Multiplicity { get; set; }

        [JsonProperty(PropertyName = "portions", NullValueHandling = NullValueHandling.Ignore)]
        public List<PortionV2> Portions { get; set; }
    }
}
