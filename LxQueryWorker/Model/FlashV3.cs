﻿using System;
using System.Collections.Generic;
using LxCommon;
using LxCommon.Models;
using Newtonsoft.Json;

namespace LxQueryWorker.Model
{
    public class FlashV3
    {
        /*
        {
            "type": 0,
            "timeStamp": "2015-11-21T00:29:01.194422986",
            "longitude": -92.4333727,
            "latitude": 38.5749431,
            "amplitude": -15176.0,
            "height": 0.0,
            "numberSensors": 48,
            "icMultiplicity": 2,
            "cgMultiplicity": 1,
            "startTime": "2015-11-21T00:29:01.164035773",
            "durationSeconds": 0.038544191,
            "minLatitude": 38.571844,
            "minLongitude": -92.440151,
            "maxLatitude": 38.581654,
            "maxLongitude": -92.428273
        }
        */
        public FlashV3()
        {
            
        }

        public FlashV3(Flash src)
        {
            Type = src.Type == FlashType.FlashTypeGlobalCG ? FlashType.FlashTypeCG : src.Type;
            Timestamp = DateTime.Parse(src.TimeStamp);
            Longitude = src.Longitude;
            Latitude = src.Latitude;
            Height = src.Height;
            PeakCurrent = src.Amplitude;
            NumberSensors = src.NumberSensors;
            IcMultiplicity = src.IcMultiplicity;
            CgMultiplicity = src.CgMultiplicity;
            StartTime = DateTime.Parse(src.StartTime);
            EndTime = DateTime.Parse(src.EndTime);
            DurationSeconds = src.DurationSeconds;
            MinLatitude = src.MinLatitude;
            MinLongitude = src.MinLongitude;
            MaxLatitude = src.MaxLatitude;
            MaxLongitude = src.MinLongitude;

            foreach (var portion in src.Portions)
            {
                if (Portions == null) Portions = new List<PortionV3>();
                Portions.Add(new PortionV3(portion));
            }
        }

        [JsonProperty(PropertyName = "type")]
        public FlashType Type { get; set; }

        [JsonProperty(PropertyName = "timeStamp")]
        public DateTime Timestamp { get; set; }

        [JsonProperty(PropertyName = "longitude")]
        public double Longitude { get; set; }

        [JsonProperty(PropertyName = "latitude")]
        public double Latitude { get; set; }

        [JsonProperty(PropertyName = "height")]
        public double? Height { get; set; }

        [JsonProperty(PropertyName = "amplitude")]
        public double? PeakCurrent { get; set; }

        [JsonProperty(PropertyName = "numberSensors")]
        public int? NumberSensors { get; set; }

        [JsonProperty(PropertyName = "icMultiplicity")]
        public int? IcMultiplicity { get; set; }

        [JsonProperty(PropertyName = "cgMultiplicity")]
        public int? CgMultiplicity { get; set; }

        [JsonProperty(PropertyName = "startTime")]
        public DateTime StartTime { get; set; }

        [JsonProperty(PropertyName = "endTime")]
        public DateTime EndTime { get; set; }

        [JsonProperty(PropertyName = "durationSeconds")]
        public decimal DurationSeconds { get; set; }

        [JsonProperty(PropertyName = "minLatitude")]
        public double MinLatitude { get; set; }

        [JsonProperty(PropertyName = "minLongitude")]
        public double MinLongitude { get; set; }

        [JsonProperty(PropertyName = "maxLatitude")]
        public double MaxLatitude { get; set; }

        [JsonProperty(PropertyName = "maxLongitude")]
        public double MaxLongitude { get; set; }

        [JsonProperty(PropertyName = "portions", NullValueHandling = NullValueHandling.Ignore)]
        public List<PortionV3> Portions { get; set; }  
    }
}
