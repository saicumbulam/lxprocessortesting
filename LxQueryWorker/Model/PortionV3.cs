﻿using System;
using LxCommon;
using LxCommon.Models;
using Newtonsoft.Json;

namespace LxQueryWorker.Model
{
    public class PortionV3
    {
        /*
         * {
                "type": 1,
                "timeStamp": "2015-11-21T00:29:01.164035773",
                "latitude": 38.5816544,
                "longitude": -92.428273,
                "amplitude": 11450.0,
                "height": 12048.81,
                "numberSensors": 48, *** Count of stationOffsets
                "errorEllipse": {
                                "maj": 0.251,
                                "min": 0.24,
                                "b": 101.5
                }
            }

        */

        public PortionV3()
        {
            
        }

        public PortionV3(Portion src)
        {
            Type = src.Type == FlashType.FlashTypeGlobalCG ? FlashType.FlashTypeCG : src.Type;
            Timestamp = DateTime.Parse(src.TimeStamp);
            Longitude = src.Longitude;
            Latitude = src.Latitude;
            Height = src.Height;
            Amplitude = src.Amplitude;
            NumberSensors = src.StationOffsets?.Count ?? 0;

            if (src.ErrorEllipse != null)
            {
                ErrorEllipse = new ErrorEllipseV3()
                {
                    MajorAxis = src.ErrorEllipse?.MajorAxis,
                    MinorAxis = src.ErrorEllipse?.MinorAxis,
                    MajorAxisBearing = src.ErrorEllipse?.MajorAxisBearing
                };
            }
        }

        [JsonProperty(PropertyName = "type")]
        public FlashType Type { get; set; }

        [JsonProperty(PropertyName = "timeStamp")]
        public DateTime Timestamp { get; set; }

        [JsonProperty(PropertyName = "longitude")]
        public double Longitude { get; set; }

        [JsonProperty(PropertyName = "latitude")]
        public double Latitude { get; set; }

        [JsonProperty(PropertyName = "height")]
        public double Height { get; set; }

        [JsonProperty(PropertyName = "amplitude")]
        public double Amplitude { get; set; }

        [JsonProperty(PropertyName = "errorEllipse")]
        public ErrorEllipseV3 ErrorEllipse { get; set; }

        [JsonProperty(PropertyName = "numberSensors")]
        public int NumberSensors { get; set; }
    }
}
