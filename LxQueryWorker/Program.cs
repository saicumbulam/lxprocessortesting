﻿using System;
using System.ServiceProcess;
using Aws.Core.Utilities;
using LxQueryWorker.Common;
using LxQueryWorker.Process;

namespace LxQueryWorker
{
    public class Program
    {
        private static Program _program = null;

        private static void Main()
        {
            var servicesToRun = new ServiceBase[] 
            { 
                new WinService() 
            };
            ServiceBase.Run(servicesToRun);
        }

        public static void Start()
        {
            EventManager.Init();

            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += OnUnhandledException;

            _program = new Program();

            _program.Begin();
        }

        public static void Stop()
        {
            _program?.End();

            EventManager.ShutDown();
        }

        private static void OnUnhandledException(object sender, UnhandledExceptionEventArgs args)
        {
            var ex = (Exception)args.ExceptionObject;
            EventManager.LogError(EventId.Program.Global, "Global unhandled exception", ex);
            Stop();
        }

        private readonly QueryListener _queryListener;
        private readonly MonitoringHandler _monitoringHandler;

        public Program()
        {
            _queryListener = new QueryListener(Config.AwsSnsLxQueryNotifierTopic, Config.AwsSqsLxQueryQueue);

            _monitoringHandler = new MonitoringHandler(_queryListener);
        }

        public void Begin()
        {
            _queryListener.Start();
            _monitoringHandler.Start();
        }

        public void End()
        {
            _monitoringHandler.Stop();
            _queryListener.Stop();
        }
    }
}
