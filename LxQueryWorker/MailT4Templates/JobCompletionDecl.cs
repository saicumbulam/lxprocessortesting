﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LxPortalLib.Models;

namespace LxQueryWorker.MailT4Templates
{
    public partial class JobCompletion
    {
        public LxQueryJobData _lxQueryJobData { get; set; }
        public string _emailSubject { get; set; }
        public List<string> _signedUrls { get; set; }
        public long _flashesCount { get; set; }
        public long _portionsCount { get; set; }
    }
}
