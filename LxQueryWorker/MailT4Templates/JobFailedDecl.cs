﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LxPortalLib.Models;

namespace LxQueryWorker.MailT4Templates
{
    public partial class JobFailed
    {
        public LxQueryJobData _lxQueryJobData { get; set; }
        public string _emailSubject { get; set; }
        public bool _isJobFailed { get; set; }
        public string _failureMessage { get; set; }
    }
}
