using System;
using System.Collections.Concurrent;
using System.Net;
using System.Threading;

using LxCommon;
using LxCommon.TcpCommunication;

using LxReceiver.Common;
using LxReceiver.Process;
using LxReceiver.Services;

namespace LxReceiver
{
    public class ProcessController
    {
        private ManualResetEventSlim _stopEvent;

        private IncomingPacketProcessor _incomingPacketProcessor;

        private DbWriterProcessor _dbWriterProcessor;
        private FileWriterProcessor _fileWriterProcessor = null;
        
        private Listener _sensorListener;
        private Listener _receiverListener;
        private WwllnListener _wwllnListener = null;
        private WwllnPacketProcessor _wwllnPacketProcessor = null;
        
        private Sender _wwllnDataForwarder = null;
        private Sender _tlnDataForwarder = null;
        private Sender _wwllnTogaSender = null;

        private Timer _stationUpdateTimer;
        private StationDataTable _stationDataTable;

        private Timer _fileArchiverTimer;
        private FileArchiver _fileArchiver;

        private Timer _sensorStatisticsTimer = null;
        private SensorStatisticsHandler _sensorStatisticsHandler = null;

        private MonitoringHandler _monitoringHandler;

        private bool _isTln;

        private static string _monitorHmtl = string.Empty;

        public ProcessController()
        {
            _stopEvent = new ManualResetEventSlim(false);
            _isTln = (Config.SystemType.ToUpperInvariant() == "TLN");
        }

        public void Start()
        {
            _stationDataTable = new StationDataTable();
            _stationDataTable.InitalLoad();

            InitalizePipeline();

            InitalizeTimers();

            InitalizeCommunication();

            _monitoringHandler = new MonitoringHandler(Config.SystemType, _sensorListener, _receiverListener, _wwllnListener, _wwllnPacketProcessor,
                _wwllnDataForwarder, _tlnDataForwarder, _incomingPacketProcessor, _dbWriterProcessor, _stationDataTable, _sensorStatisticsHandler);
            _monitoringHandler.Start();
        }

        public void Stop()
        {
            // this will signal the pipeline to shutdown
            if (_stopEvent != null)
                _stopEvent.Set();
            
            CleanupTimers();

            CleanupCommunication();

            _monitoringHandler.Stop();
        }

        private void InitalizePipeline()
        {
            _dbWriterProcessor = new DbWriterProcessor(Config.EnableGpsDb, Config.EnableLogDb, _stopEvent, Config.MaxDbArchiveProcessorQueueTasks);

            if (Config.EnableEnTlnArchive)
            {
                _fileWriterProcessor = new FileWriterProcessor(Config.EnTlnArchiveDirectory, Config.EnTlnArchiveBaseFileName, Config.EnTlnArchiveMinutesPerFile, _stopEvent);
            }

            _incomingPacketProcessor = new IncomingPacketProcessor(_dbWriterProcessor, _fileWriterProcessor, Config.IsPlayback, _stopEvent, Config.MaxIncomingPacketProcessorQueueTasks);

            _dbWriterProcessor.SensorPacketVersion = _incomingPacketProcessor.SensorPacketVersion;
        }

        private void InitalizeTimers()
        {
            _stationUpdateTimer = new Timer(_stationDataTable.Update, null, TimeSpan.FromMinutes(Config.StationDatabaseUpdateIntervalMinutes),
                TimeSpan.FromMinutes(Config.StationDatabaseUpdateIntervalMinutes));

            if (Config.ArchiverAction != ArchiverAction.None)
            {
                _fileArchiver = new FileArchiver(Config.EnTlnArchiveDirectory, Config.EnTlnArchiveBaseFileName, Config.ArchiveBasePath, Config.ArchivePathDateFormatString, 
                    Config.ArchiverAction, Config.ArchiverIntervalMinutes, _stopEvent);
                _fileArchiverTimer = new Timer(_fileArchiver.Start, null, TimeSpan.FromMinutes(1), TimeSpan.FromMilliseconds(-1));
                _fileArchiver.Timer = _fileArchiverTimer;
            }

            if (Config.EnableSensorStatistics)
            {
                _sensorStatisticsHandler = new SensorStatisticsHandler(Config.SensorStatisticsSampleSizeMinutes, Config.SensorStatisticsWriteIntervalMinutes, Config.SensorStatisiticsSaveMaxDegreeOfParallelism);
                _sensorStatisticsTimer = new Timer(_sensorStatisticsHandler.Save, null, TimeSpan.FromMinutes(Config.SensorStatisticsWriteIntervalMinutes), 
                    TimeSpan.FromMinutes(Config.SensorStatisticsWriteIntervalMinutes));

                _incomingPacketProcessor.SensorStatisticsHandler = _sensorStatisticsHandler;
            }
        }

        private void CleanupTimers()
        {
            if (_stationUpdateTimer != null)
                _stationUpdateTimer.Dispose();

            if (_fileArchiverTimer != null)
                _fileArchiverTimer.Dispose();

            if (_sensorStatisticsTimer != null)
                _sensorStatisticsTimer.Dispose();
        }

        private void InitalizeCommunication()
        {
            _sensorListener = new Listener("Sensor Listener", IPAddress.Any, Config.SensorListenPort, Config.SensorListenerMaxPendingConnections,
                _incomingPacketProcessor.DecodeSensorPacket, Config.SensorListenerRateSampleSize, Config.ConnectionRateSampleSize, Config.ListenerIdleCheckIntervalSeconds, null);
            _sensorListener.Open();

            _receiverListener = new Listener("Receiver Listener", IPAddress.Any, Config.ReceiverListenPort, Config.ReceiverListenerMaxPendingConnections,
                _incomingPacketProcessor.DecodePacket, Config.ReceiverListenerRateSampleSize, Config.ConnectionRateSampleSize, Config.ListenerIdleCheckIntervalSeconds, 
                Config.MonitorIncomingReceiverActiveConnectionsMinimum);
            _receiverListener.Open();

            ConcurrentDictionary<IPEndPoint, ISendWorker> tlnDataForward = Config.LoadTlnDataForwardConfig(_stationDataTable.Data);
            if (tlnDataForward != null && tlnDataForward.Count > 0)
            {
                _tlnDataForwarder = new Sender("TLN Data Forwarder", tlnDataForward);
                _incomingPacketProcessor.TlnDataForwarder = _tlnDataForwarder;
            }

            if (Config.WwllnTogaSender != null && Config.WwllnTogaSender.Count > 0)
            {
                _wwllnTogaSender = new Sender("WWLLN Toga Sender", Config.WwllnTogaSender);
                _incomingPacketProcessor.WwllnTogaSender = _wwllnTogaSender;
            }

            if (Config.EnableWwlln)
            {
                if (Config.WwllnDataForward != null && Config.WwllnDataForward.Count > 0)
                {
                    _wwllnDataForwarder = new Sender("WWLLN Data Forwarder", Config.WwllnDataForward);

                    _wwllnPacketProcessor = new WwllnPacketProcessor(_wwllnDataForwarder, _stationDataTable.Data, _stopEvent, Config.MaxWwllnPacketProcessorQueueTasks);

                    _incomingPacketProcessor.WwllnPacketProcessor = _wwllnPacketProcessor;

                    _wwllnListener = new WwllnListener(IPAddress.Any, Config.WwllnListenPort, _wwllnPacketProcessor.AddPacket, _stopEvent);
                    _wwllnListener.Open();
                }
            }
        }


        private void CleanupCommunication()
        {
            _sensorListener.Close();

            _receiverListener.Close();

            if (_tlnDataForwarder != null)
            {
                _tlnDataForwarder.Close();
            }

            if (_wwllnTogaSender != null)
            {
                _wwllnTogaSender.Close();
            }

            if (_wwllnListener != null)
            {
                _wwllnListener.Close();
            }

            if (_wwllnDataForwarder != null)
            {
                _wwllnDataForwarder.Close();
            }
        }
    }
}
