﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net;
using Aws.Core.Utilities;
using LxCommon;
using LxCommon.Monitoring;
using LxCommon.TcpCommunication;
using LxReceiver.Common;

namespace LxReceiver.Models
{
    public class TlnSendWorker : SendWorker
    {
        private HashSet<PacketType> _validMessagesTypes;
        private HashSet<string> _blackListIps;
        private HashSet<string> _validNetworks;
        private ConcurrentDictionary<string, StationData> _stationData;
        private bool _isPlayback;

        private const ushort EventManagerOffset = TaskMonitor.TlnSendWorker;

        public TlnSendWorker(HashSet<PacketType> validMessagesTypes, HashSet<string> validNetworks, HashSet<string> blackListIps, IPEndPoint remoteEndPoint, 
            ConcurrentDictionary<string, StationData> stationData, bool isPlayback)
            : base(remoteEndPoint)
        {
            _validMessagesTypes = validMessagesTypes;
            _validNetworks = validNetworks;
            _blackListIps = blackListIps;
            _stationData = stationData;
            _isPlayback = isPlayback;
        }

        public override void Add(PacketHeaderInfo phi)
        {
            if (phi != null)
            {
                if (!_blackListIps.Contains(phi.RemoteIP) && (_validMessagesTypes.Count == 0 || _validMessagesTypes.Contains(phi.MsgType)))
                {
                    if (_validNetworks.Count == 0)
                    {
                        base.Add(phi);
                    }
                    else if (!string.IsNullOrWhiteSpace(phi.SensorId))
                    {
                        StationData sd;
                        if (_stationData.TryGetValue(phi.SensorId, out sd))
                        {
                            if (sd.Networks != null)
                            {
                                foreach (string network in sd.Networks)
                                {
                                    if (_validNetworks.Contains(network))
                                    {
                                        base.Add(phi);
                                        break;
                                    }
                                }
                            }                            
                        }
                    }
                }
            }
        }

        protected override bool IsValidPacketToSend(byte[] packet)
        {
            bool isValid = true;
            
            PacketHeaderInfo phi = new PacketHeaderInfo();
            if (RawPacketUtil.GetPacketHeaderInfo(packet, phi))
            {
                if (!_isPlayback && phi.MsgType == PacketType.MsgTypeLtg)
                {
                    DateTime packetTime = LtgTimeUtils.GetUtcDateTimeFrom1970Seconds(phi.PacketTimeInSeconds);
                    TimeSpan ts = DateTime.UtcNow - packetTime;
                    if (Math.Abs(ts.TotalSeconds) > Config.MaxPacketArriveTimeTooEarlyOrTooLateInSeconds)
                    {
                        isValid = false;
                        EventManager.LogInfo(EventManagerOffset + 1, string.Format("Discarding {0} packet with bad time, packet age: {1} secs", phi.MsgType, ts.TotalSeconds));
                    }
                }
            }

            return isValid;
        }
    }
}
