﻿using System;
using System.Collections.Concurrent;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

using Aws.Core.Utilities;

using LxCommon;
using LxCommon.Monitoring;
using LxCommon.TcpCommunication;

using LxReceiver.Common;

namespace LxReceiver.Models
{
    public class WwllnTogaSendWorker : ISendWorker
    {
        private const ushort EventManagerOffset = TaskMonitor.WwllnTogaSenderWorker;
        
        private readonly int _maxAgeInSeconds;
        private UdpClient _udpClient;
        private ConcurrentQueue<PacketHeaderInfo> _sendQueue;
        private int _processLock;
        private ManualResetEventSlim _stopEvent;
        private IPEndPoint _remoteEndPoint;
        private RateCalculator _rateCalculator;
        private const int RateCalculatorSampleSize = 5;

        private static readonly int MaxQueueSize = Config.MaxWwllnTogaSendWorkerQueueSize;

        public WwllnTogaSendWorker(int maxAgeInSeconds, IPEndPoint remoteEndPoint)
        {
            _maxAgeInSeconds = maxAgeInSeconds;
            _remoteEndPoint = remoteEndPoint;
            _rateCalculator = new RateCalculator(RateCalculatorSampleSize);
            _sendQueue = new ConcurrentQueue<PacketHeaderInfo>();
        }

        public void Add(PacketHeaderInfo phi)
        {
            if (phi != null && phi.MsgType == PacketType.MsgTypeLtg)
            {
                if (IsValidPacket(_maxAgeInSeconds, DateTime.UtcNow, phi.PacketTimeInSeconds))
                {
                    if (_sendQueue.Count > MaxQueueSize)
                    {
                        EventManager.LogWarning(EventManagerOffset, "WwllnTogaSenderWorker queue greater than max size, dumping packets from queue");
                        PacketHeaderInfo trash;
                        while (_sendQueue.Count > MaxQueueSize)
                        {   
                            _sendQueue.TryDequeue(out trash);
                        }
                    }

                    _sendQueue.Enqueue(phi);

                    // This controls the task that manages the queue, make sure we only launch one task 
                    if (0 == Interlocked.CompareExchange(ref _processLock, 1, 0))
                    {
                        ProcessSendQueue();
                    }
                }
            }
        }

        private bool IsValidPacket(int maxAgeInSeconds, DateTime utcNow, int packetTimeInSeconds)
        {
            int t = LtgTimeUtils.GetSecondsSince1970FromDateTime(utcNow);
            return (t - packetTimeInSeconds <= maxAgeInSeconds);
        }
   
        private async void ProcessSendQueue()
        {
            PacketHeaderInfo phi;
            try
            {
                while (_sendQueue.TryDequeue(out phi))
                {
                    // the conversion is expensive so check the time again to make sure we need to convert
                    if (IsValidPacket(_maxAgeInSeconds, DateTime.UtcNow, phi.PacketTimeInSeconds))
                    {
                        byte[] waveform = RawPacketUtil.ConvertRawToWaveformBuffer(phi);
                        if (waveform != null && waveform.Length > 0)
                        {
                            await SendPacket(waveform);
                        }
                    }

                    if (_stopEvent.Wait(0))
                        break;
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventManagerOffset + 1, "Error processing send queue", ex);
            }
            finally
            {
                Interlocked.Decrement(ref _processLock);
            }
        }

        protected async Task SendPacket(byte[] packet)
        {
            if (_udpClient == null)
            {
                _udpClient = new UdpClient();
            }

            if (_udpClient != null)
            {
                try
                {
                    await _udpClient.SendAsync(packet, packet.Length, _remoteEndPoint);
                    _rateCalculator.AddSample(DateTime.UtcNow);
                }
                catch (SocketException socketEx)
                {
                    EventManager.LogError(EventManagerOffset + 2, string.Format("Error sending udp to {0}:{1}; Socket Error Code: {2}",
                        _remoteEndPoint.Address, _remoteEndPoint.Port, socketEx.ErrorCode), socketEx);

                    _udpClient = null;
                }
                catch (Exception ex)
                {
                    EventManager.LogError(EventManagerOffset + 3, string.Format("Error sending udp to {0}:{1}",
                        _remoteEndPoint.Address, _remoteEndPoint.Port), ex);

                    _udpClient = null;
                }
            }
        }

        public double GetOutgoingRatePerSecond(DateTime now)
        {
            return _rateCalculator.GetRatePerSecond(now);
        }

        public ManualResetEventSlim StopEvent
        {
            set { _stopEvent = value; }
        }

        #region ISendWorker Members

        /// <summary>
        /// Do not use, only here so we can implement the interface
        /// </summary>
        /// <param name="packet"></param>
        public void Add(byte[] packet)
        {
            
        }

        public void Close()
        {
            if (_udpClient != null)
            {
                try
                {
                    _udpClient.Close();
                }
                catch (SocketException socketEx)
                {
                    EventManager.LogError(EventManagerOffset + 4, string.Format("Error closing udp client to {0}:{1}; Socket Error Code: {2}",
                        _remoteEndPoint.Address, _remoteEndPoint.Port, socketEx.ErrorCode), socketEx);

                    _udpClient = null;
                }
            }
        }

        public bool IsActive
        {
            get { return (_udpClient != null); }
        }

        #endregion
    }
}
