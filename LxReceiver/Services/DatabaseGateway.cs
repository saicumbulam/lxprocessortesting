﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;

using Aws.Core.Utilities;

using LxCommon;
using LxCommon.Monitoring;

using LxReceiver.Process;

namespace LxReceiver.Services
{
    public class DatabaseGateway
    {
        private readonly string _connectionString;
        

        public DatabaseGateway(string connectionString)
        {
            _connectionString = connectionString;
        }

        public bool TryInsertGpsPacket(PacketHeaderInfo phi, string version)
        {
            bool success = false;
            GpsData gps = phi.OtherObject as GpsData;
            if (gps != null)
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("LtgUpdateGPSData_pr", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@SensorID", gps.SensorId);
                        cmd.Parameters.AddWithValue("@Version", gps.SensorVersion);
                        cmd.Parameters.AddWithValue("@DataVersion", version);
                        cmd.Parameters.AddWithValue("@Latitude", gps.Latitude);
                        cmd.Parameters.AddWithValue("@Longitude", gps.Longtitude);
                        cmd.Parameters.AddWithValue("@Height", gps.Height);
                        cmd.Parameters.AddWithValue("@RawData", phi.RawData);
                        cmd.Parameters.AddWithValue("@IPAddress", phi.RemoteIP);
                        cmd.Parameters.AddWithValue("@MACAddress", phi.RemoteMAC);

                        try
                        {
                            conn.Open();
                            cmd.ExecuteNonQuery();
                            success = true;
                        }
                        catch (Exception ex)
                        {
                            EventManager.LogError(TaskMonitor.TidUpdateGpsDbFail, "Error inserting gps data", ex);
                        }
                        finally
                        {
                            conn.Close();
                        }
                    }
                }
            }

            return success;
        }

        private const int MinRawDataLengthBytes = 16;
        public bool TryInsertLogPacket(PacketHeaderInfo phi)
        {
            bool success = false;
            if (phi.RawData != null && phi.RawData.Length > MinRawDataLengthBytes)
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("LtgUpdateLogDataHistory_pr", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@P_SensorID", phi.SensorId);
                        cmd.Parameters.AddWithValue("@P_LogText", Encoding.ASCII.GetString(phi.RawData, MinRawDataLengthBytes, phi.RawData.Length - MinRawDataLengthBytes));

                        try
                        {
                            conn.Open();
                            cmd.ExecuteNonQuery();
                            success = true;
                        }
                        catch (Exception ex)
                        {
                            EventManager.LogError(TaskMonitor.TidUpdateLogDbFail, "Error inserting log data", ex);
                        }
                        finally
                        {
                            conn.Close();
                        }
                    }
                }
            }

            return success;
        }

        public bool TryInsertSensorStatistic(string sensorId, SensorStatisticsHandler.Datum datum, DateTime timeStamp)
        {
            bool success = false;
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("LtgUpdateSensorStatistics_pr", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TimeStamp", timeStamp);
                    cmd.Parameters.AddWithValue("@SensorID", sensorId);
                    cmd.Parameters.AddWithValue("@TotalSeconds", datum.TotalSeconds);
                    cmd.Parameters.AddWithValue("@MissingPackets", datum.MissingPackets);
                    cmd.Parameters.AddWithValue("@DuplicatePackets", datum.DuplicatePackets);
                    cmd.Parameters.AddWithValue("@InvalidPackets", datum.InvalidPackets);
                    cmd.Parameters.AddWithValue("@WrongOrderPackets", datum.WrongOrderPackets);
                    cmd.Parameters.AddWithValue("@TotalReceivedPackets", datum.TotalReceivedPackets);
                    cmd.Parameters.AddWithValue("@MinPacketSize", datum.MinPacketSize);
                    cmd.Parameters.AddWithValue("@MaxPacketSize", datum.MaxPacketSize);
                    cmd.Parameters.AddWithValue("@TotalBytes", datum.TotalBytes);
                    cmd.Parameters.AddWithValue("@EmptyPackets", datum.EmptyPackets);
                    cmd.Parameters.AddWithValue("@MinPacketLatency", datum.MinPacketLatency);
                    cmd.Parameters.AddWithValue("@MaxPacketLatency", datum.MaxPacketLatency);
                    cmd.Parameters.AddWithValue("@MedianPacketLatency", datum.MedianPacketLatency);
                    cmd.Parameters.AddWithValue("@MinLFThreshold", datum.MinLFThreshold);
                    cmd.Parameters.AddWithValue("@MaxLFThreshold", datum.MaxLFThreshold);
                    cmd.Parameters.AddWithValue("@MedianLFThreshold", datum.MedianLFThreshold);
                    cmd.Parameters.AddWithValue("@MinHFThreshold", datum.MinHFThreshold);
                    cmd.Parameters.AddWithValue("@MaxHFThreshold", datum.MaxHFThreshold);
                    cmd.Parameters.AddWithValue("@MedianHFThreshold", datum.MedianHFThreshold);

                    try
                    {
                        conn.Open();
                        cmd.ExecuteNonQuery();
                        success = true;
                    }
                    catch (Exception ex)
                    {
                        EventManager.LogError(TaskMonitor.TidUpdateStatsDbFail, "Error inserting sensor statistics data", ex);
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }

            return success;
        }
    }
}
