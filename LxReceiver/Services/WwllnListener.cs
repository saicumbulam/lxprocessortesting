﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

using Aws.Core.Utilities;

using LxCommon;
using LxCommon.TcpCommunication;

using LxReceiver.Common;

namespace LxReceiver.Services
{
    public class WwllnListener
    {
        private const ushort EventManagerOffset = 100;
        private UdpClient _udpClient;
        private Func<PacketHeaderInfo, bool> _addPacket;
        private IPEndPoint _endPoint;
        private ManualResetEventSlim _stopEvent;
        private RateCalculator _rateCalculator;

        public WwllnListener(IPAddress ipAdd, int port, Func<PacketHeaderInfo, bool> addPacket, ManualResetEventSlim stopEvent)
        {
            _endPoint = new IPEndPoint(ipAdd, port);
            _addPacket = addPacket;
            _stopEvent = stopEvent;
            _rateCalculator = new RateCalculator(Config.WwllnListenerRateSampleSize);

            try
            {
                _udpClient = new UdpClient(_endPoint);
                EventManager.LogInfo(EventManagerOffset + 0, string.Format("Created WWLLN udp client at {0}:{1}", _endPoint.Address, _endPoint.Port));
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventManagerOffset + 1, "Unable to create UdpClient", ex);
            }
        }

        public void Open()
        {
            IPEndPoint endPoint = _udpClient.Client.LocalEndPoint as IPEndPoint;
            
            if (endPoint != null)
                EventManager.LogInfo(EventManagerOffset + 2, string.Format("WWLLN UDP client started listening on {0}:{1}", endPoint.Address, endPoint.Port));

            Listen();
        }

        private async void Listen()
        {
            while (!_stopEvent.Wait(0))
            {
                try
                {
                    if (_udpClient == null)
                    {
                        _udpClient = new UdpClient(_endPoint);
                    }
                    
                    UdpReceiveResult udpReceiveResult = await _udpClient.ReceiveAsync();
                    ProcessResult(udpReceiveResult);
                }
                catch (SocketException socketEx)
                {
                    EventManager.LogError(EventManagerOffset + 3, string.Format("Socket Error in ReceiveAsync; Socket Error Code: {0}", socketEx.ErrorCode), socketEx);
                    Close();
                }
                catch (Exception ex)
                {
                    EventManager.LogError(EventManagerOffset + 4, "Error in ReceiveAsync", ex);
                    Close();
                }
            }
        }

        private IPEndPoint _primaryEndPoint = null;
        private DateTime _lastPrimaryReceived = DateTime.UtcNow;
        private static readonly int PrimaryTimeoutSeconds = Config.WwllnSourceTimeoutSeconds;

        private void ProcessResult(UdpReceiveResult udpReceiveResult)
        {
            if (udpReceiveResult.Buffer != null && udpReceiveResult.Buffer.Length > 0)
            {
                string s = Encoding.ASCII.GetString(udpReceiveResult.Buffer);
                PacketHeaderInfo phi = new PacketHeaderInfo();
                if (RawPacketUtil.GetWWLLNFlashData(s, phi))
                {
                    if (udpReceiveResult.RemoteEndPoint != null)
                    {
                        if (_primaryEndPoint == null || (DateTime.UtcNow - _lastPrimaryReceived).TotalSeconds > PrimaryTimeoutSeconds)
                        {
                            _primaryEndPoint = udpReceiveResult.RemoteEndPoint;
                        }

                        if (Equals(_primaryEndPoint, udpReceiveResult.RemoteEndPoint))
                        {
                            phi.RemoteIP = udpReceiveResult.RemoteEndPoint.Address.ToString();
                            _lastPrimaryReceived = DateTime.UtcNow;
                            _addPacket(phi);
                            _rateCalculator.AddSample(DateTime.UtcNow);
                        }
                    }
                }
            }
        }

        public void Close()
        {
            if (_udpClient != null)
            {
                try
                {
                    _udpClient.Close();
                }
                catch (SocketException socketEx)
                {
                    EventManager.LogError(EventManagerOffset + 5, string.Format("Socket Error in Close; Socket Error Code: {0}", socketEx.ErrorCode), socketEx);
                }
                catch (Exception ex)
                {
                    EventManager.LogError(EventManagerOffset + 6, "Error in Close", ex);
                }

                _udpClient = null;
            }
        }

        public double GetIncomingRatePerSecond(DateTime now)
        {
            return _rateCalculator.GetRatePerSecond(now);
        }

    }
}
