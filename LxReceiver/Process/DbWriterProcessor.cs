﻿using System;
using System.Collections.Concurrent;
using System.Configuration;
using System.Threading;

using LxCommon;
using LxCommon.DataStructures;

using LxReceiver.Services;

namespace LxReceiver.Process
{
    public class DbWriterProcessor : ParallelQueueProcessor<PacketHeaderInfo>
    {
        private bool _enableGpsDb;
        private bool _enableLogDb;
        private DatabaseGateway _dbGateway;
        private ConcurrentDictionary<string, string> _sensorPacketVersion = null;
        private DateTime _lastGpsDbSave;
        private DateTime _lastLogDbSave;

        public DbWriterProcessor(bool enableGpsDb, bool enableLogDb, ManualResetEventSlim stopEvent, int maxActiveWorkItems) 
            : base(stopEvent, maxActiveWorkItems)
        {
            _enableGpsDb = enableGpsDb;
            _enableLogDb = enableLogDb;
            _lastGpsDbSave = DateTime.UtcNow;
            _lastLogDbSave = DateTime.UtcNow;

            if (_enableGpsDb || _enableLogDb)
            {
                _dbGateway = new DatabaseGateway(ConfigurationManager.ConnectionStrings["Lightning"].ConnectionString);
            }
        }

        protected override void DoWork(PacketHeaderInfo phi)
        {
            if (phi != null)
            {
                if (_enableGpsDb && phi.MsgType == PacketType.MsgTypeGps)
                {
                    string ver = string.Empty;
                    if (_sensorPacketVersion != null)
                    {
                        if (!_sensorPacketVersion.TryGetValue(phi.SensorId, out ver))
                        {
                            ver = string.Empty;
                        }
                    }

                    if (_dbGateway.TryInsertGpsPacket(phi, ver))
                    {
                        _lastGpsDbSave = DateTime.UtcNow;
                    }
                }
                else if (_enableLogDb && phi.MsgType == PacketType.MsgTypeLog)
                {
                    if (_dbGateway.TryInsertLogPacket(phi))
                    {
                        _lastLogDbSave = DateTime.UtcNow;
                    }
                }
            }
        }

        public ConcurrentDictionary<string, string> SensorPacketVersion
        {
            set { _sensorPacketVersion = value; }
        }

        public DateTime LastGpsDbSave
        {
            get { return _lastGpsDbSave; }
        }

        public DateTime LastLogDbSave
        {
            get { return _lastLogDbSave; }
        }
    }
}
