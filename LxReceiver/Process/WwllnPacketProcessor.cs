﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;

using LxCommon;
using LxCommon.DataStructures;
using LxCommon.Models;
using LxCommon.TcpCommunication;

using LxReceiver.Common;

namespace LxReceiver.Process
{
    public class WwllnPacketProcessor : ParallelQueueProcessor<PacketHeaderInfo>
    {
        private Sender _wwllnDataForwarder;
        private ConcurrentDictionary<string, ConcurrentSortedSet<PacketHeaderInfo>> _tlnLightningPackets;
        ConcurrentDictionary<string, StationData> _stationData;

        public WwllnPacketProcessor(Sender wwllnDataForwarder, ConcurrentDictionary<string, StationData> stationData, ManualResetEventSlim stopEvent, int maxActiveWorkItems) : base(stopEvent, maxActiveWorkItems)
        {
            _wwllnDataForwarder = wwllnDataForwarder;
            _stationData = stationData;
            _tlnLightningPackets = new ConcurrentDictionary<string, ConcurrentSortedSet<PacketHeaderInfo>>();
            
        }

        public bool AddPacket(PacketHeaderInfo phi)
        {
            bool success = false;
            if (phi != null)
            {
                Add(phi);
                success = true;
            }
            return success;
        }

        private static readonly int NumberOfSecondsTlnDataForWwllnAmplitude = Config.NumberOfSecondsTlnDataForWwllnAmplitude;
        public void AddTlnLtgPacket(PacketHeaderInfo phi)
        {
            if (phi != null && !string.IsNullOrWhiteSpace(phi.SensorId))
            {
                ConcurrentSortedSet<PacketHeaderInfo> packets = _tlnLightningPackets.GetOrAdd(phi.SensorId, new ConcurrentSortedSet<PacketHeaderInfo>(new PacketTimeInSecondsComparer()));
                packets.TryAdd(phi);

                int tooOldTimeStamp = LtgTimeUtils.GetSecondsSince1970FromDateTime(DateTime.UtcNow.AddSeconds(-NumberOfSecondsTlnDataForWwllnAmplitude));

                packets.RemoveWhere((p) => (p.PacketTimeInSeconds < tooOldTimeStamp));
            }
        }

        protected override void DoWork(PacketHeaderInfo phi)
        {
            if (phi != null && phi.RawData != null)
            {
                LTGFlash flash = RawPacketUtil.DecodeFlashPacket(phi.RawData);
                if (flash != null)
                {
                    flash = UpdateAmplitude(flash);

                    phi = RawPacketUtil.EncodeFlashToPacket(flash);
                    byte[] packet = RawPacketUtil.GetStreamingBuffer(phi);
                    _wwllnDataForwarder.AddPacket(packet);
                }
            }
        }

        private static readonly int MaxDistanceForTlnOffsetFromWwllnFlashMeters = Config.MaxDistanceForTlnOffsetFromWwllnFlashMeters;
        private LTGFlash UpdateAmplitude(LTGFlash flash)
        {
            List<Offset> offsets = FindStationOffsets(flash);
            if (offsets != null && offsets.Count > 0)
            {
                float peakCurrent = FindPeakCurrent(offsets, flash);
                foreach (LTGFlashPortion portion in flash.FlashPortionList)
                {
                    portion.Amplitude = peakCurrent;
                }
            }

            return flash;
        }

        private List<Offset> FindStationOffsets(LTGFlash flash)
        {
            List<Offset> offsets = null;
            if (_stationData != null)
            {
                offsets = new List<Offset>();
                foreach (KeyValuePair<string, StationData> kvp in _stationData)
                {
                    if (kvp.Value.QcFlag == 100 && kvp.Value.CalibrationData.CalibrationInfoLoaded)
                    {
                        StationDecibelParameter dbParameter = kvp.Value.CalibrationData.GetActiveCalibrationData();
                        if (dbParameter != null && dbParameter.Valid)
                        {
                            
                            ConcurrentSortedSet<PacketHeaderInfo> packets;
                            if (_tlnLightningPackets.TryGetValue(kvp.Key, out packets))
                            {
                                if (packets.Count > 0)
                                {
                                    
                                    // meter
                                    double dist = StationData.DistanceWithCurvatureInMicrosecond(flash.Latitude, flash.Longitude, kvp.Value.Latitude, kvp.Value.Longitude);

                                    // Can't determine the peak current for distance more than 30000 KM  (light speed 0.3km/microsecond)
                                    if (dist <= MaxDistanceForTlnOffsetFromWwllnFlashMeters)
                                    {
                                        Offset offset = new Offset
                                        {
                                            StationId = kvp.Value.StationID,
                                            DistanceToFlash = dist
                                        };
                                        offsets.Add(offset);
                                    }
                                }
                            }
                        }
                        
                    }
                }
            }

            return offsets;
        }

        //private const double SomeTimeConstant = 1e-6;
        private const int MaxDistanceFlashPolarityKm = 6666;
        private const int MinPeakCurrentsNeeded = 6;
        private float FindPeakCurrent(List<Offset> offsets, LTGFlash flash)
        {
            float peakCurrent = 0;
            offsets.Sort((x, y) => { return x.DistanceToFlash.CompareTo(y.DistanceToFlash); });
            List<float> currents = new List<float>();
            foreach (Offset offset in offsets)
            {
                //double toaSeconds = LtgTimeUtils.GetSecondsSince1970FromTimeString(flash.FlashTime) + (offset.distanceToFlash / DateTimeUtcNano.MicrosecondsPerSecond);

                long toaNanoseconds = flash.TimeStamp.Nanoseconds + (long)(offset.DistanceToFlash * DateTimeUtcNano.NanosecondsPerMicrosecond);

                DateTimeUtcNano toa = new DateTimeUtcNano(toaNanoseconds);

                double? peakVoltage = GetPeakVoltage(offset, toa);
                if (peakVoltage.HasValue)
                {
                    // Can't know the polarity due to the long distance, change it to negative
                    if (offset.DistanceToFlash > MaxDistanceFlashPolarityKm && peakVoltage.Value > 0)
                    {
                        peakVoltage = peakVoltage.Value * -1;
                    }

                    StationData sd;
                    if (_stationData.TryGetValue(offset.StationId, out sd))
                    {
                        StationDecibelParameter dbParameter = sd.CalibrationData.GetActiveCalibrationData();
                        if (dbParameter != null && dbParameter.Valid)
                        {
                            float pc = LTGFlashPortion.CalculateFlashPortionCurrent(peakVoltage.Value, offset.DistanceToFlash * LtgConstants.LightspeedInMetersPerMicrosecond, dbParameter);
                            currents.Add(pc);
                            if (currents.Count >= MinPeakCurrentsNeeded)
                            {
                                break;
                            }
                        }
                    }
                }
            }

            if (currents.Count > 0)
            {
                currents.Sort();
                peakCurrent = currents[currents.Count / 2];
            }

            return peakCurrent;
        }

        private const int TimeDeltaSeconds = 1;
        private double? GetPeakVoltage(Offset offset, DateTimeUtcNano toa)
        {
            double? volt = null;

            ConcurrentSortedSet<PacketHeaderInfo> packets;
            if (_tlnLightningPackets.TryGetValue(offset.StationId, out packets) && packets.Count > 0)
            {
                int toaSeconds = LtgTimeUtils.GetSecondsSince1970FromDateTime(toa.BaseTime);

                PacketHeaderInfo lower = new PacketHeaderInfo() { PacketTimeInSeconds = (toaSeconds - TimeDeltaSeconds) };
                PacketHeaderInfo upper = new PacketHeaderInfo() { PacketTimeInSeconds = (toaSeconds + TimeDeltaSeconds) };
                SortedSet<PacketHeaderInfo> between = packets.GetBetween(lower, upper);
                if (between != null && between.Count > 0)
                {
                    List<Waveform> waveforms = new List<Waveform>();
                    PulseData pulseData = new PulseData();
                    List<Waveform> localWaveforms = new List<Waveform>();
                    
                    foreach (PacketHeaderInfo packet in between)
                    {
                        if (pulseData.Initalize(packet))
                        {
                            StationData sd;
                            if (_stationData.TryGetValue(offset.StationId, out sd))
                            {
                                sd.CalibrationData.UpdateActiveCalirationNum(packet.SensorAttenuation);
                            }

                            pulseData.ConvertToWaveforms(localWaveforms);
                            if (localWaveforms.Count > 0)
                            {
                                waveforms.AddRange(localWaveforms);
                            }
                        }
                        pulseData.CleanForReuse();
                        localWaveforms.Clear();
                    }

                    if (waveforms.Count > 0)
                    {
                        volt = FindPeakVolt(waveforms, toa);
                    }
                }
            }

            return volt;
        }

        private const long ToaDeltaNanoseconds = 2000000;
        protected double? FindPeakVolt(List<Waveform> waveforms, DateTimeUtcNano toa)
        {
            double? volt = null;

            long minToaNanoseconds = toa.Nanoseconds - ToaDeltaNanoseconds;
            long maxToaNanoseconds = toa.Nanoseconds + ToaDeltaNanoseconds;


            Waveform peakWaveform = default(Waveform);
            foreach (Waveform waveform in waveforms)
            {
                if (waveform.TimeStamp.Nanoseconds > minToaNanoseconds && waveform.TimeStamp.Nanoseconds < maxToaNanoseconds)
                {
                    if (peakWaveform == default(Waveform) || Math.Abs(peakWaveform.Amplitude) < Math.Abs(waveform.Amplitude))
                    {
                        peakWaveform = waveform;
                    }
                }
            }
            
            if (peakWaveform != default(Waveform))
            {
                volt =  peakWaveform.Amplitude;
            }

            return volt;
        }

        private class PacketTimeInSecondsComparer : Comparer<PacketHeaderInfo>
        {
            public override int Compare(PacketHeaderInfo x, PacketHeaderInfo y)
            {
                int compare = 0;
                if (x != null && y != null)
                {
                    compare =  x.PacketTimeInSeconds.CompareTo(y.PacketTimeInSeconds);
                }
                else if (x == null && y == null)
                {
                    compare = 0;
                }
                else if (x == null)
                {
                    compare = -1;
                }
                else if (y == null)
                {
                    compare = 1;
                }

                return compare;
            }
        }
    }
}
