﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using Aws.Core.Utilities;
using LxCommon;
using LxCommon.DataStructures;
using LxCommon.Monitoring;
using LxCommon.TcpCommunication;

namespace LxReceiver.Process
{
    public class IncomingPacketProcessor : ParallelQueueProcessor<PacketHeaderInfo>
    {
        private Sender _tlnDataForwarder;
        private WwllnPacketProcessor _wwllnPacketProcessor;
        private DbWriterProcessor _dbWriterProcessor;
        private FileWriterProcessor _fileWriterProcessor;
        private SensorStatisticsHandler _sensorStatisticsHandler;
        private ConcurrentDictionary<string, string> _sensorPacketVersion;
        private bool _isPlayback;
        private Sender _wwllnTogaSender;
        private DateTime _lastEnqueued;
        private DateTime _lastDequeued;
        private ConcurrentDictionary<string, DateTime> _sensorLastPacketTime;

        private const ushort EventManagerOffset = TaskMonitor.IncomingPacketProcessor;

        public IncomingPacketProcessor(DbWriterProcessor dbWriterProcessor, FileWriterProcessor fileWriterProcessor, bool isPlayback, ManualResetEventSlim stopEvent, int maxActiveWorkItems) 
            : base (stopEvent, maxActiveWorkItems)
        {
            _dbWriterProcessor = dbWriterProcessor;
            _fileWriterProcessor = fileWriterProcessor;
            _sensorPacketVersion = new ConcurrentDictionary<string, string>();
            _isPlayback = isPlayback;
            _lastEnqueued = DateTime.UtcNow;
            _lastDequeued = DateTime.UtcNow;
            _sensorLastPacketTime = new ConcurrentDictionary<string, DateTime>();
        }

        protected override void DoWork(PacketHeaderInfo phi)
        {
            if (phi != null)
            {
                if (_tlnDataForwarder != null)
                {
                    _tlnDataForwarder.AddPacket(phi);
                }

                if (phi.MsgType == PacketType.MsgTypeLog)
                {
                    EventManager.LogInfo(EventManagerOffset, "Received log packet");
                    _dbWriterProcessor.Add(phi);
                }

                if (_sensorStatisticsHandler != null)
                {
                    _sensorStatisticsHandler.Update(phi);
                }

                if (!string.IsNullOrWhiteSpace(phi.SensorId))
                {
                    DateTime dt = TimeSvc.GetTime(phi.PacketTimeInSeconds);
                    dt = DateTime.SpecifyKind(dt, DateTimeKind.Utc);
                    _sensorLastPacketTime.AddOrUpdate(phi.SensorId, dt, (key, existingVal) => dt);
                }

                if (IsGoodPacket(phi, _isPlayback, DateTime.UtcNow))
                {
                    if (phi.MsgType == PacketType.MsgTypeLtg && !string.IsNullOrWhiteSpace(phi.SensorId))
                    {
                        string ver = string.Format("{0}.{1}", (int)phi.MajorVersion, (int)phi.MinorVersion);
                        _sensorPacketVersion.AddOrUpdate(phi.SensorId, ver, (key, existing) => ver);
                    }

                    if (_wwllnPacketProcessor != null && phi.MsgType == PacketType.MsgTypeLtg)
                    {
                        _wwllnPacketProcessor.AddTlnLtgPacket(phi);
                    }

                    if (phi.MsgType == PacketType.MsgTypeGps)
                    {
                        _dbWriterProcessor.Add(phi);
                    }

                    if (_wwllnTogaSender != null)
                    {
                        _wwllnTogaSender.AddPacket(phi);
                    }

                    if (_fileWriterProcessor != null)
                    {
                        _fileWriterProcessor.Write(phi);
                    }
                }
              

                
                _lastDequeued = LtgTimeUtils.GetUtcDateTimeFrom1970Seconds(phi.PacketTimeInSeconds);
            }
        }

        private bool IsGoodPacket(PacketHeaderInfo phi, bool isPlayback, DateTime utcNow)
        {
            bool isGood = true;

            if (!isPlayback)
            {
                DateTime packetTime = LtgTimeUtils.GetUtcDateTimeFrom1970Seconds(phi.PacketTimeInSeconds);
                TimeSpan ts = utcNow - packetTime;
                if (Math.Abs(ts.TotalSeconds) > LtgConstants.MaxPacketArriveTimeTooEarlyOrTooLateInSeconds)
                {
                    isGood = false;
                    EventManager.LogInfo(EventManagerOffset + 1, string.Format("Discarding {0} packet from sensor {1} with bad time, packet age: {2:n2} secs", phi.MsgType, phi.SensorId, ts.TotalSeconds));
                }
            }

            return isGood;
        }

        public DecodeResult DecodePacket(List<byte> packetBuffer, string remoteIp)
        {
            PacketHeaderInfo phi;
            DecodeResult result = TcpPacketUtils.TryTcpDecode(packetBuffer, remoteIp, out phi);

            if (result == DecodeResult.FoundPacket)
            {
                if (phi != null)
                {
                    _lastEnqueued = LtgTimeUtils.GetUtcDateTimeFrom1970Seconds(phi.PacketTimeInSeconds);
                }

                Add(phi);
            }

            return result;
        }

        public DecodeResult DecodeSensorPacket(List<byte> packetBuffer, string remoteIp)
        {
            PacketHeaderInfo phi;
            DecodeResult result = TcpPacketUtils.TryTcpSensorDecode(packetBuffer, remoteIp, out phi);

            if (result == DecodeResult.FoundPacket)
            {
                if (phi != null)
                {
                    _lastEnqueued = LtgTimeUtils.GetUtcDateTimeFrom1970Seconds(phi.PacketTimeInSeconds);
                }

                Add(phi);
            }

            return result;
        }

        public int GetActiveSensorCount(DateTime utcNow, int intervalSeconds)
        {
            int active = 0;
            if (_sensorLastPacketTime != null)
            {
                foreach (KeyValuePair<string, DateTime> keyValuePair in _sensorLastPacketTime)
                {
                    TimeSpan span = utcNow - keyValuePair.Value;
                    if (span.TotalSeconds <= intervalSeconds)
                    {
                        active++;
                    }
                }
            }
            return active;
        }

        public Sender TlnDataForwarder 
        {
            set { _tlnDataForwarder = value; }
        }

        public WwllnPacketProcessor WwllnPacketProcessor
        {
            set { _wwllnPacketProcessor = value; }
        }

        public ConcurrentDictionary<string, string> SensorPacketVersion
        {
            get { return _sensorPacketVersion; }
        }

        public SensorStatisticsHandler SensorStatisticsHandler
        {
            set { _sensorStatisticsHandler = value; }
        }

        public Sender WwllnTogaSender
        {
            set { _wwllnTogaSender = value; }
        }

        public DateTime LastEnqueued
        {
            get { return _lastEnqueued; }
        }

        public DateTime LastDequeued
        {
            get { return _lastDequeued; }
        }
    }
}
