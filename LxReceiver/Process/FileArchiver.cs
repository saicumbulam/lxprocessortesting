﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

using Amazon.S3;
using Amazon.S3.Transfer;

using Aws.Core.Utilities;

using LxCommon.Monitoring;

using LxReceiver.Common;

namespace LxReceiver.Process
{
    public enum ArchiverAction { None, Delete, MoveToFolder, MoveToS3 }

    public class FileArchiver
    {
        private string _folder;
        private string _fileNamePattern;
        private ManualResetEventSlim _stopEvent;
        private ArchiverAction _action;
        private string _archivePath;
        private Timer _timer;
        private int _archiverIntervalMinutes;
        private string _archivePathDateFormatString;

        private static readonly int ArchiverTimeToKeepFilesLocalMinutes = Config.ArchiverTimeToKeepFilesLocalMinutes;
        
        private const ushort EventManagerOffset = TaskMonitor.FileArchiver;

        public FileArchiver(string folder, string baseFileName, string archivePath, string archivePathDateFormatString, ArchiverAction action, int archiverIntervalMinutes, ManualResetEventSlim stopEvent)
        {
            _folder = folder;
            _fileNamePattern = ReplaceFormatTokens(baseFileName);
            _archivePath = archivePath;
            _action = action;
            _archiverIntervalMinutes = archiverIntervalMinutes;
            _stopEvent = stopEvent;
            _archivePathDateFormatString = archivePathDateFormatString;
        }

        private string ReplaceFormatTokens(string baseFileName)
        {
            if (!string.IsNullOrEmpty(baseFileName))
            {
                int start = baseFileName.IndexOf('{');
                int end = (start == -1) ? -1 : baseFileName.IndexOf('}', start);
                int count = end - start + 1;
                while (start != -1 && end != -1 && count > 0)
                {
                    baseFileName = baseFileName.Replace(baseFileName.Substring(start, count), "*");
                    start = baseFileName.IndexOf('{');
                    end = (start == -1) ? -1 : baseFileName.IndexOf('}', start);
                    count = end - start + 1;
                }
            }

            return baseFileName;
        }

        public void Start(object state)
        {
            EventManager.LogInfo("Starting FileArchiver");
            DateTime utcNow = DateTime.UtcNow;
            DirectoryInfo di = GetDirectory(_folder);
            if (di != null)
            {
                List<DirectoryInfo> subDirectories = GetSubDirectories(di, utcNow, ArchiverTimeToKeepFilesLocalMinutes);
                if (subDirectories != null)
                {
                    foreach (DirectoryInfo directoryInfo in subDirectories)
                    {
                        if (_stopEvent.Wait(0)) break;

                        ProcessDirectory(directoryInfo, utcNow, _action, _fileNamePattern, ArchiverTimeToKeepFilesLocalMinutes, _archivePath, _archivePathDateFormatString);
                    }
                }
            }

            if (_timer != null)
            {
                _timer = new Timer(Start, null, TimeSpan.FromMinutes(_archiverIntervalMinutes), TimeSpan.FromMilliseconds(-1));
            }
        }

        private DirectoryInfo GetDirectory(string folder)
        {
            DirectoryInfo di = null;
            try
            {
                di = new DirectoryInfo(folder);
                if (!di.Exists)
                    di = null;

            }
            catch (Exception ex)
            {
                di = null;
                EventManager.LogError(EventManagerOffset, string.Format("Unable to get directory for FileArchiver: {0}", folder), ex);
            }

            return di;
        }

        private List<DirectoryInfo> GetSubDirectories(DirectoryInfo di, DateTime utcNow, int timeToKeepMinutes)
        {
            List<DirectoryInfo> subs = null;
            DirectoryInfo[] dis = null;

            try
            {
                dis = di.GetDirectories();
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventManagerOffset + 1, string.Format("Unable to get sub directories for FileArchiver: {0}", di.FullName), ex);
            }

            if (dis != null && dis.Length > 0)
            {
                subs = new List<DirectoryInfo>();
                foreach (DirectoryInfo directoryInfo in dis)
                {
                    if (_stopEvent.Wait(0)) break;

                    DateTime folderTime;
                    if (TryParseFolderName(directoryInfo.Name, out folderTime))
                    {
                        TimeSpan ts = utcNow - folderTime;
                        if (ts.TotalMinutes > timeToKeepMinutes)
                        {
                            subs.Add(directoryInfo);
                        }
                    }
                }
            }

            return subs;
        }

        private bool TryParseFolderName(string folderName, out DateTime folderTime)
        {
            bool success = false;
            if (DateTime.TryParse(folderName, out folderTime))
            {
                success = true;
                folderTime = DateTime.SpecifyKind(folderTime, DateTimeKind.Utc);
            }
            return success;
        }

        private void ProcessDirectory(DirectoryInfo di, DateTime utcNow, ArchiverAction action, string fileNamePattern, int timeToKeepMinutes, string archivePath, string archivePathDateFormatString)
        {
            FileInfo[] fis = null;
            try
            {
                fis = di.GetFiles(fileNamePattern, SearchOption.TopDirectoryOnly);
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventManagerOffset + 2, string.Format("Unable to get files for FileArchiver: {0}", di.FullName), ex);
            }

            if (fis != null && fis.Length > 0)
            {
                foreach (FileInfo fileInfo in fis)
                {
                    if (_stopEvent.Wait(0)) break;

                    DateTime fileTime;
                    if (TryGetFileTime(fileInfo, fileNamePattern, out fileTime))
                    {
                        TimeSpan ts = utcNow - fileTime;
                        if (ts.TotalMinutes > timeToKeepMinutes)
                        {
                            switch (action)
                            {
                                case ArchiverAction.Delete:
                                    DeleteFile(fileInfo);
                                    break;
                                case ArchiverAction.MoveToFolder:
                                    MoveToFolder(fileInfo, fileTime, archivePath);
                                    break;
                                case ArchiverAction.MoveToS3:
                                    MoveToS3(fileInfo, fileTime, archivePathDateFormatString, archivePath);
                                    break;
                            }
                        }
                    }
                }
            }

            //remove empty directories
            try
            {
                FileSystemInfo[] infos = di.GetFileSystemInfos();
                if (infos.Length == 0)
                {
                    di.Delete();
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventManagerOffset + 11, string.Format("Unable to delete directory: {0}", di.FullName), ex);
            }
        }

        private bool TryGetFileTime(FileInfo fileInfo, string fileNamePattern, out DateTime fileTime)
        {
            bool success = false;
            fileTime = DateTime.MinValue;
            int start = fileNamePattern.IndexOf('*');
            if (start > 0)
            {
                string left = fileNamePattern.Substring(0, start);
                string right = fileNamePattern.Substring(start + 1);
                string name = fileInfo.Name.Replace(left, string.Empty);
                name = name.Replace(right, string.Empty);

                int end = name.LastIndexOf('-');
                if (end > 0)
                {
                    name = name.Remove(end, 1);
                    name = name.Insert(end, ":");

                    if (DateTime.TryParse(name, out fileTime))
                    {
                        success = true;
                        fileTime = DateTime.SpecifyKind(fileTime, DateTimeKind.Utc);
                    }
                }
            }

            return success;
        }
        
        private void DeleteFile(FileInfo fileInfo)
        {
            try
            {
                EventManager.LogInfo(EventManagerOffset + 3, string.Format("FileArchiver Deleting file: {0}", fileInfo.FullName));
                fileInfo.Delete();
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventManagerOffset + 4, string.Format("Error deleting file: {0}", fileInfo.FullName), ex);
            }
        }

        private void MoveToFolder(FileInfo fileInfo, DateTime fileTime, string archivePath)
        {
            string partial = BuildPartialFilePath(fileTime);
            string path = string.Empty;
            try
            {
                path = Path.Combine(archivePath, partial);
                DirectoryInfo di;
                if (TryGetCreateDirectory(path, out di))
                {
                    path = Path.Combine(path, fileInfo.Name);
                    EventManager.LogInfo(EventManagerOffset + 5, string.Format("FileArchiver moving file from {0} to {1}", fileInfo.FullName, path));
                    fileInfo.MoveTo(path);
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventManagerOffset + 6, string.Format("Error moving file from {0} to {1}", fileInfo.FullName, path), ex);
            }
        }


        private string BuildPartialFilePath(DateTime fileTime)
        {
            return fileTime.ToString("yyyy\\\\MM\\\\dd");
        }

        private bool TryGetCreateDirectory(string path, out DirectoryInfo di)
        {
            di = null;
            bool success = false;
            try
            {
                di = new DirectoryInfo(path);
                if (!di.Exists)
                {
                    di.Create();
                }
                success = true;

            }
            catch (Exception ex)
            {
                di = null;
                EventManager.LogError(EventManagerOffset + 7, string.Format("Unable to get/create directory for FileArchiver: {0}", path), ex);
            }

            return success;
        }

        private static readonly TransferUtilityConfig TransferUtilityConfig = new TransferUtilityConfig()
        {
            ConcurrentServiceRequests = Config.ArchiverS3NumberOfUploadThreads
        };

        private void MoveToS3(FileInfo fileInfo, DateTime fileTime, string archivePathDateFormatString, string archivePath)
        {
            string s3Path = fileTime.ToString(archivePathDateFormatString);
            s3Path = archivePath + s3Path;
            try
            {
                EventManager.LogInfo(EventManagerOffset + 8, string.Format("FileArchiver moving file from {0} to S3 {1}", fileInfo.FullName, s3Path));
                TransferUtility fileTransferUtility;
                using (fileTransferUtility = new TransferUtility(Config.AwsAccessKey, Config.AwsSecretAccess, TransferUtilityConfig))
                {
                    fileTransferUtility.Upload(fileInfo.FullName, s3Path);
                }
                DeleteFile(fileInfo);
            }
            catch (AmazonS3Exception awsEx)
            {
                EventManager.LogError(EventManagerOffset + 9,
                                      string.Format(
                                          "Error archiving file {0} to {1} in AWS. Aws Error Code = {2}, Aws Request Id = {3}, Aws Error Message = {4}",
                                          fileInfo.FullName, s3Path, awsEx.ErrorCode, awsEx.RequestId, awsEx.Message), awsEx);
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventManagerOffset + 10, string.Format("Error archiving file {0} to {1} in AWS.", fileInfo.FullName, s3Path), ex);
            }
        }


        public Timer Timer { set { _timer = value; } }
    }
}
