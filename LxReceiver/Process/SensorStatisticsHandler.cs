﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;
using Aws.Core.Utilities;
using LxCommon;
using LxCommon.Utilities;
using LxReceiver.Common;
using LxReceiver.Services;

namespace LxReceiver.Process
{
    public class SensorStatisticsHandler
    {
        private readonly ConcurrentDictionary<string, Datum> _sensorStats;
        private readonly DatabaseGateway _databaseGateway;
        private readonly ParallelOptions _parallelOptions;
        private readonly int _sampleSizeMinutes;
        private readonly int _medianSampleSizeMinutes;
        private object _latencyLock;
        private List<int> _sensorMinLatencyList;
        private List<int> _sensorMaxLatencyList;
        private List<int> _sensorMedianLatencyList;

        public SensorStatisticsHandler(int sampleSizeMinutes, int medianSampleSizeMinutes, int saveMaxDegreeOfParallelism)
        {
            
            _parallelOptions = new ParallelOptions { MaxDegreeOfParallelism = saveMaxDegreeOfParallelism };
            _sampleSizeMinutes = sampleSizeMinutes;
            _medianSampleSizeMinutes = medianSampleSizeMinutes;
            _sensorStats = new ConcurrentDictionary<string, Datum>();

            _databaseGateway = new DatabaseGateway(ConfigurationManager.ConnectionStrings["Lightning"].ConnectionString);

            _latencyLock = new object();
            _sensorMinLatencyList = new List<int>();
            _sensorMaxLatencyList = new List<int>();
            _sensorMedianLatencyList = new List<int>();
        }

        public void Update(PacketHeaderInfo phi)
        {
            if (phi != null && !string.IsNullOrWhiteSpace(phi.SensorId))
            {
                DateTime utcNow = DateTime.UtcNow;
                Datum d = _sensorStats.GetOrAdd(phi.SensorId, new Datum(_sampleSizeMinutes, _medianSampleSizeMinutes, utcNow));
                d.AddPacket(phi, utcNow);
            }
        }

        public void Save(object state)
        {
            _sensorMinLatencyList.Clear();
            _sensorMaxLatencyList.Clear();
            _sensorMedianLatencyList.Clear();
            DateTime timeStamp = DateTime.UtcNow;
            Parallel.ForEach(_sensorStats, _parallelOptions, (kvp) =>
            {
                DateTime utcNow = DateTime.UtcNow;
                Datum d = kvp.Value.SafeDataCloneAndReset(utcNow);
                _databaseGateway.TryInsertSensorStatistic(kvp.Key, d, timeStamp); 
                lock (_latencyLock)
                {
                    _sensorMinLatencyList.Add(d.MinPacketLatency);
                    _sensorMaxLatencyList.Add(d.MaxPacketLatency);
                    _sensorMedianLatencyList.Add(d.MedianPacketLatency);
                }
               
            });
        }

        public int MinPacketLatency
        {
            get
            {
                int result = 0;
                if (_sensorMinLatencyList != null && _sensorMinLatencyList.Count > 0)
                {
                    List<int> copy = new List<int>(_sensorMinLatencyList);
                    copy.Sort();
                    result = copy[0];
                }
                return result;
            }
        }

        public int MaxPacketLatency
        {
            get
            {
                int result = 0;
                if (_sensorMaxLatencyList != null && _sensorMaxLatencyList.Count > 0)
                {
                    List<int> copy = new List<int>(_sensorMaxLatencyList);
                    copy.Sort();
                    result = copy[copy.Count-1];
                }
                return result;
            }
        }

        public int MedianPacketLatency
        {
            get
            {
                int result = 0;
                if (_sensorMedianLatencyList != null && _sensorMedianLatencyList.Count > 0)
                {
                    List<int> copy = new List<int>(_sensorMedianLatencyList);
                    copy.Sort();
                    result = copy[copy.Count / 2];
                }
                return result;
            }
        }

        /// <summary>
        /// For this class all public methods aquire the lock and all private methods assume you have already aquired the lock
        /// </summary>
        public class Datum
        {
            private Queue<int> _samples;
            private int _maxSampleSize;
            private int _maxMedianSampleSize;
            private object _syncLock;
            private int _lastResetTime;
            private MedianSampler<int> _latencyMedianSampler;
            private MedianSampler<int> _lfThresholdMedianSampler;
            private MedianSampler<int> _hfThresholdMedianSampler;

            public Datum(int sampleSizeMinutes, int medianSampleSizeMinutes, DateTime utcNow)
            {
                _maxSampleSize = sampleSizeMinutes * 60;
                _maxMedianSampleSize = medianSampleSizeMinutes * 60;
                _lastResetTime = TimeSvc.GetUnixTime(utcNow);
                _samples = new Queue<int>();
                _syncLock = new object();
                _latencyMedianSampler = new MedianSampler<int>(_maxMedianSampleSize);
                _lfThresholdMedianSampler = new MedianSampler<int>(_maxMedianSampleSize);
                _hfThresholdMedianSampler = new MedianSampler<int>(_maxMedianSampleSize);
            }

            /// <summary>
            /// This constructor is only to be used to make the data clone
            /// </summary>
            private Datum()
            {
                _samples = null;
                _syncLock = null;
            }

            public void AddPacket(PacketHeaderInfo phi, DateTime utcNow)
            {
                lock (_syncLock)
                {
                    TotalReceivedPackets++;
                    TotalBytes = TotalBytes + phi.TotalLength;

                    if (phi.TotalLength <= RawPacketUtil.GetPacketHeaderLength(phi))
                    {
                        EmptyPackets++;
                    }

                    if (phi.TotalLength > MaxPacketSize)
                    {
                        MaxPacketSize = phi.TotalLength;
                    }

                    if (MinPacketSize == 0 || phi.TotalLength < MinPacketSize)
                    {
                        MinPacketSize = phi.TotalLength;
                    }

                    if (phi.RawData == null || phi.TotalLength != phi.RawData.Length || phi.PacketTimeInSeconds <= 0 || !IsValidPacketTime(phi.PacketTimeInSeconds, utcNow))
                    {
                        InvalidPackets++;
                        LastTime = phi.PacketTimeInSeconds;
                    }

                    int newTime = phi.PacketTimeInSeconds;
                    if (_samples.Count == 0)
                    {
                        AddSample(newTime);
                    }
                    else
                    {

                        if (_samples.Contains(newTime))
                        {
                            DuplicatePackets++;
                        }
                        else
                        {
                            if (newTime - LastTime > 1)
                            {
                                MissingPackets++;
                            }
                            else if (newTime - LastTime < 0)
                            {
                                WrongOrderPackets++;
                            }

                            AddSample(newTime);
                        }
                    }

                    LastTime = phi.PacketTimeInSeconds;

                    int packetLatency = LtgTimeUtils.GetSecondsSince1970FromDateTime(DateTime.UtcNow) - phi.PacketTimeInSeconds;
                    if (packetLatency <= Config.MaxPacketArriveTimeTooEarlyOrTooLateInSeconds)
                    {
                        _latencyMedianSampler.AddSample(packetLatency);
                    }

                    _lfThresholdMedianSampler.AddSample(phi.LFThreshold);
                    _hfThresholdMedianSampler.AddSample(phi.HFThreshold);
                }
            }

            private bool IsValidPacketTime(int time, DateTime utcNow)
            {
                bool isValid = false;
                DateTime packetTime = LtgTimeUtils.GetUtcDateTimeFrom1970Seconds(time);
                TimeSpan ts = utcNow - packetTime;
                if (Math.Abs(ts.TotalSeconds) <= LtgConstants.MaxPacketArriveTimeTooEarlyOrTooLateInSeconds)
                {
                    isValid = true;
                }
                return isValid;
            }

            private void AddSample(int time)
            {
                _samples.Enqueue(time);
                while (_samples.Count > _maxSampleSize)
                {   
                    _samples.Dequeue();
                }
            }

            /// <summary>
            /// Call this to get a copy of the relevant data to save to the db
            /// </summary>
            /// <param name="utcNow"></param>
            /// <returns>a syncronized copy</returns>
            public Datum SafeDataCloneAndReset(DateTime utcNow)
            {
                Datum d = null;

                lock (_syncLock)
                {
                    var latencyMinMaxMedian = _latencyMedianSampler.GetMinMaxMedian();
                    var lfThresholdMinMaxMedian = _lfThresholdMedianSampler.GetMinMaxMedian();
                    var hfThresholdMinMaxMedian = _hfThresholdMedianSampler.GetMinMaxMedian();

                    d = new Datum()
                    {
                        MissingPackets = MissingPackets,
                        InvalidPackets = InvalidPackets,
                        DuplicatePackets = DuplicatePackets,
                        WrongOrderPackets = WrongOrderPackets,
                        TotalReceivedPackets = TotalReceivedPackets,
                        MinPacketSize = MinPacketSize,
                        MaxPacketSize = MaxPacketSize,
                        TotalBytes = TotalBytes,
                        EmptyPackets = EmptyPackets,
                        TotalSeconds = TimeSvc.GetUnixTime(utcNow) - _lastResetTime,
                        MinPacketLatency = latencyMinMaxMedian.Item1,
                        MaxPacketLatency = latencyMinMaxMedian.Item2,
                        MedianPacketLatency = latencyMinMaxMedian.Item3,
                        MinLFThreshold = lfThresholdMinMaxMedian.Item1,
                        MaxLFThreshold = lfThresholdMinMaxMedian.Item2,
                        MedianLFThreshold = lfThresholdMinMaxMedian.Item3,
                        MinHFThreshold = hfThresholdMinMaxMedian.Item1,
                        MaxHFThreshold = hfThresholdMinMaxMedian.Item2,
                        MedianHFThreshold = hfThresholdMinMaxMedian.Item3
                    };

                    Reset(utcNow);
                }

                return d;
            }

            private void Reset(DateTime utcNow)
            {
                MissingPackets = 0;
                InvalidPackets = 0;
                DuplicatePackets = 0;
                WrongOrderPackets = 0;
                TotalReceivedPackets = 0;
                MinPacketSize = 0;
                MaxPacketSize = 0;
                TotalBytes = 0;
                EmptyPackets = 0;

                _lastResetTime = TimeSvc.GetUnixTime(utcNow);
                _latencyMedianSampler.Reset();
                _lfThresholdMedianSampler.Reset();
                _hfThresholdMedianSampler.Reset();
            }

            public int MissingPackets { get; private set; }
            public int InvalidPackets { get; private set; }
            public int DuplicatePackets { get; private set; }
            public int WrongOrderPackets { get; private set; }
            public int TotalReceivedPackets { get; private set; }
            public int LastTime { get; private set; }
            public int MinPacketSize { get; private set; }
            public int MaxPacketSize { get; private set; }
            public int EmptyPackets { get; private set; }
            public int TotalBytes { get; private set; }
            public int TotalSeconds { get; private set; }
            public int MinPacketLatency { get; private set; }
            public int MaxPacketLatency { get; private set; }
            public int MedianPacketLatency { get; private set; }
            public int MinLFThreshold { get; private set; }
            public int MaxLFThreshold { get; private set; }
            public int MedianLFThreshold { get; private set; }
            public int MinHFThreshold { get; private set; }
            public int MaxHFThreshold { get; private set; }
            public int MedianHFThreshold { get; private set; }
        }
    }
}
