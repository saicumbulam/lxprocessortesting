﻿using System;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading;
using Newtonsoft.Json.Linq;

using Aws.Core.Data.Common.Operations;
using Aws.Core.Utilities;

using LxCommon;
using LxCommon.Monitoring;
using LxCommon.TcpCommunication;

using LxReceiver.Services;
using LxReceiver.Common;

namespace LxReceiver.Process
{
    public class MonitoringHandler
    {
        private readonly string _systemType;
        private readonly string _monitorHmtl;
        
        private Listener _sensorListener;
        private Listener _receiverListener;
        private WwllnListener _wwllnListener;
        private WwllnPacketProcessor _wwllnPacketProcessor;
        private Sender _wwllnDataForwarder;
        private Sender _tlnDataForwarder;
        private IncomingPacketProcessor _incomingPacketProcessor;
        private DbWriterProcessor _dbWriterProcessor;
        private StationDataTable _stationDataTable;
        private SensorStatisticsHandler _sensorStatisticsHandler;

        public MonitoringHandler(string systemType, Listener sensorListener, Listener receiverListener, WwllnListener wwllnListener, WwllnPacketProcessor wwllnPacketProcessor,
            Sender wwllnDataForwarder, Sender tlnDataForwarder, IncomingPacketProcessor incomingPacketProcessor, DbWriterProcessor dbWriterProcessor, StationDataTable stationDataTable, SensorStatisticsHandler sensorStatisticsHandler)
        {
            
            _systemType = systemType;
            _sensorListener = sensorListener;
            _receiverListener = receiverListener;
            _wwllnListener = wwllnListener;
            _wwllnPacketProcessor = wwllnPacketProcessor;
            _wwllnDataForwarder = wwllnDataForwarder;
            _tlnDataForwarder = tlnDataForwarder;
            _incomingPacketProcessor = incomingPacketProcessor;
            _dbWriterProcessor = dbWriterProcessor;
            _stationDataTable = stationDataTable;
            _sensorStatisticsHandler = sensorStatisticsHandler;

            try
            {
                _monitorHmtl = File.ReadAllText(AppUtils.AppLocation + @"\Monitor.html", Encoding.UTF8);
            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.TidMonitorPageUnavailable, "Unable to load monitor html file", ex);
                _monitorHmtl = "Unavailable";
            }
        }

        public void Start()
        {
            try
            {
                OpsCommandHttpEndpointSvc.Start((cmd) => MonitorStatusCheck(cmd));
            }
            catch (Exception ex)
            {
                EventManager.LogError(0, "Unable to start ecv http endpoint", ex);
            }
        }

        private string MonitorStatusCheck(string cmd)
        {
            if (cmd.ToUpper().Equals("PRTG"))
            {
                return GeneratePrtgStatus();
            }
            else
            {
                return GenerateHtmlStatus(_systemType, _monitorHmtl);
            }

        }

        private const string Success = "<span style=\"color: green;\">SUCCESS</span>";
        private const string Failure = "<span style=\"color: red;\">FAIL</span>";
        private const string NA = "-";
        
        private string GenerateHtmlStatus(string systemType, string monitorHmtl)
        {
            string html = null;
            string success = Success;
            string failure = Failure;

            bool isTln = (systemType.ToUpper().Equals("TLN"));

            if (!isTln)
            {
                success = NA;
                failure = NA;
            }

            CultureInfo enUs = new CultureInfo("en-US");

            DateTime utcNow = DateTime.UtcNow;

            double sensorPacketRate = 0.0;
            double sensorConnections = 0.0;
            double sensorConnectionRate = 0.0;
            if (_sensorListener != null)
            {
                sensorPacketRate = _sensorListener.GetIncomingRatePerSecond(utcNow);
                sensorConnections = _sensorListener.Connections;
                sensorConnectionRate = _sensorListener.GetConnectionRatePerSecond(utcNow);
            }

            double receiverPacketRate = 0.0;
            double receiverConnections = 0.0;
            double receiverConnectionRate = 0.0;
            if (_receiverListener != null)
            {
                receiverPacketRate = _receiverListener.GetIncomingRatePerSecond(utcNow);
                receiverConnections = _receiverListener.Connections;
                receiverConnectionRate = _receiverListener.GetConnectionRatePerSecond(utcNow);
            }

            double totalIncomingPacketRate = sensorPacketRate + receiverPacketRate;

            double wwllnRate = (_wwllnListener != null) ? _wwllnListener.GetIncomingRatePerSecond(utcNow) : 0;

            int wwllnCount = 0, wwllnnActive = 0, wwllnMaxActive = 0;
            if (_wwllnPacketProcessor != null)
            {
                wwllnCount = _wwllnPacketProcessor.Count;
                wwllnnActive = _wwllnPacketProcessor.ActiveCommands;
                wwllnMaxActive = _wwllnPacketProcessor.MaxActiveWorkItems;
            }

            int wwllnActiveOut = 0, wwllnOut = 0;
            if (_wwllnDataForwarder != null)
            {
                wwllnActiveOut = _wwllnDataForwarder.ActiveConnections;
                wwllnOut = _wwllnDataForwarder.Connections;
            }

            int tlnOut = 0, tlnActiveOut = 0;
            if (_tlnDataForwarder != null)
            {
                tlnOut = _tlnDataForwarder.Connections;
                tlnActiveOut = _tlnDataForwarder.ActiveConnections;
            }

            int incomingCount = 0;
            int incomingActive = 0;
            int incomingMax = 0;
            int enqueuedUtcOffset = int.MinValue;
            int dequeuedUtcOffset = int.MinValue;
            int activeSensors = 0;

            if (_incomingPacketProcessor != null)
            {
                incomingCount = _incomingPacketProcessor.Count;
                incomingActive = _incomingPacketProcessor.ActiveCommands;
                incomingMax = _incomingPacketProcessor.MaxActiveWorkItems;
                enqueuedUtcOffset = (int)(utcNow - _incomingPacketProcessor.LastEnqueued).TotalSeconds;
                dequeuedUtcOffset = (int)(utcNow - _incomingPacketProcessor.LastDequeued).TotalSeconds;
                activeSensors = _incomingPacketProcessor.GetActiveSensorCount(utcNow, Config.MonitorActiveSensorIntervalSeconds);
            }

            try
            {
                html = string.Format(monitorHmtl,
                        AppUtils.GetVersion(), //0
                        utcNow.ToString(enUs),

                        (sensorConnections >= Config.MonitorIncomingSensorConnectionsMinimum) ? success : failure,
                        sensorConnections.ToString("n0"),

                        (sensorPacketRate >= Config.MonitorIncomingSensorTcpRateMinimum) ? success : failure,
                        sensorPacketRate.ToString("##,0.0"), //5

                        (sensorConnectionRate <= Config.MonitorSensorConnectionRateMaximum) ? success : failure,
                        sensorConnectionRate.ToString("##,0.0"), //5

                        (receiverConnections >= Config.MonitorIncomingReceiverActiveConnectionsMinimum) ? success : failure,
                        receiverConnections.ToString("n0"),

                        (receiverPacketRate >= Config.MonitorIncomingReceiverTcpRateMinimum) ? success : failure,
                        receiverPacketRate.ToString("##,0.0"),

                        (receiverConnectionRate <= Config.MonitorReceiverConnectionRateMaximum) ? success : failure,
                        receiverConnectionRate.ToString("##,0.0"), //5

                        (totalIncomingPacketRate >= Config.MonitorIncomingTotalTcpRateMinimum) ? Success : Failure,
                        totalIncomingPacketRate.ToString("##,0.0"),

                        (wwllnRate >= Config.MonitorIncomingWwllnUdpRateMinimum) ? success : failure, //10
                        wwllnRate.ToString("##,0.0"),

                        (enqueuedUtcOffset <= Config.MonitorLastEnqueuedUtcOffsetMaximum) ? success : failure,
                        enqueuedUtcOffset.ToString("n0"),

                        (dequeuedUtcOffset <= Config.MonitorLastDequeuedUtcOffsetMaximum) ? success : failure,
                        dequeuedUtcOffset.ToString("n0"), //15

                        (tlnOut == tlnActiveOut) ? success : failure,
                        tlnActiveOut,

                        (wwllnOut == wwllnActiveOut) ? success : failure,
                        wwllnActiveOut,

                        NA,
                        _dbWriterProcessor.LastGpsDbSave.ToString(enUs),

                        NA,
                        _dbWriterProcessor.LastLogDbSave.ToString(enUs),

                        NA,
                        _stationDataTable.LastRefreshTime,

                        NA,
                        activeSensors,

                        // min
                        NA,
                        (_sensorStatisticsHandler == null) ? NA : _sensorStatisticsHandler.MinPacketLatency.ToString(),

                        NA,
                        (_sensorStatisticsHandler == null) ? NA : _sensorStatisticsHandler.MaxPacketLatency.ToString(),

                        NA,
                        (_sensorStatisticsHandler == null) ? NA : _sensorStatisticsHandler.MedianPacketLatency.ToString(),

                        incomingCount, //20
                        incomingActive,
                        incomingMax,

                        wwllnCount,
                        wwllnnActive,
                        wwllnMaxActive, //25

                        _dbWriterProcessor.Count,
                        _dbWriterProcessor.ActiveCommands,
                        _dbWriterProcessor.MaxActiveWorkItems
                     );
            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.TidMonitorPageUnavailable, "Unable to build monitor html file", ex);
            }

            return html;
        }

        private string GeneratePrtgStatus()
        {
            DateTime utcNow = DateTime.UtcNow;
            JObject j = new JObject();
            double sensorPacketRate = 0.0;
            int sensorConnections = 0;
            if (_sensorListener != null)
            {
                sensorPacketRate = _sensorListener.GetIncomingRatePerSecond(utcNow);
                sensorPacketRate = Math.Round(sensorPacketRate, 2, MidpointRounding.AwayFromZero);
                sensorConnections = _sensorListener.Connections;
            }
            j.Add("sensorConnections", sensorConnections);
            j.Add("sensorPacketRate", sensorPacketRate);

            double receiverPackerRate = 0.0;
            int recieverConnections = 0;
            if (_receiverListener != null)
            {
                receiverPackerRate = _receiverListener.GetIncomingRatePerSecond(utcNow);
                receiverPackerRate = Math.Round(receiverPackerRate, 2, MidpointRounding.AwayFromZero);
                recieverConnections = _receiverListener.Connections;
            }
            j.Add("receiverConnections", recieverConnections);
            j.Add("receiverPacketRate", receiverPackerRate);
            j.Add("sensorAndReceiverPacketRate", receiverPackerRate + sensorPacketRate);

            DateTime lastPacketReceived = DateTime.MinValue;
            DateTime lastPacketProcessed = DateTime.MinValue;
            int activeSensor = 0;
            if (_incomingPacketProcessor != null)
            {
                lastPacketReceived = _incomingPacketProcessor.LastEnqueued;
                lastPacketProcessed = _incomingPacketProcessor.LastDequeued;
                activeSensor = _incomingPacketProcessor.GetActiveSensorCount(utcNow, Config.MonitorActiveSensorIntervalSeconds);
            }
            j.Add("lastPacketReceived", lastPacketReceived);
            j.Add("lastPacketProcessed", lastPacketProcessed);
            j.Add("activeSensors", activeSensor);

            double wwllnRate = (_wwllnListener != null) ? _wwllnListener.GetIncomingRatePerSecond(utcNow) : 0;
            wwllnRate = Math.Round(wwllnRate, 2, MidpointRounding.AwayFromZero);
            j.Add("wwllnReceivePacketRate", wwllnRate);

            int worker, completion;
            ThreadPool.GetMinThreads(out worker, out completion);
            j.Add("minThreadPoolWorkerThreads", worker);
            j.Add("minThreadPoolCompletionPortThreads", completion);

            ThreadPool.GetMaxThreads(out worker, out completion);
            j.Add("maxThreadPoolWorkerThreads", worker);
            j.Add("maxThreadPoolCompletionPortThreads", completion);

            ThreadPool.GetAvailableThreads(out worker, out completion);
            j.Add("availableThreadPoolWorkerThreads", worker);
            j.Add("availableThreadPoolCompletionPortThreads", completion);

            if (_sensorStatisticsHandler == null)
            {
                j.Add("minPacketLatency", null);
                j.Add("maxPacketLatency", null);
                j.Add("medianPacketLatency", null);
            }
            else
            {
                j.Add("minPacketLatency", _sensorStatisticsHandler.MinPacketLatency);
                j.Add("maxPacketLatency", _sensorStatisticsHandler.MaxPacketLatency);
                j.Add("medianPacketLatency", _sensorStatisticsHandler.MedianPacketLatency);
            }

            return j.ToString();
        }

        public void Stop()
        {
            try
            {
                OpsCommandHttpEndpointSvc.Stop();
            }
            catch (Exception ex)
            {
                EventManager.LogError(1, "Unable to stop ecv http endpoint", ex);
            }

        }

    }
}
