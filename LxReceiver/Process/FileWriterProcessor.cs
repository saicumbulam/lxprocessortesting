﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

using Aws.Core.Utilities;

using LxCommon;
using LxCommon.Monitoring;

namespace LxReceiver.Process
{
    public class FileWriterProcessor
    {
        private const ushort EventManagerOffset = TaskMonitor.FileWriterProcessor;

        private string _folder;
        private string _baseFileName;
        private ConcurrentQueue<PacketHeaderInfo> _packetsToWrite;
        private int _writerLock;
        private DirectoryInfo _directory = null;
        private int _minutesPerFile;
        private ManualResetEventSlim _stopEvent;

        public FileWriterProcessor(string folder, string baseFileName, int minutesPerFile, ManualResetEventSlim stopEvent)
        {
            _folder = folder;
            _baseFileName = baseFileName;
            _minutesPerFile = minutesPerFile;
            _stopEvent = stopEvent;

            _packetsToWrite = new ConcurrentQueue<PacketHeaderInfo>();
            
            _directory = CreateDirectory(_folder);
        }

        private DirectoryInfo CreateDirectory(string folder)
        {
            DirectoryInfo di;
            try
            {
                di = new DirectoryInfo(folder);
                if (!di.Exists)
                    di.Create();
            }
            catch (Exception ex)
            {
                di = null;
                EventManager.LogError(EventManagerOffset, string.Format("Unable to create directory for FileArchiveProcessor: {1}"), ex);
            }

            return di;
        }

        public void Write(PacketHeaderInfo phi)
        {
            _packetsToWrite.Enqueue(phi);
            // This controls the thread that manages the queue, make sure we only launch one thread 
            if (0 == Interlocked.CompareExchange(ref _writerLock, 1, 0))
            {
                Task.Factory.StartNew(WritePackets);
            }
        }

        private void WritePackets()
        {
            PacketHeaderInfo phi;
            string currentFileName = null;
            FileStream fileStream = null;
            int packetCount = 0;
            try
            {
                while (_packetsToWrite.TryDequeue(out phi) && !_stopEvent.Wait(0))
                {
                    if (phi != null && phi.RawData != null && phi.RawData.Length > 0)
                    {
                        DateTime dt = LtgTimeUtils.GetUtcDateTimeFrom1970Seconds(phi.PacketTimeInSeconds);
                        string fileName = GetFullFileName(dt);
                        if (fileName != null)
                        {
                            if (fileStream == null || currentFileName != fileName)
                            {
                                if (fileStream != null)
                                {
                                    FinalizeFileStream(fileStream, packetCount);
                                }

                                fileStream = GetFileStream(fileName, out packetCount);
                                currentFileName = fileName;
                            }
                            short size = (short)phi.RawData.Length;
                            byte[] s = BitConverter.GetBytes(size);
                            fileStream.Write(s, 0, 2);
                            fileStream.Write(phi.RawData, 0, phi.RawData.Length);
                            packetCount++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventManagerOffset + 2, "An error occured while writing the file in FileArchiveProcessor", ex);
            }
            finally
            {
                if (fileStream != null)
                {
                    FinalizeFileStream(fileStream, packetCount);
                }
                Interlocked.Decrement(ref _writerLock);
            }
        }

        private void FinalizeFileStream(FileStream fs, int packetCount)
        {
            try
            {
                byte[] num = BitConverter.GetBytes(packetCount);
                fs.Seek(0, SeekOrigin.Begin);
                fs.Write(num, 0, 4);
                fs.Close();
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventManagerOffset + 2, "An error occured while finalizing the FileStream in FileArchiveProcessor", ex);
            }
                
        }

        private FileStream GetFileStream(string fileName, out int packetCount)
        {
            FileStream fs;
            packetCount = 0;
            if (File.Exists(fileName))
            {
                fs = new FileStream(fileName, FileMode.Open, FileAccess.ReadWrite);
                if (fs.Length >= 4)
                {
                    byte[] num = new byte[4];
                    fs.Read(num, 0, 4);
                    packetCount = BitConverter.ToInt32(num, 0);
                    //fs.Seek(fs.Length, SeekOrigin.Begin);
                    fs.Seek(0, SeekOrigin.End);
                }
                else
                {
                    fs.Seek(4, SeekOrigin.Begin);
                }
            }
            else
            {
                fs = new FileStream(fileName, FileMode.CreateNew, FileAccess.ReadWrite);
                fs.Seek(4, SeekOrigin.Begin);
            }

            return fs;
        }

        private string GetFullFileName(DateTime utcTime)
        {
            string fileName = null;
            string d = string.Format("\\{0:yyyy-MM-dd}\\", utcTime);
            string dirPath = _directory.FullName + d;

            DirectoryInfo di = CreateDirectory(dirPath);
            if (di != null)
            {
                int min = (utcTime.Minute / _minutesPerFile) * _minutesPerFile;
                string f = string.Format("{0:D4}-{1:D2}-{2:D2}T{3:D2}-{4:D2}", utcTime.Year, utcTime.Month, utcTime.Day, utcTime.Hour, min);
                fileName = string.Format(_baseFileName, f);
                fileName = Path.Combine(di.FullName, fileName);
            }

            return fileName;
        }
    }
}
