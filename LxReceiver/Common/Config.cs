using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net;
using Aws.Core.Utilities;
using Aws.Core.Utilities.Config;
using LxCommon;
using LxCommon.Monitoring;
using LxCommon.TcpCommunication;
using LxReceiver.Models;
using LxReceiver.Process;

namespace LxReceiver.Common
{
    public class Config : AppConfig
    {
        private static readonly ConcurrentDictionary<IPEndPoint, ISendWorker> _wwllnDataForward;
        private static readonly ConcurrentDictionary<IPEndPoint, ISendWorker> _wwllnTogaSender;
        
        
        static Config()
        {    
            _wwllnDataForward = LoadWwllnDataForward();
            _wwllnTogaSender = LoadWwllnTogaSender();
        }

        public static ConcurrentDictionary<IPEndPoint, ISendWorker> LoadTlnDataForwardConfig(ConcurrentDictionary<string, StationData> stationData)
        {
            ConcurrentDictionary<IPEndPoint, ISendWorker> clients = new ConcurrentDictionary<IPEndPoint, ISendWorker>();
            
            int num = Get("NumTlnDataForward", 0);
            if (num > 0)
            {
            
                for (int i = 1; i <= num; i++)
                {
                    int port = Get(string.Format("TlnDataForwardPort{0}", i), -1);
                    IPAddress address;
                    if (port != -1 && IPAddress.TryParse(Get(string.Format("TlnDataForwardIp{0}", i), string.Empty), out address))
                    {
                        IPEndPoint ipep = new IPEndPoint(address, port);

                        List<string> p = GetListConfigSettings<string>(string.Format("TlnDataForwardPacketType{0}", i), null);
                        HashSet<PacketType> packets = new HashSet<PacketType>();
                        if (p != null && p.Count > 0)
                        {
                            foreach (string s in p)
                            {
                                PacketType pt;
                                if (Enum.TryParse(s, out pt))
                                {
                                    packets.Add(pt);
                                }
                            }
                        }

                        List<string> n = GetListConfigSettings<string>(string.Format("TlnDataForwardNetworkType{0}", i), null);
                        HashSet<string> networks = new HashSet<string>();
                        if (n != null && n.Count > 0)
                        {
                            foreach (string s in n)
                            {
                                if (!string.IsNullOrWhiteSpace(s))
                                {
                                    networks.Add(s.ToUpperInvariant());
                                }
                            }
                        }

                        List<string> b = GetListConfigSettings<string>(string.Format("TlnDataForwardIpBackList{0}", i), null);
                        HashSet<string> blackList = new HashSet<string>();
                        if (b != null && b.Count > 0)
                        {
                            foreach (string s in b)
                            {
                                if (!string.IsNullOrWhiteSpace(s))
                                {
                                    blackList.Add(s);
                                }
                            }
                        }


                        TlnSendWorker tsw = new TlnSendWorker(packets, networks, blackList, ipep, stationData, IsPlayback);
                        clients.TryAdd(ipep, tsw);
                    }
                    else
                    {
                        EventManager.LogError(TaskMonitor.TidServiceConfigLoadFail, string.Format("Unable to parse config for TlnDataForward{0}", i));
                    }
                }
            }
            return clients;
        }

        private static ConcurrentDictionary<IPEndPoint, ISendWorker> LoadWwllnDataForward()
        {
            ConcurrentDictionary<IPEndPoint, ISendWorker> clients = null;
            int num = Get("NumWwllnDataForward", 0);
            if (num > 0)
            {
                clients = new ConcurrentDictionary<IPEndPoint, ISendWorker>();
                for (int i = 1; i <= num; i++)
                {
                    int port = Get(string.Format("WwllnDataForwardPort{0}", i), -1);
                    IPAddress address;
                    if (port != -1 && IPAddress.TryParse(Get(string.Format("WwllnDataForwardIp{0}", i), string.Empty), out address))
                    {
                        IPEndPoint ipep = new IPEndPoint(address, port);
                        SendWorker sw = new SendWorker(ipep);
                        clients.TryAdd(ipep, sw);
                    }
                    else
                    {
                        EventManager.LogError(TaskMonitor.TidServiceConfigLoadFail, string.Format("Unable to parse config for WwllnDataForward{0}", i));
                    }
                }
            }

            return clients;
        }

        public static ConcurrentDictionary<IPEndPoint, ISendWorker> WwllnDataForward
        {
            get { return _wwllnDataForward; }
        }

        private static ConcurrentDictionary<IPEndPoint, ISendWorker> LoadWwllnTogaSender()
        {
            ConcurrentDictionary<IPEndPoint, ISendWorker> clients = null;
            
            int num = Get("NumWwllnTogaSender", 0);
            if (num > 0)
            {
                clients = new ConcurrentDictionary<IPEndPoint, ISendWorker>();
                for (int i = 1; i <= num; i++)
                {
                    int port = Get(string.Format("WwllnTogaSenderPort{0}", i), -1);
                    IPAddress address;
                    if (port != -1 && IPAddress.TryParse(Get(string.Format("WwllnTogaSenderIp{0}", i), string.Empty), out address))
                    {
                        IPEndPoint ipep = new IPEndPoint(address, port);
                        WwllnTogaSendWorker wtsw = new WwllnTogaSendWorker(WwllnTogaSenderMaxAgeInSeconds, ipep);
                        clients.TryAdd(ipep, wtsw);
                    }
                    else
                    {
                        EventManager.LogError(TaskMonitor.TidServiceConfigLoadFail, string.Format("Unable to parse config for WwllnTogaSender{0}", i));
                    }
                }
            }

            return clients;
        }

        public static ConcurrentDictionary<IPEndPoint, ISendWorker> WwllnTogaSender
        {
            get { return _wwllnTogaSender; }
        }


        public static int WwllnTogaSenderMaxAgeInSeconds
        {
            get { return Get("WwllnTogaSenderMaxAgeInSeconds", 10); }
        }

        public static int SensorListenPort
        {
            get { return Get("SensorListenPort", 9500); }
        }

        public static int SensorListenerMaxPendingConnections 
        {
            get { return Get("SensorListenerMaxPendingConnections", 1000); }
        }

        public static int SensorListenerRateSampleSize 
        {
            get { return Get("SensorListenerRateSampleSize", 15); }
        }

        public static int ReceiverListenPort
        {
            get { return Get("ReceiverListenPort", 1010); }
        }

        public static int ReceiverListenerMaxPendingConnections
        {
            get { return Get("ReceiverListenerMaxPendingConnections", 10); }
        }

        public static int ReceiverListenerRateSampleSize
        {
            get { return Get("ReceiverListenerRateSampleSize", 15); }
        }

        public static bool EnableWwlln
        {
            get { return Get("EnableWwlln", true); }
        }

        public static int WwllnListenPort
        {
            get { return Get("WwllnListenPort", 9503); }
        }

        public static bool EnablePacketTimeCheck
        {
            get { return Get("EnablePacketTimeCheck", true); }
        }

        public static bool EnableEnTlnArchive
        {
            get { return Get("EnableEnTlnArchive", true);}
        }

        public static int EnTlnArchiveMinutesPerFile
        {
            get { return Get("EnTlnArchiveMinutesPerFile", 5); }
        }

        public static string EnTlnArchiveDirectory
        {
            get { return Get("EnTlnArchiveDirectory", @"E:\EN\Archive\LxReceiver"); }
        }

        public static string EnTlnArchiveBaseFileName
        {
            get { return Get("EnTlnArchiveBaseFileName", @"Archive-{0}.LTG"); }
        }

        public static bool EnableWwllnArchive
        {
            get { return Get("EnableWwllnArchive", true); }
        }

        public static int WwllnArchiveMinutesPerFile
        {
            get { return Get("WwllnArchiveMinutesPerFile", 10); }
        }

        public static string WwllnArchiveDirectory
        {
            get { return Get("WwllnArchiveDirectory", @"E:\AppLogger\LxReceiver\WwllnArchive"); }
        }

        public static int StationDatabaseUpdateIntervalMinutes
        {
            get { return Get("StationDatabaseUpdateIntervalMinutes", 600); }
        }

        public static int MaxIncomingPacketProcessorQueueTasks
        {
            get { return Get("MaxIncomingPacketProcessorQueueTasks", 20); }
        }

        public static int WwllnSourceTimeoutSeconds
        {
            get { return Get("WwllnSourceTimeoutSeconds", 2); }
        }

        public static int MaxWwllnPacketProcessorQueueTasks 
        {
            get { return Get("MaxWwllnPacketProcessorQueueTasks", 5); }
        }

        public static int NumberOfSecondsTlnDataForWwllnAmplitude 
        {
            get { return Get("NumberOfSecondsTlnDataForWwllnAmplitude", 90); }
        }

        public static int MaxDistanceForTlnOffsetFromWwllnFlashMeters 
        {
            get { return Get("MaxDistanceForTlnOffsetFromWwllnFlashMeters", 100000); } 
        }

        public static int MaxDbArchiveProcessorQueueTasks
        {
            get { return Get("MaxDbArchiveProcessorQueueTasks", 5); }
        }

        public static bool EnableGpsDb
        {
            get { return Get("EnableGpsDb", true); }
        }

        public static bool EnableLogDb
        {
            get { return Get("EnableLogDb", true); }
        }

        public static int EcvPort 
        {
            get { return Get("EcvPort", 15555); }
        }

        public static int WwllnListenerRateSampleSize 
        {
            get { return Get("WwllnListenerRateSampleSize", 5); }
        }

        public static double MonitorIncomingSensorConnectionsMinimum 
        {
            get { return Get("MonitorIncomingSensorConnectionsMinimum", 0); }
        }

        public static double MonitorIncomingSensorTcpRateMinimum 
        {
            get { return Get("MonitorIncomingSensorTcpRateMinimum", 0); }
        }

        public static int MonitorIncomingReceiverActiveConnectionsMinimum 
        {
            get { return Get("MonitorIncomingReceiverActiveConnectionsMinimum", 1); }
        }

        public static double MonitorIncomingReceiverTcpRateMinimum 
        {
            get { return Get("MonitorIncomingReceiverTcpRateMinimum", 10); }
        }

        public static double MonitorIncomingWwllnUdpRateMinimum 
        {
            get { return Get("MonitorIncomingWwllnUdpRateMinimum", 3); }
        }

        public static bool IsPlayback
        {
            get { return Get("IsPlayback", false); }
        }

        public static string AwsAccessKey
        {
            get { return Get("AWSAccessKey", string.Empty); }
        }

        public static string AwsSecretAccess
        {
            get { return Get("AWSSecretAccess", string.Empty); }
        }

        public static string ArchiveBasePath
        {
            get { return Get("ArchiveBasePath", "dev.lightning.waveform"); }
        }

        public static ArchiverAction ArchiverAction
        {
            get 
            {
                ArchiverAction ret = ArchiverAction.None;
                string s = Get("ArchiverAction", "None");
                ArchiverAction action;
                if (Enum.TryParse(s, out action))
                {
                    ret = action;
                }
                else
                {
                    EventManager.LogError(TaskMonitor.TidServiceConfigLoadFail, string.Format("Unable to parse config for ArchiverAction, using default value of {0}", ret));
                }
                return ret;
            }
        }

        public static int ArchiverTimeToKeepFilesLocalMinutes 
        {
            get { return Get("ArchiverTimeToKeepFilesLocalMinutes", 15); }
        }

        public static int ArchiverIntervalMinutes
        {
            get { return Get("ArchiverIntervalMinutes", 15); }
        }

        public static int ArchiverS3NumberOfUploadThreads
        {
            get { return Get("ArchiverS3NumberOfUploadThreads", 30); }
        }

        public static bool EnableSensorStatistics
        {
            get { return Get("EnableSensorStatistics", false); }
        }

        public static int SensorStatisticsWriteIntervalMinutes
        {
            get { return Get("SensorStatisticsWriteIntervalMinutes", 5); }
        }

        public static int SensorStatisticsSampleSizeMinutes
        {
            get { return Get("SensorStatisticsSampleSizeMinutes", 30); }
        }

        public static int SensorStatisiticsSaveMaxDegreeOfParallelism
        {
            get { return Get("SensorStatisiticsSaveMaxDegreeOfParallelism", 5); }
        }

        public static bool EnableWwllnTogaSender
        {
            get { return Get("EnableWwllnTogaSender", false); }
        }

        public static int MonitorLastEnqueuedUtcOffsetMaximum 
        {
            get { return Get("MonitorLastEnqueuedUtcOffsetMaximum", 15); }
        }

        public static int MonitorLastDequeuedUtcOffsetMaximum 
        {
            get { return Get("MonitorLastDequeuedUtcOffsetMaximum", 30); }
        }

        public static int ConnectionRateSampleSize 
        {
            get { return Get("ConnectionRateSampleSize", 30); }
        }

        public static int MonitorActiveSensorIntervalSeconds
        {
            get { return Get("MonitorActiveSensorIntervalSeconds", 60); }
        }

        public static int MonitorSensorConnectionRateMaximum 
        {
            get { return Get("MonitorSensorConnectionRateMaximum", 5); }
        }

        public static int MonitorReceiverConnectionRateMaximum
        {
            get { return Get("MonitorReceiverConnectionRateMaximum", 1); }
        }

        public static int MaxWwllnTogaSendWorkerQueueSize 
        {
            get { return Get("MaxWwllnTogaSendWorkerQueueSize", 10000); }
        }

        public static int MonitorIncomingTotalTcpRateMinimum
        {
            get { return Get("MonitorIncomingTotalTcpRateMinimum", 0); }
        }
        
        public static string SystemType
        {
            get { return Get("SystemType", "TLN"); }
        }

        public static int ListenerIdleCheckIntervalSeconds 
        {
            get { return Get("ListenerIdleCheckIntervalSeconds", 30); }
        }

        public static int MaxPacketArriveTimeTooEarlyOrTooLateInSeconds
        {
            get
            {
                return Get("MaxPacketArriveTimeTooEarlyOrTooLateInSeconds",
                           LtgConstants.MaxPacketArriveTimeTooEarlyOrTooLateInSeconds);
            }
        }

        public static string ArchivePathDateFormatString
        {
            get
            {
                return Get("ArchivePathDateFormatString", "/yyyy/MM/dd");
            }
        }
        
    }
}
