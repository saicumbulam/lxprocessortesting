﻿using System.ServiceProcess;
using System.Threading;

namespace LxReceiver
{
    public partial class WinService : ServiceBase
    {
        public WinService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
#if (DEBUG)
            Thread.Sleep(8000);
#endif
            Program.Start();
        }

        protected override void OnStop()
        {
            Program.Stop();
        }
    }
}
