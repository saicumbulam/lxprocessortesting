using System;
using System.ServiceProcess;

using Aws.Core.Utilities;

using LxCommon.Monitoring;

namespace LxReceiver
{
    public class Program
    {
        private static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
            { 
                new WinService() 
            };
            ServiceBase.Run(ServicesToRun);
        }

        private static ProcessController _processController = null;

        public static void Start()
        {
            EventManager.Init();

            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += OnUnhandledException;
            
            _processController = new ProcessController();
            
            _processController.Start();
        }

        public static void Stop()
        {
            if (_processController != null)
                _processController.Stop();

            EventManager.ShutDown();
        }

        private static void OnUnhandledException(object sender, UnhandledExceptionEventArgs args)
        {
            Exception ex = (Exception)args.ExceptionObject;
            EventManager.LogError(TaskMonitor.TidServiceStoppedFail, "Global unhandled exception", ex);
            Stop();
        }
    }
}