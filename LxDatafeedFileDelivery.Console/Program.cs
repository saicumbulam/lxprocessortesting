﻿namespace LxDatafeedFileDelivery.Console
{
    public class Program
    {
        private static void Main(string[] args)
        {
            LxDatafeedFileDelivery.Program.Start();

            System.Console.WriteLine("Press \'q\' to quit the program.");
            while (System.Console.Read() != 'q') { }

            LxDatafeedFileDelivery.Program.Stop();
        }
    }
}
