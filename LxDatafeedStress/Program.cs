﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Aws.Core.Utilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace LxDatafeedStress
{
    public class Program
    {
        
        private static void Main(string[] args)
        {
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
            Program program = new Program(cancellationTokenSource, args[0], args[1], args[2]);

            var task = Task.Run(async () =>
            {
                await program.StartAsync();
            });

            Console.WriteLine("Press \'q\' to quit the program.");
            while (Console.Read() != 'q') { }

            program.Stop(task);
        }
        
        private const string ConnectionString = "{{\"p\":\"{0}\",\"v\":3,\"f\":1,\"t\":1,\"class\":3,\"meta\":true}}";
        private readonly Queue<byte[]> _connectionStringBuffer = new Queue<byte[]>();
        private readonly int _connections;
        private readonly List<DatafeedTcpClient> _clients;
        private readonly IPAddress _ipAddress;
        private readonly int _portNumber;
        private const int BufferSize = 4096;
        private CancellationTokenSource _cancellationTokenSource;

        public Program(CancellationTokenSource cancellationTokenSource, string ip, string port,string csvIds)
        {
            _cancellationTokenSource = cancellationTokenSource;
            _ipAddress = IPAddress.Parse(ip);
            _portNumber = int.Parse(port);
            int connections = 0;
            using (CsvFileReader csv = new CsvFileReader(csvIds, 0))
            {
                CsvRow row = new CsvRow();
                while (csv.ReadRow(row))
                {
                    _connectionStringBuffer.Enqueue(Encoding.ASCII.GetBytes(string.Format(ConnectionString, row[0])));
                    connections++;
                }
            }
            _connections = connections;
            _clients = new List<DatafeedTcpClient>();
        }

        private async Task StartAsync()
        {
            for (int i = 0; i < _connections; i++)
            {
                _clients.Add(new DatafeedTcpClient(BufferSize, i.ToString().PadLeft(10, '0')));
                //await ConnectClient(_clients[i]);
            }

            await _clients.ParallelForEachAsync(Environment.ProcessorCount * 4, ConnectClient, _cancellationTokenSource.Token);
        }

        private async Task ConnectClient(DatafeedTcpClient client)
        {   
            try
            {
                if (!_cancellationTokenSource.IsCancellationRequested)
                {
                    await client.TcpClient.ConnectAsync(_ipAddress, _portNumber);
                    Console.WriteLine($"Connected to: {_ipAddress}:{_portNumber}");

                    NetworkStream stream = client.TcpClient.GetStream();
                    byte[] connectionStringBuffer = _connectionStringBuffer.Dequeue();
                    await stream.WriteAsync(connectionStringBuffer, 0, connectionStringBuffer.Length, _cancellationTokenSource.Token);
                    await stream.FlushAsync(_cancellationTokenSource.Token);
                    Console.WriteLine("Sent connection string");

                    Task.Run(() => Read(client), _cancellationTokenSource.Token);
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                client.Reset();
                if (!_cancellationTokenSource.IsCancellationRequested)
                {
                    Task.Run(() => ConnectClient(client), _cancellationTokenSource.Token);
                };
            }
        }

        public async Task Read(DatafeedTcpClient client)
        {
            using (NetworkStream ns = client.TcpClient.GetStream())
            {
                while (!_cancellationTokenSource.Token.IsCancellationRequested)
                {
                
                    int offset = 0;
                    int bytesRead = 0;
                    try
                    {
                        bytesRead = await ns.ReadAsync(client.ReadBuffer, 0, client.BufferSize, _cancellationTokenSource.Token);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        await ReconnectClient(client);
                        break;
                    }

                    if (bytesRead == 0)
                    {
                        Console.WriteLine("Connection closed");
                        await ReconnectClient(client);
                        break;
                    }

                    int remainingBytes;
                    byte[] buffer = client.GetUnifiedBuffer(bytesRead, out remainingBytes);
                    bool readError = false;
                    while (remainingBytes >= sizeof(int))
                    {
                        int packetLLength = BitConverter.IsLittleEndian ? BigEndianBytesToInt(buffer, offset, sizeof(int)) : BitConverter.ToInt32(buffer, offset);

                        if (remainingBytes >= sizeof(int) + packetLLength)
                        {
                            int index = offset + sizeof(int);
                            string s = Encoding.UTF8.GetString(buffer, index, packetLLength - sizeof(int));
                            try
                            {
                                JObject o = (JObject)JsonConvert.DeserializeObject(s);
                                Console.WriteLine(o.ToString());
                                int type = (int)o["type"];
                                int eeMajor = (int)o["eeMajor"];
                                bool isGood = true;

                                if (type == 40 && eeMajor == 0)
                                {
                                    isGood = true;
                                }

                                if (type != 40 && eeMajor == 0)
                                {
                                    isGood = false;
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                                readError = true;
                                break;
                            }
                            remainingBytes = remainingBytes - packetLLength;
                            offset = offset + packetLLength;
                        }
                        else
                        {
                            break;
                        }
                    }

                    if (readError)
                    {
                        await ReconnectClient(client);
                        break;
                    }

                    if (remainingBytes > 0)
                    {
                        client.CopyToUnreadBuffer(buffer, offset, remainingBytes);
                    }
                }
            }
        }

        private async Task ReconnectClient(DatafeedTcpClient client)
        {
            client.Reset();
            await ConnectClient(client);
        }
        
        public static int BigEndianBytesToInt(byte[] data, int offset, int size)
        {
            if (size > 4)
            {
                throw new ArgumentOutOfRangeException("size", "size cannot be greater than 4");
            }
            if (data.Length < offset + size - 1)
            {
                throw new ArgumentOutOfRangeException("size", "size extends beyond array");
            }

            int value = 0;
            if (size == 1)
            {
                value = data[offset];
            }
            else if (size == 2)
            {
                value = (data[offset] << 8) + data[offset + 1];
            }
            else if (size == 3)
            {
                value = (data[offset] << 16) + (data[offset + 1] << 8) + data[offset + 2];
            }
            else if (size == 4)
            {
                value = (data[offset] << 24) + (data[offset + 1] << 16) + (data[offset + 2] << 8) + data[offset + 3];
            }
            
            return value;
        }
        
        private void Stop(Task task)
        {
            _cancellationTokenSource.Cancel();
            _cancellationTokenSource.Dispose();
            foreach (DatafeedTcpClient client in _clients)
            {
                client.Close();
            }
            task.Wait();
        }
    }

    public static class IEnumerableExtensions
    {
        public static Task ParallelForEachAsync<T>(this IEnumerable<T> source, int dop, Func<T, Task> body, CancellationToken cancellationToken = default(CancellationToken))
        {
            return Task.WhenAll(
                Partitioner.Create(source).GetPartitions(dop).Select(
                    partition => Task.Run(
                        async () =>
                        {
                            using (partition)
                            {
                                while (partition.MoveNext())
                                {
                                    await body(partition.Current);

                                    if (cancellationToken.IsCancellationRequested)
                                    {
                                        break;
                                    }
                                }
                            }
                        }, cancellationToken)
                ));
        }

    }
}
