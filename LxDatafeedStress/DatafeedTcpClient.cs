﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace LxDatafeedStress
{
    public class DatafeedTcpClient
    {
        private byte[] _unreadBuffer;
        private int _unreadCount;
        private byte[] _unified;
        public DatafeedTcpClient(int bufferSize, string id)
        {
            TcpClient = new TcpClient();
            BufferSize = bufferSize;
            Id = id;
            ReadBuffer = new byte[BufferSize];
            _unreadBuffer = new byte[BufferSize];
            _unified = new byte[BufferSize * 2];
            BytesRead = 0;
            _unreadCount = 0;

            if (null != TcpClient.Client && SocketType.Stream == TcpClient.Client.SocketType)
                RemoteEndPoint = TcpClient.Client.RemoteEndPoint;
            else
                RemoteEndPoint = new IPEndPoint(IPAddress.Any, 0);
        }

        public void CopyToUnreadBuffer(byte[] buffer, int offset, int unreadBytes)
        {
            Buffer.BlockCopy(buffer, offset, _unreadBuffer, _unreadCount, unreadBytes);
            _unreadCount = _unreadCount + unreadBytes; 
        }

        public byte[] GetUnifiedBuffer(int readBufferBytesRead, out int byteCount)
        {
            int size = _unreadCount + readBufferBytesRead;
            if (_unified.Length < size)
            {
                _unified = new byte[size];
            }
            byteCount = 0;
            if (_unreadCount > 0)
            {
                //copy the previous buffer;
                Buffer.BlockCopy(_unreadBuffer, 0, _unified, 0, _unreadCount);
                byteCount = _unreadCount;
                
                //reset the unread buffer
                _unreadCount = 0;
                
            }
            //copy the newly read buffer
            Buffer.BlockCopy(ReadBuffer, 0, _unified, byteCount, readBufferBytesRead);
            byteCount = byteCount + readBufferBytesRead;

            return _unified;
        }

        public void Reset()
        {
            //CloseClient();
            TcpClient = new TcpClient();
        }

        public void Close()
        {
            if (TcpClient.Connected)
            {
                TcpClient.Close();
            }
        }

        public byte[] ReadBuffer { get; }

        public int BufferSize { get; set; }

        public int BytesRead { get; set; }

        public string Id { get; set; }

        public EndPoint RemoteEndPoint { get; }

        public string RemoteIpAddress => ((IPEndPoint)RemoteEndPoint).Address.ToString();

        public TcpClient TcpClient { get; set; }
   }
}
