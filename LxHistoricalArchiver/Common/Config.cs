﻿using System.IO;
using Aws.Core.Utilities.Config;

namespace LxHistoricalArchiver.Common
{
    public class Config : AppConfig
    {
        public static string S3Bucket
        {
            get { return Get<string>("S3Bucket", null); }
        }

        public static string ProdS3Bucket
        {
            get { return Get<string>("ProdS3Bucket"); }
        }

        public static string ProdAWSAccessKey
        {
            get { return Get<string>("ProdAWSAccessKey"); }
        }

        public static string ProdAWSSecretKey
        {
            get { return Get<string>("ProdAWSSecretKey"); }
        }

        public static string DataMigrationBaseDirectory
        {
            get { return Get("DataMigrationBaseDirectory", @"C:\LxHistoricalArchive"); }
        }

        public static string DataMigrationLogDirectory
        {
            get { return Path.Combine(DataMigrationBaseDirectory, Get("DataMigrationLogDirectory", "Logs")); }
        }

        public static string DataMigrationTransferDirectory
        {
            get { return Path.Combine(DataMigrationBaseDirectory, Get("DataMigrationTransferDirectory", "Transfer")); }
        }

        public static string DataMigrationStagingDirectory
        {
            get { return Path.Combine(DataMigrationBaseDirectory, Get("DataMigrationStagingDirectory", "Staging")); }
        }

        public static int MaxStagingFileSizeMB
        {
            get { return Get("MaxStagingFileSizeMB", 100); }
        }
    }
}
