﻿using System;
using System.Collections.Generic;
using LxCommon.Models;
using LxCommon.Utilities;
using LxHistoricalArchiver.Common;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.Common;
using Newtonsoft.Json;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using LxCommon;

namespace LxHistoricalArchiver.Components
{
    internal class DataReader
    {
        private readonly DataWriter _dataWriter;
        private readonly StatusManager _statusManger;

        private const string WwllnSource = "wwlln";
        private const string TlnSource = "tln";

        public DataReader()
        {
            _dataWriter = new DataWriter();
            _statusManger = StatusManager.GetInstance();
        }

        public void RetrieveData(DateTime startDate, DateTime endDate, bool force = false)
        {
            CreateDirectories();

            foreach (var day in EachDay(startDate, endDate))
            {
                Console.WriteLine($"{DateTime.Now}: Running for date(yyyymmdd) : {day.Year:D4}{day.Month:D2}{day.Day:D2}. Force: {force}");
                RetrieveDay(day.Year, day.Month, day.Day, force);
            }

            Console.WriteLine($"{DateTime.Now}: DATA MIGRATION COMPLETE!!!");
        }

        private void RetrieveDay(int year, int month, int day, bool force = false)
        {
            var dayToProcessStart = new DateTime(year, month, day);
            var dayToProcessEnd = new DateTime(year, month, day).AddDays(1);

            var retrieveStartTime = DateTime.Now;

            if (force || !_statusManger.IsProcessed(dayToProcessStart, dayToProcessEnd))
            {
                bool? hasDayException = null;
                var yearMonth = year*100 + month;
                var connectionString = string.Format(ConfigurationManager.ConnectionStrings["LxArchive"].ConnectionString, year);

                //retrieve day's data in 1 hour intervals
                for (var hr = 0; hr <= 23; hr += 1)
                {
                    var hourToProcessStart = new DateTime(year, month, day).AddHours(hr);
                    var hourToProcessEnd = hourToProcessStart.AddHours(1);
                    bool? hasHourException = null;

                    if (!force && _statusManger.IsProcessed(hourToProcessStart, hourToProcessEnd))
                    {
                        Console.WriteLine($"{DateTime.Now}: Skipping hour as time range has already been processed. Start: {hourToProcessStart}, End: {hourToProcessEnd} ");
                        continue;
                    }

                    try
                    {
                        #region Minutes

                        var addMinutes = 60;
                        for (var minute = 0; minute < 60; minute += addMinutes)
                        {
                            var minuteToProcessStart = new DateTime(year, month, day).AddHours(hr).AddMinutes(minute);
                            var minuteToProcessEnd = minuteToProcessStart.AddMinutes(addMinutes);
                            if (!force && _statusManger.IsProcessed(minuteToProcessStart, minuteToProcessEnd))
                            {
                                Console.WriteLine($"{DateTime.Now}: Skipping minute as time range has already been processed. Start: {minuteToProcessStart}, End: {minuteToProcessEnd}");
                                continue;
                            }

                            Console.WriteLine($"{DateTime.Now}: Start purging files for Start: {minuteToProcessStart}, End: {minuteToProcessEnd}");
                            foreach (var m in EachMinute(minuteToProcessStart, minuteToProcessEnd))
                            {
                                //Console.WriteLine($"{DateTime.Now}: Start purging files for {m}");
                                PurgeFilesByPattern(Config.DataMigrationStagingDirectory, new Regex($"\\d+-{m.Year:D4}-{m.Month:D2}-{m.Day:D2}T{m.Hour:D2}.{m.Minute:D2}.*"));
                                //Console.WriteLine($"{DateTime.Now}: Finished purging files for {m}");
                            }
                            Console.WriteLine($"{DateTime.Now}: Finished purging files for Start: {minuteToProcessStart}, End: {minuteToProcessEnd}");

                            var hasMinuteException = false;
                            var intervalStartTime = DateTime.Now;

                            var startTimeString = GenerateSqlTimeString(year, month, day, hr, minute);
                            var endTimeString = GenerateSqlTimeString(year, month, day, hr, minute + addMinutes);

                            var query = CreateQueryStatement(startTimeString, endTimeString, yearMonth);
                            var conn = new SqlConnection(connectionString);
                            var dr = default(SqlDataReader);

                            Console.WriteLine($"{DateTime.Now}: Start data processing for Start: {startTimeString}; End {endTimeString}");

                            var portions = new Dictionary<Guid, List<Portion>>();
                            try
                            {
                                conn.Open();
                                using (var comm = new SqlCommand("SET ARITHABORT ON", conn))
                                {
                                    comm.ExecuteNonQuery();
                                }

                                var command = new SqlCommand(query, conn)
                                {
                                    CommandTimeout = 7200 //2 hour timeout
                                };

                                var dbQueryStartTime = DateTime.Now;
                                Console.WriteLine($"{DateTime.Now}: Start requesting data from DB at {dbQueryStartTime}. Start: {startTimeString}; End {endTimeString}");

                                try
                                {
                                    dr = command.ExecuteReader();
                                }
                                catch (SqlException ex)
                                {
                                    hasMinuteException = true;
                                    Console.WriteLine($"{DateTime.Now}: [ERROR] SQL command failed: {ex}");
                                }
                                var dbQueryEndTime = DateTime.Now;
                                Console.WriteLine($"{DateTime.Now}: Finished requesting data from DB at {dbQueryEndTime}. Start: {startTimeString}; End {endTimeString}");
                                Console.WriteLine($"{DateTime.Now}: Total time taken for requesting data from DB {dbQueryEndTime.Subtract(dbQueryStartTime).TotalSeconds} seconds");

                                if (dr != null && dr.HasRows)
                                {
                                    // Process records from DB
                                    Console.WriteLine($"{DateTime.Now}: Start processing DB records for time time range: {startTimeString} - {endTimeString}");
                                    portions = ProcessDbRecords(dr);
                                    Console.WriteLine($"{DateTime.Now}: End processing DB records for time time range: {startTimeString} - {endTimeString}");
                                }
                                else if (dr != null && !dr.HasRows)
                                {
                                    Console.WriteLine($"{DateTime.Now}: [WARN] No DB records returned for time time range: {startTimeString} - {endTimeString}");
                                    Console.WriteLine($"{DateTime.Now}: Query: {query}");
                                }
                                else
                                {
                                    Console.WriteLine($"{DateTime.Now}: [ERROR] Failed to retrieve DB records for time time range: {startTimeString} - {endTimeString}");
                                    Console.WriteLine($"{DateTime.Now}: Query: {query}");
                                }

                                dr?.Close(); dr = null;
                                conn?.Close(); conn = null;

                                // some time periods have no data
                                if (portions.Count > 0)
                                {
                                    var flashes = ProcessFlashData(portions);
                                    _dataWriter.Write(flashes);
                                }

                                hasMinuteException = false;
                            }
                            catch (Exception ex)
                            {
                                hasMinuteException = true;
                                Console.WriteLine($"{DateTime.Now}: [ERROR] Caught exception while processing data for time time range: {startTimeString} - {endTimeString}: {ex}");
                            }
                            finally
                            {
                                dr?.Close();
                                conn?.Close();

                                if (!hasMinuteException)
                                    _statusManger.SetProcessed(minuteToProcessStart, minuteToProcessEnd);

                                Console.WriteLine($"{DateTime.Now}: Processing time: {DateTime.Now.Subtract(intervalStartTime).TotalSeconds} seconds");
                            } //end try catch finally

                            if (hasMinuteException)
                            {
                                hasHourException = true;
                                hasDayException = true;
                            }

                            Console.WriteLine($"{DateTime.Now}: End data processing for Start: {startTimeString}; End {endTimeString}");

                        } //end loop - minute

                        #endregion
                    }
                    catch (Exception ex)
                    {
                        hasHourException = true;
                        hasDayException = true;
                        Console.WriteLine($"{DateTime.Now}: [ERROR] Caught exception processing {hourToProcessStart} to {hourToProcessEnd}: {ex.Message}");
                    }

                    if (!hasHourException.HasValue || !hasHourException.Value)
                        _statusManger.SetProcessed(hourToProcessStart, hourToProcessEnd);

                    if (!hasDayException.HasValue && hasHourException.HasValue && hasHourException.Value)
                        hasDayException = true;

                } //end loop - hour

                if (!hasDayException.HasValue || !hasDayException.Value)
                {
                    _statusManger.SetProcessed(dayToProcessStart, dayToProcessEnd);

                    Console.WriteLine($"{DateTime.Now}: Moving tracked files to transfer folder");
                    _dataWriter.MoveTrackedFilesToTranfer();

                    Console.WriteLine($"{DateTime.Now}: Moving remaining files for {dayToProcessStart} to transfer folder");
                    _dataWriter.MoveFilesToTranferByDate(dayToProcessStart);

                    Console.WriteLine($"{DateTime.Now}: Writing files in transfer folder for {dayToProcessStart} to Cloud");
                    _dataWriter.MoveFilesToCloudByDate(dayToProcessStart);
                }
                else
                {
                    Console.WriteLine($"{DateTime.Now}: [ERROR] Day {dayToProcessStart} has exception. Not moving tracked or remaining files to transfer folder");
                    _dataWriter.PurgeTrackedFiles();
                }
            }
            else
            { 
                Console.WriteLine($"{DateTime.Now}: [WARN] Skipping day as time range has already been processed. Start: {dayToProcessStart}, End: {dayToProcessEnd} ");

                Console.WriteLine($"{DateTime.Now}: Moving any remaining files for {dayToProcessStart} to transfer folder");
                _dataWriter.MoveFilesToTranferByDate(dayToProcessStart);

                Console.WriteLine($"{DateTime.Now}: Writing any remaining files in transfer folder to Cloud");
                _dataWriter.MoveFilesToCloudByDate(dayToProcessStart);
            }

            Console.WriteLine($"{DateTime.Now}: Total processing time: {DateTime.Now.Subtract(retrieveStartTime).TotalMinutes} minutes");
        }

        private Dictionary<Guid, List<Portion>> ProcessDbRecords(DbDataReader dr)
        {
            Console.WriteLine($"{DateTime.Now}: Start processing DB data");

            Console.WriteLine($"{DateTime.Now}: Start caching field names");
            var columnNames = new Dictionary<string, int>();
            for (var i = 0; i < dr.FieldCount; i++)
            {
                var fieldName = dr.GetName(i);
                columnNames.Add(fieldName, dr.GetOrdinal(fieldName));
            }
            Console.WriteLine($"{DateTime.Now}: End caching field names");

            //read through queried data
            Console.WriteLine($"{DateTime.Now}: Start processing records");
            var portions = new Dictionary<Guid, List<Portion>>();
            var recordsProcessed = (long)0;
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    var data = (IDataRecord)dr;

                    var portion = CreatePortion(data, columnNames);
                    if (portion.FlashGUID.HasValue)
                    {
                        var flashGuid = portion.FlashGUID.Value;

                        if (!portions.ContainsKey(flashGuid))
                        {
                            portions.Add(flashGuid, new List<Portion>());
                        }

                        portions[flashGuid].Add(portion);
                    }
                    Console.Write(".");
                    recordsProcessed++;
                }
                Console.WriteLine("");
            }
            else
                Console.WriteLine($"{DateTime.Now}: No records were returned!");

            Console.WriteLine($"{DateTime.Now}: Start sorting records");
            foreach (var kvPair in portions)
            {
                portions[kvPair.Key].Sort((x, y) => LtgTimeUtils.GetUtcDateTimeFromString(x.TimeStamp)
                                                    .CompareTo(LtgTimeUtils.GetUtcDateTimeFromString(y.TimeStamp)));
            }
            Console.WriteLine($"{DateTime.Now}: End sorting records");
            Console.WriteLine($"{DateTime.Now}: End processing records: {recordsProcessed}");
            Console.WriteLine($"{DateTime.Now}: End processing DB data");
            return portions;
        }

        private Dictionary<string, List<Flash>> ProcessFlashData(Dictionary<Guid, List<Portion>> portions)
        {
            Console.WriteLine($"{DateTime.Now}: Start processing flash data");
            var flashesProcessed = (long)0;
            var flashes = new Dictionary<string, List<Flash>>();

            // create flashes based on portion lists
            foreach (var kvPair in portions)
            {
                var createdFlash = CreateFlash(kvPair.Value);
                var quadkey = GetQuadKey(createdFlash);

                if (!flashes.ContainsKey(quadkey))
                    flashes.Add(quadkey, new List<Flash>());

                createdFlash.Portions = kvPair.Value;

                flashes[quadkey].Add(createdFlash);
                flashesProcessed++;
            }

            Console.WriteLine($"{DateTime.Now}: End processing flash data: {flashesProcessed}");

            return flashes;
        }
       
        private IEnumerable<DateTime> EachDay(DateTime from, DateTime thru)
        {
            for (var day = from; day <= thru; day = day.AddDays(1))
                yield return day;
        }

        private IEnumerable<DateTime> EachHour(DateTime from, DateTime thru)
        {
            for (var hour = from; hour <= thru; hour = hour.AddHours(1))
                yield return hour;
        }

        private IEnumerable<DateTime> EachMinute(DateTime from, DateTime thru)
        {
            for (var minute = from; minute <= thru; minute = minute.AddMinutes(1))
                yield return minute;
        }

        //returns the quad key of a Flash
        private string GetQuadKey(Flash flash)
        {
            var quadkey = "";
            var LevelOfDetail = 5;

            var latitude = flash.Latitude; var longitude = flash.Longitude;

            var pixelX = 0;
            var pixelY = 0;
            TileSystem.LatLongToPixelXY(latitude, longitude, out pixelX, out pixelY, LevelOfDetail);

            var tileX = 0;
            var tileY = 0;
            TileSystem.PixelXYToTileXY(pixelX, pixelY, out tileX, out tileY);

            quadkey = TileSystem.TileXYToQuadKey(tileX, tileY, LevelOfDetail);

            return quadkey;
        }

        private string GenerateSqlTimeString(int year, int month, int day, int hour, int minute)
        {
            var ret = new DateTime(year, month, day, hour, 0, 0).AddMinutes(minute);
            return $"{ret.Year:D4}-{ret.Month:D2}-{ret.Day:D2} {ret.Hour:D2}:{ret.Minute:D2}:00";
        }

        private string CreateQueryStatement(string start, string end, int yearmonth)
        {
            var query =
                "SELECT Flash.Flash_History_ID ,Flash.FlashGUID ,Flash.Lightning_Time ,Flash.Lightning_Time_String ,Flash.Latitude" +
                ",Flash.Longitude ,Flash.Height ,Flash.Stroke_Type ,Flash.Amplitude ,Flash.Stroke_Solution" +
                ",Flash.Confidence ,Flash.LastModifiedTime ,Flash.LastModifiedBy" +
                ",Portions.FlashPortionGUID AS FlashPortions_FlashPortionGUID ,Portions.Lightning_Time_String AS FlashPortions_Lightning_Time_String" +
                ",Portions.Latitude AS FlashPortions_Latitude ,Portions.Longitude AS FlashPortions_Longitude,Portions.Height AS FlashPortions_Height, Portions.Stroke_Solution AS FlashPortions_Stroke_Solution" +
                ",Portions.Confidence AS FlashPortions_Confidence, Portions.LastModifiedTime AS FlashPortions_LastModifiedTime ,Portions.Amplitude AS FlashPortions_Amplitude " +
                ",Portions.LastModifiedBy AS FlashPortions_LastModifiedBy ,Portions.[Offsets] AS FlashPortions_Offsets ,Portions.Stroke_Type AS FlashPortions_Stroke_Type " +
                $" FROM dbo.LtgFlash_History{yearmonth} AS Flash WITH (NOLOCK) INNER JOIN dbo.LtgFlashPortions_History{yearmonth} as Portions WITH (NOLOCK)" +
                " ON Flash.FlashGUID = Portions.FlashGUID" +
                $" WHERE Flash.Lightning_Time >= \'{start}\'" +
                $" AND Flash.Lightning_Time < \'{end}\'";
                //" ORDER BY Flash.Lightning_Time ASC;"

            //var flashData = "LtgFlash_History" + yearmonth;
            //var flashportionData = "LtgFlashPortions_History" + yearmonth;

            //query = query.Replace("LtgFlash", flashData);
            //query = query.Replace("portions", flashportionData);

            return query;
        }

        //create Portion object
        private Portion CreatePortion(IDataRecord data, Dictionary<string, int> columnNames)
        {
            var portion = new Portion
            {
                FlashGUID = data.GetGuid(columnNames["FlashGUID"]),
                FlashId = data.GetGuid(columnNames["FlashPortions_FlashPortionGUID"]),
                TimeStamp = data.GetString(columnNames["FlashPortions_Lightning_Time_String"]),
                Latitude = data.GetDouble(columnNames["FlashPortions_Latitude"]),
                Longitude = data.GetDouble(columnNames["FlashPortions_Longitude"]),
                Amplitude = data.GetDouble(columnNames["FlashPortions_Amplitude"]),
                Height = data.GetDouble(columnNames["FlashPortions_Height"]),
                Solution = data.GetString(columnNames["FlashPortions_Stroke_Solution"]),
                Confidence = data.GetInt32(columnNames["FlashPortions_Confidence"]),
                Offsets = data.GetString(columnNames["FlashPortions_Offsets"]),
                Type = (FlashType)data.GetInt32(columnNames["FlashPortions_Stroke_Type"])
            };
            portion.TimeStamp = portion.TimeStamp.Trim();

            // deserialize if Solution is json
            if (!string.IsNullOrWhiteSpace(portion.Solution)
                 && portion.Solution.Contains("ee")
                 && portion.Solution.Contains("v")
                 && portion.Solution.Contains("so"))
            {
                try
                {
                    var ped = JsonConvert.DeserializeObject<PortionExtendedData>(portion.Solution);
                    portion.ErrorEllipse = ped.ErrorEllipse;
                    portion.StationOffsets = ped.StationOffsets;
                    portion.Version = ped.Version;
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"{DateTime.Now}: [WARN] CreatePortion caught exception processing 'solution' property: {ex}");
                }
            }

            // Remove Solution & Offsets (if needed)
            portion.Solution = null;
            if (string.IsNullOrWhiteSpace(portion.Offsets)) portion.Offsets = null;

            return portion;
        }

        //create Flash object
        private Flash CreateFlash(List<Portion> portions)
        {
            var toReturn = new Flash
            {
                Latitude = 0,
                Longitude = 0,
                Height = 0,
                CgMultiplicity = 0,
                IcMultiplicity = 0,
                DurationSeconds = 0,
                Type = FlashType.FlashTypeIC
            };


            Portion brightestIc = null;
            Portion brightestCg = null;
            var uniqueSensors = new HashSet<string>();
            Portion startPortion = null;
            Portion endPortion = null;

            if (portions != null)
            {
                if (portions.Count == 1 && ((Portion)portions[0]).Type == FlashType.FlashTypeGlobalCG)
                {
                    brightestCg = ((Portion)portions[0]);
                    toReturn.Type = FlashType.FlashTypeGlobalCG;
                    if (brightestCg.Offsets != null && brightestCg.Offsets.Length > 0)
                    {
                        var OffsetList = brightestCg.Offsets.Split(',');

                        foreach (var offset in OffsetList)
                        {
                            if (offset != null && offset.Length > 0)
                            {
                                var l = offset.IndexOf("=");
                                var id = offset;

                                if (l > 0)
                                {
                                    id = offset.Substring(0, l);
                                }

                                uniqueSensors.Add(id);
                            }
                        }
                    }
                    else if (brightestCg.StationOffsets != null)
                    {
                        foreach (var offset in brightestCg.StationOffsets)
                        {
                            uniqueSensors.Add(offset.Key);
                        }
                    }

                    toReturn.CgMultiplicity = 1;
                    toReturn.Source = WwllnSource;
                    startPortion = brightestCg;
                    endPortion = brightestCg;
                    toReturn.MinLatitude = brightestCg.Latitude;
                    toReturn.MaxLatitude = brightestCg.Latitude;
                    toReturn.MinLongitude = brightestCg.Longitude;
                    toReturn.MaxLongitude = brightestCg.Longitude;
                }
                else
                {
                    var initialized = false;
                    var count = 0;
                    while (count < portions.Count)
                    {
                        var portion = portions[count] as Portion;

                        if (portion != null)
                        {
                            if (!initialized)
                            {
                                startPortion = portion;
                                endPortion = portion;
                                toReturn.MinLatitude = portion.Latitude;
                                toReturn.MaxLatitude = portion.Latitude;
                                toReturn.MinLongitude = portion.Longitude;
                                toReturn.MaxLongitude = portion.Longitude;
                                toReturn.Version = portion.Version;
                                initialized = true;
                                toReturn.Source = TlnSource;
                            }
                            else
                            {
                                if (portion.TimeStamp.CompareTo(startPortion.TimeStamp) < 0) startPortion = portion;
                                if (portion.TimeStamp.CompareTo(endPortion.TimeStamp) > 0) endPortion = portion;
                                if (portion.Latitude < toReturn.MinLatitude) toReturn.MinLatitude = portion.Latitude;
                                if (portion.Latitude > toReturn.MaxLatitude) toReturn.MaxLatitude = portion.Latitude;
                                if (portion.Longitude < toReturn.MinLongitude) toReturn.MinLongitude = portion.Longitude;
                                if (portion.Longitude > toReturn.MaxLongitude) toReturn.MaxLongitude = portion.Longitude;
                            }

                            if (portion.Type == FlashType.FlashTypeCG)
                            {
                                portion.Height = 0;
                                toReturn.Type = FlashType.FlashTypeCG;
                                if (brightestCg == null || (Math.Abs(portion.Amplitude) > Math.Abs(brightestCg.Amplitude)))
                                {
                                    brightestCg = portion;
                                }
                                toReturn.CgMultiplicity++;
                            }
                            else if (portion.Type == FlashType.FlashTypeIC)
                            {
                                if (portion.Height < 0)
                                {
                                    portion.Height *= -1;
                                }

                                if (brightestIc == null || (Math.Abs(portion.Amplitude) > Math.Abs(brightestIc.Amplitude)))
                                {
                                    brightestIc = portion;
                                }

                                toReturn.IcMultiplicity++;
                            }

                            if (portion.Offsets != null)
                            {
                                var OffsetList = portion.Offsets.Split(',');

                                foreach (var offset in OffsetList)
                                {
                                    if (!string.IsNullOrWhiteSpace(offset))
                                    {
                                        var l = offset.IndexOf("=", StringComparison.Ordinal);
                                        var id = offset;

                                        if (l > 0)
                                        {
                                            id = offset.Substring(0, l);
                                        }

                                        uniqueSensors.Add(id);
                                    }
                                }
                            }
                            else if (portion.StationOffsets != null)
                            {
                                foreach (var offset in portion.StationOffsets)
                                {
                                    uniqueSensors.Add(offset.Key);
                                }
                            }
                        }

                        count++;
                    }
                }

                if (startPortion != null)
                {
                    if (toReturn.IcMultiplicity > 1 || toReturn.CgMultiplicity > 1)
                    {
                        toReturn.StartTime = startPortion.TimeStamp;
                        toReturn.EndTime = endPortion.TimeStamp;

                        var x = Convert.ToDateTime(toReturn.EndTime) - Convert.ToDateTime(toReturn.StartTime);
                        toReturn.DurationSeconds = (decimal)x.Ticks / TimeSpan.TicksPerSecond;
                    }
                    else
                    {
                        toReturn.StartTime = startPortion.TimeStamp;
                        toReturn.EndTime = endPortion.TimeStamp;
                    }
                }

                toReturn.NumberSensors = uniqueSensors.Count;

                if ((toReturn.Type == FlashType.FlashTypeCG || toReturn.Type == FlashType.FlashTypeGlobalCG) && brightestCg != null)
                {
                    toReturn.Latitude = brightestCg.Latitude;
                    toReturn.Longitude = brightestCg.Longitude;
                    toReturn.Height = 0;
                    toReturn.Amplitude = brightestCg.Amplitude;
                    toReturn.TimeStamp = brightestCg.TimeStamp;
                    toReturn.Confidence = brightestCg.Confidence;

                }
                else if (toReturn.Type == FlashType.FlashTypeIC && brightestIc != null)
                {
                    toReturn.Latitude = brightestIc.Latitude;
                    toReturn.Longitude = brightestIc.Longitude;
                    toReturn.Height = brightestIc.Height;
                    toReturn.Amplitude = brightestIc.Amplitude;
                    toReturn.TimeStamp = brightestIc.TimeStamp;
                    toReturn.Confidence = brightestIc.Confidence;
                }
            }

            return toReturn;
        }

        private void PurgeFilesByPattern(string directoryPath, Regex pattern)
        {
            var files = Directory.GetFiles(directoryPath, "*").Where(path => pattern.IsMatch(path)).ToList();
            if (!(files?.Count > 0)) return;

            Console.WriteLine($"{DateTime.Now}: Start purging files from {directoryPath} matching pattern {pattern}");
            foreach (var file in files)
            {
                Console.WriteLine($"{DateTime.Now}: Deleting file {file}");
                File.Delete(file);
            }
            Console.WriteLine($"{DateTime.Now}: Finished purging files from {directoryPath} matching pattern {pattern}");
        }

        private void CreateDirectories()
        {
            var baseDirectoryPath = Config.DataMigrationBaseDirectory;
            if (!Directory.Exists(baseDirectoryPath))
                Directory.CreateDirectory(baseDirectoryPath);
            
            var logDirectoryPath = Config.DataMigrationLogDirectory;
            if (!Directory.Exists(logDirectoryPath))
                Directory.CreateDirectory(logDirectoryPath);

            var stagingDirectoryPath = Config.DataMigrationStagingDirectory;
            if (!Directory.Exists(stagingDirectoryPath))
                Directory.CreateDirectory(stagingDirectoryPath);

            var transferDirectoryPath = Config.DataMigrationTransferDirectory;
            if (!Directory.Exists(transferDirectoryPath))
                Directory.CreateDirectory(transferDirectoryPath);
        }
    }
}
