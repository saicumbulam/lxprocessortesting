﻿using System;
using System.Threading.Tasks;
using System.IO;

using En.Ods.Lib.Common.Dynamo;
using En.Ods.Lib.Common.Interface;
using En.Ods.Lib.Common.Serialization;

namespace LxHistoricalArchiver.Components
{
    public class CloudWriter
    {
        private readonly ICloudStorage _storage;
        private readonly string _s3Bucket;
        private readonly object _lockthis = new object();

        public CloudWriter(string s3Bucket)
        {
           
            if (string.IsNullOrWhiteSpace(s3Bucket))
            {
                throw new ArgumentNullException("s3Bucket");
            }
            _s3Bucket = s3Bucket;

            IOdsValue credentials = new JsonOdsValue();
            _storage = new S3Factory().GetStorage(credentials);

            System.Object lockThis = new System.Object();
            
        }

        public async Task WriteFileToCloudAsync(string localPath, DateTime queryDate)
        {
            await Task.Delay(500);

            var pathOnS3 = "";

            try
            {
                Console.WriteLine($"{DateTime.Now}: Start writing file {localPath} to Cloud");

                lock (_lockthis)
                {
                    pathOnS3 = GetCloudPath(localPath, queryDate);
                }

                var t = Task.Run(async () =>
                {
                    using (var fileStream = new FileStream(localPath, FileMode.Open, FileAccess.Read))
                    {
                        Console.WriteLine($"{DateTime.Now}: Start writing file {localPath} to {_s3Bucket}/{pathOnS3}");
                        await _storage.PutObjectAsync(_s3Bucket, pathOnS3, fileStream);
                        Console.WriteLine($"{DateTime.Now}: Finished writing file {localPath} to {_s3Bucket}/{pathOnS3}");
                    }
                });
                t.Wait();

                if (t.IsFaulted && t.Exception != null)
                {
                    throw t.Exception;
                }

                Console.WriteLine($"{DateTime.Now}: Finished writing file {localPath} to Cloud");
                File.Delete(localPath);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"{DateTime.Now}: [ERROR] Failed to write file {localPath} to {_s3Bucket}/{pathOnS3} due to an exception: {ex}");
            }
        }

        public string GetCloudPath(string localPath, DateTime queryDate)
        {
            var s3Filename = Path.GetFileName(localPath);   //quadkey-year-month-day-hour-month-second
                s3Filename = s3Filename.Remove(s3Filename.IndexOf(".", StringComparison.Ordinal));
            var quadkey = s3Filename.Substring(0, s3Filename.IndexOf("-", StringComparison.Ordinal));
            var year = queryDate.Year.ToString();
            var month = queryDate.Month.ToString();

            var s3 = $"{quadkey}/{year}/{month}/{s3Filename}.json";
            return s3;
        }

    }
}
