﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.Globalization;
using System.Text.RegularExpressions;
using LxCommon.Models;

using Newtonsoft.Json;
using LxHistoricalArchiver.Common;

namespace LxHistoricalArchiver.Components
{
    public class DataWriter
    {
        private readonly Dictionary<string, Stack<string>> _fileTracker;   //<quadkey, List<local path>>
        private readonly CloudWriter _cw;
        private readonly string _transferDirectoryPath;
        private readonly string _stagingDirectoryPath;
        private readonly long _maxFileSizeByte;

        public DataWriter()
        {
            _maxFileSizeByte = (Config.MaxStagingFileSizeMB * 1024) * 1024;
            _fileTracker = new Dictionary<string, Stack<string>>();
            _cw = new CloudWriter(Config.S3Bucket);
            _stagingDirectoryPath = Config.DataMigrationStagingDirectory;
            _transferDirectoryPath = Config.DataMigrationTransferDirectory;
        }

        public void Write(Dictionary<string, List<Flash>> dataHolder)
        {
            if (dataHolder?.Count == 0) return;

            Console.WriteLine($"{DateTime.Now}: Start writing data to file in staging folder {_stagingDirectoryPath}");

            foreach (var kvPair in dataHolder)
            {
                var quadKey = kvPair.Key;
                var flashes = kvPair.Value;

                if (flashes?.Count > 0)
                {
                    Console.WriteLine($"{DateTime.Now}: Start writing {flashes.Count} flashes for quadKey: {quadKey}");
                    StageDataInFilesystem(flashes, quadKey);
                    Console.WriteLine($"{DateTime.Now}: Finished writing {flashes.Count} flashes for quadKey: {quadKey}");
                }
            }
            Console.WriteLine($"{DateTime.Now}: Finished writing files to staging folder {_stagingDirectoryPath}");
        }

        private void StageDataInFilesystem(IReadOnlyCollection<Flash> flashes, string quadKey)
        {
            if (flashes?.Count < 1) return;

            var filePath = GetOutputFilePath(quadKey, flashes.FirstOrDefault());
            var fileStream = File.AppendText(filePath);

            Console.WriteLine($"{DateTime.Now}: Start writing flashes to {filePath} for {quadKey}");
            try
            {
                foreach (var flash in flashes)
                {
                    var isFlashAndFileMatch = IsFlashValidForFile(quadKey, flash, filePath);
                    var isFileMaxed = (fileStream.BaseStream.Length >= _maxFileSizeByte);

                    if (!isFlashAndFileMatch || isFileMaxed)
                    {
                        Console.WriteLine(isFileMaxed
                                            ? $"{DateTime.Now}: {filePath} exceeds max file size. Creating new file."
                                            : $"{DateTime.Now}: {filePath} does not match flash date {flash.TimeStamp}. Creating new file."
                                         );
                        fileStream.Flush();
                        fileStream.Close();
                        fileStream.Dispose();

                        filePath = GetOutputFilePath(quadKey, flash, true);
                        fileStream = File.AppendText(filePath);
                        Console.WriteLine($"{DateTime.Now}: New file {filePath} created for {quadKey}");
                    }

                    fileStream.WriteLine(JsonConvert.SerializeObject(flash, Formatting.None));
                    fileStream.Flush();
                }
            }
            finally
            {
                fileStream?.Flush();
                fileStream?.Close();
                fileStream?.Dispose();
            }
            Console.WriteLine($"{DateTime.Now}: Finished writing flashes to {filePath} for {quadKey}");
        }

        private bool IsFlashValidForFile(string quadKey, Flash flash, string existingFile)
        {
            var isMatch = false;

            var existingFileInfo = new FileInfo(existingFile);

            try
            {
                var flashDate = DateTime.Parse(flash.TimeStamp);

                //format: quadkey-YYYY-MM-DDTHH-MM-SS.json
                //eample: 31000-2016-04-06T23-59-59.999076000.json
                var existingFileDateInfo = existingFileInfo.Name.Replace($"{quadKey}-", string.Empty)
                                                                .Replace(existingFileInfo.Extension, string.Empty)
                                                                .Substring(0, 10);
                //now: 2016-04-06
                var existingFileDate = DateTime.Parse(existingFileDateInfo);

                isMatch = existingFileDate.Date.Equals(flashDate.Date);
            }
            catch (Exception)
            {
                Console.WriteLine($"{DateTime.Now}: Caught exception while comparing Flash and File date: File: {existingFile}, Flash: ({flash.TimeStamp})");
            }

            return isMatch;
        }

        private string GetOutputFilePath(string quadKey, Flash flash, bool createNew = false)
        {
            string path;
            
            if (createNew || !_fileTracker.ContainsKey(quadKey))
            {
                // remove colons from date --- colons can't be used in file names
                var datestring = flash.TimeStamp.Replace(":", "-");
                path = Path.Combine(_stagingDirectoryPath, $"{quadKey}-{datestring}.json").Trim();
                Console.WriteLine($"{DateTime.Now}: Creating new file for {quadKey}: {path}");
                File.Create(path).Close();

                if (!_fileTracker.ContainsKey(quadKey))
                    _fileTracker.Add(quadKey, new Stack<string>());
                _fileTracker[quadKey].Push(path);
            }
            else
            {
                path = _fileTracker[quadKey].Peek();

                if (!IsFlashValidForFile(quadKey, flash, path))
                {
                    Console.WriteLine($"{DateTime.Now}: Flash date {flash.TimeStamp} for quadKey {quadKey} does not match file {path}. Calling GetOutputFilePath recursively");
                    path = GetOutputFilePath(quadKey, flash, true);
                }
            }

            return path;
        }

        public void MoveTrackedFilesToTranfer()
        {
            Console.WriteLine($"{DateTime.Now}: Start moving tracked files to transfer folder {_transferDirectoryPath}");
            try
            {
                foreach (var kvPair in _fileTracker)
                {
                    var quadKey = kvPair.Key;
                    var filePaths = kvPair.Value;

                    Console.WriteLine($"{DateTime.Now}: Start moving tracked files for quadkey ({quadKey}) to transfer folder {_transferDirectoryPath}");
                    foreach (var filePath in filePaths.Where(f => !string.IsNullOrWhiteSpace(f) && File.Exists(f)))
                    {
                        Console.WriteLine($"{DateTime.Now}: Moving tracked file {filePath} for quadkey ({quadKey}) to transfer folder {_transferDirectoryPath}");
                        File.Move(filePath, Path.Combine(_transferDirectoryPath, Path.GetFileName(filePath)));
                    }
                    Console.WriteLine($"{DateTime.Now}: Finished moving tracked files for quadkey ({quadKey}) to transfer folder {_transferDirectoryPath}");
                }
                PurgeTrackedFiles();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"{DateTime.Now}: [ERROR] Caught exception moving files to transfer folder: {ex}");
            }
            Console.WriteLine($"{DateTime.Now}: Finished moving tracked files to transfer folder {_transferDirectoryPath}");
        }

        public void PurgeTrackedFiles()
        {
            Console.WriteLine($"{DateTime.Now}: Purging tracked files");
            _fileTracker.Clear();
        }

        public void MoveFilesToTranferByDate(DateTime fileDate)
        {
            Console.WriteLine($"{DateTime.Now}: Start moving files for day {fileDate} to transfer folder {_transferDirectoryPath}");
            try
            {
                var fileNameRegEx = new Regex($"\\d+-{fileDate.Year:D4}-{fileDate.Month:D2}-{fileDate.Day:D2}T\\d\\d.*");
                var files = Directory.GetFiles(_stagingDirectoryPath, "*").Where(path => fileNameRegEx.IsMatch(path));

                foreach (var file in files.Where(File.Exists))
                {
                    Console.WriteLine($"{DateTime.Now}: Moving file {file} to transfer folder {_transferDirectoryPath}");
                    File.Move(file, Path.Combine(_transferDirectoryPath, Path.GetFileName(file)));
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine($"{DateTime.Now}: [ERROR] Caught exception moving files to transfer folder: {ex}");
            }
            Console.WriteLine($"{DateTime.Now}: End moving files for day {fileDate} to transfer folder {_transferDirectoryPath}");
        }

        public void MoveFilesToCloudByDate(DateTime fileDate)
        {
            Console.WriteLine($"{DateTime.Now}: Start processing transfer files to send to Cloud");
            try
            {
                var files = new List<string>();
                var tasks = new List<Task>();

                for (var hour = fileDate.Hour; hour < 24; hour++)
                {
                    var fileNameRegEx = new Regex($"\\d+-{fileDate.Year:D4}-{fileDate.Month:D2}-{fileDate.Day:D2}T{hour:D2}-.*");
                    files = files.Union(Directory.GetFiles(_transferDirectoryPath, "*").Where(path => fileNameRegEx.IsMatch(path))).ToList();
                }

                var i = 0;
                foreach (var file in files.Where(File.Exists).OrderByDescending(f => new FileInfo(f).Length))
                {
                    Console.WriteLine($"{DateTime.Now}: Start processing transfer file {file}");

                    var fileInfo = new FileInfo(file);
                    var fileName = fileInfo.Name;
                    var timestamp = fileName.Substring(fileName.IndexOf("-", StringComparison.Ordinal) + 1);
                    while (timestamp.Contains("."))
                        timestamp = timestamp.Substring(0, timestamp.IndexOf(".", StringComparison.Ordinal));

                    var dateToProcess = DateTime.ParseExact(timestamp, "yyyy-MM-ddTHH-mm-ss", CultureInfo.InvariantCulture);

                    var t = Task.Run(async () =>
                    {
                        Console.WriteLine($"{DateTime.Now}: Start sending transfer file {file} to Cloud");
                        await _cw.WriteFileToCloudAsync(file, dateToProcess);
                        Console.WriteLine($"{DateTime.Now}: Finished sending transfer file {file} to Cloud");
                    });

                    if (i % 20 == 0)
                    {
                        Task.WaitAll(tasks.ToArray<Task>());
                        tasks.Clear();
                    }

                    tasks.Add(t);
                    i++;
                    Console.WriteLine($"{DateTime.Now}: Finished processing transfer file {file}");
                }

                Task.WaitAll(tasks.ToArray<Task>());
            }
            catch (IOException e)
            {
                Console.WriteLine($"{DateTime.Now}: [ERROR] Caught exception transferring files to Cloud: {e}");
            }

            Console.WriteLine($"{DateTime.Now}: Finished processing transfer files to send to Cloud");
        }
    }
}
