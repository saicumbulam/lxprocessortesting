﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using LxHistoricalArchiver.Common;
using Newtonsoft.Json;

namespace LxHistoricalArchiver.Components
{
    public class StatusManager : IDisposable
    {
        private static StatusManager _instance;
        private static readonly object InstanceLock = new object();

        private const string MutexName = "LxHistoricalArchiver.StatusManager";


        private readonly List<KeyValuePair<DateTime, DateTime>> _dateTimesProcessed;
        private readonly string _statusFilePath;
        private readonly JsonSerializer _serializer;
        private readonly Mutex _statusFileMutex;
        private bool _haveLock;

        public static StatusManager GetInstance()
        {
            if (_instance == null)
            {
                lock (InstanceLock)
                {
                    if (_instance == null)
                        _instance = new StatusManager();
                }
            }

            return _instance;
        }

        private StatusManager()
        {
            _haveLock = false;

            _statusFilePath = Path.Combine(Config.DataMigrationBaseDirectory, "StatusManager.json");
            _serializer = new JsonSerializer
            {
                Formatting = Formatting.Indented
            };

            _statusFileMutex = new Mutex(false, MutexName);

            //if (File.Exists(_statusFilePath))
            //{
            //    using (var file = File.OpenText(_statusFilePath))
            //    {
            //        _dateTimesProcessed = (List<KeyValuePair<DateTime, DateTime>>)_serializer.Deserialize(file, typeof (List<KeyValuePair<DateTime, DateTime>>));
            //    }
            //}
            //else
            //{
            //    _dateTimesProcessed = new List<KeyValuePair<DateTime, DateTime>>();
            //}

            _dateTimesProcessed = LoadStatusList();
        }

        ~StatusManager()
        {
            UnLockStatusFile();
        }

        public void Dispose()
        {
            SerializeData();

            UnLockStatusFile();
        }

        public bool IsProcessed(DateTime startTime, DateTime endTime)
        {
            var ret = false;

            if (_haveLock || LockStatusFile())
            {
                try
                {
                    var existingStatusFile = LoadStatusList(false);

                    var tempList = MergeLists(_dateTimesProcessed, existingStatusFile);
                    _dateTimesProcessed.Clear();
                    _dateTimesProcessed.AddRange(tempList);

                    ret = _dateTimesProcessed.Contains(new KeyValuePair<DateTime, DateTime>(startTime, endTime));
                }
                finally { UnLockStatusFile(); }
            }

            return ret;
        }

        public void SetProcessed(DateTime startTime, DateTime endTime)
        {
            if (LockStatusFile())
            {
                _dateTimesProcessed.Add(new KeyValuePair<DateTime, DateTime>(startTime, endTime));
                SerializeData(false);
                UnLockStatusFile();
            }
        }

        public List<KeyValuePair<DateTime, DateTime>> FindProcessedByStartDate(DateTime startDate)
        {
            var ret = new List<KeyValuePair<DateTime, DateTime>>();

            if (LockStatusFile())
            {
                try
                {
                    var existingStatusFile = LoadStatusList(false);

                    var tempList = MergeLists(_dateTimesProcessed, existingStatusFile);
                    _dateTimesProcessed.Clear();
                    _dateTimesProcessed.AddRange(tempList);

                    ret = _dateTimesProcessed.FindAll(f => f.Key.Date == startDate.Date);
                }
                finally { UnLockStatusFile(); }
            }

            return ret;
        }

        public List<KeyValuePair<DateTime, DateTime>> FindProcessedByStartDateRange(DateTime startDate, DateTime endDate)
        {
            var ret = new List<KeyValuePair<DateTime, DateTime>>();

            if (LockStatusFile())
            {
                try
                {
                    var existingStatusFile = LoadStatusList(false);

                    var tempList = MergeLists(_dateTimesProcessed, existingStatusFile);
                    _dateTimesProcessed.Clear();
                    _dateTimesProcessed.AddRange(tempList);

                    ret = _dateTimesProcessed.FindAll(f => f.Key.Date >= startDate.Date && f.Key.Date < endDate.Date.AddDays(1));
                }
                finally { UnLockStatusFile(); }
            }

            return ret;
        }

        //public void Prune(DateTime startTime, DateTime endTime)
        public void Prune(List<KeyValuePair<DateTime, DateTime>> entriesToPrune)
        {
            if (LockStatusFile())
            {
                try
                {
                    var existingStatusFile = LoadStatusList(false);

                    var tempList = MergeLists(_dateTimesProcessed, existingStatusFile);
                    _dateTimesProcessed.Clear();
                    _dateTimesProcessed.AddRange(tempList);


                    foreach(var entry in entriesToPrune)
                        _dateTimesProcessed.RemoveAll(f => f.Key == entry.Key && f.Value == entry.Value);

                    //_dateTimesProcessed.RemoveAll(f => f.Key == startTime && f.Value == endTime);

                    using (var file = File.CreateText(_statusFilePath))
                    {
                        _serializer.Serialize(file, _dateTimesProcessed);
                    }
                }
                finally { UnLockStatusFile(); }
            }
        }

        private void SerializeData(bool allowUnlock = true)
        {
            if (_haveLock || LockStatusFile())
            {
                try
                {
                    var existingStatusFile = LoadStatusList(false);

                    var tempList = MergeLists(_dateTimesProcessed, existingStatusFile);
                    _dateTimesProcessed.Clear();
                    _dateTimesProcessed.AddRange(tempList);

                    using (var file = File.CreateText(_statusFilePath))
                    {
                        _serializer.Serialize(file, _dateTimesProcessed);
                    }
                }
                finally { if (allowUnlock) UnLockStatusFile(); }
            }
        }

        private static List<KeyValuePair<DateTime, DateTime>> MergeLists(IEnumerable<KeyValuePair<DateTime, DateTime>> listA, IEnumerable<KeyValuePair<DateTime, DateTime>> listB)
        {
            return listA.Union(listB).ToList();
        }

        private bool LockStatusFile()
        {
            var ret = false;

            lock (InstanceLock)
            {
                if (_haveLock) return true;

                try { ret = _statusFileMutex.WaitOne(); }
                catch (Exception ex)
                {
                    Console.WriteLine($"{DateTime.Now}: [ERROR] Caught exception trying to obtain mutex lock: {ex}");
                }
                _haveLock = ret;
            }

            return ret;
        }

        private void UnLockStatusFile()
        {
            lock (InstanceLock)
            {
                if (_haveLock) _statusFileMutex.ReleaseMutex();
                _haveLock = false;
            }
        }

        private List<KeyValuePair<DateTime, DateTime>> LoadStatusList(bool allowUnlock = true)
        {
            var storedStatusList = new List<KeyValuePair<DateTime, DateTime>>();

            if (File.Exists(_statusFilePath) && LockStatusFile())
            {
                try
                {
                    using (var file = File.OpenText(_statusFilePath))
                    {
                        storedStatusList = (List<KeyValuePair<DateTime, DateTime>>)_serializer.Deserialize(file, typeof(List<KeyValuePair<DateTime, DateTime>>));
                    }
                }
                finally
                {
                    if (allowUnlock) UnLockStatusFile();
                }
            }

            return storedStatusList;
        }
    }
}
