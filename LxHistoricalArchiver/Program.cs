﻿using System;
using System.Globalization;
using System.Linq;
using Aws.Core.Utilities;
using CommandLine;
using LxHistoricalArchiver.Components;

namespace LxHistoricalArchiver
{
    class Program
    {
        private static void Main(string[] args)
        {
            EventManager.Init();
            EventManager.LogInfo("Starting up");
            var cliParser = new Parser(with =>
            {
                with.CaseSensitive = false;
                with.EnableDashDash = true;
                with.IgnoreUnknownArguments = true;
                with.ParsingCulture = CultureInfo.InvariantCulture;
                with.HelpWriter = Parser.Default.Settings.HelpWriter;
                with.CaseInsensitiveEnumValues = true;
            });

            var options = cliParser.ParseArguments<Options>(args).MapResult(opts => opts, errors => null);

            if (options != null)
            {
                try
                {
                    if (options.PruneDate || options.PruneDateRange)
                    {
                        var statusMgr = StatusManager.GetInstance();
                        var processedDays = options.PruneDateRange
                                            ? statusMgr.FindProcessedByStartDateRange(options.Start.Date, options.End.Date)
                                            : statusMgr.FindProcessedByStartDate(options.Start.Date);

                        if (processedDays?.Count < 1)
                        {
                            Console.WriteLine(options.PruneDateRange
                                ? $"No entries found in status file for range Start: {options.Start.Date}, End: {options.End.Date}"
                                : $"No entries found in status file for Start: {options.Start.Date}");
                        }
                        else
                        {
                            foreach (var processedDay in processedDays)
                            {
                                Console.WriteLine($"Removing from status file Start: {processedDay.Key}, End: {processedDay.Value}");
                                //statusMgr.Prune(processedDay.Key, processedDay.Value);
                            }
                            statusMgr.Prune(processedDays);
                        }
                    }
                    else
                    {
                        var reader = new DataReader();
                        reader.RetrieveData(options.Start.Date, options.End.Date, options.Force);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Caught exception during processing. Exception detail: {ex}");
                }
            }
            QuitApp();

            EventManager.ShutDown();
        }

        private static void QuitApp()
        {
            Console.WriteLine("Press \'q\' to quit the program.");
            while (Console.Read() != 'q') { }
        }
    }
}