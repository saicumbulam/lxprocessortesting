﻿using System;
using CommandLine;

namespace LxHistoricalArchiver
{
    class Options
    {
        [Option("startdate", HelpText = "Query start date. Day only format: YYYY-MM-DD; Day with time format: YYYY-MM-DDTHH:MM:SS", Required = true)]
        public DateTime Start { get; set; }

        [Option("enddate", HelpText = "Query end date, inclusive. Day only format: YYYY-MM-DD; Day with time format: YYYY-MM-DDTHH:MM:SS", Required = true)]
        public DateTime End { get; set; }


        [Option("force", Default = false, HelpText = "Force data ingestion, even if it's already been processed")]
        public bool Force { get; set; }

        [Option("prunedate", Default = false, Hidden = true)]
        public bool PruneDate { get; set; }

        [Option("prunedaterange", Default = false, Hidden = true)]
        public bool PruneDateRange { get; set; }
    }
}
