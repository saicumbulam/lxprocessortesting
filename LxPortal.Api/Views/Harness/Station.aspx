﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<!DOCTYPE html>

<html>
<head id="Head1" runat="server">
    <meta name="viewport" content="width=device-width" />
    <title>Station</title>
    <%: Scripts.Render("~/bundles/jquery")%>
</head>
<body>
    <h1>Station</h1>
    <p>
        <select id='service'>
            <option value="/Api/Stations">Global CORS (/Api/Stations)</option>
        </select>
    </p>
    <p>Id: <input type="text" id="id" /></p>
    <p>Name: <input type="text" id="name" /></p>
    <p>Street Address: <input type="text" id="streetAddress" /></p>
    <p>Country: <input type="text" id="country" /></p>
    <p>Province: <input type="text" id="province" /></p>
    <p>Region: <input type="text" id="region" /></p>
    <p>City: <input type="text" id="city" /></p>
    <p>Postal Code: <input type="text" id="postalCode" /></p>
    <p>Serial Number: <input type="text" id="serialNumber" /></p>
    <p>POC Name: <input type="text" id="pocName" /></p>
    <p>POC Phone: <input type="text" id="pocPhone" /></p>
    <p>POC Email: <input type="text" id="pocEmail" /></p>
    <p>Primary Delivery IP: <input type="text" id="primaryDeliveryIp" /></p>
    <p>Secondary Delivery IP: <input type="text" id="secondaryDeliveryIp" /></p>
    <p>Send Log Rate Minutes: <input type="text" id="sendLogRateMinutes" /></p>
    <p>Send GPS Rate Minutes: <input type="text" id="sendGpsRateMinutes" /></p>
    <p>Send Antenna Gain Minutes: <input type="text" id="sendAntennaGainRateMinutes" /></p>
    <p>Antenna Mode: <input type="text" id="antennaMode" /></p>
    <p>Antenna Attenuation:<input type="text" id="antennaAttenuation" /></p>
    <p>
        Notes:<textarea id="note" rows="10" cols="30"></textarea>
    </p>
    <p>
        <input type="button" id="getAll" value="Get All" />
        <input type="button" id="getOne" value="Get One" />
        <input type="button" id="post" value="Add" />
        <input type="button" id="put" value="Update" />
        <input type="button" id="delete" value="Delete" />
        <input type="button" id="clear" value="Clear Form" />
    </p>
    <div id='result'></div>
    <script type="text/javascript">
        $("#getAll").click(function () {
            var valuesAddress = $("#service").val();
            $.ajax({
                url: valuesAddress,
                type: "GET",
                success: function (result) {
                    var text = "";
                    if (result != null) {
                        for (var i = 0; i < result.length; i++) {
                            if (i > 0) text = text + ", ";
                            text = text + JSON.stringify(result[i]);
                        }
                    }

                    $("#result").text(text);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $("#result").text(textStatus);
                }
            });
        });

        $("#getOne").click(function () {
            getStation();
        });

        $("#post").click(function () {
            var valuesAddress = $("#service").val();

            var station = readStationFromForm();
            var json = JSON.stringify(station);

            $.ajax({
                url: valuesAddress,
                type: "POST",
                contentType: "application/json",
                data: json,
                success: function (result) {
                    $("#result").text(JSON.stringify(result));
                    writeStationToForm(result);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $("#result").text(textStatus);
                }
            });
        });

        $("#put").click(function () {
            var valuesAddress = $("#service").val();

            var station = readStationFromForm();
            var json = JSON.stringify(station);

            $.ajax({
                url: valuesAddress + "/" + station.Id,
                type: "PUT",
                contentType: "application/json",
                data: json,
                success: function (result) {
                    $("#result").text(result);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $("#result").text(textStatus);
                    //udpate form
                    getStation();
                }
            });
        });

        $("#delete").click(function () {
            var valuesAddress = $("#service").val();
            var id = $("#id").val();
            $.ajax({
                url: valuesAddress + "/" + id,
                type: "DELETE",
                success: function (result) {
                    clearForm();
                    $("#result").text(id + " deleted");
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $("#result").text(textStatus);
                }
            });
        });

        $("#clear").click(function () {
            clearForm();
        });


        getStation = function () {
            var valuesAddress = $("#service").val();
            var id = "/" + $("#id").val();
            $.ajax({
                url: valuesAddress + id,
                type: "GET",
                success: function (result) {
                    $("#result").text(JSON.stringify(result));
                    writeStationToForm(result);

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $("#result").text(textStatus);
                }
            });
        };

        writeStationToForm = function (station) {
            $("#id").val(station.Id);
            $("#name").val(station.Name);
            $("#streetAddress").val(station.StreetAddress);
            $("#country").val(station.Country);
            $("#province").val(station.Province);
            $("#region").val(station.Region);
            $("#city").val(station.City);
            $("#postalCode").val(station.PostalCode);
            $("#serialNumber").val(station.SerialNumber);
            $("#pocName").val(station.PocName);
            $("#pocPhone").val(station.PocPhone);
            $("#pocEmail").val(station.PocEmail);
            $("#primaryDeliveryIp").val(station.PrimaryDeliveryIp);
            $("#secondaryDeliveryIp").val(station.SecondaryDeliveryIp);
            $("#sendLogRateMinutes").val(station.SendLogRateMinutes);
            $("#sendGpsRateMinutes").val(station.SendGpsRateMinutes);
            $("#sendAntennaGainRateMinutes").val(station.SendAntennaGainRateMinutes);
            $("#antennaMode").val(station.AntennaMode);
            $("#antennaAttenuation").val(station.AntennaAttenuation);
            $("#note").val(station.Note);
        };

        readStationFromForm = function () {
            var station = {
                Id: $("#id").val(),
                Name: $("#name").val(),
                StreetAddress: $("#streetAddress").val(),
                Country: $("#country").val(),
                Province: $("#province").val(),
                Region: $("#region").val(),
                City: $("#city").val(),
                PostalCode: $("#postalCode").val(),
                SerialNumber: $("#serialNumber").val(),
                PocName: $("#pocName").val(),
                PocPhone: $("#pocPhone").val(),
                PocEmail: $("#pocEmail").val(),
                PrimaryDeliveryIp: $("#primaryDeliveryIp").val(),
                SecondaryDeliveryIp: $("#secondaryDeliveryIp").val(),
                SendLogRateMinutes: $("#sendLogRateMinutes").val(),
                SendGpsRateMinutes: $("#sendGpsRateMinutes").val(),
                SendAntennaGainRateMinutes: $("#sendAntennaGainRateMinutes").val(),
                AntennaMode: $("#antennaMode").val(),
                AntennaAttenuation: $("#antennaAttenuation").val(),
                note: $("#note").val()
            };

            return station;
        };

        clearForm = function () {
            var inputs = $("input[type='text']");
            for (var i = 0; i < inputs.length; i++) {
                $(inputs[i]).val("");
            }
            $("note").val("");
            $("#result").text("");
        };

    </script>
</body>
</html>
