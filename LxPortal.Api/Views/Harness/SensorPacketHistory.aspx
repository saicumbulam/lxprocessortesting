﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<!DOCTYPE html>

<html>
<head>
    <meta name="viewport" content="width=device-width" />
    <title>Station</title>
    <%: Scripts.Render("~/bundles/jquery")%>
</head>
<body>
    <h1>Station</h1>
    <p>Id: <input type="text" id="id" /></p>
    <p>Start: <input type="text" id="start" /></p>
    <p>End: <input type="text" id="end" /></p>
    <p>
        <input type="button" id="get" value="Get" />
    </p>
    <div id='result'></div>
    <script type="text/javascript">
        var service = "/Api/SensorPacketHistory";

        $("#get").click(function () {
            getsensorHistory();
        });

        $("#clear").click(function () {
            //clearForm();
        });


        getsensorHistory = function () {
            var id = "/" + $("#id").val();
            var startDate, endDate;
            var start = Date.parse($("#start").val());
            if (start)
                startDate = new Date(start);

            var end = Date.parse($("#end").val());
            if (end)
                endDate = new Date(end);

            var query;
            if (endDate || startDate) {
                var query = "?";
                if (startDate) {
                    query = query + "start=" + JSON.stringify(startDate);
                    if (endDate)
                        query = query + "&";
                }

                if (endDate)
                    query = query + "end=" + JSON.stringify(endDate);
            }


            $.ajax({
                url: service + id + query,
                type: "GET",
                success: function (result) {
                    $("#result").text(JSON.stringify(result));

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $("#result").text(textStatus);
                }
            });
        };
    </script>
</body>
</html>
