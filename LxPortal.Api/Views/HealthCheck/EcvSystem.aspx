﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<%@ Import Namespace="LxPortal.Api.Models" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <meta name="viewport" content="width=device-width" />
    <title>EcvSystem</title>
</head>
<body>
    
    <div>
        <p>Overall: <%= (ViewBag.OverallSuccess) ? "Success" : "Failure" %></p>
    </div>
    <br/>
    <% 
        foreach (HealthCheck hc in ViewBag.HealthChecks)
        {
    %>
    <div>
        <p>System: <%= hc.System %></p>
        <p>State: <%= hc.State %></p>
        <p>Message: <%= hc.Message %></p>
        <br/>
    </div>
    <%
        } %>
</body>
</html>
