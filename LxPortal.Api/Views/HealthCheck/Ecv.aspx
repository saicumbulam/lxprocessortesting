﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <meta name="viewport" content="width=device-width" />
    <title>Ecv</title>
</head>
<body>
    <div>
        <p>Status: <%= ViewBag.HealthCheck.State %></p>
        <p>Message: <%= ViewBag.HealthCheck.Message %></p>
    </div>
</body>
</html>
