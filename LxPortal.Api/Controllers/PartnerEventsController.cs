﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using LxPortal.Api.Models;
using LxPortalLib.Common;
using LxPortalLib.Models;

namespace LxPortal.Api.Controllers
{
    public class PartnerEventsController : ApiController
    {
        private IPartnerEventsRepository _partnerEventsRepository;
        public PartnerEventsController()
        {
            _partnerEventsRepository = new PartnerEventsAuditRepository();
        }

        public PartnerEventsController(IPartnerEventsRepository partnerEventsRepository)
        {
            _partnerEventsRepository = partnerEventsRepository;
        }
        
        // GET: api/PartnerEvents/5
        public async Task<HttpResponseMessage> Get(string id, DateTime start, DateTime end)
        {

            start = Utilities.NormalizeDateTimeToUtc(start);
            end = Utilities.NormalizeDateTimeToUtc(end);
            ResponseContainer<IEnumerable<PartnerEvent>> responseContainer = new ResponseContainer<IEnumerable<PartnerEvent>> { i = Guid.NewGuid().ToString(), r = null };

            try
            {
                IEnumerable<PartnerEvent> events = await _partnerEventsRepository.Get(id.ToLower(), start, end);
                responseContainer.c = (int)HttpStatusCode.OK;
                responseContainer.e = null;
                responseContainer.r = events;
            }
            catch (Exception ex)
            {
                responseContainer.c = (int)HttpStatusCode.InternalServerError;
                responseContainer.e = ex.Message;
            }

            return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
        }
    }
}
