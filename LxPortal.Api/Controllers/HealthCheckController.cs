﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

using Aws.Core.Utilities;
using LxCommon.Monitoring;
using LxPortal.Api.Models;
using TaskStatus = System.Threading.Tasks.TaskStatus;

namespace LxPortal.Api.Controllers
{
    public class HealthCheckController : Controller
    {
        public ActionResult Ecv()
        {
            HealthCheckRepositoryDb repository = new HealthCheckRepositoryDb();
            ViewBag.HealthCheck = repository.Get();

            return View();
        }

        public ActionResult EcvSystem()
        {
            List<HealthCheck> hcs = new List<HealthCheck>(5);

            HealthCheck stickyFailover = CheckStickyFailover();
            bool overallSuccess = stickyFailover.IsSuccess;

            hcs.Add(stickyFailover);

            //read the last status file
            DateTime? lastKnownFailureTime = OpenLastKnwonFailureTime();

            HealthCheckRepositoryDb repository = new HealthCheckRepositoryDb();
            HealthCheck apiHc = repository.Get();
            overallSuccess = apiHc.IsSuccess && overallSuccess;
            hcs.Add(apiHc);

            Task<HealthCheck>[] tasks = new Task<HealthCheck>[3]
            {
                Task<HealthCheck>.Factory.StartNew(GetHttpEcvResponse, new HealthCheck{ System = "Receiver", CheckUri = new Uri(Config.ReceiverEcv) }),
                Task<HealthCheck>.Factory.StartNew(GetHttpEcvResponse, new HealthCheck{ System = "Data Feed", CheckUri = new Uri(Config.DataFeedEcv) }),
                Task<HealthCheck>.Factory.StartNew(GetHttpEcvResponse, new HealthCheck{ System = "Processor", CheckUri = new Uri(Config.ProcessorEcv) })
            };

            Task.WaitAll(tasks);

            for (int i = 0; i < tasks.Length; i++)
            {
                if (tasks[i].Status == TaskStatus.RanToCompletion)
                    overallSuccess = tasks[i].Result.IsSuccess && overallSuccess;
                else
                    overallSuccess = false;

                hcs.Add(tasks[i].Result);
            }

            ViewBag.HealthChecks = hcs;

            //if stickyfileover.isSuccess is true do the following
            //if overallSuccess is true && file existed, erase the file
            //if overallSuccess is false, if file not existed, create file + add timestamp + overallSuccess = true
            //if overallSuccess is false  and file existed, check the time span between now and the timestamp
            //if timespan >= allowable range (created in web.config), overallSuccess = false
            //if timespan < allowable range, overallSuccess = true
            if (stickyFailover.IsSuccess)
            {
                if (overallSuccess)
                {
                    if (lastKnownFailureTime.HasValue)
                        DeleteLastKnownFailureTime();
                }
                else
                {
                    if (!lastKnownFailureTime.HasValue)
                    {
                        lastKnownFailureTime = DateTime.UtcNow;
                        WriteLastKnownFailureTime(lastKnownFailureTime);
                        overallSuccess = true;
                    }
                    else
                    {
                        TimeSpan ts = DateTime.UtcNow - lastKnownFailureTime.Value;
                        if (ts.TotalSeconds < Config.AllowableFailureTimeRangeSec)
                            overallSuccess = true;
                    }
                }
            }

            ViewBag.OverallSuccess = overallSuccess;
            WriteStickyFailover(overallSuccess);

            return View();
        }

        private DateTime? OpenLastKnwonFailureTime()
        {
            DateTime? rtnVal = null;

            try
            {
                FileInfo fi = new FileInfo(Config.LastKnwonFailureTimeFile);
                //if file exist, read the datetime, if file is not exist rtnVal is already set to null
                if (fi.Exists)
                {
                    string dtString = System.IO.File.ReadAllText(Config.LastKnwonFailureTimeFile);
                    DateTime dt;
                    if (DateTime.TryParse(dtString, out dt))
                    {
                        rtnVal = dt.ToUniversalTime();
                    }
                }
            }
            catch (Exception ex)
            {
                //if there is failure, assume the worst case and trigger overall failure if there is failure detected
                EventManager.LogError(TaskMonitor.ApiSystemEcv, string.Format("Unable to check last known failure time file: {0}. Error: {1}", Config.LastKnwonFailureTimeFile, ex.Message));
                rtnVal = DateTime.UtcNow.AddSeconds(-Config.AllowableFailureTimeRangeSec);
            }
            return rtnVal;
        }

        private void WriteLastKnownFailureTime(DateTime? dt)
        {
            try
            {
                if (!dt.HasValue)
                    dt = DateTime.UtcNow;
                System.IO.File.WriteAllText(Config.LastKnwonFailureTimeFile, dt.Value.ToString("o"));
            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.ApiSystemEcv, string.Format("Unable to write last known failure time file: {0}. Error: {1}", Config.LastKnwonFailureTimeFile, ex.Message));
            }
        }

        private void DeleteLastKnownFailureTime()
        {
            try
            {
                FileInfo fi = new FileInfo(Config.LastKnwonFailureTimeFile);
                fi.Delete();
            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.ApiSystemEcv, string.Format("Unable to delete last known failure time file: {0}. Error: {1}", Config.LastKnwonFailureTimeFile, ex.Message));
            }
        }

        private void WriteStickyFailover(bool overallSuccess)
        {
            FileInfo fi = new FileInfo(Config.FailoverFile);
            try
            {
                if (!fi.Exists && !overallSuccess)
                {
                    FileStream fs = fi.Create();
                    fs.Close();
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.ApiSystemEcv, string.Format("Unable to create sticky failover file: {0}. Error: {1}", Config.FailoverFile, ex.Message));
            }

        }

        private HealthCheck CheckStickyFailover()
        {
            HealthCheck hc = new HealthCheck { System = "Sticky File" };
            FileInfo fi = new FileInfo(Config.FailoverFile);
            try
            {
                if (fi.Exists)
                {
                    hc.IsSuccess = false;
                    hc.State = "Failure";
                    hc.Message = string.Format("Sticky failover file {0} exists", Config.FailoverFile);
                }
                else
                {
                    hc.IsSuccess = true;
                    hc.State = "Success";
                    hc.Message = string.Empty;
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.ApiSystemEcv, string.Format("Unable to check sticky failover file: {0}. Error: {1}", Config.FailoverFile, ex.Message));
                hc.IsSuccess = false;
                hc.State = "Failure";
            }

            return hc;
        }

        private HealthCheck GetHttpEcvResponse(object u)
        {
            HealthCheck hc = u as HealthCheck;
            if (hc != null)
            {
                hc.IsSuccess = false;
                StreamReader sr = null;
                if (hc.CheckUri != null)
                {
                    try
                    {
                        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(hc.CheckUri);
                        request.Timeout = Config.EcvRequestHttpTimeoutMilliseconds;
                        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                        if (response != null)
                        {
                            hc.Message = string.Format("Status Message: {0} Status Code: {1}", response.StatusDescription, (int)response.StatusCode);

                            sr = new StreamReader(response.GetResponseStream());

                            string rs = sr.ReadToEnd();

                            if (IsSuccessMessage(rs))
                            {
                                hc.IsSuccess = true;
                                hc.State = rs;
                            }
                            else
                            {
                                hc.IsSuccess = false;
                                hc.State = (!string.IsNullOrEmpty(rs)) ? rs : "Unknown";
                                EventManager.LogError(TaskMonitor.ApiSystemEcv, string.Format("System Ecv check {0} failure", hc.System));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        EventManager.LogError(TaskMonitor.ApiSystemEcv, string.Format("Unable to proxy request: {0}. Error: {1}", hc.CheckUri, ex.Message));
                    }
                    finally
                    {
                        if (null != sr) sr.Close();
                    }
                }
            }
            else
            {
                hc = new HealthCheck { IsSuccess = false };
            }

            return hc;
        }

        private bool IsSuccessMessage(string rs)
        {
            bool success = false;
            if (!string.IsNullOrEmpty(rs))
            {
                rs = rs.ToUpper();
                success = (rs.IndexOf("SUCCESS", StringComparison.Ordinal) >= 0);
            }
            return success;
        }
    }
}
