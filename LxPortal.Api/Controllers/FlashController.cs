﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using En.Ods.Lib.Common.Dynamo;
using En.Ods.Lib.Common.Interface;
using En.Ods.Lib.Common.Serialization;
using LxPortal.Api.Helper;
using LxPortal.Api.Models;
using LxPortalLib.Common;
using LxPortalLib.Models;
using Newtonsoft.Json;

namespace LxPortal.Api.Controllers
{
    public class FlashController : ApiController
    {
        private FlashRepositoryDb _repository = new FlashRepositoryDb();
        private static SnsFactory _providerFactory = new SnsFactory();
        private static ICloudTopic _topic = _providerFactory.GetNotifier(new JsonOdsValue()).GetTopic(Config.AwsSnsLxQueryNotifierTopic);
        private static IOdsValue _s3Credentials = new JsonOdsValue();
        private static IEmailSender _sender;

        public List<FlashRecord> Get(DateTime start, DateTime end, StrokeType type, decimal ullat, decimal ullon, decimal lrlat, decimal lrlon)
        {
            start = Utilities.NormalizeDateTimeToUtc(start);
            end = Utilities.NormalizeDateTimeToUtc(end);

            List<FlashRecord> flashes = _repository.GetByBoundingBox(start, end, type, ullat, ullon, lrlat, lrlon);
            if (flashes == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }

            return flashes;
        }

        public List<FlashRecord> Get(DateTime start, DateTime end, StrokeType type, decimal lat, decimal lon, int radius)
        {
            start = Utilities.NormalizeDateTimeToUtc(start);
            end = Utilities.NormalizeDateTimeToUtc(end);
            List<FlashRecord> flashes = null;

            if (radius <= Config.DetectionReportMaxRadiusInMeters)
            {
                flashes = _repository.GetByRadius(start, end, type, lat, lon, radius);
                if (flashes == null)
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
                }
            }
            return flashes;
        }

        //version 2 calls
        public HttpResponseMessage Getv2(DateTime start, DateTime end, StrokeType type, decimal lat, decimal lon, int radius)
        {
            var responseContainer = new ResponseContainer<List<FlashRecord>> {i = Guid.NewGuid().ToString(), r = null};

            //check for radius 
            int radiusVal;
            string errorMessage;
            if (!ParameterValidator.ValidateRadius(radius, out radiusVal, out errorMessage))
            {
                responseContainer.c = 400;
                responseContainer.e = errorMessage; 
                return Request.CreateResponse(HttpStatusCode.BadRequest, responseContainer, "application/json");
            }

            //check for lat/lon 
            if (!ParameterValidator.ValidateLatitude(lat, out errorMessage))
            {
                responseContainer.c = 400;
                responseContainer.e = errorMessage;
                return Request.CreateResponse(HttpStatusCode.BadRequest, responseContainer, "application/json");
            }

            if (!ParameterValidator.ValidateLongitude(lon, out errorMessage))
            {
                responseContainer.c = 400;
                responseContainer.e = errorMessage;
                return Request.CreateResponse(HttpStatusCode.BadRequest, responseContainer, "application/json");
            }


            //check for type 
            StrokeType typeOutput;
            if (!ParameterValidator.ValidateStrokeType(type, out typeOutput))
            {
                responseContainer.c = 400;
                responseContainer.e = errorMessage;
                return Request.CreateResponse(HttpStatusCode.BadRequest, responseContainer, "application/json");
            }

            //check start/end time 
            DateTime startTime;
            DateTime endTime;
            if (!ParameterValidator.ValidateStartEndTime(start, end, out startTime, out endTime, out errorMessage, true))
            {
                responseContainer.c = 400;
                responseContainer.e = errorMessage;
                return Request.CreateResponse(HttpStatusCode.BadRequest, responseContainer, "application/json");
            }

            try
            {
                List<FlashRecord> flashes = _repository.GetByRadius(startTime, endTime, typeOutput, lat, lon, radiusVal);
                responseContainer.c = 200;
                responseContainer.e = null;
                responseContainer.r = flashes;
                return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
            }
            catch (Exception ex)
            {
                responseContainer.c = 500;
                responseContainer.e = "Error occured in inner call: " + ex;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, responseContainer, "application/json");
            }

        }
        
        public HttpResponseMessage Getv2(DateTime start, DateTime end, StrokeType type, decimal ullat, decimal ullon, decimal lrlat, decimal lrlon)
        {
            var responseContainer = new ResponseContainer<List<FlashRecord>> { i = Guid.NewGuid().ToString(), r = null };
            
            string errorMessage; 

            //validate bbox 
            if (!ParameterValidator.ValidateBoundingBox(ullat, ullon, lrlat, lrlon, out errorMessage))
            {
                responseContainer.c = 400;
                responseContainer.e = errorMessage;
                return Request.CreateResponse(HttpStatusCode.BadRequest, responseContainer, "application/json");
            }

            //check for type 

            StrokeType typeOutput;
            if (!ParameterValidator.ValidateStrokeType(type, out typeOutput))
            {
                responseContainer.c = 400;
                responseContainer.e = "Invalid StrokeType input.";
                return Request.CreateResponse(HttpStatusCode.BadRequest, responseContainer, "application/json");
            }

            //check start/end time 
            DateTime startTime;
            DateTime endTime;
            if (!ParameterValidator.ValidateStartEndTime(start, end, out startTime, out endTime, out errorMessage, true))
            {
                responseContainer.c = 400;
                responseContainer.e = errorMessage;
                return Request.CreateResponse(HttpStatusCode.BadRequest, responseContainer, "application/json");
            }

            try
            {
                List<FlashRecord> flashes = _repository.GetByBoundingBox(startTime, endTime, typeOutput, ullat, ullon, lrlat, lrlon);
                responseContainer.c = 200;
                responseContainer.e = null;
                responseContainer.r = flashes;
                return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
            }
            catch (Exception ex)
            {
                responseContainer.c = 500;
                responseContainer.e = "Error occured in inner call: " + ex;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, responseContainer, "application/json");
            }
        }

        //version 3 calls
        public HttpResponseMessage Getv3(DateTime start, DateTime end, StrokeType type, decimal lat, decimal lon, int radius, string responseEmailAddress, int lxType = 0, int outputType = 0, bool sendNotifications = true, bool sendProgressNotifications = true, int lxVersion=0)
        {
            var responseContainer = new ResponseContainer<string> { i = Guid.NewGuid().ToString(), r = null };

            //check for radius 
            int radiusVal;
            string errorMessage;
            if (!ParameterValidator.ValidateRadius(radius, out radiusVal, out errorMessage))
            {
                responseContainer.c = 400;
                responseContainer.e = errorMessage;
                return Request.CreateResponse(HttpStatusCode.BadRequest, responseContainer, "application/json");
            }

            //check for lat/lon 
            if (!ParameterValidator.ValidateLatitude(lat, out errorMessage))
            {
                responseContainer.c = 400;
                responseContainer.e = errorMessage;
                return Request.CreateResponse(HttpStatusCode.BadRequest, responseContainer, "application/json");
            }

            if (!ParameterValidator.ValidateLongitude(lon, out errorMessage))
            {
                responseContainer.c = 400;
                responseContainer.e = errorMessage;
                return Request.CreateResponse(HttpStatusCode.BadRequest, responseContainer, "application/json");
            }


            //check for type 
            StrokeType typeOutput;
            if (!ParameterValidator.ValidateStrokeType(type, out typeOutput))
            {
                responseContainer.c = 400;
                responseContainer.e = errorMessage;
                return Request.CreateResponse(HttpStatusCode.BadRequest, responseContainer, "application/json");
            }

            FileOutputType fileOutputType;
            if (!ParameterValidator.ValidateFileOutputType(outputType, out fileOutputType))
            {
                responseContainer.c = 400;
                responseContainer.e = errorMessage;
                return Request.CreateResponse(HttpStatusCode.BadRequest, responseContainer, "application/json");
            }

            LxOutputVersion lxOutputVersion;
            if (!ParameterValidator.ValidateOutputVersion(lxVersion, out lxOutputVersion))
            {
                responseContainer.c = 400;
                responseContainer.e = errorMessage;
                return Request.CreateResponse(HttpStatusCode.BadRequest, responseContainer, "application/json");
            }

            //check start/end time 
            DateTime startTime;
            DateTime endTime;
            if (!ParameterValidator.ValidateStartEndTime(start, end, out startTime, out endTime, out errorMessage))
            {
                responseContainer.c = 400;
                responseContainer.e = errorMessage;
                return Request.CreateResponse(HttpStatusCode.BadRequest, responseContainer, "application/json");
            }

            // check email
            if (!ParameterValidator.ValidateEmail(responseEmailAddress, out errorMessage))
            {
                responseContainer.c = 400;
                responseContainer.e = errorMessage;
                return Request.CreateResponse(HttpStatusCode.BadRequest, responseContainer, "application/json");
            }

            try
            {
                // Add an async job to SNS/SQS queue
                LxQueryJobData lxQueryJobData = new LxQueryJobData()
                {
                    JobId = Guid.NewGuid().ToString(),
                    QueryType = LxQueryType.Circle,
                    LxType = LxType.Flash,
                    OutputVersion = lxOutputVersion,
                    OutputType = fileOutputType,
                    SubmittedDateTime = DateTime.UtcNow,
                    Start = startTime,
                    End = endTime,
                    StrokeType = typeOutput,
                    Latitude = Convert.ToDouble(lat),
                    Longitude = Convert.ToDouble(lon),
                    Radius = radius,
                    RadiusType = RadiusType.Meters,
                    RecipientEmailAddress = responseEmailAddress,
                    SendNotifications = sendNotifications,
                    SendProgressNotifications = sendProgressNotifications
                };

                string query = JsonConvert.SerializeObject(lxQueryJobData);

                _topic.SendNotification(JsonConvert.SerializeObject(lxQueryJobData));

                bool msgSent = SendSubmissionEmail(lxQueryJobData);

                responseContainer.c = 200;
                responseContainer.e = null;
                responseContainer.r = query;
                return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
            }
            catch (Exception ex)
            {
                responseContainer.c = 500;
                responseContainer.e = "Error occured in inner call: " + ex;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, responseContainer, "application/json");
            }

        }

        public static bool SendSubmissionEmail(LxQueryJobData lxQueryJobData)
        {
            try
            {
                _sender = new SesFactory().GetEmailSender(_s3Credentials);
                var emailTemplate = new JobSubmission();
                emailTemplate._lxQueryJobData = lxQueryJobData;
                emailTemplate._emailSubject = Config.JobSubmittedSubject;
                var msgBody = emailTemplate.TransformText();

                var result = _sender.SendMessageAsync(Config.EmailFromAddress, emailTemplate._emailSubject, msgBody, 
                                                    true, new List<string>() { lxQueryJobData.RecipientEmailAddress });
                return true;
            }
            catch (Exception ex)
            {
                return true;
            }
        }
        public HttpResponseMessage Getv3(DateTime start, DateTime end, StrokeType type, decimal ullat, decimal ullon, decimal lrlat, decimal lrlon, string responseEmailAddress, int lxType = 0, int outputType = 0, bool sendNotifications = true, bool sendProgressNotifications = true, int lxVersion = 0)
        {
            var responseContainer = new ResponseContainer<string> { i = Guid.NewGuid().ToString(), r = null };

            string errorMessage;

            //validate bbox 
            if (!ParameterValidator.ValidateBoundingBox(ullat, ullon, lrlat, lrlon, out errorMessage))
            {
                responseContainer.c = 400;
                responseContainer.e = errorMessage;
                return Request.CreateResponse(HttpStatusCode.BadRequest, responseContainer, "application/json");
            }

            //check for type 

            StrokeType typeOutput;
            if (!ParameterValidator.ValidateStrokeType(type, out typeOutput))
            {
                responseContainer.c = 400;
                responseContainer.e = "Invalid StrokeType input.";
                return Request.CreateResponse(HttpStatusCode.BadRequest, responseContainer, "application/json");
            }

            FileOutputType fileOutputType;
            if (!ParameterValidator.ValidateFileOutputType(outputType, out fileOutputType))
            {
                responseContainer.c = 400;
                responseContainer.e = errorMessage;
                return Request.CreateResponse(HttpStatusCode.BadRequest, responseContainer, "application/json");
            }

            LxOutputVersion lxOutputVersion;
            if (!ParameterValidator.ValidateOutputVersion(lxVersion, out lxOutputVersion))
            {
                responseContainer.c = 400;
                responseContainer.e = errorMessage;
                return Request.CreateResponse(HttpStatusCode.BadRequest, responseContainer, "application/json");
            }


            //check start/end time 
            DateTime startTime;
            DateTime endTime;
            if (!ParameterValidator.ValidateStartEndTime(start, end, out startTime, out endTime, out errorMessage))
            {
                responseContainer.c = 400;
                responseContainer.e = errorMessage;
                return Request.CreateResponse(HttpStatusCode.BadRequest, responseContainer, "application/json");
            }

            // check email
            if (!ParameterValidator.ValidateEmail(responseEmailAddress, out errorMessage))
            {
                responseContainer.c = 400;
                responseContainer.e = errorMessage;
                return Request.CreateResponse(HttpStatusCode.BadRequest, responseContainer, "application/json");
            }

            try
            {
                // Add an async job to SNS/SQS queue
                LxQueryJobData lxQueryJobData = new LxQueryJobData()
                {
                    JobId = Guid.NewGuid().ToString(),
                    QueryType = LxQueryType.BoundingBox,
                    LxType = LxType.Flash,
                    OutputVersion = lxOutputVersion,
                    OutputType = fileOutputType,
                    SubmittedDateTime = DateTime.UtcNow,
                    Start = startTime,
                    End = endTime,
                    StrokeType = typeOutput,
                    LatitudeMax = Convert.ToDouble(ullat),
                    LongitudeMax = Convert.ToDouble(lrlon),
                    LatitudeMin = Convert.ToDouble(lrlat),
                    LongitudeMin = Convert.ToDouble(ullon),
                    RecipientEmailAddress = responseEmailAddress,
                    SendNotifications = sendNotifications,
                    SendProgressNotifications = sendProgressNotifications
                };

                string query = JsonConvert.SerializeObject(lxQueryJobData);

                _topic.SendNotification(JsonConvert.SerializeObject(lxQueryJobData));

                bool msgSent = SendSubmissionEmail(lxQueryJobData);

                responseContainer.c = 200;
                responseContainer.e = null;
                responseContainer.r = query;
                return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
            }
            catch (Exception ex)
            {
                responseContainer.c = 500;
                responseContainer.e = "Error occured in inner call: " + ex;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, responseContainer, "application/json");
            }
        }
    }
}
