﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using LxPortal.Api.Models;
using LxPortalLib.Models;

namespace LxPortal.Api.Controllers
{
    public class NetworkController : ApiController
    {
        INetworkRepository _networkRepository;
        public NetworkController()
        {
            _networkRepository = new NetworkDbRepository();
        }

        public NetworkController(INetworkRepository networkRepository)
        {
            _networkRepository = networkRepository;
        }

        // GET: api/Network
        public HttpResponseMessage Get()
        {
            ResponseContainer<IEnumerable<Network>> responseContainer = new ResponseContainer<IEnumerable<Network>> { i = Guid.NewGuid().ToString(), r = null };

            try
            {
                IEnumerable<Network> networks = _networkRepository.GetAll();
                responseContainer.c = (int)HttpStatusCode.OK;
                responseContainer.e = null;
                responseContainer.r = networks;
            }
            catch (Exception ex)
            {
                responseContainer.c = (int)HttpStatusCode.InternalServerError;
                responseContainer.e = ex.Message;
            }

            return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
        }

        // POST: api/Network
        public HttpResponseMessage Post([FromBody]Network value)
        {
            ResponseContainer<Network> responseContainer = new ResponseContainer<Network> { i = Guid.NewGuid().ToString(), r = null };
            try
            {
                Network network = _networkRepository.Add(value);
                responseContainer.c = (int)HttpStatusCode.Created;
                responseContainer.e = null;
                responseContainer.r = network;
            }
            catch (Exception ex)
            {
                responseContainer.c = (int)HttpStatusCode.InternalServerError;
                responseContainer.e = ex.Message;
            }

            return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
        }

        // PUT: api/Network/5
        public HttpResponseMessage Put(string id, [FromBody]Network value)
        {
            ResponseContainer<Network> responseContainer = new ResponseContainer<Network> { i = Guid.NewGuid().ToString(), r = null };
            try
            {
                _networkRepository.Update(id, value);
                responseContainer.c = (int)HttpStatusCode.NoContent;
                responseContainer.e = null;
            }
            catch (Exception ex)
            {
                responseContainer.c = (int)HttpStatusCode.InternalServerError; ;
                responseContainer.e = ex.Message;
            }

            return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");

        }

        // DELETE: api/Network/5
        public HttpResponseMessage Delete(string id)
        {
            ResponseContainer<Network> responseContainer = new ResponseContainer<Network> { i = Guid.NewGuid().ToString(), r = null };
            try
            {
                _networkRepository.Remove(id);
                responseContainer.c = (int)HttpStatusCode.NoContent;
                responseContainer.e = null;
            }
            catch (Exception ex)
            {
                responseContainer.c = (int)HttpStatusCode.InternalServerError;
                responseContainer.e = ex.Message;
            }

            return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");

        }
    }
}
