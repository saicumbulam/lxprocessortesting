﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Aws.Core.Utilities;
using LxPortal.Api.Models;
using LxPortalLib.Models;

namespace LxPortal.Api.Controllers
{
    public class ProvisioningController : ApiController
    {

        private ProvisioningRepositoryDb _repository = new ProvisioningRepositoryDb();  //did not create interface

        #region Get Methods

        [HttpGet]
        [ActionName("Partner")]
        public HttpResponseMessage GetAll(string partnerId = null, int feedType = -1)
        {
            var responseContainer = new ResponseContainer<List<PartnerRecord>> { i = Guid.NewGuid().ToString(), r = null };

            try
            {
                if (!string.IsNullOrEmpty(partnerId))
                {
                    return GetPartner(partnerId);
                }

                List<PartnerRecord> partners = _repository.GetAllPartners(feedType);
                if (partners != null)
                {
                    EventManager.LogInfo("partners size = " + partners.Count);
                }
                else
                {
                    EventManager.LogInfo("partners is null");
                }
                if (partners != null && partners.Count > 0)
                {
                    responseContainer.c = 200;
                    responseContainer.e = null;
                    responseContainer.r = partners;
                    return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
                }

                responseContainer.c = 500;
                responseContainer.e = "Failed to get list of Partners.";
                responseContainer.r = null;
                return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
            }
            catch (Exception ex)
            {
                responseContainer.c = 500;
                responseContainer.e = "Error occured in inner call: " + ex;
                return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
            }
        }

        [NonAction]
        public HttpResponseMessage GetPartner(string id)
        {
            var responseContainer = new ResponseContainer<PartnerRecord> { i = Guid.NewGuid().ToString(), r = null };

            if (isValidParam(id))
            {
                try
                {
                    PartnerRecord partner = _repository.GetPartner(id);
                    if (partner != null)
                    {
                        responseContainer.c = 200;
                        responseContainer.e = null;
                        responseContainer.r = partner;
                        return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
                    }

                    responseContainer.c = 500;
                    responseContainer.e = "Failed to get metadata for Partner " + id;
                    responseContainer.r = null;
                    return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
                }
                catch (Exception ex)
                {
                    responseContainer.c = 500;
                    responseContainer.e = "Error occured in inner call: " + ex;
                    return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
                }
            }
            else
            {
                responseContainer.c = 500;
                responseContainer.e = "Error occured due to bad input";
                responseContainer.r = null;
                return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
            }
        }

        [ActionName("EventLog")]
        public HttpResponseMessage GetEventLog(string partnerId, int epochStart = -1, int epochEnd = -1)
        {
            var responseContainer = new ResponseContainer<List<EventLogRecord>> { i = Guid.NewGuid().ToString(), r = null };

            if (!String.IsNullOrEmpty(partnerId))
            {
                try
                {
                    List<EventLogRecord> eventLog = _repository.GenerateEventLog(partnerId, epochStart, epochEnd);
                    if (eventLog != null)
                    {
                        responseContainer.c = 200;
                        responseContainer.e = null;
                        responseContainer.r = eventLog;
                        return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
                    }

                    responseContainer.c = 500;
                    responseContainer.e = "Failed to get list of Event Log for partner " + partnerId;
                    responseContainer.r = null;
                    return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
                }
                catch (Exception ex)
                {
                    responseContainer.c = 500;
                    responseContainer.e = "Error occured in inner call: " + ex;
                    return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
                }
            }
            else
            {
                responseContainer.c = 500;
                responseContainer.e = "Error occured due to bad input";
                responseContainer.r = null;
                return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
            }

        }
        #endregion
        
        [ActionName("Partner")]
        [HttpPost]
        public HttpResponseMessage PostPartner(PartnerRecord partner)
        {

            var responseContainer = new ResponseContainer<string> { i = Guid.NewGuid().ToString(), r = null };

            if (!isValidParam(partner))
            {
                responseContainer.c = 400;
                responseContainer.e = "Invalid parameters passed in";
                return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
            }

            try
            {
                partner = _repository.Add(partner);
                if (partner != null)
                {
                    responseContainer.c = 201;
                    responseContainer.e = null;
                    responseContainer.r = partner.PartnerId;
                    return Request.CreateResponse(HttpStatusCode.OK, responseContainer);   //breaks on this line NullArgumentException 
                }

                responseContainer.c = 500;
                responseContainer.e = "Failed to Create New Partner.";
                responseContainer.r = null;
                return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json"); 
            }
            catch (Exception ex)
            {
                responseContainer.c = 500;
                responseContainer.e = "Error occured in inner call: " + ex;
                return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
            }
        }

        [ActionName("Partner")]
        [HttpPut]
        public HttpResponseMessage PutPartner(PartnerRecord partner)
        {
            var responseContainer = new ResponseContainer<string> { i = Guid.NewGuid().ToString(), r = null };

            if (!isValidParam(partner))
            {
                responseContainer.c = 400;
                responseContainer.e = "Invalid parameters passed in";
                return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
            }

            try
            {

                partner = _repository.Update(partner);
                if (partner != null)
                {
                    responseContainer.c = 200;
                    responseContainer.e = null;
                    responseContainer.r = partner.PartnerId;
                    return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");   //breaks on this line NullArgumentException 
                }

                responseContainer.c = 500;
                responseContainer.e = "Failed to Update Partner metadata.";
                responseContainer.r = null;
                return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json"); 
            }
            catch (Exception ex)
            {
                responseContainer.c = 500;
                responseContainer.e = "Error occured in inner call: " + ex;
                return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
            }
        }

        [ActionName("Partner")]
        [HttpDelete]
        public HttpResponseMessage DeletePartner(string id)
        {
            var responseContainer = new ResponseContainer<List<PartnerRecord>> { i = Guid.NewGuid().ToString(), r = null };

            try
            {
                if (_repository.DeletePartner(id))
                {
                    responseContainer.c = 200;
                    responseContainer.e = null;
                    responseContainer.r = null;
                    return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
                }

                responseContainer.c = 500;
                responseContainer.e = "Failed to delete Partner " + id;
                responseContainer.r = null;
                return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
            }
            catch (Exception ex)
            {
                responseContainer.c = 500;
                responseContainer.e = "Error occured in inner call: " + ex;
                return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
            }
        }

        //helper methods
        protected Boolean isValidParam(PartnerRecord partners)
        {

            if (!string.IsNullOrEmpty(partners.PartnerId) && partners.PartnerId.Length > 50)
            {
                return false;
            }
            if (!string.IsNullOrEmpty(partners.PartnerName) && partners.PartnerName.Length > 50)
            {
                return false;
            }
            if (!string.IsNullOrEmpty(partners.BoundingArea) && partners.BoundingArea.Length > 50)
            {
                return false;
            }
            if (!string.IsNullOrEmpty(partners.LastModifiedBy) && partners.LastModifiedBy.Length > 50)
            {
                return false;
            }
            return true;
        }

        protected Boolean isValidParam(string Id)
        {
            return (!string.IsNullOrEmpty(Id));
        }
    }
}
