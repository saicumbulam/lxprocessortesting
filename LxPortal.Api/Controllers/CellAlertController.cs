﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LxPortal.Api.Helper;
using LxPortal.Api.Models;
using LxPortalLib.Common;
using LxPortalLib.Models;

namespace LxPortal.Api.Controllers
{
    public class CellAlertController : ApiController
    {
        private ICellAlertRepository _repository = new CellAlertRepositoryDb();

        public List<CellAlertRecord> Get(DateTime start, DateTime end, decimal ullat, decimal ullon, decimal lrlat, decimal lrlon)
        {
            start = Utilities.NormalizeDateTimeToUtc(start);
            end = Utilities.NormalizeDateTimeToUtc(end);

            List<CellAlertRecord> alerts = _repository.Get(start, end, ullat, ullon, lrlat, lrlon);
            if (alerts == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }

            return alerts;
        }
        
        //cell alert v2
        public HttpResponseMessage Getv2(DateTime start, DateTime end, decimal ullat, decimal ullon, decimal lrlat, decimal lrlon)
        {
            var responseContainer = new ResponseContainer<List<CellAlertRecord>> { i = Guid.NewGuid().ToString(), r = null };
            string errorMessage; 

            //validate bbox 
            if (!ParameterValidator.ValidateBoundingBox(ullat, ullon, lrlat, lrlon, out errorMessage))
            {
                responseContainer.c = 400;
                responseContainer.e = errorMessage;
                return Request.CreateResponse(HttpStatusCode.BadRequest, responseContainer, "application/json");
            }

            //check start/end time 
            DateTime startTime;
            DateTime endTime;
            if (!ParameterValidator.ValidateStartEndTime(start, end, out startTime, out endTime, out errorMessage))
            {
                responseContainer.c = 400;
                responseContainer.e = errorMessage;
                return Request.CreateResponse(HttpStatusCode.BadRequest, responseContainer, "application/json");
            }

            try
            {
                List<CellAlertRecord> alerts = _repository.Get(startTime, endTime, ullat, ullon, lrlat, lrlon);
                responseContainer.c = 200;
                responseContainer.e = null;
                responseContainer.r = alerts;
                return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
            }
            catch(Exception ex)
            {
                responseContainer.c = 500;
                responseContainer.e = "Error occured in inner call: " + ex;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, responseContainer, "application/json");
            }
        }

    }
}
