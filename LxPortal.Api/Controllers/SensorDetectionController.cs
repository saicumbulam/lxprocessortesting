﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using LxPortal.Api.Models;
using LxPortalLib.Common;
using LxPortalLib.Models;

namespace LxPortal.Api.Controllers
{
    public class SensorDetectionController : ApiController
    {
        private ISensorDetectionRepository _repository = new SensorDetectionRepositoryDb();

        public SensorDetection Get(string id, DateTime? start, DateTime? end)
        {
            if (start.HasValue)
                start = Utilities.NormalizeDateTimeToUtc(start.Value);

            if (end.HasValue)
                end = Utilities.NormalizeDateTimeToUtc(end.Value);

            SensorDetection item = _repository.Get(id, start, end);
            if (item == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }
            if (item.DetectionList.Count == 0 && start.HasValue == false && end.HasValue == false)
            {
                item = _repository.Get(id, DateTime.MinValue, DateTime.MaxValue);
                if (item == null)
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
                }
            }
            return item;
        }

        public SensorDetection Get(string id)
        {
            return Get(id, null, null);
        }

        public HttpResponseMessage GetV2(string id, DateTime? start, DateTime? end)
        {
            var responseContainer = new ResponseContainer<SensorDetection> { i = Guid.NewGuid().ToString(), r = null };
            try
            {
                if (start.HasValue)
                    start = Utilities.NormalizeDateTimeToUtc(start.Value);

                if (end.HasValue)
                    end = Utilities.NormalizeDateTimeToUtc(end.Value);

                SensorDetection item = _repository.Get(id, start, end);
                if (item.DetectionList.Count == 0 && start.HasValue == false && end.HasValue == false)
                {
                    item = _repository.Get(id, DateTime.MinValue, DateTime.MaxValue);
                }
                responseContainer.c = 200;
                responseContainer.e = null;
                responseContainer.r = item;
                return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
            }
            catch (Exception ex)
            {
                responseContainer.c = 500;
                responseContainer.e = "Error occured in inner call: " + ex;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, responseContainer, "application/json");
            }
        }

        public HttpResponseMessage GetV2(string id)
        {
            return GetV2(id, null, null);
        }
    }
}
