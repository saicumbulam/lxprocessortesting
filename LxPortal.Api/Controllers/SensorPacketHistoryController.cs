﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using LxPortalLib.Models;
using LxPortalLib.Common;

using LxPortal.Api.Models;

namespace LxPortal.Api.Controllers
{
    public class SensorPacketHistoryController : ApiController
    {
        private ISensorPacketHistoryRepository _repository = new SensorPacketHistoryRepositoryDb();

        public List<PacketRecord> Get(string id)
        {
            List<PacketRecord> item = Get(id, null, null);

            if (item == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }

            return item;
        }

        public List<PacketRecord> Get(string id, DateTime? start, DateTime? end)
        {
            if (start.HasValue)
                start = Utilities.NormalizeDateTimeToUtc(start.Value);

            if (end.HasValue)
                end = Utilities.NormalizeDateTimeToUtc(end.Value);
            
            List<PacketRecord> item = _repository.Get(id, start, end);

            if (item == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }
            if (item.Count == 0 && start.HasValue == false && end.HasValue == false)
            {
                item = _repository.Get(id, DateTime.MinValue, DateTime.MaxValue);
                if (item == null)
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
                }
            }
            
            return item;
        }

        public HttpResponseMessage GetV2(string id)
        {
            var responseContainer = new ResponseContainer<List<PacketRecord>> { i = Guid.NewGuid().ToString(), r = null };
            try
            {
                List<PacketRecord> item = _repository.Get(id, null, null);
                responseContainer.c = 200;
                responseContainer.e = null;
                responseContainer.r = item;
                return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
            }
            catch (Exception ex)
            {
                responseContainer.c = 500;
                responseContainer.e = "Error occured in inner call: " + ex;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, responseContainer, "application/json");
            }
        }

        public HttpResponseMessage GetV2(string id, DateTime? start, DateTime? end)
        {
            var responseContainer = new ResponseContainer<List<PacketRecord>> { i = Guid.NewGuid().ToString(), r = null };
            try
            {
                if (start.HasValue)
                    start = Utilities.NormalizeDateTimeToUtc(start.Value);

                if (end.HasValue)
                    end = Utilities.NormalizeDateTimeToUtc(end.Value);

                List<PacketRecord> item = _repository.Get(id, start, end);
                if (item.Count == 0 && start.HasValue == false && end.HasValue == false)
                {
                    item = _repository.Get(id, DateTime.MinValue, DateTime.MaxValue);
                }
                responseContainer.c = 200;
                responseContainer.e = null;
                responseContainer.r = item;
                return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
            }
            catch (Exception ex)
            {
                responseContainer.c = 500;
                responseContainer.e = "Error occured in inner call: " + ex;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, responseContainer, "application/json");
            }
        }
    }
}
