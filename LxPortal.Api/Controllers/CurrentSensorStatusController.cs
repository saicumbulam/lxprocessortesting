﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using LxPortal.Api.Models;
using LxPortalLib.Models;

namespace LxPortal.Api.Controllers
{
    public class CurrentSensorStatusController : ApiController
    {
        private ICurrentSensorStatusRepository _repository = new CurrentSensorStatusRepositoryDb();

        public CurrentSensorStatus GetStation(string id)
        {
            CurrentSensorStatus item = _repository.Get(id);
            if (item == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }
            return item;
        }

        public HttpResponseMessage GetStationV2(string id)
        {
            var responseContainer = new ResponseContainer<CurrentSensorStatus> { i = Guid.NewGuid().ToString(), r = null };
            try
            {
                CurrentSensorStatus item = _repository.Get(id);
                responseContainer.c = 200;
                responseContainer.e = null;
                responseContainer.r = item;
                return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
            }
            catch (Exception ex)
            {
                responseContainer.c = 500;
                responseContainer.e = "Error occured in inner call: " + ex;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, responseContainer, "application/json");
            }
        }
    }
}
