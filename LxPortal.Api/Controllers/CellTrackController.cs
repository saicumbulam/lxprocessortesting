﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using LxPortal.Api.Helper;
using LxPortal.Api.Models;
using LxPortalLib.Common;
using LxPortalLib.Models;

namespace LxPortal.Api.Controllers
{
    public class CellTrackController : ApiController
    {
        private ICellTrackRepository _repository = new CellTrackRepositoryDb();

        public List<CellTrackRecord> Get(DateTime start, DateTime end, decimal ullat, decimal ullon, decimal lrlat, decimal lrlon)
        {
            start = Utilities.NormalizeDateTimeToUtc(start);
            end = Utilities.NormalizeDateTimeToUtc(end);

            List<CellTrackRecord> alerts = _repository.Get(start, end, ullat, ullon, lrlat, lrlon);
            if (alerts == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }

            return alerts;
        }

        //cell track v2
        public HttpResponseMessage Getv2(DateTime start, DateTime end, decimal ullat, decimal ullon, decimal lrlat, decimal lrlon)
        {
            var responseContainer = new ResponseContainer<List<CellTrackRecord>> {i = Guid.NewGuid().ToString(), r = null};
            string errorMessage;

            //validate bbox 
            if (!ParameterValidator.ValidateBoundingBox(ullat, ullon, lrlat, lrlon, out errorMessage))
            {
                responseContainer.c = 400;
                responseContainer.e = errorMessage;
                return Request.CreateResponse(HttpStatusCode.BadRequest, responseContainer, "application/json");
            }

            //check start/end time 
            DateTime startTime;
            DateTime endTime;
            if (!ParameterValidator.ValidateStartEndTime(start, end, out startTime, out endTime, out errorMessage))
            {
                responseContainer.c = 400;
                responseContainer.e = errorMessage;
                return Request.CreateResponse(HttpStatusCode.BadRequest, responseContainer, "application/json");
            }

            try
            {
                List<CellTrackRecord> tracks = _repository.Get(startTime, endTime, ullat, ullon, lrlat, lrlon);
                responseContainer.c = 200;
                responseContainer.e = null;
                responseContainer.r = tracks;
                return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
            }
            catch (Exception ex)
            {
                responseContainer.c = 500;
                responseContainer.e = "Error occured in inner call: " + ex;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, responseContainer, "application/json");
            }

        }
    }
}
