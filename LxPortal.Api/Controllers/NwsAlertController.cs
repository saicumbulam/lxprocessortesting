﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LxPortal.Api.Helper;
using LxPortal.Api.Models;
using LxPortalLib.Models;
using LxPortalLib.Common;

namespace LxPortal.Api.Controllers
{
    public class NwsAlertController : ApiController
    {
        private NwsAlertDbRepository _repository = new NwsAlertDbRepository();

        public IEnumerable<NwsAlert> Get(DateTime start, DateTime end)
        {
            start = Utilities.NormalizeDateTimeToUtc(start);
            end = Utilities.NormalizeDateTimeToUtc(end);

            IEnumerable<NwsAlert> alerts = _repository.Get(start, end);
            if (alerts == null)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));

            return alerts;

        }

        //nwsalert v2
        public HttpResponseMessage Getv2(DateTime start, DateTime end)
        {
            var responseContainer = new ResponseContainer<IEnumerable<NwsAlert>> { i = Guid.NewGuid().ToString(), r = null };
            string errorMessage; 

            //check start/end time 
            DateTime startTime;
            DateTime endTime;
            if (!ParameterValidator.ValidateStartEndTime(start, end, out startTime, out endTime, out errorMessage))
            {
                responseContainer.c = 400;
                responseContainer.e = errorMessage;
                return Request.CreateResponse(HttpStatusCode.BadRequest, responseContainer, "application/json");
            }

            try
            {
                IEnumerable<NwsAlert> alerts = _repository.Get(startTime, endTime);
                responseContainer.c = 200;
                responseContainer.e = null;
                responseContainer.r = alerts;
                return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
            }
            catch (Exception ex)
            {
                responseContainer.c = 500;
                responseContainer.e = "Error occured in inner call: " + ex;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, responseContainer, "application/json");
            }
        }
    }
}
