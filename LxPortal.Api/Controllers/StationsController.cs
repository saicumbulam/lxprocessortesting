﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using LxPortalLib.Models;

using LxPortal.Api.Models;

namespace LxPortal.Api.Controllers
{
    public class StationsController : ApiController
    {
        //private static readonly IStationRepository _repository = new StationRepositorySample();

        private IStationRepository _repository;
        private StationNetworkRepositoryDb _networkRepository = new StationNetworkRepositoryDb();

          
        public StationsController() : this(new StationRepositoryDb()) {}

        public StationsController(IStationRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<Station> GetAllStations()
        {
            return _repository.GetAll();
        }

        public HttpResponseMessage GetAllStationsv2()
        {
            var responseContainer = new ResponseContainer<IEnumerable<StationV2>> {i = Guid.NewGuid().ToString(), r = null};

            try
            {
                IEnumerable<StationV2> station = _repository.GetAllV2();
                responseContainer.c = 200;
                responseContainer.e = null;
                responseContainer.r = station; 
                return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
            }
            catch(Exception ex)
            {
                responseContainer.c = 500;
                responseContainer.e = "Error occured in inner call: " + ex;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, responseContainer, "application/json");
            }
        }

        public Station GetStation(string id)
        {
            Station item = _repository.Get(id);
            if (item == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }
            return item;
        }

        public HttpResponseMessage GetStationV2(string id)
        {
            ResponseContainer<StationV2> responseContainer = new ResponseContainer<StationV2> { i = Guid.NewGuid().ToString(), r = null };
            try
            {
                StationV2 item = _repository.GetV2(id);
                responseContainer.c = 200;
                responseContainer.e = null;
                responseContainer.r = item;
                return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
            }
            catch(Exception ex)
            {
                responseContainer.c = 500;
                responseContainer.e = "Error occured in inner call: " + ex.Message;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, responseContainer, "application/json");
            }
        }

        public HttpResponseMessage PostStation(Station station)
        {
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
            if (VerifyStationData(station))
            {
                station = _repository.Add(station);
                if (station != null)
                {
                    if (Request != null)
                    {
                        response = Request.CreateResponse<Station>(HttpStatusCode.Created, station);
                        string uri = Url.Link("DefaultApi", new { id = station.Id });
                        response.Headers.Location = new Uri(uri);
                    }
                    else
                    {
                        response.ReasonPhrase = station.Id;
                    }
                }
            }
            return response;
        }

        [HttpPost]
        public HttpResponseMessage PostStationV2(Station station)
        {
            var responseContainer = new ResponseContainer<string> { i = Guid.NewGuid().ToString(), r = null };

            if (VerifyStationData(station))
            {
                station = _repository.Add(station);
                if (station != null)
                {
                    responseContainer.c = 201;
                    responseContainer.e = null;
                    responseContainer.r = station.Id;
                    var response = Request.CreateResponse(HttpStatusCode.OK, responseContainer);
                    if (Request != null)
                    {
                        response = Request.CreateResponse<Station>(HttpStatusCode.Created, station);
                        string uri = Url.Link("DefaultApi", new { id = station.Id });
                        response.Headers.Location = new Uri(uri);
                    }
                    else
                    {
                        response.ReasonPhrase = station.Id;
                    }
                }
            }
            responseContainer.c = 500;
            responseContainer.e = "Failed to create station";
            responseContainer.r = null;
            return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
        }

        public HttpResponseMessage PutStation(string id, Station station)
        {
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
            if (VerifyStationData(station))
            {

                station.Id = id;
                int repositoryVal = _repository.Update(station);
                if (repositoryVal == 1)
                {
                    response = Request.CreateResponse(HttpStatusCode.OK);
                }
                if (repositoryVal == -1)
                {
                    response = Request.CreateResponse(HttpStatusCode.NotFound);
                }
                if (repositoryVal == -2)
                {
                    response = Request.CreateResponse(HttpStatusCode.MethodNotAllowed);
                }
            }
            else
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError));
            }

            return response; 
        }

        [HttpPut]
        public HttpResponseMessage PutStationV2(string id, Station station)
        {
            var responseContainer = new ResponseContainer<string> { i = Guid.NewGuid().ToString(), r = null };

            if (VerifyStationData(station))
            {

                station.Id = id;
                int repositoryVal = _repository.Update(station);
                if (repositoryVal == 1)
                {
                    responseContainer.c = 200;
                    responseContainer.e = null;
                    responseContainer.r = station.Id;
                    return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
                }
                if (repositoryVal == -1)
                {
                    responseContainer.c = 404;
                    responseContainer.e = null;
                    return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
                }
                if (repositoryVal == -2)
                {
                    responseContainer.c = 405;
                    responseContainer.e = null;
                    return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
                }

                responseContainer.c = 500;
                responseContainer.e = "Failed to update station " + id;
                responseContainer.r = null;
                return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
            }
            else
            {
                responseContainer.c = 400;
                responseContainer.e = "Invalid parameters passed in";
                return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
            }
        }

        public HttpResponseMessage DeleteStation(string id)
        {
            _repository.Remove(id);
            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }

        [HttpDelete]
        public HttpResponseMessage DeleteStationV2(string id)
        {
            var responseContainer = new ResponseContainer<string> { i = Guid.NewGuid().ToString(), r = null };

            try
            {
                if (_repository.Remove(id))
                {
                    responseContainer.c = 200;
                    responseContainer.e = null;
                    responseContainer.r = null;
                    return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
                }

                responseContainer.c = 500;
                responseContainer.e = "Failed to delete station " + id;
                responseContainer.r = null;
                return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
            }
            catch (Exception ex)
            {
                responseContainer.c = 500;
                responseContainer.e = "Error occured in inner call: " + ex;
                return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
            }
        }

        public HttpResponseMessage GetStationNetworks()
        {
            var responseContainer = new ResponseContainer<IEnumerable<Network>> { i = Guid.NewGuid().ToString(), r = null };

            try
            {
                IEnumerable<Network> networks = _networkRepository.GetNetworks();
                responseContainer.c = 200;
                responseContainer.e = null;
                responseContainer.r = networks;
            }
            catch (Exception ex)
            {
                responseContainer.c = 500;
                responseContainer.e = "Error occured in inner call: " + ex;
            }

            return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
        }

        [HttpGet]
        public HttpResponseMessage AddStationToNetwork(string stationId, [FromUri] string networkId)
        {
            var responseContainer = new ResponseContainer<object> { i = Guid.NewGuid().ToString(), r = null };

            try
            {
                _repository.AddStationToNetwork(stationId, networkId);
                responseContainer.c = (int) HttpStatusCode.Created;
                responseContainer.e = null;
            }
            catch (Exception ex)
            {
                responseContainer.c = (int) HttpStatusCode.InternalServerError;
                responseContainer.e = ex.Message;
            }

            return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
        }

        [HttpGet]
        public HttpResponseMessage RemoveStationFromNetwork(string stationId, string networkId)
        {
            var responseContainer = new ResponseContainer<object> { i = Guid.NewGuid().ToString(), r = null };

            try
            {
                _repository.RemoveStationFromNetwork(stationId, networkId);
                responseContainer.c = (int)HttpStatusCode.NoContent;
                responseContainer.e = null;
            }
            catch (Exception ex)
            {
                responseContainer.c = (int)HttpStatusCode.InternalServerError;
                responseContainer.e = ex.Message;
            }

            return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
        }

        [HttpGet]
        public HttpResponseMessage GetAllStationsInNetwork(string networkId)
        {
            var responseContainer = new ResponseContainer<IEnumerable<StationV2>> { i = Guid.NewGuid().ToString(), r = null };

            try
            {
                IEnumerable<StationV2> stations = _repository.GetAllStationsInNetwork(networkId);
                responseContainer.c = (int)HttpStatusCode.OK;
                responseContainer.r = stations;
                responseContainer.e = null;
            }
            catch (Exception ex)
            {
                responseContainer.c = (int)HttpStatusCode.InternalServerError;
                responseContainer.e = ex.Message;
            }

            return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
        }

        private bool VerifyStationData(Station station)
        {
            if (!string.IsNullOrEmpty(station.PrimaryDeliveryIp) && station.PrimaryDeliveryIp.Length > 50)
                return false;
            if (!string.IsNullOrEmpty(station.SecondaryDeliveryIp) && station.SecondaryDeliveryIp.Length > 50)
                return false;
            if (string.IsNullOrEmpty(station.Name) || station.Name.Length > 50)
                return false;
            if (string.IsNullOrEmpty(station.SerialNumber) || station.SerialNumber.Length > 15)
                return false;
            if (!string.IsNullOrEmpty(station.StreetAddress) && station.StreetAddress.Length > 250)
                return false;
            if (!string.IsNullOrEmpty(station.Country) && station.Country.Length > 30)
                return false;
            if (!string.IsNullOrEmpty(station.Province) && station.Province.Length > 50)
                return false;
            if (!string.IsNullOrEmpty(station.Region) && station.Region.Length > 50)
                return false;
            if (!string.IsNullOrEmpty(station.City) && station.City.Length > 30)
                return false;
            if (!string.IsNullOrEmpty(station.PostalCode) && station.PostalCode.Length > 20)
                return false;
            if (!string.IsNullOrEmpty(station.PocName) && station.PocName.Length > 50)
                return false;
            if (!string.IsNullOrEmpty(station.PocPhone) && station.PocPhone.Length > 20)
                return false;
            if (!string.IsNullOrEmpty(station.PocEmail) && station.PocEmail.Length > 250)
                return false;
            if (station.SendGpsRateMinutes == null || station.SendLogRateMinutes == null || station.SendAntennaGainRateMinutes == null || station.AntennaMode == null)
                return false;
            if (station.AntennaMode > 10 || station.AntennaMode < 9)
                return false;
            if (station.SendAntennaGainRateMinutes < 5 || station.SendAntennaGainRateMinutes > 120)
                return false;
            if (station.SendGpsRateMinutes < 10 || station.SendGpsRateMinutes > 120)
                return false;
            if (station.SendLogRateMinutes < 10 || station.SendLogRateMinutes > 720)
                return false;
            if (!string.IsNullOrEmpty(station.Note) && station.Note.Length > 1000)
                return false;
            return true;
        }
    }
}
