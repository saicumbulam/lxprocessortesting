﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LxPortal.Api.Helper;
using LxPortal.Api.Models;
using LxPortalLib.Models;  

namespace LxPortal.Api.Controllers
{
    public class StormController : ApiController
    {
        private IStormRepository _repository = new StormRepositoryDb();

        public List<StormRecord> GetAllStormData(DateTime start, DateTime end, decimal ullat, decimal ullon, decimal lrlat, decimal lrlon)
        {
            List<StormRecord> storm = _repository.GetAll(start, end, ullat, ullon, lrlat, lrlon);
            if (storm == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }
            return storm;
        }

        //storm v2
        public HttpResponseMessage GetAllStormDatav2(DateTime start, DateTime end, decimal ullat, decimal ullon, decimal lrlat, decimal lrlon)
        {
            var responseContainer = new ResponseContainer<List<StormRecord>> {i = Guid.NewGuid().ToString(), r = null};
            string errorMessage;

            //validate bbox 
            if (!ParameterValidator.ValidateBoundingBox(ullat, ullon, lrlat, lrlon, out errorMessage))
            {
                responseContainer.c = 400;
                responseContainer.e = errorMessage;
                return Request.CreateResponse(HttpStatusCode.BadRequest, responseContainer, "application/json");
            }

            //check start/end time 
            DateTime startTime;
            DateTime endTime;
            if (!ParameterValidator.ValidateStartEndTime(start, end, out startTime, out endTime, out errorMessage))
            {
                responseContainer.c = 400;
                responseContainer.e = errorMessage;
                return Request.CreateResponse(HttpStatusCode.BadRequest, responseContainer, "application/json");
            }

            try
            {
                List<StormRecord> storm = _repository.GetAll(start, end, ullat, ullon, lrlat, lrlon);
                responseContainer.c = 200;
                responseContainer.e = null;
                responseContainer.r = storm;
                return Request.CreateResponse(HttpStatusCode.OK, responseContainer, "application/json");
            }
            catch (Exception ex)
            {
                responseContainer.c = 500;
                responseContainer.e = "Error occured in inner call: " + ex;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, responseContainer, "application/json");
            }
        }
    }
}