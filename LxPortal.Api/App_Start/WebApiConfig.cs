﻿using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Routing;
using LxPortal.Api.MessageHandlers;

namespace LxPortal.Api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Formatters
            config.Formatters.JsonFormatter.SerializerSettings.Formatting = Newtonsoft.Json.Formatting.Indented;
            config.Formatters.JsonFormatter.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Include;


            DelegatingHandler uriFormat = new UriFormatExtensionHandler();
            config.MessageHandlers.Add(uriFormat);
            config.EnableCors(new EnableCorsAttribute("*", "*", "*"));

            #region Lx Archiver
            config.Routes.MapHttpRoute(
                name: "GetLxArchiver",
                routeTemplate: "api/lxarchiver",
                defaults: new { controller = "LxArchiver", action = "Get" }
            );

            #endregion

            #region Flash/FlashPortion
            //flash v1 w/o ext
            config.Routes.MapHttpRoute(
                name: "GetFlashv1",
                routeTemplate: "api/flash",
                defaults: new { controller = "Flash", action = "Get" }
            );
            //flash v1
            config.Routes.MapHttpRoute(
                name: "GetFlashv1Ext",
                routeTemplate: "api/flash.{ext}",
                defaults: new { ext = RouteParameter.Optional, controller = "Flash", action = "Get" }
            );


            //flash v2 w/o ext
            config.Routes.MapHttpRoute(
                name: "GetFlashv2",
                routeTemplate: "api/v2/flash",
                defaults: new { controller = "Flash", action = "Getv2" }
            );
            //flash v2 
            config.Routes.MapHttpRoute(
                name: "GetFlashv2Ext",
                routeTemplate: "api/v2/flash.{ext}",
                defaults: new { ext = RouteParameter.Optional, controller = "Flash", action = "Getv2" }
            );

            //flash v3 w/o ext
            config.Routes.MapHttpRoute(
                name: "GetFlashv3",
                routeTemplate: "api/v3/flash",
                defaults: new { controller = "Flash", action = "Getv3" }
            );

            //flashportion v1 w/o ext
            config.Routes.MapHttpRoute(
                name: "GetFlashPortionv1",
                routeTemplate: "api/flashportion",
                defaults: new { ext = RouteParameter.Optional, controller = "FlashPortion", action = "Get" }
            );
            //flashportion v1
            config.Routes.MapHttpRoute(
                name: "GetFlashPortionv1Ext",
                routeTemplate: "api/flashportion.{ext}",
                defaults: new { controller = "FlashPortion", action = "Get" }
            );


            //flashportion v2 w/o ext
            config.Routes.MapHttpRoute(
                name: "GetFlashPortionv2",
                routeTemplate: "api/v2/flashportion",
                defaults: new { ext = RouteParameter.Optional, controller = "FlashPortion", action = "Getv2" }
            );
            //flashportion v2
            config.Routes.MapHttpRoute(
                name: "GetFlashPortionv2Ext",
                routeTemplate: "api/v2/flashportion.{ext}",
                defaults: new { controller = "FlashPortion", action = "Getv2" }
            );

            //flashportion v3 w/o ext
            config.Routes.MapHttpRoute(
                name: "GetFlashPortionv3",
                routeTemplate: "api/v3/flashportion",
                defaults: new { controller = "FlashPortion", action = "Getv3" }
            );
            #endregion 

            #region Cell Alert 
            config.Routes.MapHttpRoute(
                name: "GetCellAlertv1",
                routeTemplate: "api/cellalert",
                defaults: new { controller = "CellAlert", action = "Get" }
            );

            config.Routes.MapHttpRoute(
                name: "GetCellAlertv1Ext",
                routeTemplate: "api/cellalert.{ext}",
                defaults: new { ext = RouteParameter.Optional, controller = "CellAlert", action = "Get" }
            );


            config.Routes.MapHttpRoute(
                name: "GetCellAlertv2",
                routeTemplate: "api/v2/cellalert",
                defaults: new { controller = "CellAlert", action = "Getv2" }
            );

            config.Routes.MapHttpRoute(
                name: "GetCellAlertv2Ext",
                routeTemplate: "api/v2/cellalert.{ext}",
                defaults: new { ext = RouteParameter.Optional, controller = "CellAlert", action = "Getv2" }
            );

            #endregion 

            #region Cell Track
            config.Routes.MapHttpRoute(
                name: "GetCellTrackv1",
                routeTemplate: "api/celltrack",
                defaults: new { controller = "CellTrack", action = "Get" }
            );
            config.Routes.MapHttpRoute(
                name: "GetCellTrackv1Ext",
                routeTemplate: "api/celltrack.{ext}",
                defaults: new { ext = RouteParameter.Optional, controller = "CellTrack", action = "Get" }
            );

            config.Routes.MapHttpRoute(
                name: "GetCellTrackv2",
                routeTemplate: "api/v2/celltrack",
                defaults: new { controller = "CellTrack", action = "Getv2" }
            );
            config.Routes.MapHttpRoute(
                name: "GetCellTrackv2Ext",
                routeTemplate: "api/v2/celltrack.{ext}",
                defaults: new { ext = RouteParameter.Optional, controller = "CellTrack", action = "Getv2" }
            );

            #endregion 

            #region Nws Alert
            config.Routes.MapHttpRoute(
                name: "GetNwsAlertv1",
                routeTemplate: "api/nwsalert",
                defaults: new { controller = "NwsAlert", action = "Get" }
            );
            config.Routes.MapHttpRoute(
                name: "GetNwsAlertv1Ext",
                routeTemplate: "api/nwsalert.{ext}",
                defaults: new { ext = RouteParameter.Optional, controller = "NwsAlert", action = "Get" }
            );


            config.Routes.MapHttpRoute(
                name: "GetNwsAlertv2",
                routeTemplate: "api/v2/nwsalert",
                defaults: new { controller = "NwsAlert", action = "Getv2" }
            );
            config.Routes.MapHttpRoute(
                name: "GetNwsAlertv2Ext",
                routeTemplate: "api/v2/nwsalert.{ext}",
                defaults: new { ext = RouteParameter.Optional, controller = "NwsAlert", action = "Getv2" }
            );

            #endregion 

            #region Storm Report
            config.Routes.MapHttpRoute(
                name: "GetStormv1",
                routeTemplate: "api/storm",
                defaults: new { controller = "Storm", action = "GetAllStormData" }
            );
            config.Routes.MapHttpRoute(
                name: "GetStormv1Ext",
                routeTemplate: "api/storm.{ext}",
                defaults: new { ext = RouteParameter.Optional, controller = "Storm", action = "GetAllStormData" }
            );

            config.Routes.MapHttpRoute(
                name: "GetStormv2",
                routeTemplate: "api/v2/storm",
                defaults: new { controller = "Storm", action = "GetAllStormDatav2" }
            );
            config.Routes.MapHttpRoute(
                name: "GetStormv2Ext",
                routeTemplate: "api/v2/storm.{ext}",
                defaults: new { ext = RouteParameter.Optional, controller = "Storm", action = "GetAllStormDatav2" }
            );

            #endregion 

            #region Get All Stations
            config.Routes.MapHttpRoute(
                name: "GetAllStations",
                routeTemplate: "api/stations",
                defaults: new { controller = "Stations", action = "GetAllStations" }
                );
            config.Routes.MapHttpRoute(
                name: "GetAllStationsExt",
                routeTemplate: "api/stations.{ext}",
                defaults: new { controller = "Stations", action = "GetAllStations" }
                );

            config.Routes.MapHttpRoute(
                name: "GetAllStationsv2",
                routeTemplate: "api/v2/stations",
                defaults: new { controller = "Stations", action = "GetAllStationsv2" },
                constraints: new { httpMethod = new HttpMethodConstraint(HttpMethod.Get) }
                );

            config.Routes.MapHttpRoute(
                name: "GetStationNetworks",
                routeTemplate: "api/stationnetworks",
                defaults: new { controller = "Stations", action = "GetStationNetworks" }
                );

            config.Routes.MapHttpRoute(
                name: "GetStation",
                routeTemplate: "api/stations/{id}.{ext}",
                defaults: new { ext = RouteParameter.Optional, controller = "Stations", action = "GetStation" }
            );

            config.Routes.MapHttpRoute(
                name: "GetStationV2",
                routeTemplate: "api/v2/stations/{id}",
                defaults: new { controller = "Stations", action = "GetStationV2" },
                constraints: new { httpMethod = new HttpMethodConstraint(HttpMethod.Get) }
            );

            config.Routes.MapHttpRoute(
                name: "PostStationV2",
                routeTemplate: "api/v2/stations",
                defaults: new { controller = "Stations", action = "PostStationV2" },
                constraints: new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) }
            );

            config.Routes.MapHttpRoute(
                name: "PutStationV2",
                routeTemplate: "api/v2/stations/{id}",
                defaults: new { controller = "Stations", action = "PutStationV2" },
                constraints: new { httpMethod = new HttpMethodConstraint(HttpMethod.Put) }
            );

            config.Routes.MapHttpRoute(
                name: "DeleteStationV2",
                routeTemplate: "api/v2/stations/{id}",
                defaults: new { controller = "Stations", action = "DeleteStationV2" },
                constraints: new { httpMethod = new HttpMethodConstraint(HttpMethod.Delete) }
            );

            config.Routes.MapHttpRoute(
                name: "AddStationToNetwork",
                routeTemplate: "api/station/AddToNetwork/{stationId}",
                defaults: new { controller = "Stations", action = "AddStationToNetwork" }
            );

            config.Routes.MapHttpRoute(
                name: "RemoveStationFromNetwork",
                routeTemplate: "api/station/RemoveFromNetwork/{stationId}",
                defaults: new { controller = "Stations", action = "RemoveStationFromNetwork" }
            );

            config.Routes.MapHttpRoute(
               name: "GetAllStationsInNetwork",
               routeTemplate: "api/station/GetAllInNetwork/{networkId}",
               defaults: new { controller = "Stations", action = "GetAllStationsInNetwork" }
           );

            #endregion 

            #region Provisioning

            config.Routes.MapHttpRoute(
                name: "Provisioning",
                routeTemplate: "api/provisioning/{action}/{id}",
                defaults: new {id = RouteParameter.Optional, controller = "Provisioning", action = RouteParameter.Optional}
                );

            #endregion 

            #region Current Sensor Status
            config.Routes.MapHttpRoute(
                name: "GetCurrentSensorStatusv1Ext",
                routeTemplate: "api/currentsensorstatus/{id}.{ext}",
                defaults: new { ext = RouteParameter.Optional, controller = "CurrentSensorStatus", action = "GetStation" }
            );

            config.Routes.MapHttpRoute(
                name: "GetCurrentSensorStatusv2",
                routeTemplate: "api/v2/currentsensorstatus",
                defaults: new { controller = "CurrentSensorStatus", action = "GetStationV2" }
            );

            #endregion

            #region Sensor Detection
            config.Routes.MapHttpRoute(
                name: "GetSensorDetectionv1Ext",
                routeTemplate: "api/sensordetection/{id}.{ext}",
                defaults: new { ext = RouteParameter.Optional, controller = "SensorDetection", action = "Get" }
            );

            config.Routes.MapHttpRoute(
                name: "GetSensorDetectionv2",
                routeTemplate: "api/v2/sensordetection",
                defaults: new { controller = "SensorDetection", action = "GetV2" }
            );

            #endregion

            #region Sensor Diagnostic
            config.Routes.MapHttpRoute(
                name: "GetSensorDiagnosticv1Ext",
                routeTemplate: "api/sensordiagnostic/{id}.{ext}",
                defaults: new { ext = RouteParameter.Optional, controller = "SensorDiagnostic", action = "Get" }
            );

            config.Routes.MapHttpRoute(
                name: "GetSensorDiagnosticv2",
                routeTemplate: "api/v2/sensordiagnostic",
                defaults: new { controller = "SensorDiagnostic", action = "GetV2" }
            );

            #endregion

            #region Sensor Packet History
            config.Routes.MapHttpRoute(
                name: "GetSensorPacketHistoryv1Ext",
                routeTemplate: "api/sensorpackethistory/{id}.{ext}",
                defaults: new { ext = RouteParameter.Optional, controller = "SensorPacketHistory", action = "Get" }
            );

            config.Routes.MapHttpRoute(
                name: "GetSensorPacketHistoryv2",
                routeTemplate: "api/v2/sensorpackethistory",
                defaults: new { controller = "SensorPacketHistory", action = "GetV2" }
            );

            #endregion

            //v1 
            config.Routes.MapHttpRoute("ControllerExt", "api/{controller}.{ext}");
            config.Routes.MapHttpRoute("ControllerIdExt", "api/{controller}/{id}.{ext}");
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

        }
    }
}
