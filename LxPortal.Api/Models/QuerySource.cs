﻿using System;
using LxPortalLib.Common;

namespace LxPortal.Api.Models
{
    public class QuerySource : IEquatable<QuerySource>
    {
        public bool UseDataMart { get; set; }
        public bool UseDataMartArchive { get; set; }
        public long DataMartStartKey { get; set; }
        public long DataMartEndKey { get; set; }
        public long DataMartArchiveStartKey { get; set; }
        public long DataMartArchiveEndKey { get; set; }

        public static QuerySource GetQuerySource(DateTime start, DateTime end, DateTime utcNow, int dataMartAgeHours, bool dataMartArchiveEnabled, long dataMartArchiveEndDateKey, long dataMartStartDateKey)
        {

            QuerySource qs = new QuerySource
                {
                    UseDataMart = true,
                    DataMartStartKey = Utilities.GetDateKey(start),
                    DataMartEndKey = Utilities.GetDateKey(end)
                };
            
            qs = CheckDataMartArchiveUsage(qs, dataMartArchiveEnabled, dataMartArchiveEndDateKey, dataMartStartDateKey);

            return qs;
        }

        private static QuerySource CheckDataMartArchiveUsage(QuerySource qs, bool dataMartArchiveEnabled, long dataMartArchiveEndDateKey, long dataMartStartDateKey)
        {
            //only going to check for data mart archive usage if we are already using data mart
            if (qs.UseDataMart && dataMartArchiveEnabled && qs.DataMartStartKey <= dataMartArchiveEndDateKey)
            {
                qs.UseDataMartArchive = true;
                qs.DataMartArchiveStartKey = qs.DataMartStartKey;

                if (qs.DataMartEndKey <= dataMartArchiveEndDateKey)
                {
                    qs.DataMartArchiveEndKey = qs.DataMartEndKey;
                    qs.UseDataMart = false;
                }
                else
                {
                    qs.DataMartArchiveEndKey = dataMartArchiveEndDateKey;
                    qs.DataMartStartKey = dataMartStartDateKey;
                }
            }
            else
                qs.UseDataMartArchive = false;

            return qs;
        }

        public bool Equals(QuerySource other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return UseDataMart.Equals(other.UseDataMart) && UseDataMartArchive.Equals(other.UseDataMartArchive) && DataMartStartKey == other.DataMartStartKey && DataMartEndKey == other.DataMartEndKey && DataMartArchiveStartKey == other.DataMartArchiveStartKey && DataMartArchiveEndKey == other.DataMartArchiveEndKey;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((QuerySource) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = UseDataMart.GetHashCode();
                hashCode = (hashCode * 397) ^ UseDataMartArchive.GetHashCode();
                hashCode = (hashCode * 397) ^ DataMartStartKey.GetHashCode();
                hashCode = (hashCode * 397) ^ DataMartEndKey.GetHashCode();
                hashCode = (hashCode * 397) ^ DataMartArchiveStartKey.GetHashCode();
                hashCode = (hashCode * 397) ^ DataMartArchiveEndKey.GetHashCode();
                return hashCode;
            }
        }
    }   
}