﻿using System;
using System.Collections;
using System.Configuration;
using System.Data.SqlClient;

using Aws.Core.Data;

using LxPortal.Api.Models.Database;

namespace LxPortal.Api.Models
{
    public class HealthCheck
    {
        public string System { get; set; }
        public Uri CheckUri { get; set; }
        public string State { get; set; }
        public string Message { get; set; }
        public bool IsSuccess { get; set; }
    }

    public class HealthCheckRepositoryDb   
    {
        public HealthCheck Get()
        {
            HealthCheck hc = new HealthCheck { State = "Failure", Message = string.Empty, IsSuccess = false, System = "API" };
            SqlConnection conn = null;
            DbCommand dbCmd = null;
            try
            {
                conn = DbConnectionFactory.Build(ConfigurationManager.ConnectionStrings["Lightning"].ConnectionString);
                dbCmd = new DbCommand(conn, new HealthCheckDbCmdText());
                Hashtable obj = dbCmd.ExecuteFirstRow() as Hashtable;
                if (obj != null && obj.Count > 0)
                {
                    hc.State = "Success";
                    hc.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                hc.Message = ex.Message;
            }
            finally
            {
                if (null != dbCmd) dbCmd.Close();
                if (null != conn) conn.Close();
            }

            return hc;
        }
    }
}