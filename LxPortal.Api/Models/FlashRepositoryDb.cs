﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;

using Aws.Core.Data;
using Aws.Core.Utilities;

using LxCommon.Monitoring;

using LxPortalLib.Models;

using LxPortal.Api.Models.Database;

namespace LxPortal.Api.Models
{
    public class FlashRepositoryDb : FlashBaseRepository
    {
     
        protected override List<FlashRecord> GetDmByBoundingBox(DateTime start, DateTime end, StrokeType type, decimal ullat, decimal ullon, decimal lrlat, decimal lrlon, string connectionName)
        {
            SqlConnection conn = null;
            SqlDataReader dr = null;
            DbCommand dbCmd = null;
            List<FlashRecord> flashes = null;

            try
            {
                conn = DbConnectionFactory.Build(ConfigurationManager.ConnectionStrings[connectionName].ConnectionString);
                FlashDmGetDmCmdText dbCmdText = new FlashDmGetDmCmdText(start, end, type, ullat, ullon, lrlat, lrlon);
                dbCmd = new DbCommand(conn, dbCmdText);
                dbCmd.Command.CommandTimeout = Config.DbCommandTimeoutMilliseconds;
                dr = dbCmd.ExecuteReader();
                flashes = new List<FlashRecord>(DefaultCapacity);
                FlashRecord flash = null;
                while (dr.Read())
                {
                    flash = dbCmdText.ParseFlashRow(dr);
                    if (null != flash)
                        flashes.Add(flash);
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.ApiFlashGet, string.Format("Unable to get flashes by bounding box. Message: {0}", ex.Message), ex);
            }
            finally
            {
                if (null != dbCmd) dbCmd.Close();
                if (null != dr) dr.Close();
                if (null != conn) conn.Close();
            }

            return flashes;
        }

        protected override List<FlashRecord> GetDmByRadius(DateTime start, DateTime end, StrokeType type, decimal lat, decimal lon, int radius, string connectionName)
        {
            SqlConnection conn = null;
            SqlDataReader dr = null;
            DbCommand dbCmd = null;
            List<FlashRecord> flashes = null;

            try
            {
                conn = DbConnectionFactory.Build(ConfigurationManager.ConnectionStrings[connectionName].ConnectionString);
                FlashDmGetCircleDbCmdText dbCmdText = new FlashDmGetCircleDbCmdText(start, end, type, lat, lon, radius);
                dbCmd = new DbCommand(conn, dbCmdText);
                dbCmd.Command.CommandTimeout = Config.DbCommandTimeoutMilliseconds;
                dr = dbCmd.ExecuteReader();
                flashes = new List<FlashRecord>(DefaultCapacity);
                FlashRecord flash = null;
                while (dr.Read())
                {
                    flash = dbCmdText.ParseFlashRow(dr);
                    if (null != flash)
                        flashes.Add(flash);
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.ApiFlashGetRadius, string.Format("Unable to get flashes by radius. Message: {0}", ex.Message), ex);
            }
            finally
            {
                if (null != dbCmd) dbCmd.Close();
                if (null != dr) dr.Close();
                if (null != conn) conn.Close();
            }

            return flashes;
        }
    }
}