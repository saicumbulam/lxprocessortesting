﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LxPortal.Api.Models.Database
{
    public class StationGetAllDbCmdText : StationBaseDbCmdText
    {
        public StationGetAllDbCmdText()
        {
            InitSP();
        }

        public override string BuildCmdText()
        {
            return "dbo.GetStationList_pr";
        }

    }
}