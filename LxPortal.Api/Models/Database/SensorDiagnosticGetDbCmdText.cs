﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Aws.Core.Data;
using Aws.Core.Utilities;
using LxPortalLib.Models;

namespace LxPortal.Api.Models.Database
{
    public class SensorDiagnosticGetDbCmdText : DbCommandText
    {
        private string _id;
        private DateTime? _start = null, _end = null;
        public SensorDiagnosticGetDbCmdText(string id, DateTime? start, DateTime? end)
        {
            _id = id;
            _start = start;
            _end = end;
            InitSP(); 
        }

        public override string BuildCmdText()
        {
            return "dbo.GetSensorLog_pr";
        }

        public override ArrayList BuildParameters()
        {
            AddParam("@Sensor_ID", _id, SqlDbType.VarChar, 10);
            if (_start.HasValue)
                AddParam("@StartTimeUTC", _start.Value, SqlDbType.DateTime);

            if (_end.HasValue)
                AddParam("@EndTimeUTC", _end.Value, SqlDbType.DateTime);

            return ParamList;
        }

        public override object ParseRow(SqlDataReader dr)
        {
            //byte[] data = dr["LogText"] as byte[]; 
            return new SensorDiagnosticData
            {
                TimestampUtc = DbHelper.GetDataNullableUtcDateTime(dr, "LastModifiedDate"),
                //Value = Encoding.UTF8.GetString(data, 0, data.Count())
                Value = GetDataString(dr, "LogText")
            };
        }

    }
}
