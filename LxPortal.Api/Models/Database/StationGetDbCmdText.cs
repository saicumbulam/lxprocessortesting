﻿using System.Collections;

namespace LxPortal.Api.Models.Database
{
    public class StationGetDbCmdText : StationBaseDbCmdText
    {
        private string _id;
        public StationGetDbCmdText(string id)
        {
            _id = id;
            InitSP();
        }

        public override string BuildCmdText()
        {
            return "dbo.GetStation_pr";
        }

        public override ArrayList BuildParameters()
        {
            AddParam("@Id", _id, System.Data.SqlDbType.VarChar, 10);

            return ParamList;
        }
    }
}