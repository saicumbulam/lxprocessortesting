﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

using Microsoft.SqlServer.Types;

using Aws.Core.Data;

using LxPortalLib.Common;
using LxPortalLib.Models;

using LxPortal.Api.Helper;

namespace LxPortal.Api.Models.Database
{
    public class CellTrackDmDbGetCmdText : DbCommandText
    {
        private long _start, _end;

        private SqlGeography _boundingBox;

        public CellTrackDmDbGetCmdText(long start, long end, decimal ullat, decimal ullon, decimal lrlat, decimal lrlon)
        {
            _start = start;
            _end = end;
            _boundingBox = SqlGeographyUtil.CreateSqlBoundingBox(ullat, ullon, lrlat, lrlon);

            InitSP();
        }

        public override string BuildCmdText()
        {
            return "dbo.GetTrackBoundingBox_pr";
        }

        public override ArrayList BuildParameters()
        {
            AddParam(new SqlParameter("@BoundingBox", _boundingBox) { UdtTypeName = "Geography" });
            AddParam("@DateKeyFrom", _start, SqlDbType.BigInt);
            AddParam("@DateKeyTo", _end, SqlDbType.BigInt);

            return ParamList;
        }

        public override object ParseRow(SqlDataReader dr)
        {
            CellTrackRecord track = null;

            string trackLocation = (dr["TrackLocation"] != null) ? dr["TrackLocation"].ToString() : null;
            string cellLocation = (dr["CellLocation"] != null) ? dr["CellLocation"].ToString() : null;
            string cellCentroid = (dr["CellCentroid"] != null) ? dr["CellCentroid"].ToString() : null;


            if (!string.IsNullOrEmpty(trackLocation) && !string.IsNullOrEmpty(cellLocation))
            {
                track = new CellTrackRecord
                {
                    CellId = GetDataString(dr, "CellId"),
                    CellTimeUtc = DbHelper.GetDataUtcDateTime(dr, "CellTimeUtc"),
                    TrackLocation = trackLocation,
                    Type = GetDataString(dr, "TrackType"),
                    CellLocation = cellLocation,
                    Area = GetDataDecimal(dr, "Area"),
                    Direction = Convert.ToDecimal(GetDataDouble(dr, "Direction")),
                    Speed = GetDataDecimal(dr, "Speed"),
                    InCloudFlashRate = GetDataDecimal(dr, "InCloudFlashRate"),
                    InCloudFlashRateChange = GetDataDecimal(dr, "InCloudFlashRateChange"),
                    CloudGroundFlashRate = GetDataDecimal(dr, "CloudGroundFlashRate"),
                    CloudGroudFlashRateChange = GetDataDecimal(dr, "CloudGroundFlashRateChange"),
                };
            }
            if (track != null && !string.IsNullOrEmpty(cellCentroid))
            {
                track.CellCentroid = cellCentroid;
            }
            return track;
        }
    }
}