﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Aws.Core.Data;
using Aws.Core.Utilities;
using LxPortalLib.Models;

namespace LxPortal.Api.Models.Database
{
    public class SensorDetectionGetDbCmdText : DbCommandText
    {
        private string _id;
        private DateTime? _start = null, _end = null;
        public SensorDetectionGetDbCmdText(string id, DateTime? start, DateTime? end)
        {
            _id = id;
            _start = start;
            _end = end;
            InitSP(); 
        }

        public override string BuildCmdText()
        {
            return "dbo.GetSensorStatistics_pr";
        }

        public override ArrayList BuildParameters()
        {
            AddParam("@Sensor_ID", _id, SqlDbType.VarChar, 10);
            if (_start.HasValue)
                AddParam("@StartTimeUTC", _start.Value, SqlDbType.DateTime);

            if (_end.HasValue)
                AddParam("@EndTimeUTC", _end.Value, SqlDbType.DateTime);

            return ParamList;
        }

        public override object ParseRow(SqlDataReader dr)
        {
            DateTime dt;
            DateTime.TryParse((string)dr["LastFlashTime"], out dt);
            DateTime? dtNullable = dt;
            if (dt == DateTime.MinValue)
                dtNullable = null;
            else
            {
                dtNullable = DateTime.SpecifyKind(dtNullable.Value, DateTimeKind.Utc);
            }
            
            return new DetectionRecordData
            {
                TimestampUtc = DbHelper.GetDataNullableUtcDateTime(dr, "LastModifiedDate"),
                LastDetectionUtc = dtNullable,
                LastLatitude =  DbHelper.GetDataNullableDecimal(dr, "LastFlashLatitude"),
                LastLongitude = DbHelper.GetDataNullableDecimal(dr, "LastFlashLongitude"),
                LastPeakCurrent = DbHelper.GetDataNullableDecimal(dr, "LastFlashAmplitude"),
                TotalLast24Hours = DbHelper.GetDataNullableInt(dr, "TotalFlashSeenInLast24Hours")
            };
        }

    }
}
