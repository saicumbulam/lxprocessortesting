﻿namespace LxPortal.Api.Models.Database
{
    public class TlnStationGetAllDbCmdText : StationBaseDbCmdText
    {
        public TlnStationGetAllDbCmdText()
        {
            InitSP();
        }

        public override string BuildCmdText()
        {
            return "dbo.TlnStationListGet_pr"; 
        }
    }
}