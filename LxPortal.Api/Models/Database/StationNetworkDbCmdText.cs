﻿using System.Data.SqlClient;
using Aws.Core.Data;
using LxPortalLib.Models;

namespace LxPortal.Api.Models.Database
{
    public class StationNetworkDbCmdText : DbCommandText
    {
        public StationNetworkDbCmdText()
        {
            InitSP();
        }

        public override string BuildCmdText()
        {
            return "dbo.GetStationNetworks_pr";
        }

        public override object ParseRow(SqlDataReader dr)
        {
            Network network = new Network
            {
                Id = GetDataString(dr, "LtgNetworkID").Trim(),
                Name = GetDataString(dr, "LtgNetworkName"),
                Description = GetDataString(dr, "Description", null)
            };

            return network;
        }
    }
}