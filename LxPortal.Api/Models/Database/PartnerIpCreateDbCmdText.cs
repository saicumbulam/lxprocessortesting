﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Aws.Core.Data;

using LxPortal.Api.Helper;
using LxPortal.Api.Models;
using LxPortalLib.Models;
using LxPortalLib.Common;

using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace LxPortal.Api.Models.Database
{
    public class PartnerIpCreateDbCmdText : DbCommandText
    {
        private string _ipAddress;
        private PartnerRecord _partner;

        public PartnerIpCreateDbCmdText(PartnerRecord partner, string ipAddress)
        {
            _partner = partner;
            _ipAddress = ipAddress;
            InitSP();
        }

        public override string BuildCmdText()
        {
            return "bizops.AddPartnerIpWhitelist";
        }

        public override ArrayList BuildParameters()
        {
            AddParam("@partnerId", _partner.PartnerId, SqlDbType.VarChar, 50);
            AddParam("@ipAddress", _ipAddress, SqlDbType.VarChar);
            AddParam("@lastModifiedBy", _partner.LastModifiedBy, SqlDbType.VarChar, 100);

            return ParamList;
        }

        //public PartnerRecord ParseRowPartner(SqlDataReader dr)
        //{
        //    PartnerRecord partner = null;

        //    string id = GetDataString(dr, "PartnerId", null);
        //    if (!string.IsNullOrEmpty(id))
        //    {
        //        id = id.ToUpper();
        //        partner = new PartnerRecord
        //        {
        //            PartnerId = id,
        //            IpAddress = GetDataString(dr, "@ipAddress", null),

        //        };
        //    }

        //    return partner;
        //}

    }
}