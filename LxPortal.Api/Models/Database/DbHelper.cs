﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Aws.Core.Utilities;

namespace LxPortal.Api.Models.Database
{
    public class DbHelper
    {
        public static DateTime? GetDataNullableUtcDateTime(SqlDataReader dr, string columnName)
        {
            DateTime? dt = null;
            try
            {
                dt = (DBNull.Value != dr[columnName] ? (DateTime?)dr[columnName] : null);
                if (dt.HasValue)
                    dt = DateTime.SpecifyKind(dt.Value, DateTimeKind.Utc);
            }
            catch (Exception ex)
            {
                EventManager.LogWarning(EventLogger.Code.Data.FailureReadingDataColumnInDbCommandText, "Failure reading DATETIME column '" + columnName + "' from the resultset.", ex);
            }

            return dt;
        }

        public static DateTime GetDataUtcDateTime(SqlDataReader dr, string columnName)
        {
            return GetDataUtcDateTime(dr, columnName, DateTime.MinValue);
        }

        public static DateTime GetDataUtcDateTime(SqlDataReader dr, string columnName, DateTime defaultValue)
        {
            DateTime dt = defaultValue;
            try
            {
                dt = (DBNull.Value != dr[columnName] ? (DateTime)dr[columnName] : defaultValue);
                if (dt.Kind != DateTimeKind.Utc)
                    dt = DateTime.SpecifyKind(dt, DateTimeKind.Utc);
            }
            catch (Exception ex)
            {
                EventManager.LogWarning(EventLogger.Code.Data.FailureReadingDataColumnInDbCommandText, "Failure reading DATETIME column '" + columnName + "' from the resultset.", ex);
            }

            return dt;
        }


        public static short? GetDataNullableShort(SqlDataReader dr, string columnName)
        {
            try
            {
                return (DBNull.Value != dr[columnName] ? (short?)dr[columnName] : null);
            }
            catch (Exception ex)
            {
                EventManager.LogWarning(EventLogger.Code.Data.FailureReadingDataColumnInDbCommandText, "Failure reading int column '" + columnName + "' from the resultset.", ex);
                return null;
            }
        }

        public static int? GetDataNullableInt(SqlDataReader dr, string columnName)
        {
            try
            {
                return (DBNull.Value != dr[columnName] ? (int?)dr[columnName] : null);
            }
            catch (Exception ex)
            {
                EventManager.LogWarning(EventLogger.Code.Data.FailureReadingDataColumnInDbCommandText, "Failure reading int column '" + columnName + "' from the resultset.", ex);
                return null;
            }
        }

        public static decimal? GetDataNullableDecimal(SqlDataReader dr, string columnName)
        {
            try
            {
                return (DBNull.Value != dr[columnName] ? (decimal?)dr[columnName] : null);
            }
            catch (Exception ex)
            {
                EventManager.LogWarning(EventLogger.Code.Data.FailureReadingDataColumnInDbCommandText, "Failure reading int column '" + columnName + "' from the resultset.", ex);
                return null;
            }
        }

        public static DateTime ParseDateTime(SqlDataReader dr, string columnName)
        {
            DateTime dt = DateTime.MinValue;
            try
            {
                string ds = (DBNull.Value != dr[columnName] ? (string)dr[columnName] : null);
                DateTime.TryParse(ds, out dt);
                    
            }
            catch (Exception ex)
            {
                EventManager.LogWarning(EventLogger.Code.Data.FailureReadingDataColumnInDbCommandText, "Failure reading int column '" + columnName + "' from the resultset.", ex);   
            }
            return dt;
        }

    }
}