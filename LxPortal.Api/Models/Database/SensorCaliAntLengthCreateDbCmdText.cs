﻿using System.Collections;
using System.Data;
using Aws.Core.Data;

namespace LxPortal.Api.Models.Database
{
    public class SensorCaliAntLengthCreateDbCmdText : DbCommandText
    {
        private string _id;
        private int _calibrationSetting;
        public SensorCaliAntLengthCreateDbCmdText(string id, int calibrationSetting)
        {
            _id = id;
            _calibrationSetting = calibrationSetting;
            InitSP();
        }

        public override string BuildCmdText()
        {
            return "dbo.InsertLtgCalibration_pr";
        }

        public override ArrayList BuildParameters()
        {
            AddParam("@Sensor_ID", _id, SqlDbType.VarChar, 10);
            AddParam("@CalibrationSetting", _calibrationSetting, SqlDbType.Int);
            return ParamList;
        }
    }
}
