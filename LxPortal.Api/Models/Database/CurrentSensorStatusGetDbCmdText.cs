﻿using System;
using System.Collections;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Aws.Core.Data;
using Aws.Core.Utilities;
using LxPortalLib.Models;

namespace LxPortal.Api.Models.Database
{
    public class CurrentSensorStatusGetDbCmdText : DbCommandText
    {
        private string _id;
        public CurrentSensorStatusGetDbCmdText(string id)
        {
            _id = id;
            InitSP();
        }

        public override string BuildCmdText()
        {
            if (Config.SystemType == "TLN")
                return "dbo.TlnCurrentSensorStatusGet_pr";
            else
                return "dbo.CurrentSensorStatusGet_pr";
        }

        public override ArrayList BuildParameters()
        {
            AddParam("@Id", _id, System.Data.SqlDbType.VarChar, 10);

            return ParamList;
        }

        internal CurrentSensorStatus Parse(SqlDataReader dr)
        {
            CurrentSensorStatus status = null;
            PacketRecord packet = null;
            HardwareRecord hardware = null;
            GpsRecord gps = null;
            DetectionRecordData detection = null;

            int resultSet = 0;
            do
            {
                while (dr.Read())
                {
                    switch (resultSet)
                    {
                        case 0:
                            Tuple<PacketRecord, HardwareRecord> packHard = ParsePacket(dr);
                            if (packHard != null)
                            {
                                packet = packHard.Item1;
                                hardware = packHard.Item2;
                            }
                            break;
                        case 1:
                            gps = ParseGps(dr);
                            break;
                        case 2:
                            detection = ParseDetection(dr);
                            break;

                    }
                }

                resultSet++;
                if (!dr.NextResult())
                    break;

            } while (true);

            if (packet != null || hardware != null || gps != null || detection != null)
            {
                status = new CurrentSensorStatus
                {
                    Id = _id,
                    Packet = packet,
                    Hardware = hardware,
                    Gps = gps,
                    Detection = detection
                };
            }

            return status;
        }

        private DetectionRecordData ParseDetection(SqlDataReader dr)
        {

            DateTime lft;
            DateTime? lftn = null;

            if (DateTime.TryParse(GetDataString(dr, "LastFlashTime"), out lft))
                lftn = DateTime.SpecifyKind(lft, DateTimeKind.Utc);

            return new DetectionRecordData
            {
                LastDetectionUtc = lftn,
                LastLatitude = DbHelper.GetDataNullableDecimal(dr, "LastFlashLatitude"),
                LastLongitude = DbHelper.GetDataNullableDecimal(dr, "LastFlashLongitude"),
                TotalLast24Hours =
                    (lftn.HasValue && ((DateTime.UtcNow - lftn.Value).TotalHours > 24))
                        ? 0
                        : DbHelper.GetDataNullableInt(dr, "TotalFlashSeenInLast24Hours")
            };
        }

        private GpsRecord ParseGps(SqlDataReader dr)
        {
            DateTime sensorTimeUtc = TimeSvc.GetTime(GetDataInt(dr, "SecondsSinceEpoch"));
            sensorTimeUtc = DateTime.SpecifyKind(sensorTimeUtc, DateTimeKind.Utc);
            
            return new GpsRecord
            {
                LastUpdateUtc = DbHelper.GetDataNullableUtcDateTime(dr, "LastModifiedDate"),
                VisibleSatellites = DbHelper.GetDataNullableInt(dr, "VisibleSatellites"),
                TrackedSatellites = DbHelper.GetDataNullableInt(dr, "TrackedSatellites"),
                Latitude = DbHelper.GetDataNullableDecimal(dr, "Latitude"),
                Longitude = DbHelper.GetDataNullableDecimal(dr, "Longitude"),
                Altitude = DbHelper.GetDataNullableDecimal(dr, "Altitude"),
                SensorTimeUtc = sensorTimeUtc
            };
        }

        private Tuple<PacketRecord, HardwareRecord> ParsePacket(SqlDataReader dr)
        {
            decimal? packetSizeKb = null;
            int? packetSizeBytes = DbHelper.GetDataNullableInt(dr, "TotalBytes");
            if (packetSizeBytes.HasValue)
                packetSizeKb = packetSizeBytes.Value/1024m;



            PacketRecord packet = new PacketRecord
            {
                LastPacketUtc = DbHelper.GetDataNullableUtcDateTime(dr, "TimeLastSuccessfulPacketReceived"),
                ReceivedPackets = DbHelper.GetDataNullableInt(dr, "TotalReceivedPackets"),
                PacketRate = DbHelper.GetDataNullableInt(dr, "PacketRatePercent"),
                TotalPacketSizeKilobytes = packetSizeKb,
                MinPacketSizeBytes = DbHelper.GetDataNullableInt(dr, "MinPacketSize"),
                MaxPacketSizeBytes = DbHelper.GetDataNullableInt(dr, "MaxPacketSize"),
                FirstPacketUtc = DbHelper.GetDataNullableUtcDateTime(dr, "TimeFirstPacketReceived"),
                EmptyPackets = DbHelper.GetDataNullableInt(dr, "EmptyPackets"),
                MinPacketLatency = DbHelper.GetDataNullableInt(dr, "MinPacketLatency"),
                MaxPacketLatency = DbHelper.GetDataNullableInt(dr, "MaxPacketLatency"),
                MedianPacketLatency = DbHelper.GetDataNullableInt(dr, "MedianPacketLatency"),
                MinLFThreshold = DbHelper.GetDataNullableInt(dr, "MinLFThreshold"),
                MaxLFThreshold = DbHelper.GetDataNullableInt(dr, "MaxLFThreshold"),
                MedianLFThreshold = DbHelper.GetDataNullableInt(dr, "MedianLFThreshold"),
                MinHFThreshold = DbHelper.GetDataNullableInt(dr, "MinHFThreshold"),
                MaxHFThreshold = DbHelper.GetDataNullableInt(dr, "MaxHFThreshold"),
                MedianHFThreshold = DbHelper.GetDataNullableInt(dr, "MedianHFThreshold")
            };

            if (packet.PacketRate.HasValue && packet.PacketRate > LxPortalLib.Config.PacketPercentThreshold)
            {
                packet.Status = 1;
            }
            else
            {
                packet.Status = 3;
            }

            CheckPacketRecord(packet); 

            HardwareRecord hardware = new HardwareRecord
            {
                LastCallHomeUtc = DbHelper.GetDataNullableUtcDateTime(dr, "LastAccess"),
                EnnaVersion = GetDataString(dr, "MCU_FWVersion"),
                EndspVersion = GetDataString(dr, "LTREmote_FWVersion")
            };

            if (Config.SystemType == "TLS")
            {
                hardware.MemBackUpVoltage = DbHelper.GetDataNullableDecimal(dr, "MemBackUpVoltage");
                hardware.BackUpBatteryVol = DbHelper.GetDataNullableDecimal(dr, "BackUpBatteryVol");
            }


            return new Tuple<PacketRecord,HardwareRecord>(packet, hardware);    
        }

        private void CheckPacketRecord(PacketRecord packetRecord)
        {
            //if last packet received is greater than 6 set packet rec to 0
            if (packetRecord.LastPacketUtc.HasValue)
            {
                TimeSpan ts = DateTime.UtcNow - packetRecord.LastPacketUtc.Value;

                if (ts.TotalMinutes > Config.LastPacketReceivedInterval)
                {
                    packetRecord.LastPacketUtc = packetRecord.LastPacketUtc;
                    packetRecord.ReceivedPackets = 0;
                    packetRecord.PacketRate = 0;
                    packetRecord.TotalPacketSizeKilobytes = 0;
                    packetRecord.MinPacketSizeBytes = 0;
                    packetRecord.MaxPacketSizeBytes = 0;
                    packetRecord.FirstPacketUtc = packetRecord.FirstPacketUtc;
                    packetRecord.EmptyPackets = 0;
                    packetRecord.MinPacketLatency = 0;
                    packetRecord.MaxPacketLatency = 0;
                    packetRecord.MedianPacketLatency = 0;
                    packetRecord.MinLFThreshold = 0;
                    packetRecord.MaxLFThreshold = 0;
                    packetRecord.MedianLFThreshold = 0;
                    packetRecord.MinHFThreshold = 0;
                    packetRecord.MaxHFThreshold = 0;
                    packetRecord.MedianHFThreshold = 0;
                    packetRecord.Status = null;
                } 
            }
        }
    }
}