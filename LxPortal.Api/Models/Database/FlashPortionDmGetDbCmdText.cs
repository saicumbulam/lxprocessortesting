﻿using System;
using System.Collections;
using System.Data;
using LxPortalLib.Models;

namespace LxPortal.Api.Models.Database
{

    public class FlashPortionDmGetDbCmdText : FlashBaseDbCmdText
    {
        private DateTime _start, _end;
        decimal _ullat, _ullon, _lrlat, _lrlon;
        StrokeType _type;

        public FlashPortionDmGetDbCmdText(DateTime start, DateTime end, StrokeType type, decimal ullat, decimal ullon, decimal lrlat, decimal lrlon)
        {
            _start = start;
            _end = end;
            _ullat = ullat;
            _ullon = ullon;
            _lrlat = lrlat;
            _lrlon = lrlon;
            if (type != StrokeType.None)
            {
                _type = type;
            }
            else
            {
                _type = StrokeType.None;
            }

            InitSP();
        }

        public override ArrayList BuildParameters()
        {
            AddParam("@DateTimeFrom", _start, SqlDbType.DateTime);
            AddParam("@DateTimeTo", _end, SqlDbType.DateTime);
            AddParam("@LatitudeFrom", _lrlat, SqlDbType.Decimal, 9);
            AddParam("@LatitudeTo", _ullat, SqlDbType.Decimal, 9);
            AddParam("@LongitudeFrom", _ullon, SqlDbType.Decimal, 9);
            AddParam("@LongitudeTo", _lrlon, SqlDbType.Decimal, 9);
            if (_type != StrokeType.None)
            {
                AddParam("@StrokeType", _type, SqlDbType.BigInt);
            }

            return ParamList;
        }

        public override string BuildCmdText()
        {
            return "dbo.GetFlashPortion_pr";
        }
    }
}