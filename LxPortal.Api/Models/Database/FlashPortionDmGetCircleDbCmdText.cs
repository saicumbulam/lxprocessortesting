﻿using System;
using System.Collections;
using System.Data;
using LxPortalLib.Models;

namespace LxPortal.Api.Models.Database
{
    public class FlashPortionDmGetCircleDbCmdText : FlashBaseDbCmdText
    {
        private DateTime _start, _end;
        private decimal _lat, _lon;
        private int _radius;
        private StrokeType _type;

        public FlashPortionDmGetCircleDbCmdText(DateTime start, DateTime end, StrokeType type, decimal lat, decimal lon, int radius)
        {
            _start = start;
            _end = end;
            _lat = lat;
            _lon = lon;
            _radius = radius;
            if (type != StrokeType.None)
            {
                _type = type; 
            }
            else
            {
                _type = StrokeType.None;
            }

            InitSP();
        }

        public override ArrayList BuildParameters()
        {
            AddParam("@Longitude", _lon, SqlDbType.Decimal, 9);
            AddParam("@Latitude", _lat, SqlDbType.Decimal, 9);
            AddParam("@Radius", _radius, SqlDbType.Int);
            AddParam("@DateTimeFrom", _start, SqlDbType.DateTime);
            AddParam("@DateTimeTo", _end, SqlDbType.DateTime); 

            if (_type != StrokeType.None)
            {
                AddParam("@StrokeType", _type, SqlDbType.BigInt);
            }

            return ParamList;
        }

        public override string BuildCmdText()
        {
            return "dbo.GetFlashPortionCircle_pr";
        }
    }
}