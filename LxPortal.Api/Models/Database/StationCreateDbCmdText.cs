﻿using System.Collections;
using System.Data;
using System.Data.SqlClient;
using LxPortalLib.Models;

namespace LxPortal.Api.Models.Database
{
    public class StationCreateDbCmdText : StationBaseDbCmdText
    {
        private Station _station;

        public StationCreateDbCmdText(Station station)
        {
            _station = station;
            InitSP();
        }

        public override string BuildCmdText()
        {
            return "dbo.InsertStation_pr";
        }

        public override ArrayList BuildParameters()
        {
            AddParam("@Id", _station.Id, SqlDbType.VarChar, 10);
            AddParam("@Name", _station.Name, SqlDbType.NVarChar, 50);
            AddParam("@StreetAddress", _station.StreetAddress, SqlDbType.NVarChar, 250);
            AddParam("@Country", _station.Country, SqlDbType.NVarChar, 30);
            AddParam("@Province", _station.Province, SqlDbType.NVarChar, 50);
            AddParam("@Region", _station.Region, SqlDbType.NVarChar, 50);
            AddParam("@City", _station.City, SqlDbType.NVarChar, 30);
            AddParam("@PostalCode", _station.PostalCode, SqlDbType.NVarChar, 20);
            AddParam("@SerialNumber", _station.SerialNumber, SqlDbType.VarChar, 15);
            AddParam("@PocName", _station.PocName, SqlDbType.NVarChar, 50);
            AddParam("@PocPhone", _station.PocPhone, SqlDbType.NVarChar, 20);
            AddParam("@PocEmail", _station.PocEmail, SqlDbType.NVarChar, 250);
            AddParam("@PrimaryDeliveryIp", _station.PrimaryDeliveryIp, SqlDbType.VarChar, 50);
            AddParam("@SecondaryDeliveryIp", _station.SecondaryDeliveryIp, SqlDbType.VarChar, 50);
            AddParam("@SendLogRateMinutes", _station.SendLogRateMinutes, SqlDbType.Int);
            AddParam("@SendGpsRateMinutes", _station.SendGpsRateMinutes, SqlDbType.Int);
            AddParam("@SendAntennaGainRateMinutes", _station.SendAntennaGainRateMinutes, SqlDbType.Int);
            AddParam("@AntennaMode", _station.AntennaMode, SqlDbType.Int);
            AddParam("@AntennaAttenuation", _station.AntennaAttenuation, SqlDbType.Int);
            AddParam("@Note", _station.Note, SqlDbType.NVarChar, 1000);
            AddParam("@QCFlag", (_station.IsEnable)? 100 : 0, SqlDbType.Int);

            return ParamList;
        }
    }
}