﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Aws.Core.Data;
using LxPortalLib.Models;

namespace LxPortal.Api.Models.Database
{
    public class PartnerGetDbCmdText : DbCommandText
    {
        private string _partnerId;

        public PartnerGetDbCmdText(string id)
        {
            _partnerId = id;
            InitSP();
        }

        public override string BuildCmdText()
        {
            return "bizops.GetPartner"; 
        }

        public override ArrayList BuildParameters()
        {
            AddParam("@partnerId", _partnerId, SqlDbType.VarChar, 50);

            return ParamList;
        }

        public PartnerRecord ParseRowPartner(SqlDataReader dr)
        {
            PartnerRecord partner = null;

            string id = GetDataString(dr, "PartnerId", null);
            if (!string.IsNullOrEmpty(id))
            {
                id = id.ToUpper();
                partner = new PartnerRecord
                {
                    PartnerId = id,
                    PartnerName = GetDataString(dr, "PartnerName", null),
                    ResponseFormatTypeId = GetDataByte(dr, "ResponseFormatTypeId"),
                    ResponsePackageTypeId = GetDataByte(dr, "ResponsePackageTypeId"),
                    IsCloudToGroundEnabled = GetDataBool(dr, "IsCloudTogroundEnabled"),
                    IsInCloudEnabled = GetDataBool(dr, "IsInCloudEnabled"),
                    IpAddress = new List<string> { GetDataString(dr, "IpAddress", null) },
                    BoundingArea = GetDataString(dr, "BoundingArea", null),
                    LastModifiedDateUtc = GetDataDateTime(dr, "LastModifiedDateUtc"),
                    LastModifiedBy = GetDataString(dr, "LastModifiedBy", null),
                    NetworkTypeId = GetDataInt(dr, "NetworkTypeId"),
                    IsActive = GetDataBool(dr, "IsActive"),
                    Metadata = GetDataBool(dr, "Metadata")
                };
            }

            return partner;
        }

    }
}