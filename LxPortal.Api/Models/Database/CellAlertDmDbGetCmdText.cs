﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Aws.Core.Data;
using LxPortal.Api.Helper;
using LxPortalLib.Common;
using LxPortalLib.Models;
using Microsoft.SqlServer.Types;

namespace LxPortal.Api.Models.Database
{
    public class CellAlertDmDbGetCmdText : DbCommandText
    {
        private long _start, _end;

        private SqlGeography _boundingBox;


        public CellAlertDmDbGetCmdText(long start, long end, decimal ullat, decimal ullon, decimal lrlat, decimal lrlon)
        {
            _start = start;
            _end = end;
            _boundingBox = SqlGeographyUtil.CreateSqlBoundingBox(ullat, ullon, lrlat, lrlon);

            InitSP();
        }

        public override string BuildCmdText()
        {
            return "dbo.GetAlertBoundingBox_pr";
        }

        public override ArrayList BuildParameters()
        {
            AddParam(new SqlParameter("@BoundingBox", _boundingBox) { UdtTypeName = "Geography" });
            AddParam("@DateKeyFrom", _start, SqlDbType.BigInt);
            AddParam("@DateKeyTo", _end, SqlDbType.BigInt);

            return ParamList;
        }

        public override object ParseRow(SqlDataReader dr)
        {
            CellAlertRecord alert = null;

            object dbObj = dr["AlertLocation"];
            string alertLocation = (dbObj != DBNull.Value) ? dbObj.ToString() : null;
            dbObj = dr["CellLocation"];
            string cellLocation = (dbObj != DBNull.Value) ? dbObj.ToString() : null;
            dbObj = dr["AlertCentroid"];
            string alertCentroid = (dbObj != DBNull.Value) ? dbObj.ToString() : null;

            if (!string.IsNullOrEmpty(alertLocation) && !string.IsNullOrEmpty(cellLocation))
            {
                alert = new CellAlertRecord
                {
                    CellId = GetDataString(dr, "CellId"),
                    CellTimeUtc = DbHelper.GetDataUtcDateTime(dr, "CellTimeUtc"),
                    AlertLocation = alertLocation,
                    CellLocation = cellLocation,
                    Area = GetDataDecimal(dr, "Area"),
                    Direction = Convert.ToDecimal(GetDataDouble(dr, "Direction")),
                    Speed = GetDataDecimal(dr, "Speed"),
                    InCloudFlashRate = GetDataDecimal(dr, "InCloudFlashRate"),
                    InCloudFlashRateChange = GetDataDecimal(dr, "InCloudFlashRateChange"),
                    CloudGroundFlashRate = GetDataDecimal(dr, "CloudGroundFlashRate"),
                    CloudGroudFlashRateChange = GetDataDecimal(dr, "CloudGroundFlashRateChange"),
                    Level = GetDataShort(dr, "AlertLevel"),
                    Threshold = GetDataShort(dr, "Threshold"),
                    StartUtc = DbHelper.GetDataUtcDateTime(dr, "StartUtc"),
                    EndUtc = DbHelper.GetDataUtcDateTime(dr, "EndUtc")
                };
            }

            if (alert != null && !string.IsNullOrEmpty(alertCentroid))
            {
                alert.AlertCentroid = alertCentroid;
            }
            return alert;
        }
    }
}