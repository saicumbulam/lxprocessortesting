﻿using System.Collections;

namespace LxPortal.Api.Models.Database
{
    public class TlnStationGetDbCmdText : StationBaseDbCmdText
    {
        private string _id;
        public TlnStationGetDbCmdText(string id)
        {
            _id = id;
            InitSP();
        }

        public override string BuildCmdText()
        {
            return "dbo.TlnStationGet_pr";
        }

        public override ArrayList BuildParameters()
        {
            AddParam("@Id", _id, System.Data.SqlDbType.VarChar, 10);

            return ParamList;
        }
    }
}