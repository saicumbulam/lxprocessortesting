﻿using System;
using System.Data.SqlClient;

using Aws.Core.Data;

using LxPortalLib.Common;
using LxPortalLib.Models;

namespace LxPortal.Api.Models.Database
{
    public abstract class StationBaseDbCmdText : DbCommandText
    {
        public Station ParseRowTls(SqlDataReader dr)
        {
            Station station = null;

            string id = GetDataString(dr, "Id", null);
            if (id != null)
            {
                id = id.ToUpper();
                station = new Station
                {
                    Id = id,
                    Name = GetDataString(dr, "Name", null),
                    StreetAddress = GetDataString(dr, "StreetAddress", null),
                    Country = GetDataString(dr, "Country", null),
                    Province = GetDataString(dr, "Province", null),
                    Region = GetDataString(dr, "Region", null),
                    City = GetDataString(dr, "City", null),
                    PostalCode = GetDataString(dr, "PostalCode", null),
                    InstallDate = DbHelper.GetDataNullableUtcDateTime(dr, "InstallDate"),
                    SerialNumber = GetDataString(dr, "SerialNumber", null),
                    Ip = GetDataString(dr, "Ip", null),
                    PocName = GetDataString(dr, "PocName", null),
                    PocPhone = GetDataString(dr, "PocPhone", null),
                    PocEmail = GetDataString(dr, "PocEmail", null),
                    SendLogRateMinutes = DbHelper.GetDataNullableInt(dr, "SendLogRateMinutes"),
                    SendGpsRateMinutes = DbHelper.GetDataNullableInt(dr, "SendGpsRateMinutes"),
                    SendAntennaGainRateMinutes = DbHelper.GetDataNullableInt(dr, "SendAntennaGainRateMinutes"),
                    AntennaMode = DbHelper.GetDataNullableInt(dr, "AntennaMode"),
                    AntennaAttenuation = GetDataInt(dr, "AntennaAttenuation"),
                    PrimaryDeliveryIp = GetDataString(dr, "PrimaryDeliveryIp", null),
                    SecondaryDeliveryIp = GetDataString(dr, "SecondaryDeliveryIp", null),
                    LastCallHome = DbHelper.GetDataNullableUtcDateTime(dr, "LastCallHome"),
                    IndoorFirmware = GetDataString(dr, "IndoorFirmware", null),
                    LastCalibration = DbHelper.GetDataNullableUtcDateTime(dr, "LastCalibration"),
                    OutdoorFirmware = GetDataString(dr, "OutdoorFirmware", null),
                    PacketPercent = DbHelper.GetDataNullableInt(dr, "PacketPercent"),
                    LastPacketUtc = DbHelper.GetDataNullableUtcDateTime(dr, "LastPacketUtc"),
                    Note = GetDataString(dr, "Note"),
                    MemBackUpVoltage = DbHelper.GetDataNullableDecimal(dr, "MemBackUpVoltage"),
                    BackUpBatteryVol = DbHelper.GetDataNullableDecimal(dr, "BackUpBatteryVol")
                };
                short? qcFlagValue = DbHelper.GetDataNullableShort(dr, "QCFlag");
                station.IsEnable = !qcFlagValue.HasValue || qcFlagValue.Value == 100;

                //check the lastPacketUtc here 
                if (station.LastPacketUtc.HasValue && station.LastPacketUtc != null)
                {
                    TimeSpan ts = DateTime.UtcNow - station.LastPacketUtc.Value;

                    if (ts.TotalMinutes > Config.LastPacketReceivedInterval)
                    {
                        station.PacketPercent = 0;
                    }
                }

                station.Status = Utilities.GetSensorStatus(station.LastCallHome, station.PacketPercent, station.IsEnable);
            }

            return station;
        }

        public Station ParseRowTlnAws(SqlDataReader dr)
        {
            Station station = null;
            TimeZoneInfo est = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            string id = GetDataString(dr, "Id", null);
            if (id != null)
            {
                id = id.ToUpper();

                station = new Station
                {
                    Id = id,
                    Name = GetDataString(dr, "Name", null),
                    StreetAddress = GetDataString(dr, "StreetAddress", null),
                    Country = GetDataString(dr, "Country", null),
                    Province = GetDataString(dr, "Province", null),
                    Region = GetDataString(dr, "Region", null),
                    City = GetDataString(dr, "City", null),
                    PostalCode = GetDataString(dr, "PostalCode", null),
                    InstallDate = DbHelper.GetDataNullableUtcDateTime(dr, "InstallDate"),
                    SerialNumber = GetDataString(dr, "SerialNumber", null),
                    PocName = GetDataString(dr, "PocName", null),
                    PocPhone = GetDataString(dr, "PocPhone", null),
                    PocEmail = GetDataString(dr, "PocEmail", null),
                    SendLogRateMinutes = DbHelper.GetDataNullableInt(dr, "SendLogRateMinutes"),
                    SendGpsRateMinutes = DbHelper.GetDataNullableInt(dr, "SendGpsRateMinutes"),
                    SendAntennaGainRateMinutes = DbHelper.GetDataNullableInt(dr, "SendAntennaGainRateMinutes"),
                    AntennaMode = DbHelper.GetDataNullableInt(dr, "AntennaMode"),
                    AntennaAttenuation = GetDataInt(dr, "AntennaAttenuation"),
                    PrimaryDeliveryIp = GetDataString(dr, "PrimaryDeliveryIp", null),
                    SecondaryDeliveryIp = GetDataString(dr, "SecondaryDeliveryIp", null),
                    LastCallHome = DbHelper.GetDataNullableUtcDateTime(dr, "LastCallHome"),
                    IndoorFirmware = GetDataString(dr, "IndoorFirmware", null),
                    MemBackUpVoltage = DbHelper.GetDataNullableDecimal(dr, "MemBackUpVoltage"),
                    BackUpBatteryVol = DbHelper.GetDataNullableDecimal(dr, "BackUpBatteryVol"),
                    Displayer = DbHelper.GetDataNullableInt(dr, "Displayer"),
                    WindSensor = DbHelper.GetDataNullableInt(dr, "WindSensor"),
                    AntennaHwRev = GetDataString(dr, "AntennaHwRev", null),
                    AntennaSerial = GetDataString(dr, "AntennaSerial", null),
                    RemoteSerial = DbHelper.GetDataNullableInt(dr, "RemoteSerial")
                };
                if (station.LastCallHome != null)
                {
                    station.LastCallHome = DateTime.SpecifyKind(station.LastCallHome.Value, DateTimeKind.Unspecified);
                    station.LastCallHome = TimeZoneInfo.ConvertTimeToUtc(station.LastCallHome.Value, est);
                }
            }

            return station;
        }

        public Station ParseRowTlnLx(SqlDataReader dr)
        {
            Station station = null;

            string id = GetDataString(dr, "Id", null);
            if (id != null)
            {
                id = id.ToUpper();

                station = new Station
                {
                    Id = id,
                    Ip = GetDataString(dr, "Ip", null),
                    LastCalibration = DbHelper.GetDataNullableUtcDateTime(dr, "LastCalibration"),
                    OutdoorFirmware = GetDataString(dr, "OutdoorFirmware", null),
                    PacketPercent = DbHelper.GetDataNullableInt(dr, "PacketPercent"), 
                    Latitude = GetDataDecimal(dr, "Latitude"),
                    Longitude = GetDataDecimal(dr, "Longitude"),
                };
                int? qcFlagValue = DbHelper.GetDataNullableShort(dr, "QCFlag");
                string networkId = GetDataString(dr, "NetworkId", null);
                string networkDescription = GetDataString(dr, "NetworkDescription", null);
                station.IsEnable = !qcFlagValue.HasValue || qcFlagValue.Value == 100;
                station.NetworkId = networkId != null ? networkId.Trim() : "ENTLN";
                station.NetworkDescription = networkDescription != null ? networkDescription.Trim() : null; 
            }

            return station;
        }
    }
}