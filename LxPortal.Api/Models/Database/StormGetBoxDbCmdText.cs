﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using Aws.Core.Data;
using LxPortalLib.Models; 
using System.Data.SqlClient;
using LxPortal.Api.Helper; 

namespace LxPortal.Api.Models.Database
{
    public class StormGetBoxDbCmdText : DbCommandText //add base class here 
    {
        private decimal _ullat, _ullon, _lrlat, _lrlon;
        private DateTime? _start, _end;

        //method that will have parameters passed in 
        public StormGetBoxDbCmdText(DateTime start, DateTime end, decimal ullat, decimal ullon, decimal lrlat, decimal lrlon)
        {
            _ullat = ullat;
            _ullon = ullon;
            _lrlat = lrlat;
            _lrlon = lrlon;
            _start = start;
            _end = end;

            
            InitSP(); 
        }

        //take those parameters and execute sp
        public override string BuildCmdText()
        {
            return "LtgGetStormRecordForBox_pr"; 
        }

        //build parameters here 
        public override ArrayList BuildParameters()
        {
            AddParam("@LatitudeFrom", _ullat, SqlDbType.Decimal, 9);
            AddParam("@LongitudeFrom", _ullon, SqlDbType.Decimal, 9);
            AddParam("@LatitudeTo", _lrlat, SqlDbType.Decimal, 9);
            AddParam("@LongitudeTo", _lrlon, SqlDbType.Decimal, 9);
            AddParam("@DateTimeFrom", _start, SqlDbType.DateTime); //revisist format
            AddParam("@DateTimeTo", _end, SqlDbType.DateTime); //revisist format

            return ParamList;
        }

        //returned list 
        public override object ParseRow(SqlDataReader dr)
        {
            DateTime? postedTime = TimeZoneSvcHelper.ConvertUtcToLocal(dr, "UtcTime");

            StormRecord stormData = new StormRecord
                {
                    LsrId = GetDataInt(dr, "LsrId"),
                    UtcTime = (DateTime)dr["ReportedTime"], //reported time is in UTC
                    Latitude = Convert.ToSingle(dr["Latitude"]),
                    Longitude = Convert.ToSingle(dr["Longitude"]),
                    Event = GetDataString(dr, "Event"),
                    Magnitude = GetDataString(dr, "Magnitude"),
                    Source = GetDataString(dr, "Source"),
                    Remarks = GetDataString(dr, "Remarks"),
                    Unit = GetDataString(dr, "Unit")
                };

                return stormData;
        }

    }
}