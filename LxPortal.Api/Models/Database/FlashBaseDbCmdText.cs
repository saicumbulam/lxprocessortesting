﻿using System;
using System.Data.SqlClient;

using Aws.Core.Data;

using LxPortalLib.Models;

namespace LxPortal.Api.Models.Database
{
    public abstract class FlashBaseDbCmdText : DbCommandText
    {
        private FlashRecord ParseBaseRow(SqlDataReader dr)
        {
            FlashRecord fr = new FlashRecord
            {
                TimeUtc = GetDataDateTime(dr, "Lightning_Time"),
                Confidence = GetDataInt(dr, "Confidence"),
                Type = (StrokeType)GetDataInt(dr, "Stroke_Type")
            };

            if (Config.SystemType == "TLN")
            {
                fr.Height = (dr["Height"] == null) ? -1 : (double)dr["Height"];
            }
            else
            {
                fr.Height = (dr["Height"] == null) ? -1 : (int)dr["Height"];
            }

            return fr;
        }

        public FlashRecord ParseFlashRow(SqlDataReader dr)
        {
            FlashRecord fr = ParseBaseRow(dr);
            fr.Latitude = Math.Round(GetDataDecimal(dr, "Latitude"), 5, MidpointRounding.ToEven); // DB = numeric(38,8)
            fr.Longitude = Math.Round(GetDataDecimal(dr, "Longitude"), 5, MidpointRounding.ToEven); // DB = numeric(38,8)
            fr.Amplitude = (double) GetDataDecimal(dr, "Amplitude"); // DB = numeric(38,8)
            if (fr != null)
                fr.Id = GetDataLong(dr, "FlashID");
            return fr;
        }


        public FlashRecord ParsePortionRow(SqlDataReader dr)
        {
            FlashRecord fr = ParseBaseRow(dr);
            fr.Latitude = Convert.ToDecimal(Math.Round(GetDataDouble(dr, "Latitude"), 5, MidpointRounding.ToEven)); // DB = float
            fr.Longitude = Convert.ToDecimal(Math.Round(GetDataDouble(dr, "Longitude"), 5, MidpointRounding.ToEven)); // DB = float
            fr.Amplitude = GetDataDouble(dr, "Amplitude"); // DB = float
            if (fr != null)
            {
                fr.Id = GetDataLong(dr, "FlashPortionID");
                fr.StrokeSolution = GetDataString(dr, "Stroke_Solution");
            }
            
            return fr;
        }
    }
}