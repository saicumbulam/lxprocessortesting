﻿using Aws.Core.Data;

namespace LxPortal.Api.Models.Database
{
    public class HealthCheckDbCmdText : DbCommandText
    {
        public override string BuildCmdText()
        {
            return "SELECT LtgNetworkName FROM dbo.LtgNetworkList";
        }
    }
}