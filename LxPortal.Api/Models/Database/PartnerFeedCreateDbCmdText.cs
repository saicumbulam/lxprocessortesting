﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Aws.Core.Data;

using LxPortal.Api.Helper;
using LxPortal.Api.Models;
using LxPortalLib.Models;
using LxPortalLib.Common;

using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace LxPortal.Api.Models.Database
{
    public class PartnerFeedCreateDbCmdText : DbCommandText
    {
        private PartnerRecord _partner;
        private int _feedType;

        public PartnerFeedCreateDbCmdText(PartnerRecord partner, int feedType)
        {
            _partner = partner;
            _feedType = feedType;
            InitSP();
        }

        public override string BuildCmdText()
        {
            return "bizops.AddPartnerFeedType";
        }

        public override ArrayList BuildParameters()
        {
            AddParam("@partnerId", _partner.PartnerId, SqlDbType.VarChar, 50);
            AddParam("@feedTypeId", _feedType, SqlDbType.Int);

            return ParamList;
        }

        //public PartnerRecord ParseRowPartner(SqlDataReader dr)
        //{
        //    PartnerRecord partner = null;

        //    string id = GetDataString(dr, "PartnerId", null);
        //    if (!string.IsNullOrEmpty(id))
        //    {
        //        id = id.ToUpper();
        //        partner = new PartnerRecord
        //        {
        //            PartnerId = id,
        //            Feed = GetDataInt(dr, "@feedTypeId")
        //        };

        //    }

        //    return partner;
        //}

    }
}