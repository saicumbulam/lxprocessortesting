﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

using Aws.Core.Data;

using LxPortalLib.Models;

namespace LxPortal.Api.Models.Database
{
    public class SensoryPacketHistoryDbCmdText : DbCommandText
    {
        private string _id;
        private DateTime? _start = null, _end = null;
        public SensoryPacketHistoryDbCmdText(string id, DateTime? start, DateTime? end)
        {
            _id = id;
            _start = start;
            _end = end;
            InitSP();
        }

        public override ArrayList BuildParameters()
        {
            AddParam("@Sensor_ID", _id, SqlDbType.VarChar, 10);
            
            if (_start.HasValue)
                AddParam("@StartTimeUTC", _start.Value, SqlDbType.DateTime);
            
            if (_end.HasValue)
                AddParam("@EndTimeUTC", _end.Value, SqlDbType.DateTime);

            return ParamList;
        }

        public override string BuildCmdText()
        {
            return "dbo.GetSensorPacket_pr";
        }

        public override object ParseRow(SqlDataReader dr)
        {
            decimal? packetSizeKb = null;
            int? packetSizeBytes = DbHelper.GetDataNullableInt(dr, "TotalBytes");
            if (packetSizeBytes.HasValue)
                packetSizeKb = packetSizeBytes.Value / 1024m;

            PacketRecord pr = new PacketRecord
            {
                TimestampUtc = DbHelper.GetDataNullableUtcDateTime(dr, "Time"),
                LastPacketUtc = DbHelper.GetDataNullableUtcDateTime(dr, "TimeLastSuccessfulPacketReceived"),
                ReceivedPackets = DbHelper.GetDataNullableInt(dr, "TotalReceivedPackets"),
                PacketRate = DbHelper.GetDataNullableInt(dr, "PacketRatePercent"),
                TotalPacketSizeKilobytes = packetSizeKb,
                MinPacketSizeBytes = DbHelper.GetDataNullableInt(dr, "MinPacketSize"),
                MaxPacketSizeBytes = DbHelper.GetDataNullableInt(dr, "MaxPacketSize"),
                FirstPacketUtc = DbHelper.GetDataNullableUtcDateTime(dr, "TimeFirstPacketReceived"),
                EmptyPackets = DbHelper.GetDataNullableInt(dr, "EmptyPackets"),
                MinPacketLatency = DbHelper.GetDataNullableInt(dr, "MinPacketLatency"),
                MaxPacketLatency = DbHelper.GetDataNullableInt(dr, "MaxPacketLatency"),
                MedianPacketLatency = DbHelper.GetDataNullableInt(dr, "MedianPacketLatency"),
                MinLFThreshold = DbHelper.GetDataNullableInt(dr, "MinLFThreshold"),
                MaxLFThreshold = DbHelper.GetDataNullableInt(dr, "MaxLFThreshold"),
                MedianLFThreshold = DbHelper.GetDataNullableInt(dr, "MedianLFThreshold"),
                MinHFThreshold = DbHelper.GetDataNullableInt(dr, "MinHFThreshold"),
                MaxHFThreshold = DbHelper.GetDataNullableInt(dr, "MaxHFThreshold"),
                MedianHFThreshold = DbHelper.GetDataNullableInt(dr, "MedianHFThreshold")
            };

            if (pr.PacketRate.HasValue && pr.PacketRate > LxPortalLib.Config.PacketPercentThreshold)
                pr.Status = 1;
            else
                pr.Status = 3;

            return pr;
        }
    }
}
