﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Aws.Core.Data;

using LxPortal.Api.Helper;
using LxPortal.Api.Models;
using LxPortalLib.Models;
using LxPortalLib.Common;

using System.Data;
using System.Data.SqlClient;
using System.Collections;
using Microsoft.SqlServer.Types;

namespace LxPortal.Api.Models.Database
{
    public class PartnerFeedRemoveDbCmdText : DbCommandText
    {
        private string _partnerId;

        public PartnerFeedRemoveDbCmdText(string id)
        {
            _partnerId = id;
            InitSP();
        }

        public override string BuildCmdText()
        {
            return "bizops.RemovePartnerFeedType";
        }

        public override ArrayList BuildParameters()
        {
            AddParam("@partnerId", _partnerId, SqlDbType.VarChar, 50);

            return ParamList;
        }
    }
}