﻿namespace LxPortal.Api.Models.Database
{
    public class TlnAwsStationGetAllDbCmdText : StationBaseDbCmdText
    {
        public TlnAwsStationGetAllDbCmdText()
        {
            InitSP();
        }

        public override string BuildCmdText()
        {
            return "dbo.LxStationListGet_pr";
        }

    }
}