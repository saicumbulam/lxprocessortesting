﻿using System.Collections;

namespace LxPortal.Api.Models.Database
{
    public class StationDeleteDbCmdText : StationBaseDbCmdText
    {
        private readonly string _id;
        public StationDeleteDbCmdText(string id)
        {
            _id = id;
            InitSP();
        }

        public override string BuildCmdText()
        {
            return "dbo.DeleteStation_pr";
        }

        public override ArrayList BuildParameters()
        {
            AddParam("@Id", _id, System.Data.SqlDbType.VarChar, 10);

            return ParamList;
        }
    }
}