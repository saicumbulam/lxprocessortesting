﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Aws.Core.Data;

using LxPortal.Api.Helper;
using LxPortal.Api.Models;
using LxPortalLib.Models;
using LxPortalLib.Common;

using System.Data;
using System.Data.SqlClient;
using System.Collections;
using Microsoft.SqlServer.Types;


namespace LxPortal.Api.Models.Database
{
    public class PartnerResponseCreateDbCmdText : DbCommandText
    {
        private ResponseDataElement _responseDataElementId;
        private PartnerRecord _partner;

        public PartnerResponseCreateDbCmdText(PartnerRecord partner, ResponseDataElement responseDataElementId)
        {
            _partner = partner;
            _responseDataElementId = responseDataElementId;
            InitSP();
        }

        public override string BuildCmdText()
        {
            return "bizops.AddPartnerResponseDataElement";
        }

        public override ArrayList BuildParameters()
        {
            AddParam("@partnerId", _partner.PartnerId, SqlDbType.VarChar, 50);
            AddParam("@responseDataElementId", _responseDataElementId, SqlDbType.SmallInt);

            return ParamList;
        }

        //public PartnerRecord ParseRowPartner(SqlDataReader dr)
        //{
        //    PartnerRecord partner = null;

        //    string id = GetDataString(dr, "PartnerId", null);
        //    int ResponseElement = GetDataInt(dr, "@responseDataElementId");

        //    if (!string.IsNullOrEmpty(id))
        //    {
        //        id = id.ToUpper();
        //        partner = new PartnerRecord
        //        {
        //            PartnerId = id,
        //            ResponseDataElementId = _responseDataElementId
        //        };
        //    }

        //    return partner;
        //}

    }
}