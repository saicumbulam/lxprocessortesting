﻿using System.Collections;

namespace LxPortal.Api.Models.Database
{
    public class TlnAwsStationGetDbCmdText : StationBaseDbCmdText
    {
        private string _id;
        public TlnAwsStationGetDbCmdText(string id)
        {
            _id = id;
            InitSP();
        }

        public override string BuildCmdText()
        {
            return "dbo.LxStationGet_pr";
        }

        public override ArrayList BuildParameters()
        {
            AddParam("@Id", _id, System.Data.SqlDbType.VarChar, 10);

            return ParamList;
        }
    }
}