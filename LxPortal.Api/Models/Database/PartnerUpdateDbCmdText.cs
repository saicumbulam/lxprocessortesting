﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Aws.Core.Data;

using LxPortal.Api.Helper;
using LxPortal.Api.Models;
using LxPortalLib.Models;
using LxPortalLib.Common;

using System.Data;
using System.Data.SqlClient;
using System.Collections;
using Microsoft.SqlServer.Types;

namespace LxPortal.Api.Models.Database
{
    public class PartnerUpdateDbCmdText : DbCommandText
    {
        private PartnerRecord _partner;
        private SqlGeography _boundingBox;

        public PartnerUpdateDbCmdText(PartnerRecord update)
        {
            _partner = update;
            _boundingBox = ConvertStringToSqlGeography();
            InitSP();
        }

        public override string BuildCmdText()
        {
            return "bizops.UpdatePartner";
        }

        public override ArrayList BuildParameters()
        {
            string bbox = (_boundingBox != null) ? _boundingBox.ToString() : null; 

            AddParam("@partnerId", _partner.PartnerId, SqlDbType.VarChar, 50);
            AddParam("@partnerName", _partner.PartnerName, SqlDbType.VarChar, 100);
            AddParam("@responseFormatTypeId", _partner.ResponseFormatTypeId, SqlDbType.TinyInt);
            AddParam("@responsePackageTypeId", _partner.ResponsePackageTypeId, SqlDbType.TinyInt);
            AddParam("@isCloudToGroundEnabled", _partner.IsCloudToGroundEnabled, SqlDbType.Bit);
            AddParam("@isInCloudEnabled", _partner.IsInCloudEnabled, SqlDbType.Bit);
            AddParam("@boundingArea", bbox, SqlDbType.VarChar);
            if (_partner.LastModifiedBy != null)
            {
                AddParam("@lastModifiedBy", _partner.LastModifiedBy, SqlDbType.VarChar, 100);
            }
            AddParam("@networkTypeId", _partner.NetworkTypeId, SqlDbType.Int);
            AddParam("@lastModifiedDateUtc", DateTime.UtcNow, SqlDbType.DateTime);
            AddParam("@isActive", _partner.IsActive, SqlDbType.Bit);
            AddParam("@metadata", _partner.Metadata, SqlDbType.Bit);

            return ParamList;
        }

        public PartnerRecord ParseRowPartner(SqlDataReader dr)
        {
            PartnerRecord partner = null;

            string id = GetDataString(dr, "PartnerId", null);
            if (!string.IsNullOrEmpty(id))
            {
                id = id.ToUpper();
                partner = new PartnerRecord
                {
                    PartnerId = id,
                    PartnerName = GetDataString(dr, "PartnerName", null),
                    ResponseFormatTypeId = GetDataByte(dr, "ResponseFormatTypeId"),
                    ResponsePackageTypeId = GetDataByte(dr, "ResponsePackageTypeId"),
                    IsCloudToGroundEnabled = GetDataBool(dr, "IsCloudTogroundEnabled"),
                    IsInCloudEnabled = GetDataBool(dr, "IsInCloudEnabled"),
                    IpAddress = new List<string> { GetDataString(dr, "IpAddress", null) },
                    FeedType =  new List<int> { GetDataInt(dr, "FeedTypeId") },
                    BoundingArea = GetDataString(dr, "BoundingArea", null),
                    LastModifiedDateUtc = GetDataDateTime(dr, "LastModifiedDateUtc"),
                    LastModifiedBy = GetDataString(dr, "LastModifiedBy", null),
                    NetworkTypeId = GetDataInt(dr, "NetworkTypeId"),
                    IsActive = GetDataBool(dr, "IsActive"),
                    Metadata = GetDataBool(dr, "Metadata")
                };
            }

            return partner;
        }

        private SqlGeography ConvertStringToSqlGeography()
        {
            SqlGeography bbox = null; 

            if (_partner.BoundingArea != null)
            {
                string boundingBox = _partner.BoundingArea;
                string[] UpperLower = boundingBox.Split(';');
                string ul = UpperLower[0];
                string lr = UpperLower[1];

                decimal ullat = decimal.Parse(ul.Substring(0, ul.IndexOf(',')));
                decimal ullon = decimal.Parse(ul.Substring(ul.IndexOf(',') + 1));

                decimal lrlat = decimal.Parse(lr.Substring(0, lr.IndexOf(',')));
                decimal lrlon = decimal.Parse(lr.Substring(lr.IndexOf(',') + 1));

                bbox = SqlGeographyUtil.CreateSqlBoundingBox(ullat, ullon, lrlat, lrlon);
            }

            return bbox; 
        }
    }
}