﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using LxPortal.Api.Helper;
using Microsoft.SqlServer.Types;

using Aws.Core.Data;
using Aws.Core.Utilities;

using LxCommon;

using LxPortalLib.Models;

namespace LxPortal.Api.Models.Database
{
    public class NwsAlertGetDbCmdText : DbCommandText
    {
        private DateTime _start, _end;
        public const int Wgs84SpatialRefId = 4326;

        public NwsAlertGetDbCmdText(DateTime start, DateTime end)
        {
            _start = start;
            _end = end;
            InitSP();
        }

        public override string BuildCmdText()
        {
            return "dbo.AlertsGetNwsPolygons_pr";
        }

        public override ArrayList BuildParameters()
        {
            AddParam("@StartUtc", _start, SqlDbType.DateTime);
            AddParam("@EndUtc", _end, SqlDbType.DateTime);
            
            return ParamList;
        }

        public NwsAlert ParseNwsAlertRow(SqlDataReader dr)
        {
            NwsAlert alert = null;

            SqlGeography loc = GetSqlGeographyFromLatLonStr(GetDataString(dr, "LAT_LON_Str"));
            if (loc != null)
            {
                alert = new NwsAlert
                {
                    VtecId = GetDataInt(dr, "PVTEC_Tracking_ID"),
                    PostTimeUtc = GetDataDateTime(dr, "UTCPostTime"),
                    ExpireTimeUtc = GetDataDateTime(dr, "UTCExpireTime"),
                    Type = GetDataString(dr, "NNN"),
                    Description = GetDataString(dr, "DESCRIPTION"),
                    DirectionDeg = GetDataInt(dr, "Direction"),
                    SpeedKts = GetDataInt(dr, "Speed"),
                    Location = loc.ToString()
                };
            }

            return alert;
        }

        private SqlGeography GetSqlGeographyFromLatLonStr(string pointString)
        {
            List<Tuple<double, double>> points = ParsePointList(pointString);
            return SqlGeographyUtil.CreateSqlPolygon(points);
        }

        private List<Tuple<double,double>> ParsePointList(string pointString)
        {
            List<Tuple<double, double>> pointList = null;
            if (!String.IsNullOrEmpty(pointString))
            {
                string[] points = pointString.Split(' ');
                //need 3 points (6 coordinates) to define polygon and make sure we have pairs
                if (points.Length >= 6 && (points.Length % 2) == 0)
                {
                    pointList = new List<Tuple<double, double>>(points.Length / 2);

                    for (int i = 0; i < points.Length; i = i + 2)
                    { 
                        Tuple<double,double> point = CreateLatLonPointFromPair(points[i], points[i + 1]);
                        if (point != null)
                            pointList.Add(point);
                    }
                }    
            }

            return pointList;
        }

        private Tuple<double, double> CreateLatLonPointFromPair(string slat, string slon)
        {
            Tuple<double, double> point = null;
            double lat, lon;
            if (double.TryParse(slat, out lat) && double.TryParse(slon, out lon))
            {
                lat = lat / 100;
                lon = (-1 * lon) / 100;

                if (lat >= -90 && lat <= 90 && lon >= -180 && lon <= 180)
                    point = new Tuple<double, double>(lat, lon);    
            }

            return point;
        }


    }
}
