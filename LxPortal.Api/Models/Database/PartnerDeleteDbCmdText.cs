﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Aws.Core.Data;
using LxPortalLib.Models;

namespace LxPortal.Api.Models.Database
{
    public class PartnerDeleteDbCmdText : DbCommandText
    {
        private string _partnerId;

        public PartnerDeleteDbCmdText(string id)
        {
            _partnerId = id; 
            InitSP();
        }

        public override string BuildCmdText()
        {
            return "bizops.DeletePartner"; 
        }

        public override ArrayList BuildParameters()
        {
            AddParam("@partnerId", _partnerId, SqlDbType.VarChar, 50);

            return ParamList;
        }
    }
}