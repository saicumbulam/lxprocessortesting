﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LxPortalLib.Models;

namespace LxPortal.Api.Models
{
    public interface IProvisioningRepository
    {
        PartnerRecord Add(PartnerRecord partners); 
    }
}