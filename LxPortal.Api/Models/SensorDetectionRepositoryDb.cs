﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using Aws.Core.Data;
using Aws.Core.Utilities;
using LxCommon;
using LxCommon.Monitoring;
using LxPortalLib.Models;

using LxPortal.Api.Models.Database;

namespace LxPortal.Api.Models
{
    public class SensorDetectionRepositoryDb : ISensorDetectionRepository
    {
        public SensorDetection Get(string id, DateTime? start, DateTime? end)
        {
            SqlConnection conn = null;
            SqlDataReader dr = null;
            DbCommand dbCmd = null;
            SensorDetection detections = null;

            try
            {
                if (start.HasValue && end.HasValue && start.Value.CompareTo(DateTime.MinValue) == 0 && end.Value.CompareTo(DateTime.MaxValue) == 0)
                {
                    start = null;
                    end = null;
                }
                conn = DbConnectionFactory.Build(ConfigurationManager.ConnectionStrings["Lightning"].ConnectionString);
                SensorDetectionGetDbCmdText dbCmdText = new SensorDetectionGetDbCmdText(id, start, end);
                dbCmd = new DbCommand(conn, dbCmdText);
                dbCmd.Command.CommandTimeout = Config.DbCommandTimeoutMilliseconds;
                dr = dbCmd.ExecuteReader();
                detections = new SensorDetection { Id = id, DetectionList = new List<DetectionRecordData>() };
                while (dr.Read())
                {
                    DetectionRecordData detection = dbCmdText.ParseRow(dr) as DetectionRecordData;
                    if (detection != null && detection.TimestampUtc != null)
                        detections.DetectionList.Add(detection);
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.ApiSensorDetectionGet, string.Format("Unable to get current sensor detections. Message: {0}", ex.Message));
            }
            finally
            {
                if (null != dbCmd) dbCmd.Close();
                if (null != dr) dr.Close();
                if (null != conn) conn.Close();
            }

            return detections;
        }
    }
}
