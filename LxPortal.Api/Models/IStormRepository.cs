﻿using System; 
using System.Collections.Generic; 
using LxPortalLib.Models; 

namespace LxPortal.Api.Models
{
    public interface IStormRepository
    {
        List<StormRecord> GetAll(DateTime start, DateTime end, decimal ullat, decimal ullon, decimal lrlat, decimal lrlon);
    }
}