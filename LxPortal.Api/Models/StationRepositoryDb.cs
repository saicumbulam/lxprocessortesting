﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Aws.Core.Data;
using Aws.Core.Utilities;

using LxCommon;
using LxCommon.Monitoring;
using LxPortalLib.Common;
using LxPortalLib.Models;

using LxPortal.Api.Models.Database;

namespace LxPortal.Api.Models
{
    public class StationRepositoryDb : IStationRepository
    {
        
        public Station Get(string id)
        {
            Station station = null;

            if (Config.SystemType == "TLN")
            {
                station = GetTln(id);
            }
            else
            {
                station = GetTls(id);
            }

            return station;
        }

        private Station GetTln(string id)
        {
            Station station = GetTlnLx(id);
            if (station != null)
            {
                Station stat = GetTlnAws(id);
                if (stat != null)
                {
                    station = MergeStation(station, stat);
                }
            }
            return station;
        }

        private static Station MergeStation(Station lxStat, Station awsStat)
        {
            lxStat.Name = awsStat.Name;
            lxStat.StreetAddress = awsStat.StreetAddress;
            lxStat.Country = awsStat.Country;
            lxStat.Province = awsStat.Province;
            lxStat.Region = awsStat.Region;
            lxStat.City = awsStat.City;
            lxStat.PostalCode = awsStat.PostalCode;
            lxStat.InstallDate = awsStat.InstallDate;
            lxStat.SerialNumber = awsStat.SerialNumber;
            lxStat.PocName = awsStat.PocName;
            lxStat.PocPhone = awsStat.PocPhone;
            lxStat.PocEmail = awsStat.PocEmail;
            lxStat.SendLogRateMinutes = awsStat.SendLogRateMinutes;
            lxStat.SendGpsRateMinutes = awsStat.SendGpsRateMinutes;
            lxStat.SendAntennaGainRateMinutes = awsStat.SendAntennaGainRateMinutes;
            lxStat.AntennaMode = awsStat.AntennaMode;
            lxStat.AntennaAttenuation = awsStat.AntennaAttenuation;
            lxStat.PrimaryDeliveryIp = awsStat.PrimaryDeliveryIp;
            lxStat.SecondaryDeliveryIp = awsStat.SecondaryDeliveryIp;
            lxStat.LastCallHome = awsStat.LastCallHome;
            lxStat.IndoorFirmware = awsStat.IndoorFirmware;
            //lxStat.IsEnable = awsStat.IsEnable;
            lxStat.BackUpBatteryVol = awsStat.BackUpBatteryVol;
            lxStat.MemBackUpVoltage = awsStat.MemBackUpVoltage;
            lxStat.LastPacketUtc = awsStat.LastPacketUtc;

            //check the lastPacketUtc here 
            if (lxStat.LastPacketUtc.HasValue && lxStat.LastPacketUtc != null)
            {
                TimeSpan ts = DateTime.UtcNow - lxStat.LastPacketUtc.Value;

                if (ts.TotalMinutes > Config.LastPacketReceivedInterval)
                {
                    lxStat.PacketPercent = 0; 
                }
            }

            lxStat.Status = Utilities.GetSensorStatus(lxStat.LastCallHome, lxStat.PacketPercent, lxStat.IsEnable);

            return lxStat;
        }

        internal static Station GetTlnAws(string id)
        {
            SqlConnection conn = null;
            SqlDataReader dr = null;
            DbCommand dbCmd = null;
            Station station = null;

            try
            {
                conn = DbConnectionFactory.Build(ConfigurationManager.ConnectionStrings["AwsData2"].ConnectionString);
                TlnAwsStationGetDbCmdText dbCmdText = new TlnAwsStationGetDbCmdText(id);
                dbCmd = new DbCommand(conn, dbCmdText);
                dbCmd.Command.CommandTimeout = Config.DbCommandTimeoutMilliseconds;
                dr = dbCmd.ExecuteReader();

                if (dr.Read())
                    station = dbCmdText.ParseRowTlnAws(dr);
            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.ApiStationGet, string.Format("Unable to get station.  Message: {0}", ex.Message));
            }
            finally
            {
                if (null != dbCmd) dbCmd.Close();
                if (null != dr) dr.Close();
                if (null != conn) conn.Close();
            }

            return station;
        }

        private Station GetTlnLx(string id)
        {
            SqlConnection conn = null;
            SqlDataReader dr = null;
            DbCommand dbCmd = null;
            Station station = null;

            try
            {
                conn = DbConnectionFactory.Build(ConfigurationManager.ConnectionStrings["Lightning"].ConnectionString);
                TlnStationGetDbCmdText dbCmdText = new TlnStationGetDbCmdText(id);
                dbCmd = new DbCommand(conn, dbCmdText);
                dbCmd.Command.CommandTimeout = Config.DbCommandTimeoutMilliseconds;
                dr = dbCmd.ExecuteReader();

                if (dr.Read())
                    station = dbCmdText.ParseRowTlnLx(dr);
            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.ApiStationGet, string.Format("Unable to get station.  Message: {0}", ex.Message));
            }
            finally
            {
                if (null != dbCmd) dbCmd.Close();
                if (null != dr) dr.Close();
                if (null != conn) conn.Close();
            }

            return station;
        }

        private Station GetTls(string id)
        {
            SqlConnection conn = null;
            SqlDataReader dr = null;
            DbCommand dbCmd = null;
            Station station = null;

            try
            {
                conn = DbConnectionFactory.Build(ConfigurationManager.ConnectionStrings["AwsData2"].ConnectionString);
                StationGetDbCmdText dbCmdText = new StationGetDbCmdText(id);
                dbCmd = new DbCommand(conn, dbCmdText);
                dbCmd.Command.CommandTimeout = Config.DbCommandTimeoutMilliseconds;
                dr = dbCmd.ExecuteReader();

                if (dr.Read())
                    station = dbCmdText.ParseRowTls(dr);
            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.ApiStationGet, string.Format("Unable to get station.  Message: {0}", ex.Message));
            }
            finally
            {
                if (null != dbCmd) dbCmd.Close();
                if (null != dr) dr.Close();
                if (null != conn) conn.Close();
            }

            return station;
        }


        private const int DefaultCapacity = 50000;

        public IEnumerable<Station> GetAll()
        {
            IEnumerable<Station> stations = null;

            if (Config.SystemType == "TLN")
            {
                stations = GetAllTln();
            }
            else
            {
                stations = GetAllTls();
            }

            return stations;
        }

        private IEnumerable<Station> GetAllTln()
        {
            List<Station> stations = GetAllTlnLx();
            if (stations != null && stations.Count > 0)
            {
                Dictionary<string, Station> statTable = GetAllTlnAws();
                if (statTable != null && statTable.Count > 0)
                {
                    for (int i = 0; i < stations.Count; i++)
                    {
                        Station stat;
                        if (statTable.TryGetValue(stations[i].Id, out stat))
                        {
                            stations[i] = MergeStation(stations[i], stat);
                        }
                    }
                }
            }
            
            return stations;
        }

        private Dictionary<string, Station> GetAllTlnAws()
        {
            SqlConnection conn = null;
            SqlDataReader dr = null;
            DbCommand dbCmd = null;
            Dictionary<string, Station> stats = null;

            try
            {
                conn = DbConnectionFactory.Build(ConfigurationManager.ConnectionStrings["AwsData2"].ConnectionString);
                TlnAwsStationGetAllDbCmdText dbCmdText = new TlnAwsStationGetAllDbCmdText();
                dbCmd = new DbCommand(conn, dbCmdText);
                dbCmd.Command.CommandTimeout = Config.DbCommandTimeoutMilliseconds;
                dr = dbCmd.ExecuteReader();
                stats = new Dictionary<string, Station>();
                
                while (dr.Read())
                {
                    Station station = dbCmdText.ParseRowTlnAws(dr);
                    if (null != station && !stats.ContainsKey(station.Id))
                        stats.Add(station.Id, station);
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.ApiStationListGet, string.Format("Unable to get station list. Message: {0}", ex.Message));
            }
            finally
            {
                if (null != dbCmd) dbCmd.Close();
                if (null != dr) dr.Close();
                if (null != conn) conn.Close();
            }

            return stats;
        }

        public List<Station> GetAllTlnLx()
        {
            SqlConnection conn = null;
            SqlDataReader dr = null;
            DbCommand dbCmd = null;
            List<Station> stats = null;

            try
            {
                conn = DbConnectionFactory.Build(ConfigurationManager.ConnectionStrings["Lightning"].ConnectionString);
                TlnStationGetAllDbCmdText dbCmdText = new TlnStationGetAllDbCmdText();
                dbCmd = new DbCommand(conn, dbCmdText);
                dbCmd.Command.CommandTimeout = Config.DbCommandTimeoutMilliseconds;
                dr = dbCmd.ExecuteReader();
                stats = new List<Station>(DefaultCapacity);
                while (dr.Read())
                {
                    Station station = dbCmdText.ParseRowTlnLx(dr);
                    if (null != station)
                        stats.Add(station);
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.ApiStationListGet, string.Format("Unable to get station list. Message: {0}", ex.Message));
            }
            finally
            {
                if (null != dbCmd) dbCmd.Close();
                if (null != dr) dr.Close();
                if (null != conn) conn.Close();
            }

            return stats;
        }

        private IEnumerable<Station> GetAllTls()
        {
            SqlConnection conn = null;
            SqlDataReader dr = null;
            DbCommand dbCmd = null;
            List<Station> list = null;

            try
            {
                conn = DbConnectionFactory.Build(ConfigurationManager.ConnectionStrings["AwsData2"].ConnectionString);
                StationGetAllDbCmdText stationListDbCmdText = new StationGetAllDbCmdText();
                dbCmd = new DbCommand(conn, stationListDbCmdText);
                dbCmd.Command.CommandTimeout = Config.DbCommandTimeoutMilliseconds;
                dr = dbCmd.ExecuteReader();
                list = new List<Station>(DefaultCapacity);
                Station station = null;
                while (dr.Read())
                {
                    station = stationListDbCmdText.ParseRowTls(dr);
                    if (null != station)
                        list.Add(station);
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.ApiStationListGet, string.Format("Unable to get station list. Message: {0}", ex.Message));
            }
            finally
            {
                if (null != dbCmd) dbCmd.Close();
                if (null != dr) dr.Close();
                if (null != conn) conn.Close();
            }

            return list;
        }

        public Station Add(Station newStation)
        {
            SqlConnection conn = null;
            SqlDataReader dr = null;
            DbCommand dbCmd = null;
            Station station = null;

            try
            {
                conn = DbConnectionFactory.Build(ConfigurationManager.ConnectionStrings["AwsData2"].ConnectionString);
                StationCreateDbCmdText dbCmdText = new StationCreateDbCmdText(newStation);
                dbCmd = new DbCommand(conn, dbCmdText);
                dbCmd.Command.CommandTimeout = Config.DbCommandTimeoutMilliseconds;

                //create a param to capture the return value from the stored procedure
                SqlParameter returnParam = dbCmd.Command.Parameters.Add("@ReturnValue", SqlDbType.Int);
                returnParam.Direction = ParameterDirection.ReturnValue;
                dr = dbCmd.ExecuteReader();

                // returnValue == -2 means invalid due to duplicate serial number
                if (returnParam.Value != null)
                {
                    int returnValue = (int)returnParam.Value;
                }
                

                if (dr.Read())
                    station = (dbCmdText.ParseRowTls(dr)) as Station;
            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.ApiStationAdd, string.Format("Unable to add station metadata. Message: {0}", ex.Message));
            }
            finally
            {
                if (null != dbCmd) dbCmd.Close();
                if (null != dr) dr.Close();
                if (null != conn) conn.Close();
            }

            try
            {
                if (station != null && station.Id != "" && station.AntennaAttenuation.HasValue)
                {
                    conn =
                        DbConnectionFactory.Build(ConfigurationManager.ConnectionStrings["Lightning"].ConnectionString);
                    SensorCaliAntLengthCreateDbCmdText dbCmdText = new SensorCaliAntLengthCreateDbCmdText(station.Id,
                                                                                                          station.AntennaAttenuation.Value);
                    dbCmd = new DbCommand(conn, dbCmdText);
                    dbCmd.Command.CommandTimeout = Config.DbCommandTimeoutMilliseconds;
                    dbCmd.ExecuteNonQuery();
                }
                else
                {
                    station = null;
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.ApiStationAdd, string.Format("Unable to add station calibration value. Message: {0}", ex.Message));
            }
            finally
            {
                if (null != dbCmd) dbCmd.Close();
                if (null != dr) dr.Close();
                if (null != conn) conn.Close();
            }

            return station;
        }

        public int Update(Station updateStation)
        {
            SqlConnection conn = null;
            DbCommand dbCmd = null;
            const int success = 1; 

            try
            {
                conn = DbConnectionFactory.Build(ConfigurationManager.ConnectionStrings["Awsdata2"].ConnectionString);
                StationUpdateDbCmdText dbCmdText = new StationUpdateDbCmdText(updateStation);
                dbCmd = new DbCommand(conn, dbCmdText);
                dbCmd.Command.CommandTimeout = Config.DbCommandTimeoutMilliseconds;
                //dbCmd.ExecuteNonQuery();
                //success = true;

                //create a param to capture the return value from the stored procedure
                SqlParameter returnParam = dbCmd.Command.Parameters.Add("@ReturnValue", SqlDbType.Int);
                returnParam.Direction = ParameterDirection.ReturnValue;
                dbCmd.ExecuteNonQuery();

                int returnValue = (int)returnParam.Value;
                if (returnValue == -2)
                {
                    return returnValue; 
                }

            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.ApiStationUpdate, string.Format("Unable to update station. Message: {0}", ex.Message));
            }
            finally
            {
                if (null != dbCmd) dbCmd.Close();
                if (null != conn) conn.Close();
            }

            return success;
        }

        public bool Remove(string id)
        {
            bool result = false;
            SqlConnection conn = null;
            DbCommand dbCmd = null;

            try
            {
                conn = DbConnectionFactory.Build(ConfigurationManager.ConnectionStrings["AwsData2"].ConnectionString);
                StationDeleteDbCmdText dbCmdText = new StationDeleteDbCmdText(id);
                dbCmd = new DbCommand(conn, dbCmdText);
                dbCmd.Command.CommandTimeout = Config.DbCommandTimeoutMilliseconds;
                dbCmd.ExecuteNonQuery();
                result = true;
            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.ApiStationDelete, string.Format("Unable to delete station. Message: {0}", ex.Message));
            }
            finally
            {
                if (null != dbCmd) dbCmd.Close();
                if (null != conn) conn.Close();
            }

            return result;
        }

        public void AddStationToNetwork(string stationId, string networkId)
        {
            SqlConnection conn = null;
            DbCommand dbCmd = null;

            try
            {
                conn = DbConnectionFactory.Build(ConfigurationManager.ConnectionStrings["Lightning"].ConnectionString);
                StationAddToNetworkDbCmdText dbCmdText = new StationAddToNetworkDbCmdText(stationId, networkId);
                dbCmd = new DbCommand(conn, dbCmdText);
                dbCmd.Command.CommandTimeout = Config.DbCommandTimeoutMilliseconds;
                dbCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.ApiStationListGet, string.Format("Unable to add station to network. Message: {0}", ex.Message));
                throw;
            }
            finally
            {
                if (null != dbCmd) dbCmd.Close();
                if (null != conn) conn.Close();
            }
        }

        public void RemoveStationFromNetwork(string stationId, string networkId)
        {
            SqlConnection conn = null;
            DbCommand dbCmd = null;

            try
            {
                conn = DbConnectionFactory.Build(ConfigurationManager.ConnectionStrings["Lightning"].ConnectionString);
                StationRemoveFromNetworkDbCmdText dbCmdText = new StationRemoveFromNetworkDbCmdText(stationId, networkId);
                dbCmd = new DbCommand(conn, dbCmdText);
                dbCmd.Command.CommandTimeout = Config.DbCommandTimeoutMilliseconds;
                dbCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.ApiStationListGet, string.Format("Unable to remove station from network. Message: {0}", ex.Message));
                throw;
            }
            finally
            {
                if (null != dbCmd) dbCmd.Close();
                if (null != conn) conn.Close();
            }
        }

        public IEnumerable<StationV2> GetAllStationsInNetwork(string networkId)
        {
            SqlConnection conn = null;
            DbCommand dbCmd = null;
            SqlDataReader dr = null;
            IEnumerable<StationV2> networkStations = null;
            try
            {
                conn = DbConnectionFactory.Build(ConfigurationManager.ConnectionStrings["Lightning"].ConnectionString);
                StationGetAllInNetworkkDbCmdText dbCmdText = new StationGetAllInNetworkkDbCmdText(networkId);
                dbCmd = new DbCommand(conn, dbCmdText);
                dbCmd.Command.CommandTimeout = Config.DbCommandTimeoutMilliseconds;
                dr = dbCmd.ExecuteReader();

                if (dr.HasRows)
                {
                    HashSet<string> networkStats = new HashSet<string>();

                    while (dr.Read())
                    {
                        string stationId = dbCmdText.ParseRow(dr) as string;
                        if (stationId != null)
                        {
                            networkStats.Add(stationId);
                        }
                    }

                    if (networkStats.Count > 0)
                    {
                        IEnumerable<StationV2> allStats = GetAllV2();
                        networkStations = MatchStations(networkStats, allStats);
                    }

                }   
            }          
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.ApiStationListGet, string.Format("Unable to remove station from network. Message: {0}", ex.Message));
                throw;
            }
            finally
            {
                if (null != dbCmd) dbCmd.Close();
                if (null != conn) conn.Close();
            }

            return networkStations;
        }

        private IEnumerable<StationV2> MatchStations(HashSet<string> networkStats, IEnumerable<StationV2> allStats)
        {
            IList<StationV2> all = allStats as IList<StationV2> ?? allStats.ToList();
            
            if (all != null && all.Count > 0)
            {
                int i = 0;
                
                while (i < all.Count)
                {
                    if (networkStats.Contains(all[i].Id))
                    {
                        i++;
                    }
                    else
                    {
                        all.RemoveAt(i);
                    }
                }
            }

            return all;
        }

        public IEnumerable<StationV2> GetAllV2()
        {
            IEnumerable<StationV2> stations = null;

            if (Config.SystemType == "TLN")
            {
                stations = GetAllTlnV2();
            }
            

            return stations;
        }

        public StationV2 GetV2(string id)
        {
            StationV2 station = null;

            if (Config.SystemType == "TLN")
            {
                station = GetTlnV2(id);
            }

            return station;
        }

        private StationV2 GetTlnV2(string id)
        {
            StationV2 station = GetTlnLxV2(id);
            if (station != null)
            {
                Station stat = GetTlnAws(id);
                if (stat != null)
                {
                    station = MergeStationV2(station, stat);
                }
            }
            return station;
        }

        private IEnumerable<StationV2> GetAllTlnV2()
        {
            List<StationV2> stations = GetAllTlnLxV2();
            if (stations != null && stations.Count > 0)
            {
                Dictionary<string, Station> statTable = GetAllTlnAws();
                if (statTable != null && statTable.Count > 0)
                {
                    for (int i = 0; i < stations.Count; i++)
                    {
                        Station stat;
                        if (statTable.TryGetValue(stations[i].Id, out stat))
                        {
                            stations[i] = MergeStationV2(stations[i], stat);
                        }
                    }
                }
            }

            return stations;
        }

        private StationV2 MergeStationV2(StationV2 lxStat, Station awsStat)
        {
            lxStat.Name = awsStat.Name;
            lxStat.StreetAddress = awsStat.StreetAddress;
            lxStat.Country = awsStat.Country;
            lxStat.Province = awsStat.Province;
            lxStat.Region = awsStat.Region;
            lxStat.City = awsStat.City;
            lxStat.PostalCode = awsStat.PostalCode;
            lxStat.InstallDate = awsStat.InstallDate;
            lxStat.SerialNumber = awsStat.SerialNumber;
            lxStat.PocName = awsStat.PocName;
            lxStat.PocPhone = awsStat.PocPhone;
            lxStat.PocEmail = awsStat.PocEmail;
            lxStat.SendLogRateMinutes = awsStat.SendLogRateMinutes;
            lxStat.SendGpsRateMinutes = awsStat.SendGpsRateMinutes;
            lxStat.SendAntennaGainRateMinutes = awsStat.SendAntennaGainRateMinutes;
            lxStat.AntennaMode = awsStat.AntennaMode;
            lxStat.AntennaAttenuation = awsStat.AntennaAttenuation;
            lxStat.PrimaryDeliveryIp = awsStat.PrimaryDeliveryIp;
            lxStat.SecondaryDeliveryIp = awsStat.SecondaryDeliveryIp;
            lxStat.LastCallHome = awsStat.LastCallHome;
            lxStat.IndoorFirmware = awsStat.IndoorFirmware;
            lxStat.BackUpBatteryVol = awsStat.BackUpBatteryVol;
            lxStat.MemBackUpVoltage = awsStat.MemBackUpVoltage;
            lxStat.LastPacketUtc = awsStat.LastPacketUtc;
            lxStat.Displayer = awsStat.Displayer;
            lxStat.WindSensor = awsStat.WindSensor;
            lxStat.AntennaHwRev = awsStat.AntennaHwRev;
            lxStat.AntennaSerial = awsStat.AntennaSerial;
            lxStat.RemoteSerial = awsStat.RemoteSerial;

            //check the lastPacketUtc here 
            if (lxStat.LastPacketUtc.HasValue && lxStat.LastPacketUtc != null)
            {
                TimeSpan ts = DateTime.UtcNow - lxStat.LastPacketUtc.Value;

                if (ts.TotalMinutes > Config.LastPacketReceivedInterval)
                {
                    lxStat.PacketPercent = 0;
                }
            }

            lxStat.Status = Utilities.GetSensorStatus(lxStat.LastCallHome, lxStat.PacketPercent, lxStat.IsEnable);

            return lxStat;
        }

        private List<StationV2> GetAllTlnLxV2()
        {
            SqlConnection conn = null;
            SqlDataReader dr = null;
            DbCommand dbCmd = null;
            List<StationV2> stats = null;

            try
            {
                conn = DbConnectionFactory.Build(ConfigurationManager.ConnectionStrings["Lightning"].ConnectionString);
                TlnStationGetAllV2DbCmdText dbCmdText = new TlnStationGetAllV2DbCmdText();
                dbCmd = new DbCommand(conn, dbCmdText);
                dbCmd.Command.CommandTimeout = Config.DbCommandTimeoutMilliseconds;
                dr = dbCmd.ExecuteReader();
                stats = new List<StationV2>(DefaultCapacity);
                string prevId = string.Empty;
                while (dr.Read())
                {
                    StationV2 station = dbCmdText.ParseRowTlnLx(dr);
                    if (null != station)
                    {
                        if (prevId == station.Id && station.Networks != null && station.Networks.Count > 0)
                        {
                            stats[stats.Count - 1].Networks.Add(station.Networks[0]);
                        }
                        else
                        {
                            stats.Add(station);
                        }
                        
                        prevId = station.Id;
                    }
                    
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.ApiStationListGet, string.Format("Unable to get station list. Message: {0}", ex.Message));
            }
            finally
            {
                if (null != dbCmd) dbCmd.Close();
                if (null != dr) dr.Close();
                if (null != conn) conn.Close();
            }

            return stats;
        }

        private StationV2 GetTlnLxV2(string id)
        {
            SqlConnection conn = null;
            SqlDataReader dr = null;
            DbCommand dbCmd = null;
            StationV2 station = null;

            try
            {
                conn = DbConnectionFactory.Build(ConfigurationManager.ConnectionStrings["Lightning"].ConnectionString);
                TlnStationGetV2DbCmdText dbCmdText = new TlnStationGetV2DbCmdText(id);
                dbCmd = new DbCommand(conn, dbCmdText);
                dbCmd.Command.CommandTimeout = Config.DbCommandTimeoutMilliseconds;
                dr = dbCmd.ExecuteReader();

                string prevId = string.Empty;
                while (dr.Read())
                {
                    StationV2 stat = dbCmdText.ParseRowTlnLx(dr);
                    if (null != stat)
                    {
                        if (station == null)
                        {
                            station = stat;
                        }
                        else if (prevId == stat.Id && stat.Networks != null && stat.Networks.Count > 0)
                        {
                            if (station.Networks == null)
                            {
                                station.Networks = new List<Network>();
                            }
                            station.Networks.Add(stat.Networks[0]);
                        }
                        
                        prevId = station.Id;
                    }
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.ApiStationGet, string.Format("Unable to get station.  Message: {0}", ex.Message));
            }
            finally
            {
                if (null != dbCmd) dbCmd.Close();
                if (null != dr) dr.Close();
                if (null != conn) conn.Close();
            }

            return station;
        }

        private class TlnStationGetAllV2DbCmdText : DbCommandText
        {
            private readonly string _stationId;
            private readonly string _networkId;
            public TlnStationGetAllV2DbCmdText()
            {
                InitSP();
            }

            public override string BuildCmdText()
            {
                return "dbo.TlnStationListGetV2_pr";
            }

           
            public StationV2 ParseRowTlnLx(SqlDataReader dr)
            {
                StationV2 station = null;

                string id = GetDataString(dr, "Id", null);

                if (id != null)
                {
                    id = id.ToUpper();

                    station = new StationV2
                    {
                        Id = id,
                        Ip = GetDataString(dr, "Ip", null),
                        LastCalibration = DbHelper.GetDataNullableUtcDateTime(dr, "LastCalibration"),
                        OutdoorFirmware = GetDataString(dr, "OutdoorFirmware", null),
                        PacketPercent = DbHelper.GetDataNullableInt(dr, "PacketPercent"),
                        Latitude = GetDataDecimal(dr, "Latitude"),
                        Longitude = GetDataDecimal(dr, "Longitude"),
                        Networks = null
                    };
                    int? qcFlagValue = DbHelper.GetDataNullableShort(dr, "QCFlag");
                    string networkId = GetDataString(dr, "NetworkId", null);
                    string networkName = GetDataString(dr, "NetworkName", null);
                    string networkDescription = GetDataString(dr, "NetworkDescription", null);

                    
                    
                    if (networkId != null)
                    {
                        station.Networks = new List<Network> 
                        {
                            new Network
                            {
                                Id = networkId,
                                Name = networkName,
                                Description = networkDescription
                            }
                        };
                    }

                    station.IsEnable = !qcFlagValue.HasValue || qcFlagValue.Value == 100;
                    
                }

                return station;
            }

        }

        private class TlnStationGetV2DbCmdText : DbCommandText
        {
            private string _id;
            public TlnStationGetV2DbCmdText(string id)
            {
                _id = id;
                InitSP();
            }

            public override string BuildCmdText()
            {
                return "dbo.TlnStationGetV2_pr";
            }

            public override ArrayList BuildParameters()
            {
                AddParam("@Id", _id, System.Data.SqlDbType.VarChar, 10);

                return ParamList;
            }

            internal StationV2 ParseRowTlnLx(SqlDataReader dr)
            {
                StationV2 station = null;

                string id = GetDataString(dr, "Id", null);

                if (id != null)
                {
                    id = id.ToUpper();

                    station = new StationV2
                    {
                        Id = id,
                        Ip = GetDataString(dr, "Ip", null),
                        LastCalibration = DbHelper.GetDataNullableUtcDateTime(dr, "LastCalibration"),
                        OutdoorFirmware = GetDataString(dr, "OutdoorFirmware", null),
                        PacketPercent = DbHelper.GetDataNullableInt(dr, "PacketPercent"),
                        Latitude = GetDataDecimal(dr, "Latitude"),
                        Longitude = GetDataDecimal(dr, "Longitude"),
                        Networks = null
                    };
                    int? qcFlagValue = DbHelper.GetDataNullableShort(dr, "QCFlag");
                    string networkId = GetDataString(dr, "NetworkId", null);
                    string networkName = GetDataString(dr, "NetworkName", null);
                    string networkDescription = GetDataString(dr, "NetworkDescription", null);

                    if (networkId != null)
                    {
                        station.Networks = new List<Network> 
                        {
                            new Network
                            {
                                Id = networkId,
                                Name = networkName,
                                Description = networkDescription
                            }
                        };
                    }

                    station.IsEnable = !qcFlagValue.HasValue || qcFlagValue.Value == 100;

                }

                return station;
            }
        }

        private class StationAddToNetworkDbCmdText : DbCommandText
        {
            private readonly string _stationId;
            private readonly string _networkId;
            public StationAddToNetworkDbCmdText(string stationId, string networkId)
            {
                _stationId = stationId;
                _networkId = networkId;
                InitSP();
            }

            public override string BuildCmdText()
            {
                return "dbo.LtgStationAddToNetwork_pr";
            }

            public override ArrayList BuildParameters()
            {
                AddParam("@StationId", _stationId, SqlDbType.VarChar, 10);
                AddParam("@NetworkId", _networkId, SqlDbType.NChar, 10);

                return ParamList;
            }
        }

        private class StationRemoveFromNetworkDbCmdText : DbCommandText
        {
            private readonly string _stationId;
            private readonly string _networkId;
            public StationRemoveFromNetworkDbCmdText(string stationId, string networkId)
            {
                _stationId = stationId;
                _networkId = networkId;
                InitSP();
            }

            public override string BuildCmdText()
            {
                return "dbo.LtgStationRemoveFromNetwork_pr";
            }

            public override ArrayList BuildParameters()
            {
                AddParam("@StationId", _stationId, SqlDbType.VarChar, 10);
                AddParam("@NetworkId", _networkId, SqlDbType.NChar, 10);

                return ParamList;
            }
        }

        private class StationGetAllInNetworkkDbCmdText : DbCommandText
        {
            private readonly string _networkId;
            public StationGetAllInNetworkkDbCmdText(string networkId)
            {
                _networkId = networkId;
                InitSP();
            }

            public override string BuildCmdText()
            {
                return "dbo.LtgNetworkGetAllStations_pr";
            }

            public override ArrayList BuildParameters()
            {
                AddParam("@NetworkId", _networkId, SqlDbType.NChar, 10);

                return ParamList;
            }

            public override object ParseRow(SqlDataReader dr)
            {
                return GetDataString(dr, "Station_ID", null);
            }
        }
    }
}