﻿using System;
using System.Collections.Generic;
using LxPortalLib.Models; 


namespace LxPortal.Api.Models
{
    public interface ICellTrackRepository 
    {
        List<CellTrackRecord> Get(DateTime start, DateTime end, decimal ullat, decimal ullon, decimal lrlat, decimal lrlon); 
    }
}