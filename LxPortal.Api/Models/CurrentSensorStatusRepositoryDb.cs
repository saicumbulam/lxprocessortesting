﻿using System;
using System.Configuration;
using System.Data.SqlClient;

using Aws.Core.Data;
using Aws.Core.Utilities;

using LxCommon;
using LxCommon.Monitoring;
using LxPortalLib.Models;

using LxPortal.Api.Models.Database;

namespace LxPortal.Api.Models
{
    public class CurrentSensorStatusRepositoryDb : ICurrentSensorStatusRepository
    {
        #region ICurrentSensorStatusRepository Members

        public CurrentSensorStatus Get(string id)
        {
            SqlConnection conn = null;
            SqlDataReader dr = null;
            DbCommand dbCmd = null;
            CurrentSensorStatus status = null;

            try
            {
                conn = DbConnectionFactory.Build(ConfigurationManager.ConnectionStrings["Lightning"].ConnectionString);
                CurrentSensorStatusGetDbCmdText dbCmdText = new CurrentSensorStatusGetDbCmdText(id);
                dbCmd = new DbCommand(conn, dbCmdText);
                dbCmd.Command.CommandTimeout = Config.DbCommandTimeoutMilliseconds;
                dr = dbCmd.ExecuteReader();

                status = dbCmdText.Parse(dr);
            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.ApiCurrentSensorStatusGet, string.Format("Unable to get current sensor status. Message: {0}", ex.Message));
            }
            finally
            {
                if (null != dbCmd) dbCmd.Close();
                if (null != dr) dr.Close();
                if (null != conn) conn.Close();
            }

            if (status != null && status.Hardware != null && Config.SystemType == "TLN")
            {
                Station stat = StationRepositoryDb.GetTlnAws(id);
                if (stat != null)
                {
                    status.Hardware.LastCallHomeUtc = stat.LastCallHome;
                    status.Hardware.MemBackUpVoltage = stat.MemBackUpVoltage;
                    status.Hardware.BackUpBatteryVol = stat.BackUpBatteryVol; 
                }
            }

            return status;
        }

        #endregion
    }
}