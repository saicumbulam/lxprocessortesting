﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;

using Aws.Core.Data;
using Aws.Core.Utilities;

using LxCommon;
using LxCommon.Monitoring;
using LxPortalLib.Models;

using LxPortal.Api.Models.Database;

namespace LxPortal.Api.Models
{
    public class CellTrackRepositoryDb : ICellTrackRepository
    {
        protected static readonly int DataMartAgeHours = Config.DataMartAgeHours;
        protected static readonly bool UseDataMartArchive = Config.UseDataMartArchive;
        protected static readonly long DataMartArchiveEndDateKey = Config.DataMartArchiveEndDateKey;
        protected static readonly long DataMartStartDateKey = Config.DataMartStartDateKey;

        public List<CellTrackRecord> Get(DateTime start, DateTime end, decimal ullat, decimal ullon, decimal lrlat, decimal lrlon)
        {
            List<CellTrackRecord> tracks = null;

            QuerySource qs = QuerySource.GetQuerySource(start, end, DateTime.UtcNow, DataMartAgeHours, UseDataMartArchive, DataMartArchiveEndDateKey, DataMartStartDateKey);

            if (qs.UseDataMart)
                tracks = GetFromDataMart(qs.DataMartStartKey, qs.DataMartEndKey, ullat, ullon, lrlat, lrlon, "LightningDM");

            if (qs.UseDataMartArchive)
            {
                if (tracks == null)
                    tracks = GetFromDataMart(qs.DataMartArchiveStartKey, qs.DataMartArchiveEndKey, ullat, ullon, lrlat, lrlon, "LightningDMArchive");
                else
                {
                    List<CellTrackRecord> archiveTracks = GetFromDataMart(qs.DataMartArchiveStartKey, qs.DataMartArchiveEndKey, ullat, ullon, lrlat, lrlon, "LightningDMArchive");
                    tracks.AddRange(archiveTracks);
                }

            }

            //massage data set to return specified start/end time 
            tracks = MassageDataSet(start, end, tracks);

            return tracks;
        }

        public List<CellTrackRecord> GetFromDataMart(long start, long end, decimal ullat, decimal ullon, decimal lrlat, decimal lrlon, string connectionName)
        {
            SqlConnection conn = null;
            SqlDataReader dr = null;
            DbCommand dbCmd = null;
            List<CellTrackRecord> tracks = new List<CellTrackRecord>();

            try
            {
                conn = DbConnectionFactory.Build(ConfigurationManager.ConnectionStrings[connectionName].ConnectionString);
                CellTrackDmDbGetCmdText dbCmdText = new CellTrackDmDbGetCmdText(start, end, ullat, ullon, lrlat, lrlon);
                dbCmd = new DbCommand(conn, dbCmdText);
                dbCmd.Command.CommandTimeout = Config.DbCommandTimeoutMilliseconds;
                dr = dbCmd.ExecuteReader();
                while (dr.Read())
                {
                    CellTrackRecord track = dbCmdText.ParseRow(dr) as CellTrackRecord;
                    if (track != null)
                        tracks.Add(track);
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.ApiCellTrackGet, string.Format("Unable to get cell track. Message: {0}", ex.Message), ex);

            }
            finally
            {
                if (null != dbCmd) dbCmd.Close();
                if (null != dr) dr.Close();
                if (null != conn) conn.Close();
            }

            return tracks;
        }
        
        private List<CellTrackRecord>  MassageDataSet(DateTime start, DateTime end, List<CellTrackRecord> cellTracks)
        {
            List<CellTrackRecord> tracks = new List<CellTrackRecord>();
            if (cellTracks != null && cellTracks.Count > 0)
            {
                foreach (CellTrackRecord track in cellTracks)
                {
                     if(track.CellTimeUtc >= start && track.CellTimeUtc <= end)
                     {
                         tracks.Add(track);
                     }
                }
            }
            return tracks; 
        }
    }
}