﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;

using Aws.Core.Data;
using Aws.Core.Utilities;

using LxCommon.Monitoring;
using LxPortalLib.Models;

using LxPortal.Api.Models.Database;

namespace LxPortal.Api.Models
{
    public class StationNetworkRepositoryDb
    {
        public List<Network> GetNetworks()
        {
            SqlConnection conn = null;
            SqlDataReader dr = null;
            DbCommand dbCmd = null;
            List<Network> networks = new List<Network>();

            try
            {
                conn = DbConnectionFactory.Build(ConfigurationManager.ConnectionStrings["Lightning"].ConnectionString);
                StationNetworkDbCmdText dbCmdText = new StationNetworkDbCmdText();
                dbCmd = new DbCommand(conn, dbCmdText);
                dbCmd.Command.CommandTimeout = Config.DbCommandTimeoutMilliseconds;
                dr = dbCmd.ExecuteReader();

                Network network;
                while (dr.Read())
                {
                    network = (dbCmdText.ParseRow(dr)) as Network;
                    if (network != null)
                    {
                        networks.Add(network);
                    }
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.ApiStationGet, string.Format("Unable to get station networks.  Message: {0}", ex.Message));
            }
            finally
            {
                if (null != dbCmd) dbCmd.Close();
                if (null != dr) dr.Close();
                if (null != conn) conn.Close();
            }

            return networks;
        }
    }
}