﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using Aws.Core.Data;
using Aws.Core.Utilities;
using LxCommon;
using LxCommon.Monitoring;
using LxPortalLib.Models;
using LxPortal.Api.Models.Database;

namespace LxPortal.Api.Models
{
    public class CellAlertRepositoryDb : ICellAlertRepository
    {
        protected static readonly int DataMartAgeHours = Config.DataMartAgeHours;
        protected static readonly bool UseDataMartArchive = Config.UseDataMartArchive;
        protected static readonly long DataMartArchiveEndDateKey = Config.DataMartArchiveEndDateKey;
        protected static readonly long DataMartStartDateKey = Config.DataMartStartDateKey;

        public List<CellAlertRecord> Get(DateTime start, DateTime end, decimal ullat, decimal ullon, decimal lrlat, decimal lrlon)
        {
            QuerySource qs = QuerySource.GetQuerySource(start, end, DateTime.UtcNow, DataMartAgeHours, UseDataMartArchive, DataMartArchiveEndDateKey, DataMartStartDateKey);
            List<CellAlertRecord> alerts = null;
            if (qs.UseDataMart)
            {
                alerts = GetFromDataMart(qs.DataMartStartKey, qs.DataMartEndKey, ullat, ullon, lrlat, lrlon, "LightningDM");
            }

            if (qs.UseDataMartArchive)
            {
                if (alerts == null)
                    alerts = GetFromDataMart(qs.DataMartArchiveStartKey, qs.DataMartArchiveEndKey, ullat, ullon, lrlat, lrlon, "LightningDMArchive");
                else
                {
                    List<CellAlertRecord> archiveAlerts = GetFromDataMart(qs.DataMartArchiveStartKey, qs.DataMartArchiveEndKey, ullat, ullon, lrlat, lrlon, "LightningDMArchive");
                    alerts.AddRange(archiveAlerts);
                }
            }

            //massage data set to return specified start/end time 
            alerts = MassageDataSet(start, end, alerts);

            return alerts;
        }

        private List<CellAlertRecord> GetFromDataMart(long startKey, long endKey, decimal ullat, decimal ullon, decimal lrlat, decimal lrlon, string connectionName)
        {
            SqlConnection conn = null;
            SqlDataReader dr = null;
            DbCommand dbCmd = null;
            List<CellAlertRecord> alerts = new List<CellAlertRecord>();

            try
            {
                conn = DbConnectionFactory.Build(ConfigurationManager.ConnectionStrings[connectionName].ConnectionString);
                CellAlertDmDbGetCmdText dbCmdText = new CellAlertDmDbGetCmdText(startKey, endKey, ullat, ullon, lrlat, lrlon);
                dbCmd = new DbCommand(conn, dbCmdText);
                dbCmd.Command.CommandTimeout = Config.DbCommandTimeoutMilliseconds;
                dr = dbCmd.ExecuteReader();
                while (dr.Read())
                {
                    CellAlertRecord alert = dbCmdText.ParseRow(dr) as CellAlertRecord;
                    if (alert != null)
                        alerts.Add(alert);
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.ApiCellAlertGet, string.Format("Unable to get cell alert. Message: {0}", ex.Message), ex);

            }
            finally
            {
                if (null != dbCmd) dbCmd.Close();
                if (null != dr) dr.Close();
                if (null != conn) conn.Close();
            }

            return alerts;
        }

        private List<CellAlertRecord> MassageDataSet(DateTime start, DateTime end, List<CellAlertRecord> cellAlerts)
        {
            //1. check start/end time 
            //2. remove future data
            List<CellAlertRecord> dataSet = new List<CellAlertRecord>();
            if (cellAlerts != null && cellAlerts.Count > 0)
            {
                foreach (CellAlertRecord alert in cellAlerts)
                {
                    if (alert.CellTimeUtc >= start && alert.CellTimeUtc <= end)
                    {
                        dataSet.Add(alert);
                    }
                }
            }
            return dataSet; 
        }
    }
}