﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LxPortalLib.Models;

namespace LxPortal.Api.Models
{
    public interface ISensorPacketHistoryRepository
    {
        List<PacketRecord> Get(string id, DateTime? start, DateTime? end);
    }

}
