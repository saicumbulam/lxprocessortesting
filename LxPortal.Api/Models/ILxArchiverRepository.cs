﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LxPortalLib.Models;

namespace LxPortal.Api.Models
{
    interface ILxArchiverRepository
    {
        List<LxArchiverRecord> Get(DateTime start, DateTime end, decimal ullat, decimal ullon, decimal lrlat, decimal lrlon); 
    }
}
