﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Aws.Core.Data;
using Aws.Core.Utilities;
using LxCommon.Monitoring;
using LxPortal.Api.Models.Database;
using LxPortalLib.Models;

namespace LxPortal.Api.Models
{
    public interface INetworkRepository
    {
        void Update(string id, Network updateNetwork);

        IEnumerable<Network> GetAll();

        Network Add(Network network);

        void Remove(string id);
    }
    
    public class NetworkDbRepository : INetworkRepository 
    {
        public void Update(string id, Network updateNetwork)
        {
            SqlConnection conn = null;
            DbCommand dbCmd = null;

            try
            {
                conn = DbConnectionFactory.Build(ConfigurationManager.ConnectionStrings["Lightning"].ConnectionString);
                NetworkUpdateDbCmdText dbCmdText = new NetworkUpdateDbCmdText(id, updateNetwork);
                dbCmd = new DbCommand(conn, dbCmdText);
                dbCmd.Command.CommandTimeout = Config.DbCommandTimeoutMilliseconds;
                dbCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.ApiStationListGet, string.Format("Unable to update network. Message: {0}", ex.Message));
                throw;
            }
            finally
            {
                if (null != dbCmd) dbCmd.Close();
                if (null != conn) conn.Close();
            }
        }

        public IEnumerable<Network> GetAll()
        {
            
            SqlConnection conn = null;
            SqlDataReader dr = null;
            DbCommand dbCmd = null;
            List<Network> networks = null;

            try
            {
                conn = DbConnectionFactory.Build(ConfigurationManager.ConnectionStrings["Lightning"].ConnectionString);
                NetworkGetAllDbCmdText dbCmdText = new NetworkGetAllDbCmdText();
                dbCmd = new DbCommand(conn, dbCmdText);
                dbCmd.Command.CommandTimeout = Config.DbCommandTimeoutMilliseconds;
                dr = dbCmd.ExecuteReader();
                if (dr.HasRows)
                {
                    networks = new List<Network>();
                    while (dr.Read())
                    {
                        Network network = dbCmdText.ParseRow(dr) as Network;
                        if (null != network)
                        {
                            networks.Add(network);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.ApiStationListGet, string.Format("Unable to get network list. Message: {0}", ex.Message));
                throw;
            }
            finally
            {
                if (null != dbCmd) dbCmd.Close();
                if (null != dr) dr.Close();
                if (null != conn) conn.Close();
            }
            
            return networks;
        }

        public Network Add(Network network)
        {
            SqlConnection conn = null;
            DbCommand dbCmd = null;
            
            try
            {
                conn = DbConnectionFactory.Build(ConfigurationManager.ConnectionStrings["Lightning"].ConnectionString);
                NetworkAddDbCmdText dbCmdText = new NetworkAddDbCmdText(network);
                dbCmd = new DbCommand(conn, dbCmdText);
                dbCmd.Command.CommandTimeout = Config.DbCommandTimeoutMilliseconds;
                dbCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.ApiStationListGet, string.Format("Unable to get network list. Message: {0}", ex.Message));
                throw;
            }
            finally
            {
                if (null != dbCmd) dbCmd.Close();
                if (null != conn) conn.Close();
            }

            return network;
        }

        public void Remove(string id)
        {
            SqlConnection conn = null;
            DbCommand dbCmd = null;

            try
            {
                conn = DbConnectionFactory.Build(ConfigurationManager.ConnectionStrings["Lightning"].ConnectionString);
                NetworkDeleteDbCmdText dbCmdText = new NetworkDeleteDbCmdText(id);
                dbCmd = new DbCommand(conn, dbCmdText);
                dbCmd.Command.CommandTimeout = Config.DbCommandTimeoutMilliseconds;
                dbCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.ApiStationListGet, string.Format("Unable to delete network. Message: {0}", ex.Message));
                throw;
            }
            finally
            {
                if (null != dbCmd) dbCmd.Close();
                if (null != conn) conn.Close();
            }
        }

        private class NetworkGetAllDbCmdText : DbCommandText
        {
            public NetworkGetAllDbCmdText()
            {
                InitSP();
            }

            public override string BuildCmdText()
            {
                return "dbo.LtgNetworkGetAll_pr";
            }

            public override object ParseRow(SqlDataReader dr)
            {
                Network network = null;
                string id = GetDataString(dr, "LtgNetworkID", null);
                if (id != null)
                {
                    network = new Network
                    {
                        Id = id,
                        Name = GetDataString(dr, "LtgNetworkName", null),
                        Description = GetDataString(dr, "Description", null)
                    };
                }
                     
                return network;
            }
        }

        private class NetworkAddDbCmdText : DbCommandText
        {
            private readonly Network _network;
            public NetworkAddDbCmdText(Network network)
            {
                if (network == null)
                {
                    throw new ArgumentNullException("network");
                }
                _network = network;
                InitSP();
            }

            public override string BuildCmdText()
            {
                return "dbo.LtgNetworkCreate_pr";
            }

            public override ArrayList BuildParameters()
            {
                AddParam("@Id", _network.Id, SqlDbType.NChar, 10);
                AddParam("@Name", _network.Name, SqlDbType.VarChar, 50);
                AddParam("@Desc", _network.Description, SqlDbType.VarChar, 250);

                return ParamList;
            }
        }

        private class NetworkUpdateDbCmdText : DbCommandText
        {
            private readonly string _id;
            private readonly Network _network;
            public NetworkUpdateDbCmdText(string id, Network network)
            {
                if (string.IsNullOrWhiteSpace(id))
                {
                    throw new ArgumentNullException("id");
                }
                _id = id;

                if (network == null)
                {
                    throw new ArgumentNullException("network");
                }
                _network = network;
                
                InitSP();
            }

            public override string BuildCmdText()
            {
                return "dbo.LtgNetworkUpdate_pr";
            }

            public override ArrayList BuildParameters()
            {
                AddParam("@Id", _id, SqlDbType.NChar, 10);
                AddParam("@Name", _network.Name, SqlDbType.VarChar, 50);
                AddParam("@Desc", _network.Description, SqlDbType.VarChar, 250);

                return ParamList;
            }
        }

        private class NetworkDeleteDbCmdText : DbCommandText
        {
            private readonly string _id;
            private readonly Network _network;
            public NetworkDeleteDbCmdText(string id)
            {
                if (string.IsNullOrWhiteSpace(id))
                {
                    throw new ArgumentNullException("id");
                }
                _id = id;

                InitSP();
            }

            public override string BuildCmdText()
            {
                return "dbo.LtgNetworkDelete_pr";
            }

            public override ArrayList BuildParameters()
            {
                AddParam("@Id", _id, SqlDbType.NChar, 10);
                return ParamList;
            }
        }
    }
}