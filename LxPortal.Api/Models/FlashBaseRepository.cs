﻿using System;
using System.Collections.Generic;

using LxPortalLib.Models;

namespace LxPortal.Api.Models
{
    public abstract class FlashBaseRepository
    {
        protected const int DefaultCapacity = 5000;
        protected static readonly int DataMartAgeHours = Config.DataMartAgeHours;
        protected static readonly bool UseDataMartArchive = Config.UseDataMartArchive;
        protected static readonly long DataMartArchiveEndDateKey = Config.DataMartArchiveEndDateKey;
        protected static readonly long DataMartStartDateKey = Config.DataMartStartDateKey;

        public List<FlashRecord> GetByBoundingBox(DateTime start, DateTime end, StrokeType type, decimal ullat, decimal ullon, decimal lrlat, decimal lrlon)
        {
            var flashes = GetDmByBoundingBox(start, end, type, ullat, ullon, lrlat, lrlon, "Lightning");

            return flashes;
        }

        protected abstract List<FlashRecord> GetDmByBoundingBox(DateTime start, DateTime end, StrokeType type, decimal ullat, decimal ullon, decimal lrlat, decimal lrlon, string connectionName);

        public List<FlashRecord> GetByRadius(DateTime start, DateTime end, StrokeType type, decimal lat, decimal lon, int radius)
        {
            var flashes = GetDmByRadius(start, end, type, lat, lon, radius, "Lightning");

            return flashes;
        }

        protected abstract List<FlashRecord> GetDmByRadius(DateTime start, DateTime end, StrokeType type, decimal lat, decimal lon, int radius, string connectionName);
       
        private List<FlashRecord> MassageDataSet (DateTime start, DateTime end, List<FlashRecord> flashes)
        {
            List<FlashRecord> dataSet = new List<FlashRecord>();
            if (flashes != null && flashes.Count > 0)
            {
                foreach(FlashRecord flash in flashes)
                {
                    if (flash.TimeUtc >= start && flash.TimeUtc <= end)
                    {
                        dataSet.Add(flash);
                    }
                }
            }
            return dataSet;
        }
    }
}
