﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Aws.Core.Data;
using Aws.Core.Utilities;
using LxCommon;
using System.Configuration;
using LxCommon.Monitoring;
using LxPortal.Api.Models.Database;
using LxPortalLib.Models;


namespace LxPortal.Api.Models
{
    public class StormRepositoryDb : IStormRepository 
    {
        public List<StormRecord> GetAll(DateTime start, DateTime end, decimal ullat, decimal ullon, decimal lrlat, decimal lrlon)
        {
            SqlConnection conn = null;
            SqlDataReader dr = null;
            DbCommand dbCmd = null;
            List<StormRecord> list = new List<StormRecord>(); 

            try
            {
                conn = DbConnectionFactory.Build(ConfigurationManager.ConnectionStrings["StormPager"].ConnectionString); 
                StormGetBoxDbCmdText stormListDbCmdText = new StormGetBoxDbCmdText(start, end, ullat, ullon, lrlat, lrlon);
                dbCmd = new DbCommand(conn, stormListDbCmdText);
                dbCmd.Command.CommandTimeout = Config.DbCommandTimeoutMilliseconds;
                dr = dbCmd.ExecuteReader(); 
 
                while(dr.Read())
                {
                    StormRecord stormdata = stormListDbCmdText.ParseRow(dr) as StormRecord; 
                        list.Add(stormdata);
                }

            }
            catch(Exception ex)
            {
                //modify event log and probably add to TaskMonitor to follow api structure
                EventManager.LogError(TaskMonitor.ApiStormGet, string.Format("test storm report API Message: {0}", ex.Message)); 
            }

            finally
            {
                if (null != dbCmd) dbCmd.Close();
                if (null != dr) dr.Close();
                if (null != conn) conn.Close();
            }

            return list; 
        }

    }
}