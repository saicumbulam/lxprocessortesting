﻿using System.Collections.Generic;

using LxPortalLib.Models;

namespace LxPortal.Api.Models
{
    public interface IStationRepository
    {
        int Update(Station updateStation);

        Station Get(string id);

        IEnumerable<Station> GetAll();

        Station Add(Station station);

        bool Remove(string id);
        
        void AddStationToNetwork(string stationId, string networkId);

        void RemoveStationFromNetwork(string stationId, string networkId);

        IEnumerable<StationV2> GetAllStationsInNetwork(string networkId);

        IEnumerable<StationV2> GetAllV2();
        StationV2 GetV2(string id);
    }
}