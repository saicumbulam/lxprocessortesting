﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.IO;

using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;

using En.Ods.Lib.Common.Dynamo;
using En.Ods.Lib.Common.Interface;
using En.Ods.Lib.Common.Serialization;

using Aws.Core.Data;
using Aws.Core.Utilities;

using LxCommon;
using LxCommon.Models;
using LxCommon.Monitoring;
using LxPortalLib.Models;

using LxPortal.Api.Helper;
using LxPortal.Api;
using LxPortal.Api.Models.Database;

using Newtonsoft.Json;

namespace LxPortal.Api.Models
{
    public class LxArchiverRepositoryDb : ILxArchiverRepository
    {
        private CloudReader _reader;
        private List<LxArchiverRecord> _records;

        public LxArchiverRepositoryDb()
        {
            _reader = new CloudReader("LxHistoricalWriter");
            _records = new List<LxArchiverRecord>();
        }

        //returns a list of LxArchiverRecords that are within a time range and a bounding box
        public List<LxArchiverRecord> Get(DateTime start, DateTime end, decimal ullat, decimal ullon, decimal lrlat, decimal lrlon)
        {
            HashSet<string> AllQuadkeys = GetQuadkeys(ullat, ullon, lrlat, lrlon);

            foreach (string quadkey in AllQuadkeys)
            {
              
                List<string> filePaths = GetListFiles(start, end, quadkey);

                foreach (string path in filePaths)
                {
                    ICloudObject cloudObj = _reader.GetCloudObject(path);
                    GetCloudObjData(cloudObj.Stream, start, end, (double)ullat, (double)ullon, (double)lrlat, (double)lrlon);
                }
            }

            return _records;
        }

        //gets all quadkeys within or partially within bounding box
        private HashSet<string> GetQuadkeys(decimal ullat, decimal ullon, decimal lrlat, decimal lrlon)
        {
            HashSet<string> quadkeys = new HashSet<string>();


            int UpperPixelX = 0;
            int UpperPixelY = 0;
            TileSystem.LatLongToPixelXY((double)ullat, (double)ullon, 5, out UpperPixelX, out UpperPixelY);

            int LowerPixelX = 0;
            int LowerPixelY = 0;
            TileSystem.LatLongToPixelXY((double)lrlat, (double)lrlon, 5, out LowerPixelX, out LowerPixelY);

            int UpperTileX = 0;
            int UpperTileY = 0;
            TileSystem.PixelXYToTileXY(UpperPixelX, UpperPixelY, out UpperTileX, out UpperTileY);

            int LowerTileX = 0;
            int LowerTileY = 0;
            TileSystem.PixelXYToTileXY(LowerPixelX, LowerPixelY, out LowerTileX, out LowerTileY);

            //get all the possible quadkeys
            for (int x = UpperTileX; x <= LowerTileX; x++)
            {
                for (int y = UpperTileY; y <= LowerTileY; y++)
                {
                    quadkeys.Add(TileSystem.TileXYToQuadKey(x, y, 5));
                }
            }

            return quadkeys;
        }

        //get all S3 file paths that may contain relevant data --- calls stored procedure
        private List<string> GetListFiles(DateTime start, DateTime end, string quadkey)
        {
            List<string> s3Paths = new List<string>();

            SqlConnection conn = null;
            SqlCommand command = null;
            SqlDataReader dr = null;

            IDataRecord data = null;

            string ConnectionString = ConfigurationManager.ConnectionStrings["LxArchiveGetFile"].ConnectionString;

            Boolean isConnected = false;

            while (isConnected == false)
            {

                try
                {
                    conn = new SqlConnection(ConnectionString);
                    conn.Open();

                    string storeProc = "GetListFile_pr";
                    command = new SqlCommand(storeProc, conn);
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("@Spatial_hash", SqlDbType.VarChar, 50).Value = quadkey;
                    command.Parameters.Add("@ts_from", SqlDbType.DateTime).Value = start;
                    command.Parameters.Add("@ts_to", SqlDbType.DateTime).Value = end;

                    int storeprodVal = command.ExecuteNonQuery();
                    dr = command.ExecuteReader();

                    //get paths returned in db table from store procedure
                    while (dr.Read())
                    {
                        data = (IDataRecord)dr;
                        string path = GetFilePath(data);

                        s3Paths.Add(path);
                    }

                    isConnected = true;

                }
                catch (SqlException)
                {

                    Console.WriteLine(DateTime.Now + " Connection Failed. Trying Again in 5 minutes...");
                    System.Threading.Thread.Sleep(300000);  //Wait for 5 minutes
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }

            return s3Paths;
        }

        //get the file path on S3
        private string GetFilePath(IDataRecord data)
        {
            string file = (string)data["s3path"];
            return file;
        }

        //add flash data that is in bounding box and is in timespan to LxArchiverRecords
        private void GetCloudObjData(Stream fromCloud, DateTime start, DateTime end, double ullat, double ullon, double lrlat, double lrlon)
        {

            Boolean InDateRange = true;

            try
            {

                using (StreamReader sr = new StreamReader(fromCloud))
                {
                    string line;

                    while ((line = sr.ReadLine()) != null && InDateRange)
                    {
                        Flash flash = DeSerializeFlash(line);
                        DateTime current = Convert.ToDateTime(flash.TimeStamp);

                        if (current >= start && current <= end)
                        {
                            //must be inside bounding box
                            if ((flash.Latitude >= lrlat && flash.Latitude <= ullat) && (flash.Longitude <= lrlon && flash.Longitude >= ullon))
                            {
                                _records.Add(CreateLxArchiverRecord(flash));
                            }
                        }
                        else if(current > end)       //stops reading file when the current flash's timestamp > timespans endtime
                        {
                            InDateRange = false;
                        }

                    }
                }

                Console.WriteLine(DateTime.Now + " Retrieval Complete.");

            }
            catch (IOException)
            {
                Console.WriteLine(DateTime.Now + " IOException Occured.");
            }

        }

        //get Flash - string "data" from .json file
        private Flash DeSerializeFlash(string data)
        {
            return JsonConvert.DeserializeObject<Flash>(data);
        }

        //create LxArchiverRecord --- contains list of FlashPortionRecords
        private LxArchiverRecord CreateLxArchiverRecord(Flash flash)
        {
            LxArchiverRecord record = new LxArchiverRecord();

            record.Amplitude = flash.Amplitude;
            record.CgMultiplicity = flash.CgMultiplicity;
            record.Confidence = flash.Confidence;
            record.DurationSeconds = flash.DurationSeconds;
            record.EndTime = flash.EndTime;
            record.Height = flash.Height;
            record.IcMultiplicity = flash.IcMultiplicity;
            record.Latitude = (decimal)flash.Latitude;
            record.Longitude = (decimal)flash.Longitude;
            record.MaxLatitude = flash.MaxLatitude;
            record.MinLatitude = flash.MinLatitude;
            record.MaxLongitude = flash.MaxLongitude;
            record.MinLongitude = flash.MinLongitude;
            record.NumberSensors = flash.NumberSensors;
            record.Source = flash.Source;
            record.StartTime = flash.StartTime;
            record.TimeUtc = flash.TimeStamp;
            record.Type = flash.Type;
            record.Version = flash.Version;

            record.Portions = new List<FlashPortionRecord>();

            foreach (Portion portion in flash.Portions)
            {
                FlashPortionRecord portionRecord = CreatePortionRecord(portion);
                record.Portions.Add(portionRecord);
            }

            return record;
        }

        //create FlashPortionRecord
        private FlashPortionRecord CreatePortionRecord(Portion portion)
        {
            FlashPortionRecord record = new FlashPortionRecord();

            record.Amplitude = portion.Amplitude;
            record.Confidence = portion.Confidence;
            record.FlashGUID = portion.FlashGUID;
            record.Height = portion.Height;
            record.Latitude = (decimal)portion.Latitude;
            record.Longitude = (decimal)portion.Longitude;
            record.ErrorEllipse = portion.ErrorEllipse;
            record.FlashID = portion.FlashId;
            record.Offsets = portion.Offsets;
            record.StationOffsets = portion.StationOffsets;
            record.TimeUtc = portion.TimeStamp;
            record.Type = portion.Type;
            record.Version = portion.Version;
            record.StrokeSolution = portion.Solution;

            return record;
        }
    }
}