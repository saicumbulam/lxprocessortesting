﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;

using Aws.Core.Data;
using Aws.Core.Utilities;

using LxCommon;
using LxCommon.Monitoring;
using LxPortalLib.Common;
using LxPortalLib.Models;

using LxPortal.Api.Models.Database;

namespace LxPortal.Api.Models
{
    public class SensorDiagnosticRepositoryDb : ISensorDiagnosticRepository
    {
        public SensorDiagnostics Get(string id, DateTime? start, DateTime? end)
        {
            SqlConnection conn = null;
            SqlDataReader dr = null;
            DbCommand dbCmd = null;
            SensorDiagnostics diags = null;

            try
            {
                if (start.HasValue && end.HasValue && start.Value.CompareTo(DateTime.MinValue) == 0 && end.Value.CompareTo(DateTime.MaxValue) == 0)
                {
                    start = null;
                    end = null;
                }
                conn = DbConnectionFactory.Build(ConfigurationManager.ConnectionStrings["Lightning"].ConnectionString);
                SensorDiagnosticGetDbCmdText dbCmdText = new SensorDiagnosticGetDbCmdText(id, start, end);
                dbCmd = new DbCommand(conn, dbCmdText);
                dbCmd.Command.CommandTimeout = Config.DbCommandTimeoutMilliseconds;
                dr = dbCmd.ExecuteReader();
                diags = new SensorDiagnostics{Id = id, DiagnosticList = new List<SensorDiagnosticData>()};
                while (dr.Read())
                {
                    SensorDiagnosticData diag = dbCmdText.ParseRow(dr) as SensorDiagnosticData;
                    if (diag != null && diag.TimestampUtc != null)
                        diags.DiagnosticList.Add(diag) ;
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.ApiCurrentSensorDiagnosticGet, string.Format("Unable to get current sensor diagnostics. Message: {0}", ex.Message));
            }
            finally
            {
                if (null != dbCmd) dbCmd.Close();
                if (null != dr) dr.Close();
                if (null != conn) conn.Close();
            }

            return diags;
        }
    }
}
