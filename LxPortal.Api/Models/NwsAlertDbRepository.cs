﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using Aws.Core.Data;
using Aws.Core.Utilities;
using LxCommon;
using LxCommon.Monitoring;
using LxPortal.Api.Models.Database;
using LxPortalLib.Models;

namespace LxPortal.Api.Models
{
    public class NwsAlertDbRepository
    {
        public IEnumerable<NwsAlert> Get(DateTime start, DateTime end)
        {
            SqlConnection conn = null;
            SqlDataReader dr = null;
            DbCommand dbCmd = null;
            List<NwsAlert> alerts = null;

            try
            {
                conn = DbConnectionFactory.Build(ConfigurationManager.ConnectionStrings["StormPager"].ConnectionString);
                NwsAlertGetDbCmdText dbCmdText = new NwsAlertGetDbCmdText(start, end);
                dbCmd = new DbCommand(conn, dbCmdText);
                dbCmd.Command.CommandTimeout = Config.DbCommandTimeoutMilliseconds;
                dr = dbCmd.ExecuteReader();
                alerts = new List<NwsAlert>();
                NwsAlert alert = null;
                while (dr.Read())
                {
                    alert = dbCmdText.ParseNwsAlertRow(dr);
                    if (null != alert)
                        alerts.Add(alert);
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.ApiNwsAlertGet, string.Format("Unable to get nws alerts . Message: {0}", ex.Message));
            }
            finally
            {
                if (null != dbCmd) dbCmd.Close();
                if (null != dr) dr.Close();
                if (null != conn) conn.Close();
            }

            return alerts;
        }
    }
}