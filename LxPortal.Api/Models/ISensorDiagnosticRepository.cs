﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using LxPortalLib.Models;

namespace LxPortal.Api.Models
{
    public interface ISensorDiagnosticRepository
    {
        SensorDiagnostics Get(string id, DateTime? start, DateTime? end);
    }
}
