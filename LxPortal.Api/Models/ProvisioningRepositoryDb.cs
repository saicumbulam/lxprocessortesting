﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using LxPortalLib.Common;
using LxPortalLib.Models;

using Aws.Core.Data;
using Aws.Core.Utilities;

using LxPortal.Api.Models.Database;

namespace LxPortal.Api.Models
{
    public class ProvisioningRepositoryDb
    {

        public PartnerRecord Add(PartnerRecord newPartnerRecord)
        {

            SqlConnection conn = null;
            SqlDataReader dr = null;
            DbCommand dbCmd = null;
            PartnerRecord partner = null;

            try
            {
                conn = DbConnectionFactory.Build(ConfigurationManager.ConnectionStrings["Lightning"].ConnectionString);
                PartnerCreateDbCmdText dbCmdText = new PartnerCreateDbCmdText(newPartnerRecord);
                dbCmd = new DbCommand(conn, dbCmdText);
                dbCmd.Command.CommandTimeout = Config.DbCommandTimeoutMilliseconds;

                dr = dbCmd.ExecuteReader(); //only when query returns single return value

                if (dr.Read())
                    partner = (dbCmdText.ParseRowPartner(dr)) as PartnerRecord;

            }
            catch (Exception ex)
            {
                EventManager.LogError(string.Format("Unable to add partner. Message: {0}", ex));
            }
            finally
            {
                if (null != dr) dr.Close();
            }

            try
            {
                if (partner != null)
                {

                    // IC Height
                    PartnerResponseCreateDbCmdText PartnerResponse_dbCmdText = new PartnerResponseCreateDbCmdText(partner, ResponseDataElement.ICHeight);
                    dbCmd = new DbCommand(conn, PartnerResponse_dbCmdText);
                    dbCmd.Command.CommandTimeout = Config.DbCommandTimeoutMilliseconds;
                    dbCmd.ExecuteNonQuery();

                    // Number of Sensors
                    PartnerResponse_dbCmdText = new PartnerResponseCreateDbCmdText(partner, ResponseDataElement.NumberOfSensor);
                    dbCmd = new DbCommand(conn, PartnerResponse_dbCmdText);
                    dbCmd.Command.CommandTimeout = Config.DbCommandTimeoutMilliseconds;
                    dbCmd.ExecuteNonQuery();

                    // Multiplicity
                    PartnerResponse_dbCmdText = new PartnerResponseCreateDbCmdText(partner, ResponseDataElement.Multiplicity);
                    dbCmd = new DbCommand(conn, PartnerResponse_dbCmdText);
                    dbCmd.Command.CommandTimeout = Config.DbCommandTimeoutMilliseconds;
                    dbCmd.ExecuteNonQuery();

                    // FeedTypes 
                    PartnerFeedCreateDbCmdText PartnerFeed_dbCmdText;
                    if (newPartnerRecord.FeedType != null && newPartnerRecord.FeedType.Count > 0)
                    {
                        foreach (int feed in newPartnerRecord.FeedType)
                        {
                            PartnerFeed_dbCmdText = new PartnerFeedCreateDbCmdText(partner, feed);
                            dbCmd = new DbCommand(conn, PartnerFeed_dbCmdText);
                            dbCmd.Command.CommandTimeout = Config.DbCommandTimeoutMilliseconds;
                            dbCmd.ExecuteNonQuery();
                        }
                    }
                    

                    // IP's
                    PartnerIpCreateDbCmdText PartnerIp_dbCmdText;
                    if (newPartnerRecord.IpAddress != null && newPartnerRecord.IpAddress.Count > 0)
                    {
                        foreach (string ip in newPartnerRecord.IpAddress)
                        {
                            PartnerIp_dbCmdText = new PartnerIpCreateDbCmdText(partner, ip);
                            dbCmd = new DbCommand(conn, PartnerIp_dbCmdText);
                            dbCmd.Command.CommandTimeout = Config.DbCommandTimeoutMilliseconds;
                            dbCmd.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(string.Format("Unable to add partner metadata. Message: {0}", ex));
            }
            finally
            {
                if (null != dbCmd) dbCmd.Close();
                if (null != conn) conn.Close();
            }

            return partner;
        }

        public PartnerRecord Update(PartnerRecord updatedPartnerRecord)
        {
            SqlConnection conn = null;
            SqlDataReader dr = null;
            DbCommand dbCmd = null;
            PartnerRecord partner = null;

            try
            {
                conn = DbConnectionFactory.Build(ConfigurationManager.ConnectionStrings["Lightning"].ConnectionString);
                PartnerUpdateDbCmdText dbCmdText = new PartnerUpdateDbCmdText(updatedPartnerRecord);
                dbCmd = new DbCommand(conn, dbCmdText);
                dbCmd.Command.CommandTimeout = Config.DbCommandTimeoutMilliseconds;

                dr = dbCmd.ExecuteReader(); //only when query returns single return value

                if (dr.Read())
                    partner = (dbCmdText.ParseRowPartner(dr)) as PartnerRecord;

            }
            catch (Exception ex)
            {
                EventManager.LogError(string.Format("Unable to update partner {0}. Message: {1}", updatedPartnerRecord.PartnerId, ex));
            }
            finally
            {
                if (null != dr) dr.Close();
            }

            try
            {
                //if return from db is not null update was successfull 
                if (partner != null)
                {
                    //if the input is not null or empty then update else leave alone
                    if (updatedPartnerRecord.IpAddress != null)
                    {
                        // Delete previous ip address(es)
                        PartnerIpRemoveDbCmdText PartnerRemoveIp_dbCmdText = new PartnerIpRemoveDbCmdText(partner.PartnerId);
                        dbCmd = new DbCommand(conn, PartnerRemoveIp_dbCmdText);
                        dbCmd.Command.CommandTimeout = Config.DbCommandTimeoutMilliseconds;
                        dbCmd.ExecuteNonQuery();

                        // Update ip address(es)
                        PartnerIpCreateDbCmdText PartnerIp_dbCmdText;
                        foreach (string ip in updatedPartnerRecord.IpAddress)
                        {
                            PartnerIp_dbCmdText = new PartnerIpCreateDbCmdText(partner, ip);
                            dbCmd = new DbCommand(conn, PartnerIp_dbCmdText);
                            dbCmd.Command.CommandTimeout = Config.DbCommandTimeoutMilliseconds;
                            dbCmd.ExecuteNonQuery();
                        }
                    }

                    // Do not update feed type(s)
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(string.Format("Unable to update partner {0} metadata. Message: {1}", updatedPartnerRecord.PartnerId, ex));
            }
            finally
            {
                if (null != dbCmd) dbCmd.Close();
                if (null != conn) conn.Close();
            }

            return partner;
        }

        public PartnerRecord GetPartner(string partnerId)
        {
            PartnerRecord partner = null;
            SqlConnection conn = null;
            SqlDataReader dr = null;
            DbCommand dbCmd = null;

            try
            {
                if (!string.IsNullOrEmpty(partnerId))
                {
                    conn = DbConnectionFactory.Build(ConfigurationManager.ConnectionStrings["Lightning"].ConnectionString);
                    PartnerGetDbCmdText dbCmdText = new PartnerGetDbCmdText(partnerId);
                    dbCmd = new DbCommand(conn, dbCmdText);
                    dbCmd.Command.CommandTimeout = Config.DbCommandTimeoutMilliseconds;

                    dr = dbCmd.ExecuteReader(); //only when query returns single return value

                    if (dr.Read())
                    {
                        partner = new PartnerRecord();

                        string id = !string.IsNullOrEmpty(dr["PartnerId"].ToString()) ? dr["PartnerId"].ToString() : null;

                        if (!string.IsNullOrEmpty(id))
                        {
                            partner.IpAddress = GetIpAddresses(id);
                            partner.FeedType = GetFeedTypes(id);
                        }
                        CreatePartner(dr, partner);
                    }

                    dr.Close();
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(string.Format("Unable to get partner {0} data. Message: {1}", partnerId, ex));
            }

            return partner;
        }

        public List<PartnerRecord> GetAllPartners(int feedType)
        {
            List<PartnerRecord> partners = new List<PartnerRecord>();

            try
            {
                SqlConnection conn = null;
                SqlDataReader dr = null;
                DbCommand dbCmd = null;

                conn = DbConnectionFactory.Build(ConfigurationManager.ConnectionStrings["Lightning"].ConnectionString);
                PartnerGetDbCmdText dbCmdText = new PartnerGetDbCmdText(null);
                dbCmd = new DbCommand(conn, dbCmdText);
                dbCmd.Command.CommandTimeout = Config.DbCommandTimeoutMilliseconds;

                EventManager.LogInfo("before dr.ExecuteReader()");
                dr = dbCmd.ExecuteReader(); //only when query returns single return value
                EventManager.LogInfo("after dr.ExecuteReader()");
                while (dr.Read())
                {
                    EventManager.LogInfo("dr.read()");
                    PartnerRecord partner = new PartnerRecord();

                    string id = !string.IsNullOrEmpty(dr["PartnerId"].ToString()) ? dr["PartnerId"].ToString() : null;
                    EventManager.LogInfo("id = "+ id);

                    if (!string.IsNullOrEmpty(id))
                    {
                        EventManager.LogInfo("id not null or empty " + id);
                        partner.IpAddress = GetIpAddresses(id);
                        EventManager.LogInfo("partner.IpAddress " + partner.IpAddress.Count);
                        partner.FeedType = GetFeedTypes(id);
                        EventManager.LogInfo("partner.FeedType " + partner.FeedType.Count);
                        if (feedType != -1 && !partner.FeedType.Contains(feedType))
                        {
                            EventManager.LogInfo("feed type is " + feedType + "partner.FeedType.Contains(feedType) = " + partner.FeedType.Contains(feedType));
                            continue;
                        }
                    }
                    CreatePartner(dr, partner);

                    partners.Add(partner);
                }

                dr.Close();
                EventManager.LogInfo("dr. close");

            }
            catch (Exception ex)
            {
                EventManager.LogError(string.Format("Unable to get all partner data. Message: {0}", ex));
            }

            return partners;
        }

        public List<string> GetIpAddresses(string partnerId)
        {
            List<string> ipAddresses = new List<string>();

            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Lightning"].ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();
                    SqlParameter param = new SqlParameter();
                    cmd.Connection = conn; 


                    if (!string.IsNullOrEmpty(partnerId))
                    {
                        cmd.CommandText = "bizops.GetPartnerIpWhitelist_pr";
                        cmd.CommandType = CommandType.StoredProcedure;
                        param.ParameterName = "@partnerId"; 
                        param.SqlDbType = SqlDbType.NVarChar;
                        param.Direction = ParameterDirection.Input;
                        param.Value = partnerId;
                        cmd.Parameters.Add(param); 

                    }
                    conn.Open();
                    var reader = cmd.ExecuteReader(); 

                    while(reader.Read())
                    {
                        ipAddresses.Add(reader["IpAddress"].ToString());
                    }

                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(string.Format("Unable to get ip addresses for partner {0}. Message: {1}", partnerId, ex));
            }

            return ipAddresses; 
        }

        public List<int> GetFeedTypes(string partnerId)
        {
            List<int> feedTypes = new List<int>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Lightning"].ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();
                    SqlParameter param = new SqlParameter();
                    cmd.Connection = conn;

                    if (!string.IsNullOrEmpty(partnerId))
                    {
                        cmd.CommandText = "bizops.GetPartnerFeedType_pr";
                        cmd.CommandType = CommandType.StoredProcedure;
                        param.ParameterName = "@partnerId"; 
                        param.SqlDbType = SqlDbType.NVarChar;
                        param.Direction = ParameterDirection.Input;
                        param.Value = partnerId;
                        cmd.Parameters.Add(param);
                    }
                    conn.Open();
                    var reader = cmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            feedTypes.Add((int)reader["FeedTypeId"]);
                        }
                    }
                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(string.Format("Unable to get feed types for partner {0}. Message: {1}", partnerId, ex));
            }

            return feedTypes; 
        }

        protected void CreatePartner(SqlDataReader reader, PartnerRecord partner)
        {
            
            if (!String.IsNullOrEmpty(reader["PartnerId"].ToString()))
            {
                partner.PartnerId = reader["PartnerId"].ToString();
            }
            if (!String.IsNullOrEmpty(reader["PartnerName"].ToString()))
            {
                partner.PartnerName = reader["PartnerName"].ToString();
            }
            if (!String.IsNullOrEmpty(reader["IsActive"].ToString()))
            {
                partner.IsActive = Convert.ToBoolean(reader["IsActive"].ToString());
            }
            if (!String.IsNullOrEmpty(reader["IsCloudToGroundEnabled"].ToString()))
            {
                partner.IsCloudToGroundEnabled = Convert.ToBoolean(reader["IsCloudToGroundEnabled"].ToString());
            }
            if (!String.IsNullOrEmpty(reader["IsInCloudEnabled"].ToString()))
            {
                partner.IsInCloudEnabled = Convert.ToBoolean(reader["IsInCloudEnabled"].ToString());
            }
            if (!String.IsNullOrEmpty(reader["ResponseFormatTypeId"].ToString()))
            {
                partner.ResponseFormatTypeId = Convert.ToByte(reader["ResponseFormatTypeId"].ToString());
            }
            if (!String.IsNullOrEmpty(reader["ResponsePackageTypeId"].ToString()))
            {
                partner.ResponsePackageTypeId = Convert.ToByte(reader["ResponsePackageTypeId"].ToString());
            }
            if (!String.IsNullOrEmpty(reader["BoundingArea"].ToString()))
            {
                partner.BoundingArea = reader["BoundingArea"].ToString();
            }
            if (!String.IsNullOrEmpty(reader["LastModifiedDateUtc"].ToString()))
            {
                partner.LastModifiedDateUtc = Convert.ToDateTime(reader["LastModifiedDateUtc"].ToString());
            }
            if (!String.IsNullOrEmpty(reader["LastModifiedBy"].ToString()))
            {
                partner.LastModifiedBy = reader["LastModifiedBy"].ToString();
            }
            if (!String.IsNullOrEmpty(reader["NetworkTypeId"].ToString()))
            {
                partner.NetworkTypeId = int.Parse(reader["NetworkTypeId"].ToString());
            }
            if (!String.IsNullOrEmpty(reader["Metadata"].ToString()))
            {
                partner.Metadata = Convert.ToBoolean(reader["Metadata"].ToString());
            }
        }

        protected void CreateEvent(SqlDataReader reader, EventLogRecord eventLog)
        {
            if (!String.IsNullOrEmpty(reader["EventText"].ToString()))
            {
                eventLog.EventText = reader["EventText"].ToString();
            }
            if (!String.IsNullOrEmpty(reader["RemoteIpAddress"].ToString()))
            {
                eventLog.RemoteIpAddress = reader["RemoteIpAddress"].ToString();
            }
            if (!String.IsNullOrEmpty(reader["LastModifiedDateUtc"].ToString()))
            {
                eventLog.LastModifiedDateUtc = Convert.ToDateTime(reader["LastModifiedDateUtc"].ToString());
            }
        }

        public List<EventLogRecord> GenerateEventLog(string id, int epochStart, int epochEnd)
        {
            List<EventLogRecord> EventLog = new List<EventLogRecord>();

            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Lightning"].ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();
                    SqlParameter param;
                    cmd.Connection = conn;
                    conn.Open();

                    cmd.CommandText = "bizops.GetPartnerEventLog_pr";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@partnerId", SqlDbType = SqlDbType.NVarChar, Direction = ParameterDirection.Input, Value = id });
                    if (epochStart != -1)
                    {
                        cmd.Parameters.Add(new SqlParameter { ParameterName = "@startUtc", SqlDbType = SqlDbType.DateTime, Direction = ParameterDirection.Input, Value = Utilities.ConvertFromEpoch(epochStart) });
                    }
                    if (epochEnd != -1)
                    {
                        cmd.Parameters.Add(new SqlParameter { ParameterName = "@endUtc", SqlDbType = SqlDbType.DateTime, Direction = ParameterDirection.Input, Value = Utilities.ConvertFromEpoch(epochEnd) });
                    }
                        
                    var reader = cmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            EventLogRecord addEvent = new EventLogRecord();
                            CreateEvent(reader, addEvent);

                            EventLog.Add(addEvent);
                        }
                    }
                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(string.Format("Unable to get event log for partner {0}. epochStart = {1}, epochEnd = {2}, Message: {3}", id, epochStart, epochEnd, ex));
            }

            return EventLog;
        }

        public bool DeletePartner(string partnerId)
        {
            bool result = false;
            SqlConnection conn = null;
            //SqlDataReader dr = null;
            DbCommand dbCmd = null;

            try
            {
                if (!string.IsNullOrEmpty(partnerId))
                {
                    conn = DbConnectionFactory.Build(ConfigurationManager.ConnectionStrings["Lightning"].ConnectionString);
                    PartnerDeleteDbCmdText dbCmdText = new PartnerDeleteDbCmdText(partnerId);
                    dbCmd = new DbCommand(conn, dbCmdText);
                    dbCmd.Command.CommandTimeout = Config.DbCommandTimeoutMilliseconds;

                    dbCmd.ExecuteNonQuery();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(string.Format("Unable to delete partner {0}. Message: {1}", partnerId, ex));
            }

            return result;
        }
    }
}