﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

using LxPortalLib.Models;

namespace LxPortal.Api.Models
{
    public class StationRepositorySample : IStationRepository
    {
        private List<Station> _stations = new List<Station>
        {
            new Station { 
                Id = "IAF001", 
                Name = "Station Name #1", 
                SerialNumber = "5375", 
                InstallDate = new DateTime(2012, 1, 1), 
                IndoorFirmware = "1.1.6.19", 
                Ip = "204.156.11.104", 
                FirstCallHome = new DateTime(2012, 1, 1),
                LastCallHome = new DateTime(2012, 7, 31),
                LastCalibration = new DateTime(2012, 7, 30),
                StreetAddress = "Dattaram Lad Marg",
                City = "Mumbai",
                Region = null,
                Province = "Maharashtra",
                Country = "India",
                PostalCode = "400012",
                PocName = "Joe Smith",
                PocPhone = "1-555-555-1434",
                PocEmail = "jsmith@gmail.com",
                SendLogRateMinutes = 60,
                SendGpsRateMinutes = 60,
                SendAntennaGainRateMinutes = 15,
                AntennaMode = 9,
                AntennaAttenuation = 40,
                PrimaryDeliveryIp = "8.8.8.1",
                SecondaryDeliveryIp = "8.8.8.2",
                OutdoorFirmware = "3.0.1.50",
                Status = 0
            },
            new Station { 
                Id = "IAF002", 
                Name = "Station Name #2", 
                SerialNumber = "5377", 
                InstallDate = new DateTime(2012, 1, 1), 
                IndoorFirmware = "1.1.6.19", 
                Ip = "205.157.12.105", 
                FirstCallHome = new DateTime(2012, 1, 2),
                LastCallHome = new DateTime(2012, 7, 30),
                LastCalibration = new DateTime(2012, 7, 29),
                StreetAddress = "Bazar Guard Cross Road",
                City = "Hyderabad",
                Region = null,
                Province = "Andhra Pradesh",
                Country = "India",
                PostalCode = "500001",
                PocName = "John Doe",
                PocPhone = "1-555-555-1435",
                PocEmail = "john.doe@gmail.com",
                SendLogRateMinutes = 60,
                SendGpsRateMinutes = 60,
                SendAntennaGainRateMinutes = 15,
                AntennaMode = 9,
                AntennaAttenuation = 40,
                PrimaryDeliveryIp = "8.8.8.1",
                SecondaryDeliveryIp = "8.8.8.2",
                OutdoorFirmware = "1.0.6.8",
                Status = 1
            },
            new Station { 
                Id = "IAF003", 
                Name = "Station Name #3", 
                SerialNumber = "5378",
                InstallDate = new DateTime(2012, 1, 1), 
                IndoorFirmware = "1.1.5.17", 
                Ip = "206.158.13.106", 
                FirstCallHome = new DateTime(2012, 1, 2),
                LastCallHome = new DateTime(2012, 7, 30),
                LastCalibration = new DateTime(2012, 7, 29),
                StreetAddress = "Feroze Gandhi Marg",
                City = "Delhi",
                Region = null,
                Province = "Delhi",
                Country = "India",
                PostalCode = "110024",
                PocName = "Jane Doe",
                PocPhone = "1-555-555-1436",
                PocEmail = "jane.doe@gmail.com",
                SendLogRateMinutes = 60,
                SendGpsRateMinutes = 60,
                SendAntennaGainRateMinutes = 15,
                AntennaMode = 9,
                AntennaAttenuation = 40,
                PrimaryDeliveryIp = "8.8.8.1",
                SecondaryDeliveryIp = "8.8.8.2",
                OutdoorFirmware = "1.0.6.8",
                Status = 2
            },
            new Station { 
                Id = "IAF004", 
                Name = "Station Name #4", 
                SerialNumber = "5379",
                InstallDate = new DateTime(2012, 1, 4), 
                IndoorFirmware = "1.1.5.17", 
                Ip = "206.158.13.106", 
                FirstCallHome = new DateTime(2012, 1, 2),
                LastCallHome = new DateTime(2012, 8, 6),
                LastCalibration = new DateTime(2012, 7, 29),
                StreetAddress = "Main Street",
                City = "Puducherry",
                Region = null,
                Province = "Puducherry ",
                Country = "India",
                PostalCode = "605004",
                PocName = "Jesse Tomlinson",
                PocPhone = "1-555-555-1437",
                PocEmail = " jtomlinson@earthnetworks.com",
                SendLogRateMinutes = 60,
                SendGpsRateMinutes = 60,
                SendAntennaGainRateMinutes = 15,
                AntennaMode = 9,
                AntennaAttenuation = 40,
                PrimaryDeliveryIp = "8.8.8.1",
                SecondaryDeliveryIp = "8.8.8.2",
                OutdoorFirmware = "1.0.6.8",
                Status = 3
            },
            new Station { 
                Id = "IAF005", 
                Name = "Station Name #5", 
                SerialNumber = "5380",
                InstallDate = new DateTime(2012, 1, 5), 
                IndoorFirmware = "1.1.5.18", 
                Ip = "206.158.13.106", 
                FirstCallHome = new DateTime(2012, 1, 2),
                LastCallHome = new DateTime(2012, 7, 30),
                LastCalibration = new DateTime(2012, 7, 29),
                StreetAddress = "Balan K Nair Road",
                City = "Kozhikode",
                Region = null,
                Province = "Kerala",
                Country = "India",
                PostalCode = "673005",
                PocName = "Justin Dearing",
                PocPhone = "1-555-555-1438",
                PocEmail = "jdearing@earthnetworks.com",
                SendLogRateMinutes = 60,
                SendGpsRateMinutes = 60,
                SendAntennaGainRateMinutes = 15,
                AntennaMode = 9,
                AntennaAttenuation = 40,
                PrimaryDeliveryIp = "8.8.8.1",
                SecondaryDeliveryIp = "8.8.8.2",
                OutdoorFirmware = "1.0.6.8",
                Status = 4
            },
            new Station { 
                Id = "IAF008", 
                Name = "Station Name #8", 
                SerialNumber = "5480", 
                InstallDate = new DateTime(2012, 1, 1), 
                IndoorFirmware = "1.1.6.19", 
                Ip = "204.156.11.104", 
                FirstCallHome = new DateTime(2012, 1, 1),
                LastCallHome = new DateTime(2012, 7, 31),
                LastCalibration = new DateTime(2012, 7, 30),
                StreetAddress = "Dattaram Lad Marg",
                City = "Mumbai",
                Region = null,
                Province = "Maharashtra",
                Country = "India",
                PostalCode = "400012",
                PocName = "Joe Smith",
                PocPhone = "1-555-555-1434",
                PocEmail = "jsmith@gmail.com",
                SendLogRateMinutes = 60,
                SendGpsRateMinutes = 60,
                SendAntennaGainRateMinutes = 15,
                AntennaMode = 9,
                AntennaAttenuation = 40,
                PrimaryDeliveryIp = "8.8.8.1",
                SecondaryDeliveryIp = "8.8.8.2",
                OutdoorFirmware = "3.0.1.50",
                Status = 0
            },
            new Station { 
                Id = "IAF014", 
                Name = "Station Name #14", 
                SerialNumber = "5481", 
                InstallDate = new DateTime(2012, 1, 1), 
                IndoorFirmware = "1.1.6.19", 
                Ip = "205.157.12.105", 
                FirstCallHome = new DateTime(2012, 1, 2),
                LastCallHome = new DateTime(2012, 7, 30),
                LastCalibration = new DateTime(2012, 7, 29),
                StreetAddress = "Bazar Guard Cross Road",
                City = "Hyderabad",
                Region = null,
                Province = "Andhra Pradesh",
                Country = "India",
                PostalCode = "500001",
                PocName = "John Doe",
                PocPhone = "1-555-555-1435",
                PocEmail = "john.doe@gmail.com",
                SendLogRateMinutes = 60,
                SendGpsRateMinutes = 60,
                SendAntennaGainRateMinutes = 15,
                AntennaMode = 9,
                AntennaAttenuation = 40,
                PrimaryDeliveryIp = "8.8.8.1",
                SecondaryDeliveryIp = "8.8.8.2",
                OutdoorFirmware = "1.0.6.8",
                Status = 1
            },
            new Station { 
                Id = "IAF012", 
                Name = "Station Name #12", 
                SerialNumber = "5390",
                InstallDate = new DateTime(2012, 1, 1), 
                IndoorFirmware = "1.1.5.17", 
                Ip = "206.158.13.106", 
                FirstCallHome = new DateTime(2012, 1, 2),
                LastCallHome = new DateTime(2012, 7, 30),
                LastCalibration = new DateTime(2012, 7, 29),
                StreetAddress = "Feroze Gandhi Marg",
                City = "Delhi",
                Region = null,
                Province = "Delhi",
                Country = "India",
                PostalCode = "110024",
                PocName = "Jane Doe",
                PocPhone = "1-555-555-1436",
                PocEmail = "jane.doe@gmail.com",
                SendLogRateMinutes = 60,
                SendGpsRateMinutes = 60,
                SendAntennaGainRateMinutes = 15,
                AntennaMode = 9,
                AntennaAttenuation = 40,
                PrimaryDeliveryIp = "8.8.8.1",
                SecondaryDeliveryIp = "8.8.8.2",
                OutdoorFirmware = "1.0.6.8",
                Status = 2
            },
            new Station { 
                Id = "IAF010", 
                Name = "Station Name #10", 
                SerialNumber = "5400",
                InstallDate = new DateTime(2012, 1, 4), 
                IndoorFirmware = "1.1.5.17", 
                Ip = "206.158.13.106", 
                FirstCallHome = new DateTime(2012, 1, 2),
                LastCallHome = new DateTime(2012, 8, 6),
                LastCalibration = new DateTime(2012, 7, 29),
                StreetAddress = "Main Street",
                City = "Puducherry",
                Region = null,
                Province = "Puducherry ",
                Country = "India",
                PostalCode = "605004",
                PocName = "Jesse Tomlinson",
                PocPhone = "1-555-555-1437",
                PocEmail = " jtomlinson@earthnetworks.com",
                SendLogRateMinutes = 60,
                SendGpsRateMinutes = 60,
                SendAntennaGainRateMinutes = 15,
                AntennaMode = 9,
                AntennaAttenuation = 40,
                PrimaryDeliveryIp = "8.8.8.1",
                SecondaryDeliveryIp = "8.8.8.2",
                OutdoorFirmware = "1.0.6.8",
                Status = 3
            },
            new Station { 
                Id = "IAF011", 
                Name = "Station Name #11", 
                SerialNumber = "5410",
                InstallDate = new DateTime(2012, 1, 5), 
                IndoorFirmware = "1.1.5.18", 
                Ip = "206.158.13.106", 
                FirstCallHome = new DateTime(2012, 1, 2),
                LastCallHome = new DateTime(2012, 7, 30),
                LastCalibration = new DateTime(2012, 7, 29),
                StreetAddress = "Balan K Nair Road",
                City = "Kozhikode",
                Region = null,
                Province = "Kerala",
                Country = "India",
                PostalCode = "673005",
                PocName = "Justin Dearing",
                PocPhone = "1-555-555-1438",
                PocEmail = "jdearing@earthnetworks.com",
                SendLogRateMinutes = 60,
                SendGpsRateMinutes = 60,
                SendAntennaGainRateMinutes = 15,
                AntennaMode = 9,
                AntennaAttenuation = 40,
                PrimaryDeliveryIp = "8.8.8.1",
                SecondaryDeliveryIp = "8.8.8.2",
                OutdoorFirmware = "1.0.6.8",
                Status = 4
            },
            new Station { 
                Id = "IAF015", 
                Name = "Station Name #15", 
                SerialNumber = "5420", 
                InstallDate = new DateTime(2012, 1, 1), 
                IndoorFirmware = "1.1.6.19", 
                Ip = "204.156.11.104", 
                FirstCallHome = new DateTime(2012, 1, 1),
                LastCallHome = new DateTime(2012, 7, 31),
                LastCalibration = new DateTime(2012, 7, 30),
                StreetAddress = "Dattaram Lad Marg",
                City = "Mumbai",
                Region = null,
                Province = "Maharashtra",
                Country = "India",
                PostalCode = "400012",
                PocName = "Joe Smith",
                PocPhone = "1-555-555-1434",
                PocEmail = "jsmith@gmail.com",
                SendLogRateMinutes = 60,
                SendGpsRateMinutes = 60,
                SendAntennaGainRateMinutes = 15,
                AntennaMode = 9,
                AntennaAttenuation = 40,
                PrimaryDeliveryIp = "8.8.8.1",
                SecondaryDeliveryIp = "8.8.8.2",
                OutdoorFirmware = "3.0.1.50",
                Status = 0
            },
            new Station { 
                Id = "IAF013", 
                Name = "Station Name #13", 
                SerialNumber = "5430", 
                InstallDate = new DateTime(2012, 1, 1), 
                IndoorFirmware = "1.1.6.19", 
                Ip = "205.157.12.105", 
                FirstCallHome = new DateTime(2012, 1, 2),
                LastCallHome = new DateTime(2012, 7, 30),
                LastCalibration = new DateTime(2012, 7, 29),
                StreetAddress = "Bazar Guard Cross Road",
                City = "Hyderabad",
                Region = null,
                Province = "Andhra Pradesh",
                Country = "India",
                PostalCode = "500001",
                PocName = "John Doe",
                PocPhone = "1-555-555-1435",
                PocEmail = "john.doe@gmail.com",
                SendLogRateMinutes = 60,
                SendGpsRateMinutes = 60,
                SendAntennaGainRateMinutes = 15,
                AntennaMode = 9,
                AntennaAttenuation = 40,
                PrimaryDeliveryIp = "8.8.8.1",
                SecondaryDeliveryIp = "8.8.8.2",
                OutdoorFirmware = "1.0.6.8",
                Status = 1
            },
            new Station { 
                Id = "IAF007", 
                Name = "Station Name #7", 
                SerialNumber = "5440",
                InstallDate = new DateTime(2012, 1, 1), 
                IndoorFirmware = "1.1.5.17", 
                Ip = "206.158.13.106", 
                FirstCallHome = new DateTime(2012, 1, 2),
                LastCallHome = new DateTime(2012, 7, 30),
                LastCalibration = new DateTime(2012, 7, 29),
                StreetAddress = "Feroze Gandhi Marg",
                City = "Delhi",
                Region = null,
                Province = "Delhi",
                Country = "India",
                PostalCode = "110024",
                PocName = "Jane Doe",
                PocPhone = "1-555-555-1436",
                PocEmail = "jane.doe@gmail.com",
                SendLogRateMinutes = 60,
                SendGpsRateMinutes = 60,
                SendAntennaGainRateMinutes = 15,
                AntennaMode = 9,
                AntennaAttenuation = 40,
                PrimaryDeliveryIp = "8.8.8.1",
                SecondaryDeliveryIp = "8.8.8.2",
                OutdoorFirmware = "1.0.6.8",
                Status = 2
            },
            new Station { 
                Id = "IAF009", 
                Name = "Station Name #9", 
                SerialNumber = "5450",
                InstallDate = new DateTime(2012, 1, 4), 
                IndoorFirmware = "1.1.5.17", 
                Ip = "206.158.13.106", 
                FirstCallHome = new DateTime(2012, 1, 2),
                LastCallHome = new DateTime(2012, 8, 6),
                LastCalibration = new DateTime(2012, 7, 29),
                StreetAddress = "Main Street",
                City = "Puducherry",
                Region = null,
                Province = "Puducherry ",
                Country = "India",
                PostalCode = "605004",
                PocName = "Jesse Tomlinson",
                PocPhone = "1-555-555-1437",
                PocEmail = " jtomlinson@earthnetworks.com",
                SendLogRateMinutes = 60,
                SendGpsRateMinutes = 60,
                SendAntennaGainRateMinutes = 15,
                AntennaMode = 9,
                AntennaAttenuation = 40,
                PrimaryDeliveryIp = "8.8.8.1",
                SecondaryDeliveryIp = "8.8.8.2",
                OutdoorFirmware = "1.0.6.8",
                Status = 3
            },
            new Station { 
                Id = "IAF006", 
                Name = "Station Name #6", 
                SerialNumber = "5460",
                InstallDate = new DateTime(2012, 1, 5), 
                IndoorFirmware = "1.1.5.18", 
                Ip = "206.158.13.106", 
                FirstCallHome = new DateTime(2012, 1, 2),
                LastCallHome = new DateTime(2012, 7, 30),
                LastCalibration = new DateTime(2012, 7, 29),
                StreetAddress = "Balan K Nair Road",
                City = "Kozhikode",
                Region = null,
                Province = "Kerala",
                Country = "India",
                PostalCode = "673005",
                PocName = "Justin Dearing",
                PocPhone = "1-555-555-1438",
                PocEmail = "jdearing@earthnetworks.com",
                SendLogRateMinutes = 60,
                SendGpsRateMinutes = 60,
                SendAntennaGainRateMinutes = 15,
                AntennaMode = 9,
                AntennaAttenuation = 40,
                PrimaryDeliveryIp = "8.8.8.1",
                SecondaryDeliveryIp = "8.8.8.2",
                OutdoorFirmware = "1.0.6.8",
                Status = 4
            },
        };

        private int _nextId = 3;

        public int Update(Station updateStation)
        {
            const int success = 1;
            int index = _stations.FindIndex(p => p.Id == updateStation.Id);
            if (index != -1)
            {
                _stations.RemoveAt(index);
                _stations.Add(updateStation);
            }
            return success;

        }

        public Station Get(string id)
        {
            return _stations.Find(p => p.Id == id);
        }

        public IEnumerable<Station> GetAll()
        {
            return _stations.AsQueryable();
        }

        public Station Add(Station station)
        {
            station.Id = GetIdString(_nextId++);
            _stations.Add(station);
            return station;
        }

        private string GetIdString(int id)
        {
            return string.Format("IAF{0}", id.ToString(CultureInfo.InvariantCulture).PadLeft(3, '0'));
        }

        public bool Remove(string id)
        {
            try
            {
                _stations.RemoveAll(p => p.Id == id);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public void AddStationToNetwork(string stationId, string networkId)
        {
            
        }

        public void RemoveStationFromNetwork(string stationId, string networkId)
        {
            
        }

        public IEnumerable<StationV2> GetAllStationsInNetwork(string networkId)
        {
            return null;
        }

        public IEnumerable<StationV2> GetAllV2()
        {
            return null;
        }

        public StationV2 GetV2(string id)
        {
            return null;
        }
    }
}