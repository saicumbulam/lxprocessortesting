﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;

using Aws.Core.Data;
using Aws.Core.Utilities;

using LxCommon;
using LxCommon.Monitoring;
using LxPortalLib.Models;

using LxPortal.Api.Models.Database;

namespace LxPortal.Api.Models
{
    public class SensorPacketHistoryRepositoryDb : ISensorPacketHistoryRepository
    {
        private const int DefaultCapacity = 100;
        public List<PacketRecord> Get(string id, DateTime? start, DateTime? end)
        {
            SqlConnection conn = null;
            SqlDataReader dr = null;
            DbCommand dbCmd = null;
            List<PacketRecord> packets = null;

            try
            {
                if (start.HasValue && end.HasValue && start.Value.CompareTo(DateTime.MinValue) == 0 && end.Value.CompareTo(DateTime.MaxValue) == 0)
                {
                    start = null;
                    end = null;
                }
                conn = DbConnectionFactory.Build(ConfigurationManager.ConnectionStrings["Lightning"].ConnectionString);
                SensoryPacketHistoryDbCmdText dbCmdText = new SensoryPacketHistoryDbCmdText(id, start, end);
                dbCmd = new DbCommand(conn, dbCmdText);
                dbCmd.Command.CommandTimeout = Config.DbCommandTimeoutMilliseconds;
                dr = dbCmd.ExecuteReader();
                packets = new List<PacketRecord>(DefaultCapacity);
                PacketRecord packet;
                while (dr.Read())
                {
                    packet = (dbCmdText.ParseRow(dr)) as PacketRecord;
                    if (null != packet)
                        packets.Add(packet);
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.ApiSensorPacketHistoryGet, string.Format("Unable to get current sensor status. Message: {0}", ex.Message));
            }
            finally
            {
                if (null != dbCmd) dbCmd.Close();
                if (null != dr) dr.Close();
                if (null != conn) conn.Close();
            }

            return packets;
        }

    }
}