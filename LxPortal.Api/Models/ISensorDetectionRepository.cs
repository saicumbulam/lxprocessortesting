﻿using System;
using LxPortalLib.Models;

namespace LxPortal.Api.Models
{
    public interface ISensorDetectionRepository
    {
        SensorDetection Get(string id, DateTime? start, DateTime? end);
    }
}
