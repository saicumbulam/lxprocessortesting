﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

using En.Ods.Lib.Common.Interface;
using En.Ods.Lib.Common.Utility.ServiceClient;

using En.Data.Api.Audit.Client;
using En.Data.Api.Audit.Client.Models;

using LxPortal.Api.Helper;

namespace LxPortal.Api.Models
{

    public interface IPartnerEventsRepository
    {
        Task<IEnumerable<PartnerEvent>> Get(string id, DateTime start, DateTime end);
    }

    public class PartnerEventsAuditRepository : IPartnerEventsRepository
    {
        private static readonly string AuditBaseUrl;
        private static readonly string AuditTableName;
        static PartnerEventsAuditRepository()
        {
            AuditBaseUrl = Config.AuditBaseUrl;
            if (string.IsNullOrWhiteSpace(AuditBaseUrl))
            {
                throw new ArgumentException("Missing config value AuditBaseUrl");
            }

            AuditTableName = Config.AuditTableName;
            if (string.IsNullOrWhiteSpace(AuditTableName))
            {
                throw new ArgumentException("Missing config value AuditTableName");
            }
        }

        public async Task<IEnumerable<PartnerEvent>> Get(string id, DateTime start, DateTime end)
        {
            IEnumerable<PartnerEvent> events = null;
            AuditClient client = new AuditClient(AuditBaseUrl);
            OdsServiceResponse<AuditRecordList> result = await client.GetAudit(AuditTableName, start, end, null, id, null, null, null, null);

            events = PartnerEvent.CreatePartnerEvents(result);

            return events;
        }
    }

    public class PartnerEvent
    {
        private object RemoteIP;

        internal static IEnumerable<PartnerEvent> CreatePartnerEvents(OdsServiceResponse<AuditRecordList> result)
        {
            List<PartnerEvent> events = null;
            if (result.IsSuccessStatusCode() && result.Result != null && result.Result.Records != null)
            {
                events = new List<PartnerEvent>();
                foreach (AuditRecord record in result.Result.Records)
                {
                    PartnerEvent pevent = new PartnerEvent();
                    if (record.AuditAttributes.AsDictionaryElements.Any())
                    {
                        pevent.TimeUtc = record.RangeKey.DateTimeUtc;
                        foreach (KeyValuePair<string, IOdsValue> item in record.AuditAttributes.AsDictionaryElements)
                        {
                            if (item.Key == "lastModifiedBy")
                            {
                                pevent.LastModifiedBy = item.Value.AsString;
                            }
                            else if (item.Key == "remoteIpAddress")
                            {
                                pevent.RemoteIpAddress = item.Value.AsString;
                            }
                            else if (item.Key == "eventText")
                            {
                                pevent.Text = item.Value.AsString;
                            }
                        }
                    }
                    events.Add(pevent);
                }
            }

            return events;
        }

        public string Text { get; set; }

        public string RemoteIpAddress { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime TimeUtc { get; set; }
    }
}