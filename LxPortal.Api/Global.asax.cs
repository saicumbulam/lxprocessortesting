﻿using System;
using System.IO;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Aws.Core.Utilities;
using LxCommon.Monitoring;

namespace LxPortal.Api
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            SqlServerTypes.Utilities.LoadNativeAssemblies(Server.MapPath("."));

            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            EventManager.Init();

            CreateFailoverFolderPath();
        }

        private void CreateFailoverFolderPath()
        {
            try
            {
                DirectoryInfo di = new DirectoryInfo(Config.FailoverFolderPath);
                if (!di.Exists)
                {
                    di.Create();
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.ApiStickyFailover, string.Format("Unable to create failover folder directory: {0}", ex.Message));
            }

        }
    }
}