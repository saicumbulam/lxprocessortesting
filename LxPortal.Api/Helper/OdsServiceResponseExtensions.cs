﻿using System;
using System.Net;

using En.Ods.Lib.Common.Utility.ServiceClient;

namespace LxPortal.Api.Helper
{
    public static class OdsServiceResponseExtensions
    {
        public static bool IsSuccessStatusCode<T>(this OdsServiceResponse<T> osr)
        {
            HttpStatusCode hsc = osr.GetResponseStatusCode();
            return (hsc >= HttpStatusCode.OK && hsc <= (HttpStatusCode)299);
        }

        public static HttpStatusCode GetResponseStatusCode<T>(this OdsServiceResponse<T> osr)
        {
            HttpStatusCode hsc;
            object val = Enum.ToObject(typeof(HttpStatusCode), osr.Code);
            if (Enum.IsDefined(typeof(HttpStatusCode), val))
            {
                hsc = (HttpStatusCode)val;
            }
            else
            {
                hsc = HttpStatusCode.Unused;
            }
            return hsc;
        }
    }
}