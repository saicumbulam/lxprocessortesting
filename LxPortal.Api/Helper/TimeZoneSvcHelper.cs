﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using Aws.Core.TimeZoneServiceRef;
using Aws.Core.Utilities; 
using System.Data.SqlClient;
using LxPortal.Api.Models.Database;

namespace LxPortal.Api.Helper
{
    public class TimeZoneSvcHelper
    {
        private static Dictionary<string, TimeZoneData> _TimeZoneCache = new Dictionary<string, TimeZoneData>();

        public static TimeZoneData GetTimeZone(double latitude, double longitude)
        {
            string cacheKey = latitude.ToString("N8") + "_" + longitude.ToString("N8");
            TimeZoneData data = null;
            if (_TimeZoneCache.ContainsKey(cacheKey))
            {
                data = _TimeZoneCache[cacheKey];
            }
            else
            {
                TimeZoneServiceClient client = new TimeZoneServiceClient();
                try
                {
                    if (!((client.State == CommunicationState.Created) ||
                            (client.State != CommunicationState.Opened) ||
                            (client.State != CommunicationState.Faulted)))
                    {
                        client.Abort();
                        client = new TimeZoneServiceClient();
                    }

                    data = client.GetTimeZone(latitude, longitude);
                    if (data != null)
                    {
                        _TimeZoneCache[cacheKey] = data;
                    }
                }
                finally
                {
                    try
                    {
                        if ((client != null) && (client.State != CommunicationState.Closed))
                        {
                            if (client.State == CommunicationState.Faulted)
                                client.Abort();
                            else
                                client.Close();
                        }
                    }
                    catch (CommunicationException)
                    {
                        client.Abort();
                    }
                    catch (TimeoutException)
                    {
                        client.Abort();
                    }
                    catch (System.Exception)
                    {
                        throw;
                    }
                }
            }

            return data;
        }

        public static DateTime UtcToLocal(DateTime dtUTC, double latitude, double longitude)
        {
            if (dtUTC == DateTime.MinValue)
                return DateTime.MinValue;

            TimeZoneData data = GetTimeZone(latitude, longitude);

            if (data != null)
            {
                DateTime adjustedTime = dtUTC.AddHours(data.TimeZoneOffset);
                if ((adjustedTime >= data.DayLightSavingStartTime) && (adjustedTime <= data.DayLightSavingEndTime))
                {
                    adjustedTime = adjustedTime.AddHours(1);
                }
                //data.TimeZoneOffset
                return adjustedTime;
            }

            return dtUTC;

        }
        
        public static DateTime? ConvertUtcToLocal(SqlDataReader dr, string columnName)
        {
            DateTime? postedTime = null; //utcTime
            try
            {
                float lat = 0.0F;
                float lon = 0.0F;
                DateTime localTime = Convert.ToDateTime(dr["ReportedTime"].ToString()); 

                postedTime = (DBNull.Value != dr[columnName] ? (DateTime?) dr[columnName] : null);
                if (postedTime.HasValue)

                if (dr["Latitude"] != DBNull.Value)
                {
                    lat = Convert.ToSingle(dr["Latitude"].ToString()); 
                }
                if (dr["Longitude"] != DBNull.Value)
                {
                    lon = Convert.ToSingle(dr["Longitude"].ToString());
                }

                TimeZoneData tz = GetTimeZone(lat, lon); 
                if (tz != null)
                {
                    postedTime = localTime.AddHours(-1.0*tz.TimeZoneOffset); 
                }
            }
            catch (Exception ex)
            {

                EventManager.LogWarning(EventLogger.Code.Data.FailureReadingDataColumnInDbCommandText, "Failed to read ReporTime colunm '" + columnName + "' from the resultset.", ex);
            }
            return postedTime; 
        }

    }
}