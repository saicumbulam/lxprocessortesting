﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.IO;

using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;

using En.Ods.Lib.Common.Dynamo;
using En.Ods.Lib.Common.Interface;
using En.Ods.Lib.Common.Serialization;

using Aws.Core.Data;
using Aws.Core.Utilities;

using LxCommon;
using LxCommon.Models;
using LxCommon.Monitoring;
using LxPortalLib.Models;

using LxPortal.Api.Helper;
using LxPortal.Api;
using LxPortal.Api.Models.Database;

using Newtonsoft.Json;


namespace LxPortal.Api.Helper
{
    public class CloudReader
    {
        private ICloudStorage _storage;
        private string _s3Bucket;
        private IEnumerable<ICloudStorageKey> _keys;

        public CloudReader(string s3Bucket)
        {

            if (string.IsNullOrWhiteSpace(s3Bucket))
            {
                throw new ArgumentNullException("s3Bucket");
            }
            _s3Bucket = s3Bucket;

            IOdsValue credentials = GetTopicOverrideCredentials();
            _storage = new S3Factory().GetStorage(credentials);

        }

        //get amazon keys
        public List<ICloudStorageKey> GetKeys(string matchingPart)
        {

            _keys = _storage.GetKeys(_s3Bucket, matchingPart);
            List<ICloudStorageKey> allKeys = new List<ICloudStorageKey>();

            foreach (ICloudStorageKey key in _keys)
            {
                allKeys.Add(key);
            }

            return allKeys;
        }

        //returns local path or null if no file was read in
        public ICloudObject GetCloudObject(string path)
        {

            ICloudObject cloudObject = _storage.GetObject(_s3Bucket, path);

            return cloudObject;
        }

        private IOdsValue GetTopicOverrideCredentials()
        {
            IOdsValue credentials = null;

            string account = Config.HistoricalAWSAccessKey;
            string password = Config.HistoricalAWSSecretKey;

            if (!String.IsNullOrWhiteSpace(account) && !String.IsNullOrWhiteSpace(password))
            {
                credentials = new JsonOdsValue();
                credentials[CredentialsFactoryConstants.AccountId] = new JsonOdsValue(account);
                credentials[CredentialsFactoryConstants.Password] = new JsonOdsValue(password);
            }

            return credentials;
        }

        public void DeleteFiles()
        {
            IOdsValue credentials = new JsonOdsValue();
            ICloudStorage S3storage = new S3Factory().GetStorage(credentials);

            //GetKeys method does not work very well when there is a folder structure
            for (int i = 0; i <= 3; i++)
            {

                IEnumerable<ICloudStorageKey> keys = S3storage.GetKeys(_s3Bucket, i + "");

                List<ICloudStorageKey> allKeysList = new List<ICloudStorageKey>();

                foreach (ICloudStorageKey key in keys)
                {
                    allKeysList.Add(key);
                }

                foreach (ICloudStorageKey key in allKeysList)
                {
                    //delete all 2010 files from S3
                    if (key.Path.Contains("-2010-"))
                    {
                        Console.WriteLine(DateTime.Now + " " + key.Path);
                        S3storage.DeleteObject(_s3Bucket, key.Path);
                    }
                }
            }
        }
    }
}