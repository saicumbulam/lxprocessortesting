﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Aws.Core.Utilities;
using LxCommon;
using LxCommon.Monitoring;
using Microsoft.SqlServer.Types;

namespace LxPortal.Api.Helper
{
    public class SqlGeographyUtil
    {
        public const int Wgs84SpatialRefId = 4326;


        public static SqlGeography CreateSqlBoundingBox(decimal ullat, decimal ullon, decimal lrlat, decimal lrlon)
        {
            return CreateSqlBoundingBox(Convert.ToDouble(ullat), Convert.ToDouble(ullon), Convert.ToDouble(lrlat), Convert.ToDouble(lrlon));
        }

        public static SqlGeography CreateSqlBoundingBox(double ullat, double ullon, double lrlat, double lrlon)
        {
            SqlGeography geo = null;
            if (IsValidLatitude(ullat) && IsValidLatitude(lrlat) && IsValidLongitude(ullon) && IsValidLongitude(lrlon))
            {
                List<Tuple<double, double>> points = new List<Tuple<double, double>> 
                { 
                    new Tuple<double,double>(ullat, lrlon),
                    new Tuple<double,double>(lrlat, lrlon),
                    new Tuple<double,double>(lrlat, ullon),
                    new Tuple<double,double>(ullat, ullon)
                };

                geo = CreateSqlPolygon(points);
            }
            
            return geo;
        }

        public static SqlGeography CreateSqlPolygon(List<Tuple<double, double>> points)
        {
            SqlGeography geo = null;
            if (points.Count() >= 3)
            {
                SqlGeographyBuilder geoBuilder = new SqlGeographyBuilder();
                geoBuilder.SetSrid(Wgs84SpatialRefId);
                try
                {
                    geoBuilder.BeginGeography(OpenGisGeographyType.Polygon);

                    //whether each ring is a "hole" or not depends on the order in which you define the points. 
                    //use anticlockwise ordering for the outer ring, clockwise for interior rings.
                    int count = points.Count() - 1;

                    geoBuilder.BeginFigure(points[count].Item1, points[count].Item2);

                    for (int i = count - 1; i >= 0; i--)
                        geoBuilder.AddLine(points[i].Item1, points[i].Item2);

                    // last point same as first to close polygon
                    geoBuilder.AddLine(points[count].Item1, points[count].Item2);

                    geoBuilder.EndFigure();
                    geoBuilder.EndGeography();
                    geo = geoBuilder.ConstructedGeography;
                }
                catch (Exception ex)
                {
                    EventManager.LogError(TaskMonitor.ApiNwsAlertGet, "Unable to create SqlGeography from list of points", ex);
                }
            }
            return geo;
        }

        public static bool IsValidLatitude(double lat)
        {
            return (lat >= -90 && lat <= 90);
        }

        public static bool IsValidLongitude(double lon)
        {
            return (lon >= -180 && lon <= 180);
        }
    }
}