﻿using System;
using Aws.Core.Utilities.Validation;
using LxPortalLib.Common;
using LxPortalLib.Models;

namespace LxPortal.Api.Helper
{
    public static class ParameterValidator
    {
        public static bool ValidateRadius(int input, out int output, out string message)
        {
            message = string.Empty; 
            output = (input >= Config.DetectionReportMinRadiusInMeters && input <= Config.DetectionReportMaxRadiusInMeters) ? input : 0; 

            if (output == 0)
            {
                message = "Radius input is below or above configured min/max values.";
                return false; 
            }
            return true; 
        }

        public static bool ValidateLatitude(decimal latInput, out string message)
        {
            if ((latInput <= 90 && latInput >= -90))
            {
                message = null; 
                return true; 
            }
            message = "Invalid latitude value.";
            return false; 
        }

        public static bool ValidateLongitude(decimal lonInput, out string message)
        {
            if (lonInput <= 180 && lonInput >= -180)
            {
                message = null;
                return true;
            }
            message = "Invalid longitude value.";
            return false;
        }

        public static bool ValidateStrokeType(StrokeType typeInput, out StrokeType typeOutput)
        {
            //if stroketype invalid, set none as default 

            typeOutput = (typeInput != StrokeType.None && typeInput != StrokeType.IntraCloud &&
                          typeInput != StrokeType.CloudToGround)
                             ? StrokeType.None
                             : typeInput; 
            return true; 
        }

        public static bool ValidateLxType(int typeInput, out LxType typeOutput)
        {
            if (typeInput == 0)
                typeOutput = LxType.Flash;
            else if(typeInput == 1)
                typeOutput = LxType.Portion;
            else
                typeOutput = LxType.Both;

            return true;
        }

        public static bool ValidateFileOutputType(int typeInput, out FileOutputType typeOutput)
        {
            if (typeInput == 0)
                typeOutput = FileOutputType.Json;
            else if (typeInput == 1)
                typeOutput = FileOutputType.Csv;
            else
                typeOutput = FileOutputType.Kmz;

            return true;
        }

        public static bool ValidateOutputVersion(int typeInput, out LxOutputVersion typeOutput)
        {
            if (typeInput == 0)
                typeOutput = LxOutputVersion.V2;
            else
                typeOutput = LxOutputVersion.V3;

            return true;
        }

        public static bool ValidateStartEndTime(DateTime start, DateTime end, out DateTime startTime, out DateTime endTime, out string message, bool checkAge = false)
        {
            message = string.Empty;

            if (checkAge && (DateTime.UtcNow - start).TotalDays > Config.LxQueryAgeDays) // start time is too old
            {
                message = "Start time is too old.";
                startTime = Utilities.NormalizeDateTimeToUtc(start);
                endTime = Utilities.NormalizeDateTimeToUtc(end);
                return false;
            }

            if (start < end )
            {
                startTime = Utilities.NormalizeDateTimeToUtc(start);
                endTime = Utilities.NormalizeDateTimeToUtc(end);
            }
            else
            {
                message = "Invalid start and end time.";
                startTime = Utilities.NormalizeDateTimeToUtc(start);
                endTime = Utilities.NormalizeDateTimeToUtc(end);
                return false; 
            }
            return true; 
        }

        public static bool ValidateBoundingBox(decimal ullat, decimal ullon, decimal lrlat, decimal lrlon, out string message)
        {
            //if valid 
            if (ValidateLatitude(ullat, out message) && ValidateLongitude(ullon, out message) && ValidateLatitude(lrlat, out message) && ValidateLongitude(lrlon, out message) && ullat > lrlat && ullon < lrlon)
            {
                message = null;
                return true; 
            }

            message = "Invalid bounding box, check latitude and longitude inputs. ";
            return false; 
        }

        public static bool ValidateEmail(string responseEmailAddress, out string message)
        {
            if (EmailValidator.Validate(responseEmailAddress))
            {
                message = null;
                return true; 
            }

            message = "Invalid email address.";
            return false;
        }
    }
}