﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LxPortalLib.Models;

namespace LxPortal.Api.Helper
{
    public partial class JobSubmission
    {
        public LxQueryJobData _lxQueryJobData { get; set; }
        public string _emailSubject { get; set; }
    }
}
