﻿using System;
using System.IO;
using Aws.Core.Utilities.Config;

namespace LxPortal.Api
{
    public class Config : AppConfig
    {
        public static int DbCommandTimeoutMilliseconds
        {
            get { return Get<int>("LxPortal.Api.Models.DbCommandTimeoutMilliseconds", 5000); }
        }

        public static int EcvRequestHttpTimeoutMilliseconds
        {
            get { return Get<int>("LxPortal.Api.Controller.EcvRequestHttpTimeoutMilliseconds", 3000); }
        }

        public static int DataMartAgeHours
        {
            get { return Get<int>("LxPortal.Api.Models.DataMartAgeHours", 48); }
        }

        public static int LxQueryAgeDays
        {
            get { return Get<int>("LxPortal.Api.Models.LxQueryAgeDays", 5); }
        }

        public static string ReceiverEcv
        {
            get { return Get<string>("LxPortal.Api.Controller.ReceiverEcv", ""); }
        }

        public static string ProcessorEcv
        {
            get { return Get<string>("LxPortal.Api.Controller.ProcessorEcv", ""); }
        }

        public static string DataFeedEcv
        {
            get { return Get<string>("LxPortal.Api.Controller.DataFeedEcv", ""); }
        }

        public static string SystemType
        {
            get { return Get<string>("LxPortal.Api.Controller.SystemType", ""); }
        }

        public static bool UseDataMartArchive
        {
            get { return Get("LxPortal.Api.Models.UseDataMartArchive", false); }
        }

        public static long DataMartArchiveEndDateKey
        {
            get { return Get("LxPortal.Api.Models.DataMartArchiveEndDateKey", 20130331234); }
        }

        public static long DataMartStartDateKey
        {
            get { return Get("LxPortal.Api.Models.DataMartStartDateKey", 20130401001); }
        }

        public static int LastPacketReceivedInterval
        {
            get { return Get("LxPortal.Api.Models.LastPacketReceivedInterval", 5); }
        }

        public static string FailoverFolderPath
        {
            get
            {
                string path = Get<string>("LxPortal.Api.FailoverFolderPath", null) ??
                              Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                                           "Earth Networks\\LxPortalApi");
                return path;
            }
        }

        public static string FailoverFile
        {
            get { return Path.Combine(FailoverFolderPath, "StickyFailover.txt"); }
        }

        public static int AllowableFailureTimeRangeSec
        {
            get { return Get<int>("LxPortal.Api.AllowableFailureTimeInSec", 60); }
        }

        public static string LastKnwonFailureTimeFile
        {
            get { return Path.Combine(FailoverFolderPath, "LastKnownFailureTime.txt"); }
        }

        public static int DetectionReportMaxRadiusInMeters
        {
            get { return Get("LxPortal.Api.Models.DetectionReportMaxRadiusInMeters", 804672); }
        }

        public static int DetectionReportMinRadiusInMeters
        {
            get { return Get("LxPortal.Api.Models.DetectionReportMinRadiusInMeters", 10000); }
        }

        public static string AuditBaseUrl
        {
            get { return Get<string>("AuditBaseUrl", null); }
        }

        public static string AuditTableName
        {
            get { return Get<string>("AuditTableName", "LxDatafeedPartner"); }
        }

        public static string HistoricalAWSAccessKey
        {
            get { return Get<string>("HistoricalAWSAccessKey"); } 
        }

        public static string HistoricalAWSSecretKey
        {
            get { return Get<string>("HistoricalAWSSecretKey"); }
        }

        public static string AwsSnsLxQueryNotifierTopic
        {
            get { return Get<string>("AwsSnsLxQueryNotifierTopic", null); }
        }

        public static string JobSubmittedSubject
        {
            get { return Get<string>("JobSubmittedSubject", null); }
        }

        public static string EmailFromAddress
        {
            get { return Get<string>("EmailFromAddress", null); }
        }
    }
}