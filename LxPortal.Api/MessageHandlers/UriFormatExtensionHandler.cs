﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace LxPortal.Api.MessageHandlers
{
    public class UriFormatExtensionHandler : DelegatingHandler
    {
        private static readonly IDictionary<string, string> ExtensionMappings = new Dictionary<string, string>()
            {
                { "xml", "application/xml" },
                { "json", "application/json" }
            };

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            string path = request.RequestUri.AbsolutePath;
            string ext = path.Substring(path.LastIndexOf('.') + 1);
            string mediaType = null;

            if (ExtensionMappings.TryGetValue(ext, out mediaType))
            {
                string newUri = request.RequestUri.OriginalString.Replace('.' + ext, String.Empty);
                request.RequestUri = new Uri(newUri, UriKind.Absolute);
                request.GetRouteData().Values.Remove("ext");
                request.Headers.Accept.Clear();
                request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue(mediaType));
            }

            return base.SendAsync(request, cancellationToken);
        }

    }
}