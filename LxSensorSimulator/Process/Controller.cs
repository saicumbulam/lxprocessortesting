﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

using LxCommon;

using LxSensorSimulator.Models;

namespace LxSensorSimulator.Process
{
    public class Controller
    {
        private string _folder;
        private string _ipString;
        private string _portString;
        private bool _loopPlayback;
        private bool _useSocketPerSensor;
        private int _numSockets;
        private bool _useSensorPacket;
        private Action<string> _updateStatus;
        private Action<int> _setTotal;
        private Action _incrementCompleted;
        private IPEndPoint _ipEndPoint;
        private DirectoryInfo _directory;
        private static readonly DateTime EpochStartTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public Controller(string folder, string ipString, string portString, bool loopPlayback, bool useSocketPerSensor, int numSockets, bool useSensorPacket,
            Action<string> updateStatus, Action<int> setTotal, Action incrementCompleted)
        {
            _folder = folder;
            _ipString = ipString;
            _portString = portString;
            _loopPlayback = loopPlayback;
            _useSocketPerSensor = useSocketPerSensor;
            _numSockets = numSockets;
            _useSensorPacket = useSensorPacket;
            _updateStatus = updateStatus;
            _setTotal = setTotal;
            _incrementCompleted = incrementCompleted;
            _ipEndPoint = null;
            _directory = null;
        }

        public async Task<bool> TryInit()
        {
            bool success = false;
            IPEndPoint ipep;
            if (TryParseIpEndPoint(_ipString, _portString, out ipep))
            {
                _ipEndPoint = ipep;
                string status = await TestNetwork(ipep.Address.ToString(), ipep.Port.ToString(CultureInfo.InvariantCulture));
                if (status.Contains("Successfully connected to"))
                {
                    success = true;
                }
                else
                {
                    _updateStatus(status);
                }
            }
            else
            {
                _updateStatus("Invalid ip address");
            }

            DirectoryInfo di;
            if (TryGetDirectory(_folder, out di))
            {
                _directory = di;
                success = (success && true);
            }
            else
            {
                success = false;
            }

            return success;
        }

        private bool TryGetDirectory(string folder, out DirectoryInfo di)
        {
            bool success = false;
            di = null;
            try
            {
                di = new DirectoryInfo(folder);
                if (di.Exists)
                {
                    FileInfo[] fi = di.GetFiles("*.ltg", SearchOption.AllDirectories);
                    if (fi.Length > 0)
                    {
                        success = true;
                    }
                    else
                    {
                        _updateStatus(string.Format("The directory \"{0}\" does not contain any waveform files", folder));
                    }
                }
                else
                {
                    success = false;
                    _updateStatus(string.Format("The directory \"{0}\" does not exist", folder));
                }
            }
            catch (Exception ex)
            {
                _updateStatus(ex.Message);
            }

            return success;
        }


        private static bool TryParseIpEndPoint(string ipString, string portString, out IPEndPoint ipEndPoint)
        {
            bool success = false;
            ipEndPoint = null;

            int port;
            if (int.TryParse(portString, out port) && port > 0 && port <= 65535)
            {
                IPAddress address;
                if (!string.IsNullOrEmpty(ipString))
                {
                    if (IPAddress.TryParse(ipString, out address))
                    {
                        ipEndPoint = new IPEndPoint(address, port);
                        success = true;
                    }
                }
            }

            return success;
        }


        public void StartPlayback(CancellationToken cancelToken)
        {
            FileInfo[] fis = GetFiles(_directory, _updateStatus);
            if (fis != null)
            {
                if (fis.Length > 0)
                {
                    int totalPackets = GetTotalPacketsInFiles(fis);

                    _updateStatus(string.Format("Playback of {0} files with {1:n0} packets", fis.Length, totalPackets));

                    _setTotal(totalPackets);

                    List<Task> tasks = new List<Task>();

                    int lastPacketTime = 0;
                    int? fileOffset = null;
                    do
                    {
                        foreach (FileInfo fi in fis)
                        {
                            List<Task> tsks = ReadFile(fi, ref lastPacketTime, ref fileOffset, _ipEndPoint, _useSocketPerSensor, _numSockets, _useSensorPacket, _updateStatus,
                                _incrementCompleted, cancelToken);

                            if (tsks != null && tsks.Count > 0)
                            {
                                tasks.AddRange(tsks);
                            }

                            if (cancelToken.IsCancellationRequested)
                            {
                                break;
                            }
                        }

                    } while (_loopPlayback && !cancelToken.IsCancellationRequested);

                    try
                    {
                        Task.WaitAll(tasks.ToArray(), cancelToken);
                        _updateStatus("Playback completed");
                    }
                    catch (Exception ex)
                    {
                        if (ex is OperationCanceledException || ex is AggregateException)
                        {
                            _updateStatus("Playback has been canceled");
                        }
                    }

                    Sensor.Cleanup();

                }
                else
                {
                    _updateStatus(string.Format("The directory \"{0}\" does not contain any waveform files", _directory.FullName));
                }
            }
        }

        private int GetTotalPacketsInFiles(FileInfo[] fis)
        {
            int numPackets = 0;
            foreach (FileInfo fi in fis)
            {
                using (FileStream fs = fi.OpenRead())
                {
                    if (fs.Length >= MinimumFileLengthBytes)
                    {
                        byte[] bytes = new byte[4];
                        fs.Read(bytes, 0, 4);
                        numPackets += BitConverter.ToInt32(bytes, 0);
                    }
                }
            }

            return numPackets;
        }

        private FileInfo[] GetFiles(DirectoryInfo directory, Action<string> updateStatus)
        {
            FileInfo[] fis = null;
            try
            {
                if (directory.Exists)
                {
                    fis = directory.GetFiles("*.ltg", SearchOption.AllDirectories);
                }
                else
                {
                    updateStatus(string.Format("The directory \"{0}\" does not exist", _directory.FullName));
                }
            }
            catch (Exception ex)
            {
                updateStatus(string.Format("Error: {0}", ex.Message));
            }

            return fis;
        }

        private const int MinimumFileLengthBytes = 6;
        private List<Task> ReadFile(FileInfo fi, ref int lastPacketTime, ref int? fileOffset, IPEndPoint ipEndPoint, bool useSocketPerSensor, int numSockets, bool useSensorPacket, Action<string> updateStatus, Action incrementCompleted, CancellationToken cancelToken)
        {
            updateStatus(string.Format("Started reading of \"{0}\"", fi.FullName));

            List<Task> tasks = new List<Task>();
            try
            {
                using (FileStream fs = fi.OpenRead())
                {
                    if (fs.Length >= MinimumFileLengthBytes)
                    {
                        byte[] bytes = new byte[4];
                        fs.Read(bytes, 0, 4);
                        int numOfPackets = BitConverter.ToInt32(bytes, 0);

                        updateStatus(string.Format("\"{0}\" has {1:n0} packets", fi.FullName, numOfPackets));

                        for (int i = 0; i < numOfPackets; i++)
                        {
                            Task t = ReadPacket(fi, ipEndPoint, useSocketPerSensor, numSockets, useSensorPacket, updateStatus, incrementCompleted,
                                ref cancelToken, fs, ref lastPacketTime, ref fileOffset);

                            if (t != null)
                            {
                                tasks.Add(t);
                            }

                            if (cancelToken.IsCancellationRequested)
                            {
                                break;
                            }
                        }
                    }
                    else
                    {
                        updateStatus(string.Format("Error: file \"{0}\" is less than {1} bytes", fi.FullName, MinimumFileLengthBytes));
                    }
                }

                updateStatus(string.Format("Finished reading file \"{0}\"", fi.FullName));
            }
            catch (Exception ex)
            {
                updateStatus(string.Format("Unable to read file \"{0}\" Error{1}\r\n", fi.FullName, ex.Message));
            }

            return tasks;
        }


        private static PacketHeaderInfo ReadRawPacket(FileStream fs, FileInfo fi, Action<string> updateStatus)
        {
            PacketHeaderInfo phi = null;
            int packetSize = 0;
            int bytesRead = -1;
            byte[] rawData = null;

            try
            {
                byte[] bytes = new byte[2];
                fs.Read(bytes, 0, 2);
                packetSize = BitConverter.ToInt16(bytes, 0);
                rawData = new byte[packetSize];
                bytesRead = fs.Read(rawData, 0, packetSize);
            }
            catch (Exception ex)
            {
                updateStatus(string.Format("Unable to read {0} bytes from file: \"{1}\" error: {2}", packetSize, fi.FullName, ex.Message));
            }

            if (bytesRead == packetSize && rawData != null && rawData.Length > 0)
            {
                phi = new PacketHeaderInfo();
                if (!RawPacketUtil.GetPacketHeaderInfo(rawData, phi))
                {
                    phi = null;
                }
            }

            return phi;
        }


        private static Task ReadPacket(FileInfo fi, IPEndPoint ipEndPoint, bool useSocketPerSensor, int numSockets, bool useSensorPacket, Action<string> updateStatus, Action incrementCompleted,
            ref CancellationToken cancelToken, FileStream fs, ref int lastPacketTime, ref int? fileOffset)
        {
            Task t = null;

            PacketHeaderInfo phi = ReadRawPacket(fs, fi, updateStatus);

            if (phi != null)
            {
                if (phi.MsgType == PacketType.MsgTypeGps || phi.MsgType == PacketType.MsgTypeLtg)
                {
                    if (!fileOffset.HasValue)
                    {
                        fileOffset = GetOffset(phi.RawData, lastPacketTime);
                        if (fileOffset.HasValue)
                            updateStatus(string.Format("File offset of {0:n0} seconds being applied", fileOffset.Value));
                    }

                    if (fileOffset.HasValue)
                    {
                        lastPacketTime = phi.PacketTimeInSeconds + fileOffset.Value;
                        if (phi.MsgType == PacketType.MsgTypeLtg)
                        {
                            byte[] date = BitConverter.GetBytes(lastPacketTime);
                            phi.RawData[14] = date[3];
                            phi.RawData[15] = date[2];
                            phi.RawData[16] = date[1];
                            phi.RawData[17] = date[0];
                        }
                        else if (phi.MsgType == PacketType.MsgTypeGps)
                        {
                            DateTime dt = EpochStartTime.AddSeconds(lastPacketTime);
                            phi.RawData[18] = BitConverter.GetBytes(dt.Month)[0];
                            phi.RawData[19] = BitConverter.GetBytes(dt.Day)[0];
                            byte[] y = BitConverter.GetBytes(dt.Year);
                            phi.RawData[20] = y[1];
                            phi.RawData[21] = y[0];
                            phi.RawData[22] = BitConverter.GetBytes(dt.Hour)[0];
                            phi.RawData[23] = BitConverter.GetBytes(dt.Minute)[0];
                            phi.RawData[24] = BitConverter.GetBytes(dt.Second)[0];
                        }

                        byte[] packet = useSensorPacket ? phi.RawData : RawPacketUtil.GetStreamingBuffer(phi);

                        t = Sensor.EnqueuePacket(packet, phi.SensorId, ipEndPoint, useSocketPerSensor, numSockets, useSensorPacket, cancelToken, updateStatus, incrementCompleted);
                    }
                }
            }

            return t;
        }


        private static int? GetOffset(byte[] rawData, int lastPacketTime)
        {
            DateTime startTime;

            startTime = lastPacketTime == 0 ? DateTime.UtcNow : EpochStartTime.AddSeconds(lastPacketTime);

            TimeSpan ts = startTime - ReadPacketTime(rawData, 14);
            return (int)ts.TotalSeconds;
        }

        public static DateTime ReadPacketTime(byte[] packet, int startPos)
        {
            DateTime dt = EpochStartTime.AddSeconds(ReadPacketEpochTime(packet, startPos));
            return DateTime.SpecifyKind(dt, DateTimeKind.Utc);
        }

        private static int ReadPacketEpochTime(byte[] packet, int startPos)
        {
            int epochTime = 0;
            if (packet != null && packet.Length >= startPos + 3)
            {
                byte[] date = new[] { packet[startPos + 3], packet[startPos + 2], packet[startPos + 1], packet[startPos] };
                epochTime = BitConverter.ToInt32(date, 0);
            }
            return epochTime;
        }

        public static async Task<string> TestNetwork(string ipString, string portString)
        {
            string status;
            IPEndPoint ipep;
            if (TryParseIpEndPoint(ipString, portString, out ipep))
            {
                Tuple<TcpClient, string> val = await TryConnect(ipep.Address, ipep.Port);
                TcpClient client = val.Item1;
                status = val.Item2;
                if (client != null && client.Connected)
                {
                    PacketHeaderInfo phi = RawPacketUtil.GetAKeepAlivePacket();
                    byte[] packet = RawPacketUtil.GetStreamingBuffer(phi);
                    status = await TrySendPacket(client, packet);
                    client.Close();
                }
            }
            else
            {
                status = "Invalid IP address and port";
            }

            return status;
        }

        private static async Task<Tuple<TcpClient, string>> TryConnect(IPAddress address, int port)
        {
            string status = null;
            TcpClient client = new TcpClient(AddressFamily.InterNetwork);

            try
            {
                await client.ConnectAsync(address, port);
            }
            catch (SocketException socketEx)
            {
                status = string.Format("Socket error code: {0}, {1}", socketEx.ErrorCode, socketEx.Message);
            }
            catch (Exception ex)
            {
                status = string.Format("Error: {0}", ex.Message);
            }

            Tuple<TcpClient, string> val = new Tuple<TcpClient, string>(client, status);

            return val;
        }

        private static async Task<string> TrySendPacket(TcpClient client, byte[] packet)
        {
            string status = null;
            try
            {
                NetworkStream ns = client.GetStream();
                await ns.WriteAsync(packet, 0, packet.Length);
                IPEndPoint remoteEndPoint = client.Client.RemoteEndPoint as IPEndPoint;
                status = string.Format("Successfully connected to {0}:{1}", remoteEndPoint.Address, remoteEndPoint.Port);
            }
            catch (SocketException socketEx)
            {
                status = string.Format("Socket error code: {0}, {1}", socketEx.ErrorCode, socketEx.Message);
            }
            catch (Exception ex)
            {
                status = string.Format("Error: {0}", ex.Message);
            }

            return status;

        }
    }
}
