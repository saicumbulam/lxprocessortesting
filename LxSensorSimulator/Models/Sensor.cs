﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

using LxSensorSimulator.Process;

namespace LxSensorSimulator.Models
{    
    public class Sensor
    {
        private IPEndPoint _ipEndPoint;
        private Action<string> _updateStatus;
        private ConcurrentQueue<byte[]> _packets;
        private TcpClient _tcpClient;
        private int _processLock;
        private Action _incrementCompleted;
        private int _maxQueueSize;
        CancellationToken _cancelToken;
        private int _timeStampPacketOffset;

        private const int DateTimeOffsetSensor = 14;
        private const int DateTimeOffsetStreaming = 16;

        
        private Sensor(IPEndPoint ipEndPoint, Action<string> updateStatus, CancellationToken cancelToken, Action incrementCompleted, int maxQueueSize, int timeStampPacketOffset)
        {
            _ipEndPoint = ipEndPoint;
            _updateStatus = updateStatus;
            _cancelToken = cancelToken;
            _incrementCompleted = incrementCompleted;
            _packets = new ConcurrentQueue<byte[]>();
            _tcpClient = null;
            _processLock = 0;
            _maxQueueSize = maxQueueSize;
            _timeStampPacketOffset = timeStampPacketOffset;
        }

        private Task AddPacket(byte[] packet)
        {
            Task task = null;

            if (_packets.Count >= _maxQueueSize)
            {
                SpinWait.SpinUntil(() => { return _packets.Count < (_maxQueueSize/2) || _cancelToken.IsCancellationRequested; });
            }

            if (!_cancelToken.IsCancellationRequested)
            {
                _packets.Enqueue(packet);
                if (0 == Interlocked.CompareExchange(ref _processLock, 1, 0))
                {
                    Func<Task> ft = ProcessPackets;
                    Task<Task> t = Task.Factory.StartNew(ft);
                    task = t.Unwrap();
                }
            }
            return task;
        }

        private async Task ProcessPackets()
        {   
            try
            {
                byte[] packet;
                while (_packets.TryDequeue(out packet) && !_cancelToken.IsCancellationRequested)
                {
                    DateTime packetTime = Controller.ReadPacketTime(packet, _timeStampPacketOffset);
                    TimeSpan ts = packetTime - DateTime.UtcNow;

                    if (ts.TotalMilliseconds > 0)
                    {
                        bool sleep = true;
                        byte[] peekPacket;
                        if (_packets.TryPeek(out peekPacket))
                        {
                            DateTime peekTime = Controller.ReadPacketTime(peekPacket, _timeStampPacketOffset);
                            sleep = (peekTime > packetTime);
                        }

                        if (sleep)
                        {
                            try
                            {
                                await Task.Delay(ts, _cancelToken);
                            }
                            catch{}
                        }
                    }

                    await SendPacket(packet);
                }
            }
            catch(Exception ex)
            {
                _updateStatus(string.Format("Error processing the send queue: {0}", ex.Message));
            }
            finally
            {
                Interlocked.Decrement(ref _processLock);
            }
        }

        private async Task SendPacket(byte[] packet)
        {
            while ((_tcpClient == null || !_tcpClient.Connected) && !_cancelToken.IsCancellationRequested)
            {
                _tcpClient = await CreateClient(_tcpClient, _ipEndPoint, _updateStatus);
            }

            if (_tcpClient != null && _tcpClient.Connected)
            {
                try
                {
                    NetworkStream ns = _tcpClient.GetStream();
                    await ns.WriteAsync(packet, 0, packet.Length, _cancelToken);
                }
                catch (SocketException se)
                {
                    _updateStatus(string.Format("Error: {0}, Error Code: {1}", se.Message, se.ErrorCode));
                }
                catch (Exception ex)
                {
                    _updateStatus(string.Format("Error: {0}", ex.Message));
                }
            }

            _incrementCompleted();
        }

        private async Task<TcpClient> CreateClient(TcpClient client, IPEndPoint ipEndPoint, Action<string> updateStatus)
        {
            CleanupClient(client);

            client = new TcpClient(AddressFamily.InterNetwork);

            try
            {
                Task connectionResult = client.ConnectAsync(ipEndPoint.Address, ipEndPoint.Port);
                await connectionResult;

            }
            catch (SocketException se)
            {
                updateStatus(string.Format("Error: {0}, Error Code: {1}", se.Message, se.ErrorCode));
            }

            return client;
        }

        private void CleanupClient()
        {
            CleanupClient(_tcpClient);
            _packets = null;
        }

        private void CleanupClient(TcpClient client)
        {
            if (client != null)
            {
                try
                {
                    if (client.Connected)
                    {
                        NetworkStream ns = _tcpClient.GetStream();
                        ns.Close();
                    }

                    client.Close();
                }
                catch (Exception ex)
                {
                    _updateStatus(string.Format("Error: {0}, Error Code: {1}", ex.Message));
                }
                    
            }

            client = null;
        }

        private static volatile Dictionary<string, Sensor> _sensors = new Dictionary<string, Sensor>();
        private const int MaxQueueSizeMutipleSockets = 500;
        private const int MaxQueueSizeSingleSocket = 100000;

        public static Task EnqueuePacket(byte[] packet, string id, IPEndPoint ipEndPoint, bool useSocketPerSensor, int numSockets, bool useSensorPacket, CancellationToken cancelToken, 
            Action<string> updateStatus, Action incrementCompleted)
        {

            int timeStampPacketOffset = (useSensorPacket) ? DateTimeOffsetSensor : DateTimeOffsetStreaming;
            Sensor sensor;

            if (!useSocketPerSensor)
            {
                Random random = new Random();
                id = random.Next(1, numSockets + 1).ToString(CultureInfo.InvariantCulture);
            }

            if (!_sensors.TryGetValue(id, out sensor))
            {
                sensor = new Sensor(ipEndPoint, updateStatus, cancelToken, incrementCompleted, MaxQueueSizeMutipleSockets, timeStampPacketOffset);
                _sensors.Add(id, sensor);
            }

            return sensor.AddPacket(packet);
        }

        public static void Cleanup()
        {
            if (_sensors.Count > 0)
            {
                Sensor sensor;
                string[] keys = new string[_sensors.Count];
                _sensors.Keys.CopyTo(keys, 0);
                
                foreach (string key in keys)
                {
                    if (_sensors.TryGetValue(key, out sensor))
                    {
                        sensor.CleanupClient();
                        _sensors.Remove(key);
                    }
                }
            }
        }
    }
}
