﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

using Forms = System.Windows.Forms;

using LxSensorSimulator.Process;
 

namespace LxSensorSimulator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Forms.FolderBrowserDialog _folderBrowserDialog;
        private Controller _controller;
        private CancellationTokenSource _cancellationTokenSource;
        private int _total;
        private int _completed;
        private List<string> _statusLines;
        private const int MaxStatusLineCount = 1334;

        public MainWindow()
        {
            _folderBrowserDialog = new Forms.FolderBrowserDialog();
            InitializeComponent();
            _statusLines = new List<string>(MaxStatusLineCount);
            _total = 0;
            _completed = 0;
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            tboxFolder.Text = Properties.Settings.Default.Folder;
            cboxLoop.IsChecked = Properties.Settings.Default.Loop;
            tboxIpAddress.Text = Properties.Settings.Default.IpAddress;
            tboxPort.Text = Properties.Settings.Default.Port;

            tboxNumberSockets.Text = Properties.Settings.Default.NumberSockets;
            if (Properties.Settings.Default.NumSockets)
            {
                radioNumSockets.IsChecked = true;
                radioSocketPerSensor.IsChecked = false;
                tboxNumberSockets.IsEnabled = true;
            }
            else
            {
                radioNumSockets.IsChecked = false;
                radioSocketPerSensor.IsChecked = true;
                tboxNumberSockets.IsEnabled = false;
            }

            if (Properties.Settings.Default.PacketSensor)
            {
                radioSensor.IsChecked = true;
                radioStreaming.IsChecked = false;
            }
            else
            {
                radioSensor.IsChecked = false;
                radioStreaming.IsChecked = true;
            }
        }

        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Properties.Settings.Default.Folder = tboxFolder.Text;
            Properties.Settings.Default.Loop = cboxLoop.IsChecked.HasValue && cboxLoop.IsChecked.Value;
            Properties.Settings.Default.IpAddress = tboxIpAddress.Text;
            Properties.Settings.Default.Port = tboxPort.Text;


            Properties.Settings.Default.NumberSockets = tboxNumberSockets.Text;

            if (radioNumSockets.IsChecked.HasValue && radioNumSockets.IsChecked.Value)
            {
                Properties.Settings.Default.NumSockets = true;
                Properties.Settings.Default.SocketPerSensor = false;
            }
            else
            {
                Properties.Settings.Default.NumSockets = false;
                Properties.Settings.Default.SocketPerSensor = true;
            }

            if (radioSensor.IsChecked.HasValue && radioSensor.IsChecked.Value)
            {
                Properties.Settings.Default.PacketSensor = true;
                Properties.Settings.Default.PacketStreaming = false;
            }
            else
            {
                Properties.Settings.Default.PacketSensor = false;
                Properties.Settings.Default.PacketStreaming = true;
            }

            Properties.Settings.Default.Save();
        }

        private void Choose_Click(object sender, RoutedEventArgs e)
        {
            // Show the FolderBrowserDialog.
            Forms.DialogResult result = _folderBrowserDialog.ShowDialog();
            if (result == Forms.DialogResult.OK)
            {
                tboxFolder.Text = _folderBrowserDialog.SelectedPath;
            }
        }

        private async void Test_Click(object sender, RoutedEventArgs e)
        {
            string status = await Controller.TestNetwork(tboxIpAddress.Text, tboxPort.Text);

            UpdateStatus(status);
        }

        private async void Start_Click(object sender, RoutedEventArgs e)
        {
            UpdateStatus("Starting playback");
            _total = 0;
            _completed = 0;
            UpdateProgress(0);

            tboxFolder.IsReadOnly = true;
            btnChoose.IsEnabled = false;
            bool loopPlayback = (cboxLoop.IsChecked.HasValue && cboxLoop.IsChecked.Value);
            cboxLoop.IsEnabled = false;

            tboxIpAddress.IsReadOnly = true;
            tboxPort.IsReadOnly = true;
            btnTest.IsEnabled = false;

            bool useSocketPerSensor = (radioSocketPerSensor.IsChecked.HasValue && radioSocketPerSensor.IsChecked.Value);
            int numSockets = 0;
            if (!useSocketPerSensor)
            {
                if (!int.TryParse(tboxNumberSockets.Text, out numSockets))
                {
                    numSockets = 1;
                    tboxNumberSockets.Text = numSockets.ToString(CultureInfo.InvariantCulture);
                }
            }

            radioNumSockets.IsEnabled = false;
            radioSocketPerSensor.IsEnabled = false;
            tboxNumberSockets.IsReadOnly = true;


            bool useSensorPacket = radioSensor.IsChecked.HasValue && radioSensor.IsChecked.Value;
            radioSensor.IsEnabled = false;
            radioStreaming.IsEnabled = false;

            btnStop.IsEnabled = true;
            btnStart.IsEnabled = false;

            _controller = new Controller(tboxFolder.Text, tboxIpAddress.Text, tboxPort.Text, loopPlayback, useSocketPerSensor, numSockets, useSensorPacket, UpdateStatus, SetTotal, IncrementCompleted);
            bool success = await _controller.TryInit();

            if (success)
            {
                _cancellationTokenSource = new CancellationTokenSource();
                Action action = () => { _controller.StartPlayback(_cancellationTokenSource.Token); };
                try
                {
                    Task t = Task.Run(action, _cancellationTokenSource.Token);
                    await t;
                }
                catch (Exception ex)
                {
                    int i = 0;
                }
            }

            _cancellationTokenSource = null;
            _controller = null;

            tboxFolder.IsReadOnly = false;
            btnChoose.IsEnabled = true;
            cboxLoop.IsEnabled = true;

            tboxIpAddress.IsReadOnly = false;
            tboxPort.IsReadOnly = false;
            btnTest.IsEnabled = true;

            radioNumSockets.IsEnabled = true;
            radioSocketPerSensor.IsEnabled = true;
            tboxNumberSockets.IsReadOnly = false;

            radioSensor.IsEnabled = true;
            radioStreaming.IsEnabled = true;

            btnStop.IsEnabled = false;
            btnStart.IsEnabled = true;
        }

        private void Stop_Click(object sender, RoutedEventArgs e)
        {
            UpdateStatus("Stopping playback");
            if (_cancellationTokenSource != null && !_cancellationTokenSource.IsCancellationRequested)
            {
                _cancellationTokenSource.Cancel();
            }
        }

        public void UpdateStatus(string status)
        {
            if (tboxStatus.Dispatcher.CheckAccess())
            {
                if (_statusLines.Count > MaxStatusLineCount)
                {
                    while (_statusLines.Count > (MaxStatusLineCount * 0.75))
                    {
                        _statusLines.RemoveAt(0);
                    }

                    tboxStatus.Text = string.Concat(_statusLines);
                }

                string line = string.Format("{0}: {1}{2}", DateTime.Now.ToString("T"), status, Environment.NewLine);

                _statusLines.Add(line);
                tboxStatus.AppendText(line);
                tboxStatus.Focus();
                tboxStatus.CaretIndex = tboxStatus.Text.Length;
                tboxStatus.ScrollToEnd();
            }
            else
            {
                tboxStatus.Dispatcher.InvokeAsync(() => { UpdateStatus(status); });
            }
        }

        public void SetTotal(int total)
        {
            _total = total;
        }

        public void IncrementCompleted()
        {
            Interlocked.Increment(ref _completed);

            if (_total > 0)
            {
                int perc = Convert.ToInt32(((decimal)_completed / (decimal)_total) * 100);
                UpdateProgress(perc);
            }
        }

        private void UpdateProgress(int percentage)
        {
            if (progress.Dispatcher.CheckAccess())
            {
                progress.Value = percentage;
            }
            else
            {
                progress.Dispatcher.InvokeAsync(() => { UpdateProgress(percentage); });
            }
        }
    }
}
