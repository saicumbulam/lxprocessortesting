﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlashFileOrganizer
{
    public static class Logger
    {
        private static readonly bool _hasConsole;
        private const string ConsoleMessageFormat = "{0}-{1}: {2}";
        private const string DateFormat = "HH:mm:ss.fff";

        static Logger()
        {
            _hasConsole = Console.OpenStandardInput(1) != Stream.Null;
        }

        public static void Message(string format, params object[] args)
        {
            string msg = string.Format(format, args);
            ConsoleLog(msg, "Msg", null);
        }

        public static void Error(Exception ex, string format, params object[] args)
        {
            string msg = string.Format(format, args);
            ConsoleLog(msg, "Err", ex);
        }

        public static void Warning(Exception ex, string format, params object[] args)
        {
            string msg = string.Format(format, args);
            ConsoleLog(msg, "Wrn", ex);
        }

    
        private static void ConsoleLog(string msg, string type, Exception ex)
        {
            if (_hasConsole)
            {
                Console.WriteLine(ConsoleMessageFormat, DateTime.Now.ToString(DateFormat), type, msg);
                if (null != ex)
                {
                    Console.WriteLine("\\t" + ex.Message);
                }
            }
        }
    }
}
