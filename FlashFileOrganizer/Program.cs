﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Amazon;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;

namespace FlashFileOrganizer
{
    public class Program
    {
        private static void Main(string[] args)
        {
            if (args.Length > 3)
            {
                Program prog = new Program(args[0], args[1], args[2], args[3]);
                prog.Start();

                Console.WriteLine("Press \'q\' to quit the program.");
                while (Console.Read() != 'q') { }

                prog.Stop();
            }
        }
        
        private Task _eventLoop;
        private readonly DirectoryInfo _rootFolder;
        private readonly string _s3Bucket;
        private readonly string _filePattern;
        private readonly CancellationTokenSource _cancellationTokenSource;
        private readonly BlockingCollection<FileInfo> _files;
        private const string S3PathPattern = "/{year}/{month}/{day}/{hour}/{fileName}";
        private readonly AWSCredentials _awsCredentials;
        private readonly RegionEndpoint _awsRegion;
        private readonly string _pathPrefix;

        public Program(string root, string filePattern, string s3Bucket, string pathPrefix)
        {
            if (string.IsNullOrWhiteSpace(root) || !Directory.Exists(root))
            {
                throw new ArgumentException("Invalid root folder specified", root);
            }
            _rootFolder = new DirectoryInfo(root);

            if (string.IsNullOrWhiteSpace(filePattern))
            {
                throw new ArgumentNullException(filePattern);
            }
            _filePattern = filePattern;
            
            if (string.IsNullOrWhiteSpace(s3Bucket))
            {
                throw new ArgumentNullException(s3Bucket);
            }
            _s3Bucket = s3Bucket;
            
            if (string.IsNullOrWhiteSpace(pathPrefix))
            {
                throw new ArgumentNullException(pathPrefix);
            }
            _pathPrefix = pathPrefix;
            
            _files = new BlockingCollection<FileInfo>();
            _cancellationTokenSource = new CancellationTokenSource();

            _awsCredentials = new BasicAWSCredentials("AKIAICC5Y37YHWTR6B7Q", "A8nSaZg56v3tii+gAppNRCoah6YEq1B3uYbRNebn");
            _awsRegion = RegionEndpoint.USEast1;
        }

        private void Start()
        {
            _eventLoop = Task.Run(async () => { await OrganizeFiles(); }, _cancellationTokenSource.Token);
        }

        private void Stop()
        {
            _cancellationTokenSource.Cancel();
            _cancellationTokenSource.Dispose();
            _files.Dispose();
        }

        private async Task OrganizeFiles()
        {
            //await Task.Delay(TimeSpan.FromMinutes(1), _cancellationTokenSource.Token);

            Task t1 = Task.Run(() => FindFiles());
            Task t2 = ProcessFiles();

            await Task.WhenAll(t1, t2);

            Logger.Message("Finished");
        }

        private void FindFiles()
        {
            CrawlFolder(_rootFolder);
            _files.CompleteAdding();
        }

        private void CrawlFolder(DirectoryInfo folder)
        {
            if (folder.Exists)
            {
                FileInfo[] files = null;
                try
                {
                    files = folder.GetFiles(_filePattern, SearchOption.TopDirectoryOnly);
                }
                catch (Exception ex)
                {
                    Logger.Error(ex, string.Empty);
                }

                if (files != null && files.Length > 0)
                {
                    foreach (FileInfo file in files)
                    {
                        _files.Add(file);
                    }
                }
                
                DirectoryInfo[] subFolders = null;
                try
                {
                    subFolders = folder.GetDirectories();
                }
                catch (Exception ex)
                {
                    Logger.Error(ex, string.Format("Unable to find folders in directory: {0}", folder.FullName));
                }

                if (subFolders != null && subFolders.Length > 0)
                {
                    foreach (DirectoryInfo subFolder in subFolders)
                    {
                        CrawlFolder(subFolder);
                        if (_cancellationTokenSource.IsCancellationRequested) break;
                    }
                }
            }
        }

        private async Task ProcessFiles()
        {
            await _files.GetConsumingEnumerable().ForEachAsync(
                Environment.ProcessorCount * 4,
                ProcessFile,
                _cancellationTokenSource.Token);
        }

        private async Task ProcessFile(FileInfo file)
        {
            DateTime fileTime = GetFileTimeFromName(file.Name, _filePattern);
            string fileName = NormalizeFileName(file.Name);
            Logger.Message(fileTime.ToString());

            try
            {
                using (Stream fileStream = file.OpenRead())
                {
                    using (MemoryStream compressedMemoryStream = new MemoryStream())
                    {
                        using (GZipStream compressionStream = new GZipStream(compressedMemoryStream, CompressionMode.Compress, true))
                        {
                            await fileStream.CopyToAsync(compressionStream);
                            compressionStream.Flush();
                        }

                        compressedMemoryStream.Position = 0;

                        string key = $"{_pathPrefix}/{fileTime.ToString("yyyy/MM/dd/HH")}/{fileName}";
                        
                        var req = new PutObjectRequest
                        {
                            BucketName = _s3Bucket,
                            Key = key,
                            InputStream = compressedMemoryStream
                        };

                        try
                        {
                            using (IAmazonS3 client = new AmazonS3Client(_awsCredentials, _awsRegion))
                            {
                                await client.PutObjectAsync(req, _cancellationTokenSource.Token);
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(ex, "Error uploading file to S3 {0}/{1}", req.BucketName, req.Key);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                Logger.Error(ex, "Error processing file", file.FullName);
            }
        }

        private string NormalizeFileName(string name)
        {
            string normal = name.ToLower();
            normal = normal.Replace(' ', '-');
            normal = normal + ".gz";
            return normal;
        }

        private DateTime GetFileTimeFromName(string fileName, string filePattern)
        {
            DateTime fileTime = DateTime.MinValue;
            int start = filePattern.IndexOf('*');
            if (start >= 0)
            {
                string left = filePattern.Substring(0, start);
                string right = filePattern.Substring(start + 1);
                string date = fileName;

                if (left.Length > 0)
                {
                    date = fileName.Replace(left, string.Empty);
                }

                if (right.Length > 0)
                {
                    date = date.Replace(right, string.Empty);
                }

                int index = date.IndexOf("T");
                if (index > 0)
                {
                    index = date.IndexOf("-", index);
                    if (index > 0)
                    {
                        date = date.Remove(index, 1).Insert(index, ":");
                    }
                }

                if (DateTime.TryParse(date, out fileTime))
                {
                    fileTime = DateTime.SpecifyKind(fileTime, DateTimeKind.Utc);
                }
            }

            return fileTime;
        }
    }

    public static class ParallelAsync
    {
        public static Task ForEachAsync<T>(this IEnumerable<T> source, int dop, Func<T, Task> body, CancellationToken cancellationToken = default(CancellationToken))
        {
            return Task.WhenAll(
                Partitioner.Create(source).GetPartitions(dop).Select(
                    partition => Task.Run(
                        async () =>
                        {
                            using (partition)
                            {
                                while (partition.MoveNext())
                                {
                                    await body(partition.Current);

                                    if (cancellationToken.IsCancellationRequested)
                                    {
                                        break;
                                    }
                                }
                            }
                        }, cancellationToken)
                ));
        }

    }
}
