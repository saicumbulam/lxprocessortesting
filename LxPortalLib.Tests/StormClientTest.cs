﻿using LxPortalLib.Api;
using LxPortalLib.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace LxPortalLib.Tests
{
    
    
    [TestClass()]
    public class StormClientTest
    {


        private TestContext testContextInstance;

        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for GetBoxStormData
        ///</summary>
        [TestMethod()]
        public void GetBoxStormDataTest()
        {
            DateTime start;
            DateTime.TryParse("2011-08-05 13:50:00.000", out start);
            DateTime end;
            DateTime.TryParse("2011-08-06 23:00:00.000", out end);
            Decimal ullat = 46;
            Decimal ullon = -103;
            Decimal lrlat = 38;
            Decimal lrlon = -89;

            ResponseContainer<List<StormRecord>> actual;
            actual = StormClient.GetBoxStormData(start, end, ullat, ullon, lrlat, lrlon);
            if (actual != null)
            {
                Assert.IsTrue(actual != null, "sensor id:" + actual.r.ToString());
            }
            else
            {
                Assert.IsTrue(actual == null, "check internal server if down");
            }
        }
    }
}
