﻿using LxPortalLib.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace LxPortalLib.Tests
{
    [TestClass()]
    public class UtilitiesTest
    {
        [TestMethod()]
        public void GetSensorStatusNoLastCallHomeUtcTest()
        {
            Nullable<DateTime> lastCallHomeUtc = null;
            Nullable<int> packetRate = null;
            DateTime currentUtcTime = new DateTime(2012, 10, 9);
            int packetRateThreshold = 75;
            int callHomeThresholdMinutes = 120;
            int expected = 0;
            int actual;
            bool sensorEnabled = false; 
            actual = Utilities.GetSensorStatus(lastCallHomeUtc, packetRate, sensorEnabled, currentUtcTime, packetRateThreshold, callHomeThresholdMinutes);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void GetSensorStatusValidTest()
        {
            DateTime currentUtcTime = new DateTime(2012, 10, 9);
            Nullable<DateTime> lastCallHomeUtc = currentUtcTime.AddMinutes(-45);
            int packetRateThreshold = 75;
            Nullable<int> packetRate = packetRateThreshold + 5;
            int callHomeThresholdMinutes = 120;
            int expected = 1;
            int actual; 
            bool sensorEnabled = false; 
            actual = Utilities.GetSensorStatus(lastCallHomeUtc, packetRate,sensorEnabled, currentUtcTime, packetRateThreshold, callHomeThresholdMinutes);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void GetSensorStatusValidNoPacketRateTest()
        {
            DateTime currentUtcTime = new DateTime(2012, 10, 9);
            Nullable<DateTime> lastCallHomeUtc = currentUtcTime.AddMinutes(-45);
            int packetRateThreshold = 75;
            Nullable<int> packetRate = null;
            int callHomeThresholdMinutes = 120;
            int expected = 3;
            int actual;
            bool sensorEnabled = false;
            actual = Utilities.GetSensorStatus(lastCallHomeUtc, packetRate, sensorEnabled, currentUtcTime, packetRateThreshold, callHomeThresholdMinutes);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void GetSensorStatusOldCallHomeTest()
        {
            DateTime currentUtcTime = new DateTime(2012, 10, 9);
            int callHomeThresholdMinutes = 120;
            Nullable<DateTime> lastCallHomeUtc = currentUtcTime.AddMinutes(-1 * (callHomeThresholdMinutes + 1));
            int packetRateThreshold = 75;
            Nullable<int> packetRate = null;
            
            int expected = 4;
            int actual;
            bool sensorEnabled = false; 
            actual = Utilities.GetSensorStatus(lastCallHomeUtc, packetRate,sensorEnabled,  currentUtcTime, packetRateThreshold, callHomeThresholdMinutes);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void GetDateKeyTest0()
        {
            DateTime date = new DateTime();
            DateTime.TryParse("2012-09-24 03:00", out date);
            long expected = 20120924031;
            long actual;

            actual = Utilities.GetDateKey(date);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void GetDateKeyTest1()
        {
            DateTime date = new DateTime();
            DateTime.TryParse("2012-11-01 15:33", out date);
            long expected = 20121101153;
            long actual;
            actual = Utilities.GetDateKey(date);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void GetDateKeyTest2()
        {
            DateTime date = new DateTime();
            DateTime.TryParse("2012-1-1 15:15", out date);
            long expected = 20120101152;
            long actual;
            actual = Utilities.GetDateKey(date);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void GetDateKeyTest3()
        {
            DateTime date = new DateTime();
            DateTime.TryParse("2012-1-1 00:59", out date);
            long expected = 20120101004;
            long actual;
            actual = Utilities.GetDateKey(date);
            Assert.AreEqual(expected, actual);
        }
    }
}
