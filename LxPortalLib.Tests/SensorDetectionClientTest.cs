﻿using LxPortalLib.Api;
using LxPortalLib.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace LxPortalLib.Tests
{
    
    
    [TestClass()]
    public class SensorDetectionClientTest
    {


        private TestContext testContextInstance;

        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        //1. test for Get method in SensorDetectionClient with given id and time 
        [TestMethod()]
        public void GetTest()
        {
            string id = "IAF0003";
            SensorDetection actual; 
            actual = SensorDetectionClient.Get(id, Convert.ToDateTime("2012-10-04 00:00:00.000"), Convert.ToDateTime("2012-10-04 23:59:59.999"));
            if (actual != null)
            {
                Assert.IsTrue(actual != null, "values returned:" + actual.ToString());
            }
            else
            {
                Assert.IsTrue(actual == null, "check internal server error - service unavailable?");
            }

            //2. test for Get method in SensorDetectionClient with null start/end value 
            actual = SensorDetectionClient.Get(id, null, null);
            if (actual != null)
            {
                Assert.IsTrue(actual != null, "values returned:" + actual.ToString());
            }
            else
            {
                Assert.IsTrue(actual == null, "check internal server error - service unavailable?");
            }

            //3. test for Get method in SensorDetectionClient with null start value but no end value  
            actual = SensorDetectionClient.Get(id, Convert.ToDateTime("2012-10-04 00:00:00.000"), null);
            if (actual != null)
            {
                Assert.IsTrue(actual != null, "values returned:" + actual.ToString());
            }
            else
            {
                Assert.IsTrue(actual == null, "check internal server error - service unavailable?");
            }
        }
    }
}
