﻿using LxPortalLib.Api;
using LxPortalLib.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace LxPortalLib.Tests
{
    [TestClass()]
    public class SensorPacketClientTest
    {
        [TestMethod()]
        public void GetTest()
        {
            string id = "IAF0003";

            Nullable<DateTime> start1 = new Nullable<DateTime>();
            start1 = new DateTime(2011, 04, 17);
            Nullable<DateTime> end1 = new Nullable<DateTime>();
            end1 = new DateTime(2011, 04, 18);
            Nullable<DateTime> start2 = new Nullable<DateTime>();
            start2 = new DateTime(2011, 05, 29, 22, 0, 0);
            Nullable<DateTime> end2 = new Nullable<DateTime>();
            end2 = new DateTime(2011, 05, 29, 23, 20, 0);


            List<PacketRecord> actual = SensorPacketClient.Get(id, null, null);
            if (actual != null)
            {
                Assert.IsTrue(actual.Count >= 0);
            }
            else
            {
                Assert.IsTrue(actual == null, "check internal server - service might be down");
            }


            //2. check to see datetime formatting 
            List<PacketRecord> actual1 = SensorPacketClient.Get(id, start1, end1);
            if (actual1 != null)
            {
                Assert.IsTrue(actual1.Count == 0);
            }
            else
            {
                Assert.IsTrue(actual1 == null, "check internal server - service might be down");
            }

            //3. change the datetime formatting to more granular 
            List<PacketRecord> actual2 = SensorPacketClient.Get(id, start2, end2);
            if (actual2 != null)
            {
                Assert.IsTrue(actual2 != null);
            }
            else
            {
                Assert.IsTrue(actual2 == null, "check internal server - service might be down");
            }

        }

    }
}
