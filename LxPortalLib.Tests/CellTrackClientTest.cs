﻿using LxPortalLib.Api;
using LxPortalLib.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace LxPortalLib.Tests
{
    
    
    [TestClass()]
    public class CellTrackClientTest
    {


        private TestContext testContextInstance;

        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion



        //A test for GetBoxCellTrack
        [TestMethod()]
        public void GetBoxCellTrackTest()
        {
            DateTime start = new DateTime(2013, 01, 30, 02, 11, 00, 0);
            DateTime end = new DateTime(2013, 01, 30, 03, 11, 00, 0);
            Decimal ullat = 51;
            Decimal ullon = -153;
            Decimal lrlat = -41;
            Decimal lrlon = -16;
            ResponseContainer<List<CellTrackRecord>> actual;
            actual = CellTrackClient.GetBoxCellTrack(start, end, ullat, ullon, lrlat, lrlon);
            if (actual != null)
            {
                Assert.IsTrue(actual != null, "values are returned: " + actual.r.ToString());
            }
            else
            {
                Assert.IsTrue(actual == null, "failed to return call, check website is running ");
            }

        }
    }
}
