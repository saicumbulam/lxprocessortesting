﻿using LxPortalLib.Api;
using LxPortalLib.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace LxPortalLib.Tests
{
    
    
    /// <summary>
    ///This is a test class for CellAlertClientTest and is intended
    ///to contain all CellAlertClientTest Unit Tests
    ///</summary>
    [TestClass()]
    public class CellAlertClientTest
    {


        private TestContext testContextInstance;


        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion



        //A test for GetBoxCellAlert
        //check that you get all the parameters back 
        //check the bbox output 

        [TestMethod()]
        public void GetBoxCellAlertTest()
        {
            DateTime start = new DateTime(2013,01,30,02,00,00,0);
            DateTime end = new DateTime(2013, 01, 30, 04, 00, 00, 0);
            string alertTypeList = "3"; 
            Decimal ullat =  34; 
            Decimal ullon = -92; 
            Decimal lrlat =  32; 
            Decimal lrlon = -90; 
            ResponseContainer<List<CellAlertRecord>> actual;

            actual = CellAlertClient.GetBoxCellAlert(start, end, alertTypeList, ullat, ullon, lrlat, lrlon);
            if (actual != null)
            {
                Assert.IsTrue(actual != null, "values are returned: " + actual.r.ToString());
            }
            else
            {
                Assert.IsTrue(actual == null, "failed to return call, check website is running " );
            }

        }
    }
}
