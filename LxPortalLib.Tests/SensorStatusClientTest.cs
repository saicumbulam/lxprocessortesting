﻿using LxPortalLib.Api;
using LxPortalLib.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LxPortalLib.Tests
{
    [TestClass()]
    public class SensorStatusClientTest
    {
        [TestMethod()]
        public void GetTest()
        {
            string id = "IAF0003";
            CurrentSensorStatus actual = SensorStatusClient.Get(id);
            if (actual != null)
            {
                Assert.IsTrue(actual != null, "sensor id:" + actual.Id);
            }
            else
            {
                Assert.IsTrue(actual == null, "check internal server if down");
            }
        }
    }
}
