﻿using LxPortalLib.Api;
using LxPortalLib.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace LxPortalLib.Tests
{
    
    [TestClass()]
    public class DetectionReportClientTest
    {


        private TestContext testContextInstance;

        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        [TestMethod()]
        public void GetDetectionsTest()
        {

            DateTime start = new DateTime(2012, 11, 13, 13, 0, 0); 
            start = DateTime.SpecifyKind(start, DateTimeKind.Utc);
            DateTime end = new DateTime(2012, 11, 14, 1, 15, 0);
            end = DateTime.SpecifyKind(end, DateTimeKind.Utc);

            string type = "ic"; //type 1
            LayerType layer = LayerType.Flash;
            string version = "base"; //base address 


            Decimal lat = (decimal)27.8;
            Decimal lon = (decimal)-86;
            int radius = 50000;
            

            //valid for the intracloud call or type 1
            Decimal ullat = (decimal) 27.9;
            Decimal ullon = (decimal) -86.2;
            Decimal lrlat = (decimal) 26.8;
            Decimal lrlon = (decimal) -85.2;


            ResponseContainer<List<FlashRecord>> actualCircle;
            ResponseContainer<List<FlashRecord>> actualBox;


            actualCircle = DetectionReportClient.GetCircleDetections(start, end, type, lat, lon, radius, layer, version);
            if (actualCircle != null)
            {
                Assert.IsTrue(actualCircle != null, "values returned" + actualCircle.ToString());
            }
            else
            {
                Assert.IsTrue(actualCircle == null, "internal server error, check service?");
            }

            actualBox = DetectionReportClient.GetBoxDetections(start, end, type, ullat, ullon, lrlat, lrlon, layer, version);
            if (actualBox != null)
            {
                Assert.IsTrue(actualBox != null, "values returned" + actualBox.ToString());
            }
            else
            {
                Assert.IsTrue(actualBox == null, "internal server error, check service?");
            }


        }
    }















}
