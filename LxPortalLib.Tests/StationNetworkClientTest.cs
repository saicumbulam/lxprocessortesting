﻿using LxPortalLib.Api;
using LxPortalLib.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace LxPortalLib.Tests
{
    [TestClass()]
    public class StationNetworkClientTest
    {
        [TestMethod()]
        public void GetTest()
        {
            ResponseContainer<List<StationNetwork>> actual = StationNetworkClient.GetStationNetworks();
            if (actual != null)
            {
                Assert.IsTrue(actual.r != null, "check internal server if down");
            }
            else
            {
                Assert.IsTrue(actual == null, "check internal server if down");
            }
        }
    }
}
