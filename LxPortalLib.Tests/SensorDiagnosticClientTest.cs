﻿using LxPortalLib.Api;
using LxPortalLib.Common;
using LxPortalLib.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace LxPortalLib.Tests
{
    [TestClass]
    public class SensorDiagnosticClientTest
    {
        [TestMethod]
        public void SensorDiagnosticListGetTest()
        {
            string id = "ADWLL";
            SensorDiagnostics actual;
            actual = SensorDiagnosticClient.Get(id, null, null);

            if (actual != null)
            {
                Assert.IsTrue(actual != null, "Sensor Diagnostic (Log) cannot get data without date params");
                Assert.IsTrue(actual.Id == id, "Sensor Diagnostic (Log) cannot get data without date params");    
            }
            else
            {
                Assert.IsTrue(actual == null, "Http Response status code must've been thrown");
            }
                
            actual = SensorDiagnosticClient.Get(id, Utilities.NormalizeDateTimeToUtc(DateTime.UtcNow.AddDays(-2)), Utilities.NormalizeDateTimeToUtc(DateTime.UtcNow));

            if(actual != null)
            {
                Assert.IsTrue(actual != null, "Sensor Diagnostic (Log) cannot get data with date params");
                Assert.IsTrue(actual.Id == id, "Sensor Diagnostic (Log) cannot get data with date params");
            }
            else
            {
                Assert.IsTrue(actual == null, "Simulate service is down");
            }

        }
    }
}
