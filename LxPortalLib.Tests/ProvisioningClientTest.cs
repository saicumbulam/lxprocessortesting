﻿using System.Collections.Generic;
using LxPortalLib.Api;
using LxPortalLib.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LxPortalLib.Tests
{
    [TestClass()]
    public class ProvisioningClientTest
    {
        [TestMethod()]
        public void GetAllTest()
        {
            ResponseContainer<List<ProvisioningPartner>> actual = ProvisioningClient.GetAll(Provisioning.FeedType.LxDetectionFeed);
            if (actual != null)
            {
                Assert.IsTrue(actual.r != null, "check internal server if down");
            }
            else
            {
                Assert.IsTrue(actual == null, "check internal server if down");
            }
        }

        [TestMethod()]
        public void GetTest()
        {
            string id = "01AFD62A-60C9-42CE-A187-8F347F0BCEF3";
            ResponseContainer<ProvisioningPartner> actual = ProvisioningClient.Get(id);
            if (actual != null)
            {
                Assert.IsTrue(actual.r != null, "check internal server if down");
            }
            else
            {
                Assert.IsTrue(actual == null, "check internal server if down");
            }
        }
    }
}
