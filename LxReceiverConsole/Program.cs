﻿using System;

namespace LxReceiverConsole
{
    public class Program
    {
        private static void Main(string[] args)
        {
            LxReceiver.Program.Start();

            Console.WriteLine("Press \'q\' to quit the program.");
            while (Console.Read() != 'q') { }

            LxReceiver.Program.Stop();
        }
    }
}
