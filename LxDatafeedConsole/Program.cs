﻿using System;

namespace LxDatafeedConsole
{
    public class Program
    {
        static void Main(string[] args)
        {
            LxDatafeed.Program.Start();

            Console.WriteLine("Press \'q\' to quit the program.");
            while (Console.Read() != 'q') { }

            LxDatafeed.Program.Stop();
        }
    }
}
