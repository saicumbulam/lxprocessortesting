﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using LxPortalLib.Api;
using LxPortalLib.Models;

namespace LightningAnalysis
{
    /// <summary>
    /// Summary description for ecv
    /// </summary>
    public class Ecv : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                string type = context.Request.QueryString["type"].ToLower();
                if (type == "browser")
                {
                    ConstructEcvForBrowser(context);
                }
                else if (type == "prtg")
                {

                }
                else if (type == "elb")
                {

                }

            }
            catch (Exception exp)
            {
                context.Response.ContentType = "text/html";
                context.Response.StatusCode = (int)HttpStatusCode.OK;

                string html = File.ReadAllText(Path.GetDirectoryName(context.Request.PhysicalPath) + @"/Monitor.html", Encoding.UTF8);
                html = string.Format(html, DateTime.UtcNow.ToString(CultureInfo.InvariantCulture), "F", "FAIL", "F", "FAIL");
                context.Response.Write(html);
                context.Response.Write("<br/>Internal Exception logged: <br/>");
                context.Response.Write(exp.ToString());
                context.Response.Flush();
            }
        }

        private void ConstructEcvForBrowser(HttpContext context)
        {
            context.Response.ContentType = "text/html";
            context.Response.StatusCode = (int)HttpStatusCode.OK;

            string html = File.ReadAllText(Path.GetDirectoryName(context.Request.PhysicalPath) + @"/Monitor.html", Encoding.UTF8);
            bool canAccessCtaFile = TestCellTrackerFileAccess();
            bool canAccessLxPortalApi = TestLxPortalApiAccess();
            html = string.Format(html, DateTime.UtcNow.ToString(CultureInfo.InvariantCulture),
                                 canAccessCtaFile ? "S" : "F", //1 
                                 canAccessCtaFile ? "SUCCESS" : "FAILURE", //2
                                 canAccessLxPortalApi ? "S" : "F", //3 
                                 canAccessLxPortalApi ? "SUCCESS" : "FAILURE" //4
                );
            context.Response.Write(html);
            context.Response.Flush();
        }

        /// <summary>
        /// Testing access to the remote cell tracker file storage using timestamp DateTime.UtcNow.AddDay(-1)
        /// </summary>
        /// <returns>
        /// Return True if the directory can be found. Return False if the directory cannot be found.
        /// </returns>
        private bool TestCellTrackerFileAccess()
        {
            try
            {
                DateTime timeStamp = DateTime.UtcNow.AddDays(-1);

                string archivedPath = ConfigurationManager.AppSettings["ArchievedPath"];
                string archivedFilename = ConfigurationManager.AppSettings["ArchievedFilename"];

                string fname = string.Format("{0}\\{1}\\{2}_{3}*.*",
                                             archivedPath, timeStamp.ToString("yyyy/M/d").Replace("/", "\\"),
                                             archivedFilename, timeStamp.ToString("yyyyMMdd"));

                string dirPath = Path.GetDirectoryName(fname);
                return dirPath != null && Directory.Exists(dirPath);

            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Testing access to Lx Portal API
        /// </summary>
        /// <returns>
        /// Return True if API returns normally. Return False if the return object is null.
        /// </returns>
        private bool TestLxPortalApiAccess()
        {
            try
            {
                DateTime timsStamp = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, 0, 0,
                                                  0);
                List<FlashRecord> flashes = DetectionReportClient.GetBoxDetections(timsStamp, timsStamp.AddMinutes(15), "both", 80, 80,
                                                                                   79, 81, LayerType.Flash, null).r;
                return flashes != null;

            }
            catch
            {
                return false;
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}