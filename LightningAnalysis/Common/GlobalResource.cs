﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using System.Configuration;

namespace LightningAnalysis.Common
{
	public static class GlobalResource
	{	
	    public static string GetResourceValue(string resourcekey)
		{
			string resourcevalue = string.Empty;
			try
			{
				if (!string.IsNullOrEmpty(resourcekey))
				{
					string sCultureCookie = string.Empty;
					if (HttpContext.Current.Request.Cookies["CultureCookie"] != null)
					{
						sCultureCookie = HttpContext.Current.Request.Cookies["CultureCookie"].Value;
					}
					if (string.IsNullOrEmpty(sCultureCookie) && (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["DefaultCulture"])))
					{
						sCultureCookie = ConfigurationManager.AppSettings["DefaultCulture"];
					}
					if (!string.IsNullOrEmpty(sCultureCookie))
					{
						resourcevalue = (string)HttpContext.GetGlobalResourceObject("LightningResource", resourcekey, new System.Globalization.CultureInfo(sCultureCookie));
					}
				}
				return string.IsNullOrEmpty(resourcevalue) ? resourcekey : resourcevalue.Replace("'", "\'");

			}
			catch (Exception ex)
			{
				Aws.Core.Utilities.EventManager.LogError("Exception in getting the resource value" + ex);
				System.Diagnostics.Debug.WriteLine("Exception in getting the resource value" + ex);
				return resourcekey;
			}
		}

		public static string UserDateFormat
		{
			get
			{
				string _userdateformat = string.Empty;
				if (HttpContext.Current.Request.Cookies["UserDateFormat"] != null)
				{
					_userdateformat = HttpContext.Current.Request.Cookies["UserDateFormat"].Value;
				}
				if (string.IsNullOrEmpty(_userdateformat))
				{

					HttpCookie culturecookie = HttpContext.Current.Request.Cookies["CultureCookie"];
					if (culturecookie != null && culturecookie.Value == "en-US")
					{
						_userdateformat = "MM/dd/yyyy";
					}
					else if (culturecookie != null && culturecookie.Value == "es-ES")
					{
						_userdateformat = "dd/MM/yyyy";
					}
					if (culturecookie == null)
					{
						string _sdefaultculture = ConfigurationManager.AppSettings["DefaultCulture"];
						if (!string.IsNullOrEmpty(_sdefaultculture) && _sdefaultculture == "en-US")
						{
							_userdateformat = "MM/dd/yyyy";
						}
						else
						{
							_userdateformat = "dd/MM/yyyy";
						}
					}
				}
                if (_userdateformat == "dd/MM/yyyy" && ConfigurationManager.AppSettings["DefaultCulture"] == "en-US")
                {
                    _userdateformat = "MM/dd/yyyy";
                }

				return _userdateformat;
			}
		}

		public static string GetUserCulture()
		{
			string sCultureCookie = string.Empty;
			if (HttpContext.Current.Request.Cookies["CultureCookie"] != null)
			{
				sCultureCookie = HttpContext.Current.Request.Cookies["CultureCookie"].Value;
			}
			if (string.IsNullOrEmpty(sCultureCookie) && (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["DefaultCulture"])))
			{
				sCultureCookie = ConfigurationManager.AppSettings["DefaultCulture"];
			}
			return sCultureCookie;
		}


		
	}
}