﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CalendarPopUp.aspx.cs" Inherits="LightningAnalysis.CalendarPopUp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<head runat="server">
    <title><%=LightningAnalysis.Common.GlobalResource.GetResourceValue("CalendarPopup")%></title>
</head>
<body >
 <script type="text/javascript" src='<%= ConfigurationManager.AppSettings["JQueryURL"] %>/js/jquery-1.3.2.min.js' ></script>
 <script type="text/javascript" src='<%= ConfigurationManager.AppSettings["JQueryURL"] %>/development-bundle/ui/ui.core.js' ></script>
<script type="text/javascript">
    function calenderGoToTooTip() {
        
        var prev = "Go to the previous month";
        var nxt = "Go to the next month";

        $("td.calenderNextPrevLink a").each(function (i, e) {
            if (e.title == prev) {
                e.title = '<%=LightningAnalysis.Common.GlobalResource.GetResourceValue("Gotothepreviousmonth")%>';
            }
            if (e.title == nxt) {
                e.title = '<%=LightningAnalysis.Common.GlobalResource.GetResourceValue("Gotothenextmonth")%>';
            }
        });
    }

    function getCookie(cookieName) {
        if (document.cookie.length > 0) {
            var cookieStart = document.cookie.indexOf(cookieName + "=")
            if (cookieStart != -1) {
                cookieStart = cookieStart + cookieName.length + 1
                cookieEnd = document.cookie.indexOf(";", cookieStart)
                if (cookieEnd == -1)
                    cookieEnd = document.cookie.length
                return unescape(document.cookie.substring(cookieStart, cookieEnd))
            }
        }
        return ""
    } 

</script>
    <form id="form1" runat="server">
    <div style="margin:2px 2px 2px 2px">
        <asp:Calendar runat="server" BackColor="#FFFFCC" BorderColor="#FFCC66" ID="Calendar1"
            BorderWidth="1px" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt"
            ForeColor="#663399" Height="200px" ShowGridLines="True" Width="220px" OnDayRender="Calendar1_DayRender" NextPrevStyle-CssClass="calenderNextPrevLink">
            <SelectedDayStyle BackColor="#CCCCFF" Font-Bold="True" />
            <SelectorStyle BackColor="#FFCC66" />
            <TodayDayStyle BackColor="#FFCC66" ForeColor="White" />
            <OtherMonthDayStyle ForeColor="#CC9966" />
            <NextPrevStyle Font-Size="9pt" ForeColor="#FFFFCC"  CssClass="calenderNextPrevLink" />
            <DayHeaderStyle BackColor="#FFCC66" Font-Bold="True" Height="1px" />
            <TitleStyle BackColor="#990000" Font-Bold="True" Font-Size="9pt" 
                ForeColor="#FFFFCC"  Wrap="False"/>
        </asp:Calendar>
    </div>
    </form>
</body>
</html>
