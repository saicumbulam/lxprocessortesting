﻿using System;
using System.Collections;
using System.Web;
using System.Web.Services;

using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.IO;
using Aws.Core.TimeZoneServiceRef;
using AwsGeometries;
using System.Xml;
using System.Data.SqlClient;
using System.Globalization;
using ICSharpCode.SharpZipLib.Zip;
using LightningAnalysis.Common;
using LxPortalLib.Api;
using LxPortalLib.Models;


namespace LightningAnalysis
{
	/// <summary>
	/// Summary description for $codebehindclassname$
	/// </summary>
	[WebService(Namespace = "http://tempuri.org/")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	public class LightningHandler : IHttpHandler
	{

		public void ProcessRequest(HttpContext context)
		{
			string startdate = string.Empty;
			string starttime = string.Empty;
			string enddate = string.Empty;
			string endtime = string.Empty;
			string stype = string.Empty;
			IFormatProvider formatprovider = null;

			try
			{
			
				string strKml = string.Empty;
				
				startdate = context.Request.Params["startdate"];
				starttime = context.Request.Params["starttime"];
				enddate = context.Request.Params["enddate"];
				endtime = context.Request.Params["endtime"];
				stype = context.Request.Params["type"]; 				

				//IFormatProvider formatprovider = new CultureInfo(userCulture);				

				string userDateFormat = Common.GlobalResource.UserDateFormat;

				if (userDateFormat == "dd/MM/yyyy")
				{
					formatprovider = new CultureInfo("es-ES");
				}
				else
				{
					formatprovider = new CultureInfo("en-US");
				}

				
				if (string.IsNullOrEmpty(startdate) && string.IsNullOrEmpty(starttime) && string.IsNullOrEmpty(enddate) && string.IsNullOrEmpty(endtime))
				{
					Aws.Core.Utilities.EventManager.LogError("Input params are null or empty ");				
					return;
				}
				DateTime sdate = DateTime.Parse(startdate + " " + starttime, formatprovider);
				DateTime edate = DateTime.Parse(enddate + " " + endtime, formatprovider);
				
                context.Response.ContentType = "application/kmz";
                context.Response.AppendHeader("Content-Encoding", "kmz");

                if (stype == "Radar")
                {
                    context.Response.AppendHeader("Content-Disposition", "filename=\"RadarOverlay.kmz\"");
                    WriteRadarKmzFile(context.Response.OutputStream, sdate, edate);
                    return;
                }

                if (stype == "NWSAlerts")
                {
                    context.Response.AppendHeader("Content-Disposition", "filename=\"NWSAlerts.kmz\"");
                    WriteNWSAlertsKmzFile(context.Response.OutputStream, sdate, edate);
                    return;
                }

                if (stype == "StormReports")
                {
                    context.Response.AppendHeader("Content-Disposition", "filename=\"LocalStormReports.kmz\"");
                    WriteStormReportsKmzFile(context.Response.OutputStream, sdate, edate);
                    return;
                }
                if (stype == "LtgFlashFrame")
                {
                    context.Response.AppendHeader("Content-Disposition", "filename=\"LtgFlashTemplate.kmz\"");
                    WriteLightningFlashKmzFileTemplate(context.Response.OutputStream, sdate, edate);
                    return;
                }
                if (stype == "LtgFlash")
                {
					string sDownloadType =context.Request.Params["downloadtype"];
					if(string.IsNullOrEmpty(sDownloadType))
					{
						context.Response.AppendHeader("Content-Disposition", "filename=\"LtgFlash" + sdate.ToString("yyyy-MM-dd-HH-mm") + ".kmz\"");
						WriteLightningFlashKmzFile(context.Response.OutputStream, sdate, edate);
					}
					else
					{
						context.Response.AppendHeader("Content-Disposition", "filename=\"LtgFlash" + sdate.ToString("yyyy-MM-dd-HH-mm") + ".csv\"");
						WriteLtgFlashCSV(context.Response.OutputStream, sdate, edate);
					}
                    return;
                }//stdy tool
                if (stype == "LtgPulses")
                {
                    string sDownloadType = context.Request.Params["downloadtype"];
                    if (string.IsNullOrEmpty(sDownloadType))
                    {
                        context.Response.AppendHeader("Content-Disposition", "filename=\"LtgPulses" + sdate.ToString("yyyy-MM-dd-HH-mm") + ".kmz\"");
                        WriteLightningPulseKmzFile(context.Response.OutputStream, sdate, edate);
                    }
                    else
                    {
                        context.Response.AppendHeader("Content-Disposition", "filename=\"LtgPulses" + sdate.ToString("yyyy-MM-dd-HH-mm") + ".csv\"");
                        WriteLtgPulseDataCsv(context.Response.OutputStream, sdate, edate);
                    }
                    return;
                }
			    if (!string.IsNullOrEmpty(startdate) && !string.IsNullOrEmpty(starttime) && !string.IsNullOrEmpty(enddate) && !string.IsNullOrEmpty(endtime)&& !string.IsNullOrEmpty(stype))
				{
					//2010/04/27/filename
					
					
				    string cacheKey = sdate.Ticks + "_" + edate.Ticks + "_" + stype;				    
				    List<IGeometry> geometries = LightningArchivedManager.GetArchiveGeometryList(cacheKey,stype);
                   // context.Response.ContentType = "application/vnd.google-earth.kml+xml";
					context.Response.AppendHeader("Content-Disposition", "filename=\"Lightning Download.kmz\"");
										
					
					long mStartTick = Environment.TickCount;
				    ParseFilesEx(geometries, context.Response.OutputStream, cacheKey,stype,edate);
					System.Diagnostics.Debug.WriteLine("Time to create kml: " + (Environment.TickCount - mStartTick).ToString() + "msec");
				    return;					
				}
			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.WriteLine("Exception Occured in ProcessRequest: " + ex + " input params startdt:" + startdate + " " + starttime + "," + formatprovider + ",enddt:" + enddate + " " + endtime + "," + formatprovider + ",type " + stype);
				Aws.Core.Utilities.EventManager.LogError("Exception Occured in ProcessRequest: " + ex);
				Aws.Core.Utilities.EventManager.LogError("Input params startdt:" + startdate + " " + starttime + "," + formatprovider + ",enddt:" + enddate + " " + endtime + "," + formatprovider + ",type " + stype);
			}	
		
		}

        private static void WriteLightningFlashKmzFileTemplate(Stream outputStream, DateTime startTime, DateTime endTime)
        {
            string strWebUrl = ConfigurationManager.AppSettings["WebUrl"];
            
            
            MemoryStream memStream = new MemoryStream();
            XmlTextWriter writer = new XmlTextWriter(memStream, null) { Indentation = 4 };

            WriteDocumentHeader(writer);

            DateTime sTime = startTime;
            do
            {
                DateTime eTime = sTime.AddMinutes(10.0);
                string querystring = "startdate=" + sTime.ToString("MM/dd/yyyy") + "&amp;starttime=" + sTime.ToString("HH:mm") + "&amp;enddate=" + eTime.ToString("MM/dd/yyyy") + "&amp;endtime=" + eTime.ToString("HH:mm");
                string timespan = "<TimeSpan>" +
                                 "<begin>" + sTime.ToString("yyyy-MM-ddTHH:mm:00Z") + "</begin>" +
                                 "<end>" + eTime.ToString("yyyy-MM-ddTHH:mm:00Z") + "</end>" +
                                 "</TimeSpan>";

                string strkml = "<NetworkLink>" +
                                "<name>" + sTime.ToString("MM/dd/yyyy HH:mm UTC") + "</name>" +
                                  "<Link>" +
                                  "<href>" + strWebUrl + "/LightningHandler.ashx?" + querystring + "&amp;type=LtgFlash&amp;</href>" +
                                  "<refreshMode>onInterval</refreshMode>" +
                                  "<refreshInterval>18000</refreshInterval>" +
                                  "<viewRefreshMode>onRequest</viewRefreshMode>" +
                                  "</Link>" +
                                  timespan +
                                  "</NetworkLink>";

                writer.WriteRaw(strkml);

                sTime = sTime.AddMinutes(10);

            } while (sTime < endTime);

            writer.WriteEndElement(); //</Document>
            writer.WriteEndElement(); //</KML>                
            writer.Flush();
            writer.Close();

            ZipStream(memStream, outputStream);   
        }
        private static void WriteLightningFlashKmzFile(Stream outputStream, DateTime startTime, DateTime endTime)
        {			
            
            string style = @"<Style id=""ICFlash"">" +
                            "<IconStyle><scale>0.25</scale>" +
                            "<Icon><href>https://streamerrt.enterprise.weatherbug.com/LightningAnalysis/images/AWSLTGFlash_1.gif</href></Icon>" +
                            "</IconStyle>" +
                            "</Style>";

            style += @"<Style id=""CGFlash"">" +
                            "<IconStyle><scale>0.25</scale>" +
                            "<Icon><href>https://streamerrt.enterprise.weatherbug.com/LightningAnalysis/images/AWSLTGFlash_0.gif</href></Icon>" +
                            "</IconStyle>" +
                            "</Style>";
			style += @"<Style id=""CGNegative"">" +
							"<IconStyle><scale>0.5</scale>" +
                            "<Icon><href>https://streamerrt.enterprise.weatherbug.com/LightningAnalysis/images/ltg-cg-negative.png</href></Icon>" +
							"</IconStyle>" +
							"</Style>";
			style += @"<Style id=""CGPositive"">" +
							"<IconStyle><scale>0.5</scale>" +
                            "<Icon><href>https://streamerrt.enterprise.weatherbug.com/LightningAnalysis/images/ltg-cg-positive.png</href></Icon>" +
							"</IconStyle>" +
							"</Style>";
			style += @"<Style id=""Ltg-IC"">" +
							"<IconStyle><scale>0.5</scale>" +
                            "<Icon><href>https://streamerrt.enterprise.weatherbug.com/LightningAnalysis/images/ltg-ic.png</href></Icon>" +
							"</IconStyle>" +
							"</Style>";

            
            MemoryStream memStream = new MemoryStream();
            XmlTextWriter writer = new XmlTextWriter(memStream, null) { Indentation = 4 };

            WriteDocumentHeader(writer);
            writer.WriteRaw(style);


            string bbox = HttpContext.Current.Request.Params["BBOX"];
            string errMsg = string.Empty;

            decimal minX = 0;
            decimal maxX = 0;
            decimal minY = 0;
            decimal maxY = 0;
            string[] bboxValues = null;
            if (!string.IsNullOrEmpty(bbox))
            {
                bboxValues = bbox.Split(",".ToCharArray());
                if (bboxValues.Length > 0)
                {
                    //minX + "," + maxY + "," + maxX + "," + minY;
                    if (!decimal.TryParse(bboxValues[0], out minX))
                        errMsg += " Invalid parameter: minX";
                    if (!decimal.TryParse(bboxValues[2], out maxX))
                        errMsg += " Invalid parameter: maxX";
                    if (!decimal.TryParse(bboxValues[3], out minY))
                        errMsg += " Invalid parameter: minY";
                    if (!decimal.TryParse(bboxValues[1], out maxY))
                        errMsg += " Invalid parameter: maxY";
                }
            }

            if (errMsg != string.Empty)
            {
                System.Diagnostics.Debug.WriteLine(errMsg);
            }

            #region lx portal api call

            List<FlashRecord> flashes = null;
            flashes = DetectionReportClient.GetBoxDetections(startTime, endTime, "both", maxY, minX, minY, maxX, LayerType.Flash, null).r;
            #endregion

            if (flashes != null && flashes.Count > 0)
            {
                foreach (FlashRecord flash in flashes)
                {
                    writer.WriteStartElement("Placemark");
                    DateTime flashTime = flash.TimeUtc;
                    string timespan = "<TimeSpan>" +
                                      "<begin>" + flashTime.ToString("yyyy-MM-ddTHH:mm:00Z") + "</begin>" +
                                      "<end>" + flashTime.AddMinutes(10.0).ToString("yyyy-MM-ddTHH:mm:00Z") + "</end>" +
                                      "</TimeSpan>";

                    if (flash.Type == StrokeType.IntraCloud)
                    {
                        writer.WriteElementString("styleUrl", "#Ltg-IC");
                    }
                    else
                    {
                        if (flash.Amplitude > 0)
                        {
                            writer.WriteElementString("styleUrl", "#CGPositive");
                        }
                        else
                        {
                            writer.WriteElementString("styleUrl", "#CGNegative");
                        }
                    }
                    writer.WriteRaw(timespan);
                    writer.WriteStartElement("Point");
                    writer.WriteElementString("coordinates", flash.Longitude + "," + flash.Latitude + ",0");
                    writer.WriteEndElement(); //Point
                    writer.WriteEndElement(); //Placemark
                    writer.WriteWhitespace(Environment.NewLine);
                }
            }
            writer.WriteEndElement(); //</Document>
            writer.WriteEndElement(); //</KML>                
            writer.Flush();
            writer.Close();

            ZipStream(memStream, outputStream);

        }

        private static void WriteStormReportsKmzFile(Stream outputStream, DateTime startTime, DateTime endTime)
        {
            //TORNADO style
            string style = @"<Style id=""TORNADO_Style"">" +
                            "<IconStyle><scale>1.0</scale>" +
                            "<color>FF0000FF</color>" +
                            "<Icon><href>http://www.aws.com/images/whitetriangle.png</href></Icon>" +
                            "</IconStyle>" +
                            "</Style>";

            //TSTM WND DMG & TSTM WND GST
            style += @"<Style id=""TSTM_WND_Style"">" +
                            "<IconStyle><scale>1.0</scale>" +
                            "<color>FFFF0000</color>" +
                            "<Icon><href>http://www.aws.com/images/whitetriangle.png</href></Icon>" +
                            "</IconStyle>" +
                            "</Style>";
            
            //Hail Style
            style += @"<Style id=""HAIL_Style"">" +
                            "<IconStyle><scale>1.0</scale>" +
                            "<color>FF00CD00</color>" +
                            "<Icon><href>http://www.aws.com/images/whitetriangle.png</href></Icon>" +
                            "</IconStyle>" +
                            "</Style>";

            MemoryStream memStream = new MemoryStream();
            XmlTextWriter writer = new XmlTextWriter(memStream, null) { Indentation = 4 };
            
            WriteDocumentHeader(writer);
            writer.WriteRaw(style);
            

            //Get the Old alerts
            using (SqlConnection cnn = new SqlConnection(ConfigurationManager.AppSettings["StormReportsConnectionString"]))
            {
                string sql = "SELECT LSR.MsgID, ISNULL(lu.LsrId, 0) AS LsrId, LSR.WfoID, LSR.ReportedTime, LSR.TimeZone, LSR.UTCTime, LSR.Event, LSR.Magnitude, LSR.Unit, LSR.Accuracy, " +
                            "LSR.Latitude, LSR.Longitude, LSR.Source, LSR.Remarks "+
                            "FROM  dbo.NBCSAS_LSR AS LSR LEFT OUTER JOIN " +
                            "dbo.NBCSAS_LU_LSR AS lu ON lu.event = LSR.Event " +
                            "WHERE (LSR.ReportedTime between '" + startTime.AddHours(-10.0) + "' AND '" + endTime + "') AND (lu.LsrId IN (15, 37, 39, 40, 41))";

                using (SqlCommand cmd = new SqlCommand(sql, cnn))
                {
                    cmd.CommandTimeout = 5 * 60 * 1000; //10Mins.
                    cnn.Open();
                    Aws.Core.Utilities.EventManager.LogDebug("LSR Sql query " + sql);
                    SqlDataReader reader = cmd.ExecuteReader();
                    WriteStormReportsData(reader, writer, startTime, endTime);
                }
            }
            writer.WriteEndElement(); //</Document>
            writer.WriteEndElement(); //</KML>                
            writer.Flush();
            writer.Close();

            ZipStream(memStream, outputStream);

        }

	    private static void WriteStormReportsData(SqlDataReader reader, XmlTextWriter writer, DateTime startTime, DateTime endTime)
	    {
	        int i = 0;
	        while (reader != null && reader.Read())
	        {
	            i++;
	            DateTime postedTime = Convert.ToDateTime(reader["UTCTime"].ToString());
	            DateTime localTime = Convert.ToDateTime(reader["ReportedTime"].ToString());
	            string localTimeZone = reader["TimeZone"].ToString();
                
	            int stormtype = 0;
	            float lat = 0.0F;
	            float lon = 0.0F;
                Dictionary<string, string> mapTipData = new Dictionary<string, string>();


                if (reader["Latitude"] != DBNull.Value)
                {
                    lat = Convert.ToSingle(reader["Latitude"].ToString());
                }

                if (reader["Longitude"] != DBNull.Value)
                {
                    lon = Convert.ToSingle(reader["Longitude"].ToString());
                }
                
                mapTipData["Reported Time Local:"] = localTime.ToString("MM/dd/yyyy hh:mm tt") + " " + localTimeZone;

                DateTime utcTime = postedTime;
                Aws.Core.Utilities.EventManager.LogDebug("Posted utc time " + utcTime);
	            try
	            {
	                
	                TimeZoneData tz =TimeZoneSvcHelper.GetTimeZone(lat, lon);
                    if (tz != null)
                    {
                        utcTime = localTime.AddHours(-1.0 * tz.TimeZoneOffset);
                        mapTipData["Reported Time UTC:"] = utcTime.ToString("MM/dd/yyyy HH:mm UTC");
                    }
                    
	            }
	            catch (Exception ex)
	            {
	                System.Diagnostics.Debug.WriteLine(ex.ToString());
                    Aws.Core.Utilities.EventManager.LogDebug("error in getting timezone in LSR " + ex);
	            }

                if (utcTime < startTime || utcTime > endTime)
                {
                    Aws.Core.Utilities.EventManager.LogDebug(
                          "utc time is less than start time or greater than endtime,starttime " + startTime + " ,endtime" +
                          endTime + " ,utctime" + utcTime);
                 
                    continue;
                }

	            if (reader["Event"] != DBNull.Value)
	            {
                    mapTipData["Event Type"] = reader["Event"].ToString();
	            }
                if (reader["Magnitude"] != DBNull.Value)
                {
                    mapTipData["Magnitude"] = reader["Magnitude"].ToString() + " " + reader["Unit"].ToString();
                }
                
                
                mapTipData["Location (lat, lon)"] = lat.ToString("N2") + ", " + lon.ToString("N2");

	            if (reader["Source"] != DBNull.Value)
	            {
                    mapTipData["Source"] = reader["Source"].ToString();
	            }
	            if (reader["Remarks"] != DBNull.Value)
	            {
                    mapTipData["Remarks"] = reader["Remarks"].ToString();
	            }

	            if (reader["LsrId"] != DBNull.Value)
	            {
	                stormtype = Convert.ToInt32(reader["LsrId"].ToString());
	            }

	            #region Polygon
	            writer.WriteStartElement("Placemark");
	            writer.WriteWhitespace(Environment.NewLine);

                writer.WriteElementString("description", GetMaptipText(mapTipData));
	            writer.WriteWhitespace(Environment.NewLine);
                DateTime eTime = utcTime.AddHours(2.0);
                if (eTime > endTime)
                {
                    eTime = endTime;
                }

	            string timespan = "<TimeSpan>" +
                                  "<begin>" + utcTime.ToString("yyyy-MM-ddTHH:mm:00Z") + "</begin>" +
                                  "<end>" + eTime.ToString("yyyy-MM-ddTHH:mm:00Z") + "</end>" +
	                              "</TimeSpan>";

	            if (!string.IsNullOrEmpty(timespan))
	            {
	                writer.WriteRaw(timespan);
	                writer.WriteWhitespace(Environment.NewLine);
	            }

	            string styleId = string.Empty;
	            switch(stormtype)
	            {
	                case 15: //hail
	                    styleId = "#HAIL_Style";
	                    break;

	                case 37: //Tornado
	                    styleId = "#TORNADO_Style";
	                    break;

	                case 39: //TSTM WND DMG
	                    styleId = "#TSTM_WND_Style";
	                    break;

	                case 40: //TSTM WND GST
	                    styleId = "#TSTM_WND_Style";
	                    break;

	                case 41: //WATER SPOUT
	                    styleId = "#TSTM_WND_Style";
	                    break;
	            }

	            if (!string.IsNullOrEmpty(styleId))
	            {
	                writer.WriteElementString("styleUrl", styleId);
	            }

	            #region Point
	            writer.WriteStartElement("Point");
	            writer.WriteElementString("coordinates", lon + "," + lat + ",0");
	            writer.WriteEndElement();
	            writer.WriteWhitespace(Environment.NewLine);
	            #endregion
                        
                        
	            writer.WriteEndElement();//</PlaceMark>
	            writer.WriteWhitespace(Environment.NewLine);
	            #endregion
	        }
            Aws.Core.Utilities.EventManager.LogDebug("Number of storm reports count " + i);
	    }

	    private static void WriteNWSAlertsKmzFile(Stream outputStream, DateTime startTime, DateTime endTime)
        {
            string style = @"<Style id=""SVRPolyStyle"">" +
                                        "<LineStyle>" +
                                        "<color>ff00B9FE</color>" +
                                        "<width>4</width>" +
                                        "</LineStyle>" +
                                        "<PolyStyle>" +
                                        "<color>ff00B9FE</color>" +
                                        "<colorMode>normal</colorMode>" +
                                        "<fill>0</fill>" +
                                        "</PolyStyle>" +
                                        "</Style>";
            style += @"<Style id=""TORPolyStyle"">" +
                                    "<LineStyle>" +
                                    "<color>ff0000cc</color>" +
                                    "<width>4</width>" +
                                    "</LineStyle>" +
                                    "<PolyStyle>" +
                                    "<color>ff0000cc</color>" +
                                    "<colorMode>normal</colorMode>" +
                                    "<fill>0</fill>" +
                                    "</PolyStyle>" +
                                    "</Style>";
            //Get the Old alerts
            using (SqlConnection cnn = new SqlConnection(ConfigurationManager.AppSettings["NWSAlertsConnectionString"]))
            {
                //string sql = "SELECT HIST.*, T.* FROM stormpager.dbo.NWS_ALERT_POLYGONS_HISTORY AS HIST (NOLOCK) " +
                //             "JOIN ( SELECT PVTEC_Tracking_ID, TRG_ACTION, MIN(UTCPostTime) AS UTCPostTime FROM stormpager.dbo.NWS_ALERT_POLYGONS_HISTORY AS pvtecg (NOLOCK) " +
                //             " GROUP BY PVTEC_Tracking_ID, trg_action HAVING TRG_ACTION = 'INSERT') A ON HIST.PVTEC_Tracking_ID = A.PVTEC_Tracking_ID " +
                //             "AND HIST.TRG_ACTION = A.TRG_ACTION AND HIST.UTCPostTime = A.UTCPostTime, StormPager.dbo.NBCSAS_NNN_Type AS T " +
                //             "WHERE HIST.NNN_Type_ID = T.NNN_Type_ID AND (HIST.NNN_Type_ID = '25' OR HIST.NNN_Type_ID = '26') AND HIST.UTCPostTime BETWEEN '" +
                //             startTime.ToString() + "' AND '" + endTime.ToString() + "' ORDER BY HIST.UTCPostTime";

                string sql = " Select H.*, T.* from stormpager.dbo.NWS_ALERT_POLYGONS_HISTORY H(nolock) " +
                             "LEFT JOIN StormPager.dbo.NBCSAS_NNN_Type  T  (nolock) ON T.NNN_Type_ID = H.NNN_Type_ID " +
                             "where UTCPostTime between '"+ startTime.ToString() + "' AND '" + endTime.ToString() +"' "+
                             "and H.NNN_Type_ID IN ('25', '26') and TRG_ACTION = 'INSERT'";
                using (SqlCommand cmd = new SqlCommand(sql, cnn))
                {
                    cmd.CommandTimeout = 5*60*1000; //10Mins.
                    cnn.Open();
                    Aws.Core.Utilities.EventManager.LogDebug("Nws Alerts Sql query " + sql);

                    SqlDataReader reader = cmd.ExecuteReader();
                    MemoryStream memStream = new MemoryStream();
                    XmlTextWriter writer = new XmlTextWriter(memStream, null) { Indentation = 4 };
                    if (reader != null)
                    {
                        WriteDocumentHeader(writer);
                        writer.WriteRaw(style);
                    }

                    int l = 0;
                    while(reader != null && reader.Read())
                    {
                        l++;
                        DateTime postedTime = Convert.ToDateTime(reader["UTCPostTime"].ToString());
                        DateTime expireTime = Convert.ToDateTime(reader["UTCExpireTime"].ToString());
                        string[] latlonString = null;
                        string desc = string.Empty;
                        string stormtype = string.Empty;
                        Dictionary<string, string> mapTipData = new Dictionary<string, string>();
                        if (reader["LAT_LON_Str"] != DBNull.Value)
                        {
                            latlonString = reader["LAT_LON_Str"].ToString().Split(" ".ToCharArray());
                        }

                        if (reader["DESCRIPTION"] != DBNull.Value)
                        {
                            mapTipData["Type"] = reader["DESCRIPTION"].ToString();
                        }

                        mapTipData["Posted Time"] = postedTime.ToString("MM/dd/yyyy HH:mm UTC");
                        mapTipData["Expire Time"] = expireTime.ToString("MM/dd/yyyy HH:mm UTC");
                        if (reader["Direction"] != DBNull.Value)
                        {
                            mapTipData["Direction"] = reader["Direction"].ToString() +" deg";
                        }
                        if (reader["Speed"] != DBNull.Value)
                        {
                            mapTipData["Speed"] = reader["Speed"].ToString() + " MPH";
                        }

                        if (reader["NNN"] != DBNull.Value)
                        {
                            stormtype = reader["NNN"].ToString();
                        }

                        #region Polygon
                        writer.WriteStartElement("Placemark");
                        writer.WriteWhitespace(Environment.NewLine);

                        writer.WriteElementString("description", GetMaptipText(mapTipData));
                        writer.WriteWhitespace(Environment.NewLine);

                        DateTime eTime = expireTime;
                        if (eTime > endTime)
                        {
                            eTime = endTime;
                        }
                            
                        string timespan = "<TimeSpan>" +
                                          "<begin>" + postedTime.ToString("yyyy-MM-ddTHH:mm:00Z") + "</begin>" +
                                          "<end>" + eTime.ToString("yyyy-MM-ddTHH:mm:00Z") + "</end>" +
                                          "</TimeSpan>";

                        if (!string.IsNullOrEmpty(timespan))
                        {
                            writer.WriteRaw(timespan);
                            writer.WriteWhitespace(Environment.NewLine);
                        }
                        if (stormtype == "TOR")
                        {
                            writer.WriteElementString("styleUrl", "#TORPolyStyle");
                        } 
                        else if (stormtype == "SVR")
                        {
                            writer.WriteElementString("styleUrl", "#SVRPolyStyle");    
                        }
                        

                        writer.WriteStartElement("Polygon");
                        //writer.WriteElementString("extrude", "1");
                        writer.WriteElementString("tessellate", "1");
                        writer.WriteElementString("altitudeMode", "clampToGround");

                        writer.WriteStartElement("outerBoundaryIs");
                        writer.WriteStartElement("LinearRing");
                        
                        StringBuilder sv = new StringBuilder();
                        for (int nVertex = 0; nVertex < latlonString.Length; nVertex++ )
                        {
                            float lat = Convert.ToSingle(latlonString[nVertex])/100.0F;
                            nVertex++;
                            float lon = Convert.ToSingle(latlonString[nVertex]) / -100.0F;

                            sv.Append(lon.ToString("N2") + "," + lat.ToString("N2") + ",0" + Environment.NewLine);
                        }
                        //Close the Polygon
                        float lat0 = Convert.ToSingle(latlonString[0]) / 100.0F;
                        float lon0 = Convert.ToSingle(latlonString[1]) / -100.0F;
                        sv.Append(lon0.ToString("N2") + "," + lat0.ToString("N2") + ",0" + Environment.NewLine);

                        writer.WriteElementString("coordinates", sv.ToString());
                        writer.WriteEndElement(); //</outerBoundaryIs>
                        writer.WriteEndElement();//</LinearRing>
                        writer.WriteEndElement(); //</polygon>
                        writer.WriteEndElement();//</PlaceMark>
                        writer.WriteWhitespace(Environment.NewLine);
                        #endregion
                    }
                    Aws.Core.Utilities.EventManager.LogDebug("NWS count " + l);
                    writer.WriteEndElement(); //</Document>
                    writer.WriteEndElement(); //</KML>                
                    writer.Flush();
                    writer.Close();

                    ZipStream(memStream, outputStream);
                }
            }

        }

	    private static void WriteRadarKmzFile(Stream outputStream, DateTime startTime, DateTime endTime)
	    {
            MemoryStream memStream = new MemoryStream();
            XmlTextWriter writer = new XmlTextWriter(memStream, null) { Indentation = 4 };

            WriteDocumentHeader(writer);

            DateTime sTime = startTime.AddHours(-4.0);
	        endTime = endTime.AddHours(-4.0);
	        do
	        {

	            string strkml = "<GroundOverlay><name>Radar</name><color>9cffffff</color>" +
	                            "<Icon>" +
                                "<href>http://kml.datafeed.weatherbug.com/GIS/images/9/" + sTime.ToString("yyyy/MM/dd/HH/yyyy-MM-dd-HH-mm") + ".png</href>" +
	                            "<viewBoundScale>0.75</viewBoundScale> " +
	                            "</Icon>" +
	                            "<LatLonBox>" +
	                            "<north>51</north>" +
	                            "<south>21</south>" +
	                            "<east>-65</east>" +
	                            "<west>-127</west>" +
	                            "</LatLonBox>" +
	                            "<TimeSpan>" +
                                "<begin>" + sTime.ToString("yyyy-MM-ddTHH:mm:00Z") + "</begin>" +
                                "<end>" + sTime.AddMinutes(5).ToString("yyyy-MM-ddTHH:mm:00Z") + "</end>" +
	                            "</TimeSpan>" +
	                            "<drawOrder>0</drawOrder>" +
	                            "</GroundOverlay>";
	            writer.WriteRaw(strkml);

	            sTime = sTime.AddMinutes(5);

            } while (sTime <= endTime);

            writer.WriteEndElement(); //</Document>
            writer.WriteEndElement(); //</KML>                
            writer.Flush();
            writer.Close();

            ZipStream(memStream, outputStream);
	    }

	    private static void ZipStream(MemoryStream inputStream, Stream outputStream)
	    {
	        byte[] bytes = inputStream.ToArray();
	        using (ZipOutputStream gzOs = new ZipOutputStream(outputStream))
	        {
	            //ZipEntry entry = new ZipEntry("Radar_Overlay.kml");
				string stype = HttpContext.Current.Request.Params["type"];
				string fname = "LighningAnalysis.kml";
				if (stype == "Radar")
                {
                    fname = "Radar_Overlay.kml";
                }
                if (stype == "NWSAlerts")
                {
                     fname = "NWSAlerts_Overlay.kml";
                }
                if (stype == "StormReports")
                {
                    fname = "StormReports_Overlay.kml";
                }
                if (stype == "LtgFlashFrame")
                {
                   fname = "LtgFlashFrame_Overlay.kml";
                }
                if (stype == "LtgFlash")
                {
					fname = "LtgFlash_Overlay.kml";
				}
				else if (stype == "CellTrack")
				{
					fname = "CellTrack.kml";
				}
				else if (stype == "Polygon")
				{
					fname = "CellPolygon.kml";
				}

				ZipEntry entry = new ZipEntry(fname);
	            gzOs.UseZip64 = UseZip64.Off;
	            gzOs.SetLevel(9);
	            gzOs.PutNextEntry(entry);
	            gzOs.Write(bytes, 0, bytes.Length);
	            gzOs.CloseEntry();
	            gzOs.Close();
	        }
	    }

	    private static void WriteDocumentHeader(XmlTextWriter writer)
	    {
	        writer.WriteStartDocument();
	        writer.WriteWhitespace(Environment.NewLine);
	        writer.WriteStartElement("kml");
	        writer.WriteAttributeString("xmlns", "http://earth.google.com/kml/2.0");
	        writer.WriteWhitespace(Environment.NewLine);
	        writer.WriteStartElement("Document");
	        writer.WriteElementString("name", "KML powered by Earth Networks StreamerRT");
	    }

        private static string GetMaptipText(Dictionary<string, string> mapTipData)
        {
            StringBuilder sbDesc = new StringBuilder();
            //int iCellID = 0;

            sbDesc.Append("<div id='maptip_wrapper'  style='background-color:#f2dcdb;border:1px solid #953735'>");
            sbDesc.Append("<table width='100%' cellpadding='2'  cellspacing='0' style='border-collapse: collapse;background-color: #f2dcdb;'>");
            sbDesc.Append("<tr><td align='center' style = 'padding-bottom:3px'>");
            sbDesc.Append("<table style='width:100%;border-collapse:collapse;border: 1px solid white' cellpadding='3' cellspacing='0'>");

            int k = 0;

            foreach (var data in mapTipData)
            {
                if (k % 2 == 0)
                {
                    sbDesc.Append("<tr><td style='width:45%; background-color:#e8d0d0;border: 1px  solid white;padding-left :4px;' align='left'>" + data.Key + "</td><td  align='left' style='width:55%;background-color:#e8d0d0;border: 1px  solid white;padding-left :4px;'>" + data.Value + "</td></tr>");
                }
                else
                {
                    sbDesc.Append("<tr><td style='width:45%;background-color: #f4e9e9;border: 1px  solid white;padding-left :4px;' align='left'>" + data.Key + "</td><td align='left' style='width:55%;background-color: #f4e9e9;border: 1px  solid white;padding-left :4px;'>" + data.Value + "</td></tr>");
                }
                k++;
            }
            sbDesc.Append("</table></td></tr>");
            sbDesc.Append("</table></div>");

            return sbDesc.ToString();
        }


	    public string ParseFilesEx(List<IGeometry> featureList, System.IO.Stream outputStream, string cacheKey,string stype,DateTime endDateTime)
        {
            try
            {
                string strkml = string.Empty;
                //XmlTextWriter writer = new XmlTextWriter(outputStream, null) { Indentation = 4 };

				MemoryStream memStream = new MemoryStream();
				XmlTextWriter writer = new XmlTextWriter(memStream, null) { Indentation = 4 };

                WriteDocumentHeader(writer);

                #region Styles

                string style = @"<Style id=""ltgPolyStyle"">" +
                                        "<LineStyle>" +
                                        "<color>ffff00ff</color>" +
                                        "<width>4</width>" +
                                        "</LineStyle>" +
                                        "<PolyStyle>" +
                                        "<color>ff00ff00</color>" +
                                        "<colorMode>normal</colorMode>" +
                                        "<fill>0</fill>" +
                                        "</PolyStyle>" +
                                        "</Style>";
                style += @"<Style id=""ltgPolyGreenStyle"">" +
                                        "<LineStyle>" +
                                        "<color>ff008000</color>" +
                                        "<width>4</width>" +
                                        "</LineStyle>" +
                                        "<PolyStyle>" +
                                        "<color>ff008000</color>" +
                                        "<colorMode>normal</colorMode>" +
                                        "<fill>0</fill>" +
                                        "</PolyStyle>" +
                                        "</Style>";
                style += @"<Style id=""ltgPolyYellowStyle"">" +
                                        "<LineStyle>" +
                                        "<color>ff00ffff</color>" +
                                        "<width>4</width>" +
                                        "</LineStyle>" +
                                        "<PolyStyle>" +
                                        "<color>ff00ffff</color>" +
                                        "<colorMode>normal</colorMode>" +
                                        "<fill>0</fill>" +
                                        "</PolyStyle>" +
                                        "</Style>";
                style += @"<Style id=""ltgPolyOrangeStyle"">" +
                                        "<LineStyle>" +
                                        "<color>ff00A5ff</color>" +
                                        "<width>4</width>" +
                                        "</LineStyle>" +
                                        "<PolyStyle>" +
                                        "<color>ff00A5ff</color>" +
                                        "<colorMode>normal</colorMode>" +
                                        "<fill>0</fill>" +
                                        "</PolyStyle>" +
                                        "</Style>";
                style += @"<Style id=""ltgPolyRedStyle"">" +
                                        "<LineStyle>" +
                                        "<color>ff0000ff</color>" +
                                        "<width>4</width>" +
                                        "</LineStyle>" +
                                        "<PolyStyle>" +
                                        "<color>ff0000ff</color>" +
                                        "<colorMode>normal</colorMode>" +
                                        "<fill>0</fill>" +
                                        "</PolyStyle>" +
                                        "</Style>";
                style += @"<Style id=""ltgPolyPurpleStyle"">" +
                                        "<LineStyle>" +
                                        "<color>ff800080</color>" +
                                        "<width>4</width>" +
                                        "</LineStyle>" +
                                        "<PolyStyle>" +
                                        "<color>ff800080</color>" +
                                        "<colorMode>normal</colorMode>" +
                                        "<fill>0</fill>" +
                                        "</PolyStyle>" +
                                        "</Style>";
                style += @"<Style id=""ltgPolyTransparentStyle"">" +
                                        "<LineStyle>" +
                                        "<color>00000000</color>" +
                                        "<width>4</width>" +
                                        "</LineStyle>" +
                                        "<PolyStyle>" +
                                        "<color>00000000</color>" +
                                        "<colorMode>normal</colorMode>" +
                                        "<fill>0</fill>" +
                                        "</PolyStyle>" +
                                        "</Style>";
				//Lx Alert Polygon 
				style += @"<Style id=""ltgPolyAlertStyle"">" +
									   "<LineStyle>" +
									   "<color>ffff00b2</color>" +
									   "<width>4</width>" +
									   "</LineStyle>" +
									   "<PolyStyle>" +
									   "<color>ffff00b2</color>" +
									   "<colorMode>normal</colorMode>" +
									   "<fill>0</fill>" +
									   "</PolyStyle>" +
									   "</Style>";
                //LineStyles
                style += @"<Style id=""ltgLineStyle"">" +
                                     "<LineStyle>" +
                                     "<color>ff00ff00</color>" +
                                     "<width>4</width>" +
                                     "</LineStyle>" +
                                     "</Style>";
                style += @"<Style id=""ltgLineGreenStyle"">" +
                             "<LineStyle>" +
                             "<color>ff008000</color>" +
                             "<width>4</width>" +
                             "</LineStyle>" +
                             "</Style>";
                style += @"<Style id=""ltgLineYellowStyle"">" +
                             "<LineStyle>" +
                             "<color>ff00ffff</color>" +
                             "<width>4</width>" +
                             "</LineStyle>" +
                             "</Style>";
                style += @"<Style id=""ltgLineOrangeStyle"">" +
                             "<LineStyle>" +
                             "<color>ff00A5ff</color>" +
                             "<width>4</width>" +
                             "</LineStyle>" +
                             "</Style>";
                style += @"<Style id=""ltgLineRedStyle"">" +
                             "<LineStyle>" +
                             "<color>ff0000ff</color>" +
                             "<width>4</width>" +
                             "</LineStyle>" +
                             "</Style>";
                style += @"<Style id=""ltgLinePurpleStyle"">" +
                             "<LineStyle>" +
                             "<color>ff800080</color>" +
                             "<width>4</width>" +
                             "</LineStyle>" +
                             "</Style>";
                style += @"<Style id=""ltgLineTransparentStyle"">" +
                             "<LineStyle>" +
                             "<color>00000000</color>" +
                             "<width>4</width>" +
                             "</LineStyle>" +
                             "</Style>";

                style += @"<Style id=""ltgLineBlackStyle"">" +
                              "<LineStyle>" +
                              "<color>ff000000</color>" +
                              "<width>4</width>" +
                              "</LineStyle>" +
                              "</Style>";

                //vector style
                style += @"<Style id=""ltgLineVectorStyle"">" +
                                         "<LineStyle>" +
                                         "<color>ffffff00</color>" +
                                         "<width>4</width>" +
                                         "</LineStyle>" +
                                         "</Style>";
			
                #endregion

                writer.WriteRaw(style);

                #region Export KML

                foreach (var feature in featureList)
                {
                    StringBuilder sbDesc = new StringBuilder();
                    string timeSpan = string.Empty;
                    DateTime cellTime = new DateTime();
                    //int iCellID = 0;
					string iCellID = string.Empty;

                    sbDesc.Append("<div id='maptip_wrapper'  style='background-color:#f2dcdb;border:1px solid #953735'>");
                    sbDesc.Append("<table width='100%' cellpadding='2'  cellspacing='0' style='border-collapse: collapse;background-color: #f2dcdb;'>");
                    sbDesc.Append("<tr><td align='center' style = 'padding-bottom:3px'>");
                    sbDesc.Append("<table style='width:100%;border-collapse:collapse;border: 1px solid white' cellpadding='3' cellspacing='0'>");

                    int k = 0;
                    #region Get Description Test.
                    foreach (var column in feature.MetaData.Keys)
                    {
                        if (k % 2 == 0)
                        {
                            sbDesc.Append("<tr><td style='width:45%; background-color:#e8d0d0;border: 1px  solid white;padding-left :4px;' align='left'>" + column + "</td><td  align='left' style='width:55%;background-color:#e8d0d0;border: 1px  solid white;padding-left :4px;'>" + feature.MetaData[column] + "</td></tr>");
                        }
                        else
                        {
                            sbDesc.Append("<tr><td style='width:45%;background-color: #f4e9e9;border: 1px  solid white;padding-left :4px;' align='left'>" + column + "</td><td align='left' style='width:55%;background-color: #f4e9e9;border: 1px  solid white;padding-left :4px;'>" + feature.MetaData[column] + "</td></tr>");
                        }
                        k++;
                        //TODO: HACK
                        if (column == "CellTime")
                        {
                            cellTime = DateTime.Parse(feature.MetaData[column].ToString());
                            timeSpan = "<TimeSpan><begin>" + cellTime.ToString("yyyy-MM-ddTHH:mm:ssZ") + "</begin><end>" + cellTime.AddMinutes(1.0).ToString("yyyy-MM-ddTHH:mm:ssZ") + "</end></TimeSpan>";
                        }

                        if (column == "CellID")
                        {
                           // iCellID = int.Parse(feature.MetaData[column].ToString());
						   iCellID = feature.MetaData[column].ToString();
                        }
                    }
                    sbDesc.Append("</table></td></tr>");

                    string strWebUrl = ConfigurationManager.AppSettings["WebUrl"];

                    if (!string.IsNullOrEmpty(strWebUrl)&&(stype != "Polygon"))
                    {
                        string url = strWebUrl + "/LightningAlertDetails.aspx?cellid=" + iCellID + "&key=" + cacheKey;
                        sbDesc.Append("<tr ><td style='padding:4px'><a id='lightningdetails' href='" + url + "' target='_blank' title='Details'>Details</a></td></tr>");
                        
                    }

                    sbDesc.Append("</table></div>");
                    #endregion

                    Geometry geometry = feature as AwsGeometries.Geometry;
                    if (geometry is Point)
                    {
                        #region Point
                        writer.WriteStartElement("Placemark");
                        writer.WriteWhitespace(Environment.NewLine);
                        Point point = geometry as Point;
                        writer.WriteElementString("description", sbDesc.ToString());
                        writer.WriteWhitespace(Environment.NewLine);
                        writer.WriteStartElement("Point");
                        writer.WriteElementString("coordinates", point.X + "," + point.Y + ",0");
                        writer.WriteEndElement();
                        writer.WriteEndElement();//</PlaceMark>
                        writer.WriteWhitespace(Environment.NewLine);
                        #endregion

                    }
                    else if (geometry is LineString)
                    {
                        #region LineString
                        if (feature.MetaData.ContainsKey("FeatureType") && (feature.MetaData["FeatureType"].ToString() == "Vector"))
                        {
                            timeSpan = "<TimeSpan><begin>" + cellTime.ToString("yyyy-MM-ddTHH:mm:ssZ") + "</begin><end>" + cellTime.AddMinutes(2.0).ToString("yyyy-MM-ddTHH:mm:ssZ") + "</end></TimeSpan>";
                        }
                        else
                        {
                            timeSpan = "<TimeSpan><begin>" + cellTime.ToString("yyyy-MM-ddTHH:mm:ssZ") + "</begin><end>" + endDateTime.ToString("yyyy-MM-ddTHH:mm:ssZ") + "</end></TimeSpan>";
                        }  

                        writer.WriteStartElement("Placemark");
                        writer.WriteWhitespace(Environment.NewLine);
                        LineString lineString = geometry as LineString;
                        writer.WriteElementString("description", sbDesc.ToString());
                        writer.WriteWhitespace(Environment.NewLine);
                        
                        #region Assign LineStyles

                        if (feature.MetaData.ContainsKey("FeatureType") && (feature.MetaData["FeatureType"].ToString() == "Vector"))
                        {
                            writer.WriteElementString("styleUrl", "#ltgLineVectorStyle");
                        }

                        else if (feature.MetaData.ContainsKey("TotalRateFlashesPerMinute") && (!string.IsNullOrEmpty(feature.MetaData["TotalRateFlashesPerMinute"].ToString())))
                        {
                            float iTotalLxRate = 0;
                            iTotalLxRate = float.Parse(feature.MetaData["TotalRateFlashesPerMinute"].ToString());

                            if (iTotalLxRate <= 1.9999)
                            {
                                writer.WriteElementString("styleUrl", "#ltgLineBlackStyle"); //Black
                            }
                            else if (iTotalLxRate > 1.9999 && iTotalLxRate <= 4.9999)
                            {
                                writer.WriteElementString("styleUrl", "#ltgLineGreenStyle");//Green
                            }
                            else if (iTotalLxRate > 4.9999 && iTotalLxRate <= 9.9999)
                            {
                                writer.WriteElementString("styleUrl", "#ltgLineYellowStyle");//yellow
                            }
                            else if (iTotalLxRate > 9.9999 && iTotalLxRate <= 19.9999)
                            {
                                writer.WriteElementString("styleUrl", "#ltgLineOrangeStyle");//orange
                            }
                            else if (iTotalLxRate > 19.9999 && iTotalLxRate <= 39.9999)
                            {
                                writer.WriteElementString("styleUrl", "#ltgLineRedStyle");//red
                            }
                            else if (iTotalLxRate > 39.9999 && iTotalLxRate <= 999.9999)
                            {
                                writer.WriteElementString("styleUrl", "#ltgLinePurpleStyle");//purple
                            }
                            else
                            {
                                writer.WriteElementString("styleUrl", "#ltgLineTransparentStyle");//transperant
                            }
                        }
                        #endregion  Assign LineStyles

                        if (!string.IsNullOrEmpty(timeSpan))
                        {
                            writer.WriteRaw(timeSpan);
                            writer.WriteWhitespace(Environment.NewLine);
                        }
                        writer.WriteStartElement("LineString");
                        //writer.WriteElementString("extrude", "1");
                        //writer.WriteElementString("tessellate", "1");

                        StringBuilder sv = new StringBuilder();
                        foreach (var vertex in lineString.Vertices)
                        {
                            sv.Append(vertex.X + "," + vertex.Y + ",0" + Environment.NewLine);
                        }
                        writer.WriteElementString("coordinates", sv.ToString());
                        writer.WriteEndElement();
                        writer.WriteEndElement();//</PlaceMark>
                        writer.WriteWhitespace(Environment.NewLine);
                        #endregion

                    }
                    else if (geometry is Polygon)
                    {
                        #region Polygon
                        writer.WriteStartElement("Placemark");
                        writer.WriteWhitespace(Environment.NewLine);
                        Polygon poly = geometry as Polygon;
                        writer.WriteElementString("description", sbDesc.ToString());
                        writer.WriteWhitespace(Environment.NewLine);

                        if ((stype == "Polygon") && feature.MetaData.ContainsKey("StartTime") && feature.MetaData.ContainsKey("EndTime"))
                        {
                            DateTime starttime = DateTime.Parse(feature.MetaData["StartTime"].ToString());
                            DateTime endtime = DateTime.Parse(feature.MetaData["EndTime"].ToString());
                            timeSpan = "<TimeSpan><begin>" + starttime.ToString("yyyy-MM-ddTHH:mm:ssZ") + "</begin>"+
                                        "<end>" + starttime.AddMinutes(5.0).ToString("yyyy-MM-ddTHH:mm:ssZ") + "</end></TimeSpan>";
                        }
                        if (!string.IsNullOrEmpty(timeSpan))
                        {
                            writer.WriteRaw(timeSpan);
                            writer.WriteWhitespace(Environment.NewLine);
                        }

                        //writer.WriteElementString("styleUrl", "#ltgPolyStyle");
                        #region PolyStyles
                        if(stype == "Polygon")
                        {
							writer.WriteElementString("styleUrl", "#ltgPolyAlertStyle");
                        }

                        
                        else if (feature.MetaData.ContainsKey("TotalRateFlashesPerMinute") && (!string.IsNullOrEmpty(feature.MetaData["TotalRateFlashesPerMinute"].ToString())))
                        {
                            float iTotalLxRate = 0;
                            iTotalLxRate = float.Parse(feature.MetaData["TotalRateFlashesPerMinute"].ToString());

                            if (iTotalLxRate > 1.9999 && iTotalLxRate <= 4.9999)
                            {
                                writer.WriteElementString("styleUrl", "#ltgPolyGreenStyle");//Green
                            }
                            else if (iTotalLxRate > 4.9999 && iTotalLxRate <= 9.9999)
                            {
                                writer.WriteElementString("styleUrl", "#ltgPolyYellowStyle");//yellow
                            }
                            else if (iTotalLxRate > 9.9999 && iTotalLxRate <= 19.9999)
                            {
                                writer.WriteElementString("styleUrl", "#ltgPolyOrangeStyle");//orange
                            }
                            else if (iTotalLxRate > 19.9999 && iTotalLxRate <= 39.9999)
                            {
                                writer.WriteElementString("styleUrl", "#ltgPolyRedStyle");//red
                            }
                            else if (iTotalLxRate > 39.9999 && iTotalLxRate <= 999.9999)
                            {
                                writer.WriteElementString("styleUrl", "#ltgPolyPurpleStyle");//purple
                            }
                            else
                            {
                                writer.WriteElementString("styleUrl", "#ltgPolyTransparentStyle");//transperant
                            }
                        }
                        #endregion

                        writer.WriteStartElement("Polygon");
                        writer.WriteElementString("altitudeMode", "clampToGround");
                        writer.WriteElementString("tessellate", "1");

                        writer.WriteStartElement("outerBoundaryIs");
                        writer.WriteStartElement("LinearRing");
                        
                        StringBuilder sv = new StringBuilder();
                        foreach (var vertex in poly.ExteriorRing.Vertices)
                        {
                            sv.Append(vertex.X + "," + vertex.Y + ",0" + Environment.NewLine);
                        }
                        sv.Append(poly.ExteriorRing.Vertices[0].X + "," + poly.ExteriorRing.Vertices[0].Y + ",1000" + Environment.NewLine);
                        writer.WriteElementString("coordinates", sv.ToString());
                        writer.WriteEndElement(); //</outerBoundaryIs>
                        writer.WriteEndElement();//</LinearRing>
                        writer.WriteEndElement(); //</polygon>
                        writer.WriteEndElement();//</PlaceMark>
                        writer.WriteWhitespace(Environment.NewLine);
                        #endregion
                    }
                    else if (geometry is MultiPolygon)
                    {

                        #region MultiPolygon
                        MultiPolygon multipoly = geometry as MultiPolygon;
                        foreach (Polygon poly in multipoly)
                        {
                            writer.WriteStartElement("Placemark");
                            writer.WriteWhitespace(Environment.NewLine);
                            writer.WriteElementString("description", sbDesc.ToString());
                            writer.WriteWhitespace(Environment.NewLine);
                            if (!string.IsNullOrEmpty(timeSpan))
                            {
                                writer.WriteRaw(timeSpan);
                                writer.WriteWhitespace(Environment.NewLine);
                            }
                            writer.WriteElementString("styleUrl", "#ltgPolyStyle");
                            writer.WriteStartElement("Polygon");
                            //writer.WriteElementString("extrude", "1");
                            //writer.WriteElementString("tessellate", "1");
                            writer.WriteStartElement("outerBoundaryIs");
                            writer.WriteStartElement("LinearRing");
                            StringBuilder sv = new StringBuilder();
                            foreach (var vertex in poly.ExteriorRing.Vertices)
                            {
                                sv.Append(vertex.X + "," + vertex.Y + ",0" + Environment.NewLine);
                            }
                            sv.Append(poly.ExteriorRing.Vertices[0].X + "," + poly.ExteriorRing.Vertices[0].Y + ",0" + Environment.NewLine);

                            writer.WriteElementString("coordinates", sv.ToString());
                            writer.WriteEndElement(); //</outerBoundaryIs>
                            writer.WriteEndElement();//</LinearRing>

                            writer.WriteEndElement();
                            writer.WriteEndElement();//</PlaceMark>
                            writer.WriteWhitespace(Environment.NewLine);
                        }
                        #endregion

                    }
                }
                #endregion kml

                writer.WriteEndElement(); //</Document>
                writer.WriteEndElement(); //</KML>                
                writer.Flush();

                //strkml = stream.GetStringBuilder().ToString();
                //strkml = stream.GetBuffer(); 
                writer.Close();

                ZipStream(memStream, outputStream);

                return strkml;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Exception in Parsefile: " + ex);
                Aws.Core.Utilities.EventManager.LogError("Exception in Parsefile: " + ex);
                return string.Empty;
            }

        }

		public bool IsReusable
		{
			get
			{
				return false;
			}
		}

		public static void WriteLtgFlashCSV(Stream outputStream, DateTime startTime, DateTime endTime)
		{
			MemoryStream memStream = new MemoryStream();
			StreamWriter writer = new StreamWriter(memStream,Encoding.UTF8);
			

			string bbox = HttpContext.Current.Request.Params["BBOX"];
			string errMsg = string.Empty;

		    decimal minX = 0;
            decimal maxX = 0;
            decimal minY = 0;
            decimal maxY = 0;
            string[] bboxValues = null;
			if (!string.IsNullOrEmpty(bbox))
			{
				bboxValues = bbox.Split(",".ToCharArray());
				if (bboxValues.Length > 0)
				{
					//minX + "," + maxY + "," + maxX + "," + minY;
                    if (!decimal.TryParse(bboxValues[0], out minX))
						errMsg += " Invalid parameter: minX";
                    if (!decimal.TryParse(bboxValues[2], out maxX))
						errMsg += " Invalid parameter: maxX";
                    if (!decimal.TryParse(bboxValues[3], out minY))
						errMsg += " Invalid parameter: minY";
                    if (!decimal.TryParse(bboxValues[1], out maxY))
						errMsg += " Invalid parameter: maxY";
				}
			}

			if (errMsg != string.Empty)
			{
				System.Diagnostics.Debug.WriteLine(errMsg);
			}

            #region lx portal api call

		    List<FlashRecord> flashes = null;
            flashes = DetectionReportClient.GetBoxDetections(startTime, endTime, "both", maxY, minX, minY, maxX, LayerType.Flash, null).r;
            #endregion

            #region build csv
            StringBuilder sbFlashData = new StringBuilder();

		    if (GlobalResource.GetUserCulture() == "es-ES")
		    {
		        sbFlashData.Append("Tiempo_de_Relámpagos_UTC,Latitud,Longitud,Corriente_máximan,Clasificación");
		    }
		    else
		    {
		        sbFlashData.Append("Lightning_Time_UTC,Latitude,Longitude,Peak_Current,Classification");
		    }
		    if (flashes != null && flashes.Count > 0)
		    {
                string dateformat = Common.GlobalResource.UserDateFormat + " " + "HH:mm:ss.fff";
                foreach (FlashRecord flash in flashes)
                {
                    sbFlashData.Append(System.Environment.NewLine);
                    sbFlashData.Append(flash.TimeUtc.ToString(dateformat) + ",");
                    sbFlashData.Append(flash.Latitude + ",");
                    sbFlashData.Append(flash.Longitude+",");
                    sbFlashData.Append(flash.Amplitude + ",");
                    sbFlashData.Append(flash.Type + ",");

                }
            }
            System.Diagnostics.Debug.WriteLine("Number of rows returned from the query " + ((flashes == null)? 0: flashes.Count));
            Aws.Core.Utilities.EventManager.LogInfo("  Number of rows returned from the query " + ((flashes == null) ? 0 : flashes.Count));
            writer.Write(sbFlashData);

            writer.Close();

            byte[] data = memStream.ToArray();

            outputStream.Write(data, 0, data.Length);
            #endregion

		}

		public static void LightningFlashHistoryData_KML(XmlTextWriter writer, DateTime startTime, DateTime endTime)
		{
			try
			{
				string LtgArchiveConnString = ConfigurationManager.AppSettings["LightningConnectionString"];
				string LtgArchiveTableName = ConfigurationManager.AppSettings["LtgArchiveTableName"] + startTime.ToString("yyyy") + startTime.ToString("MM");
				string LtgHistoryConnectionString = ConfigurationManager.AppSettings["AWSLightningConnString"];
				string LtgHistoryTableName = ConfigurationManager.AppSettings["LtgTableName"];
				string LtgMainTableName = ConfigurationManager.AppSettings["LtgTableName"];

				ArrayList arrLtgkeys = new ArrayList();
				LightningFlashData_Xml(writer,startTime, endTime, LtgArchiveConnString, LtgArchiveTableName, true, arrLtgkeys);
				LightningFlashData_Xml(writer, startTime, endTime, LtgHistoryConnectionString, LtgHistoryTableName + "_History", false, arrLtgkeys);
				//LightningFlashData_Xml(writer,startTime, endTime, LtgArchiveConnString, LtgArchiveTableName, true, arrLtgkeys);
				//Get Data From Main Table
				LightningFlashData_Xml(writer, startTime, endTime, LtgHistoryConnectionString, LtgMainTableName, false, arrLtgkeys);
				}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.WriteLine("Exception Occured in LightningFlashHistoryData_CSV " + ex);
				Aws.Core.Utilities.EventManager.LogError("Exception Occured in LightningFlashHistoryData_CSV " + ex);

			}
		}

		private static void LightningFlashData_Xml(XmlTextWriter writer, DateTime startTime, DateTime endTime, string ConnectionString, string tablename, bool IsArchivedDB ,ArrayList dtLtgXMLData)
		{
			double minX = 0;
			double maxX = 0;
			double minY = 0;
			double maxY = 0;

			string bbox = HttpContext.Current.Request.Params["BBOX"];
			string errMsg = string.Empty;
			try
			{
				string[] bboxValues = null;
				if (!string.IsNullOrEmpty(bbox))
				{
					bboxValues = bbox.Split(",".ToCharArray());
					if (bboxValues.Length > 0)
					{
						//minX + "," + maxY + "," + maxX + "," + minY;
						if (!double.TryParse(bboxValues[0], out minX))
							errMsg += " Invalid parameter: minX";
						if (!double.TryParse(bboxValues[2], out maxX))
							errMsg += " Invalid parameter: maxX";
						if (!double.TryParse(bboxValues[3], out minY))
							errMsg += " Invalid parameter: minY";
						if (!double.TryParse(bboxValues[1], out maxY))
							errMsg += " Invalid parameter: maxY";
					}
				}

				if (errMsg != string.Empty)
				{
					System.Diagnostics.Debug.WriteLine(errMsg);
				}

				if (string.IsNullOrEmpty(ConnectionString) && string.IsNullOrEmpty(tablename))
				{
					return;
				}
				#region
				using (SqlConnection cnn = new SqlConnection(ConnectionString))
				{

					string sql = "SELECT Lightning_Time, Latitude, Longitude, Stroke_Type ,Amplitude FROM " + tablename + " (nolock)" +
								  " WHERE Lightning_Time between '" + startTime.ToString("MM/dd/yyyy HH:mm:ss") + "' AND '" + endTime.ToString("MM/dd/yyyy HH:mm:ss") + "'";

					//if ((!string.IsNullOrEmpty(bbox)) && (errMsg == string.Empty) && (bboxValues.Length > 4))//commetned for download link
					if ((!string.IsNullOrEmpty(bbox)) && (errMsg == string.Empty))
					{
						//sql += " AND (Longitude  BETWEEN " +minX + " AND " + maxX + " ) AND " +
						//        "( Latitude BETWEEN " + minY  + " AND " + maxY + ")";

						double Xmin = (minX < maxX) ? minX : maxX;
						double Xmax = (minX > maxX) ? minX : maxX;
						double Ymin = (minY < maxY) ? minY : maxY;
						double Ymax = (minY > maxY) ? minY : maxY;

						sql += " AND (Longitude  BETWEEN " + Xmin + " AND " + Xmax + " ) AND " +
								"( Latitude BETWEEN " + Ymin + " AND " + Ymax + ")";
					}

					if (startTime.ToString("MM") != endTime.ToString("MM") && IsArchivedDB)
					{
						var arctablename = ConfigurationManager.AppSettings["LtgArchiveTableName"] + startTime.ToString("yyyy") + endTime.ToString("MM");
						sql += " UNION SELECT Lightning_Time, Latitude, Longitude, Stroke_Type ,Amplitude FROM " + arctablename + " (nolock)" +
								 " WHERE Lightning_Time between '" + startTime.ToString("MM/dd/yyyy HH:mm:ss") + "' AND '" + endTime.ToString("MM/dd/yyyy HH:mm:ss") + "'";

						//if ((!string.IsNullOrEmpty(bbox)) && (errMsg == string.Empty) && (bboxValues.Length > 4))//commetned for download link
						if ((!string.IsNullOrEmpty(bbox)) && (errMsg == string.Empty))
						{
							//sql += " AND Longitude  BETWEEN " + minX + " AND " + maxX + " AND " +
							//        "Latitude BETWEEN " + minY + " AND " + maxY;

							double Xmin = (minX < maxX) ? minX : maxX;
							double Xmax = (minX > maxX) ? minX : maxX;
							double Ymin = (minY < maxY) ? minY : maxY;
							double Ymax = (minY > maxY) ? minY : maxY;

							sql += " AND (Longitude  BETWEEN " + Xmin + " AND " + Xmax + " ) AND " +
									"( Latitude BETWEEN " + Ymin + " AND " + Ymax + ")";
						}
					}

					System.Diagnostics.Debug.WriteLine("TimeStamp :" + DateTime.Now + ", Lightning Sql command " + sql);
					Aws.Core.Utilities.EventManager.LogInfo("TimeStamp :" + DateTime.Now + ", Lightning Sql command " + sql);

					using (SqlCommand cmd = new SqlCommand(sql, cnn))
					{
						cmd.CommandTimeout = 10 * 60 * 1000; //10Mins.
						cnn.Open();

						//new
						if (IsArchivedDB)
						{
							string ltgArchiveDB = ConfigurationManager.AppSettings["ltgArchiveDB"].ToString();
							if (!string.IsNullOrEmpty(ltgArchiveDB))
							{
								cnn.ChangeDatabase(ltgArchiveDB + startTime.ToString("yyyy"));
							}
							else
							{
								System.Diagnostics.Debug.WriteLine("ltgArchiveDB config key missed in the config file ");
							}
						}

						SqlDataReader reader = cmd.ExecuteReader();
						while (reader != null && reader.Read())
						{
							string key = string.Empty;

							DateTime flashTime = Convert.ToDateTime(reader["Lightning_Time"]);
							Double dLat = Double.Parse(reader["Latitude"].ToString());
							Double dLon = Double.Parse(reader["Longitude"].ToString());
							key = flashTime.ToString("MM/dd/yyyy_HH:mm:ss.fff") + "_" + dLat.ToString("N8") + "_" + dLon.ToString("N8");
								
							//key = flashTime.ToString("MM/dd/yyyy" + "_" + "HH:mm:ss.fff") + "_" + reader["Longitude"] + "_" + reader["Latitude"];							
							
																					
							if(dtLtgXMLData.Contains(key))
								continue;
							else{
								dtLtgXMLData.Add(key);
							}

							writer.WriteStartElement("Placemark");

							string timespan = "<TimeSpan>" +
									 "<begin>" + flashTime.ToString("yyyy-MM-ddTHH:mm:00Z") + "</begin>" +
									 "<end>" + flashTime.AddMinutes(10.0).ToString("yyyy-MM-ddTHH:mm:00Z") + "</end>" +
									 "</TimeSpan>";

							if (reader["Stroke_Type"].ToString() == "1")
							{
								writer.WriteElementString("styleUrl", "#Ltg-IC");
							}
							else
							{
								if (reader["Amplitude"] != DBNull.Value)
								{
									if (double.Parse(reader["Amplitude"].ToString()) > 0)
									{
										writer.WriteElementString("styleUrl", "#CGPositive");
									}
									else
									{
										writer.WriteElementString("styleUrl", "#CGNegative");
									}
								}

								//writer.WriteElementString("styleUrl", "#CGFlash");
							}



							writer.WriteRaw(timespan);
							writer.WriteStartElement("Point");
							writer.WriteElementString("coordinates", reader["Longitude"] + "," + reader["Latitude"] + ",0");
							writer.WriteEndElement(); //Point
							writer.WriteEndElement();//Placemark
							writer.WriteWhitespace(Environment.NewLine);
						}
					}
				}
				#endregion
			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.WriteLine("Exception Occured in LtgFlashData " + ex);
				Aws.Core.Utilities.EventManager.LogError("Exception Occured in LightningFlashData " + ex);
			}

        }






        #region
        #region Pulse  csv data

        public void WriteLtgPulseDataCsv(Stream outputStream, DateTime startTime, DateTime endTime)
        {

            MemoryStream memStream = new MemoryStream();
            StreamWriter writer = new StreamWriter(memStream, Encoding.UTF8);


            string ConnectionString = string.Empty;
            string tablename = string.Empty;

            string LtgArchiveConnString = ConfigurationManager.AppSettings["LightningConnectionString"];
            string LtgArchiveTableName = ConfigurationManager.AppSettings["LtgArchiveTableName"] + startTime.ToString("yyyy") + startTime.ToString("MM");

            string LtgConnectionString = ConfigurationManager.AppSettings["AWSLightningConnString"];
            string LtgTableName = ConfigurationManager.AppSettings["LtgTableName"];

            DateTime currentTime = DateTime.Now.ToUniversalTime();

            TimeSpan startDiff = currentTime - startTime;
            TimeSpan endDiff = currentTime - endTime;

            int _ltgArchiveBackup = 0;//number of months

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["LtgArchiveBackup"]))
            {
                _ltgArchiveBackup = Convert.ToInt32(ConfigurationManager.AppSettings["LtgArchiveBackup"]);
            }

            int ltgTransitionChk = 0; //number of days

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["ltgTransitionChk"]))
            {
                ltgTransitionChk = Convert.ToInt32(ConfigurationManager.AppSettings["ltgTransitionChk"]);
            }


            if ((startDiff.TotalDays > _ltgArchiveBackup) && (endDiff.TotalDays > _ltgArchiveBackup))
            {
                if ((startDiff.TotalDays > ltgTransitionChk) && (endDiff.TotalDays > ltgTransitionChk))
                {
                    LightningPulseDataCsv(writer, startTime, endTime, LtgArchiveConnString, LtgArchiveTableName, true, true);//archieve db
                }
                else
                {
                    LightningPulseHistoryDataCsv(writer, startTime, endTime);//merge
                }
            }
            else if ((startDiff.TotalDays <= _ltgArchiveBackup) && (endDiff.TotalDays <= _ltgArchiveBackup))
            {
                LightningPulseDataCsv(writer, startTime, endTime, LtgConnectionString, LtgTableName, false, true);//live
            }
            else
            {
                LightningPulseHistoryDataCsv(writer, startTime, endTime);//merge
            }

            writer.Close();

            byte[] data = memStream.ToArray();

            outputStream.Write(data, 0, data.Length);
        }

        private static void LightningPulseDataCsv(StreamWriter writer, DateTime startTime, DateTime endTime, string ConnectionString, string tablename, bool IsArchivedDB, bool headerFlag)
        {
            string errMsg = string.Empty;
          
            double north = 0;
            double south = 0;
            double east = 0;
            double west = 0;
            string sep = ",";
            try
            {

                if (HttpContext.Current.Request.Params["north"] != null)
                {
                    if (!double.TryParse(HttpContext.Current.Request.Params["north"].ToString(), out north))
                    {
                        errMsg += "Invalid north";
                    }
                }
                if (HttpContext.Current.Request.Params["south"] != null)
                {
                    if (!double.TryParse(HttpContext.Current.Request.Params["south"].ToString(), out south))
                    {
                        errMsg += "Invalid south";
                    }
                }
                if (HttpContext.Current.Request.Params["east"] != null)
                {
                    if (!double.TryParse(HttpContext.Current.Request.Params["east"].ToString(), out east))
                    {
                        errMsg += "Invalid east";
                    }
                }
                if (HttpContext.Current.Request.Params["west"] != null)
                {
                    if (!double.TryParse(HttpContext.Current.Request.Params["west"].ToString(), out west))
                    {
                        errMsg += "Invalid west";
                    }
                }


                if (string.IsNullOrEmpty(errMsg) && HttpContext.Current.Request.Params["north"] != null && HttpContext.Current.Request.Params["south"] != null && HttpContext.Current.Request.Params["east"] != null && HttpContext.Current.Request.Params["west"] != null)
                {

                    if (errMsg != string.Empty)
                    {
                        Aws.Core.Utilities.EventManager.LogInfo(errMsg);
                    }

                    if (string.IsNullOrEmpty(ConnectionString) && string.IsNullOrEmpty(tablename))
                    {
                        return;
                    }

                    double Xmin = (east < west) ? east : west;
                    double Xmax = (east > west) ? east : west;
                    double Ymin = (north < south) ? north : south; 
                    double Ymax = (north > south) ? north : south;


                    #region
                    using (SqlConnection cnn = new SqlConnection(ConnectionString))
                    {

                        string sql = "SELECT Lightning_Time_String,Lightning_Time, Latitude, Longitude, Stroke_Type ,Amplitude FROM " + tablename + " (nolock)" +
                                      " WHERE Lightning_Time between '" + startTime.ToString("MM/dd/yyyy HH:mm:ss") + "' AND '" + endTime.ToString("MM/dd/yyyy HH:mm:ss") + "'";

                        sql += " AND (Longitude  BETWEEN " + Xmin + " AND " + Xmax + " ) AND " +
                                 "( Latitude BETWEEN " + Ymin + " AND " + Ymax + ")";

                        if (startTime.ToString("MM") != endTime.ToString("MM") && IsArchivedDB)
                        {
                            var arctablename = ConfigurationManager.AppSettings["LtgArchiveTableName"] + startTime.ToString("yyyy") + endTime.ToString("MM");
                            sql += " UNION SELECT Lightning_Time_String,Lightning_Time, Latitude, Longitude, Stroke_Type ,Amplitude FROM " + arctablename + " (nolock)" +
                                     " WHERE Lightning_Time between '" + startTime.ToString("MM/dd/yyyy HH:mm:ss") + "' AND '" + endTime.ToString("MM/dd/yyyy HH:mm:ss") + "'";


                            sql += " AND (Longitude  BETWEEN " + Xmin + " AND " + Xmax + " ) AND " +
                                     "( Latitude BETWEEN " + Ymin + " AND " + Ymax + ")";
                        }


                        System.Diagnostics.Debug.WriteLine("TimeStamp :" + DateTime.Now + ", Pulses Lightning Sql command " + sql);
                        Aws.Core.Utilities.EventManager.LogInfo("TimeStamp :" + DateTime.Now + ",Pulses Lightning Sql command " + sql);

                        int rCount = 0;
                        using (SqlCommand cmd = new SqlCommand(sql, cnn))
                        {
                            cmd.CommandTimeout = 10 * 60 * 1000; //10Mins.
                            cnn.Open();
                            //new
                            if (IsArchivedDB)
                            {
                                string ltgArchiveDB = ConfigurationManager.AppSettings["ltgArchiveDB"].ToString();
                                if (!string.IsNullOrEmpty(ltgArchiveDB))
                                {
                                    cnn.ChangeDatabase(ltgArchiveDB + startTime.ToString("yyyy"));
                                }
                                else
                                {
                                    System.Diagnostics.Debug.WriteLine("ltgArchiveDB config key missed in the config file ");
                                }
                            }

                            SqlDataReader reader = cmd.ExecuteReader();

                            StringBuilder sbFlashData = new StringBuilder();

                            if (GlobalResource.GetUserCulture() == "es-ES")
                            {
                                if (headerFlag)
                                {
                                    sbFlashData.Append("Tiempo_de_Relámpagos_UTC,Latitud,Longitud,Corriente_máximan(kA),Clasificación");
                                }
                            }
                            else
                            {
                                if (headerFlag)
                                {
                                    sbFlashData.Append("Lightning_Time_UTC,Latitude,Longitude,Peak_Current(kA),Classification");
                                }
                            }

                            string ltgTimeinUTC = string.Empty;
                            while (reader != null && reader.Read())
                            {
                                sbFlashData.Append(System.Environment.NewLine);

                                if (reader["Lightning_Time"] != DBNull.Value)
                                {
                                    ltgTimeinUTC = reader["Lightning_Time"].ToString();
                                    if (!string.IsNullOrEmpty(ltgTimeinUTC))
                                    {
                                        string dateformat = Common.GlobalResource.UserDateFormat + " " + "HH:mm:ss.fff";
                                        DateTime flashTime = Convert.ToDateTime(reader["Lightning_Time"]);
                                        // sbFlashData.Append(flashTime.ToString(dateformat));
                                    }
                                    else
                                    {
                                        //sbFlashData.Append(ltgTimeinUTC);
                                    }
                                }

                                if (reader["Lightning_Time_String"] != null)
                                {
                                    sbFlashData.Append(reader["Lightning_Time_String"]);
                                }
                                else
                                {
                                    sbFlashData.Append(string.Empty);
                                }

                                sbFlashData.Append(sep);

                                sbFlashData.Append(reader["Latitude"]);
                                sbFlashData.Append(sep);

                                sbFlashData.Append(reader["Longitude"]);
                                sbFlashData.Append(sep);

                                string amplitude = string.Empty;

                                if (reader["Amplitude"] != DBNull.Value)
                                {
                                    amplitude = reader["Amplitude"].ToString();
                                    if (!string.IsNullOrEmpty(amplitude))
                                    {
                                        sbFlashData.Append(Convert.ToDouble(amplitude) / 1000);
                                    }
                                    else
                                    {
                                        sbFlashData.Append(string.Empty);
                                    }
                                }
                                else
                                {
                                    sbFlashData.Append(string.Empty);
                                }

                                //sbFlashData.Append(reader["Amplitude"]);
                                sbFlashData.Append(sep);

                                if (reader["Stroke_Type"].ToString() == "1")
                                {
                                    sbFlashData.Append("IC");
                                }
                                else
                                {
                                    sbFlashData.Append("CG");
                                }

                                rCount++;
                            }

                            System.Diagnostics.Debug.WriteLine("Number of rows returned from the Ltg pulse query " + rCount);
                            Aws.Core.Utilities.EventManager.LogInfo("  Number of rows returned the Ltg pulse query " + rCount);
                            writer.Write(sbFlashData);
                        }
                    }
                    #endregion
                }

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Exception Occured in LightningPulseDataCsv " + ex);
                Aws.Core.Utilities.EventManager.LogError("Exception Occured in LightningPulseDataCsv " + ex);
                writer.Write(" ");
            }

        }

        public static void LightningPulseHistoryDataCsv(StreamWriter writer, DateTime startTime, DateTime endTime)
        {
            try
            {
                string LtgArchiveConnString = ConfigurationManager.AppSettings["LightningConnectionString"];
                string LtgArchiveTableName = ConfigurationManager.AppSettings["LtgArchiveTableName"] + startTime.ToString("yyyy") + startTime.ToString("MM");
                string LtgHistoryConnectionString = ConfigurationManager.AppSettings["AWSLightningConnString"];
                string LtgHistoryTableName = ConfigurationManager.AppSettings["LtgTableName"];
                string LtgMaintable = ConfigurationManager.AppSettings["LtgTableName"];

                ArrayList arLtgKeys = new ArrayList();

                //get data from History tables								
                LightningPulseHistoryDataFromDatabase(writer, startTime, endTime, LtgArchiveConnString, LtgArchiveTableName, true, true, arLtgKeys);
                Aws.Core.Utilities.EventManager.LogError("Number of keys in the list  : " + arLtgKeys.Count);
                LightningPulseHistoryDataFromDatabase(writer, startTime, endTime, LtgHistoryConnectionString, LtgHistoryTableName + "_History", false, false, arLtgKeys);
                Aws.Core.Utilities.EventManager.LogError("Number of keys in the list  : " + arLtgKeys.Count);

                //Getting data from Main ltg Table
                LightningPulseHistoryDataFromDatabase(writer, startTime, endTime, LtgHistoryConnectionString, LtgMaintable, false, false, arLtgKeys);
                Aws.Core.Utilities.EventManager.LogError("Number of keys in the list  : " + arLtgKeys.Count);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Exception Occured in LightningPulseHistoryDataCsv " + ex);
                Aws.Core.Utilities.EventManager.LogError("Exception Occured in LightningPulseHistoryDataCsv " + ex);
            }
        }

        private static void LightningPulseHistoryDataFromDatabase(StreamWriter writer, DateTime startTime, DateTime endTime, string ConnectionString, string tablename, bool IsArchivedDB, bool headerFlag, ArrayList arLtgkeys)
        {
            string errMsg = string.Empty;
            double north = 0;
            double south = 0;
            double east = 0;
            double west = 0;
            string sep = ",";


            try
            {
                if (HttpContext.Current.Request.Params["north"] != null)
                {
                    if (!double.TryParse(HttpContext.Current.Request.Params["north"].ToString(), out north))
                    {
                        errMsg += "Invalid north";
                    }
                }
                if (HttpContext.Current.Request.Params["south"] != null)
                {
                    if (!double.TryParse(HttpContext.Current.Request.Params["south"].ToString(), out south))
                    {
                        errMsg += "Invalid south";
                    }
                }
                if (HttpContext.Current.Request.Params["east"] != null)
                {
                    if (!double.TryParse(HttpContext.Current.Request.Params["east"].ToString(), out east))
                    {
                        errMsg += "Invalid east";
                    }
                }
                if (HttpContext.Current.Request.Params["west"] != null)
                {
                    if (!double.TryParse(HttpContext.Current.Request.Params["west"].ToString(), out west))
                    {
                        errMsg += "Invalid west";
                    }
                }


                if (string.IsNullOrEmpty(errMsg) && HttpContext.Current.Request.Params["north"] != null && HttpContext.Current.Request.Params["south"] != null && HttpContext.Current.Request.Params["east"] != null && HttpContext.Current.Request.Params["west"] != null)
                {
                    if (errMsg != string.Empty)
                    {
                        System.Diagnostics.Debug.WriteLine(errMsg);
                    }

                    if (string.IsNullOrEmpty(ConnectionString) && string.IsNullOrEmpty(tablename))
                    {
                        return;
                    }


                    double Xmin = (east < west) ? east : west;
                    double Xmax = (east > west) ? east : west;
                    double Ymin = (north < south) ? north : south;
                    double Ymax = (north > south) ? north : south;


                    #region

                    using (SqlConnection cnn = new SqlConnection(ConnectionString))
                    {

                        string sql = "SELECT Lightning_Time_String, Lightning_Time, Latitude, Longitude, Stroke_Type ,Amplitude FROM " +
                                     tablename + " (nolock)" +
                                     " WHERE Lightning_Time between '" + startTime.ToString("MM/dd/yyyy HH:mm:ss") +
                                     "' AND '" + endTime.ToString("MM/dd/yyyy HH:mm:ss") + "'";

                        sql += " AND (Longitude  BETWEEN " + Xmin + " AND " + Xmax + " ) AND " +
                                "( Latitude BETWEEN " + Ymin + " AND " + Ymax + ")";


                        if (startTime.ToString("MM") != endTime.ToString("MM") && IsArchivedDB)
                        {
                            var arctablename = ConfigurationManager.AppSettings["LtgArchiveTableName"] +
                                               startTime.ToString("yyyy") + endTime.ToString("MM");
                            sql += " UNION SELECT Lightning_Time_String,Lightning_Time, Latitude, Longitude, Stroke_Type ,Amplitude FROM " +
                                   arctablename + " (nolock)" +
                                   " WHERE Lightning_Time between '" + startTime.ToString("MM/dd/yyyy HH:mm:ss") +
                                   "' AND '" + endTime.ToString("MM/dd/yyyy HH:mm:ss") + "'";

                            sql += " AND (Longitude  BETWEEN " + Xmin + " AND " + Xmax + " ) AND " +
                                     "( Latitude BETWEEN " + Ymin + " AND " + Ymax + ")";

                        }
                     
                        System.Diagnostics.Debug.WriteLine("TimeStamp :" + DateTime.Now + ", Lightning Pulses Sql command " +
                                                           sql);
                        Aws.Core.Utilities.EventManager.LogInfo("TimeStamp :" + DateTime.Now +
                                                                ", Lightning Pulses Sql command " + sql);

                        int rCount = 0;
                        using (SqlCommand cmd = new SqlCommand(sql, cnn))
                        {
                            cmd.CommandTimeout = 10 * 60 * 1000; //10Mins.
                            cnn.Open();
                            //new
                            if (IsArchivedDB)
                            {
                                string ltgArchiveDB = ConfigurationManager.AppSettings["ltgArchiveDB"].ToString();
                                if (!string.IsNullOrEmpty(ltgArchiveDB))
                                {
                                    cnn.ChangeDatabase(ltgArchiveDB + startTime.ToString("yyyy"));
                                }
                                else
                                {
                                    System.Diagnostics.Debug.WriteLine(
                                        "ltgArchiveDB config key missed in the config file ");
                                }
                            }

                            SqlDataReader reader = cmd.ExecuteReader();

                            StringBuilder sbFlashData = new StringBuilder();

                            if (GlobalResource.GetUserCulture() == "es-ES")
                            {
                                if (headerFlag)
                                {
                                    sbFlashData.Append(
                                        "Tiempo_de_Relámpagos_UTC,Latitud,Longitud,Corriente_máximan(kA),Clasificación");
                                }
                            }
                            else
                            {
                                if (headerFlag)
                                {
                                    sbFlashData.Append(
                                        "Lightning_Time_UTC,Latitude,Longitude,Peak_Current(kA),Classification");
                                }
                            }

                            string ltgTimeinUTC = string.Empty;
                            while (reader != null && reader.Read())
                            {
                                rCount++;

                                string key = string.Empty;

                                sbFlashData.Append(System.Environment.NewLine);

                                if (reader["Lightning_Time"] != DBNull.Value)
                                {
                                    DateTime ltgflashTime = Convert.ToDateTime(reader["Lightning_Time"]);
                                    Double dLat = Double.Parse(reader["Latitude"].ToString());
                                    Double dLon = Double.Parse(reader["Longitude"].ToString());
                                    key = ltgflashTime.ToString("MM/dd/yyyy_HH:mm:ss.fff") + "_" + dLat.ToString("N8") +
                                          "_" + dLon.ToString("N8");
                                }

                                if (string.IsNullOrEmpty(key))
                                    continue;

                                if (arLtgkeys.Contains(key))
                                {
                                    continue;
                                }
                                else
                                {
                                    arLtgkeys.Add(key);
                                }

                                if (reader["Lightning_Time"] != DBNull.Value)
                                {
                                    ltgTimeinUTC = reader["Lightning_Time"].ToString();
                                    if (!string.IsNullOrEmpty(ltgTimeinUTC))
                                    {
                                        string dateformat = Common.GlobalResource.UserDateFormat + " " + "HH:mm:ss.fff";
                                        //DateTime flashTime = Convert.ToDateTime(ltgTimeinUTC);
                                        DateTime flashTime = Convert.ToDateTime(reader["Lightning_Time"]);
                                        //sbFlashData.Append(flashTime.ToString(dateformat));

                                    }
                                    else
                                    {
                                        //sbFlashData.Append(ltgTimeinUTC);
                                    }
                                }
                                //sbFlashData.Append(reader["Lightning_Time"]);							

                                if (reader["Lightning_Time_String"] != null)
                                {
                                    sbFlashData.Append(reader["Lightning_Time_String"]);
                                }
                                else
                                {
                                    sbFlashData.Append(string.Empty);
                                }

                                sbFlashData.Append(sep);

                                sbFlashData.Append(reader["Latitude"]);
                                sbFlashData.Append(sep);

                                sbFlashData.Append(reader["Longitude"]);
                                sbFlashData.Append(sep);

                                string amplitude = string.Empty;

                                if (reader["Amplitude"] != DBNull.Value)
                                {
                                    amplitude = reader["Amplitude"].ToString();
                                    if (!string.IsNullOrEmpty(amplitude))
                                    {
                                        sbFlashData.Append(Convert.ToDouble(amplitude) / 1000);
                                    }
                                    else
                                    {
                                        sbFlashData.Append(string.Empty);
                                    }
                                }
                                else
                                {
                                    sbFlashData.Append(string.Empty);
                                }

                                //sbFlashData.Append(reader["Amplitude"]);
                                sbFlashData.Append(sep);

                                if (reader["Stroke_Type"].ToString() == "1")
                                {
                                    sbFlashData.Append("IC");
                                }
                                else
                                {
                                    sbFlashData.Append("CG");
                                }

                                //sbFlashData.Append(System.Environment.NewLine);				

                            }

                            System.Diagnostics.Debug.WriteLine("Number of rows returned from the  Ltg pulses  query " + rCount);
                            Aws.Core.Utilities.EventManager.LogInfo("  Number of rows returned from Ltg pulses the query " + rCount);
                            writer.Write(sbFlashData);
                            //writer.Close();


                        }
                    }

                    #endregion
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Exception Occured in LightningPulseHistoryDataFromDatabase " + ex);
                Aws.Core.Utilities.EventManager.LogError("Exception Occured in LightningPulseHistoryDataFromDatabase " + ex);
                writer.Write(" ");
            }

        }

        #endregion

        #region pulse kml

        private static void WriteLightningPulseKmzFile(Stream outputStream, DateTime startTime, DateTime endTime)
        {

            string style = @"<Style id=""ICFlash"">" +
                            "<IconStyle><scale>0.25</scale>" +
                            "<Icon><href>https://streamerrt.enterprise.weatherbug.com/LightningAnalysis/images/AWSLTGFlash_1.gif</href></Icon>" +
                            "</IconStyle>" +
                            "</Style>";

            style += @"<Style id=""CGFlash"">" +
                            "<IconStyle><scale>0.25</scale>" +
                            "<Icon><href>https://streamerrt.enterprise.weatherbug.com/LightningAnalysis/images/AWSLTGFlash_0.gif</href></Icon>" +
                            "</IconStyle>" +
                            "</Style>";
            style += @"<Style id=""CGNegative"">" +
                            "<IconStyle><scale>0.5</scale>" +
                            "<Icon><href>https://streamerrt.enterprise.weatherbug.com/LightningAnalysis/images/ltg-cg-negative.png</href></Icon>" +
                            "</IconStyle>" +
                            "</Style>";
            style += @"<Style id=""CGPositive"">" +
                            "<IconStyle><scale>0.5</scale>" +
                            "<Icon><href>https://streamerrt.enterprise.weatherbug.com/LightningAnalysis/images/ltg-cg-positive.png</href></Icon>" +
                            "</IconStyle>" +
                            "</Style>";
            style += @"<Style id=""Ltg-IC"">" +
                            "<IconStyle><scale>0.5</scale>" +
                            "<Icon><href>https://streamerrt.enterprise.weatherbug.com/LightningAnalysis/images/ltg-ic.png</href></Icon>" +
                            "</IconStyle>" +
                            "</Style>";


            MemoryStream memStream = new MemoryStream();
            XmlTextWriter writer = new XmlTextWriter(memStream, null) { Indentation = 4 };

            WriteDocumentHeader(writer);
            writer.WriteRaw(style);


            string ConnectionString = string.Empty;
            string tablename = string.Empty;

            string LtgArchiveConnString = ConfigurationManager.AppSettings["LightningConnectionString"];
            string LtgArchiveTableName = ConfigurationManager.AppSettings["LtgArchiveTableName"] + startTime.ToString("yyyy") + startTime.ToString("MM");

            string LtgConnectionString = ConfigurationManager.AppSettings["AWSLightningConnString"];
            string LtgTableName = ConfigurationManager.AppSettings["LtgTableName"];

            DateTime currentTime = DateTime.Now.ToUniversalTime();
            TimeSpan startDiff = currentTime - startTime;
            TimeSpan endDiff = currentTime - endTime;

            int _ltgArchiveBackup = 0;

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["LtgArchiveBackup"]))
            {
                _ltgArchiveBackup = Convert.ToInt32(ConfigurationManager.AppSettings["LtgArchiveBackup"]);
            }

            int ltgTransitionChk = 0;

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["ltgTransitionChk"]))
            {
                ltgTransitionChk = Convert.ToInt32(ConfigurationManager.AppSettings["ltgTransitionChk"]);
            }

            if ((startDiff.TotalDays > _ltgArchiveBackup) && (endDiff.TotalDays > _ltgArchiveBackup))
            {
                if ((startDiff.TotalDays > ltgTransitionChk) && (endDiff.TotalDays > ltgTransitionChk))
                {
                    LightningPulseData(writer, startTime, endTime, LtgArchiveConnString, LtgArchiveTableName, true);
                }
                else
                {
                    LightningPulseHistoryDataKML(writer, startTime, endTime);
                }
            }
            else if ((startDiff.TotalDays <= _ltgArchiveBackup) && (endDiff.TotalDays <= _ltgArchiveBackup))
            {
                LightningPulseData(writer, startTime, endTime, LtgConnectionString, LtgTableName, false);
            }
            else
            {
                LightningPulseHistoryDataKML(writer, startTime, endTime);
            }


            writer.WriteEndElement(); //</Document>
            writer.WriteEndElement(); //</KML>                
            writer.Flush();
            writer.Close();

            ZipStream(memStream, outputStream);

        }

        private static void LightningPulseData(XmlTextWriter writer, DateTime startTime, DateTime endTime, string ConnectionString, string tablename, bool IsArchivedDB)
        {
            double north = 0;
            double south = 0;
            double east = 0;
            double west = 0;
          
            string errMsg = string.Empty;
            try
            {
                if (HttpContext.Current.Request.Params["north"] != null)
                {
                    if (!double.TryParse(HttpContext.Current.Request.Params["north"].ToString(), out north))
                    {
                        errMsg += "Invalid north";
                    }
                }
                if (HttpContext.Current.Request.Params["south"] != null)
                {
                    if (!double.TryParse(HttpContext.Current.Request.Params["south"].ToString(), out south))
                    {
                        errMsg += "Invalid south";
                    }
                }
                if (HttpContext.Current.Request.Params["east"] != null)
                {
                    if (!double.TryParse(HttpContext.Current.Request.Params["east"].ToString(), out east))
                    {
                        errMsg += "Invalid east";
                    }
                }
                if (HttpContext.Current.Request.Params["west"] != null)
                {
                    if (!double.TryParse(HttpContext.Current.Request.Params["west"].ToString(), out west))
                    {
                        errMsg += "Invalid west";
                    }
                }

                if (string.IsNullOrEmpty(errMsg) && HttpContext.Current.Request.Params["north"] != null && HttpContext.Current.Request.Params["south"] != null && HttpContext.Current.Request.Params["east"] != null && HttpContext.Current.Request.Params["west"] != null)
                {
                    if (errMsg != string.Empty)
                    {
                        Aws.Core.Utilities.EventManager.LogInfo(errMsg);
                    }

                    if (string.IsNullOrEmpty(ConnectionString) && string.IsNullOrEmpty(tablename))
                    {
                        return;
                    }


                    double Xmin = (east < west) ? east : west;
                    double Xmax = (east > west) ? east : west;
                    double Ymin = (north < south) ? north : south;
                    double Ymax = (north > south) ? north : south;


                    #region

                    using (SqlConnection cnn = new SqlConnection(ConnectionString))
                    {

                        string sql = "SELECT Lightning_Time, Latitude, Longitude, Stroke_Type ,Amplitude FROM " +
                                     tablename + " (nolock)" +
                                     " WHERE Lightning_Time between '" + startTime.ToString("MM/dd/yyyy HH:mm:ss") +
                                     "' AND '" + endTime.ToString("MM/dd/yyyy HH:mm:ss") + "'";

                        sql += " AND (Longitude  BETWEEN " + Xmin + " AND " + Xmax + " ) AND " +
                               "( Latitude BETWEEN " + Ymin + " AND " + Ymax + ")";


                        System.Diagnostics.Debug.WriteLine("TimeStamp :" + DateTime.Now + ", Lightning pulses Sql command " +
                                                           sql);
                        Aws.Core.Utilities.EventManager.LogInfo("TimeStamp :" + DateTime.Now +
                                                                ", Lightning pulses Sql command " + sql);


                        using (SqlCommand cmd = new SqlCommand(sql, cnn))
                        {
                            cmd.CommandTimeout = 10 * 60 * 1000; //10Mins.
                            cnn.Open();

                            //new
                            if (IsArchivedDB)
                            {
                                string ltgArchiveDB = ConfigurationManager.AppSettings["ltgArchiveDB"].ToString();
                                if (!string.IsNullOrEmpty(ltgArchiveDB))
                                {
                                    cnn.ChangeDatabase(ltgArchiveDB + startTime.ToString("yyyy"));
                                }
                                else
                                {
                                    System.Diagnostics.Debug.WriteLine(
                                        "ltgArchiveDB config key missed in the config file ");
                                }
                            }

                            SqlDataReader reader = cmd.ExecuteReader();
                            while (reader != null && reader.Read())
                            {
                                writer.WriteStartElement("Placemark");
                                DateTime flashTime = Convert.ToDateTime(reader["Lightning_Time"]);
                                string timespan = "<TimeSpan>" +
                                                  "<begin>" + flashTime.ToString("yyyy-MM-ddTHH:mm:00Z") + "</begin>" +
                                                  "<end>" + flashTime.AddMinutes(10.0).ToString("yyyy-MM-ddTHH:mm:00Z") +
                                                  "</end>" +
                                                  "</TimeSpan>";

                                if (reader["Stroke_Type"].ToString() == "1")
                                {
                                    writer.WriteElementString("styleUrl", "#Ltg-IC");
                                }
                                else
                                {
                                    if (reader["Amplitude"] != DBNull.Value)
                                    {
                                        if (double.Parse(reader["Amplitude"].ToString()) > 0)
                                        {
                                            writer.WriteElementString("styleUrl", "#CGPositive");
                                        }
                                        else
                                        {
                                            writer.WriteElementString("styleUrl", "#CGNegative");
                                        }
                                    }

                                    //writer.WriteElementString("styleUrl", "#CGFlash");
                                }



                                writer.WriteRaw(timespan);
                                writer.WriteStartElement("Point");
                                writer.WriteElementString("coordinates",
                                                          reader["Longitude"] + "," + reader["Latitude"] + ",0");
                                writer.WriteEndElement(); //Point
                                writer.WriteEndElement(); //Placemark
                                writer.WriteWhitespace(Environment.NewLine);
                            }
                        }
                    }

                    #endregion
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Exception Occured in LightningPulseData " + ex);
                Aws.Core.Utilities.EventManager.LogError("Exception Occured in LightningPulseData " + ex);
            }

        }

        public static void LightningPulseHistoryDataKML(XmlTextWriter writer, DateTime startTime, DateTime endTime)
        {
            try
            {
                string LtgArchiveConnString = ConfigurationManager.AppSettings["LightningConnectionString"];
                string LtgArchiveTableName = ConfigurationManager.AppSettings["LtgArchiveTableName"] + startTime.ToString("yyyy") + startTime.ToString("MM");
                string LtgHistoryConnectionString = ConfigurationManager.AppSettings["AWSLightningConnString"];
                string LtgHistoryTableName = ConfigurationManager.AppSettings["LtgTableName"];
                string LtgMainTableName = ConfigurationManager.AppSettings["LtgTableName"];

                ArrayList arrLtgkeys = new ArrayList();
                LightningPulseDataXml(writer, startTime, endTime, LtgArchiveConnString, LtgArchiveTableName, true, arrLtgkeys);
                LightningPulseDataXml(writer, startTime, endTime, LtgHistoryConnectionString, LtgHistoryTableName + "_History", false, arrLtgkeys);
                //Get Data From Main Table
                LightningPulseDataXml(writer, startTime, endTime, LtgHistoryConnectionString, LtgMainTableName, false, arrLtgkeys);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Exception Occured in LightningPulseHistoryDataKML " + ex);
                Aws.Core.Utilities.EventManager.LogError("Exception Occured in LightningPulseHistoryDataKML " + ex);

            }
        }

        private static void LightningPulseDataXml(XmlTextWriter writer, DateTime startTime, DateTime endTime, string ConnectionString, string tablename, bool IsArchivedDB, ArrayList dtLtgXMLData)
        {
         
            double north = 0;
            double south = 0;
            double east = 0;
            double west = 0;

            string errMsg = string.Empty;
            try
            {
                if (HttpContext.Current.Request.Params["north"] != null)
                {
                    if (!double.TryParse(HttpContext.Current.Request.Params["north"].ToString(), out north))
                    {
                        errMsg += "Invalid north";
                    }
                }
                if (HttpContext.Current.Request.Params["south"] != null)
                {
                    if (!double.TryParse(HttpContext.Current.Request.Params["south"].ToString(), out south))
                    {
                        errMsg += "Invalid south";
                    }
                }
                if (HttpContext.Current.Request.Params["east"] != null)
                {
                    if (!double.TryParse(HttpContext.Current.Request.Params["east"].ToString(), out east))
                    {
                        errMsg += "Invalid east";
                    }
                }
                if (HttpContext.Current.Request.Params["west"] != null)
                {
                    if (!double.TryParse(HttpContext.Current.Request.Params["west"].ToString(), out west))
                    {
                        errMsg += "Invalid west";
                    }
                }



                if (string.IsNullOrEmpty(errMsg) && HttpContext.Current.Request.Params["north"] != null && HttpContext.Current.Request.Params["south"] != null && HttpContext.Current.Request.Params["east"] != null && HttpContext.Current.Request.Params["west"] != null)
                {
                    if (errMsg != string.Empty)
                    {
                        System.Diagnostics.Debug.WriteLine(errMsg);
                    }

                    if (string.IsNullOrEmpty(ConnectionString) && string.IsNullOrEmpty(tablename))
                    {
                        return;
                    }


                    double Xmin = (east < west) ? east : west;
                    double Xmax = (east > west) ? east : west;
                    double Ymin = (north < south) ? north : south;
                    double Ymax = (north > south) ? north : south;


                    #region

                    using (SqlConnection cnn = new SqlConnection(ConnectionString))
                    {


                        string sql = "SELECT Lightning_Time, Latitude, Longitude, Stroke_Type ,Amplitude FROM " +
                                     tablename + " (nolock)" +
                                     " WHERE Lightning_Time between '" + startTime.ToString("MM/dd/yyyy HH:mm:ss") +
                                     "' AND '" + endTime.ToString("MM/dd/yyyy HH:mm:ss") + "'";

                        sql += " AND (Longitude  BETWEEN " + Xmin + " AND " + Xmax + " ) AND " +
                                 "( Latitude BETWEEN " + Ymin + " AND " + Ymax + ")";


                        if (startTime.ToString("MM") != endTime.ToString("MM") && IsArchivedDB)
                        {
                            var arctablename = ConfigurationManager.AppSettings["LtgArchiveTableName"] +
                                               startTime.ToString("yyyy") + endTime.ToString("MM");
                            sql += " UNION SELECT Lightning_Time, Latitude, Longitude, Stroke_Type ,Amplitude FROM " +
                                   arctablename + " (nolock)" +
                                   " WHERE Lightning_Time between '" + startTime.ToString("MM/dd/yyyy HH:mm:ss") +
                                   "' AND '" + endTime.ToString("MM/dd/yyyy HH:mm:ss") + "'";

                            sql += " AND (Longitude  BETWEEN " + Xmin + " AND " + Xmax + " ) AND " +
                                 "( Latitude BETWEEN " + Ymin + " AND " + Ymax + ")";

                        }

                        System.Diagnostics.Debug.WriteLine("TimeStamp :" + DateTime.Now + ", Lightning pulses Sql command " + sql);
                        Aws.Core.Utilities.EventManager.LogInfo("TimeStamp :" + DateTime.Now + ", Lightning pulses Sql command " +
                                                                sql);

                        using (SqlCommand cmd = new SqlCommand(sql, cnn))
                        {
                            cmd.CommandTimeout = 10 * 60 * 1000; //10Mins.
                            cnn.Open();

                            //new
                            if (IsArchivedDB)
                            {
                                string ltgArchiveDB = ConfigurationManager.AppSettings["ltgArchiveDB"].ToString();
                                if (!string.IsNullOrEmpty(ltgArchiveDB))
                                {
                                    cnn.ChangeDatabase(ltgArchiveDB + startTime.ToString("yyyy"));
                                }
                                else
                                {
                                    System.Diagnostics.Debug.WriteLine(
                                        "ltgArchiveDB config key missed in the config file ");
                                }
                            }

                            SqlDataReader reader = cmd.ExecuteReader();
                            while (reader != null && reader.Read())
                            {
                                string key = string.Empty;

                                DateTime flashTime = Convert.ToDateTime(reader["Lightning_Time"]);
                                Double dLat = Double.Parse(reader["Latitude"].ToString());
                                Double dLon = Double.Parse(reader["Longitude"].ToString());
                                key = flashTime.ToString("MM/dd/yyyy_HH:mm:ss.fff") + "_" + dLat.ToString("N8") + "_" +
                                      dLon.ToString("N8");

                                //key = flashTime.ToString("MM/dd/yyyy" + "_" + "HH:mm:ss.fff") + "_" + reader["Longitude"] + "_" + reader["Latitude"];							


                                if (dtLtgXMLData.Contains(key))
                                    continue;
                                else
                                {
                                    dtLtgXMLData.Add(key);
                                }

                                writer.WriteStartElement("Placemark");

                                string timespan = "<TimeSpan>" +
                                                  "<begin>" + flashTime.ToString("yyyy-MM-ddTHH:mm:00Z") + "</begin>" +
                                                  "<end>" + flashTime.AddMinutes(10.0).ToString("yyyy-MM-ddTHH:mm:00Z") +
                                                  "</end>" +
                                                  "</TimeSpan>";

                                if (reader["Stroke_Type"].ToString() == "1")
                                {
                                    writer.WriteElementString("styleUrl", "#Ltg-IC");
                                }
                                else
                                {
                                    if (reader["Amplitude"] != DBNull.Value)
                                    {
                                        if (double.Parse(reader["Amplitude"].ToString()) > 0)
                                        {
                                            writer.WriteElementString("styleUrl", "#CGPositive");
                                        }
                                        else
                                        {
                                            writer.WriteElementString("styleUrl", "#CGNegative");
                                        }
                                    }

                                    //writer.WriteElementString("styleUrl", "#CGFlash");
                                }



                                writer.WriteRaw(timespan);
                                writer.WriteStartElement("Point");
                                writer.WriteElementString("coordinates",
                                                          reader["Longitude"] + "," + reader["Latitude"] + ",0");
                                writer.WriteEndElement(); //Point
                                writer.WriteEndElement(); //Placemark
                                writer.WriteWhitespace(Environment.NewLine);
                            }
                        }
                    }

                    #endregion
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Exception Occured in LightningPulseDataXml " + ex);
                Aws.Core.Utilities.EventManager.LogError("Exception Occured in LightningPulseDataXml " + ex);
            }

        }

        #endregion

        #endregion

















        #region 
        #region Pulse  csv data

        public void WriteLtgPulseDataCsv11(Stream outputStream, DateTime startTime, DateTime endTime)
        {

            MemoryStream memStream = new MemoryStream();
            StreamWriter writer = new StreamWriter(memStream, Encoding.UTF8);


            string ConnectionString = string.Empty;
            string tablename = string.Empty;

            string LtgArchiveConnString = ConfigurationManager.AppSettings["LightningConnectionString"];
            string LtgArchiveTableName = ConfigurationManager.AppSettings["LtgArchiveTableName"] + startTime.ToString("yyyy") + startTime.ToString("MM");

            string LtgConnectionString = ConfigurationManager.AppSettings["AWSLightningConnString"];
            string LtgTableName = ConfigurationManager.AppSettings["LtgTableName"];

            DateTime currentTime = DateTime.Now.ToUniversalTime();

            TimeSpan startDiff = currentTime - startTime;
            TimeSpan endDiff = currentTime - endTime;

            int _ltgArchiveBackup = 0;//number of months

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["LtgArchiveBackup"]))
            {
                _ltgArchiveBackup = Convert.ToInt32(ConfigurationManager.AppSettings["LtgArchiveBackup"]);
            }

            int ltgTransitionChk = 0; //number of days

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["ltgTransitionChk"]))
            {
                ltgTransitionChk = Convert.ToInt32(ConfigurationManager.AppSettings["ltgTransitionChk"]);
            }


            if ((startDiff.TotalDays > _ltgArchiveBackup) && (endDiff.TotalDays > _ltgArchiveBackup))
            {
                if ((startDiff.TotalDays > ltgTransitionChk) && (endDiff.TotalDays > ltgTransitionChk))
                {
                    LightningPulseDataCsv(writer, startTime, endTime, LtgArchiveConnString, LtgArchiveTableName, true, true);//archieve db
                }
                else
                {
                    LightningPulseHistoryDataCsv(writer, startTime, endTime);//merge
                }
            }
            else if ((startDiff.TotalDays <= _ltgArchiveBackup) && (endDiff.TotalDays <= _ltgArchiveBackup))
            {
                LightningPulseDataCsv(writer, startTime, endTime, LtgConnectionString, LtgTableName, false, true);//live
            }
            else
            {
                LightningPulseHistoryDataCsv(writer, startTime, endTime);//merge
            }

            writer.Close();

            byte[] data = memStream.ToArray();

            outputStream.Write(data, 0, data.Length);
        }

        public static void LightningPulseHistoryDataCsv11(StreamWriter writer, DateTime startTime, DateTime endTime)
        {
            try
            {
                string LtgArchiveConnString = ConfigurationManager.AppSettings["LightningConnectionString"];
                string LtgArchiveTableName = ConfigurationManager.AppSettings["LtgArchiveTableName"] + startTime.ToString("yyyy") + startTime.ToString("MM");
                string LtgHistoryConnectionString = ConfigurationManager.AppSettings["AWSLightningConnString"];
                string LtgHistoryTableName = ConfigurationManager.AppSettings["LtgTableName"];
                string LtgMaintable = ConfigurationManager.AppSettings["LtgTableName"];

                ArrayList arLtgKeys = new ArrayList();

                //get data from History tables								
                LightningPulseHistoryDataFromDatabase(writer, startTime, endTime, LtgArchiveConnString, LtgArchiveTableName, true, true, arLtgKeys);
                Aws.Core.Utilities.EventManager.LogError("Number of keys in the list  : " + arLtgKeys.Count);
                LightningPulseHistoryDataFromDatabase(writer, startTime, endTime, LtgHistoryConnectionString, LtgHistoryTableName + "_History", false, false, arLtgKeys);
                Aws.Core.Utilities.EventManager.LogError("Number of keys in the list  : " + arLtgKeys.Count);

                //Getting data from Main ltg Table
                LightningPulseHistoryDataFromDatabase(writer, startTime, endTime, LtgHistoryConnectionString, LtgMaintable, false, false, arLtgKeys);
                Aws.Core.Utilities.EventManager.LogError("Number of keys in the list  : " + arLtgKeys.Count);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Exception Occured in LightningPulseHistoryDataCsv " + ex);
                Aws.Core.Utilities.EventManager.LogError("Exception Occured in LightningPulseHistoryDataCsv " + ex);
            }
        }

        #endregion

        #region pulse kml

        public static void LightningPulseHistoryDataKML11(XmlTextWriter writer, DateTime startTime, DateTime endTime)
        {
            try
            {
                string LtgArchiveConnString = ConfigurationManager.AppSettings["LightningConnectionString"];
                string LtgArchiveTableName = ConfigurationManager.AppSettings["LtgArchiveTableName"] + startTime.ToString("yyyy") + startTime.ToString("MM");
                string LtgHistoryConnectionString = ConfigurationManager.AppSettings["AWSLightningConnString"];
                string LtgHistoryTableName = ConfigurationManager.AppSettings["LtgTableName"];
                string LtgMainTableName = ConfigurationManager.AppSettings["LtgTableName"];

                ArrayList arrLtgkeys = new ArrayList();
                LightningPulseDataXml(writer, startTime, endTime, LtgArchiveConnString, LtgArchiveTableName, true, arrLtgkeys);
                LightningPulseDataXml(writer, startTime, endTime, LtgHistoryConnectionString, LtgHistoryTableName + "_History", false, arrLtgkeys);
                //Get Data From Main Table
                LightningPulseDataXml(writer, startTime, endTime, LtgHistoryConnectionString, LtgMainTableName, false, arrLtgkeys);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Exception Occured in LightningPulseHistoryDataKML " + ex);
                Aws.Core.Utilities.EventManager.LogError("Exception Occured in LightningPulseHistoryDataKML " + ex);

            }
        }

        #endregion

        #endregion

    }
}
