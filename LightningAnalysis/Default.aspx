﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="LightningAnalysis._Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<head runat="server">
    <title><%=LightningAnalysis.Common.GlobalResource.GetResourceValue("LightningAnalysis")%></title>

    <link rel="Stylesheet" href="JQuery/jquery-ui-1.10.4.custom/css/redmond/jquery-ui-1.10.4.custom.min.css" />
    <link rel="Stylesheet" href="JQuery/jquery-ui-timepicker-addon.css" />

    <script type="text/javascript" src="JQuery/jquery-ui-1.10.4.custom/js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="JQuery/jquery-ui-1.10.4.custom/js/jquery-ui-1.10.4.custom.min.js"></script>
    <script type="text/javascript" src="JQuery/daterangepicker.min.js"></script>
    <script type="text/javascript" src="JQuery/jquery-ui-timepicker-addon.js"></script>
    <script type="text/javascript" src="Scripts/moment.min.js"></script>

    <script type="text/javascript">
        function calendarPicker(strField) {

            if (null == strField && "" == strField)
                return;
            window.open('CalendarPopUp.aspx?field=' + strField, 'calendarPopup', 'titlebar=0,menubar=0,status=0,width=250,height=250,resizable=yes');
        }

        var userDateformatForCalender = '<%= LightningAnalysis.Common.GlobalResource.UserDateFormat%>';

        function getDateFormForCalendar() {

            if (null == userDateformatForCalender || userDateformatForCalender == "") {
                userDateformatForCalender = "MM/dd/yyyy";
            }
            userDateformatForCalender = userDateformatForCalender.toUpperCase();
            if (userDateformatForCalender == "DD/MM/YYYY")
                return "dd/mm/yy";
            else
                return "mm/dd/yy";

        }

        function ValidateDate(obj) {

            var userDateformat = '<%= LightningAnalysis.Common.GlobalResource.UserDateFormat%>';
            if (null == userDateformat || userDateformat == "") {
                userDateformat = "MM/dd/yyyy";
            }
            var re = "";
            if (userDateformat == "dd/MM/yyyy") {
                re = /^([1-9]|0[1-9]|[12][0-9]|3[01])[- /.]([1-9]|0[1-9]|1[012])[- /.](19|20)\d\d$/; // dd/mm/yyyy or d/m/yyyy 
            }
            else {
                re = /^([1-9]|0[1-9]|1[012])[- /.]([1-9]|0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d$/; // mm/dd/yyyy or m/d/yyyy
            }

            if (null == obj)
                return;
            if (obj.value != "") {
                if (!re.test(obj.value)) {
                    alert('<%=LightningAnalysis.Common.GlobalResource.GetResourceValue("Pleaseenterthedateincorrectformat")%>');

                    //obj.value = "";
                    obj.focus();
                }

            }
        }
        function ValidateTime(obj) {
            //re = /^(\d{1,2}):(\d{2})([ap]m)?$/;
            re = /^(([0-1][0-9])|([2][0-3]))[:]([0-5][0-9])$/;//HH:MM
            if (null == obj)
                return;
            if (obj.value != "") {
                if (!re.test(obj.value)) {
                    alert('<%=LightningAnalysis.Common.GlobalResource.GetResourceValue("PleaseentertheTimeincorrectformat")%>');
                    //obj.value = "";
                    obj.focus();
                }

            }
        }



        function getCookieValue(cookieName) {
            if (document.cookie.length > 0) {
                var cookieStart = document.cookie.indexOf(cookieName + "=");
                if (cookieStart != -1) {
                    cookieStart = cookieStart + cookieName.length + 1;
                    cookieEnd = document.cookie.indexOf(";", cookieStart);
                    if (cookieEnd == -1)
                        cookieEnd = document.cookie.length;
                    return unescape(document.cookie.substring(cookieStart, cookieEnd));
                }
            }
            return "";
        }

        function onPulseChkClick(pulChkobj) {

            var chkLtgCelltrack = document.getElementById("LtgCellTrackingCheckBox");
            var chkLtgAlertPolygon = document.getElementById("LtgAlertPolygonCheckBox");
            var chkNwsAlert = document.getElementById("NWSAlertsCheckBox");
            var chkStormReports = document.getElementById("StormReportsCheckBox");
            var chkRadar = document.getElementById("RadarCheckBox");
            var chkFlashes = document.getElementById("LtgFlashesCheckBox");
            var lnkcsv = document.getElementById("lnkExportCSV");
            //            var txtLat = document.getElementById("txtlatitude");
            //            var txtLng = document.getElementById("txtlongitude"); 
            //            var txtRadius = document.getElementById("txtRadius");

            var txtnorth = document.getElementById("bxtxtNorth");
            var txtsouth = document.getElementById("bxtxtSouth");
            var txteast = document.getElementById("bxtxtEast");
            var txtwest = document.getElementById("bxtxtWest");


            if (null != pulChkobj && pulChkobj.checked) {


                if (null != chkLtgCelltrack && chkLtgCelltrack.checked) {
                    chkLtgCelltrack.checked = false;
                }
                if (null != chkLtgAlertPolygon && chkLtgAlertPolygon.checked) {
                    chkLtgAlertPolygon.checked = false;
                }
                if (null != chkNwsAlert && chkNwsAlert.checked) {
                    chkNwsAlert.checked = false;
                }
                if (null != chkStormReports && chkStormReports.checked) {
                    chkStormReports.checked = false;
                }
                if (null != chkRadar && chkRadar.checked) {
                    chkRadar.checked = false;
                }
                if (null != chkFlashes && chkFlashes.checked) {
                    chkFlashes.checked = false;
                }
                if (null != lnkcsv) {
                    lnkcsv.className = "lnkcsvBlock";
                }

                if (null != txtnorth) {
                    txtnorth.disabled = false;
                    txtnorth.className = "txtBackgroundWhite";
                }
                if (null != txtsouth) {
                    txtsouth.disabled = false;
                    txtsouth.className = "txtBackgroundWhite";
                }
                if (null != txteast) {
                    txteast.disabled = false;
                    txteast.className = "txtBackgroundWhite";
                }
                if (null != txtwest) {
                    txtwest.disabled = false;
                    txtwest.className = "txtBackgroundWhite";
                }

                jQuery('#bxtxtNorth').focus();
            }
            else {

                if (null != lnkcsv) {
                    // lnkcsv.style.display = "none"; 
                    lnkcsv.className = "lnkCSVNone";
                }
                if (null != txtnorth) {
                    txtnorth.disabled = true;
                    txtnorth.className = "BoundingBoxcss";
                }
                if (null != txtsouth) {
                    txtsouth.disabled = true;
                    txtsouth.className = "BoundingBoxcss";
                }
                if (null != txteast) {
                    txteast.disabled = true;
                    txteast.className = "BoundingBoxcss";
                }
                if (null != txtwest) {
                    txtwest.disabled = true;
                    txtwest.className = "BoundingBoxcss";
                }

                /*if (null != txtLat) {
                         txtLat.disabled = true;
                        txtLat.className = "BoundingBoxcss";
                        // txtLat.value = "";
                    
                      }
                if (null != txtLng) {
                       txtLng.disabled = true; 
                        txtLng.className = "BoundingBoxcss";
                        //txtLng.value = "";
                      }
                if(null != txtRadius) {
                     txtRadius.disabled = true; 
                     txtRadius.className = "BoundingBoxcss";
                    //txtRadius.value = "";
                }*/
            }
        }

        function validateLat(txtObj) {

            var re = /^[\-\+]?(([0-9]+)([\.]([0-9]+))?|([\.]([0-9]+))?)$/;
            //var re = /^[-]?[0-9]*[.]{0,1}[0-9]$/ ;
            if (null != txtObj) {
                if (txtObj.value == "") {
                    // alert('Please enter latitude value in correct format(decimal)');
                    //txtObj.focus();
                }
                else if (!re.test(txtObj.value)) {
                    // alert('Please enter latitude values in correct format(decimal)');
                    alert('Please enter the  value in correct format(decimal)');

                    txtObj.focus();
                }
            }

        }

        function validateLng(txtObj) {

            var re = /^[\-\+]?(([0-9]+)([\.]([0-9]+))?|([\.]([0-9]+))?)$/;
            //var re = /^[-]?[0-9]*[.]{0,1}[0-9]$/ ;
            if (null != txtObj) {
                if (txtObj.value == "") {
                    // alert('Please enter longitude values in correct format(decimal)');
                    // txtObj.focus();
                }
                else if (!re.test(txtObj.value)) {
                    alert('Please enter longitude values in correct format(decimal)');

                    txtObj.focus();
                }
            }

        }

        function validateRadius(txtObj) {

            var re = /^[\-\+]?\d+$/;

            if (null != txtObj) {
                if (txtObj.value == "") {

                }
                else if (null != txtObj) {
                    if (!re.test(txtObj.value)) {
                        alert('Please enter the radius value in correct format(int)');

                        txtObj.focus();
                    }
                }
            }

        }

        //Start new start date/end date section

        $(document).ready(function () {

            var tempDateCalendarFormat = getDateFormForCalendar();

            var startTxtBx = 'startDateTimeTxtBox';
            var endTxtBx = 'endDateTimeTxtBox';

            var nowNew = new Date();
            var momentNow = moment(nowNew);//.format('MM/DD/YYYY h:mm A ZZ');

            var maxDateMom = moment(momentNow).subtract(180, 'days').add(30, 'minutes');
            var startMom = moment(momentNow).subtract(6, 'hours');
            var endMom = moment(momentNow);
            var lastyear = (nowNew.getFullYear() - 1);
            var currYearRange = lastyear.toString() + ":" + nowNew.getFullYear();
            moment.createFromInputFallback = function (config) { config._d = new Date(config._i); };
            var startDTP = jQuery('#' + startTxtBx).datetimepicker({
                dateFormat: tempDateCalendarFormat,
                timeFormat: 'h:mm TT z',
                stepMinute: 15,
                alwaysSetTime: true,
                useLocalTimezone: true,
                minDate: new Date(maxDateMom),
                maxDate: moment(nowNew).format(userDateformatForCalender + ' h:mm A ZZ'),
                changeYear: true,
                yearRange: currYearRange
            });

            var endDTP = jQuery('#' + endTxtBx).datetimepicker({
                dateFormat: tempDateCalendarFormat,
                timeFormat: 'h:mm TT z',
                stepMinute: 15,
                alwaysSetTime: true,
                useLocalTimezone: true,
                minDate: new Date(maxDateMom),
                maxDate: moment(nowNew).format(userDateformatForCalender + ' h:mm A ZZ'),
                changeYear: true,
                yearRange: currYearRange
            });

            jQuery('#' + startTxtBx).datetimepicker('setDate', new Date(startMom));
            jQuery("#StartDate").val(moment.utc(startMom).format(userDateformatForCalender));
            jQuery('#StartTime option[value="' + moment.utc(jQuery('#' + startTxtBx).val()).format('HH:mm') + '"]').prop('selected', true);

            jQuery('#' + endTxtBx).datetimepicker('setDate', new Date(endMom));
            jQuery("#EndDate").val(moment.utc(endMom).format(userDateformatForCalender));
            jQuery('#EndTime option[value="' + moment.utc(jQuery('#' + endTxtBx).val()).format('HH:mm') + '"]').prop('selected', true);

            jQuery('#' + startTxtBx).attr("readonly", "readonly");
            jQuery('#' + endTxtBx).attr("readonly", "readonly");
            jQuery("#UTCStartDate").text(moment.utc(jQuery('#' + startTxtBx).val()).format('MM/DD/YYYY h:mm A ZZ'));
            jQuery("#UTCEndDate").text(moment.utc(jQuery('#' + endTxtBx).val()).format('MM/DD/YYYY h:mm A ZZ'));


            //if start date become more than 6 hours of end date than we need to adjust end date
            jQuery('#' + startTxtBx).change(function (e) {
                if (!(jQuery('#' + startTxtBx).val() == "")) {
                    var startDTstr = moment(jQuery('#' + startTxtBx).val());
                    var endDTstr = moment(jQuery('#' + endTxtBx).val());
                    var duration = moment.duration(endDTstr.diff(startDTstr));
                    var minDiff = duration.asMinutes();

                    jQuery("#StartDate").val(moment.utc(startDTstr).format(userDateformatForCalender));
                    jQuery('#StartTime option[value="' + moment.utc(startDTstr).format('HH:mm') + '"]').prop('selected', true);
                    jQuery("#UTCStartDate").text(moment.utc(jQuery('#' + startTxtBx).val()).format('MM/DD/YYYY h:mm A ZZ'));

                    //if (minDiff > 360) {
                    //    jQuery('#' + endTxtBx).val(moment(startDTstr).add(6, 'hours').format(userDateformatForCalender +' h:mm A ZZ'));
                    //}
                    //else if (minDiff <= 0) {
                    //    jQuery('#' + endTxtBx).val(moment(startDTstr).add(1, 'hours').format(userDateformatForCalender+' h:mm A ZZ'));
                    //}
                    //else {
                    //    //everything is good. Remove any classes
                    //}

                    //jQuery("#EndDate").val(moment.utc(jQuery('#' + endTxtBx).val()).format(userDateformatForCalender));
                    //jQuery('#EndTime option[value="' + moment.utc(jQuery('#' + endTxtBx).val()).format('HH:mm') + '"]').prop('selected', true);

                }
            });

            jQuery('#' + endTxtBx).change(function (e) {
                if (!(jQuery('#' + endTxtBx).val() == "")) {
                    var startDTstr = moment(jQuery('#' + startTxtBx).val());
                    var endDTstr = moment(jQuery('#' + endTxtBx).val());
                    var duration = moment.duration(endDTstr.diff(startDTstr));
                    var minDiff = duration.asMinutes();

                    jQuery("#EndDate").val(moment.utc(endDTstr).format(userDateformatForCalender));
                    jQuery('#EndTime option[value="' + moment.utc(endDTstr).format('HH:mm') + '"]').prop('selected', true);
                    jQuery("#UTCEndDate").text(moment.utc(jQuery('#' + endTxtBx).val()).format('MM/DD/YYYY h:mm A ZZ'));

                    ////console.log("END UTC: " + moment.utc(endDTstr).format('MM/DD/YYYY h:mm A ZZ'));

                    //if (minDiff > 360) {
                    //    jQuery('#' + startTxtBx).val(moment(endDTstr).add(-6, 'hours').format(userDateformatForCalender +' h:mm A ZZ'));
                    //}
                    //else if (minDiff <= 0) {
                    //    jQuery('#' + startTxtBx).val(moment(endDTstr).add( -1, 'hours').format(userDateformatForCalender +' h:mm A ZZ'));
                    //}
                    //else {
                    //    //everything is good. Remove any classes

                    //}

                    //jQuery("#StartDate").val(moment.utc(jQuery('#' + startTxtBx).val()).format(userDateformatForCalender));
                    //jQuery('#StartTime option[value="' + moment.utc(jQuery('#' + startTxtBx).val()).format('HH:mm') + '"]').prop('selected', true);

                }
            });
        });

    </script>
    <style type="text/css">
        .dateformatlblstyle {
            padding: 3px 3px 3px 3px;
            font-size: 9pt;
            color: Red;
        }

        #header_bg {
            position: absolute;
            float: right;
            background-image: url(images/top_bg_img.gif);
            background-repeat: repeat-x;
            width: 880px;
            min-height: 31px;
            margin-left: 100px;
            background-position-y: top;
            background-position-x: 350px;
        }

        #logo {
            float: left;
            padding: 5px;
        }

        #header {
            MARGIN: 0px 0px 0px 0px;
            /*WIDTH: 1278px;*/
            width: 100%;
        }

        .header_bgCss {
            background-image: url(images/top_bg_img.gif);
            background-repeat: repeat-x;
        }

        .BoundingBoxcss {
            padding: 6px;
            margin: 3px;
            color: #939192;
            min-width: 200px;
            background: #f5f5f5 !important;
            border: 1px solid #939192;
        }

        .txtBackgroundWhite {
            padding: 6px;
            margin: 3px;
            color: #939192;
            min-width: 200px;
            border: 1px solid #000;
        }

        .lnkCSVNone {
            display: none;
        }

        .lnkcsvBlock {
            display: block;
        }

        #ui-datepicker-div {
            font-family: Arial;
            font-size: 11pt;
        }

        .ui-widget-header {
            background: #0e248a;
        }

            .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {
                border: 1px solid #c7e1c4;
                color: #0e248a;
                background: #c7e1c4;
            }


        .startendLbl {
            font-size: 10pt;
            margin-top: -22px;
            display: inline-block;
            min-width: 75px;
            text-align: right;
        }

        .datetimeTxtBox {
            cursor: pointer;
            cursor: hand;
            padding: 6px;
            margin: 3px;
            color: #939192;
            min-width: 200px;
            background: #f5f5f5 !important;
            border: 1px solid #939192;
        }

        .oldstartendaspnet {
            display: none;
        }

        .labelutc {
            font-size: 13.333px;
            padding-right: 7px;
            background: none !important;
            border: none !important;
            display: inline-block;
            text-align: left;
        }

        .label-utc {
            margin-top: -25px;
        }
    </style>

</head>
<%--<body style="margin: 150px 150px 150px 150px;font-family:Verdana">--%>
<%--<body style="margin: 80px 10px 10px 50px;font-family:Verdana">--%>
<body style="font-family: Verdana; margin: 0px 0px 0px 0px;">
    <form id="Ltgform" runat="server">

        <table cellpadding="0" cellspacing="0" style="width: 100%">
            <tr>
                <td style="" valign="top">
                    <table style="width: 100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="top" width="30%" align="left">
                                <img src="images/loginLogo.jpg" alt="Earth Networks" title='EarthNetworks' />
                            </td>
                            <td width="2%" valign="top">
                                <asp:Image ID="Image1" ImageUrl="images/top_bg_corner.gif" alt="" Style="float: left" runat="server" />
                            </td>
                            <td width="58%" class="header_bgCss" valign="top"></td>
                        </tr>
                    </table>
                    <%--<div id="header">
      
            <img src="images/loginLogo.jpg" alt="WeatherBug Professional" title='WeatherBugProfessional' />
         
        <div id="header_bg"><asp:Image ID="TopImg" ImageUrl="images/top_bg_corner.gif"  alt="" style="float:left" runat="server" />        
        </div> 
    </div>--%>

                </td>
            </tr>
            <tr>
                <td style="width: 100%" align="center">
                    <table style="margin: 60px 10px 10px 10px">
                        <tr>
                            <td align="center">
                                <span style="padding: 3px 3px 3px 3px; font-weight: bold; font-size: 20px;">
                                    <%=LightningAnalysis.Common.GlobalResource.GetResourceValue("LightningDataAnalysis")%></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table style="border: 1px solid #C0C0C0;" cellpadding="10" cellspacing="0" border="0">
                                    <tr>
                                        <td colspan="4" align="center">
                                            <strong><%=LightningAnalysis.Common.GlobalResource.GetResourceValue("EntertheDateTimeRange")%></strong>
                                        </td>
                                    </tr>
                                    <%--begin new dt picker--%>
                                    <tr>
                                        <td nowrap align="" colspan="4">
                                            <%--<asp:Label ID="Label1" Text="Label" runat="server" Font-Size="10pt"></asp:Label>--%>
                                            <table cellpadding="6" cellspacing="5" border="0" width="100%">
                                                <tr>
                                                    <td width="50%" align="right">
                                                        <asp:Label CssClass="startendLbl" ID="Label1" Text="Start Date" runat="server"></asp:Label>
                                                        <asp:TextBox CssClass="datetimeTxtBox " ID="startDateTimeTxtBox" runat="server" ClientIDMode="Static"></asp:TextBox>
                                                        <%-- <asp:Label CssClass="datetimeTxtBox labelutc" ID="UTCStartDate" Text="w" runat="server"></asp:Label>--%>
                                                        <p style="font-size: 13.333px; margin: 0px; font-family: Arial; margin-right: 53px;" id="UTCStartDate"></p>
                                                    </td>
                                                    <td width="50%" align="left">
                                                        <asp:Label CssClass="startendLbl" ID="Label2" Text="End Date" runat="server"></asp:Label>
                                                        <asp:TextBox CssClass="datetimeTxtBox " ID="endDateTimeTxtBox" runat="server" ClientIDMode="Static"></asp:TextBox>
                                                        <p style="font-size: 13.333px; margin: 0px; font-family: Arial; margin-left: 90px;" id="UTCEndDate"></p>
                                                        <%--<asp:Label CssClass="datetimeTxtBox labelutc" ID="UTCEndDate" Text="w" runat="server"></asp:Label>--%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <%--end new dt picker--%>
                                    <tr class="oldstartendaspnet">
                                        <td nowrap align="right">
                                            <asp:Label ID="lblStartDate" runat="server" Font-Size="10pt"></asp:Label>
                                        </td>
                                        <td nowrap align="left">
                                            <asp:TextBox ID="StartDate" ClientIDMode="Static" runat="server"></asp:TextBox>
                                            <a href="#" title="Pick Date from Calendar">
                                                <img src="Images/calendar.png" onclick="calendarPicker('StartDate');"
                                                    alt="Pick Date from Calendar" border="0px" /></a>
                                            <asp:Label ID="lbldateformat" runat="server" CssClass="dateformatlblstyle"></asp:Label>
                                        </td>
                                        <td nowrap align="right">
                                            <asp:Label ID="lblStartTime" runat="server" Font-Size="10pt"></asp:Label>
                                        </td>
                                        <td nowrap align="left">
                                            <asp:DropDownList ID="StartTime" ClientIDMode="Static" runat="server">
                                            </asp:DropDownList>
                                            <span style="padding: 3px 3px 3px 3px; font-size: 9pt; color: Red"><%=LightningAnalysis.Common.GlobalResource.GetResourceValue("HHmmUTC24hr")%></span>
                                        </td>
                                    </tr>
                                    <tr class="oldstartendaspnet">
                                        <td nowrap align="right">
                                            <asp:Label ID="lblEndDate" runat="server" Font-Size="10pt"></asp:Label>
                                        </td>
                                        <td nowrap align="left">
                                            <asp:TextBox ID="EndDate" runat="server"></asp:TextBox>
                                            <a href="#" title="<%=LightningAnalysis.Common.GlobalResource.GetResourceValue("PickDatefromCalendar")%>">
                                                <img src="Images/calendar.png" onclick="calendarPicker('EndDate');"
                                                    alt="<%=LightningAnalysis.Common.GlobalResource.GetResourceValue("PickDatefromCalendar")%>" border="opx" /></a>
                                            <asp:Label ID="endlblDateformat" runat="server" CssClass="dateformatlblstyle"></asp:Label>
                                        </td>
                                        <td nowrap align="right">
                                            <asp:Label ID="lblEndTime" runat="server" Font-Size="10pt"></asp:Label>
                                        </td>
                                        <td nowrap align="left">
                                            <asp:DropDownList ID="EndTime" runat="server">
                                            </asp:DropDownList>
                                            <span style="padding: 3px 3px 3px 3px; font-size: 9pt; color: Red"><%=LightningAnalysis.Common.GlobalResource.GetResourceValue("HHmmUTC24hr")%></span>
                                        </td>
                                    </tr>
                                    <tr runat="server">
                                        <td nowrap align="right" colspan="2">
                                            <asp:Label ID="lblNorth" CssClass="startendLbl" runat="server" Font-Size="10pt"></asp:Label>
                                            <asp:TextBox ID="txtNorth" runat="server" ReadOnly="true" CssClass="BoundingBoxcss"></asp:TextBox>
                                        </td>
                                        <td nowrap align="left" colspan="2">
                                            <asp:Label ID="lblSouth" CssClass="startendLbl" runat="server" Font-Size="10pt"></asp:Label>
                                            <asp:TextBox ID="txtSouth" runat="server" ReadOnly="true" CssClass="BoundingBoxcss"></asp:TextBox>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td nowrap align="right" colspan="2">
                                            <asp:Label ID="lblEast" CssClass="startendLbl" runat="server" Font-Size="10pt"></asp:Label>
                                            <asp:TextBox ID="txtEast" runat="server" ReadOnly="true" CssClass="BoundingBoxcss"></asp:TextBox>
                                        </td>
                                        <td nowrap align="left" colspan="2">
                                            <asp:Label ID="lblWest" CssClass="startendLbl" runat="server" Font-Size="10pt"></asp:Label>
                                            <asp:TextBox ID="txtWest" runat="server" ReadOnly="true" CssClass="BoundingBoxcss"></asp:TextBox>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td nowrap align="right" colspan="2">
                                            <asp:Label ID="lblEmail" CssClass="startendLbl" runat="server" Font-Size="10pt"></asp:Label>
                                            <asp:TextBox ID="txtEmail" runat="server" ReadOnly="false" CssClass="BoundingBoxcss"></asp:TextBox>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td nowrap align="right" colspan="2">
                                            <asp:Label ID="lblOutputFormat" CssClass="startendLbl" runat="server" Font-Size="10pt"></asp:Label>
                                            <asp:RadioButton ID="rdbkmz" Text="Export.kmz" runat="server" GroupName="rblOutputFormat" Checked Font-Size="10pt" />
                                            <asp:RadioButton ID="rdbcsv" Text="Export.csv" runat="server" GroupName="rblOutputFormat" Style="font-size: 10pt; margin-right: 25px;" />
                                        </td>
                                    </tr>

                                    <tr>
                                        <td nowrap align="right" colspan="2">
                                            <asp:CheckBox ID="cbSendProgressNotifications" Checked runat="server" AutoPostBack="false" Text="Send Progress Notifications" Style="font-size: 10pt;" />&nbsp;&nbsp;
                                        </td>
                                    </tr>

                                    <tr>
                                        <td nowrap align="right" colspan="2">
                                            <asp:Button ID="btnDownloadButton" runat="server" Font-Size="10pt" OnClick="btnDownloadButton_Click" Text="SUBMIT" Style="margin-right: 144px;"></asp:Button>

                                        </td>
                                    </tr>

                                    <!-- lat long !-->
                                    <tr>
                                        <td colspan="4">
                                            <asp:Panel ID="pnlLocation" runat="server">

                                                <fieldset>
                                                    <legend style="font-size: 10pt"><strong>Location Details</strong>
                                                    </legend>
                                                    <table align="center" border="0" width="100%">
                                                        <%--  <tr>
                                                         <td nowrap align="" style="padding:5px">
                                                             <asp:Label ID="lbllatitude" runat="server" Font-Size="10pt" >Latitude</asp:Label>
                                                         </td>
                                                         <td nowrap align="" style="padding:5px">
                                                             <asp:TextBox ID="txtlatitude" runat="server"    CssClass= "BoundingBoxcss" ></asp:TextBox>
                                                         </td>
                                                         <td nowrap align="" style="padding:5px">
                                                             <asp:Label ID="lbllongitude" runat="server" Font-Size="10pt">Longitude</asp:Label>
                                                         </td>
                                                         <td nowrap align="" style="padding:5px">
                                                             <asp:TextBox ID="txtlongitude" runat="server" CssClass= "BoundingBoxcss" ></asp:TextBox>
                                                         </td>
                                                          <td nowrap align="" style="padding:5px">
                                                             <asp:Label ID="lblRadius" runat="server" Font-Size="10pt">Radius</asp:Label>
                                                         </td>
                                                          <td nowrap align="" style="padding:5px">
                                                             <asp:TextBox ID="txtRadius" runat="server"  CssClass= "BoundingBoxcss"  ></asp:TextBox>
                                                              <asp:Label ID="lblUnits" runat="server" Text="(Miles)" Font-Size="8pt" ForeColor="Red"></asp:Label>
                                                         </td>

                                                     </tr>--%>
                                                        <tr>
                                                            <td nowrap align="right">
                                                                <asp:Label ID="bxlblNorth" runat="server" CssClass="startendLbl" Font-Size="10pt">North</asp:Label>
                                                                <asp:TextBox ID="bxtxtNorth" runat="server" CssClass="BoundingBoxcss"></asp:TextBox>
                                                            </td>
                                                            <td nowrap align="left">
                                                                <asp:Label ID="bxlblSouth" runat="server" CssClass="startendLbl" Font-Size="10pt">South</asp:Label>
                                                                <asp:TextBox ID="bxtxtSouth" runat="server" CssClass="BoundingBoxcss"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td nowrap align="right">
                                                                <asp:Label ID="bxlblEast" runat="server" CssClass="startendLbl" Font-Size="10pt">East</asp:Label>
                                                                <asp:TextBox ID="bxtxtEast" runat="server" CssClass="BoundingBoxcss"></asp:TextBox>
                                                            </td>
                                                            <td nowrap align="left">
                                                                <asp:Label ID="bxlblWest" runat="server" CssClass="startendLbl" Font-Size="10pt">West</asp:Label>
                                                                <asp:TextBox ID="bxtxtWest" runat="server" CssClass="BoundingBoxcss"></asp:TextBox>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td colspan="2" align="center">
                                                                <asp:Label ID="lbllocnote" runat="server" Text="(Note: Please select the Ltg Pulses layer to enable the Location Details)" Font-Size="8pt"></asp:Label></td>
                                                        </tr>
                                                    </table>
                                                </fieldset>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <!--lat -->

                                    <tr>
                                        <td colspan="4">
                                            <asp:Panel ID="PanelChkboxes" runat="server">

                                                <fieldset style="padding: 10px; font-size: 10pt;">
                                                    <legend><strong><%=LightningAnalysis.Common.GlobalResource.GetResourceValue("SelectLayers")%></strong></legend>
                                                    <br />
                                                    <div id="chkboxGroup">
                                                        <asp:CheckBox ID="LtgCellTrackingCheckBox" Checked runat="server" AutoPostBack="false" Text="Ltg Cell Tracking" />&nbsp;&nbsp;
                                    <asp:CheckBox ID="LtgAlertPolygonCheckBox" Checked runat="server" AutoPostBack="false" Text="Ltg Alert Polygon" />&nbsp;&nbsp;
                                    <asp:CheckBox ID="NWSAlertsCheckBox" Checked runat="server" AutoPostBack="false" Text="NWS Alerts" />&nbsp;&nbsp;
                                    <asp:CheckBox ID="StormReportsCheckBox" Checked runat="server" AutoPostBack="false" Text="Storm Reports" />&nbsp;&nbsp;
                                    <asp:CheckBox ID="RadarCheckBox" runat="server" AutoPostBack="false" Text="Radar" />&nbsp;&nbsp;
                                    <asp:CheckBox ID="LtgFlashesCheckBox" runat="server" AutoPostBack="false" Text="Ltg Flashes" />&nbsp;&nbsp;
                                    <asp:CheckBox ID="LtgPulsesCheckBox" runat="server" AutoPostBack="false" Text="Ltg Pulses" onclick="onPulseChkClick(this);" />&nbsp;&nbsp;
                                                    </div>
                                                </fieldset>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <%--<tr>
                <td colspan="4">
                    <span style="padding:3px 3px 3px 3px; font-family:Arial;font-size:6pt;color:Red"> Note: StartDate and EndDate Should be in mm/dd/yyyy format.  </span></br>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="padding:3px 3px 3px 3px; font-family:Arial;font-size:6pt;color:Red">StartTime and EndTime should be in HH:mm (UTC Time-24hr format)</span>
                </td>
            </tr>--%>
                                    <tr>
                                        <td colspan="4" align="center" style="font-size: 10pt">
                                            <div style="text-align: left; padding-left: 15%; padding-right: 15%; line-height: 1.5em;">
                                                <ul>
                                                    <li><%=LightningAnalysis.Common.GlobalResource.GetResourceValue("NoteYoucannotdownloadolderthan6months")%></li>
                                                    <li><%=LightningAnalysis.Common.GlobalResource.GetResourceValue("ArchiveNote")%> <a href="mailto:salesops@earthnetworks.com">salesops@earthnetworks.com </a></li>
                                                    
                                                    <%--<li runat="server" id="liNoteDownload"></li>
                                                    <li ><span runat="server" id="liArchiveNote"></span> <a href="mailto:salesops@earthnetworks.com">salesops@earthnetworks.com </a></li>--%>
                                                    

                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" align="center" nowrap>

<%--                                            <asp:LinkButton ID="DownLoadLnk" runat="server" Font-Size="10pt" OnClick="DownLoadLnk_Click"><%=LightningAnalysis.Common.GlobalResource.GetResourceValue("DownLoadKmzFile")%>  </asp:LinkButton>&nbsp;&nbsp;--%>
                                
                                            <asp:LinkButton ID="lnkDownloadKml" runat="server" Font-Size="10pt" OnClick="lnkDownloadKml_Click" ToolTip="DownLoad as .kmz">DownLoad as .kmz</asp:LinkButton>
                                            &nbsp;
                                
                                            <asp:LinkButton ID="lnkGO" runat="server" OnClick="lnkGO_Click" Font-Size="10pt"><%=LightningAnalysis.Common.GlobalResource.GetResourceValue("ViewinGoogleEarth")%>  </asp:LinkButton>
                                            &nbsp;&nbsp;
                                            
<%--                                            <asp:LinkButton ID="lnkDownloadCSV" runat="server" Font-Size="10pt" OnClick="lnkDownloadCSV_Click"><%=LightningAnalysis.Common.GlobalResource.GetResourceValue("DownloadTOCSVFile")%></asp:LinkButton>--%>

                                            <asp:LinkButton ID="lnkExportCSV" runat="server" Font-Size="10pt" OnClick="lnkExportCSV_Click">Export to CSV</asp:LinkButton>

                                            <br />
<%--                                            <asp:Label ID="lblmessage" runat="server" Font-Size="8pt"></asp:Label>--%>

                                            <%--<span style="font-size:8pt;" ><%=LightningAnalysis.Common.GlobalResource.GetResourceValue("ClicktoexportkmzfileforGoogleEarth")%></span>--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" align="center"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>

                </td>
            </tr>

        </table>

    </form>
</body>
</html>
