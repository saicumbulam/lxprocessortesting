﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Globalization;
using System.Threading;
using LightningAnalysis.Common;


namespace LightningAnalysis
{
	public partial class CalendarPopUp : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{

			try
			{
				string sCultureCookie = string.Empty;
				if (HttpContext.Current.Request.Cookies["CultureCookie"] != null)
				{
					sCultureCookie = HttpContext.Current.Request.Cookies["CultureCookie"].Value;
				}
				if (string.IsNullOrEmpty(sCultureCookie) && (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["DefaultCulture"])))
				{
					sCultureCookie = ConfigurationManager.AppSettings["DefaultCulture"];
				}

				CultureInfo culture = CultureInfo.CreateSpecificCulture(sCultureCookie);
				culture.DateTimeFormat.Calendar = culture.Calendar;
				Thread.CurrentThread.CurrentCulture = culture;
				Thread.CurrentThread.CurrentUICulture = culture;
				this.Calendar1.Attributes.Add("title", string.Empty);
				ScriptManager.RegisterStartupScript(this, typeof(string), "CalenderTooltip", "javascript:calenderGoToTooTip();", true);   
			}
			catch(Exception ex)
			{
				Aws.Core.Utilities.EventManager.LogError("Exception occurred in setting the culture to the calendar control" + ex);
				System.Diagnostics.Debug.WriteLine("Exception occurred in setting the culture to the calendar control" + ex);
			}

		}

		protected void Calendar1_DayRender(object sender, DayRenderEventArgs e)
		{
			// Clear the link from this day
			e.Cell.Controls.Clear();

			// Add the custom link
			System.Web.UI.HtmlControls.HtmlGenericControl Link = new System.Web.UI.HtmlControls.HtmlGenericControl();
			Link.TagName = "a";
			Link.InnerText = e.Day.DayNumberText;			

			string dtformat = GlobalResource.UserDateFormat;

			if(string.IsNullOrEmpty(dtformat))
			{
				dtformat = "MM/dd/yyyy";
			}

			string strDate = e.Day.Date.ToString(dtformat);			

			//Link.Attributes.Add("href", String.Format("JavaScript:window.opener.document.{0}.value = \'{1:d}\'; window.close();", Request.QueryString["field"], e.Day.Date));
			Link.Attributes.Add("href", String.Format("JavaScript:window.opener.document.getElementById('Ltgform').{0}.value = \'{1:d}\'; window.close();", Request.QueryString["field"], strDate));

			// By default, this will highlight today's date.
			if (e.Day.IsSelected)
			{
				Link.Attributes.Add("style", this.Calendar1.SelectedDayStyle.ToString());
			}

			// Now add our custom link to the page
			e.Cell.Controls.Add(Link);
		}	
		
	}
}
