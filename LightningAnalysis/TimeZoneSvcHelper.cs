﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

using Aws.Core.TimeZoneServiceRef;

namespace LightningAnalysis
{
    public static class TimeZoneSvcHelper
    {
        //TimeZone Cache - Key  lat.ToString("N8")_lon.ToString("N8");
        private static Dictionary<string, TimeZoneData> _TimeZoneCache = new Dictionary<string, TimeZoneData>();

        public static TimeZoneData GetTimeZone(double latitude, double longitude)
        {
            string cacheKey = latitude.ToString("N8") + "_" + longitude.ToString("N8");
            TimeZoneData data = null;
            if (_TimeZoneCache.ContainsKey(cacheKey))
            {
                data = _TimeZoneCache[cacheKey];
            }
            else
            {
                TimeZoneServiceClient client = new TimeZoneServiceClient();
                try
                {
                    if (!((client.State == CommunicationState.Created) ||
                            (client.State != CommunicationState.Opened) ||
                            (client.State != CommunicationState.Faulted)))
                    {
                        client.Abort();
                        client = new TimeZoneServiceClient();
                    }
                    
                    data = client.GetTimeZone(latitude, longitude);
                    if (data != null)
                    {
                        _TimeZoneCache[cacheKey] = data;
                    }
                }
                finally
                {
                    try
                    {
                        if ((client != null) && (client.State != CommunicationState.Closed))
                        {
                            if (client.State == CommunicationState.Faulted)
                                client.Abort();
                            else
                                client.Close();
                        }
                    }
                    catch (CommunicationException)
                    {
                        client.Abort();
                    }
                    catch (TimeoutException)
                    {
                        client.Abort();
                    }
                    catch (System.Exception)
                    {
                        throw;
                    }
                }
            }

            return data;
        }

        public static DateTime UtcToLocal(DateTime dtUTC, double latitude, double longitude)
        {
            if (dtUTC == DateTime.MinValue)
                return DateTime.MinValue;

            TimeZoneData data = GetTimeZone(latitude, longitude);            

            if (data != null)
            {
                DateTime adjustedTime = dtUTC.AddHours(data.TimeZoneOffset);
                if ((adjustedTime >= data.DayLightSavingStartTime) && (adjustedTime <= data.DayLightSavingEndTime))
                {
                    adjustedTime = adjustedTime.AddHours(1);
                }
                //data.TimeZoneOffset
                return adjustedTime;
            }

            return dtUTC;

        }
        /// <summary>
        /// Converts the UTC datetime to the local string.
        /// </summary>
        /// <param name="dtUTC"></param>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <param name="formatString"></param>
        /// <returns></returns>
        public static string UtcToLocalString(DateTime dtUTC, double latitude, double longitude, string formatString)
        {
            if (dtUTC == DateTime.MinValue)
                return string.Empty;

            TimeZoneData data = GetTimeZone(latitude, longitude);
            if (data != null)
            {
                DateTime adjustedTime = dtUTC.AddHours(data.TimeZoneOffset);
                bool bDayLight = false;
                if ((adjustedTime >= data.DayLightSavingStartTime) && (adjustedTime <= data.DayLightSavingEndTime))
                {
                    adjustedTime = adjustedTime.AddHours(1);
                    bDayLight = true;
                }

                string str = adjustedTime.ToString(formatString);
                str = str.Replace("TZ",(bDayLight ? data.DayLightSavingTimeAbbr : data.StandardTimeAbbr));
                return str;
            }
            
            return string.Empty;

        }

        /// <summary>
        /// This is quick and dirty fix until we find a way for the Timezone service to return correct time in the ocean.
        /// </summary>
        /// <param name="dtUTC"></param>
        /// <param name="stormBasin"></param>
        /// <param name="formatString"></param>
        /// <returns></returns>

        public static string UtcToLocalLongString(DateTime dtUTC, string stormBasin, string formatString)
        {
            if (dtUTC == DateTime.MinValue) return string.Empty;
            try
            {
                DateTime dtLocal = dtUTC.ToLocalTime();
                bool bDayLightTime = TimeZone.CurrentTimeZone.IsDaylightSavingTime(dtLocal);
                string sTimeZone = bDayLightTime ? "EDT" : "EST";
                if (stormBasin == "EP")
                {
                    dtLocal = dtLocal.AddHours(-3);
                    sTimeZone = bDayLightTime ? "PDT" : "PST";
                }
                return dtLocal.ToString(formatString).Replace("TZ", sTimeZone);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
            }

            return string.Empty;
        }
    }
}
