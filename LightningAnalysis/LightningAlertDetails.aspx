﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LightningAlertDetails.aspx.cs" Inherits="LightningAnalysis.LightningAlertDetails" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Lightning Alert Details</title>
    <script type="text/javascript">
        function CopyGridView()
        {            
            var div = document.getElementById('tblDiv');
            if(null != div)
            {
                div.contentEditable = 'true';
                var controlRange;
                if (document.body.createControlRange) {
                controlRange = document.body.createControlRange();
                controlRange.addElement(div);
                controlRange.execCommand('Copy');
                }
                div.contentEditable = 'false';
                alert('Successfully copied the data into clipBoard');
             }
        }
    </script>
</head>
<body style="margin: 5px 5px 5px 5px">
    <form id="form1" runat="server">
    <div>
        <table id="Lxtable" style="width: 100%; height: 100%" border="0" cellpadding="1"
            cellspacing="4">
            <tr>
                <td style="height: 500px; width: 35%;font-Size:7.2pt;font-family:Arial" align="left" valign="top" >
                    <div  id = "tblDiv" style="margin:5px;overflow: auto; overflow-x: hidden; height: 500px;width:100%">
                        <table style="width:95%" cellpadding="0" cellspacing="0" >
                            <tr>
                                <td style="white-space:nowrap;padding:5px;width: 100% ;background-color:#800000;border:1px solid white;color:#FFFFFF;font-weight:bold;" align='left'>
                                  &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp; <asp:Label ID="lbcellData" runat="server" Text="" ></asp:Label>  &nbsp;&nbsp;&nbsp;
                                  &nbsp;&nbsp;&nbsp;  <asp:Label ID="lblCellDate" runat="server" Text=""></asp:Label>
                                                            
                                </td>
                            </tr>
                            <tr>
                                <td  style="width: 100%;font-size:7.2pt;font-family:Arial">
                                    <asp:GridView ID="LxGridView" runat="server"  Width="100%" BackColor="White"
                                        BorderColor="Maroon" BorderStyle="Solid" BorderWidth="1px" CellPadding="4" GridLines="Vertical"
                                        AutoGenerateColumns="false" >
                                        <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                                        <RowStyle BackColor="#e8d0d0" ForeColor="#000" BorderColor="Green" BorderStyle="Solid"
                                            BorderWidth="1px" />
                                        <Columns>
                                            <asp:BoundField DataField="Time" HeaderText="Time">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" Wrap="false" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Size" HeaderText="Size" DataFormatString="{0:f4}">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Right" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ICRate" HeaderText="IC Rate" DataFormatString="{0:f1}">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Right" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="CGRate" HeaderText="CG Rate" DataFormatString="{0:f1}">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Right" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="TotalRate" HeaderText="Total Rate" DataFormatString="{0:f1}">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Right" />
                                            </asp:BoundField>
                                        </Columns>
                                        <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                                        <SelectedRowStyle BackColor="#800000" Font-Bold="True" ForeColor="#F7F7F7" />
                                        <HeaderStyle BackColor="#800000" Font-Bold="True" ForeColor="#FFFFFF" />
                                        <AlternatingRowStyle BackColor="#f4e9e9" />
                                    </asp:GridView>                                   
                                </td>
                            </tr>
                        </table>
                    </div>                    
                     <a id = "copy" href="#" onclick="CopyGridView();"  style="color:#800000;padding:4px 4px 4px 4px;font-family:Arial;font-size:8pt;TEXT-DECORATION: underline">Copy Table</a>  
                     
                     <div id ="units" style="padding:3px 3px 3px 3px; font-family:Arial;font-size:6pt" > Note: All LxRates are in Flashes/min and Size in km^2 </div>
                </td>
                <td style="height: 500px; width: 65%; padding-left:10px">
                    <table style="width: 100%; height: 100%">
                        <tr>
                            <td>
                                <asp:Chart ID="LightningAlertChart" runat="server" Width="600px" Height="500px" Palette="Light"
                                    BorderDashStyle="Solid" BackSecondaryColor="White" BackGradientStyle="TopBottom"
                                    BorderWidth="2" BackColor="#e8d0d0" BorderColor="26, 59, 105" >
                                    <Legends>
                                        <asp:Legend Name="LxLegend" BackColor="Transparent" 
                                            Font="Trebuchet MS, 8.25pt, style=Bold" DockedToChartArea="LxChartArea" 
                                            LegendStyle="Row">
                                        </asp:Legend>
                                    </Legends>
                                    <BorderSkin SkinStyle="Emboss"></BorderSkin>
                                    <Titles>
                                        <asp:Title Font="Arial, 12pt, style=Bold" Name="LxTitle" Text="Lightning Cell Data">
                                        </asp:Title>
                                    </Titles>
                                    <Series>
                                        <asp:Series Name="LxTotalRate" BorderWidth="2" Color="Green" 
                                            XValueType="DateTime" MarkerBorderColor="Transparent"
                                            MarkerSize="6" MarkerStyle="Circle" Legend="LxLegend" ShadowOffset="2" 
                                            LegendText="Total Rate" >
                                            <Points>
                                            </Points>
                                        </asp:Series>
                                        <asp:Series Name="LxICRate" Color="Red" BorderWidth="2" XValueType="DateTime" MarkerSize="6"
                                            MarkerStyle="Circle" Legend="LxLegend" ShadowOffset="2" 
                                            LegendText="IC Rate">
                                        </asp:Series>
                                        <asp:Series Name="LxCGRate" Color="Orange" BorderWidth="2" 
                                            XValueType="DateTime" MarkerSize="6"
                                            MarkerStyle="Circle" Legend="LxLegend" ShadowOffset="2" 
                                            LegendText="CG Rate">
                                        </asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="LxChartArea" BorderColor="64, 64, 64, 64" BorderDashStyle="Solid"
                                            BackSecondaryColor="White" BackColor="#f4e9e9" ShadowColor="Transparent" BackGradientStyle="TopBottom">
                                            <Area3DStyle Rotation="10" Perspective="10" Inclination="15" IsRightAngleAxes="False"
                                                WallWidth="0" IsClustered="False"></Area3DStyle>
                                            <AxisY IntervalType="Auto" Title="LxRate" LineColor="64, 64, 64, 64" >
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                                <LabelStyle Font="Arial, 8.25pt" />
                                            </AxisY>
                                            <AxisX IntervalType="Auto" Title=" Cell Time" LineColor="64, 64, 64, 64">
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                                <LabelStyle Font="Arial, 8.25pt" />
                                            </AxisX>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="left">
                                <span style="font-family: Arial; font-size: 9pt;">Chart Type : </span>
                                <asp:DropDownList ID="LxDropDownList" runat="server" OnSelectedIndexChanged="LxDropDownList_selectedindexchanged"
                                    AutoPostBack="true" Font-Names="Arial" Font-Size="9pt" Font-Bold="false">                                   
                                    <asp:ListItem>Line</asp:ListItem>
                                    <asp:ListItem>Column</asp:ListItem>
                                    <asp:ListItem>Area</asp:ListItem>                                   
                                </asp:DropDownList>                                
                               <%-- &nbsp;&nbsp;&nbsp;&nbsp;<a id = "kmlAnim" href = "KmlFiles/LightningAlert.kml" target="_blank" style="color:#800000;padding:4px 4px 4px 4px;font-family:Arial;font-size:8pt;TEXT-DECORATION: underline">Animate On GoogleEarth</a>  --%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
