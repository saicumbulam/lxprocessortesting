﻿using System;
using System.Configuration;
using System.Web;
using System.Web.UI.WebControls;
using LightningAnalysis.Common;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Net.Http;
using Newtonsoft.Json.Linq;

namespace LightningAnalysis
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                #region
                /*if (HttpContext.Current.Request.Params["UserCulture"] != null && HttpContext.Current.Request.Params["UserCulture"] != "")
				{
					this.UserCultureCookie = HttpContext.Current.Request.Params["UserCulture"];
				}
				else if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["DefaultCulture"]))
				{
					this.UserCultureCookie = ConfigurationManager.AppSettings["DefaultCulture"];
				}*/
                #endregion

                #region Start Time Dropdown List
                string StartHour, StartMinute;
                for (int h = 0; h < 24; h++)
                {
                    for (int m = 0; m < 60; m += 15)
                    {
                        if (h < 10)
                        {
                            StartHour = "0" + h.ToString();
                        }
                        else
                        {
                            StartHour = h.ToString();
                        }
                        if (m < 15)
                        {
                            StartMinute = "0" + m.ToString();
                        }
                        else
                        {
                            StartMinute = m.ToString();
                        }

                        StartTime.Items.Add(new ListItem(StartHour + ":" + StartMinute));
                    }
                }
                #endregion

                #region End Time Dropdown List
                string EndHour, EndMinute;
                for (int h = 0; h < 24; h++)
                {
                    for (int m = 0; m < 60; m += 15)
                    {
                        if (h < 10)
                        {
                            EndHour = "0" + h.ToString();
                        }
                        else
                        {
                            EndHour = h.ToString();
                        }
                        if (m < 15)
                        {
                            EndMinute = "0" + m.ToString();
                        }
                        else
                        {
                            EndMinute = m.ToString();
                        }

                        EndTime.Items.Add(new ListItem(EndHour + ":" + EndMinute));
                    }
                }
                #endregion


                try
                {
                    //User LoggedInUser = UserSvc.GetCurrentlyLoggedInUser();
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("Exception occured in IdentitySrv" + ex);
                }

                if (HttpContext.Current.Request.Params["BBOX"] != null)
                {
                    LtgAlertPolygonCheckBox.Checked = false;
                    LtgCellTrackingCheckBox.Checked = false;
                    RadarCheckBox.Checked = false;
                    NWSAlertsCheckBox.Checked = false;
                    StormReportsCheckBox.Checked = false;
                    LtgFlashesCheckBox.Checked = true;
                    PanelChkboxes.Visible = false;
                    lnkGO.Visible = false;
                    lnkDownloadKml.Visible = false; //new
                    lnkExportCSV.Visible = false;//new stdy

                    txtNorth.Visible = true;
                    txtSouth.Visible = true;
                    txtEast.Visible = true;
                    txtWest.Visible = true;

                    lblEast.Visible = true;
                    lblWest.Visible = true;
                    lblNorth.Visible = true;
                    lblSouth.Visible = true;

                    lblEmail.Visible = true;
                    lblOutputFormat.Visible = true;
                    txtEmail.Visible = true;
                    rdbkmz.Visible = true;
                    rdbcsv.Visible = true;
                    btnDownloadButton.Visible = true;
                    cbSendProgressNotifications.Visible = true;

                    //rblOutputFormat.Visible = true;

                    string[] bboxValues = null;
                    string bbox = HttpContext.Current.Request.Params["BBOX"];
                    bboxValues = bbox.Split(",".ToCharArray());

                    string errMsg = string.Empty;
                    double minX = 0;
                    double maxX = 0;
                    double minY = 0;
                    double maxY = 0;

                    if (bboxValues.Length > 0)
                    {
                        //minX + "," + maxY + "," + maxX + "," + minY;
                        if (!double.TryParse(bboxValues[0], out minX))
                            errMsg += " Invalid parameter: minX";
                        if (!double.TryParse(bboxValues[2], out maxX))
                            errMsg += " Invalid parameter: maxX";
                        if (!double.TryParse(bboxValues[3], out minY))
                            errMsg += " Invalid parameter: minY";
                        if (!double.TryParse(bboxValues[1], out maxY))
                            errMsg += " Invalid parameter: maxY";
                    }
                    if (errMsg != string.Empty)
                    {
                        System.Diagnostics.Debug.WriteLine(errMsg);
                    }

                    txtNorth.Text = Math.Round(maxY, 5).ToString();
                    txtSouth.Text = Math.Round(minY, 5).ToString();
                    txtEast.Text = Math.Round(maxX, 5).ToString();
                    txtWest.Text = Math.Round(minX, 5).ToString();
                    //liNoteDownload.InnerText = GlobalResource.GetResourceValue("NoteYoucannotdownloadolderthan12months");
                    //liArchiveNote.InnerText = GlobalResource.GetResourceValue("ArchiveNoteFor365days");
                    // + "<a href=\"mailto: salesops @earthnetworks.com\">salesops@earthnetworks.com </a>";
                    //stdytool
                    pnlLocation.Visible = false;

                }
                else
                {
                    //liNoteDownload.InnerText = GlobalResource.GetResourceValue("NoteYoucannotdownloadolderthan6months");
                    //liArchiveNote.InnerText = GlobalResource.GetResourceValue("ArchiveNote");
                    txtNorth.Visible = false;
                    txtSouth.Visible = false;
                    txtEast.Visible = false;
                    txtWest.Visible = false;


                    //rblOutputFormat.Visible = false;
                    rdbcsv.Visible = false;
                    rdbcsv.Visible = false;
                    btnDownloadButton.Visible = false;

                    lblEast.Visible = false;
                    lblWest.Visible = false;
                    lblNorth.Visible = false;
                    lblSouth.Visible = false;
                    /*DownLoadLnk.Visible = false;
                    lnkDownloadCSV.Visible = false;
                     
                     */


                    txtEmail.Visible = false;
                    lblEmail.Visible = false;
                    lblOutputFormat.Visible = false;
                    rdbkmz.Visible = false;
                    rdbcsv.Visible = false;
                    btnDownloadButton.Visible = false;
                    cbSendProgressNotifications.Visible = false;
                    //added for study tool fnty
                    if (LtgPulsesCheckBox.Checked)
                    {
                        //lnkExportCSV.Style.Add("display", "block");
                        lnkExportCSV.CssClass = "lnkcsvBlock";

                        //txtlatitude.CssClass = "txtBackgroundWhite";
                        //txtlatitude.Enabled = true;

                        //txtlongitude.CssClass = "txtBackgroundWhite";
                        //txtlongitude.Enabled = true;
                        //txtRadius.CssClass = "txtBackgroundWhite";
                        //txtRadius.Enabled = true;

                        bxtxtEast.Enabled = true;
                        bxtxtWest.Enabled = true;
                        bxtxtNorth.Enabled = true;
                        bxtxtSouth.Enabled = true;

                        bxtxtEast.CssClass = "txtBackgroundWhite";
                        bxtxtWest.CssClass = "txtBackgroundWhite";
                        bxtxtNorth.CssClass = "txtBackgroundWhite";
                        bxtxtSouth.CssClass = "txtBackgroundWhite";

                    }
                    else
                    {
                        //lnkExportCSV.Style.Add("display", "none");
                        lnkExportCSV.CssClass = "lnkCSVNone";
                        //txtlatitude.CssClass = "BoundingBoxcss";
                        //txtlatitude.Enabled = false;
                        //txtlongitude.CssClass = "BoundingBoxcss";
                        //txtlongitude.Enabled = false;
                        //txtRadius.CssClass = "BoundingBoxcss";
                        //txtRadius.Enabled = false;

                        bxtxtEast.Enabled = false;
                        bxtxtWest.Enabled = false;
                        bxtxtNorth.Enabled = false;
                        bxtxtSouth.Enabled = false;

                        bxtxtEast.CssClass = "BoundingBoxcss";
                        bxtxtWest.CssClass = "BoundingBoxcss";
                        bxtxtNorth.CssClass = "BoundingBoxcss";
                        bxtxtSouth.CssClass = "BoundingBoxcss";

                    }

                    pnlLocation.Visible = true;
                    /*
                      lblmessage.Visible = false;
                     */

                }
                this.StartDate.Attributes.Add("onblur", "javascript:ValidateDate(this)");
                this.EndDate.Attributes.Add("onblur", "javascript:ValidateDate(this)");
                this.StartTime.Attributes.Add("onblur", "javascript:ValidateTime(this)");
                this.EndTime.Attributes.Add("onblur", "javascript:ValidateTime(this)");

                //ADDED STDY
                //this.txtlatitude.Attributes.Add("onblur", "javascript:validateLat(this)");
                //this.txtlongitude.Attributes.Add("onblur", "javascript:validateLng(this)");
                //this.txtRadius.Attributes.Add("onblur", "javascript:validateRadius(this)");

                this.bxtxtNorth.Attributes.Add("onblur", "javascript:validateLat(this)");
                this.bxtxtSouth.Attributes.Add("onblur", "javascript:validateLat(this)");
                this.bxtxtEast.Attributes.Add("onblur", "javascript:validateLat(this)");
                this.bxtxtWest.Attributes.Add("onblur", "javascript:validateLat(this)");

            }
            catch (Exception ex)
            {
                Aws.Core.Utilities.EventManager.LogError("Exception in Pageload" + ex);
                System.Diagnostics.Debug.WriteLine("Exception in Pageload" + ex);
            }
        }

        private bool VerifyInput()
        {
            string userDateFormat = GlobalResource.UserDateFormat;

            IFormatProvider formatprovider = null;

            if (string.IsNullOrEmpty(userDateFormat))
            {
                userDateFormat = "MM/dd/yyyy";
            }

            if (userDateFormat == "dd/MM/yyyy")
            {
                formatprovider = new CultureInfo("es-ES");
            }
            else
            {
                formatprovider = new CultureInfo("en-US");
            }

            System.Diagnostics.Debug.WriteLine("Dateformat: " + userDateFormat);

            string errMsg = string.Empty;

            DateTime startTime = new DateTime();
            DateTime endTime = new DateTime();
            DateTime StartDateTxt = Convert.ToDateTime(startDateTimeTxtBox.Text);
            DateTime EndDateTxt = Convert.ToDateTime(endDateTimeTxtBox.Text);

            DateTime currentTime = DateTime.Now.ToUniversalTime();


            if (!DateTime.TryParse(StartDate.Text + " " + StartTime.Text, formatprovider, DateTimeStyles.None, out startTime))
            {
                errMsg += GlobalResource.GetResourceValue("StartDateTime");

            }
            if (!DateTime.TryParse(EndDate.Text + " " + EndTime.Text, formatprovider, DateTimeStyles.None, out endTime))
            {
                if (!string.IsNullOrEmpty(errMsg))
                {
                    errMsg += ",";
                }
                errMsg += GlobalResource.GetResourceValue("EndDateTime");
            }

            if (!string.IsNullOrEmpty(errMsg))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "LayerScript", "<script language='javascript'>alert('" + GlobalResource.GetResourceValue("Pleaseentercorrect") + " " + errMsg + " " + GlobalResource.GetResourceValue("format") + "');</Script>");
                return false;
            }

            if (startTime > endTime)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "LayerScript", "<script language='javascript'>alert('" + GlobalResource.GetResourceValue("PleaseenterStarttimeshouldbelessthanEndTime") + "');</Script>");
                return false;
            }

            if ((EndDateTxt - StartDateTxt).TotalHours > 6)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "LayerScript", "<script language='javascript'>alert('" + GlobalResource.GetResourceValue("PleaseenterEndDateTimeshouldbelessthan6hoursfromStartDate") + "');</Script>");
                return false;
            }

            int downloadMonths = 0;
            if (!int.TryParse(ConfigurationManager.AppSettings["DownloadMonths"], out downloadMonths))
            {
                System.Diagnostics.Debug.WriteLine("Can not find the DownloadMonths config key");
                Aws.Core.Utilities.EventManager.LogError("Check for DownloadMonths key in config file");
                ClientScript.RegisterStartupScript(this.GetType(), "LayerScript", "<script language='javascript'>alert('" + GlobalResource.GetResourceValue("WeCannotdownloadthedata") + "')</Script>");
                return false;
            }

            int downloadDays = 0;
            if (!int.TryParse(ConfigurationManager.AppSettings["downloadDays"], out downloadDays))
            {
                System.Diagnostics.Debug.WriteLine("Can not find the downloadDays config key");
                Aws.Core.Utilities.EventManager.LogError("Check for downloadDays key in config file");
                ClientScript.RegisterStartupScript(this.GetType(), "LayerScript", "<script language='javascript'>alert('" + GlobalResource.GetResourceValue("WeCannotdownloadthedata") + "')</Script>");
                return false;
            }

            DateTime dtpastime = currentTime.AddDays(downloadDays);

            string strCurrDate = dtpastime.ToString("MMddyyyy");
            string strstartDate = startTime.ToString("MMddyyyy");

            if (dtpastime > startTime)
            //if (int.Parse(strCurrDate) > int.Parse(strstartDate))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "LayerScript", "<script language='javascript'>alert('" + GlobalResource.GetResourceValue("Youcannotdownloadolderthan12monthsofdata") + "')</Script>");
                return false;
            }

            return true;
        }

        protected void lnkGO_Click(object sender, EventArgs e)
        {

            try
            {
                if (!string.IsNullOrEmpty(StartDate.Text) && !string.IsNullOrEmpty(StartTime.Text) && !string.IsNullOrEmpty(EndDate.Text) && !string.IsNullOrEmpty(EndTime.Text))
                {

                    string strWebUrl = ConfigurationManager.AppSettings["WebUrl"];

                    if (string.IsNullOrEmpty(strWebUrl))
                        return;

                    if (!VerifyInput())
                        return;

                    string querystring = "startdate=" + StartDate.Text + "&amp;starttime=" + StartTime.Text + "&amp;enddate=" + EndDate.Text + "&amp;endtime=" + EndTime.Text;

                    string strkml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + "<kml xmlns=\"http://www.opengis.net/kml/2.2\" xmlns:gx=\"http://www.google.com/kml/ext/2.2\" xmlns:kml=\"http://www.opengis.net/kml/2.2\" xmlns:atom=\"http://www.w3.org/2005/Atom\">" +
                                    "<Folder>" +
                                    "<name>Earth Networks Total Lightning</name>    <visibility>0</visibility>    <open>0</open>";
                    //Add Cell Track
                    if (LtgCellTrackingCheckBox.Checked)
                    {
                        strkml += "<NetworkLink>" +
                                  "<name>Ltg Cell Tracking</name>" +
                                  "<Link>" +
                                  "<href>" + strWebUrl + "/LightningHandler.ashx?" + querystring + "&amp;type=CellTrack&amp;</href>" +
                                  "<refreshMode>onInterval</refreshMode>" +
                                  "<refreshInterval>18000</refreshInterval>" +
                                  "<viewRefreshMode>onRequest</viewRefreshMode>" +
                                  "</Link></NetworkLink>";
                    }

                    if (LtgAlertPolygonCheckBox.Checked)
                    {
                        //Add Polygon
                        strkml += "<NetworkLink>" +
                                  "<name>Ltg Alert Polygon</name>" +
                                  "<Link>" +
                                  "<href>" + strWebUrl + "/LightningHandler.ashx?" + querystring + "&amp;type=Polygon&amp;</href>" +
                                  "<refreshMode>onInterval</refreshMode>" +
                                  "<refreshInterval>18000</refreshInterval>" +
                                  "<viewRefreshMode>onRequest</viewRefreshMode>" +
                                  "</Link></NetworkLink>";
                    }

                    if (NWSAlertsCheckBox.Checked)
                    {
                        strkml += "<NetworkLink>" +
                                  "<name>NWS Alerts</name>" +
                                  "<Link>" +
                                  "<href>" + strWebUrl + "/LightningHandler.ashx?" + querystring + "&amp;type=NWSAlerts&amp;</href>" +
                                  "<refreshMode>onInterval</refreshMode>" +
                                  "<refreshInterval>18000</refreshInterval>" +
                                  "<viewRefreshMode>onRequest</viewRefreshMode>" +
                                  "</Link></NetworkLink>";
                    }

                    if (RadarCheckBox.Checked)
                    {
                        strkml += "<NetworkLink>" +
                                  "<name>Radar Mosaic</name>" +
                                  "<Link>" +
                                  "<href>" + strWebUrl + "/LightningHandler.ashx?" + querystring + "&amp;type=Radar&amp;</href>" +
                                  "<refreshMode>onInterval</refreshMode>" +
                                  "<refreshInterval>18000</refreshInterval>" +
                                  "<viewRefreshMode>onRequest</viewRefreshMode>" +
                                  "</Link></NetworkLink>";
                    }

                    if (StormReportsCheckBox.Checked)
                    {
                        strkml += "<NetworkLink>" +
                                  "<name>Local Storm Reports</name>" +
                                  "<Link>" +
                                  "<href>" + strWebUrl + "/LightningHandler.ashx?" + querystring + "&amp;type=StormReports&amp;</href>" +
                                  "<refreshMode>onInterval</refreshMode>" +
                                  "<refreshInterval>18000</refreshInterval>" +
                                  "<viewRefreshMode>onRequest</viewRefreshMode>" +
                                  "</Link></NetworkLink>";
                    }
                    if (LtgFlashesCheckBox.Checked)
                    {
                        string bbox = Request.Params["BBOX"];
                        string mapcenter = Request.Params["MapCenter"];
                        if (!string.IsNullOrEmpty(bbox))
                        {
                            querystring += "&amp;BBOX=" + bbox;
                        }

                        strkml += "<NetworkLink>" +
                                  "<name>Lightning Flashes</name>" +
                                  "<Link>" +
                                  "<href>" + strWebUrl + "/LightningHandler.ashx?" + querystring + "&amp;type=LtgFlash&amp;</href>" +
                                  "<refreshMode>onInterval</refreshMode>" +
                                  "<refreshInterval>18000</refreshInterval>" +
                                  "<viewRefreshMode>onRequest</viewRefreshMode>" +
                                  "</Link></NetworkLink>";
                        if (!string.IsNullOrEmpty(mapcenter))
                        {
                            string[] mapcenterVals = mapcenter.Split(",".ToCharArray());
                            if (mapcenterVals.Length >= 1)
                            {
                                double minX = 0;
                                double maxX = 0;
                                double minY = 0;
                                double maxY = 0;

                                string bboxx = HttpContext.Current.Request.Params["BBOX"];
                                string errMsg = string.Empty;
                                if (!string.IsNullOrEmpty(bboxx))
                                {
                                    string[] bboxValues = bbox.Split(",".ToCharArray());
                                    if (bboxValues.Length > 0)
                                    {
                                        //minX + "," + maxY + "," + maxX + "," + minY;
                                        if (!double.TryParse(bboxValues[0], out minX))
                                            errMsg += " Invalid parameter: minX";
                                        if (!double.TryParse(bboxValues[2], out maxX))
                                            errMsg += " Invalid parameter: maxX";
                                        if (!double.TryParse(bboxValues[3], out minY))
                                            errMsg += " Invalid parameter: minY";
                                        if (!double.TryParse(bboxValues[1], out maxY))
                                            errMsg += " Invalid parameter: maxY";
                                    }
                                }

                                if (errMsg != string.Empty)
                                {
                                    System.Diagnostics.Debug.WriteLine("View goolgle link : " + errMsg);
                                }

                                strkml += "<LookAt>" +
                                            "<longitude>" + double.Parse(mapcenterVals[0]) + "</longitude>" +
                                            "<latitude>" + double.Parse(mapcenterVals[1]) + "</latitude>" +
                                            "<altitude>0</altitude>" +
                                            "<range>" + GetDistance(maxY, minX, maxY, maxX) + "</range>" +
                                            "<tilt>0</tilt>" +
                                            "<heading>0</heading>" +
                                            "<altitudeMode>relativeToGround</altitudeMode>" +
                                            "</LookAt>";

                            }
                        }
                    }
                    if (LtgPulsesCheckBox.Checked)
                    {

                        //if (!string.IsNullOrEmpty(txtlatitude.Text) && !string.IsNullOrEmpty(txtlongitude.Text) && !string.IsNullOrEmpty(txtRadius.Text))
                        if (!string.IsNullOrEmpty(bxtxtNorth.Text) && !string.IsNullOrEmpty(bxtxtSouth.Text) && !string.IsNullOrEmpty(bxtxtWest.Text) && !string.IsNullOrEmpty(bxlblEast.Text))
                        {
                            //querystring += "&amp;lat=" + txtlatitude.Text + "&amp;lng=" + txtlongitude.Text + "&amp;radius=" + txtRadius.Text;
                            querystring += "&amp;north=" + bxtxtNorth.Text + "&amp;south=" + bxtxtSouth.Text + "&amp;east=" + bxtxtEast.Text + "&amp;west=" + bxtxtWest.Text;
                        }
                        else
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "LayerScript", "<script language='javascript'>alert('Please enter all fields in location details ');</Script>");
                            return;
                        }
                        strkml += "<NetworkLink>" +
                                  "<name>Lightning Pulses</name>" +
                                  "<Link>" +
                                  "<href>" + strWebUrl + "/LightningHandler.ashx?" + querystring + "&amp;type=LtgPulses&amp;</href>" +
                                  "<refreshMode>onInterval</refreshMode>" +
                                  "<refreshInterval>18000</refreshInterval>" +
                                  "<viewRefreshMode>onRequest</viewRefreshMode>" +
                                  "</Link></NetworkLink>";
                    }
                    strkml += "</Folder>" +
                                "</kml>";

                    Response.Clear();

                    Response.AppendHeader("Content-Disposition", "filename=\"WeatherBugTotalLighning.kml\"");
                    Response.ContentType = "application/vnd.google-earth.kml+xml";
                    Response.Write(strkml);
                    Response.End();

                }


                else
                {
                    //ClientScript.RegisterStartupScript(this.GetType(), "LayerScript", "<script language='javascript'>alert('Please enter data in all fields');</Script>");
                    ClientScript.RegisterStartupScript(this.GetType(), "LayerScript", "<script language='javascript'>alert('" + GlobalResource.GetResourceValue("Pleaseenterdatainallfields") + "');</Script>");
                }
            }


            catch (System.Threading.ThreadAbortException ex)
            {
                //System.Diagnostics.Debug.WriteLine("Exception in Redirecting" + ex);				

            }
            catch (Exception ex)
            {
                Aws.Core.Utilities.EventManager.LogError("Exception in Redirecting" + ex);
                System.Diagnostics.Debug.WriteLine("Exception in Redirecting" + ex);
            }
        }

        public static double Radians(double x)
        {
            const double PIx = 3.141592653589793;
            return x * PIx / 180;
        }

        public static double GetDistance(double lat1, double lon1, double lat2, double lon2)
        {
            float EARTH_RADIUS = 6378000.0F;
            double fLat1 = lat1 * Math.PI / 180.0F;
            double fLong1 = lon1 * Math.PI / 180.0F;
            double fLat2 = lat2 * Math.PI / 180.0F;
            double fLong2 = lon2 * Math.PI / 180.0F;

            double dist = EARTH_RADIUS * Math.Acos(Math.Cos(fLong1 - fLong2) * Math.Cos(fLat1) * Math.Cos(fLat2) + Math.Sin(fLat1) * Math.Sin(fLat2));
            return (float)dist;
        }

        protected void DownLoadLnk_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(StartDate.Text) && !string.IsNullOrEmpty(StartTime.Text) && !string.IsNullOrEmpty(EndDate.Text) && !string.IsNullOrEmpty(EndTime.Text))
                {

                    string strWebUrl = ConfigurationManager.AppSettings["WebUrl"];

                    if (string.IsNullOrEmpty(strWebUrl))
                        return;

                    if (!VerifyInput())
                        return;

                    string querystring = "startdate=" + StartDate.Text + "&starttime=" + StartTime.Text + "&enddate=" + EndDate.Text + "&endtime=" + EndTime.Text;
                    string bbox = Request.Params["BBOX"];
                    if (!string.IsNullOrEmpty(bbox))
                    {
                        querystring += "&BBOX=" + bbox;
                    }


                    string url = strWebUrl + "/LightningHandler.ashx?" + querystring + "&type=LtgFlash";
                    System.Diagnostics.Debug.WriteLine("Redirecting Url " + url);
                    Response.Redirect(url, false);
                    Response.End();

                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "LayerScript", "<script language='javascript'>alert('" + GlobalResource.GetResourceValue("Pleaseenterdatainallfields") + "');</Script>");
                }
            }
            catch (System.Threading.ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                Aws.Core.Utilities.EventManager.LogError("Exception in Redirecting" + ex);
                System.Diagnostics.Debug.WriteLine("Exception in Redirecting" + ex);
                ClientScript.RegisterStartupScript(this.GetType(), "LayerScript", "<script language='javascript'>alert('" + GlobalResource.GetResourceValue("WeCannotdownloadthedata") + "')</Script>");
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            lblStartDate.Text = GlobalResource.GetResourceValue("StartDate");
            lblStartTime.Text = GlobalResource.GetResourceValue("StartTime");
            lblEndDate.Text = GlobalResource.GetResourceValue("EndDate");
            lblEndTime.Text = GlobalResource.GetResourceValue("EndTime");
            /*DownLoadLnk.ToolTip = GlobalResource.GetResourceValue("DownLoaddataasKmz");
             */

            lnkGO.ToolTip = GlobalResource.GetResourceValue("ViewdatainGoogleEarth");
            StartDate.ToolTip = GlobalResource.GetResourceValue("EnterorPicktheDatefromCalendar");
            EndDate.ToolTip = GlobalResource.GetResourceValue("EnterorPicktheDatefromCalendar");
            LtgCellTrackingCheckBox.Text = GlobalResource.GetResourceValue("LtgCellTracking");
            LtgAlertPolygonCheckBox.Text = GlobalResource.GetResourceValue("LtgAlertPolygon");
            NWSAlertsCheckBox.Text = GlobalResource.GetResourceValue("NWSAlertsCheckBox");
            StormReportsCheckBox.Text = GlobalResource.GetResourceValue("StormReports");
            RadarCheckBox.Text = GlobalResource.GetResourceValue("Radar");
            LtgFlashesCheckBox.Text = GlobalResource.GetResourceValue("LtgFlashes");

            string userDateFormat = Common.GlobalResource.UserDateFormat;

            if (string.IsNullOrEmpty(userDateFormat))
            {
                userDateFormat = "MM/dd/yyyy";
            }

            if (userDateFormat == "dd/MM/yyyy")
            {
                lbldateformat.Text = GlobalResource.GetResourceValue("ddmmyyyy");
                endlblDateformat.Text = GlobalResource.GetResourceValue("ddmmyyyy");
            }
            else
            {
                lbldateformat.Text = GlobalResource.GetResourceValue("mmddyyyy");
                endlblDateformat.Text = GlobalResource.GetResourceValue("mmddyyyy");
            }

            lblNorth.Text = GlobalResource.GetResourceValue("North");
            lblSouth.Text = GlobalResource.GetResourceValue("South");
            lblEast.Text = GlobalResource.GetResourceValue("East");
            lblWest.Text = GlobalResource.GetResourceValue("West");

            lblEmail.Text = GlobalResource.GetResourceValue("Email");
            lblOutputFormat.Text = GlobalResource.GetResourceValue("OutputFormat");
            /*
            lblmessage.Text = GlobalResource.GetResourceValue("ClicktoexportkmzfileforGoogleEarth");
            lnkDownloadCSV.ToolTip = GlobalResource.GetResourceValue("DownLoaddataascsv");
             */

        }

        public string UserCultureCookie
        {
            get
            {
                string sCultureCookie = string.Empty;
                if (HttpContext.Current.Request.Cookies["CultureCookie"] != null)
                {
                    sCultureCookie = HttpContext.Current.Request.Cookies["CultureCookie"].Value;
                }
                if (string.IsNullOrEmpty(sCultureCookie) && (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["DefaultCulture"])))
                {
                    sCultureCookie = ConfigurationManager.AppSettings["DefaultCulture"];
                }
                return sCultureCookie;
            }
        }

        protected void lnkDownloadCSV_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(StartDate.Text) && !string.IsNullOrEmpty(StartTime.Text) && !string.IsNullOrEmpty(EndDate.Text) && !string.IsNullOrEmpty(EndTime.Text))
                {

                    string strWebUrl = ConfigurationManager.AppSettings["WebUrl"];

                    if (string.IsNullOrEmpty(strWebUrl))
                        return;

                    if (!VerifyInput())
                        return;

                    string querystring = "startdate=" + StartDate.Text + "&starttime=" + StartTime.Text + "&enddate=" + EndDate.Text + "&endtime=" + EndTime.Text;
                    string bbox = Request.Params["BBOX"];
                    if (!string.IsNullOrEmpty(bbox))
                    {
                        querystring += "&BBOX=" + bbox;
                    }

                    string url = strWebUrl + "/LightningHandler.ashx?" + querystring + "&type=LtgFlash&downloadtype=csv";
                    System.Diagnostics.Debug.WriteLine("Redirecting Url " + url);
                    Aws.Core.Utilities.EventManager.LogError("Redirecting Url " + url);
                    Response.Redirect(url, false);
                    Response.End();

                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "LayerScript", "<script language='javascript'>alert('" + GlobalResource.GetResourceValue("Pleaseenterdatainallfields") + "');</Script>");
                }
            }
            catch (System.Threading.ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                Aws.Core.Utilities.EventManager.LogError("Exception in Redirecting" + ex);
                System.Diagnostics.Debug.WriteLine("Exception in Redirecting" + ex);
                ClientScript.RegisterStartupScript(this.GetType(), "LayerScript", "<script language='javascript'>alert('" + GlobalResource.GetResourceValue("WeCannotdownloadthedata") + "')</Script>");
            }
        }

        //view in google
        protected void lnkDownloadKml_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(StartDate.Text) && !string.IsNullOrEmpty(StartTime.Text) && !string.IsNullOrEmpty(EndDate.Text) && !string.IsNullOrEmpty(EndTime.Text))
                {

                    string strWebUrl = ConfigurationManager.AppSettings["WebUrl"];

                    if (string.IsNullOrEmpty(strWebUrl))
                        return;

                    if (!VerifyInput())
                        return;

                    string querystring = "startdate=" + StartDate.Text + "&starttime=" + StartTime.Text + "&enddate=" + EndDate.Text + "&endtime=" + EndTime.Text;
                    string bbox = Request.Params["BBOX"];
                    if (!string.IsNullOrEmpty(bbox))
                    {
                        querystring += "&BBOX=" + bbox;
                    }

                    int iCheckedCount = 0;
                    if (LtgCellTrackingCheckBox.Checked)
                    {
                        iCheckedCount++;
                    }
                    if (LtgAlertPolygonCheckBox.Checked)
                    {
                        iCheckedCount++;
                    }

                    if (NWSAlertsCheckBox.Checked)
                    {
                        iCheckedCount++;
                    }
                    if (RadarCheckBox.Checked)
                    {
                        iCheckedCount++;
                    }
                    if (StormReportsCheckBox.Checked)
                    {
                        iCheckedCount++;
                    }
                    if (LtgFlashesCheckBox.Checked)
                    {
                        iCheckedCount++;
                    }
                    if (LtgPulsesCheckBox.Checked)
                    {
                        iCheckedCount++;
                    }

                    if (iCheckedCount != 1)
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "LayerScript", "<script language='javascript'>alert('" + GlobalResource.GetResourceValue("You can download only one kmz file at a time") + "');</Script>");
                        return;
                    }


                    //string url = strWebUrl + "/LightningHandler.ashx?" + querystring + "&type=LtgFlash";
                    string url = strWebUrl + "/LightningHandler.ashx?" + querystring;

                    if (LtgCellTrackingCheckBox.Checked)
                    {
                        url += "&type=CellTrack";
                    }
                    else if (LtgAlertPolygonCheckBox.Checked)
                    {
                        url += "&type=Polygon";
                    }
                    else if (NWSAlertsCheckBox.Checked)
                    {
                        url += "&type=NWSAlerts";
                    }
                    else if (RadarCheckBox.Checked)
                    {
                        url += "&type=Radar";
                    }
                    else if (StormReportsCheckBox.Checked)
                    {
                        url += "&type=StormReports";
                    }
                    else if (LtgFlashesCheckBox.Checked)
                    {
                        url += "&type=LtgFlash";
                    }
                    else if (LtgPulsesCheckBox.Checked)
                    {
                        url += "&type=LtgPulses";

                        // if (!string.IsNullOrEmpty(txtlatitude.Text) && !string.IsNullOrEmpty(txtlongitude.Text) && !string.IsNullOrEmpty(txtRadius.Text))
                        if (!string.IsNullOrEmpty(bxtxtNorth.Text) && !string.IsNullOrEmpty(bxtxtSouth.Text) && !string.IsNullOrEmpty(bxtxtWest.Text) && !string.IsNullOrEmpty(bxlblEast.Text))
                        {
                            // url += "&lat=" + txtlatitude.Text + "&lng=" + txtlongitude.Text + "&radius=" + txtRadius.Text;
                            url += "&north=" + bxtxtNorth.Text + "&south=" + bxtxtSouth.Text + "&east=" + bxtxtEast.Text + "&west=" + bxtxtWest.Text;
                        }
                        else
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "LayerScript", "<script language='javascript'>alert('Please enter all fields in location details ');</Script>");
                            return;
                        }
                    }

                    System.Diagnostics.Debug.WriteLine("Redirecting Url " + url);
                    Aws.Core.Utilities.EventManager.LogError("Redirecting Url " + url);
                    Response.Redirect(url, false);
                    Response.End();

                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "LayerScript", "<script language='javascript'>alert('" + GlobalResource.GetResourceValue("Pleaseenterdatainallfields") + "');</Script>");
                }
            }
            catch (System.Threading.ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                Aws.Core.Utilities.EventManager.LogError("Exception in Redirecting" + ex);
                System.Diagnostics.Debug.WriteLine("Exception in Redirecting" + ex);
                ClientScript.RegisterStartupScript(this.GetType(), "LayerScript", "<script language='javascript'>alert('" + GlobalResource.GetResourceValue("WeCannotdownloadthedata") + "')</Script>");
            }
        }

        //new stdytool
        protected void lnkExportCSV_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(StartDate.Text) && !string.IsNullOrEmpty(StartTime.Text) && !string.IsNullOrEmpty(EndDate.Text) && !string.IsNullOrEmpty(EndTime.Text))
                {

                    string strWebUrl = ConfigurationManager.AppSettings["WebUrl"];

                    if (string.IsNullOrEmpty(strWebUrl))
                        return;

                    if (!VerifyInput())
                        return;

                    string querystring = "&startdate=" + StartDate.Text + "&starttime=" + StartTime.Text + "&enddate=" + EndDate.Text + "&endtime=" + EndTime.Text;
                    string bbox = Request.Params["BBOX"];
                    if (!string.IsNullOrEmpty(bbox))
                    {
                        querystring += "&BBOX=" + bbox;
                    }



                    string url = strWebUrl + "/LightningHandler.ashx?downloadtype=csv";

                    url += querystring;

                    if (LtgPulsesCheckBox.Checked)
                    {
                        url += "&type=LtgPulses";
                        // if (!string.IsNullOrEmpty(txtlatitude.Text) && !string.IsNullOrEmpty(txtlongitude.Text) && !string.IsNullOrEmpty(txtRadius.Text))
                        if (!string.IsNullOrEmpty(bxtxtNorth.Text) && !string.IsNullOrEmpty(bxtxtSouth.Text) && !string.IsNullOrEmpty(bxtxtWest.Text) && !string.IsNullOrEmpty(bxlblEast.Text))
                        {
                            //url += "&lat=" + txtlatitude.Text + "&lng=" + txtlongitude.Text + "&radius=" + txtRadius.Text;

                            url += "&north=" + bxtxtNorth.Text + "&south=" + bxtxtSouth.Text + "&east=" + bxtxtEast.Text + "&west=" + bxtxtWest.Text;
                        }
                        else
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "LayerScript", "<script language='javascript'>alert('Please enter all fields in location details ');</Script>");
                            return;
                        }
                    }
                    else
                    {
                        url += "&type=LtgFlash";
                    }


                    //  string url = strWebUrl + "/LightningHandler.ashx?" + querystring + "&type=LtgFlash&downloadtype=csv";
                    System.Diagnostics.Debug.WriteLine("Redirecting Url " + url);
                    Aws.Core.Utilities.EventManager.LogError("Redirecting Url " + url);
                    Response.Redirect(url, false);
                    Response.End();

                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "LayerScript", "<script language='javascript'>alert('" + GlobalResource.GetResourceValue("Pleaseenterdatainallfields") + "');</Script>");
                }
            }
            catch (System.Threading.ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                Aws.Core.Utilities.EventManager.LogError("Exception in Redirecting" + ex);
                System.Diagnostics.Debug.WriteLine("Exception in Redirecting" + ex);
                ClientScript.RegisterStartupScript(this.GetType(), "LayerScript", "<script language='javascript'>alert('" + GlobalResource.GetResourceValue("WeCannotdownloadthedata") + "')</Script>");
            }
        }

        protected void btnDownloadButton_Click(object sender, EventArgs e)
        {

            try
            {
                if (!string.IsNullOrEmpty(StartDate.Text) && !string.IsNullOrEmpty(StartTime.Text) && !string.IsNullOrEmpty(EndDate.Text) && !string.IsNullOrEmpty(EndTime.Text))
                {
                    string pattern = null;
                    pattern = "^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$";
                    var start = startDateTimeTxtBox.Text;
                    var end = endDateTimeTxtBox.Text;
                    string startdate = string.Empty;
                    string starttime = string.Empty;
                    string enddate = string.Empty;
                    string endtime = string.Empty;
                    IFormatProvider formatprovider = null;
                    string userDateFormat = string.Empty;
                    var lightningBaseURL = ConfigurationManager.AppSettings["EarthNetworks.Lightning.Api.BaseAddress"];

                    if (string.IsNullOrWhiteSpace(lightningBaseURL))
                        return;

                    if (!Regex.IsMatch(txtEmail.Text, pattern))
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "LayerScript", "<script language='javascript'>alert('" + GlobalResource.GetResourceValue("PleaseenterValidEmailAddress") + "');</Script>");
                        return;
                    }

                    if (!VerifyInput())
                        return;

                    userDateFormat = Common.GlobalResource.UserDateFormat;
                    formatprovider = userDateFormat == "dd/MM/yyyy" ? new CultureInfo("es-ES") : new CultureInfo("en-US");

                    startdate = StartDate.Text; starttime = StartTime.Text; enddate = EndDate.Text; endtime = EndTime.Text;

                    if (string.IsNullOrWhiteSpace(startdate) && string.IsNullOrWhiteSpace(starttime) && string.IsNullOrWhiteSpace(enddate) && string.IsNullOrWhiteSpace(endtime))
                    {
                        Aws.Core.Utilities.EventManager.LogError("date time params are null or empty ");
                        return;
                    }

                    var sdate = DateTime.Parse(startdate + " " + starttime, formatprovider).ToString("s");
                    var edate = DateTime.Parse(enddate + " " + endtime, formatprovider).ToString("s");
                    
                    var strokeTypeVal = 2;
                    var lxTypeVal = "flash";
                    var lxSelectedType = 0;
                    var lxSelectedVersion = 0;
                    var responseEmailAddress = txtEmail.Text;
                    var outputFormat = rdbkmz.Checked ? 2 : 1;      // 2 - kmz , 1 - csv , 0- json formt
                    var sendProgressEmail = cbSendProgressNotifications.Checked ? true : false;

                    var ullat = Convert.ToDouble(txtNorth.Text);  // lat max
                    var ullon = Convert.ToDouble(txtWest.Text);   // lon min
                    var lrlat = Convert.ToDouble(txtSouth.Text);  //lat min
                    var lrlon = Convert.ToDouble(txtEast.Text);   //lon max                   

                    HttpClient client = new HttpClient();
                    client.BaseAddress = new Uri(lightningBaseURL);

                    var path = $"/api/v3/{lxTypeVal}?start={sdate}&end={edate}&type={strokeTypeVal}&ullat={ullat}&ullon={ullon}&lrlat={lrlat}&lrlon={lrlon}&ResponseEmailAddress={responseEmailAddress}&lxType={lxSelectedType}&outputType={outputFormat}&lxVersion={lxSelectedVersion}&sendNotifications=true&sendProgressNotifications={sendProgressEmail}";

                    HttpResponseMessage response = client.GetAsync(path).ConfigureAwait(false).GetAwaiter().GetResult();

                    if (response.IsSuccessStatusCode)
                    {
                        var apiResponse = response.Content.ReadAsStringAsync().ConfigureAwait(false).GetAwaiter().GetResult();//.Result;
                        JObject responseJObject = JObject.Parse(apiResponse);
                        if (responseJObject["Result"].Type == JTokenType.Null )
                        {
                            
                            ClientScript.RegisterStartupScript(this.GetType(), "LayerScript", "<script language='javascript'>alert('" + GlobalResource.GetResourceValue("Pleaseenterdatainallfields") + "');</Script>");
                        }
                        else
                        {
                            var jobId = JObject.Parse((string)responseJObject["Result"])["JobId"];
                            if (jobId.Type == JTokenType.Null)
                            {
                                ClientScript.RegisterStartupScript(this.GetType(), "LayerScript", "<script language='javascript'>alert('" + GlobalResource.GetResourceValue("Pleaseenterdatainallfields") + "');</Script>");
                            }
                            else
                            {
                                ClientScript.RegisterStartupScript(this.GetType(), "LayerScript", "<script language='javascript'>alert('Job successfully submitted. Your Job ID : '+'" + jobId + "');</Script>");
                            }
                        }
                    }
                    else
                    {
                        var apiResponse = response.Content.ReadAsStringAsync().ConfigureAwait(false).GetAwaiter().GetResult();
                        JObject responseJObject = JObject.Parse(apiResponse);
                        var errorMessage = responseJObject["ErrorMessage"];
                        ClientScript.RegisterStartupScript(this.GetType(), "LayerScript", "<script language='javascript'>alert('" + errorMessage + "');</Script>");
                        return;
                    }
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "LayerScript", "<script language='javascript'>alert('" + GlobalResource.GetResourceValue("Pleaseenterdatainallfields") + "');</Script>");
                }
            }
            catch (System.Threading.ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                Aws.Core.Utilities.EventManager.LogError("Exception in Submiting Job" + ex);
                System.Diagnostics.Debug.WriteLine("Exception in Submiting Job" + ex);
                ClientScript.RegisterStartupScript(this.GetType(), "LayerScript", "<script language='javascript'>alert('Something wrong. Please try again.');</script>");
            }
        }
    }
}
