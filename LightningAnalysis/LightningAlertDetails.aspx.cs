﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Web.UI.DataVisualization.Charting;
using Aws.Core.Utilities;
using System.Drawing;
//using Aws.MesoStreamer.Common;
using Aws.Core.TimeZoneServiceRef;
using System.IO;
using AwsGeometries;


namespace LightningAnalysis
{
	public partial class LightningAlertDetails : System.Web.UI.Page
	{
		
		
		protected void Page_Load(object sender, EventArgs e)
		{
			try
			{
				if(!IsPostBack)
				{					
				    List<Dictionary<string, string>> lstLxData = GetGeometryList(Request.QueryString["key"], Request.QueryString["cellid"]);
					DrawChart(lstLxData);
					
				}
			}
			catch(Exception ex)
			{
				System.Diagnostics.Debug.WriteLine("Failed to execute the GetGeometryList. " + ex);
				Aws.Core.Utilities.EventManager.LogError("Failed to execute the GetGeometryList: " + ex);
			}			

		}

        private List<Dictionary<string, string>> GetGeometryList(string cacheKey, string cellID)
        {
			List<IGeometry> features = LightningArchivedManager.GetArchiveGeometryList(cacheKey, "CellTrack");

            var cellFeatures = from p in features								
							   where p.MetaData["CellID"] == cellID && p.MetaData["FeatureType"] == "CellTrack" 
                               select  p.MetaData;

            return cellFeatures.ToList();
        }

        public void DrawChart(List<Dictionary<string, string>> lstLxData)
		{
			try
			{
				System.Diagnostics.Debug.WriteLine("Rendering Chart");
				#region Build Datatable
				DataTable dtable = new DataTable();
				dtable.Columns.Add("Time", typeof(System.String));
				dtable.Columns.Add("Size", typeof(System.Single));
				dtable.Columns.Add("ICRate", typeof(System.Single));
				dtable.Columns.Add("CGRate", typeof(System.Single));
				dtable.Columns.Add("TotalRate", typeof(System.Single));
				#endregion

				#region Select CharType
				DropDownList LxDropDownList = (DropDownList)this.FindControl("LxDropDownList");

				LightningAlertChart.Series["LxTotalRate"].ChartType = (SeriesChartType)Enum.Parse(typeof(SeriesChartType), LxDropDownList.SelectedValue); ;
				LightningAlertChart.Series["LxICRate"].ChartType = (SeriesChartType)Enum.Parse(typeof(SeriesChartType), LxDropDownList.SelectedValue);
				LightningAlertChart.Series["LxCGRate"].ChartType = (SeriesChartType)Enum.Parse(typeof(SeriesChartType), LxDropDownList.SelectedValue);
				#endregion

				#region Get StartTime

				int icnt = 0;
				DateTime temp = new DateTime();
				string strCenlatlon = string.Empty;
				foreach (var htLxdata in lstLxData)
				{
					DateTime dt = new DateTime();
					if (icnt == 0)
					{
						if (DateTime.TryParse(htLxdata["CellTime"].ToString(), out dt))
						{
							temp = dt;
							strCenlatlon = htLxdata["CellCentroidLatLon"].ToString();
						}
					}
					else if (DateTime.TryParse(htLxdata["CellTime"].ToString(), out dt))
					{
						if (temp > dt)
						{
							temp = dt;
							strCenlatlon = htLxdata["CellCentroidLatLon"].ToString();
						}
					}
					icnt++;
				}

				#endregion

				#region Get TimeZoneOffset

				TimeZoneData data = null;
				
				if (!string.IsNullOrEmpty(strCenlatlon))
				{
					double latitude = 0, longitude = 0;
					string[] latlong = strCenlatlon.Split(',');
					if (double.TryParse(latlong[0], out latitude) && double.TryParse(latlong[1], out longitude))
					{						
						data = TimeZoneSvcHelper.GetTimeZone(latitude, longitude);
					}
				}

				#endregion

				int iCount = 0;
				foreach (var htLxdata in lstLxData)
				{
				    DateTime dt = new DateTime();
				    DateTime dtLocal = new DateTime();
				    string strLocalformat = string.Empty;
				    float fTotalLXRate = 0;
				    float fICRate = 0;
				    float fCGRate = 0;
				    float fSize = 0;

				    #region UTC to Local (TimeZoneOffset)

				    if (DateTime.TryParse(htLxdata["CellTime"].ToString(), out dt))
				    {
				        if (data != null)
				        {
				            dtLocal = dt.AddHours(data.TimeZoneOffset);
				            bool bDayLight = false;
				            if ((dtLocal >= data.DayLightSavingStartTime) && (dtLocal <= data.DayLightSavingEndTime))
				            {
				                dtLocal = dtLocal.AddHours(1);
				                bDayLight = true;
				            }
				            if (iCount == 0)
				            {
				                //strLocalformat = dtLocal.ToString("hh:mm tt TZ - MM/dd/yyyy");
								strLocalformat = dtLocal.ToString("hh:mm tt TZ - MM/dd/yyyy").Replace("TZ", (bDayLight ? data.DayLightSavingTimeAbbr : data.StandardTimeAbbr));
				            }
				        }
				    }
				    #endregion

				    #region UTC to Local (Working Code)
				    //if ((DateTime.TryParse(htLxdata["CellTime"].ToString(), out dt)) && (!string.IsNullOrEmpty(htLxdata["CellCentroidLatLon"].ToString())))
				    //{	
				    //    double latitude = 0, longitude = 0;
				    //    string[] latlong = htLxdata["CellCentroidLatLon"].ToString().Split(',');
				    //    if(double.TryParse(latlong[0],out latitude)&&double.TryParse(latlong[1], out longitude))
				    //    {
				    //        dtLocal = TimeZoneSvcHelper.UtcToLocal(dt, latitude, longitude);

				    //        if(iCount == 0)
				    //        {
				    //            strLocalformat = TimeZoneSvcHelper.UtcToLocalString(dt, latitude, longitude, "hh:mm tt TZ - MM/dd/yyyy");							
				    //        }
				    //    }
				    //    else
				    //    {
				    //        AppLogger.LogDebug("Not able to Parse Lat Lon velues. ");
				    //        System.Diagnostics.Debug.WriteLine("Not able to Parse Lat Lon velues ");
				    //    }
				    //}
				    #endregion

				    #region Header Data

				    if (iCount == 0)
				    {
				        Label lblcellData = (Label)this.FindControl("lbcellData");
				        Label lblCellDate = (Label)this.FindControl("lblCellDate");

				        if (lblcellData != null)
				        {
				            lblcellData.Text = " CellID : " + htLxdata["CellID"].ToString();
				        }
				        if (lblCellDate != null)
				        {
				            //lblCellDate.Text = " Date : " + htLxdata["CellTime"].ToString();	
				            lblCellDate.Text = " Date : " + strLocalformat.ToString();
				        }
				    }

				    iCount++;

				    #endregion

				    #region Constructing Series

				    float.TryParse(htLxdata["CellAreaInSquareKm"].ToString(), out fSize);

				    if (float.TryParse(htLxdata["TotalRateFlashesPerMinute"].ToString(), out fTotalLXRate))
				    {
				        LightningAlertChart.Series["LxTotalRate"].Points.AddXY(dtLocal, fTotalLXRate);
				    }
				    if (float.TryParse(htLxdata["ICRateFlashesPerMinute"].ToString(), out fICRate))
				    {
				        LightningAlertChart.Series["LxICRate"].Points.AddXY(dtLocal, fICRate);
				    }
				    if (float.TryParse(htLxdata["CGRateFlashesPerMinute"].ToString(), out fCGRate))
				    {
				        LightningAlertChart.Series["LxCGRate"].Points.AddXY(dtLocal, fCGRate);

				    }
				    dtable.Rows.Add(dtLocal.ToString("hh:mm:ss tt"), fSize, fICRate, fCGRate, fTotalLXRate);

				    #region working code UTC time
				    //if ((DateTime.TryParse(htLxdata["CellTime"].ToString(), out dt)) && (float.TryParse(htLxdata["TotalRateFlashesPerMinute"].ToString(), out fTotalLXRate)))
				    //{
				    //    LightningAlertChart.Series["LxTotalRate"].Points.AddXY(dt, fTotalLXRate); 					
				    //}
				    //if ((DateTime.TryParse(htLxdata["CellTime"].ToString(), out dt)) && (float.TryParse(htLxdata["ICRateFlashesPerMinute"].ToString(), out fICRate)))
				    //{
				    //    LightningAlertChart.Series["LxICRate"].Points.AddXY(dt, fICRate);						
				    //}
				    //if ((DateTime.TryParse(htLxdata["CellTime"].ToString(), out dt)) && (float.TryParse(htLxdata["CGRateFlashesPerMinute"].ToString(), out fCGRate)))
				    //{
				    //    LightningAlertChart.Series["LxCGRate"].Points.AddXY(dt, fCGRate);					

				    //}
				    //dtable.Rows.Add(dt.ToString("hh:mm:ss tt"), fSize, fICRate, fCGRate, fTotalLXRate);
				    #endregion

				    #endregion
				}

				#region Setting of Chart

				LightningAlertChart.Series["LxTotalRate"].ToolTip = "Cell Time = #VALX{t}\nTotal Rate = #VALY{##0.##}";
				LightningAlertChart.Series["LxICRate"].ToolTip = "Cell Time = #VALX{t}\nIC Rate = #VALY{##0.##}";
				LightningAlertChart.Series["LxCGRate"].ToolTip = "Cell Time = #VALX{t}\nCG Rate = #VALY{##0.##}";

				LightningAlertChart.ChartAreas["LxChartArea"].AxisX.TitleFont = new Font("Arial", 9, FontStyle.Bold);
				LightningAlertChart.ChartAreas["LxChartArea"].AxisX.TitleForeColor = Color.Black;

				LightningAlertChart.ChartAreas["LxChartArea"].AxisY.TitleFont = new Font("Arial", 9, FontStyle.Bold);
				LightningAlertChart.ChartAreas["LxChartArea"].AxisY.TitleForeColor = Color.Black;

				LightningAlertChart.Series["LxTotalRate"].AxisLabel = "#VALX{t}";
				LightningAlertChart.Series["LxICRate"].AxisLabel = "#VALX{t}";
				LightningAlertChart.Series["LxCGRate"].AxisLabel = "#VALX{t}";

				#endregion

				LxGridView.DataSource = dtable;
				LxGridView.DataBind();
			}
			catch (Exception ex)
			{
				Aws.Core.Utilities.EventManager.LogError("Exception Occured in Plotting the Graph: " + ex);
				System.Diagnostics.Debug.WriteLine("Exception Occured in Plotting the Graph " + ex);
			}

		}		
		
		protected void LxDropDownList_selectedindexchanged(object sender, EventArgs e)
		{			
            List<Dictionary<string, string>> lstLxData = GetGeometryList(Request.QueryString["key"], Request.QueryString["cellid"]);
			DrawChart(lstLxData);
		}

	}
}
