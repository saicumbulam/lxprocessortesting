﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Collections.Generic;
using AwsGeometries;
using System.IO;
using System.Threading;
using System.Web.Script.Serialization;
using System.Web.Caching;
using System.Xml;
using System.Text;

namespace LightningAnalysis
{
	public static class LightningArchivedManager
	{
		public static List<IGeometry> GetArchiveGeometryList(string cacheKey,string stype)
		{
			List<IGeometry> list = new List<IGeometry>();
			if (HttpRuntime.Cache[cacheKey] != null)
			{
				return HttpRuntime.Cache[cacheKey] as List<IGeometry>;
			}

			//Split the Cachekey and get it from archive folder.
			#region
			long mStartTick = Environment.TickCount;
			string[] dates = cacheKey.Split("_".ToCharArray());
			DateTime sdate = new DateTime(Convert.ToInt64(dates[0]));
			DateTime edate = new DateTime(Convert.ToInt64(dates[1]));		
			

			string ArchivedPath = ConfigurationManager.AppSettings["ArchievedPath"];
			string ArchivedFilename = string.Empty;

			if (stype == "Polygon")
				ArchivedFilename = ConfigurationManager.AppSettings["ArchievedPolygonFilename"];
			else
				ArchivedFilename = ConfigurationManager.AppSettings["ArchievedFilename"];

			TimeSpan datediff = edate - sdate;
			int iTotalDays = datediff.Days;
			
            if (sdate.Day != edate.Day)
                iTotalDays++;
            
            List<string> fileList = new List<string>();
			for (int i = 0; i <= iTotalDays; i++)
			{
				DateTime dt = sdate + new TimeSpan(i, 0, 0, 0);
				string fname = string.Format("{0}\\{1}\\{2}_{3}*.*",
							  ArchivedPath, dt.ToString("yyyy/M/d").Replace("/", "\\"),
							  ArchivedFilename, dt.ToString("yyyyMMdd"));

				string dirPath = Path.GetDirectoryName(fname);
				if (!Directory.Exists(dirPath))
					continue;

                string[] files = Directory.GetFiles(dirPath, Path.GetFileName(fname));
                if (files != null && files.Length > 0)
                {
                    fileList.AddRange(files);    
                }
                

			}
			#endregion

			#region

			List<string> lstArchivedFiles = new List<string>();

            foreach (var file in fileList)
			{
				double _sdate = double.Parse(sdate.ToString("yyyyMMddHHmm"));
				double _edate = double.Parse(edate.ToString("yyyyMMddHHmm"));
				string filename = Path.GetFileNameWithoutExtension(file);
				string[] fVal = filename.Split("_".ToCharArray());
				if ((fVal.Length >= 2) && (!string.IsNullOrEmpty(fVal[0])) && (!string.IsNullOrEmpty(fVal[1])))
				{
					double fct = double.Parse(fVal[1].ToString());
					if (fct >= _sdate && fct <= _edate)
					{
						lstArchivedFiles.Add(file);
					}
				}
			}
			
			System.Diagnostics.Debug.WriteLine("Time to get Archived files: " + (Environment.TickCount - mStartTick).ToString() + "msec");

			#endregion
			//Go through the Archive folder List
			foreach (var filename in lstArchivedFiles)
			{
				#region
				List<IGeometry> _parsedData = null;
				StreamReader strmReader = new StreamReader(filename);
				JavaScriptSerializer serializer = new JavaScriptSerializer(new SimpleTypeResolver())
				{
					MaxJsonLength = 100 * 1024 * 1024
				};
				string data = strmReader.ReadToEnd();
				strmReader.Close();

				if (data != null && data.Count() > 0)
				{
					_parsedData = serializer.Deserialize<List<IGeometry>>(data);
					list.AddRange(_parsedData);
				}
				#endregion
			}

			HttpRuntime.Cache.Add(cacheKey, list, null, Cache.NoAbsoluteExpiration, new TimeSpan(1, 0, 0), CacheItemPriority.Normal, null);

			return list;
		}

		
	}
}
