﻿using System;
using CommandLine;
using LxPortalLib.Models;

namespace QuadKeyGenerator
{
    class Options
    {
        [Option("querytype", Required = true, HelpText = "Valid values are Circle, BoundingBox, or QuadKeyToLatLon (returns center lat/lon for a given quadKey)")]
        public QueryType QueryType { get; set; }


        [Option("radius", SetName = "Circle", Default = 10, HelpText = "Size of radius")]
        public int? Radius { get; set; }

        [Option("radiustype", SetName = "Circle", Default = RadiusType.Miles, HelpText = "Valid values are Miles or Meters")]
        public RadiusType RadiusType { get; set; }

        [Option("latitude", SetName = "Circle", HelpText = "If QueryType is Circle, this must be included")]
        public float? Latitude { get; set; }

        [Option("longitude", SetName = "Circle", HelpText = "If QueryType is Circle, this must be included")]
        public float? Longitude { get; set; }



        [Option("latitudemax", SetName = "BoundingBox", HelpText = "If QueryType is BoundingBox, this must be included")]
        public float? LatitudeMax { get; set; }

        [Option("latitudemin", SetName = "BoundingBox", HelpText = "If QueryType is BoundingBox, this must be included")]
        public float? LatitudeMin { get; set; }

        [Option("longitudemax", SetName = "BoundingBox", HelpText = "If QueryType is BoundingBox, this must be included")]
        public float? LongitudeMax { get; set; }

        [Option("longitudemin", SetName = "BoundingBox", HelpText = "If QueryType is BoundingBox, this must be included")]
        public float? LongitudeMin { get; set; }

        [Option("quadkey", SetName = "QuadKey", HelpText = "If QueryType is QuadKeyToLatLon, this must be included")]
        public string QuadKey { get; set; }
    }
}
