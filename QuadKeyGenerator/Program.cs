﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Aws.Core.Utilities.Spatial;
using CommandLine;
using LxPortalLib.Models;

namespace QuadKeyGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            var cliParser = new Parser(with =>
            {
                with.CaseSensitive = false;
                with.EnableDashDash = true;
                with.IgnoreUnknownArguments = true;
                with.ParsingCulture = CultureInfo.InvariantCulture;
                with.HelpWriter = Parser.Default.Settings.HelpWriter;
                with.CaseInsensitiveEnumValues = true;
            });

            var options = cliParser.ParseArguments<Options>(args).MapResult(opts => opts, errors => null);

            if (options != null)
            {
                switch (options.QueryType)
                {
                    case QueryType.QuadKeyToLatLon:
                    {
                        double latitude, longitude;
                        QuadKeyToLatLon(options.QuadKey, out latitude, out longitude);
                        Console.WriteLine($"Quad key {options.QuadKey} = Latitude: {latitude}, Longitude: {longitude}");
                        break;

                    }

                    case QueryType.BoundingBox:
                    {
                        var quadKeys = GetQuadKeys(GetBoundingBox(options.LatitudeMin.Value,
                                                                  options.LongitudeMin.Value,
                                                                  options.LatitudeMax.Value,
                                                                  options.LongitudeMax.Value));
                        Console.WriteLine($"Quad keys: {string.Join(", ", quadKeys)}");
                        break;
                    }

                    case QueryType.Circle:
                    {
                        var quadKeys = GetQuadKeys(GetBoundingBox(options.Latitude.Value,
                                                                  options.Longitude.Value,
                                                                  options.Radius.Value,
                                                                  options.RadiusType));
                        Console.WriteLine($"Quad keys: {string.Join(", ", quadKeys)}");
                        break;
                    }
                }
            }
        }

        private static List<string> GetQuadKeys(BoundingBox bBox)
        {
            const int levelOfDetail = 5;
            var quadKeys = new List<string>();

            // 1 = upper left, 2 = lower right
            int pixelX1, pixelY1;
            TileSystem.LatLongToPixelXY(bBox.LatitudeMax, bBox.LongitudeMin, levelOfDetail, out pixelX1, out pixelY1);
            int tileX1, tileY1;
            TileSystem.PixelXYToTileXY(pixelX1, pixelY1, out tileX1, out tileY1);

            int pixelX2, pixelY2;
            TileSystem.LatLongToPixelXY(bBox.LatitudeMin, bBox.LongitudeMax, levelOfDetail, out pixelX2, out pixelY2);
            int tileX2, tileY2;
            TileSystem.PixelXYToTileXY(pixelX2, pixelY2, out tileX2, out tileY2);

            // for loop
            for (var x = tileX1; x <= tileX2; x++)
            {
                for (var y = tileY1; y <= tileY2; y++)
                {
                    quadKeys.Add(TileSystem.TileXYToQuadKey(x, y, levelOfDetail));
                }
            }

            return quadKeys;
        }

        private static BoundingBox GetBoundingBox(float latitude, float longitude, int radius, RadiusType radiusType)
        {
            return new BoundingBox(latitude, longitude, radiusType == RadiusType.Meters ? ConvertMetersToMiles(radius) : radius);
        }

        private static BoundingBox GetBoundingBox(float latitudeMin, float longitudeMin, float latitudeMax, float longitudeMax)
        {
            return new BoundingBox(latitudeMin, longitudeMin, latitudeMax, longitudeMax);
        }

        private static float ConvertMetersToMiles(int meters)
        {
            return (float)(meters / 1609.344);
        }

        private static void QuadKeyToLatLon(string quadKey, out double latidude, out double longitude)
        {
            int tileX, tileY, levelOfDetail;
            TileSystem.QuadKeyToTileXY(quadKey, out tileX, out tileY, out levelOfDetail);

            int pixelX, pixelY;
            TileSystem.TileXYToPixelXY(tileX, tileY, out pixelX, out pixelY);

            TileSystem.PixelXYToLatLong(pixelX, pixelY, levelOfDetail, out latidude, out longitude);
        }

    }

    public enum QueryType { BoundingBox = 0, Circle = 1, QuadKeyToLatLon = 2 }
}
