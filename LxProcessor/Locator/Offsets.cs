using System.Collections.Generic;
using LxCommon;
using LxCommon.Models;
using LxProcessor.WaveformData;

namespace LxProcessor.Locator
{
    public class Offsets
    {
        private readonly DateTimeUtcNano _startTime;
        private readonly List<Offset> _offsetList;


        public Offsets(DateTimeUtcNano startTime)
        {
            CommonPortion = null;
            _startTime = startTime;
            _offsetList = new List<Offset>();
        }

        public void AddOffset(Offset offest)
        {
            _offsetList.Add(offest);
        }

        public void NormalizeOffsets()
        {
            double minOffset = int.MaxValue;
            foreach (Offset offset in _offsetList)
            {
                if (minOffset > offset.Value)
                    minOffset = offset.Value;
            }
            if (minOffset == 0)
                return;

            foreach (Offset offset in _offsetList)
            {
                offset.Value -= minOffset;
            }
        }

        #region MyRegion

        public DateTimeUtcNano StartTime
        {
            get { return _startTime; }
        }
        public List<Offset> OffsetList
        {
            get { return _offsetList; }
        }

        public CommonPortion CommonPortion { get; set; }

        public object ActiveSensors { get; set; }

        public override string ToString()
        {
            string str = OffsetList.Count + " sensors at " + StartTime + ":";
            foreach (Offset offset in OffsetList)
            {
                str += offset.StationId + ":" + offset.Value + " ";
            }
            return str;
        }
        #endregion
    }
    
    public class WaveformForOffset
    {
        public WaveformForOffset()
        {
            MinorVersion = 0;
            MajorVersion = 0;
            WaveformScore = null;
            StationId = string.Empty;
            SampleList = null;
            MaxBin = 0;
        }

        public DateTimeUtcNano OriginalStartTime { get; set; }

        public DateTimeUtcNano StartTime { get; set; }

        public DateTimeUtcNano EndTime { get; set; }
        
        public SensorVersion MajorVersion { get; set; }

        public SensorVersion MinorVersion { get; set; }

        public WaveformScore WaveformScore { get; set; }

        public string StationId { get; set; }

        public List<Sample> SampleList { get; set; }

        public int MaxBin { get; set; }

        
    }
}
