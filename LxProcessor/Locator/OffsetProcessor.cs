﻿using LxCommon;
using LxCommon.DataStructures;
using LxCommon.Utilities;
using LxProcessor.PostLocator;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace LxProcessor.Locator
{
    public class OffsetProcessor : ParallelQueueProcessor<Offsets>
    {
        private const int MinimumClrThreads = 3;   //use this value to control the threads
        private const int MaximumClrThreads = 4;   //use this value to control the threads

        private PostLocatorProcessor _postLocatorProcessor;
        private ConcurrentDictionary<string, StationData> _stationData;
        private MedianSampler<double> _sampler;
        private DateTime _lastPortionTimeStamp;

        public OffsetProcessor(PostLocatorProcessor postLocatorProcessor, ConcurrentDictionary<string, StationData> stationData, int sampleSize,
            ManualResetEventSlim stopEvent, int maxActiveWorkItems) : base(stopEvent, maxActiveWorkItems)
        {
            _postLocatorProcessor = postLocatorProcessor;
            _stationData = stationData;
            _sampler = new MedianSampler<double>(sampleSize);
            _lastPortionTimeStamp = DateTime.MinValue;
        }

        // Firts step to find the flash portions
        protected override void DoWork(Offsets offsets)
        {
            Tuple<int, int> threadpool1 = InitThreadPool();
            ThreadPool.SetMinThreads(threadpool1.Item1, threadpool1.Item2);
            ThreadPool.QueueUserWorkItem(runinthread, new object[] { offsets } );
        }

        public void runinthread(object state)
        {
            object[] array = state as object[];
            Offsets offsets = (Offsets)array[0]; ;
            //stopwatch has been started for processing
            Stopwatch sw = new Stopwatch();
            sw.Start();
            // find the flash portions from the flashlocator
            //ThreadPool.QueueUserWorkItem(delegate(object state) { },new object[] { offsets, _stationData, Count });
            List<LTGFlashPortion> flashPortions = FlashLocator.FindFlashPortionFromOffsets(offsets, _stationData, Count);
            //Dictionary<string, int> activeSensors = CommonPortionFinder.FindActiveSensors(offsets.CommonPortion.ShortPortions);
            if (flashPortions != null)
            {
                //add the active sensor table to the each of the portions in the flash portions(ie pulses)
                foreach (LTGFlashPortion portion in flashPortions)
                {
                    portion.ActiveSensorTable = offsets.ActiveSensors;
                    _postLocatorProcessor.AddPortion(portion);
                    // move default value 01/01/0001 to the flash timestamp
                    if (portion.TimeStamp.BaseTime > _lastPortionTimeStamp)
                    {
                        _lastPortionTimeStamp = portion.TimeStamp.BaseTime;
                    }
                }
            }
            sw.Stop();
            _sampler.AddSample(sw.Elapsed.TotalMilliseconds);
        }

        public DateTime LastPortionTimeStamp
        {
            get { return _lastPortionTimeStamp; }
        }

        public Tuple<double, double, double> DurationMinMaxMedian
        {
            get { return _sampler.GetMinMaxMedian(); }
        }

        private static Tuple<int, int> InitThreadPool()
        {
            int logicalProcessors = Environment.ProcessorCount;

            int minWorkerThreads = MinimumClrThreads * logicalProcessors;
            int minIoThreads = MinimumClrThreads * logicalProcessors;
            //int maxWorkerThreads = MaximumClrThreads * logicalProcessors;
            //int maxIoThreads = MaximumClrThreads * logicalProcessors;

            //set the minumum number of worker and IO threads
            //ThreadPool.SetMaxThreads(maxWorkerThreads, maxIoThreads);
            //ThreadPool.SetMinThreads(minWorkerThreads, minIoThreads);
            return Tuple.Create(minWorkerThreads, minIoThreads);
        }
    }
}
