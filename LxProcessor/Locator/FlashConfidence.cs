using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using LxCommon;
using LxCommon.DataStructures;

using LxProcessor.Common;
using LxProcessor.WaveformData;

namespace LxProcessor.Locator
{
    public class FlashConfidence
    {
        private readonly static ConcurrentSimplePool<SortedList<double, string>> _sensorDistancePool;
        private readonly static ConcurrentSimplePool<List<double>> _angleListPool;
        private readonly static ConcurrentSimplePool<List<int>> _quadrantListPool;
        private readonly static ConcurrentSimplePool<int[]> _binsPool;

        static FlashConfidence()
        {
            _sensorDistancePool = new ConcurrentSimplePool<SortedList<double, string>>(Config.SensorDistancePoolSize,
                () => { return new SortedList<double, string>(); },
                (list) => { list.Clear(); },
                "_sensorDistancePool");

            _angleListPool = new ConcurrentSimplePool<List<double>>(Config.AngleListPoolSize,
                () => { return new List<double>(); },
                (list) => { list.Clear(); },
                "_angleListPool");

            _quadrantListPool = new ConcurrentSimplePool<List<int>>(Config.QuadrantListPoolSize,
                () => { return new List<int>(); },
                (list) => { list.Clear(); },
                "_quadrantListPool");

            _binsPool = new ConcurrentSimplePool<int[]>(Config.BinsPoolSize,
                () => { return new int[36]; },
                (array) =>
                {
                    for (int i = 0; i < 36; i++) array[i] = default(int);

                },
                "_binsPool");
        }

        public static void GetFlashesConfidence(List<LTGFlash> flashList)
        {
            foreach (LTGFlash flash in flashList)
            {
                GetFlashConfidence(flash);
            }
        }

        private static void GetFlashConfidence(LTGFlash flash)
        {
            flash.Confidence = 100;
            if (flash.FlashPortionList != null)
            {
                foreach (LTGFlashPortion flashPortion in flash.FlashPortionList)
                {
                    if (flash.Confidence > flashPortion.Confidence)
                        flash.Confidence = flashPortion.Confidence;
                }
            }
        }
        // get the flash portion confidence
        public static void GetFlashPortionConfidence(LTGFlashPortion flashPortion, ConcurrentDictionary<string, StationData> stationDataTable, bool postProcess)
        {
            if (Config.ProcessorType == ProcessorType.ProcessorTypeHF)
            {
                if (postProcess)
                {
                    flashPortion.Confidence = GetPostFlashPortionConfidenceFromNearbySensors(flashPortion, stationDataTable, _sensorDistancePool);
                }
                else
                {
                    // This is for the prepocess. make the confidence equal to 100
                    flashPortion.ConfidenceData.NearSensorConfidence = 100;
                    flashPortion.Confidence = flashPortion.ConfidenceData.NearSensorConfidence;
                }
                //get the maximum angle of sensors from the flash. the anglelistpool and quadrant list pool 
                // are the constants value which are coming
                flashPortion.ConfidenceData.BiggestAngle = (short)(GetMaxAngleOfSensorsFromFlash(flashPortion, stationDataTable, _angleListPool, _quadrantListPool) * 180 / Math.PI);

                //get the number of bins of sensors from the flash
                flashPortion.ConfidenceData.SensorDistribution = (short)GetTotalBinsOfSensorsFromFlash(flashPortion, stationDataTable, _binsPool);

                // Very loose confidence check 35 degrees / 2 bins
                if (flashPortion.ConfidenceData.BiggestAngle < LtgConstants.MinSensorAroundFlashAngle || flashPortion.ConfidenceData.SensorDistribution < 2)
                    flashPortion.Confidence = 0;

            }
            else if (Config.ProcessorType == ProcessorType.ProcessorTypeLF)
            {
                if (flashPortion.Confidence > 100)
                    flashPortion.Confidence = 100;
            }
        }

        private static int GetPostFlashPortionConfidenceFromNearbySensors(LTGFlashPortion flashPortion, ConcurrentDictionary<string, StationData> stationDataTable,
            ConcurrentSimplePool<SortedList<double, string>> sensorDistancePool)
        {
            if (flashPortion.OffsetList.Count >= LtgConstants.MaxSensorNumberInPostProcessingLocator - 4)
            {
                flashPortion.ConfidenceData.NearSensorConfidence = 100;
            }
            else
            {
                flashPortion.ConfidenceData.NearSensorConfidence = CheckPostFlashPortionConfidenceWithClosestSensors(flashPortion, stationDataTable, sensorDistancePool);
            }

            return flashPortion.ConfidenceData.NearSensorConfidence;
        }

        private static short CheckPostFlashPortionConfidenceWithClosestSensors(LTGFlashPortion postPortion, ConcurrentDictionary<string, StationData> stationTable,
            ConcurrentSimplePool<SortedList<double, string>> sensorDistancePool)
        {
            // Original common portion
            CommonPortion commonPortion = (CommonPortion)postPortion.CommonPortionRef;
            StationData station;
            short confidence = 0;

            SortedList<double, string> sensorDistance = sensorDistancePool.Take();

            // Find the short portions (sensors)  that meet the distance to the flash

            int i = 0;
            ShortPortion shortPortion;
            while (i < commonPortion.ShortPortions.Count)
            {
                shortPortion = commonPortion.ShortPortions[i];
                if (!sensorDistance.ContainsValue(shortPortion.StationId) && stationTable.TryGetValue(shortPortion.StationId, out station))
                {
                    double distance = station.DistanceFromLatLongHeightInMicrosecond(postPortion.Latitude, postPortion.Longitude, 0);
                    sensorDistance.Add(distance, shortPortion.StationId);
                }
                i++;
            }

            int closestSensor = sensorDistance.IndexOfValue(postPortion.OffsetList[0].StationId);
            int farthestSensor = sensorDistance.IndexOfValue(postPortion.OffsetList[postPortion.OffsetList.Count - 1].StationId);


            if (farthestSensor == -1 || closestSensor == -1)
            {
                confidence = 0;
            }
            else
            {
                confidence = (short)(postPortion.OffsetList.Count * 100 / (farthestSensor + 1));
                if (confidence > 100) confidence = 100;
            }

            sensorDistancePool.Return(sensorDistance);

            return confidence;
        }

        // Calculate the max angle from the sensors to the flash
        private static double GetMaxAngleOfSensorsFromFlash(LTGFlashPortion flashPortion, ConcurrentDictionary<string, StationData> stationDataTable,
            ConcurrentSimplePool<List<double>> angleListPool, ConcurrentSimplePool<List<int>> quadrantListPool)
        {
            double startAngle = Double.MaxValue;
            double endAngle = Double.MinValue;

            List<double> angleList = angleListPool.Take();
            List<int> quadrantList = quadrantListPool.Take();

            foreach (Offset offset in flashPortion.OffsetList)
            {
                StationData stationData;
                if (stationDataTable.TryGetValue(offset.StationId, out stationData))
                {
                    double angle = stationData.BeringFromALocation(flashPortion.Latitude, flashPortion.Longitude);

                    int quadrant = GetQuadrant(angle);

                    if (!quadrantList.Contains(quadrant))
                        quadrantList.Add(quadrant);

                    angleList.Add(angle);
                }
            }

            double maxAngle = double.NaN;
            if (quadrantList.Count >= 3)
            {
                maxAngle = 1.5 * Math.PI;
            }
            else if (quadrantList.Count == 2)
            {
                int delta = Math.Abs(quadrantList[1] - quadrantList[0]);
                if (delta == 2)
                { // two quadrants apart
                    maxAngle = Math.PI;
                }
                else if (delta == 3)
                {   // quadrant 1 and 4
                    for (int i = 0; i < angleList.Count; i++)
                    {
                        angleList[i] = angleList[i] - Math.PI * 1.5;
                        if (angleList[i] < 0)
                        {
                            angleList[i] = angleList[i] + 2.0 * Math.PI;
                        }
                    }
                }
            }

            if (double.IsNaN(maxAngle))
            {
                foreach (double angle in angleList)
                {
                    if (angle < startAngle)
                        startAngle = angle;
                    if (angle > endAngle)
                        endAngle = angle;
                }
                maxAngle = endAngle - startAngle;
            }

            angleListPool.Return(angleList);
            quadrantListPool.Return(quadrantList);

            return maxAngle;
        }

        private static int GetQuadrant(double angle)
        {
            int quadrant = 0;
            if (angle >= 0 && angle < Math.PI * 0.5)
            {
                quadrant = 1;
            }
            else if (angle >= Math.PI / 2 && angle < Math.PI)
            {
                quadrant = 2;
            }
            else if (angle >= Math.PI && angle < Math.PI * 1.5)
            {
                quadrant = 3;
            }
            else if (angle >= Math.PI * 1.5 && angle <= Math.PI * 2.0)
            {
                quadrant = 4;
            }

            return quadrant;
        }

        // Divide the 360 degree into 36 bins, each of which has 10 degrees
        // Check total how many bins that all the sensors occupy
        private static int GetTotalBinsOfSensorsFromFlash(LTGFlashPortion flashPortion, ConcurrentDictionary<string, StationData> stationDataTable,
            ConcurrentSimplePool<int[]> binPool)
        {
            int totalBin = 0;
            int[] bins = binPool.Take();

            foreach (Offset offset in flashPortion.OffsetList)
            {
                StationData station;
                if (stationDataTable.TryGetValue(offset.StationId, out station))
                {
                    double angle = station.AngleFromALocation(flashPortion.Latitude, flashPortion.Longitude, flashPortion.Height);
                    bins[GetAngleBinNumber(angle)]++;
                }
            }

            // Adjust for neighbor bins
            for (int i = 0; i < 36; i++)
            {
                if (bins[i] != 0)
                    totalBin++;
            }

            binPool.Return(bins);

            return totalBin;
        }

        private static int GetAngleBinNumber(double angle)
        {
            return (int)(angle * 18 / Math.PI);
        }
    }

}
