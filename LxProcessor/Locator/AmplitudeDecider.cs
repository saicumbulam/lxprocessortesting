using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using Aws.Core.Utilities;
using LxCommon;
using LxCommon.Models;
using LxProcessor.WaveformData;

namespace LxProcessor.Locator
{
    public static class AmplitudeDecider
    {
        public static void UpdateAmplitude(List<LTGFlash> flashList, ConcurrentDictionary<string, StationData> stationData)
        {
            foreach (LTGFlash flash in flashList)
            {
                UpdateAmplitude(flash, stationData);
            }
        }

        private static void UpdateAmplitude(LTGFlash flash, ConcurrentDictionary<string, StationData> stationData)
        {
            float flashCurrent = 0;
            if (flash.FlashPortionList != null)
            {
                foreach (LTGFlashPortion flashPortion in flash.FlashPortionList)
                {
                    float theCurrent = UpdateAmplitude(flashPortion, stationData);

                    if (Math.Abs(flashCurrent) < Math.Abs(theCurrent))
                    {
                        flashCurrent = theCurrent;
                    }

                }
                flash.Amplitude = flashCurrent;
            }
        }


        //public static void UpdateAmplitude(List<LTGFlashPortion> flashPortionList, ConcurrentDictionary<string, StationData> stationData)
        //{
        //    foreach (LTGFlashPortion flashPortion in flashPortionList)
        //    {
        //        UpdateAmplitude(flashPortion, stationData);
        //    }
        // }

        public static float UpdateAmplitude(LTGFlashPortion flashPortion, ConcurrentDictionary<string, StationData> stationData)
        {
            float amplitude = 0;
            try
            {
                // Find the closest 5 sensors
                List<Offset> offsetList = flashPortion.ClosestSensorOffsetForClassification;
                if (offsetList != null)
                {
                    int polarity = 0;
                    List<float> absCurrentList = new List<float>();

                    // Use the first 3 sensors
                    int i = 0;
                    int selectedSensorNum = 0;


                    while (i < offsetList.Count && selectedSensorNum < LtgConstants.MaxNumberOfNearestSensorsForClassification)
                    {
                        Offset offset = offsetList[i++];


                        StationData station;
                        if (stationData.TryGetValue(offset.StationId, out station))
                        {
                            StationDecibelParameter calibData = station.CalibrationData.GetActiveCalibrationData();
                            if (!calibData.Valid)
                            {
                                continue;
                            }

                            WaveformScore score = ((WaveformForOffset) offset.WaveFormforOffset).WaveformScore;

                            double peakVolts = GetPeakVolts(score);
                            // skip current iteration if greater than saturation value
                            if (peakVolts > LtgConstants.SaturationValueForPeakcurrent)
                            {
                                continue;
                            }
                            // calculate the distnace of the flash in meter
                            double distanceOfFlashAndSensorInMeter = offset.DistanceToFlash*
                                                                     LtgConstants.LightspeedInMetersPerMicrosecond;
                            // calculate flash portion current
                            float flashPortionCurrent = CalculateFlashPortionCurrent(offset.StationId, peakVolts,
                                                                                     distanceOfFlashAndSensorInMeter,
                                                                                     stationData);

                            // skip current iteration if greater than max allowed current
                            if (Math.Abs(flashPortionCurrent) > LtgConstants.MaxAllowedPeakCurrent)
                            {
                                continue;
                            }

                            if (flashPortionCurrent >= 0)
                            {
                                polarity++;
                            }
                            else if (flashPortionCurrent < 0)
                            {
                                polarity--;
                            }

                            absCurrentList.Add(Math.Abs(flashPortionCurrent));
                            selectedSensorNum++;
                        }
                    }

                    // deciding the amplitude value based on the abs.current value
                    if (absCurrentList.Count == 0)
                    {
                        flashPortion.Amplitude = 0;
                    }
                    else
                    {
                        absCurrentList.Sort();
                        
                        if (polarity == 0)
                            polarity = -1;
                        //took the middle of the amplitude list from the list of amplitudes with the sign 
                        // sign is dependent on the polarity 
                        flashPortion.Amplitude = (float)absCurrentList[absCurrentList.Count / 2] * Math.Sign(polarity);
                    }
                    // amplitude is passed here
                    amplitude = flashPortion.Amplitude;
                }
            }
            catch (Exception ex)
            {
                EventManager.LogWarning(EventId.Locator.AmplitudeDecider.UpdateAmplitude, "Failure in update amplitude", ex);   
            }

            return amplitude;
        }


        // should return Amperes
        private static float CalculateFlashPortionCurrent(string stationId, double peakVolts, double distanceInMeter, ConcurrentDictionary<string, StationData> stationData)
        {
            float current = 0.0f;
            StationData station;
            if (stationData.TryGetValue(stationId, out station))
            {
                StationDecibelParameter calibData = station.CalibrationData.GetActiveCalibrationData();
                if (calibData.Valid)
                    current = LTGFlashPortion.CalculateFlashPortionCurrent(peakVolts, distanceInMeter, calibData);
            }
            return current;
        }
// 
        // if the difference between the positive and negative peaks is less than 50% and the time span is less than 100 microseconds, choose the peaks that apears first
        // otherwise choose the larger peak
        private static double GetPeakVolts(WaveformScore score)
        {
            double peakVolts;
            // check for zero
            if (score.PeakForCalculatingPeakCurrent != default(Waveform))
            {
                peakVolts = score.PeakForCalculatingPeakCurrent.Amplitude;
            }
            else
            {
                // declare first has the score first waveform
                Waveform first = score.FirstPeakWaveform;
                //declare second as zero
                Waveform second = default(Waveform);

                // If the second waveform is bigger than the first one
                // pass the score abs waveform to second if its diffrent from first
                if (score.AbsPeakWaveform != score.FirstPeakWaveform)
                {
                    second = score.AbsPeakWaveform;
                }
                // if second is zero then return first amplitude
                if (second == default(Waveform))
                {
                    return first.Amplitude;
                }
                // get the minimum between first and the second waveform amplitiude
                double divsor = Math.Min(Math.Abs(first.Amplitude), Math.Abs(second.Amplitude));
                // if minimum is greater than zero
                if (divsor > 0)
                {
                    // return the maxim diff divided by minimum diffrence between first and second waveform
                    double valueDiff = Math.Max(Math.Abs(first.Amplitude), Math.Abs(second.Amplitude)) / Math.Min(Math.Abs(first.Amplitude), Math.Abs(second.Amplitude));
                    // get the time diffence betwen first and the second waveform
                    long timeDiff = second.TimeStamp.Microseconds - first.TimeStamp.Microseconds;

                    // If the larger peak is less than twice the smaller peak and the time is less than 100 microsecond apart
                    // Choose the polarity of the first peak
                    if (valueDiff < 1.6 && timeDiff < 100)
                        peakVolts = first.Amplitude;
                    else
                        peakVolts = second.Amplitude;

                }
                else
                {
                    peakVolts = first.Amplitude;
                }
            }
            return peakVolts;
        }

    }
}
