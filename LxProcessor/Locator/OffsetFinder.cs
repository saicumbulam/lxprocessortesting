using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using Aws.Core.Utilities;
using LxCommon;
using LxCommon.DataStructures;
using LxCommon.Models;
using LxProcessor.Common;
using LxProcessor.WaveformData;

namespace LxProcessor.Locator
{
    public class OffsetFinder
    {
        private readonly static ConcurrentSimplePool<List<double>> _crossCorrelatePool;
        private readonly static ConcurrentSimplePool<List<short>> _eValuesPool;

        static OffsetFinder()
        {
            _crossCorrelatePool = new ConcurrentSimplePool<List<double>>(Config.CrossCorrelatePoolSize,
                () => { return new List<double>(); },
                list => list.Clear(),
                "_crossCorrelatePool");

            _eValuesPool = new ConcurrentSimplePool<List<short>>(Config.EValuesPoolSize,
                () => { return new List<short>(); },
                list => list.Clear(),
                "_eValuesPool");
        }

        private static readonly int MinimumSensorNumberInLocator = Config.MinimumSensorNumberInLocator;
        //  cross-correlation: use the nearby sensors for cross-correlation or peak lineup
        public static Offsets GetOffsetsFromCommonPortion(CommonPortion commonPortion, ConcurrentDictionary<string, StationData> stationDataTable, bool usePeakLineup, bool useClone)
        {
            //List<ShortPortion> shortPortionList = commonPortion.ShortPortions;
            int dominantPolarity = 0;
            if (usePeakLineup)
            {
                dominantPolarity = DominantPolarity(commonPortion.ShortPortions);
            }

            Offsets offsets;
            if (dominantPolarity == 0)
            {
                if (LongDistanceShortPortions(stationDataTable, commonPortion.ShortPortions))
                {
                    offsets = GetOffsetsUsingNextCrossCorrelation(stationDataTable, commonPortion.ShortPortions, _crossCorrelatePool, _eValuesPool);
                }
                else
                {
                    offsets = GetOffsetsUsingFirstCorrelation(stationDataTable, commonPortion.ShortPortions, _crossCorrelatePool);
                }
            }
            else
            {
                // All the peaks have the same polarity so use peak lineup for the offsets
                offsets = GetOffsetsUsingPeakLineup(commonPortion.ShortPortions, dominantPolarity);
            }

            if (offsets != null)
            {
                if (useClone)
                {
                    offsets.CommonPortion = (CommonPortion)commonPortion.Clone();
                }
                else
                {
                    offsets.CommonPortion = commonPortion;
                }
            }

            return offsets;
        }

        private static WaveformForOffset ShortPortionGetWaveformForOffset(ConcurrentDictionary<string, StationData> stationTable, DateTimeUtcNano startTime, ShortPortion shortPortion)
        {
            WaveformForOffset wfo = null;
            string stationId = shortPortion.StationId;
            StationData station;
            if (stationTable.TryGetValue(stationId, out station))
            {
                // Original waveform startTime
                if (shortPortion.Waveforms != null && shortPortion.Waveforms.Count != 0)
                {
                    DateTimeUtcNano originalShortPortionStartTime = shortPortion.Waveforms[0].TimeStamp;

                    IList<Waveform> subWaveformList = shortPortion.Waveforms;

                    int maxBinSize = 0;
                    if (subWaveformList != null && subWaveformList.Count > 0)
                    {
                        List<Sample> waveFormArrayInBins = null;

                        if (station.MajorVersion == SensorVersion.ID_VERSION_SENSOR_9_MAJOR
                            && station.MinorVersion >= SensorVersion.ID_VERSION_SENSOR_9_MINOR1)
                        {
                            // Format with compression
                            if (Config.ProcessorType == ProcessorType.ProcessorTypeLF)
                            {
                                waveFormArrayInBins = GetSampleListForCrossCorrelationForLf(startTime, subWaveformList, ref maxBinSize, _eValuesPool);
                            }
                            else
                            {
                                waveFormArrayInBins = GetSampleListForCrossCorrelationForHf(startTime, subWaveformList, ref maxBinSize, _eValuesPool);
                            }
                        }
                        else
                        {
                            waveFormArrayInBins = GetSampleListForCrossCorrelationForVersion90(startTime, subWaveformList, ref maxBinSize, _eValuesPool);
                        }


                        if (waveFormArrayInBins != null)
                        {
                            int binArrayCount = waveFormArrayInBins.Count;
                            if (binArrayCount >= 2)
                            {
                                wfo = new WaveformForOffset
                                {
                                    StationId = stationId,
                                    MajorVersion = station.MajorVersion,
                                    MinorVersion = station.MinorVersion,
                                    SampleList = waveFormArrayInBins,
                                    MaxBin = maxBinSize,
                                    WaveformScore = (WaveformScore)shortPortion.WaveformScore.Clone(),
                                    OriginalStartTime = originalShortPortionStartTime,
                                    StartTime = subWaveformList[0].TimeStamp,
                                    EndTime = subWaveformList[subWaveformList.Count - 1].TimeStamp
                                };
                            }
                        }
                    }
                }
            }

            return wfo;
        }

        private static List<Sample> GetSampleListForCrossCorrelationForVersion90(DateTimeUtcNano startTime, IList<Waveform> waveformList, ref int maxBin, ConcurrentSimplePool<List<short>> eValuesPool)
        {
            List<Sample> sampleListForCc = null;
            if (waveformList.Count >= 2)
            {
                List<short> eValues = null;
                try
                {
                    long startMicrosecond = startTime.Microseconds;
                    maxBin = 0;
                    sampleListForCc = new List<Sample>();
                    int nlastBin = int.MaxValue;
                    eValues = eValuesPool.Take();
                    foreach (Waveform waveForm in waveformList)
                    {
                        long diff = waveForm.TimeStamp.Microseconds - startMicrosecond;

                        if (diff >= LtgConstants.MaxCorrelationArraySize || diff < 0)
                        {
                            //EventManager.LogInfo("Correlation array is too big: " + bin.ToString());
                            continue; // need revisit
                        }

                        int bin = (int)diff;

                        if (bin > nlastBin && eValues.Count > 0)
                        {
                            Sample sample = new Sample
                            {
                                Tick = (uint)nlastBin,
                                E = (short)(Sum(eValues) / eValues.Count)
                            };

                            if (sample.E != 0)
                                sampleListForCc.Add(sample);

                            eValues.Clear();
                            if (nlastBin > maxBin)
                                maxBin = nlastBin;
                        }
                        nlastBin = bin;
                        eValues.Add(waveForm.Amplitude);
                    }
                    if (eValues.Count > 0)
                    {
                        Sample sample = new Sample { Tick = (uint)nlastBin, E = (short)(Sum(eValues) / eValues.Count) };

                        if (sample.E != 0)
                            sampleListForCc.Add(sample);

                        eValues.Clear();

                        if (nlastBin > maxBin)
                            maxBin = nlastBin;
                    }

                }
                catch (Exception ex)
                {
                    EventManager.LogWarning(EventId.OffsetData.OffsetFinder.CrossCorrelation, "Failed to get waveform array for cross correlation version 9_0", ex);
                }
                finally
                {
                    if (eValues != null) eValuesPool.Return(eValues);
                }
            }

            return sampleListForCc;
        }

        private static List<Sample> GetSampleListForCrossCorrelationForHf(DateTimeUtcNano startTime, IList<Waveform> waveformList, ref int maxBin, ConcurrentSimplePool<List<short>> eValuesPool)
        {
            List<Sample> sampleListForCc = null;
            if (waveformList.Count >= 2)
            {
                List<short> eValues = null;
                try
                {
                    long startMicrosecond = startTime.Microseconds;

                    maxBin = 0;
                    sampleListForCc = new List<Sample>();
                    int nlastBin = int.MaxValue;
                    eValues = eValuesPool.Take();
                    foreach (Waveform waveform in waveformList)
                    {
                        long diff = waveform.TimeStamp.Microseconds - startMicrosecond;

                        if (diff >= LtgConstants.MaxCorrelationArraySize || diff < 0)
                        {
                            //EventManager.LogInfo("Correlation array is too big: " + curBin.ToString());
                            continue; // need revisit
                        }

                        int curBin = (int)diff;

                        if (curBin > nlastBin)
                        {
                            // Need to calculate the value of nlastBin first
                            if (eValues.Count > 0)
                            {
                                Sample sample = new Sample
                                {
                                    Tick = (uint)nlastBin,
                                    E = (short)(Sum(eValues) / eValues.Count)
                                };

                                if (sample.E != 0)
                                    sampleListForCc.Add(sample);

                                eValues.Clear();

                                if (nlastBin > maxBin)
                                    maxBin = nlastBin;
                            }
                        }

                        nlastBin = curBin;
                        eValues.Add(waveform.Amplitude);
                    }

                    // Last bin
                    if (eValues.Count > 0)
                    {
                        Sample sample = new Sample { Tick = (uint)nlastBin, E = (short)(Sum(eValues) / eValues.Count) };

                        if (sample.E != 0)
                            sampleListForCc.Add(sample);

                        eValues.Clear();

                        if (nlastBin > maxBin)
                            maxBin = nlastBin;
                    }

                }
                catch (Exception ex)
                {
                    EventManager.LogWarning(EventId.OffsetData.OffsetFinder.CrossCorrelationHf, "Failed to get waveform array for cross correlation hf", ex);
                    sampleListForCc = null;
                }
                finally
                {
                    if (eValues != null) eValuesPool.Return(eValues);
                }
            }

            return sampleListForCc;
        }

        // Get the waveform Array for LF waveforms
        private static List<Sample> GetSampleListForCrossCorrelationForLf(DateTimeUtcNano startTime, IList<Waveform> waveformList, ref int maxBin, ConcurrentSimplePool<List<short>> eValuesPool)
        {
            List<Sample> sampleList = null;
            List<short> eValues = null;
            if (waveformList.Count >= 2)
            {

                try
                {
                    long startMicrosecond = startTime.Microseconds;
                    maxBin = 0;
                    sampleList = new List<Sample>();
                    int nlastBin = int.MaxValue;
                    eValues = eValuesPool.Take();
                    foreach (Waveform waveform in waveformList)
                    {
                        long diff = waveform.TimeStamp.Microseconds - startMicrosecond;

                        if (diff >= 20 * LtgConstants.MaxCorrelationArraySize || diff < 0)
                        {
                            //EventManager.LogInfo("Correlation array is too big: " + curBin.ToString());
                            continue; // need revisit
                        }

                        int curBin = (int)diff;

                        if (curBin > nlastBin)
                        {
                            // Need to calculate the value of nlastBin first
                            if (eValues.Count > 0)
                            {
                                Sample sample = new Sample
                                {
                                    Tick = (uint)nlastBin,
                                    E = (short)(Sum(eValues) / eValues.Count)
                                };

                                if (sample.E != 0)
                                    sampleList.Add(sample);

                                eValues.Clear();

                                if (nlastBin > maxBin)
                                    maxBin = nlastBin;
                            }
                        }

                        nlastBin = curBin;
                        eValues.Add(waveform.Amplitude);
                    }

                    // Last bin
                    if (eValues.Count > 0)
                    {
                        Sample sample = new Sample { Tick = (uint)nlastBin, E = (short)(Sum(eValues) / eValues.Count) };

                        if (sample.E != 0)
                            sampleList.Add(sample);

                        eValues.Clear();

                        if (nlastBin > maxBin)
                            maxBin = nlastBin;
                    }
                }
                catch (Exception ex)
                {
                    EventManager.LogWarning(EventId.OffsetData.OffsetFinder.CrossCorrelationLf, "Failed to get waveform array for cross correlation lf", ex);
                    sampleList = null;
                }
                finally
                {
                    if (eValues != null) eValuesPool.Return(eValues);
                }
            }

            return sampleList;
        }

        // if A/B > 1.3 then A is dominant
        // 
        private static int DominantPolarity(IReadOnlyList<ShortPortion> shortPortionList)
        {
            int totalPositive = 0;
            int totalNegative = 0;
            int sensorNum = 0;
            while (sensorNum < shortPortionList.Count && sensorNum < 2 * MinimumSensorNumberInLocator)
            {
                ShortPortion portion = shortPortionList[sensorNum++];
                if (portion.WaveformScore.PositivePeak == default(Waveform))
                {
                    if (portion.WaveformScore.PeakList.Count == 1)
                        totalNegative++;
                }
                else if (portion.WaveformScore.NegativePeak == default(Waveform))
                {
                    if (portion.WaveformScore.PeakList.Count == 1)
                        totalPositive++;

                }
                else if (portion.WaveformScore.PositivePeak.Amplitude > Math.Abs(portion.WaveformScore.NegativePeak.Amplitude * 2))
                {
                    totalPositive++;
                }
                else if (Math.Abs(portion.WaveformScore.NegativePeak.Amplitude) > portion.WaveformScore.PositivePeak.Amplitude * 2)
                {
                    totalNegative++;
                }
            }

            if (totalPositive >= MinimumSensorNumberInLocator && totalPositive > 2 * totalNegative)
                return 1;
            if (totalNegative >= MinimumSensorNumberInLocator && totalNegative > 2 * totalPositive)
                return -1;

            return 0;
        }

        private static Offsets GetOffsetsUsingNextCrossCorrelation(ConcurrentDictionary<string, StationData> stationDataTable, IReadOnlyList<ShortPortion> shortPortionList,
            ConcurrentSimplePool<List<double>> crossCorrelatePool, ConcurrentSimplePool<List<short>> eValuesPool)
        {
            DateTimeUtcNano startTime = shortPortionList[0].StartTime;

            Offsets offsets = new Offsets(startTime);

            int i = 0;
            WaveformForOffset preWaveformForOffset = null;
            Offset preOffset = null;
            while (i < shortPortionList.Count)
            {
                ShortPortion curShortPortion = shortPortionList[i++];
                WaveformForOffset curWaveformForOffset = ShortPortionGetWaveformForOffset(stationDataTable, startTime, curShortPortion);
                if (curWaveformForOffset != null)
                {
                    Offset curOffset = new Offset
                    {
                        StationId = curShortPortion.StationId,
                        WaveFormforOffset = curWaveformForOffset,
                        AbsPeakAmplitude = curWaveformForOffset.WaveformScore.AbsPeakWaveform.Amplitude,
                        OriginalShortPortion = curShortPortion.Clone()
                    };
                    if (preWaveformForOffset == null)
                    {
                        curOffset.Value = 0;
                    }
                    else
                    {
                        int maxDelay = CrossCorrelate(preWaveformForOffset.SampleList, curWaveformForOffset.SampleList,
                            Math.Max(preWaveformForOffset.MaxBin, curWaveformForOffset.MaxBin), crossCorrelatePool);

                        curOffset.Value = preOffset.Value + maxDelay;
                    }

                    preWaveformForOffset = curWaveformForOffset;
                    offsets.AddOffset(curOffset);
                    preOffset = curOffset;
                }
            }

            if (offsets.OffsetList.Count <= MinimumSensorNumberInLocator)
            {
                offsets = null;
            }

            return offsets;
        }

        // Find the list of Offsets from a commonPortion using largest peaks rather than using cross-correlation
        private static Offsets GetOffsetsUsingPeakLineup(IReadOnlyList<ShortPortion> shortPortionList, int dominantPolarity)
        {
            Offsets offsets = new Offsets(shortPortionList[0].StartTime);
            Waveform firstPeak = default(Waveform);

            long startTime = 0;
            foreach (ShortPortion shortPortion in shortPortionList)
            {
                Waveform peak = (dominantPolarity > 0) ? shortPortion.WaveformScore.PositivePeak : shortPortion.WaveformScore.NegativePeak;

                if (firstPeak == default(Waveform))
                {
                    firstPeak = peak;
                    startTime = firstPeak.TimeStamp.Microseconds;
                }
                Offset offset = new Offset
                {
                    StationId = shortPortion.StationId,
                    Value = (peak.TimeStamp.Microseconds - startTime),
                    WaveFormforOffset = new WaveformForOffset(),
                    OriginalShortPortion = shortPortion.Clone()
                };
                ((WaveformForOffset)offset.WaveFormforOffset).WaveformScore = (WaveformScore)shortPortion.WaveformScore.Clone();
                offset.AbsPeakAmplitude = ((WaveformForOffset)offset.WaveFormforOffset).WaveformScore.AbsPeakWaveform.Amplitude;
                offsets.AddOffset(offset);
            }
            offsets.NormalizeOffsets();
            return offsets;
        }

        // Check the distance of the shortportions
        private static bool LongDistanceShortPortions(ConcurrentDictionary<string, StationData> stationTable, IReadOnlyList<ShortPortion> shortPortionList)
        {
            bool retVal = false;

            if (shortPortionList.Count > 5)
            {
                StationData station1;
                StationData station5;
                if (stationTable.TryGetValue(shortPortionList[0].StationId, out station1) && stationTable.TryGetValue(shortPortionList[4].StationId, out station5))
                {
                    retVal = (station1.DistanceFromStationInMicrosecond(station5) > 2000);
                }
            }

            return retVal;
        }

        private static ShortPortion FindSensorClosestToCentroidOfSensors(IReadOnlyList<ShortPortion> shortPortionList, ConcurrentDictionary<string, StationData> stationTable)
        {
            double centroidLatitude = 0.0;
            double centroidLongitude = 0.0;
            int numberOfSensors = 0;

            foreach (ShortPortion portion in shortPortionList)
            {
                StationData station;
                if (stationTable.TryGetValue(portion.StationId, out station))
                {
                    centroidLatitude += station.Latitude;
                    centroidLongitude += station.Longitude;
                    numberOfSensors++;
                }
            }

            centroidLatitude /= numberOfSensors;
            centroidLongitude /= numberOfSensors;

            ShortPortion closestPortion = null;
            double minDistance = double.MaxValue;
            foreach (ShortPortion portion in shortPortionList)
            {
                StationData station;
                if (stationTable.TryGetValue(portion.StationId, out station))
                {
                    double dist = station.DistanceFromLatLongHeightInMicrosecond(centroidLatitude, centroidLongitude, 0);
                    if (minDistance > dist)
                    {
                        minDistance = dist;
                        closestPortion = portion;
                    }
                }
            }

            return closestPortion;
        }

        // Find the list of Offset by cross-correlating all sensors with the first sensor
        private static Offsets GetOffsetsUsingFirstCorrelation(ConcurrentDictionary<string, StationData> stationDataTable, IReadOnlyList<ShortPortion> shortPortionList,
            ConcurrentSimplePool<List<double>> crossCorrelatePool)
        {
            // shortPortionList.Sort();
            DateTimeUtcNano startTime = shortPortionList[0].StartTime;

            Offsets offsets = new Offsets(startTime);

            ShortPortion bestShortPortion = FindSensorClosestToCentroidOfSensors(shortPortionList, stationDataTable);
            WaveformForOffset bestWaveFormOffset = ShortPortionGetWaveformForOffset(stationDataTable, startTime, bestShortPortion);

            foreach (ShortPortion shortPortion in shortPortionList)
            {
                WaveformForOffset waveFormForOffset = ShortPortionGetWaveformForOffset(stationDataTable, startTime, shortPortion);
                if (waveFormForOffset == null)
                    continue;
                if (bestWaveFormOffset == null)
                {
                    bestWaveFormOffset = waveFormForOffset;
                }

                int maxDelay = CrossCorrelate(bestWaveFormOffset.SampleList, waveFormForOffset.SampleList, Math.Max(bestWaveFormOffset.MaxBin, waveFormForOffset.MaxBin), crossCorrelatePool);

                Offset offset = new Offset
                {
                    StationId = waveFormForOffset.StationId,
                    Value = maxDelay,
                    WaveFormforOffset = waveFormForOffset,
                    AbsPeakAmplitude = waveFormForOffset.WaveformScore.AbsPeakWaveform.Amplitude,
                    OriginalShortPortion = shortPortion.Clone()
                };
                offsets.AddOffset(offset);
            }
            offsets.NormalizeOffsets();
            return offsets;
        }

        // return delay that has the max correlation coefficient
        private static int CrossCorrelate(List<Sample> x, List<Sample> y, int maxBin, ConcurrentSimplePool<List<double>> crossCorrelatePool)
        {
            int delay = 0;

            //double[] crossCorrelation = new double[2 * maxBin + 1];
            List<double> crossCorrelation = crossCorrelatePool.Take();
            int size = 2 * maxBin + 1;
            while (size > 0)
            {
                crossCorrelation.Add(default(double));
                size--;
            }

            foreach (Sample sample1 in x)
            {
                foreach (Sample sample2 in y)
                {
                    int sampleDelay = (int)(sample2.Tick - sample1.Tick + maxBin);
                    if (sampleDelay < 0 || sampleDelay > 2 * maxBin)
                        continue;
                    crossCorrelation[sampleDelay] += sample1.E * sample2.E;
                }
            }

            double maxCorrelation = double.MinValue;
            for (int i = 0; i < 2 * maxBin; i++)
            {
                if (Math.Abs(crossCorrelation[i]) > 0 && maxCorrelation < crossCorrelation[i])
                {
                    maxCorrelation = crossCorrelation[i];
                    delay = i - maxBin;
                }
            }

            crossCorrelatePool.Return(crossCorrelation);

            return delay;
        }

        private static int Sum(List<short> intArrayList)
        {
            int nSum = 0;
            foreach (short nInt in intArrayList)
                nSum += nInt;
            return nSum;
        }
    }
}