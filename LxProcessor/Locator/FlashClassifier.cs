using System;
using System.Collections.Generic;

using LxCommon;
using LxCommon.DataStructures;
using LxCommon.Models;

using LxProcessor.Common;
using LxProcessor.WaveformData;

namespace LxProcessor.Locator
{
    public class FlashClassifier
    {
        

        private static readonly int MaxWaveformLengthMicroseconds = Config.ClassifierMaxWaveformLengthMicroseconds;
        private static readonly int MaxWaveformTimeBeforeToaMicroseconds = Config.ClassifierMaxWaveformTimeBeforeToaMicroseconds;
        private static readonly int MaxSensorForClassification = Config.ClassifierMaxSensorForClassification;
        private static readonly int MaxDistanceBeforeIonosphereReflectionMicroseconds = Config.ClassifierMaxDistanceBeforeIonosphereReflectionMicroseconds;
        private static readonly int DistanceReversedCgPolarityMicroseconds = Config.ClassifierDistanceReversedCgPolarityMicroseconds;
        private static readonly int MinFirstReturnStrokePeakCurrent = Config.ClassifierMinFirstReturnStrokePeakCurrent;
        private static readonly int CgPercentage = Config.ClassifierCgPercentage;

        private static readonly long MinimumReturnStrokeTimeDeltaNanoseconds = Config.MinimumReturnStrokeTimeDeltaNanoseconds;
        private static readonly long MinimumFlashGroupTimeInNanoseconds = (long)(Config.MinimumFlashGroupTimeInSecond * DateTimeUtcNano.NanosecondsPerSecond);

        private static readonly double MinimumFlashGroupDistanceInKm = Config.MinimumFlashGroupDistanceInKm;

        private ConcurrentSortedSet<LTGFlashPortion> _returnStrokes;

        public FlashClassifier()
        {
            _returnStrokes = new ConcurrentSortedSet<LTGFlashPortion>();
        }
     

        public void ClassifyFlashPortion(LTGFlashPortion flashPortion)
        {
            // Use the first 3 non-saturated sensors
            List<Offset> classificationOffsets = GetClassificationOffsets(flashPortion);
            if (classificationOffsets.Count == 0)
                return;

            int totalCg = 0, totalIc = 0;
            int peakcurrentSign = 0;

            int nearbySensorNum = 0;
            int nearbySensorCg = 0;
            int nearbySensorPeakcurrentSign = 0;

            bool firstReturnStrokeExists = IsThereFirstReturnStroke(flashPortion, _returnStrokes);

            foreach (Offset offset in classificationOffsets)
            {
                PortionAttributes portionAttr = new PortionAttributes()
                {
                    SensorId = offset.StationId,
                    DistanceToSensorMicroseconds = offset.DistanceToFlash,
                    FirstReturnStrokeExists = firstReturnStrokeExists
                };


                long toa = flashPortion.TimeStamp.Nanoseconds + (long)(offset.DistanceToFlash * DateTimeUtcNano.NanosecondsPerMicrosecond);
                
                portionAttr.Toa = new DateTimeUtcNano(toa);
                portionAttr.PeakCurrent = (int)flashPortion.Amplitude;
                GetWaveformStats(((ShortPortion)offset.OriginalShortPortion).Waveforms, portionAttr);

                FlashType type = GetFlashTypeFromStat(portionAttr);
                peakcurrentSign += Math.Sign(portionAttr.PeakCurrent);

                if (type == FlashType.FlashTypeCG)
                    totalCg++;
                else if (type == FlashType.FlashTypeIC)
                    totalIc++;

                if (portionAttr.DistanceToSensorMicroseconds < MaxDistanceBeforeIonosphereReflectionMicroseconds)
                {
                    nearbySensorNum++;
                    if (type == FlashType.FlashTypeCG)
                    {
                        nearbySensorCg++;
                    }
                    nearbySensorPeakcurrentSign += Math.Sign(portionAttr.PeakCurrent);
                }

            }

            
            // If not all sensors are nearby or far away, use the nearby sensors
            if (nearbySensorNum > 0 && nearbySensorNum < MaxSensorForClassification)
            {
                flashPortion.FlashType = nearbySensorCg * 100 / nearbySensorNum > CgPercentage ? FlashType.FlashTypeCG : FlashType.FlashTypeIC;
                flashPortion.Amplitude = ((nearbySensorPeakcurrentSign > 0) ? 1 : -1) * Math.Abs(flashPortion.Amplitude);
            }
            else
            {
                // The sensors are either all nearby or all far away
                flashPortion.FlashType = totalCg * 100 / MaxSensorForClassification > CgPercentage ? FlashType.FlashTypeCG : FlashType.FlashTypeIC;
                flashPortion.Amplitude = ((peakcurrentSign > 0) ? 1 : -1) * Math.Abs(flashPortion.Amplitude);
            }

            // Check positive CG
            if (flashPortion.FlashType == FlashType.FlashTypeCG && flashPortion.Amplitude > 0)
            {
                if (flashPortion.Amplitude < LtgConstants.MinPeakCurrentForPositiveCg)
                    flashPortion.FlashType = FlashType.FlashTypeIC;
                if (classificationOffsets[0].DistanceToFlash > DistanceReversedCgPolarityMicroseconds)
                    flashPortion.Amplitude *= -1;
            }

            if (flashPortion.FlashType == FlashType.FlashTypeCG && Math.Abs(flashPortion.Amplitude) > MinFirstReturnStrokePeakCurrent)
            {
                UpdateReturnStrokes(flashPortion, _returnStrokes);
            }
                
        }

        private static List<Offset> GetClassificationOffsets(LTGFlashPortion flashPortion)
        {
            List<Offset> finalOffsets = new List<Offset>();
            if (flashPortion.OffsetList.Count <= MaxSensorForClassification)
                return flashPortion.OffsetList;

            foreach (Offset offset in flashPortion.OffsetList)
            {
                WaveformScore score = ((WaveformForOffset)offset.WaveFormforOffset).WaveformScore;
                if (Math.Abs(score.AbsPeakWaveform.Amplitude) < LtgConstants.SaturationValueForPeakcurrent)
                {
                    finalOffsets.Add(offset);
                }
                if (finalOffsets.Count >= MaxSensorForClassification)
                {
                    break;
                }
            }
            if (finalOffsets.Count < MaxSensorForClassification)
            {  // Most nearby sensors saturated, choose the last 3
                finalOffsets.Clear();
                finalOffsets.AddRange(flashPortion.ClosestSensorOffsetForClassification.GetRange(flashPortion.ClosestSensorOffsetForClassification.Count - MaxSensorForClassification, MaxSensorForClassification));
            }
            return finalOffsets;
        }

        private static FlashType GetFlashTypeFromStat(PortionAttributes portionAttr)
        {
            portionAttr.ClassifiedFlashType = FlashType.FlashTypeUnknown;

            // Check waveform shapes: Amplitude ratio and positive to negative peak time
            CheckWaveformShapes(portionAttr);

            // Check the peak current
            CheckPeakCurrent(portionAttr);

            return portionAttr.ClassifiedFlashType;
        }

        private static void CheckWaveformShapes(PortionAttributes portionAttr)
        {
            if (portionAttr.PositivePeaks + portionAttr.NegativePeaks == 1)
            {
                if (portionAttr.NegativePeaks == 1)
                {
                    int minSampleForCg = 7;
                    double minWidthForCg = 3.0;
                    if (portionAttr.FirstReturnStrokeExists)
                    {
                        minSampleForCg = 3;
                        minWidthForCg = 2.4;
                    }
                    // Nagative peak
                    if (Math.Abs(portionAttr.PeakCurrent) > MinFirstReturnStrokePeakCurrent)
                    {
                        portionAttr.ClassifiedFlashType = FlashType.FlashTypeCG;
                        portionAttr.ClassificationReason = "Only One Peak strong Negative CG: 100%";
                        portionAttr.Certainty = 100;
                    }
                    else if (portionAttr.FirstPeakWidthMicroseconds > minWidthForCg && portionAttr.FirstPeakWidthMicroseconds < 15 &&
                             portionAttr.TotalSamples >= minSampleForCg)
                    {
                        portionAttr.ClassifiedFlashType = FlashType.FlashTypeCG;
                        portionAttr.ClassificationReason = string.Format("Only One Peak width>{0}: CG", minWidthForCg);
                        portionAttr.Certainty = 100;
                    }
                    else
                    {
                        portionAttr.ClassifiedFlashType = FlashType.FlashTypeIC;
                        portionAttr.ClassificationReason = "Only One Peak: narrow<3us IC 100%";
                        portionAttr.Certainty = 100;
                    }
                }
                else
                {
                    if (portionAttr.DistanceToSensorMicroseconds < MaxDistanceBeforeIonosphereReflectionMicroseconds)
                    {
                        // Positive
                        if (Math.Abs(portionAttr.PeakCurrent) > LtgConstants.MinPeakCurrentForPositiveCg)
                        {
                            portionAttr.ClassifiedFlashType = FlashType.FlashTypeCG;
                            portionAttr.ClassificationReason = "Only One Peak strong Positive CG: 100%";
                            portionAttr.Certainty = 100;
                        }
                        else
                        {
                            portionAttr.ClassifiedFlashType = FlashType.FlashTypeIC;
                            portionAttr.ClassificationReason = "Only One Peak weak positive: IC 100%";
                            portionAttr.Certainty = 100;
                        }
                    }
                    else
                    {
                        portionAttr.ClassifiedFlashType = FlashType.FlashTypeCG;
                        portionAttr.ClassificationReason = "reversed to CG: 100%";
                        portionAttr.Certainty = 30;
                    }
                }
            }
            else if (portionAttr.PositivePeaks + portionAttr.NegativePeaks > 4)
            {
                // Check Peak gaps and ratio
                if (portionAttr.DistanceToSensorMicroseconds < MaxDistanceBeforeIonosphereReflectionMicroseconds)
                {
                    if (portionAttr.MaxPeakAmpRatio > -1.5)
                    {
                        portionAttr.ClassifiedFlashType = FlashType.FlashTypeIC;
                        portionAttr.ClassificationReason = "More Than 4 Peaks and small amp ratio 100% IC ";
                        portionAttr.Certainty = 100;
                    }
                    else if (portionAttr.NegativePeaks == 1 && portionAttr.MaxPeakAmpRatio < -2.5)
                    {
                        portionAttr.ClassifiedFlashType = FlashType.FlashTypeCG;
                        portionAttr.ClassificationReason = "More Than 4 Peaks but big ratio 100% CG ";
                        portionAttr.Certainty = 100;
                    }
                }
                else
                {
                    portionAttr.ClassifiedFlashType = FlashType.FlashTypeIC;
                    portionAttr.ClassificationReason = "More Than 4 Peaks and long disance  IC";
                    portionAttr.Certainty = 100;
                }

            }
            else
            {
                if (portionAttr.DistanceToSensorMicroseconds < MaxDistanceBeforeIonosphereReflectionMicroseconds)
                {
                    if (portionAttr.FirstPeakSign < 0)
                    {   // negative pulse
                        double minWidthForCg = 12;
                        if (portionAttr.FirstReturnStrokeExists)
                        {
                            minWidthForCg = 9;
                        }
                        if (portionAttr.FirstSecondPeakAmpRatio < -1.8 && portionAttr.MaxPeakAmpRatio < -1.8 &&
                                 portionAttr.FirstPeakToSecondPeakTimeMicrosecond > minWidthForCg && portionAttr.FirstPeakToSecondPeakTimeMicrosecond < 90)
                        {
                            portionAttr.ClassifiedFlashType = FlashType.FlashTypeCG;
                            portionAttr.ClassificationReason = string.Format("Dist<2000Us|Big-FirstSecondPeakRatioAndTime>{0} CG 100%", minWidthForCg);
                            portionAttr.Certainty = 100;
                        }
                        else if (portionAttr.FirstSecondPeakAmpRatio < -1.5 && portionAttr.MaxPeakAmpRatio < -1.5 &&
                                 portionAttr.FirstPeakToSecondPeakTimeMicrosecond > minWidthForCg + 5 &&
                                 portionAttr.FirstPeakToSecondPeakTimeMicrosecond < 90)
                        {
                            portionAttr.ClassifiedFlashType = FlashType.FlashTypeCG;
                            portionAttr.ClassificationReason = "Dist<2000Us|Big-FirstSecondPeakRatioAndTime>20 CG 90%";
                            portionAttr.Certainty = 90;
                        }
                        else if ((portionAttr.MaxPeakAmpRatio > -1.2 || portionAttr.FirstSecondPeakAmpRatio > -1.0) &&
                                 portionAttr.FirstPeakToSecondPeakTimeMicrosecond < minWidthForCg + 6)
                        {
                            portionAttr.ClassifiedFlashType = FlashType.FlashTypeIC;
                            portionAttr.ClassificationReason = "Dist<2000Us|and Time IC 100%";
                            portionAttr.Certainty = 100;
                        }
                        else if (portionAttr.NegativePeaks == 1 && portionAttr.FirstPeakToSecondPeakTimeMicrosecond < minWidthForCg)
                        {
                            if (portionAttr.FirstPeakSign < 0 && portionAttr.FirstSecondPeakAmpRatio < -2.0 &&
                                portionAttr.MaxPeakAmpRatio < -2.0 &&
                                portionAttr.TotalSamples > 50 &&
                                portionAttr.FirstPeakToSecondPeakTimeMicrosecond > minWidthForCg / 2)
                            {
                                portionAttr.ClassifiedFlashType = FlashType.FlashTypeCG;
                                portionAttr.ClassificationReason = "Narrow but Big amp ratio and long waveform";
                                portionAttr.Certainty = 100;
                            }
                            else
                            {
                                portionAttr.ClassifiedFlashType = FlashType.FlashTypeIC;
                                portionAttr.ClassificationReason = string.Format("Dist<2000Us|Too narrow<{0}", minWidthForCg);
                                portionAttr.Certainty = 100;
                            }
                        }
                        else
                        {
                            if (portionAttr.NegativePeaks == portionAttr.PositivePeaks || portionAttr.NegativePeaks + portionAttr.PositivePeaks > 3)
                            {  // More like bi polar

                                portionAttr.ClassifiedFlashType = FlashType.FlashTypeIC;
                                portionAttr.ClassificationReason = "Total peaks: " + portionAttr.NegativePeaks + portionAttr.PositivePeaks;
                                portionAttr.Certainty = 70;
                            }
                            //Unknown - check peak current and distance later
                        }
                    }
                    else
                    {  // First peak is positive, most IC have this kind of feature
                        if (portionAttr.FirstSecondPeakAmpRatio < -2.0 && portionAttr.MaxPeakAmpRatio < -2.0 &&
                            portionAttr.FirstPeakToSecondPeakTimeMicrosecond > 20 && portionAttr.FirstPeakToSecondPeakTimeMicrosecond < 90)
                        {
                            portionAttr.ClassifiedFlashType = FlashType.FlashTypeCG;
                            portionAttr.ClassificationReason = "Dist<2000Us|FirstSecondPeakRatio Positive CG 100%;";
                            portionAttr.Certainty = 100;
                        }
                        else
                        {
                            portionAttr.ClassifiedFlashType = FlashType.FlashTypeIC;
                            portionAttr.ClassificationReason = "Dist<2000Us|FirstSecondPeakRatio IC 80%%;";
                            portionAttr.Certainty = 100;
                        }
                    }

                }
                else
                {  // Long distance
                    if (portionAttr.FirstSecondPeakAmpRatio < 0 && portionAttr.FirstPeakToSecondPeakTimeMicrosecond > 40 &&
                        portionAttr.FirstPeakToSecondPeakTimeMicrosecond < 80)
                    {
                        portionAttr.ClassifiedFlashType = FlashType.FlashTypeCG;
                        portionAttr.ClassificationReason = "Dist>2000Us|FirstSecondPeakTime>40 CG 70%";
                        portionAttr.Certainty = 70;
                    }
                    else if (portionAttr.FirstPeakToSecondPeakTimeMicrosecond < 20)
                    {
                        portionAttr.ClassifiedFlashType = FlashType.FlashTypeIC;
                        portionAttr.ClassificationReason = "Dist>2000Us|FirstSecondPeakTime<20 IC 70%";
                        portionAttr.Certainty = 70;
                    }
                }
            }
        }

        private static void CheckPeakCurrent(PortionAttributes portionAttr)
        {
            if (portionAttr.PeakCurrent == 0)
            {
                // When the sensors are not calibrated, usually there is low density
                if (portionAttr.ClassifiedFlashType == FlashType.FlashTypeUnknown)
                {
                    portionAttr.ClassifiedFlashType = FlashType.FlashTypeCG;
                }
            }
            else
            {
                // Check peak current
                if (portionAttr.ClassifiedFlashType == FlashType.FlashTypeCG) // && stat.Cerntainty < 100)
                {
                    // +CG > 15kA
                    // |-CG| > 4.1kA
                    if (Math.Abs(portionAttr.PeakCurrent) < LtgConstants.MinPeakCurrentForNegativeCg)
                    {
                        portionAttr.ClassifiedFlashType = FlashType.FlashTypeIC;
                        portionAttr.ClassificationReason = "MinPeackCurrentForNegativeCG";

                    }
                    else if (portionAttr.PeakCurrent > 0)
                    {
                        if (portionAttr.PeakCurrent < LtgConstants.MinPeakCurrentForPositiveCg)
                        {
                            portionAttr.ClassifiedFlashType = FlashType.FlashTypeIC;
                            portionAttr.ClassificationReason = "MinPeackCurrentForPosiiveCG";
                        }
                        else if (portionAttr.DistanceToSensorMicroseconds > MaxDistanceBeforeIonosphereReflectionMicroseconds)
                        {
                            portionAttr.PeakCurrent *= -1;
                            portionAttr.ClassificationReason = "Reflection Changed to Negative";
                        }
                    }
                }
                else if (portionAttr.ClassifiedFlashType == FlashType.FlashTypeIC)
                {
                    // |IC| < 30kA
                    if (Math.Abs(portionAttr.PeakCurrent) > LtgConstants.MaxPeakCurrentForIc)
                    {
                        portionAttr.ClassifiedFlashType = FlashType.FlashTypeCG;
                        portionAttr.ClassificationReason = "MaxPeakCurrentForIC:" + portionAttr.PeakCurrent;
                    }
                    else if (portionAttr.PositivePeaks + portionAttr.NegativePeaks == 1)
                    {
                        if (Math.Abs(portionAttr.PeakCurrent) > LtgConstants.MinPeakCurrentForPositiveCg)
                        {
                            portionAttr.ClassifiedFlashType = FlashType.FlashTypeCG;
                            portionAttr.ClassificationReason = "Single Peak|CheckPeakCurrent";
                        }
                    }
                }
                // this is ambiguous based on the shapes of waveforms
                else if (portionAttr.ClassifiedFlashType == FlashType.FlashTypeUnknown)
                {
                    if (Math.Abs(portionAttr.PeakCurrent) > LtgConstants.MaxPeakCurrentForIc / 2 || portionAttr.DistanceToSensorMicroseconds > LtgConstants.MaxDistanceForICTravelInMicrosecond)
                    {
                        portionAttr.ClassifiedFlashType = FlashType.FlashTypeCG;
                        portionAttr.ClassificationReason = "PeakCurrent|OrDistance Unknown-> CG";
                    }
                    else
                    {
                        portionAttr.ClassifiedFlashType = FlashType.FlashTypeIC;
                        portionAttr.ClassificationReason = "PeakCurrent|OrDistance unknown-> IC";
                    }
                }
            }
        }

        private static void GetWaveformStats(IList<Waveform> waveforms, PortionAttributes portionAttr)
        {

            List<Waveform> peakList = new List<Waveform>();

            // stat.SampleNumBelow50PercentPeak = GetSamplesBelow50Percent(waveforms, stat.Toa);

            GetPeakList(waveforms, portionAttr.Toa, peakList);

            if (peakList.Count > 0)
                StatsFromPeaks(peakList, waveforms, portionAttr);

        }

        private static void StatsFromPeaks(IList<Waveform> peakList, IList<Waveform> waveforms, PortionAttributes portionAttr)
        {

            try
            {
                // Total Width
                //stat.TotalWaveformWidth = (int)(waveforms[waveforms.Count - 1].TimeStamp.Microseconds - waveforms[0].TimeStamp.Microseconds);
                portionAttr.TotalSamples = waveforms.Count;

                // Get the average peak distance
                //stat.AveragePeakDistance = GetMinPeakDistance(peakList);


                Waveform maxPositivePeak = new Waveform() { Amplitude = 0 };
                Waveform maxNegativePeak = new Waveform() { Amplitude = 0 };
                Waveform absPeakWaveform = peakList[0];

                Waveform curWaveform;
                int i = 0;

                // Find positive peak and negative peak
                while (i < peakList.Count)
                {
                    curWaveform = peakList[i++];
                    if (curWaveform.Amplitude > 0)
                    {
                        if (maxPositivePeak.Amplitude < curWaveform.Amplitude)
                        {
                            maxPositivePeak = curWaveform;
                            if (Math.Abs(absPeakWaveform.Amplitude) < maxPositivePeak.Amplitude)
                                absPeakWaveform = maxPositivePeak;
                        }
                    }
                    else
                    {
                        if (maxNegativePeak.Amplitude > curWaveform.Amplitude)
                        {
                            maxNegativePeak = curWaveform;
                            if (Math.Abs(absPeakWaveform.Amplitude) < Math.Abs(maxNegativePeak.Amplitude))
                                absPeakWaveform = maxNegativePeak;
                        }
                    }

                }

                if (maxPositivePeak.Amplitude != 0 && maxNegativePeak.Amplitude != 0)
                {
                    portionAttr.MaxPeakAmpRatio = (double)maxPositivePeak.Amplitude / maxNegativePeak.Amplitude;
                    if (Math.Abs(portionAttr.MaxPeakAmpRatio) < 1)
                        portionAttr.MaxPeakAmpRatio = 1 / portionAttr.MaxPeakAmpRatio;
                }


                // remove the weak peaks that 50% lower than the max amp
                i = 0;
                while (i < peakList.Count)
                {
                    curWaveform = peakList[i];
                    if ((curWaveform.Amplitude > 0 && curWaveform.Amplitude < maxPositivePeak.Amplitude * 0.5) ||
                        (curWaveform.Amplitude < 0 && curWaveform.Amplitude > maxNegativePeak.Amplitude * 0.5))
                    {
                        peakList.RemoveAt(i);
                    }
                    else
                    {
                        i++;
                    }
                }

                // Calculate the number of positive peak
                foreach (var peak in peakList)
                {
                    if (peak.Amplitude > 0)
                    {
                        portionAttr.PositivePeaks++;
                    }
                    else
                    {
                        portionAttr.NegativePeaks++;
                    }
                }
                if (peakList[0].Amplitude > 0)
                    portionAttr.FirstPeakSign = 1;
                else
                    portionAttr.FirstPeakSign = -1;

                // Calculate the ratio of the first peak amp vs second peak amp
                // calcuate teh time from the first peak to the second peak
                if (peakList.Count > 1)
                {
                    portionAttr.FirstSecondPeakAmpRatio = (double)peakList[0].Amplitude / (double)peakList[1].Amplitude;
                    portionAttr.FirstPeakToSecondPeakTimeMicrosecond =
                        (int)(peakList[1].TimeStamp.Microseconds - peakList[0].TimeStamp.Microseconds);
                }

                // Calcualte the rise time and decay time of the first peak
                // Calculate the zero crossing weiths of the first and second peak
                i = 0;
                if (peakList.Count == 1)
                {
                    portionAttr.RiseTimeMicroseconds = GetRiseTime(waveforms, peakList[0]);
                    portionAttr.DecayTimeMicroseconds = GetDecayTime(waveforms, peakList[0]);
                    portionAttr.FirstPeakWidthMicroseconds = portionAttr.RiseTimeMicroseconds + portionAttr.DecayTimeMicroseconds;
                }
                //if (peakList.Count > 1)
                //    stat.SecondPeakWidthInUs = GetZeroCrossingPeakWidth(waveforms, peakList[1]);

            }
            catch (Exception ex)

            { }
        }

        // from 50% peak to the peak time
        private static double GetRiseTime(IList<Waveform> waveforms, Waveform peak)
        {
            // Find the two sample below and above the 50% of peak
            int i = 0;
            int halfPeak = peak.Amplitude / 2;
            while (i < waveforms.Count)
            {
                var waveform = waveforms[i];
                if (waveform == peak)
                    break;
                i++;
            }

            if (i == 0 || i >= waveforms.Count)
                return 0;

            int j = i;
            Waveform above, below;
            while (j > 0)
            {
                above = waveforms[j];
                below = waveforms[j - 1];
                if (Math.Abs(below.Amplitude) <= Math.Abs(halfPeak) &&
                    Math.Abs(above.Amplitude) >= Math.Abs(halfPeak))
                {
                    double t = (above.TimeStamp.Nanoseconds - below.TimeStamp.Nanoseconds) * (halfPeak - below.Amplitude) /
                            (above.Amplitude - below.Amplitude) + below.TimeStamp.Nanoseconds;
                    return (peak.TimeStamp.Nanoseconds - t) / 1000;
                }
                j--;
            }

            // Can't find the crossing, try projection, there is only one peak when this function is called
            j = 1;
            below = waveforms[0];
            above = waveforms[1];
            while (j <= i)
            {
                above = waveforms[j];
                if (Math.Abs(above.Amplitude - below.Amplitude) > 10)
                {
                    break;
                }
                j++;
            }
            if (j <= i)
            {
                double t = (above.TimeStamp.Nanoseconds - below.TimeStamp.Nanoseconds) * (halfPeak - below.Amplitude) /
                  (above.Amplitude - below.Amplitude) + below.TimeStamp.Nanoseconds;
                return (peak.TimeStamp.Nanoseconds - t) / 1000;
            }
            return 0;
        }

        private static double GetDecayTime(IList<Waveform> waveforms, Waveform peak)
        {
            // Find the two sample below and above the 50% of peak
            int i = 0;
            int halfPeak = peak.Amplitude / 2;
            while (i < waveforms.Count)
            {
                var waveform = waveforms[i];
                if (waveform == peak)
                    break;
                i++;
            }

            if (i == 0 || i >= waveforms.Count)
                return 0;

            int j = i;
            Waveform above, below;
            while (j < waveforms.Count - 1)
            {
                above = waveforms[j];
                below = waveforms[j + 1];
                if (Math.Abs(below.Amplitude) <= Math.Abs(halfPeak) &&
                    Math.Abs(above.Amplitude) >= Math.Abs(halfPeak))
                {
                    double t = (above.TimeStamp.Nanoseconds - below.TimeStamp.Nanoseconds) * (halfPeak - below.Amplitude) /
                            (above.Amplitude - below.Amplitude) + below.TimeStamp.Nanoseconds;
                    return (t - peak.TimeStamp.Nanoseconds) / 1000;
                }
                j++;
            }

            // Can't find the crossing, try projection, there is only one peak when this function is called
            j = waveforms.Count - 2;
            below = waveforms[waveforms.Count - 1];
            above = waveforms[waveforms.Count - 2];
            while (j > i)
            {
                above = waveforms[j];
                if (Math.Abs(above.Amplitude - below.Amplitude) > 10)
                {
                    break;
                }
                j--;
            }
            if (j >= i)
            {
                double t = (above.TimeStamp.Nanoseconds - below.TimeStamp.Nanoseconds) * (halfPeak - below.Amplitude) /
                  (above.Amplitude - below.Amplitude) + below.TimeStamp.Nanoseconds;
                return (t - peak.TimeStamp.Nanoseconds) / 1000;
            }

            return 0;
        }

        private static void GetPeakList(IList<Waveform> waveforms, DateTimeUtcNano toa, IList<Waveform> peakList)
        {
            try
            {
                int i = 0;


                while (i < waveforms.Count)
                {
                    if (toa.Microseconds - waveforms[i].TimeStamp.Microseconds < MaxWaveformTimeBeforeToaMicroseconds)
                        break;
                    i++;
                }

                if (waveforms.Count < i + 3)
                    return;

                Waveform preWaveform = waveforms[i++];
                Waveform curWaveform = waveforms[i++];
                Waveform nextWaveform;
                while (i < waveforms.Count)
                {
                    nextWaveform = waveforms[i++];
                    if (nextWaveform.TimeStamp.Microseconds - toa.Microseconds > MaxWaveformLengthMicroseconds)
                        break;

                    if ((curWaveform.Amplitude > 0 && curWaveform.Amplitude > preWaveform.Amplitude && curWaveform.Amplitude > nextWaveform.Amplitude) ||
                        (curWaveform.Amplitude < 0 && curWaveform.Amplitude < preWaveform.Amplitude && curWaveform.Amplitude < nextWaveform.Amplitude))
                    {
                        peakList.Add(curWaveform);
                    }
                    preWaveform = curWaveform;
                    curWaveform = nextWaveform;
                    if (i == waveforms.Count)
                    {  // Last sample
                        if ((curWaveform.Amplitude > 0 && curWaveform.Amplitude > preWaveform.Amplitude) || (curWaveform.Amplitude < 0 && curWaveform.Amplitude < preWaveform.Amplitude))
                        {
                            peakList.Add(curWaveform);

                        }
                    }
                }

                // Remove the HF noises
                i = 0;
                while (i < peakList.Count - 1)
                {
                    curWaveform = peakList[i];
                    nextWaveform = peakList[i + 1];
                    if (Math.Sign(curWaveform.Amplitude) == Math.Sign(nextWaveform.Amplitude))
                    {
                        if (nextWaveform.TimeStamp.Microseconds - curWaveform.TimeStamp.Microseconds <= 50)
                        {
                            if (Math.Abs(curWaveform.Amplitude) < Math.Abs(nextWaveform.Amplitude))
                            {
                                peakList.RemoveAt(i);
                            }
                            else
                            {
                                peakList.RemoveAt(i + 1);
                            }
                        }
                        else
                            i++;
                    }
                    else
                        i++;
                }
            }

            catch (Exception ex)
            {

            }
        }

        private const long NanosecondsToKeep = 2 * DateTimeUtcNano.NanosecondsPerSecond;

        private void UpdateReturnStrokes(LTGFlashPortion flashPortion, ConcurrentSortedSet<LTGFlashPortion> returnStrokes)
        {
            DateTimeUtcNano lower = DateTimeUtcNano.MinValue;
            LTGFlashPortion lowerPortion = new LTGFlashPortion { TimeStamp = lower };
            DateTimeUtcNano upper = new DateTimeUtcNano(flashPortion.TimeStamp.Nanoseconds - NanosecondsToKeep);
            LTGFlashPortion upperPortion = new LTGFlashPortion {TimeStamp = upper};

            returnStrokes.RemoveBetween(lowerPortion, upperPortion);
            
            returnStrokes.TryAdd(flashPortion);
        }

        // Check whether there is a return stroke within 500ms and within same flash distance
        private bool IsThereFirstReturnStroke(LTGFlashPortion flashPortion, ConcurrentSortedSet<LTGFlashPortion> returnStrokes)
        {
            bool exists = false;
            DateTimeUtcNano lower = new DateTimeUtcNano(flashPortion.TimeStamp.Nanoseconds + MinimumReturnStrokeTimeDeltaNanoseconds);
            LTGFlashPortion lowerPortion = new LTGFlashPortion {TimeStamp = lower};
            DateTimeUtcNano upper = new DateTimeUtcNano(flashPortion.TimeStamp.Nanoseconds + MinimumFlashGroupTimeInNanoseconds);
            LTGFlashPortion upperPortion = new LTGFlashPortion {TimeStamp = upper};
            SortedSet<LTGFlashPortion> canidates = returnStrokes.GetBetween(lowerPortion, upperPortion);

            if (canidates != null)
            {
                foreach(LTGFlashPortion portion in canidates)
                {
                    if (StationData.DistanceInKM(flashPortion.Latitude, flashPortion.Longitude, 0, portion.Latitude,
                          portion.Longitude, 0) < MinimumFlashGroupDistanceInKm)
                    {
                        exists = true;
                        break;
                    }
                }
            }

            return exists;
        }

        private class PortionAttributes
        {
            public PortionAttributes()
            {
                PositivePeaks = 0;
                NegativePeaks = 0;
            }
            public DateTimeUtcNano Toa { set; get; }
            public FlashType ClassifiedFlashType { get; set; }
            public string ClassificationReason { get; set; }
            public double DistanceToSensorMicroseconds { get; set; }
            public double RiseTimeMicroseconds { set; get; }
            public double DecayTimeMicroseconds { set; get; }
            public double FirstPeakWidthMicroseconds { get; set; }
            public int TotalSamples { get; set; }
            public int PositivePeaks { get; set; }
            public int NegativePeaks { get; set; }
            public short FirstPeakSign { get; set; }  // 1 or -1
            public double MaxPeakAmpRatio { get; set; }
            public double FirstSecondPeakAmpRatio { get; set; }
            public int PeakCurrent { get; set; }
            public int Certainty { get; set; }
            public bool FirstReturnStrokeExists { get; set; }
            public int FirstPeakToSecondPeakTimeMicrosecond { get; set; }
            public string SensorId { get; set; }
        }

    }
}

