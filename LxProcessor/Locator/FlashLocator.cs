using Aws.Core.Utilities;
using LxCommon;
using LxCommon.DataStructures;
using LxCommon.Models;
using LxProcessor.Common;
using LxProcessor.Optimizer;
using LxProcessor.WaveformData;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading;

namespace LxProcessor.Locator
{
    public class FlashLocator
    {


        private static readonly int MaxTimeInLocatorMillisecond = Config.MaxTimeInLocatorMillisecond;
        private static readonly int MaxOffsetQueueCount = Config.MaxOffsetQueueCount;
        private static readonly int MinLocatorAttempts = Config.MinLocatorAttempts;
        private static readonly int MinimumSensorNumberInLocator = Config.MinimumSensorNumberInLocator;
        private static readonly int AngleFactor = Config.AngleIncreaseRatioOnSensorDensity;
        private static readonly int MinDistanceFactorOfTwoSensors = Config.MinDistanceOfTwoSensorsIncreaseFactorOnSensorDensity;
        private static readonly double MinimumSensorDensityForCheckingClosestSensor = Config.MinimumSensorDensityForClosestSensorCheck;

        private readonly static ConcurrentSimplePool<double[]> _fVecLm3Pool;
        private readonly static ConcurrentSimplePool<FluxList<double>> _doubleListPool;
        private readonly static ConcurrentSimplePool<Matrix<double>> _matrixPool;
        private readonly static ConcurrentSimplePool<JaggedList<double>> _jaggedPool;

        private static readonly ObservationsComparer _observationsComparer;

        static FlashLocator()
        {
            _fVecLm3Pool = new ConcurrentSimplePool<double[]>(Config.FVecLm3PoolSize,
                () => { return new double[4]; },
                (list) =>
                {
                    for (int i = 0; i < list.Length; i++)
                    {
                        list[i] = default(double);
                    }
                },
                "_fVecLm3Pool");

            _doubleListPool = new ConcurrentSimplePool<FluxList<double>>(Config.FlashLocatorDoubleListPoolSize,
                () => { return new FluxList<double>(); },
                (list) => { list.Clear(); },
                "_doubleListPool");

            _matrixPool = new ConcurrentSimplePool<Matrix<double>>(Config.FlashLocatorMatrixPoolSize,
                () => { return new Matrix<double>(); },
                (matrix) => { matrix.Clear(); },
                "_matrixPool");

            _jaggedPool = new ConcurrentSimplePool<JaggedList<double>>(Config.FlashLocatorJaggedPoolSize,
                () => { return new JaggedList<double>(); },
               (jagged) => { jagged.Clear(); },
               "_jaggedPool");

            _observationsComparer = new ObservationsComparer();
        }

        //find the flash portions from the list of data.
        // Find the flash portions using the Multilateration, improve efficiency
        public static List<LTGFlashPortion> FindFlashPortionFromOffsets(Offsets offsets, ConcurrentDictionary<string, StationData> stationData, int offsetQueueCount)
        {
            //intialize the offset array and check and sort the stations. The
            // stations are checked with the stationdata table
            Offset[] offsetArray = CheckAndSortStations(offsets, stationData);
            //intilaize the goodflashportions as null for passing and returning it to goodflashes
            List<LTGFlashPortion> goodFlashPortionList = null;
            //check if the offset array length is gretaer than the minimum sensors required in a
            // location
            if (offsetArray.Length >= MinimumSensorNumberInLocator)
            {
                //get the offset locations x,y,z,t coordinates into the offset array
                offsetArray = SetOffsetLocations(offsetArray, stationData);

                List<LTGFlashPortion> candidateFlashPortionList;
                List<List<LTGFlashPortion>> flashClusterList;

                int tries;
                //Stopwatch sw = new Stopwatch();
                //sw.Start();
                //get the goodflashportionlist and if its null get it rejected.
                goodFlashPortionList = FindPortions(offsets, stationData, offsetQueueCount, offsetArray, out candidateFlashPortionList, out flashClusterList, out tries);

                //EventManager.LogInfo(999, "FindPortions Milliseconds: " + sw.ElapsedMilliseconds + " Tries: " + tries + " Offset length: " + offsetArray.Length);
                // the rejected flash portion. if goodflashes is been null.
                if (goodFlashPortionList == null || goodFlashPortionList.Count == 0)
                {
                    // No data then please reject the flashes.
                    if (candidateFlashPortionList.Count == 0 || flashClusterList.Count == 0)
                        return null;

                    // Find the good locations from the clusters
                    //ArrayList flashPortionList = GetFlashPortionsFromClusters(flashClusterList);
                    goodFlashPortionList = GetFlashPortionsFromClusters(flashClusterList);

                    //ArrayList goodFlashPortionList = CheckFlashPortionList(flashPortionList);
                    if ((goodFlashPortionList == null || goodFlashPortionList.Count == 0) &&
                        candidateFlashPortionList.Count > 0)
                    {
                        if (EventManager.IsTypeDebugEnabled)
                        {
                            Utilities.Export.ExportFlashPortions(candidateFlashPortionList, "Not clustered and rejected FlashPortions");
                        }
                    }
                }
            }

            return goodFlashPortionList;
        }

        private static List<LTGFlashPortion> FindPortions(Offsets offsets, ConcurrentDictionary<string, StationData> stationData, int offsetQueueCount,
            Offset[] offsetArray, out List<LTGFlashPortion> candidateFlashPortionList, out List<List<LTGFlashPortion>> flashClusterList, out int nTries)
        {
            double[,] observations;

            nTries = 0; // iteration number of mulilateration

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            TimeSpan span;

            int groupingSensorSkipNum = 1;
            int groupingStartingLocation = 0;

            candidateFlashPortionList = new List<LTGFlashPortion>();
            flashClusterList = new List<List<LTGFlashPortion>>();
            // Each item in the list is a list of flashes that can be grouped together
            List<LTGFlashPortion> goodFlashPortionList = null;
            double[] refinedSolution;
            bool remainingSensorTried = false;
            while (true) //infinite loop being given
            {
                //code to form the quartet for the offsets.
                Offset[] quartet = GetQuartetOfSensors(offsetArray, ref groupingSensorSkipNum, ref groupingStartingLocation);
                if (quartet != null)
                {
                    //need to find what is the solution referred being here
                    double[][] solutions = Locate(quartet);

                    //code to check if solutions is not null and first entry not zero
                    if (solutions != null && solutions.GetLength(0) != 0)
                    {
                        //if the first entry is only present in the list then the first is got
                        double[] solution;
                        if (solutions.GetLength(0) == 1)
                            solution = solutions[0];
                        else
                            //if nore solutions is there , then the best height of the solutions is being checked.
                            //height of the stations is been checked and the station with best height is
                            // been taken into considerations
                            solution = ChooseSolutionWithBestHeight(solutions);

                        // Check the offsets that meet the Residual errors
                        //pass the solutions,offset and the maximum residula errors to the function and
                        //find the goodoffsetarray.
                        Offset[] goodOffsetArray = CheckAbsResidualsWithinErrorRange(solution, offsetArray,
                            LtgConstants.MaxResidualErrorsInLocatorPrelocation);

                        // Use the good offset array otherwise use everything
                        bool exactSolutionGood = false;
                        //if the goodoffsetarray is not null and its greater than the minimum sensors
                        if (goodOffsetArray != null && goodOffsetArray.Length >= MinimumSensorNumberInLocator)
                        {
                            // after the locations of the stations is been extracted from the tabel then the
                            // flag of the exactsolution good is been converted to True
                            observations = GetObsLocation(goodOffsetArray, stationData);
                            exactSolutionGood = true;
                        }
                        else
                        {
                            //if there is no goodoffset array then, calculate the number from the
                            //groupingstartinglocation and add 1
                            int num = offsetArray.Length - groupingStartingLocation + 1;
                            //if the number is greater than the minimum sensor in the locator
                            if (num >= MinimumSensorNumberInLocator)
                            {
                                //create the goodoffset null array using the num value
                                goodOffsetArray = new Offset[num];
                                //Copies a range of elements in one Array to another Array and performs type casting and boxing as required.
                                Array.Copy(offsetArray, groupingStartingLocation - 1, goodOffsetArray, 0, num);
                                observations = GetObsLocation(goodOffsetArray, stationData);
                            }
                            else
                            {
                                break;
                            }
                        }
                        //refine solutions. Which is very important.
                        // Applies the Non-linear least square optimization to find the
                        // good best fit
                        refinedSolution = RefineSolutionUsingLm3(observations, solution);
                        //if the refined solution is not null
                        if (refinedSolution != null)
                        {
                            //pass the refined solution to the residula error check
                            Offset[] refinedGoodOffsetArray = CheckAbsResidualsWithinErrorRange(refinedSolution,
                                offsetArray, LtgConstants.MaxResidualErrorsInLocator);
                            //if its null
                            if (refinedGoodOffsetArray == null ||
                                refinedGoodOffsetArray.Length < MinimumSensorNumberInLocator)
                            {
                                //if the refined is not good, then take the solutions itself for the
                                //good offset array
                                if (exactSolutionGood)
                                {
                                    refinedGoodOffsetArray = goodOffsetArray;
                                    refinedSolution = solution;
                                }
                                else
                                    continue;
                            }
                            //make the solutions value same as the refined solutions value
                            solution = refinedSolution;
                            //make the offset array into a flash data with portions in it.
                            //this is the classification and creation of flash data and adding the
                            // pulses in the flash data
                            LTGFlashPortion flashPortion = GetFlashPortionFromSolution(solution, refinedGoodOffsetArray,
                                stationData);
                            if (flashPortion != null)
                            {
                                // make the common portion from the offset array and clone it to the flash portion
                                flashPortion.CommonPortionRef = (CommonPortion)offsets.CommonPortion.Clone();
                                // get the flashportion confidence. pass the flash portion, station data and also the
                                // false value so that to indicate this a preprocess
                                FlashConfidence.GetFlashPortionConfidence(flashPortion, stationData, false);

                                if (flashPortion.ConfidenceData.BiggestAngle >= LtgConstants.MinSensorAroundFlashAngle)
                                {
                                    if (refinedGoodOffsetArray.Length == offsetArray.Length
                                        // || refinedGoodOffsetArray.Length >= LtgConstants.MaxSensorNumberOfValidDetections
                                        )
                                    {
                                        goodFlashPortionList = new List<LTGFlashPortion> { flashPortion };
                                        return goodFlashPortionList;
                                    }
                                    //
                                    if (flashPortion.Confidence > LtgConstants.MinNearestSensorConfidenceLevelForPreLocator &&
                                        flashPortion.ConfidenceData.BiggestAngle > LtgConstants.MinSensorAroundFlashAngle)
                                    {
                                        AddFlashPortionToCadidatePortionList(candidateFlashPortionList, flashPortion);
                                        AddFlashPortionToCluster(flashPortion, flashClusterList);
                                    }

                                    if (candidateFlashPortionList.Count >= 6)
                                        break; // this is enough , just to save time
                                    //check to see if there is a minimum of 3 portion in the flash
                                    if (EnoughFlashPortionCluser(flashClusterList))
                                        break;

                                    // The code below is used to check the number of attempts and if it is more just brek
                                    // out from the loop
                                    // This is a time-consuming process, if the total time spending here exceeds 500ms, it should quit
                                    if (nTries++ >= MinLocatorAttempts)
                                    {
                                        span = stopwatch.Elapsed;

                                        if (offsetQueueCount > MaxOffsetQueueCount)
                                        {
                                            EventManager.LogWarning(EventId.Locator.FlashLocator.ProcPreprocessTimeOut,
                                                String.Format(
                                                    "Preprocessing offsetQueue length exceeded, Tries: {0} Portions found: {1} Milliseconds: {2} Time: {3}",
                                                    nTries, candidateFlashPortionList.Count,
                                                    span.TotalMilliseconds,
                                                    offsets.StartTime));
                                            break;
                                        }

                                        if (span.TotalMilliseconds > MaxTimeInLocatorMillisecond)
                                        {
                                            EventManager.LogWarning(EventId.Locator.FlashLocator.ProcPreprocessTimeOut,
                                                String.Format(
                                                    "Preprocessing timeout, Tries: {0} Portions found: {1} Milliseconds: {2} Time: {3}",
                                                    nTries, candidateFlashPortionList.Count,
                                                    span.TotalMilliseconds,
                                                    offsets.StartTime));
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    break;
                }
            }

            stopwatch.Stop();

            return goodFlashPortionList;
        }

        private static double[] ChooseSolutionWithBestHeight(double[][] solutions)
        {
            //we got two solutions with 4 matrices containing the location data. I think this is from the
            // local quartet function which calculates the statiosn which are near to each other.
            double[] theSolution;
            // results1 and results2 are extrracted by passing to the function LatLonHeightFromXYZ height.
            // used to convert the xyz to lat,lon,height
            double[] result1 = StationData.LatLonHeightFromXyz(solutions[0]);
            double[] result2 = StationData.LatLonHeightFromXyz(solutions[1]);
            //checked for the height of the two results
            if (result1[2] >= 0 && result2[2] >= 0)
            {
                //chossing the largest height of the station
                if (result1[2] > result2[2])
                {
                    theSolution = solutions[1];
                }
                else
                {
                    theSolution = solutions[0];
                }
            }
            //if both the stations heights are in negative values then,
            //take the largest stations among them. which is the closer to zero
            else if (result1[2] <= 0 && result2[2] <= 0)
            {
                if (result1[2] < result2[2])
                {
                    theSolution = solutions[1];
                }
                else
                {
                    theSolution = solutions[0];
                }
            }
            //check whether any of the stations height is largest and take the value from it.
            else if (result1[2] >= 0)
            {
                theSolution = solutions[0];
            }
            else
            {
                theSolution = solutions[1];
            }
            return theSolution;
        }

        // Check whether there are enough data in the first cluster
        private static bool EnoughFlashPortionCluser(List<List<LTGFlashPortion>> flashClusterList)
        {
            //flashClusterList.Sort(ListCountComparison);
            return flashClusterList.Count >= 3;
        }

        // Get all the flashportion list
        private static List<LTGFlashPortion> GetFlashPortionsFromClusters(List<List<LTGFlashPortion>> flashClusterList)
        {
            if (flashClusterList.Count == 0)
                return null;

            List<LTGFlashPortion> flashPortionList = new List<LTGFlashPortion>();
            int i = 0;
            foreach (List<LTGFlashPortion> cluster in flashClusterList)
            {
                LTGFlashPortion flashPortion = GetFlashPortionFromACluster(cluster);
                if (flashPortion == null)
                    break;
                flashPortionList.Add(flashPortion);
                if (++i >= 3)  // Choose the first 3
                    break;
            }
            return flashPortionList;
        }

        // Functions to minimize
        //  for every sensor
        //  fi= (x-xi)^2+(y-yi)^2+(z-zi)^2 - (t-ti)^2;
        private static void FVecLm3(IList<double> x, IList<double> fi, object obj)
        {
            double[,] observations = (double[,])obj;
            int m = observations.GetLength(0);

            double[] d = _fVecLm3Pool.Take();

            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    d[j] = x[j] - observations[i, j];
                }

                fi[i] = d[0] * d[0] + d[1] * d[1] + d[2] * d[2] - d[3] * d[3];
            }

            _fVecLm3Pool.Return(d);
        }

        //  for every sensor:
        //  d/dx = 2(x-x1)
        //  d/dy = 2(y-y1)
        //  d/dz = 2(z-z1)
        //  d/dt = -2(t-t1)
        private static void JacLm3(IList<double> x, IList<double> fi, Matrix<double> jac, object obj)
        {
            // Functions
            FVecLm3(x, fi, obj);

            // Jacobins
            double[,] observations = (double[,])obj;
            int m = observations.GetLength(0);

            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    jac[i, j] = 2 * (x[j] - observations[i, j]);
                }
                jac[i, 3] *= -1;
            }
        }

        private static readonly int MaxIterations = Config.RefineSolutionMaxIterations;
        private const double Epsg = 0.00000001;
        private const double Epsf = 0;
        private const double Epsx = 0;

        // Find the flash portion using the Multilateration and LM 3.2 lib --- finding the pulses using multilateration
        private static double[] RefineSolutionUsingLm3(double[,] observations, double[] solutions)
        {
            //here Fveclm3 and JacLM3 are not having any data. Ep* series varibles are already declared and their values
            // are been passed here.
            //Tuple<int, int> threadpool1 = InitThreadPool();
            //ThreadPool.SetMinThreads(threadpool1.Item1, threadpool1.Item2);

            //solutions = ThreadPool.QueueUserWorkItem(LmOptimizerSvc.RefineSolution(observations, solutions, FVecLm3, JacLm3, Epsg, Epsf, Epsx, 1000));
            solutions = LmOptimizerSvc.RefineSolution(observations, solutions, FVecLm3, JacLm3, Epsg, Epsf, Epsx, 1000);
            return solutions;
        }

        private static LTGFlashPortion GetFlashPortionFromSolution(double[] refinedSolution, Offset[] offsetArray, ConcurrentDictionary<string, StationData> stationTable)
        {
            LTGFlashPortion flashData = null;
            // result[0]: Lat, result[1]: Lon, result[2]: Height
            //take the refined solution and get the lat, lon and height by comparing from the station table
            double[] result = StationData.LatLonHeightFromXyz(refinedSolution);
            //get the flashtime from result and the offset array
            long flashTime = GetFlashTime(result[0], result[1], offsetArray, stationTable);

            if (flashTime != long.MinValue)
            {
                // make a main flash data from the refined solution. get the timestamp
                // from the flashtime, lat, lon from the refined solution conversion to lat,lon, height
                // function, amplitude make it to zero
                //declare a empty offset array
                flashData = new LTGFlashPortion()
                {
                    TimeStamp = new DateTimeUtcNano(flashTime),
                    Latitude = result[0],
                    Longitude = result[1],
                    Height = result[2],
                    Amplitude = 0,     // making the amplitude as zero when the flashes are created
                    OffsetList = new List<Offset>(offsetArray)
                };
                // make the solution of the newly created flashdata to be the refined solution data
                flashData.Solution = refinedSolution;
                // oincrease the Insolutioncount element in the offset of a offset array
                foreach (Offset offset in offsetArray)
                    offset.InSolutionCount++;
            }

            return flashData;
        }

        // will get the flash time from this method
        private static long GetFlashTime(double flashLat, double flashLong, Offset[] offsetArray, ConcurrentDictionary<string, StationData> stationTable)
        {
            // Initialize the flashtime as the minimum value of the long value in c#
            long flashTime = long.MinValue;
            try
            {
                //take the first offset from the offsetarray
                Offset firstOffset = offsetArray[0];
                //get the waveformscore
                WaveformScore score = ((WaveformForOffset)firstOffset.WaveFormforOffset).WaveformScore;
                //get the station data from the first offset
                StationData stationData = stationTable[firstOffset.StationId];

                // Ignore the flash height, beacause it can be very wrong due to the algorithm erros
                //pass the result variable from the xyzt to lat,lon,height function as flashlat and flashlon. then pass the
                //station data from the first offset locations and calculate the distance in microseconds
                double deltaT = StationData.DistanceInMicrosecond(flashLat, flashLong, stationData.Altitude, stationData.Latitude, stationData.Longitude, stationData.Altitude);

                //make it into a nano second format
                long deltaNano = (long)(deltaT * 1000L);

                //Get the flash time from the score .
                flashTime = score.FirstPeakWaveform.TimeStamp.Nanoseconds - deltaNano;
            }
            catch (Exception ex)
            {
                EventManager.LogWarning(EventId.Locator.FlashLocator.GetFlashTime, "Failure getting flash time", ex);
            }

            return flashTime;
        }

        // check for stations and sort it.
        private static Offset[] CheckAndSortStations(Offsets inputOffsets, ConcurrentDictionary<string, StationData> stationDataTable)
        {
            //code to check the stations and remove them if they are not present
            Offset[] offsetArray = new Offset[0];

            int i = 0;
            while (i < inputOffsets.OffsetList.Count)
            {
                Offset offset = inputOffsets.OffsetList[i];

                if (!stationDataTable.ContainsKey(offset.StationId))
                {   // Remove the offset if the station doesn't exist in the database
                    inputOffsets.OffsetList.Remove(offset);
                    continue;
                }
                i++;
            }
            // sorting the sensors based on distance from first sensor
            // Sort the sensors based on the distance from the first sensor

            StationData firstSensor = stationDataTable[inputOffsets.OffsetList[0].StationId];
            i = 0;
            while (++i < inputOffsets.OffsetList.Count)
            {
                StationData sensor;
                if (stationDataTable.TryGetValue(inputOffsets.OffsetList[i].StationId, out sensor))
                {
                    double distance;
                    if (firstSensor.DistanceInMicrosecondTable.TryGetValue(sensor.StationID, out distance))
                    {
                        inputOffsets.OffsetList[i].DistanceToFlash = distance;
                    }
                }
            }
            inputOffsets.OffsetList.Sort(Offset.CompareOffsetByDistanceToFlash);

            offsetArray = inputOffsets.OffsetList.ToArray();
            // Array.Sort(offsetArray);

            return offsetArray;
        }

        private static Offset[] SetOffsetLocations(Offset[] offsets, ConcurrentDictionary<string, StationData> stationTable)
        {
            //check code removed because of repetion
            //if (offsets != null && offsets.Length > 0)
            //{
            //check for the station id for each of the offset and get the x,y,z,t coordinates list
            StationData station;
            foreach (Offset offset in offsets)
            {
                if (stationTable.TryGetValue(offset.StationId, out station))
                {
                    offset.X = station.Xyz[0];
                    offset.Y = station.Xyz[1];
                    offset.Z = station.Xyz[2];
                    offset.T = offset.Value;
                }
            }
            //}

            return offsets;
        }

        // Note that the collection ie the dictionary defined here is concurrent. Meaning these can be accessed
        // by the multithreds conncurrently while on execution
        // Function to compare the table and take out the offset locations to it.
        private static double[,] GetObsLocation(Offset[] offsets, ConcurrentDictionary<string, StationData> stationTable)
        {
            //the whole station table is being imported here to check.It consists of 1618 elements
            double[,] obs = null;
            try
            {
                //defined a multidimensional array
                obs = new double[offsets.Length, 4];
                //passsing the offset array and for each the staions locations is been getting
                // the ID  and the X,Y,Z,T values were passed on to it.
                for (int i = 0; i < offsets.Length; i++)
                {
                    Offset offset = offsets[i];
                    StationData stationData = stationTable[offset.StationId];
                    obs[i, 0] = offset.X = stationData.Xyz[0];
                    obs[i, 1] = offset.Y = stationData.Xyz[1];
                    obs[i, 2] = offset.Z = stationData.Xyz[2];
                    obs[i, 3] = offset.T = offset.Value;
                }
            }
            catch (Exception ex)
            {
                EventManager.LogWarning(EventId.Locator.FlashLocator.GetObsLocation, "Failure in GetObsLocation", ex);
            }

            return obs;
        }

        private static readonly double[] ZeroLine = { 0, 0, 0, 0 };

        //need to find what this function is
        private static double[][] Locate(IEnumerable<Offset> offsetQuartet)
        {
            double[][] xyzts = null;

            //double[][] observations = new double[4][];
            JaggedList<double> observations = _jaggedPool.Take();
            observations.Resize(4, 4);

            int nObs = 0;
            foreach (Offset offset in offsetQuartet)
            {
                if (nObs < 4)
                {
                    observations[nObs][0] = offset.X;
                    observations[nObs][1] = offset.Y;
                    observations[nObs][2] = offset.Z;
                    observations[nObs][3] = offset.T;
                }
                nObs++;
            }

            // first find a line the solution lies on, as a parametric function of t
            // change coordinates to put first observation at the origin
            if (nObs == 4)
            {
                observations.Sort(_observationsComparer);

                //Array.Sort(observations, new ArrayComparerOriginal(3));

                //double[][] obs = new double[observations.GetLength(0)][];
                JaggedList<double> obs = _jaggedPool.Take();
                obs.Resize(4, 4);
                int i = 0, j = 0;
                for (i = 0; i < 4; i++)
                {
                    for (j = 0; j < 4; j++)
                    {
                        obs[i][j] = observations[i][j] - observations[0][j];
                    }
                }

                // subtract first equation from all other equations to get a system of linear equations
                // coefficient matrix for that linear system of equations
                //double[,] coefficientMatrix = new double[4, 4];
                Matrix<double> coefficientMatrix = _matrixPool.Take();
                coefficientMatrix.Resize(4, 4);
                for (i = 0; i < nObs - 1; i++)
                {
                    coefficientMatrix[i, 0] = obs[i + 1][0]; // -obs[0][0];
                    coefficientMatrix[i, 1] = obs[i + 1][1]; // -obs[0][1];
                    coefficientMatrix[i, 2] = obs[i + 1][2]; // -obs[0][2];
                    coefficientMatrix[i, 3] = obs[i + 1][3]; // -obs[0][3];
                }

                // constant terms (right hand side) for that system of equations
                //double[] rhs1 = new double[3];
                FluxList<double> rhs1 = _doubleListPool.Take();
                rhs1.Resize(3);
                for (i = 0; i < 3; i++)
                {
                    rhs1[i] = MinkowskiNormSquared(obs[i + 1]) / 2;
                }

                _jaggedPool.Return(obs);
                obs = null;

                // we shall define our line by its value at t=0, and its derivative.
                // at t=0, we don't need the t terms in our coefficient matrix
                //double[,] reducedCoefficientMatrix = new double[3, 3];
                Matrix<double> reducedCoefficientMatrix = _matrixPool.Take();
                reducedCoefficientMatrix.Resize(3, 3);
                for (i = 0; i < 3; i++)
                {
                    for (j = 0; j < 3; j++)
                    {
                        reducedCoefficientMatrix[i, j] = coefficientMatrix[i, j];
                    }
                }

                //becomes the coefficient of dx/dt, the coefficient of t becomes a constant term,
                // and the constant term vanishes. Moving the constant term to the right hand side
                //double[] rhs2 = new double[3];
                FluxList<double> rhs2 = _doubleListPool.Take();
                rhs2.Resize(3);
                for (i = 0; i < 3; i++)
                {
                    rhs2[i] = coefficientMatrix[i, 3];
                }

                _matrixPool.Return(coefficientMatrix);
                coefficientMatrix = null;

                // solve
                // (x,y,z)

                //double[] lineOrigin = Matrix.SolveLinearEquations(reducedCoefficientMatrix.ToArray(), rhs1.ToArray());
                FluxList<double> lineOrigin = Matrix<double>.SolveLinearEquations(reducedCoefficientMatrix, rhs1, _matrixPool, _doubleListPool);
                //(dx/dt, dy/dt, dz/dt)
                //double[] lineDerivative = Matrix.SolveLinearEquations(reducedCoefficientMatrix.ToArray(), rhs2.ToArray());
                FluxList<double> lineDerivative = Matrix<double>.SolveLinearEquations(reducedCoefficientMatrix, rhs2, _matrixPool, _doubleListPool);

                _matrixPool.Return(reducedCoefficientMatrix);
                reducedCoefficientMatrix = null;
                _doubleListPool.Return(rhs1);
                rhs1 = null;
                _doubleListPool.Return(rhs2);
                rhs2 = null;

                if (lineOrigin != null && lineDerivative != null)
                {
                    // have line, now intersect with cone
                    FluxList<double> solutions = IntersectionsOfLineAndCone(lineOrigin, lineDerivative, ZeroLine, _doubleListPool);

                    i = 0;
                    while (i < solutions.Count)
                    {
                        double solution = solutions[i];
                        if (solution > 0)
                        {
                            solutions.RemoveAt(0);
                        }
                        else
                        {
                            i++;
                        }
                    }

                    if (solutions.Count != 0)
                    {
                        xyzts = new double[solutions.Count][];
                        i = 0;
                        foreach (double t in solutions)
                        {
                            xyzts[i] = new double[4];
                            xyzts[i][0] = (lineOrigin[0] + lineDerivative[0] * t) + observations[0][0];
                            xyzts[i][1] = (lineOrigin[1] + lineDerivative[1] * t) + observations[0][1];
                            xyzts[i][2] = (lineOrigin[2] + lineDerivative[2] * t) + observations[0][2];
                            xyzts[i][3] = t + observations[0][3];
                            i++;
                        }
                    }

                    _doubleListPool.Return(solutions);
                }

                if (lineOrigin != null) _doubleListPool.Return(lineOrigin);
                if (lineDerivative != null) _doubleListPool.Return(lineDerivative);
            }

            _jaggedPool.Return(observations);

            return xyzts;
        }

        private static double MinkowskiNormSquared(IList<double> v)
        {
            return (v[0] * v[0] + v[1] * v[1] + v[2] * v[2] - v[3] * v[3]);
        }

        //calculate the error using the error variable obtained from the residuals error.
        private static double TimeError(double[] v)
        {
            return Math.Abs(Math.Sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]) - v[3]);
        }

        private static readonly ProcessorType ProcessorType = Config.ProcessorType;

        // Check whether enough sensors within the max error, then ouput the Offsets that meet the error check
        private static Offset[] CheckAbsResidualsWithinErrorRange(double[] solution, Offset[] offsetArray, double minResidualError)
        {
            Offset[] offsets = null;
            //checked to see whether the processor type is hf and then being processed
            if (ProcessorType != ProcessorType.ProcessorTypeLF)
                offsets = CheckAbsResidualWithinErrorRangeHf(solution, offsetArray, minResidualError);

            return offsets;
        }

        //check for the error range
        private static Offset[] CheckAbsResidualWithinErrorRangeHf(double[] solution, Offset[] offsetArray, double maxErrorAllowed)
        {
            //Offset[] outputOffsetArray = new Offset[0];
            List<Offset> outputOffsetArrayList = new List<Offset>();
            double[] error = new double[4];
            foreach (Offset offset in offsetArray)
            {
                error[0] = offset.X - solution[0];
                error[1] = offset.Y - solution[1];
                error[2] = offset.Z - solution[2];
                error[3] = offset.T - solution[3];
                offset.ResidualError = TimeError(error);
                //check to see if the error is satisfactory and less than the minimum error
                if (Math.Abs(offset.ResidualError) < maxErrorAllowed)
                {
                    //if it is then add the array list to the newly created output array list
                    outputOffsetArrayList.Add(offset);
                }
            }
            //check to see if the output array list count is greater than the minimum
            //sensors in the locator
            if (outputOffsetArrayList.Count >= MinimumSensorNumberInLocator)
            {
                //outputOffsetArray = (Offset[])outputOffsetArrayList.ToArray(typeof(Offset));
                //create another variable and clone it like the previous.
                Offset[] outputOffsetArray = new Offset[outputOffsetArrayList.Count];
                for (int i = 0; i < outputOffsetArrayList.Count; i++)
                {
                    outputOffsetArrayList[i].InSolutionCount++; //this is being incremented. this is a
                    //variable present in the offsetarraylist
                    outputOffsetArray[i] = (Offset)((Offset)outputOffsetArrayList[i]).Clone();
                }
                return outputOffsetArray;
            }
            return null;
        }

        private static FluxList<double> IntersectionsOfLineAndCone(FluxList<double> g, FluxList<double> h, double[] spaceTimePoint, ConcurrentSimplePool<FluxList<double>> listPool)
        {
            FluxList<double> solutions = listPool.Take();

            double s0 = g[0] - spaceTimePoint[0];
            double s1 = g[1] - spaceTimePoint[1];
            double s2 = g[2] - spaceTimePoint[2];

            double te = spaceTimePoint[3];
            double c = s0 * s0 + s1 * s1 + s2 * s2 - te * te;
            double b = 2 * (s0 * h[0] + s1 * h[1] + s2 * h[2] + te);
            double a = h[0] * h[0] + h[1] * h[1] + h[2] * h[2] - 1;
            double discriminant = b * b - 4 * a * c;

            if (!(Math.Abs(a) < LtgConstants.MachineEpsilon))
            {
                if (discriminant >= 0)
                {
                    double d = Math.Sqrt(discriminant);
                    s1 = (-b + d) / (2 * a);
                    s2 = (-b - d) / (2 * a);
                    if (Math.Abs(s1) > Math.Abs(s2))
                    {
                        solutions.Add(s2);
                        solutions.Add(s1);
                    }
                    else
                    {
                        solutions.Add(s1);
                        solutions.Add(s2);
                    }
                }
                else if (Math.Abs(Math.Sqrt(-discriminant) / (2 * a)) < 10000)
                {
                    solutions.Add(-b / (2 * a));
                }
            }

            return solutions;
        }

        private static Offset[] GetQuartetOfSensors(Offset[] offsetArray, ref int groupingSkipNumber, ref int groupingStartingLocation)
        {
            Offset[] quartet = null;
            while (quartet == null)
            {
                //selecting atleast one group. The array length is the length of the number of offsets and is divided by 4
                if (groupingSkipNumber > offsetArray.Length / 4)
                {
                    return null;
                }
                //check to get the next group of sensors to form the quaret
                // Group the next four sensors from groupingStartingLocation
                if (offsetArray.Length >= groupingStartingLocation + groupingSkipNumber * 4)
                {
                    quartet = new Offset[4];
                    for (int i = 0; i < 4; i++)
                    {
                        // the groupskpnumber is 1 and the index starts increasing
                        int index = groupingStartingLocation + i * groupingSkipNumber;
                        //check to see if the stations are not same in the quartet being selected and if length is smaller than index
                        // which is incremented.
                        while (index < offsetArray.Length && StationInOffsetArray(offsetArray[index].StationId, quartet))
                        {
                            index++;
                        }
                        if (index >= offsetArray.Length)
                        {
                            groupingStartingLocation = 0;
                            groupingSkipNumber++;
                            quartet = null;
                            break;
                        }
                        quartet[i] = offsetArray[index];
                    }
                }
                else
                {
                    groupingStartingLocation = 0;
                    groupingSkipNumber++;
                }
            }
            groupingStartingLocation++;
            return quartet;
        }

        //check to see if the stations are not same in the quartet being selected
        private static bool StationInOffsetArray(string stationId, Offset[] offsets)
        {
            foreach (Offset offset in offsets)
            {
                if (offset != null)
                {
                    if (String.Compare(offset.StationId, stationId, StringComparison.Ordinal) == 0)
                        return true;
                }
                else
                {
                    return false;
                }
            }
            return false;
        }

        // Make sure there is no flashPortion in the list that has the exact same sensors as newFlashPortion
        private static void AddFlashPortionToCadidatePortionList(List<LTGFlashPortion> candidateFlashPortionList, LTGFlashPortion newFlashPortion)
        {
            candidateFlashPortionList.Add(newFlashPortion);
        }

        private static readonly double MinimumFlashGroupDistanceInKm = Config.MinimumFlashGroupDistanceInKm;

        private const double MinGapOfFlashPortionInMicroseconds = LtgConstants.MinGapOfFlashPortionInSecond * DateTimeUtcNano.MicrosecondsPerSecond;

        private static void AddFlashPortionToCluster(LTGFlashPortion flashPortion, List<List<LTGFlashPortion>> flashPortionClusterList)
        {
            int i;
            int j;
            List<LTGFlashPortion> cluster = null;
            for (i = 0; i < flashPortionClusterList.Count; i++)
            {
                cluster = flashPortionClusterList[i];
                for (j = 0; j < cluster.Count; j++)
                {
                    LTGFlashPortion theFlashPortion = cluster[j];
                    double geoDistance = flashPortion.LocationCompareTo(theFlashPortion);

                    long timeDeltaMicroseconds = Math.Abs(flashPortion.TimeStamp.Microseconds - theFlashPortion.TimeStamp.Microseconds);

                    if (geoDistance < 20 * MinimumFlashGroupDistanceInKm && timeDeltaMicroseconds < MinGapOfFlashPortionInMicroseconds)
                    {
                        cluster.Add(flashPortion);
                        return;
                    }
                }
            }
            cluster = new List<LTGFlashPortion> { flashPortion };
            flashPortionClusterList.Add(cluster);
        }

        private static LTGFlashPortion GetFlashPortionFromACluster(List<LTGFlashPortion> cluster)
        {
            int nNumOfClusters = cluster.Count;

            LTGFlashPortion averageFlashPortion = new LTGFlashPortion();
            double latitude = 0.0, longitude = 0.0, height = 0.0, amplitude = 0.0, confidence = 0.0;
            //List<DateTimeUtcNano> flashTimeList = new List<DateTimeUtcNano>();
            LTGFlashPortion bestFlashPortion = null;

            foreach (LTGFlashPortion ltgPortion in cluster)
            {
                if (bestFlashPortion == null)
                {
                    bestFlashPortion = ltgPortion;
                }
                else if (ltgPortion.OffsetList.Count > bestFlashPortion.OffsetList.Count
                         || ltgPortion.ConfidenceData.BiggestAngle > bestFlashPortion.ConfidenceData.BiggestAngle)
                {
                    bestFlashPortion = ltgPortion;
                }
                latitude += ltgPortion.Latitude;
                longitude += ltgPortion.Longitude;
                height += ltgPortion.Height;
                amplitude += ltgPortion.Amplitude;
                confidence += ltgPortion.Confidence;
                //flashTimeList.Add(ltgPortion.TimeStamp);
            }

            // Need enough sensor for the location
            if (bestFlashPortion == null || bestFlashPortion.OffsetList.Count < MinimumSensorNumberInLocator)
                return null;

            //flashTimeList.Sort();
            averageFlashPortion.TimeStamp = bestFlashPortion.TimeStamp; // (string)flashTimeList[flashTimeList.Count / 2];

            averageFlashPortion.Latitude = latitude / nNumOfClusters;
            averageFlashPortion.Longitude = longitude / nNumOfClusters;
            averageFlashPortion.Height = height / nNumOfClusters;
            averageFlashPortion.Amplitude = (float)amplitude / nNumOfClusters;
            averageFlashPortion.Confidence = (int)confidence / nNumOfClusters;
            averageFlashPortion.ConfidenceData = bestFlashPortion.ConfidenceData;
            averageFlashPortion.CommonPortionRef = ((CommonPortion)cluster[0].CommonPortionRef).Clone();
            averageFlashPortion.OffsetList = bestFlashPortion.OffsetList; //.AddRange(offsets.Values);
            averageFlashPortion.OffsetList.Sort();
            //averageFlashPortion.Description = GetFlashPortionDescription((Offset[])averageFlashPortion.OffsetArrayList.ToArray(typeof(Offset)));
            return averageFlashPortion;
        }

        #region PostLocator

        // Get the short portions of the closest sensors that saw the flashPortion

        public static CommonPortion GetPostCommonPortionFromPreFlashPortion(LTGFlashPortion flashPortion, ConcurrentDictionary<string, StationData> stationTable, double sensorDensity)
        {
            CommonPortion newCommonPortion = null;

            List<ShortPortionDistance> shortPortionDistanceList = new List<ShortPortionDistance>();

            // Original common portion
            CommonPortion commonPortion = (CommonPortion)flashPortion.CommonPortionRef;
            StationData station;
            // Find the short portions (sensors)  that meet the distance to the flash
            int i = 0, j = 0;
            while (i < commonPortion.ShortPortions.Count)
            {
                ShortPortion shortPortion = commonPortion.ShortPortions[i];
                if (stationTable.TryGetValue(shortPortion.StationId, out station))
                {
                    if (flashPortion.TimeStamp <= shortPortion.EndTime)
                    {
                        double distance = station.DistanceFromLatLongHeightInMicrosecond(flashPortion.Latitude, flashPortion.Longitude, 0);
                        if (distance < LtgConstants.MaxDistanceOfSensorToFlashInMicrosecond)
                        {
                            shortPortionDistanceList.Add(new ShortPortionDistance { ShortPortion = shortPortion, DistanceToFlash = distance });
                        }
                    }
                }
                i++;
            }

            if (shortPortionDistanceList.Count < MinimumSensorNumberInLocator)
                return null;

            shortPortionDistanceList.Sort(new ShortPortionComparerByDistance());

            // Remove the duplicated sensors: Keep the sensors with smaller TOA error
            i = 0;
            while (i < shortPortionDistanceList.Count)
            {
                ShortPortionDistance portionDistance1 = shortPortionDistanceList[i];
                j = i + 1;
                while (j < shortPortionDistanceList.Count)
                {
                    ShortPortionDistance portionDistance2 = shortPortionDistanceList[j];
                    if (portionDistance2.DistanceToFlash > portionDistance1.DistanceToFlash + 10)
                        break;

                    if (String.Compare(portionDistance1.ShortPortion.StationId, portionDistance2.ShortPortion.StationId, StringComparison.Ordinal) == 0)
                    {
                        double toaError1 = Math.Abs(portionDistance1.DistanceToFlash
                            - Math.Abs(flashPortion.TimeStamp.Microseconds - portionDistance1.ShortPortion.WaveformScore.FirstPeakWaveform.TimeStamp.Microseconds));
                        double toaError2 = Math.Abs(portionDistance2.DistanceToFlash
                            - Math.Abs(flashPortion.TimeStamp.Microseconds - portionDistance2.ShortPortion.WaveformScore.FirstPeakWaveform.TimeStamp.Microseconds));

                        if (toaError1 > toaError2)
                        {
                            shortPortionDistanceList.RemoveAt(i);
                            i--;
                            break;
                        }
                        shortPortionDistanceList.RemoveAt(j);
                    }
                    else
                    {
                        j++;
                    }
                }
                i++;
            }

            if (shortPortionDistanceList.Count < MinimumSensorNumberInLocator)
                return null;

            newCommonPortion = new CommonPortion();
            i = 0;
            int selectedSensorNum = 0;
            int closestGoodSensorNum = -1;
            int secondCloseestSensorToFlashDistance = int.MaxValue;
            int farestGoodSensorNum = -1;
            ShortPortionDistance firstPortionDistanceSelected = null;

            int minimumSensorDistance = GetMinimumSensorDistance(sensorDensity);

            while (i < shortPortionDistanceList.Count && selectedSensorNum < LtgConstants.MaxSensorNumberInPostProcessingLocator)
            {
                ShortPortion portion = shortPortionDistanceList[i].ShortPortion;

                double toaError = Math.Abs(shortPortionDistanceList[i].DistanceToFlash -
                                           Math.Abs(flashPortion.TimeStamp.Microseconds - portion.WaveformScore.AbsPeakWaveform.TimeStamp.Microseconds));

                if (toaError < LtgConstants.MaxBadToasInDetectionRange && !SensorTooCloseInPostProcessor(portion, newCommonPortion.ShortPortions, stationTable, minimumSensorDistance))
                {
                    if (closestGoodSensorNum == -1)
                    {
                        closestGoodSensorNum = i;
                        if (i <= shortPortionDistanceList.Count - 2)
                        {
                            secondCloseestSensorToFlashDistance = (int)shortPortionDistanceList[i + 1].DistanceToFlash;
                        }
                    }
                    farestGoodSensorNum = i;
                    newCommonPortion.AddShortPortion(portion);
                    selectedSensorNum++;

                    if (firstPortionDistanceSelected == null)
                    {
                        firstPortionDistanceSelected = shortPortionDistanceList[i];
                    }
                }
                i++;
            }

            if (selectedSensorNum < MinimumAgreedSensorNumber(sensorDensity))
            {
                return null;
            }

            if (sensorDensity > MinimumSensorDensityForCheckingClosestSensor && secondCloseestSensorToFlashDistance > 150 && flashPortion.OffsetList.Count < 7
                && TooManyNearbySensorNotUsed(flashPortion, stationTable, flashPortion.ActiveSensorTable as ConcurrentDictionary<string, DateTime>,
                    secondCloseestSensorToFlashDistance))
            {
                return null;
            }

            newCommonPortion.SortShortPortions();

            // If the closest sensor is more than 1ms check if there are active sensors closer to the flash
            int checkDistance = 2000;

            if (flashPortion.OffsetList.Count < MinimumSensorNumberInLocator + 1)
            {
                checkDistance = 800;
            }
            else if (flashPortion.OffsetList.Count < MinimumSensorNumberInLocator + 2)
            {
                checkDistance = 1000;
            }
            else if (flashPortion.OffsetList.Count < MinimumSensorNumberInLocator + 3)
            {
                checkDistance = 1200;
            }

            if (firstPortionDistanceSelected.DistanceToFlash > checkDistance)
            {
                double delta_lat = 0.75 * LtgConstants.LightspeedInKMPerMicrosecond * firstPortionDistanceSelected.DistanceToFlash / 111; // 111km/degree
                double delta_lon = 0.75 * delta_lat / Math.Cos(flashPortion.Latitude * Math.PI / 180);
                Rect rect = new Rect(flashPortion.Longitude - delta_lon, flashPortion.Longitude + delta_lon, flashPortion.Latitude - delta_lat, flashPortion.Latitude + delta_lat);
                int closerSensors = 0;

                ConcurrentDictionary<string, DateTime> active = flashPortion.ActiveSensorTable as ConcurrentDictionary<string, DateTime>;
                if (active != null)
                {
                    foreach (string sensorId in active.Keys)
                    {
                        if (stationTable.TryGetValue(sensorId, out station) && rect.IsInside(station.Longitude, station.Latitude))
                        {
                            closerSensors++;
                        }
                    }
                }

                if (closerSensors > 2 * MinimumSensorNumberInLocator)
                {
                    return null;
                }
            }

            // Too many nearby sensors don't agree
            if (closestGoodSensorNum > 2 * MinimumSensorNumberInLocator || farestGoodSensorNum - closestGoodSensorNum > 2 * selectedSensorNum)
            {
                return null;
            }

            if (newCommonPortion.ShortPortions.Count < MinimumSensorNumberInLocator)
            {
                return null;
            }

            return newCommonPortion;
        }

        private static bool SensorTooCloseInPostProcessor(ShortPortion portion, ReadOnlyCollection<ShortPortion> shortPortionList, ConcurrentDictionary<string, StationData> stationData,
            int minDistance)
        {
            StationData station;
            if (stationData.TryGetValue(portion.StationId, out station))
            {
                double distance;
                foreach (ShortPortion theportion in shortPortionList)
                {
                    if (station.DistanceInMicrosecondTable.TryGetValue(theportion.StationId, out distance))
                    {
                        if (distance < minDistance)
                            return true;
                    }
                }
            }
            return false;
        }

        public static int MinimumAgreedSensorNumber(double density)
        {
            /*
            if (density > 5)
                return MinimumSensorNumberInLocator + 2;
              */

            if (density > 2)
                return MinimumSensorNumberInLocator + 1;

            return MinimumSensorNumberInLocator;
        }

        private static int GetMinAngleFromSensorDensity(double sensorDensity)
        {
            int minAngle;
            if (sensorDensity > 2)
                minAngle = LtgConstants.MinSensorAroundFlashAngle + AngleFactor * 2;
            else
                minAngle = LtgConstants.MinSensorAroundFlashAngle + (int)(AngleFactor * sensorDensity);
            return minAngle;
        }

        private static int GetMinSensorDistributionFromSensorDensity(double sensorDensity)
        {
            int minDistribution = LtgConstants.MinSensorBinDistribution;
            if (sensorDensity > 2)
                minDistribution = LtgConstants.MinSensorBinDistribution + 3;
            else if (sensorDensity > 1)
                minDistribution = LtgConstants.MinSensorBinDistribution + 2;
            else if (sensorDensity > 0.05)
                minDistribution = LtgConstants.MinSensorBinDistribution + 1;
            return minDistribution;
        }

        // This is the minimum distance for sensors that can be grouped together
        private static int GetMinimumSensorDistance(double density)
        {
            int minDistance;
            if (density > 2)
                minDistance = LtgConstants.MinDistanceOfTwoSensorsInMicrosecond + MinDistanceFactorOfTwoSensors * 2;
            else
                minDistance = LtgConstants.MinDistanceOfTwoSensorsInMicrosecond + (int)(MinDistanceFactorOfTwoSensors * density);
            return minDistance;
        }

        private static bool TooManyNearbySensorNotUsed(LTGFlashPortion flashPortion, ConcurrentDictionary<string, StationData> stationTable, ConcurrentDictionary<string, DateTime> activeSensors,
            int firstPortionDistanceSelected)
        {
            double delta_lat = LtgConstants.LightspeedInKMPerMicrosecond * firstPortionDistanceSelected / 111;
            // 111km/degree
            double delta_lon = delta_lat / Math.Cos(flashPortion.Latitude * Math.PI / 180);
            Rect rect = new Rect(flashPortion.Longitude - delta_lon, flashPortion.Longitude + delta_lon,
                flashPortion.Latitude - delta_lat, flashPortion.Latitude + delta_lat);

            int nearbySensors = 0;
            foreach (string sensorId in activeSensors.Keys)
            {
                StationData station = null;
                if (stationTable.TryGetValue(sensorId, out station) && rect.IsInside(station.Longitude, station.Latitude))
                {
                    nearbySensors++;
                }
            }

            // Quick check: too many
            if (nearbySensors >= flashPortion.OffsetList.Count)
                return true;

            return false;
        }

        private static readonly int MaxTimeInPostLocatorMillisecond = Config.MaxTimeInPostLocatorMillisecond;
        private static readonly int MaxPostLocatorCount = Config.MaxPostLocatorCount;
        private static readonly int MinPostLocatorAttempts = Config.MinPostLocatorAttempts;

        // The offsets are from the closest sensors, only one flashPortion should be detected

        public static LTGFlashPortion PostFindFlashPortionFromOffsets(Offsets offsets, ConcurrentDictionary<string, StationData> stationData, LTGFlashPortion preFlashPortion, int portionCount, double sensorDensity)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            TimeSpan span;

            Offset[] offsetArray = CheckAndSortStations(offsets, stationData);
            LTGFlashPortion bestFlashPortion = null;

            if (offsetArray == null || offsetArray.Length < MinimumSensorNumberInLocator)
                return null;

            int nTries = 0; // iteration number of mulilateration
            int nOptimizedNum = 0;

            offsetArray = SetOffsetLocations(offsetArray, stationData);

            int groupingSensorSkipNum = 1;
            int groupingStartingLocation = 0;
            double[,] observations;

            while (true)
            {
                Offset[] quartet = GetQuartetOfSensors(offsetArray, ref groupingSensorSkipNum, ref groupingStartingLocation);
                if (quartet == null)
                {
                    break;
                }
                // Get a solution from 4 sensors
                double[][] solutions = Locate(quartet);

                if (solutions != null && solutions.GetLength(0) != 0)
                {
                    double[] solution;
                    if (solutions.GetLength(0) == 1)
                    {
                        // just one solution
                        solution = solutions[0];
                    }
                    else
                    {
                        // There two solutions:  Find the one with the best height
                        solution = ChooseSolutionWithBestHeight(solutions);
                    }

                    // Check the offsets that meet the Residual errors
                    Offset[] goodOffsetArray = CheckAbsResidualsWithinErrorRange(solution, offsetArray, LtgConstants.MaxResidualErrorsInLocator);

                    // Use the good offset array otherwise use everything
                    bool exactSolutionGood = false;
                    if (goodOffsetArray != null && goodOffsetArray.Length >= MinimumSensorNumberInLocator)
                    {
                        observations = GetObsLocation(goodOffsetArray, stationData);
                        exactSolutionGood = true;
                    }
                    else
                    {
                        int num = offsetArray.Length - groupingStartingLocation + 1;
                        if (num >= MinimumSensorNumberInLocator)
                        {
                            goodOffsetArray = new Offset[num];
                            Array.Copy(offsetArray, groupingStartingLocation - 1, goodOffsetArray, 0, num);
                            observations = GetObsLocation(goodOffsetArray, stationData);
                        }
                        else
                            observations = GetObsLocation(offsetArray, stationData);
                    }

                    // Set height to 0
                    double[] laLonHeight = StationData.LatLonHeightFromXyz(solution);
                    double initSolutionX, initSolutionY, initSolutionZ;
                    StationData.XyzFromLatLonHeight(laLonHeight[0], laLonHeight[1], laLonHeight[2], out initSolutionX, out initSolutionY, out initSolutionZ);

                    solution[0] = initSolutionX;
                    solution[1] = initSolutionY;
                    solution[2] = initSolutionZ;

                    double[] refinedSolution = RefineSolutionUsingLm3(observations, solution);

                    if (refinedSolution != null)
                    {
                        Offset[] refinedGoodOffsetArray = CheckAbsResidualsWithinErrorRange(refinedSolution, offsetArray, LtgConstants.MaxResidualErrorsInLocator);

                        if (refinedGoodOffsetArray == null || refinedGoodOffsetArray.Length < MinimumSensorNumberInLocator)
                        {
                            if (exactSolutionGood)
                            {
                                refinedGoodOffsetArray = goodOffsetArray;
                                refinedSolution = solution;
                            }
                            else
                                continue;
                        }

                        solution = refinedSolution;

                        LTGFlashPortion flashPortion = GetFlashPortionFromSolution(solution, refinedGoodOffsetArray, stationData);

                        if (flashPortion != null)
                        {
                            flashPortion.CommonPortionRef = preFlashPortion.CommonPortionRef;
                            flashPortion.UpdateSensorToFlashDistance(stationData);
                            FlashConfidence.GetFlashPortionConfidence(flashPortion, stationData, true);

                            if (nOptimizedNum > 8)
                                break;

                            span = stopwatch.Elapsed;

                            // This is a time-consuming process, if the total time spending here exceeds 500ms, it should quit
                            if (nTries++ >= MinPostLocatorAttempts)
                            {
                                if (portionCount > MaxPostLocatorCount)
                                {
                                    EventManager.LogWarning(EventId.PostLocator.PostProcessTimeout,
                                      string.Format("Postprocessing waiting portion length exceeded, Tries: {0}, Milliseconds: {1} Time: {2}, PreportionCount:{3}", nTries, span.TotalMilliseconds, offsets.StartTime, portionCount));
                                    break;
                                }

                                if (span.TotalMilliseconds > MaxTimeInPostLocatorMillisecond)
                                {
                                    EventManager.LogWarning(EventId.PostLocator.PostProcessTimeout,
                                      string.Format("Postprocessing max time exceeded, Tries: {0}, Milliseconds: {1} Time: {2}, PreportionCount:{3}", nTries, span.TotalMilliseconds, offsets.StartTime, portionCount));
                                    break;
                                }
                            }

                            double minSensorAroundFlashAngle = GetMinAngleFromSensorDensity(sensorDensity);
                            int minDistribution = GetMinSensorDistributionFromSensorDensity(sensorDensity);

                            if (flashPortion.ConfidenceData.BiggestAngle < minSensorAroundFlashAngle
                                || flashPortion.ConfidenceData.SensorDistribution < minDistribution
                                || flashPortion.Confidence < LtgConstants.MinNearestSensorConfidenceLevelForPostLocator)
                                continue;

                            if (bestFlashPortion == null)
                            {
                                bestFlashPortion = flashPortion;
                            }
                            else
                            {
                                //if (FlashGrouper.BetterFlashPortionBasedOnDistance(flashPortion, bestFlashPortion))
                                // if (BetterFlashPortionInAverageResidual(flashPortion, bestFlashPortion))
                                if (BetterFlashPortion(flashPortion, bestFlashPortion))
                                {
                                    nOptimizedNum++;
                                    bestFlashPortion = flashPortion;
                                }
                            }

                            // Stop early if the confidence is bigger than the minimum required value
                            if (bestFlashPortion.Confidence > 80)
                                break;
                        }
                    }
                }
            }

            stopwatch.Stop();
            return bestFlashPortion;
        }

        // Check whether the newFlashPortion is better than the old one

        private static bool BetterFlashPortion(LTGFlashPortion newFlashPortion, LTGFlashPortion oldFlashPortion)
        {
            return (newFlashPortion.OffsetList.Count > oldFlashPortion.OffsetList.Count ||
                newFlashPortion.Confidence > oldFlashPortion.Confidence);
        }

        #endregion PostLocator

        #region ErrorEllipse

        // Adjust ellipse axes by empirically determined fudge factor
        // If we find that more than half of all true locations are inside our ellipses,
        // we need to make this factor smaller.  If we find that less than half of all
        // true locations are inside, we need to make this factor larger.
        private static readonly double ErrorEllipseEmpiricalFudgeFactor = Config.ErrorEllipseEmpiricalFudgeFactor;

        private static readonly double ErrorEllipseMaxDistanceAllowedKm = Config.ErrorEllipseMaxDistanceAllowedKm;

        public static void GetErrorEllipseForFlashPortions(List<LTGFlashPortion> flashPortions, ConcurrentDictionary<string, StationData> stationData)
        {
            int i = 0;
            while (i < flashPortions.Count)
            {
                LTGFlashPortion flashPortion = flashPortions[i];
                CalculateFlashPortionEllipse(flashPortion, stationData);
                i++;
            }
        }

        // Return the major axis value of the error ellipse
        public static void CalculateFlashPortionEllipse(LTGFlashPortion flashPortion, ConcurrentDictionary<string, StationData> stationData)
        {
            List<Offset> offsetList = flashPortion.OffsetList;

            double xT0, yT0, zT0;
            StationData.XyzFromLatLonHeight(flashPortion.Latitude, flashPortion.Longitude, 0, out xT0, out yT0, out zT0);
            double majorAxisLength = 0.0;
            double minorAxisLength = 0.0;
            double majorAxisBearing = 0.0;

            // calculate the error from each sensor when it is left out in the calculation
            List<Offset> leaveOneOutErrorOffsetList = new List<Offset>();
            //ArrayList leaveOneOutErrorOffsetList = new ArrayList();
            for (int i = 0; i < offsetList.Count; i++)
            {
                List<Offset> chosenOffsets = new List<Offset>();
                // Select the offsets excluding one sensor
                for (int j = 0; j < offsetList.Count; j++)
                {
                    if (i != j)
                    {
                        chosenOffsets.Add(offsetList[j]);
                    }
                }

                Offset[] offsetArray = chosenOffsets.ToArray();
                double[,] observations = GetObsLocation(offsetArray, stationData);
                double[] refinedSolution = RefineSolutionUsingLm3(observations, flashPortion.Solution);

                Offset theOffset = (Offset)offsetList[i].Clone();
                double error = FindError(theOffset, refinedSolution); // In microsecond

                // offset.(x, y, z) now represents the vector from the flash to the sensor
                theOffset.X -= xT0;
                theOffset.Y -= yT0;
                theOffset.Z -= zT0;

                // Borrow the Offset.toga for this purpose
                theOffset.ResidualError = error;
                leaveOneOutErrorOffsetList.Add(theOffset);
            }

            leaveOneOutErrorOffsetList.Sort(Offset.CompareOffsetByResidualError);  // Sort based  on the residual error

            // Find major axis and minor axis
            int halfNumOfOffsets = (leaveOneOutErrorOffsetList.Count + 1) / 2;
            Offset medianOffset = leaveOneOutErrorOffsetList[halfNumOfOffsets];

            // Choose the ellipse that has the smallest possible major axis, ans contains the true solution half the time
            majorAxisLength = medianOffset.ResidualError;

            //double[] majorAxisXyz = { medianOffset.X, medianOffset.Y, medianOffset.Z };
            double norm = Norm(medianOffset.X, medianOffset.Y, medianOffset.Z);

            medianOffset.X /= norm;
            medianOffset.Y /= norm;
            medianOffset.Z /= norm;

            // Project into tangent plane
            double majorAxisX, majorAxisY, majorAxisZ;
            Undot(medianOffset.X, medianOffset.Y, medianOffset.Z, xT0, yT0, zT0, out majorAxisX, out majorAxisY, out majorAxisZ);

            // Renormalize it
            norm = Norm(majorAxisX, majorAxisY, majorAxisZ);
            majorAxisX /= norm;
            majorAxisY /= norm;
            majorAxisZ /= norm;

            // bearing is the angle in degrees east of north of the major axis of the ellipse
            // north=0, east=90, south=180, west=270
            // I'm assuming a sphere here; using an ellipsoid would make minor corrections
            // but we are only reporting the bearing to one degree (and our uncertainty is greater
            // than one degree) so there is no point in using an ellipsoid.
            // bearing is arcsine of ratio of z component of major axis to z component of
            // unit north vector at this latitude

            double cosvalue = majorAxisZ / Math.Sqrt(1 - Math.Pow(zT0 / Norm(xT0, yT0, zT0), 2));
            if (cosvalue > 1.0)
                cosvalue = 1.0;
            if (cosvalue < -1.0)
                cosvalue = -1.0;
            majorAxisBearing = Math.Acos(cosvalue);

            // Now find the minor axis length that contains all the observed errors.  We
            // only consider the half of observations with the smallest errors
            //(leaveOneOutErrors[:n]) and we ompute minor axis length for each and keep
            // the largest.
            minorAxisLength = 0;
            for (int i = 0; i < halfNumOfOffsets; i++)
            {
                double error = leaveOneOutErrorOffsetList[i].ResidualError;
                double theta = Math.Acos(Dot(leaveOneOutErrorOffsetList[i].X, leaveOneOutErrorOffsetList[i].Y, leaveOneOutErrorOffsetList[i].Z, majorAxisX, majorAxisY, majorAxisZ)
                    / Norm(leaveOneOutErrorOffsetList[i].X, leaveOneOutErrorOffsetList[i].Y, leaveOneOutErrorOffsetList[i].Z));
                Double b = Math.Sin(theta) / Math.Sqrt(1 / (error * error) - Math.Pow(Math.Cos(theta) / majorAxisLength, 2));
                if (b > minorAxisLength)
                    minorAxisLength = b;
            }

            // Convert microsecond to Km
            majorAxisLength *= LtgConstants.LightspeedInKMPerMicrosecond;
            minorAxisLength *= LtgConstants.LightspeedInKMPerMicrosecond;

            // Convert radians to degrees
            majorAxisBearing *= 180.0 / Math.PI;

            // Apply the Fudge factor
            majorAxisLength *= ErrorEllipseEmpiricalFudgeFactor;
            minorAxisLength *= ErrorEllipseEmpiricalFudgeFactor;

            // If ellipse axis length would display as zero, increase it to 100 meters.
            // Convert the values to KM
            majorAxisLength = Math.Max(0.1, majorAxisLength);
            minorAxisLength = Math.Max(0.1, minorAxisLength);

            // Append the data to the end of description of each portion
            AddErrorEllipseToFlashPortion(flashPortion, majorAxisLength, minorAxisLength, majorAxisBearing);
            //return majorAxisLength;
        }

        private static void AddErrorEllipseToFlashPortion(LTGFlashPortion flashPortion, double majorAxisLength,
            double minorAxisLength, double majorAxisBearing)
        {
            ErrorEllipse ee = new ErrorEllipse
            {
                MajorAxis = Math.Round(majorAxisLength, 3),
                MinorAxis = Math.Round(minorAxisLength, 3),
                MajorAxisBearing = Math.Round(majorAxisBearing, 1)
            };

            flashPortion.ErrorEllipse = ee;

            //string ellipseDescription = String.Format("ErrEllipse:majorAxis={0:0.000},minorAxis={1:0.000},bearing={2:0.0}",
            //    majorAxisLength, minorAxisLength, majorAxisBearing);
            //flashPortion.Description += ellipseDescription;
            //flashPortion.Description = ellipseDescription;
        }

        // return meters from the flashPortion location to the sensor location
        private static double FindError(Offset offset, double[] solution)
        {
            try
            {
                double[] error = new double[4];
                error[0] = offset.X - solution[0];
                error[1] = offset.Y - solution[1];
                error[2] = offset.Z - solution[2];
                error[3] = offset.T - solution[3];
                double timeError = TimeError(error);
                return Math.Abs(timeError);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        private static double Dot(double[] a, double[] b)
        {
            return a[0] * b[0] + a[1] * b[1] + a[2] * b[2];
        }

        private static double Dot(double aX, double aY, double aZ, double bX, double bY, double bZ)
        {
            return aX * bX + aY * bY + aZ * bZ;
        }

        private static double[] Undot(double[] a, double[] b)
        {
            double aDotb = Dot(a, b);
            double bDotb = Dot(b, b);
            double[] undotresult = new double[3];
            for (int i = 0; i < 3; i++)
            {
                undotresult[i] = a[i] - aDotb * b[i] / bDotb;
            }
            return undotresult;
        }

        private static void Undot(double aX, double aY, double aZ, double bX, double bY, double bZ, out double undotX, out double undotY, out double undotZ)
        {
            double aDotb = Dot(aX, aY, aZ, bX, bY, bZ);
            double bDotb = Dot(bX, bY, bZ, bX, bY, bZ);

            undotX = aX - aDotb * bX / bDotb;
            undotY = aY - aDotb * bY / bDotb;
            undotZ = aZ - aDotb * bZ / bDotb;
        }

        private static double Norm(double[] a)
        {
            return Math.Sqrt(Dot(a, a));
        }

        private static double Norm(double x, double y, double z)
        {
            return Math.Sqrt(Dot(x, y, z, x, y, z));
        }

        #endregion ErrorEllipse

        public class ObservationsComparer : IComparer<FluxList<double>>
        {
            private int Compare(FluxList<double> ob1, FluxList<double> ob2)
            {
                if (ob1 == null && ob2 == null) return 0;
                else if (ob1 == null) return -1;
                else if (ob2 == null) return 1;
                else if (ob1.Count < 4 && ob2.Count < 4) return 0;
                else if (ob1.Count < 4) return -1;
                else if (ob2.Count < 4) return 1;
                else
                {
                    return ob1[3].CompareTo(ob2[3]);
                }
            }

            int IComparer<FluxList<double>>.Compare(FluxList<double> x, FluxList<double> y)
            {
                return Compare(x, y);
            }
        }

        public class ArrayComparerOriginal : IComparer
        {
            private readonly int _index;

            public ArrayComparerOriginal(int sortFieldIndex)
            {
                _index = sortFieldIndex;
            }

            public int Compare(object x, object y)
            {
                IComparable cx = (IComparable)((Array)x).GetValue(_index);
                IComparable cy = (IComparable)((Array)y).GetValue(_index);
                return cx.CompareTo(cy);
            }
        }
    }
}
