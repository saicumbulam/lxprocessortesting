﻿using System;
using System.Runtime;
using System.ServiceProcess;
using Aws.Core.Utilities;

using LxCommon.Monitoring;

using LxProcessor.MainController;

namespace LxProcessor
{
    public class Program
    {
        private static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
            { 
                new WinService() 
            };
            ServiceBase.Run(ServicesToRun);
        }

        private static ProcessController _processController = null;
        
        public static void Start()
        {
            try
            {
                

                EventManager.Init();
                
                if (_processController == null)
                {
                    _processController = new ProcessController();
                }
                _processController.Start();

                
            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.TidServiceStartupFail,
                                      "Lightning Processor Service failed to start.\r\nReason: " + ex.Message);
            }
        }

        public static void Stop()
        {
            try
            {
                if (_processController != null)
                    _processController.Stop();
            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.TidServiceStoppedFail,
                                      "Lightning Processor Service stopped with errors. \r\nReason: " + ex.Message);
            }
        }
    }
}
