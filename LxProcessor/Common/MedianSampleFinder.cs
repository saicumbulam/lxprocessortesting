﻿using System;
using System.Threading;

namespace LxProcessor.Common
{
    public class MedianSampleFinder
    {
        private int[] _array;
        private readonly int _capacity;
        private int _index;
        private object _lock;
        private int _median;
        private ReaderWriterLockSlim _rwLock;

        public MedianSampleFinder(int capacity)
        {
            _capacity = capacity;
            _array = new int[_capacity];
            _index = 0;
            _median = 0;
            _lock = new object();
            _rwLock = new ReaderWriterLockSlim();
        }

        public void AddSample(int item)
        {
            int[] copy = null;
            lock (_lock)
            {
                if (_index > (_capacity - 1))
                {
                    _index = 0;
                    copy = new int[_capacity];
                    _array.CopyTo(copy, 0);
                }        

                _array[_index] = item;
                _index++;
            }

            if (copy != null)
                FindMedian(copy);
        }

        private void FindMedian(int[] copy)
        {
            Array.Sort(copy);
            int index = (_capacity - 1) / 2;

            _rwLock.EnterWriteLock();
            try
            {
                _median = copy[index];
            }
            finally
            {
                _rwLock.ExitWriteLock();
            }
        }

        public int Median
        {
            get 
            {
                _rwLock.EnterReadLock();
                try
                {
                    return _median;
                }
                finally
                {
                    _rwLock.ExitReadLock();
                }
            }
        }

    }
}
