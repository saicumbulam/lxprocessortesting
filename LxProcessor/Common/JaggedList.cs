﻿using System;
using System.Collections.Generic;
using LxCommon.DataStructures;

namespace LxProcessor.Common
{
    public class JaggedList<T>
    {
        private readonly FluxList<FluxList<T>> _jagged;

        private const int InitialSize = 10;
        private int _outerSize;

        
        public JaggedList() : this(InitialSize, InitialSize) {}
        
        public JaggedList(int outerSize, int innerSize)
        {
            _jagged = new FluxList<FluxList<T>>();
            for (int i = 0; i < outerSize; i++)
            {
                FluxList<T> l = new FluxList<T>();
                for (int j = 0; j < innerSize; j++)
                {
                    l.Add(default(T));
                }
                _jagged.Add(l);
            }
            _outerSize = outerSize;
        }

        private JaggedList(int outerSize)
        {
            _jagged = new FluxList<FluxList<T>>();
            for (int i = 0; i < outerSize; i++)
            {
                _jagged.Add(null);
            }
            _outerSize = outerSize;
        }

        public void Resize(int outer, int inner)
        {
            _jagged.SetLengthAtLeast(outer, () => { return new FluxList<T>(); });
            for (int i = 0; i < outer; i++)
            {
                if (_jagged[i] == null)
                {
                    _jagged[i] = new FluxList<T>();
                }
                _jagged[i].Resize(inner);
            }
            _outerSize = outer;
        }


        public void Clear()
        {
            int index = 0;
            while (index < _outerSize)
            {
                _jagged[index].Clear();
                index++;
            }
        }

        public FluxList<T> this[int index]
        {
            get
            {
                if (index >= _outerSize)
                {
                    throw new ArgumentOutOfRangeException("index");
                }
                return _jagged[index];
            }
            set
            {
                if (index >= _outerSize)
                {
                    throw new ArgumentOutOfRangeException("index");
                }
                _jagged[index] = value;
            }
        }

        public void Sort(IComparer<FluxList<T>> comparer)
        {
            _jagged.Sort(0, _outerSize, comparer);
        }

        public JaggedList<T> Clone()
        {
            JaggedList<T> clone = null;
            if (_outerSize > 0)
            {
                clone = new JaggedList<T>(_outerSize);
                for (int i = 0; i < _outerSize; i++)
                {
                    if (_jagged[i] != null)
                    {
                        clone[i] = new FluxList<T>(_jagged[i]);
                    }
                }
            }
            return clone;
        }
    }
}
