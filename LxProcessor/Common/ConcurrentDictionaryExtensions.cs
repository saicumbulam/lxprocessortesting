﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace LxProcessor.Common
{
    public static class ConcurrentDictionaryExtensions
    {
        public static IList<TValue> GetRemoveWhere<TKey, TValue>(this ConcurrentDictionary<TKey, TValue> dict, Func<KeyValuePair<TKey, TValue>, bool> predicate)
        {
            List<TValue> removed = new List<TValue>();
            
            List<TKey> keys = new List<TKey>();
            foreach (KeyValuePair<TKey, TValue> keyValuePair in dict)
            {
                if (predicate(keyValuePair))
                {
                    keys.Add(keyValuePair.Key);
                }
            }

            TValue trash;
            foreach (TKey key in keys)
            {
                if (dict.TryRemove(key, out trash))
                {
                    removed.Add(trash);
                }
            }

            return removed;
        }

        public static void RemoveWhere<TKey, TValue>(this ConcurrentDictionary<TKey, TValue> dict, Func<KeyValuePair<TKey, TValue>, bool> predicate)
        {
            List<TKey> keys = new List<TKey>();
            foreach (KeyValuePair<TKey, TValue> keyValuePair in dict)
            {
                if (predicate(keyValuePair))
                {
                    keys.Add(keyValuePair.Key);
                }
            }

            TValue trash;
            foreach (TKey key in keys)
            {
                dict.TryRemove(key, out trash);
            }
        }

        public static IList<TValue> GetValueListWhere<TKey, TValue>(this ConcurrentDictionary<TKey, TValue> dict, Func<KeyValuePair<TKey, TValue>, bool> predicate)
        {
            List<TKey> keys = new List<TKey>();
            foreach (KeyValuePair<TKey, TValue> keyValuePair in dict)
            {
                if (predicate(keyValuePair))
                {
                    keys.Add(keyValuePair.Key);
                }
            }

            List<TValue> values = new List<TValue>();
            TValue value;
            foreach (TKey key in keys)
            {
                if (dict.TryGetValue(key, out value))
                {
                    values.Add(value);
                }
            }

            return values;
        }

        public static IList<TKey> GetKeysSnapshot<TKey, TValue>(this ConcurrentDictionary<TKey, TValue> dict)
        {
            List<TKey> keys = new List<TKey>();
            foreach (KeyValuePair<TKey, TValue> keyValuePair in dict)
            {
                keys.Add(keyValuePair.Key);
            }

            return keys;
        }
    }
}
