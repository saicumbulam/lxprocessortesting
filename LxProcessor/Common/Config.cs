using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net;
using System.Reflection;

using Aws.Core.Utilities;
using Aws.Core.Utilities.Config;

using LxCommon;
using LxCommon.TcpCommunication;

namespace LxProcessor.Common
{
    public class Config : AppConfig
    {
        private static readonly ConcurrentDictionary<IPEndPoint, ISendWorker> _allManagerAddresses;
        private static readonly ConcurrentDictionary<IPEndPoint, ISendWorker> _rawDataAddresses;
        private static readonly Rect _includeSensorRegion;
        private static readonly Rect _excludeSensorRegion;
        private static readonly string _assemblyVersion;
        private const string AssemblyName = "LxProcessor";
        static Config()
        {
            _allManagerAddresses = LoadForwardServers("LtgManager");
            _rawDataAddresses = LoadForwardServers("ForwardRawData");
            _includeSensorRegion = LoadRegion("IncludeSensorRegion");
            _excludeSensorRegion = LoadRegion("ExcludeSensorRegion");
            _assemblyVersion = FindAssemblyVersion(AssemblyName);
        }

        private static string FindAssemblyVersion(string name)
        {
            string version = string.Empty;
            Assembly[] assemblyList = AppDomain.CurrentDomain.GetAssemblies();
            foreach (Assembly assembly in assemblyList)
            {
                AssemblyName assemblyName = assembly.GetName();
                if (name == assemblyName.Name)
                {
                    version = assemblyName.Version.ToString();
                    break;
                }
            }
            return version;
        }

        private static ConcurrentDictionary<IPEndPoint, ISendWorker> LoadForwardServers(string configPrefix)
        {
            ConcurrentDictionary<IPEndPoint, ISendWorker> clients = null;

            int num = Get<int>(configPrefix + "Num", 0);
            if (num == 0)
                num = Get<int>(configPrefix + "ServerNum", 0);

            if (num > 0)
            {
                clients = new ConcurrentDictionary<IPEndPoint, ISendWorker>();

                for (int i = 1; i <= num; i++)
                {
                    int port = Get<int>(configPrefix + "Port" + i, -1);
                    IPAddress address;
                    if (port != -1 && IPAddress.TryParse(Get(configPrefix + "Ip" + i, string.Empty), out address))
                    {
                        IPEndPoint ipep = new IPEndPoint(address, port);
                        SendWorker sw = new SendWorker(ipep);
                        clients.TryAdd(ipep, sw);
                    }
                    else
                    {
                        EventManager.LogError(EventId.Config.AppConfig.ParsingForwardServer, "Error parsing config file");
                    }
                }
            }

            return clients;
        }

        private static Rect LoadRegion(string keyName)
        {
            Rect rect = null; ;

            if (!string.IsNullOrWhiteSpace(Get<string>(keyName, null)))
            {
                List<double> points = GetListConfigSettings<double>(keyName, null, ',');
                if (points != null && points.Count >= 4)
                {
                    rect = new Rect(points[0], points[1], points[2], points[3]);
                    EventManager.LogInfo(EventId.Config.AppConfig.UsingRegion, string.Format("Using {0}, (left: {1}, right: {2}, bottom: {3}, top: {4})", keyName, points[0], points[1], points[2], points[3]));
                }
                else
                {
                    EventManager.LogError(EventId.Config.AppConfig.ParsingRegion, string.Format("Unable to parse config for {0}", keyName));
                }
            }

            return rect;
        }

        public static int StationDatabaseUpdateIntervalMinutes
        {
            get { return Get<int>("StationDatabaseUpdateIntervalMinutes", 600); }
        }

        public static ProcessorType ProcessorType
        {
            get { return (ProcessorType)Get<int>("ProcessorType", 0); }
        }

        public static int ProcessorNum
        {
            get { return (Get<int>("ProcessorNo", 1) - 1); }
        }

        public static int TotalProcessors
        {
            get { return Get<int>("TotalProcessors", 1); }
        }

        public static bool IsPlayback
        {
            get { return Get<bool>("Playback", false); }
        }

        public static bool IsPlaybackFiles
        {
            get { return Get<bool>("PlaybackFiles", false); }
        }

        public static int LtgStatlistCheckDbFrequency
        {
            get { return Get<int>("LTGCheckStatListDBFrequency", 1); }
        }

        public static int TcpListenPort
        {
            get { return Get<int>("TCPListenPort", 1); }
        }

        public static int MinimumSensorNumberInLocator
        {
            get { return Get<int>("MinimumSensorNumberInLocator", 5); }
        }

        public static int MaximumWaveformProcessingDelaySeconds
        {
            get { return Get<int>("MaximumWaveformProcessingDelaySeconds", 30); }
        }

        public static double MinimumFlashGroupTimeInSecond
        {
            get { return Get<double>("MinimumFlashGroupTimeInSecond", 0.7); }
        }

        public static double MinimumFlashGroupDistanceInKm
        {
            get { return Get<double>("MinimumFlashGroupDistanceInKM", 10); }
        }

        public static string LtgFileDirArchive
        {
            get { return Get<string>("LTGFileArchiveDir", null); }
        }

        public static bool ExportAllRawData
        {
            get { return Get<bool>("ExportAllRawData", false); }
        }

        public static bool ExportFlash
        {
            get { return Get("ExportFlash", false); }
        }

        public static string ExportFlashFolder
        {
            get { return Get("ExportFlashFolder", @"E:\EN\Archive\LxProcessor"); }
        }

        public static string ExportFlashFileName
        {
            get { return Get("ExportFlashFileName", @"flash-{0}.json"); }
        }

        public static ConcurrentDictionary<IPEndPoint, ISendWorker> LtgManagerAddresses
        {
            get { return _allManagerAddresses; }
        }

        public static ConcurrentDictionary<IPEndPoint, ISendWorker> RawDataAddresses
        {
            get { return _rawDataAddresses; }
        }

        public static bool KeepStrokeDescriptionInfo
        {
            get { return Get<bool>("KeepStrokeDescriptionInfo", false); }
        }

        public static double ResidualErrorInSecond
        {
            get { return Get<double>("ResidualErrorInSecond", 0D); }
        }

        public static int NumberOfTcpHandlerThreads
        {
            get { return Get<int>("NumberOfTcpHandlerThreads", 1); }
        }

        public static int MaxRawDataQueueTasks
        {
            get { return Get<int>("MaxRawDataQueueTasks", 4); }
        }

        public static int MaxSampleDataQueueTasks
        {
            get { return Get<int>("MaxSampleDataQueueTasks", 4); }
        }

        public static int MaxWaveformTasks
        {
            get { return Get<int>("MaxWaveformTasks", 4); }
        }

        public static int MaxOffsetTasks
        {
            get { return Get<int>("MaxOffsetTasks", 8); }
        }

        public static int MaxPostLocatorTasks
        {
            get { return Get<int>("MaxPostLocatorTasks", 6); }
        }

        public static int MaxGrouperTasks
        {
            get { return Get<int>("MaxGrouperTasks", 2); }
        }

        public static int MaxTimeInLocatorMillisecond
        {
            get { return Get<int>("MaxTimeInLocatorMillisecond", 50); }
        }


        public static int MinLocatorAttempts
        {
            get { return Get<int>("MinLocatorAttempts", 8); }
        }

        public static int MaxTimeInPostLocatorMillisecond
        {
            get { return Get<int>("MaxTimeInPostLocatorMillisecond", 100); }
        }

        public static int MaxPostLocatorCount
        {
            get { return Get<int>("MaxPostLocatorCount", 1000); }
        }

        public static int MinPostLocatorAttempts
        {
            get { return Get<int>("MinPostLocatorAttempts", 10); }
        }

        public static int MaxWaveformsTableSeconds
        {
            get { return Get<int>("MaxWaveformsTableSeconds", 15); }
        }

        public static int WaveformDelayMedianSampleSize
        {
            get { return Get<int>("WaveformDelayMedianSampleSize", 60); }
        }

        public static int GrouperFlashPortionTimeDelayInMillisecond
        {
            get { return Get<int>("GrouperFlashPortionTimeDelayInMillisecond", 1000); }
        }

        public static int GrouperAccumulatedFlashPortions
        {
            get { return Get<int>("GrouperAccumulatedFlashPortions", 150); }
        }

        public static int MonitorConvertedAgeThresholdSeconds
        {
            get { return Get<int>("MonitorConvertedAgeThresholdSeconds", 60); }
        }

        public static int MonitorProcessedAgeThresholdSeconds
        {
            get { return Get<int>("MonitorProcessedAgeThresholdSeconds", 60); }
        }

        public static int MonitorDbStationsMinimum
        {
            get { return Get<int>("MonitorDbStationsMinimum", 1); }
        }

        public static int MonitorStationsWithWaveformsMinimum
        {
            get { return Get<int>("MonitorStationsWithWaveformsMinimum", 1); }
        }

        public static int MonitorIncomingTcpRateMinimum
        {
            get { return Get<int>("MonitorIncomingTcpRateMinimum", 1); }
        }

        public static int MonitorIncomingActiveConnectionsMinimum
        {
            get { return Get<int>("MonitorIncomingActiveConnectionsMinimum", 1); }
        }
        public static int MonitorIncomingConnectionRateMaximum
        {
            get { return Get<int>("MonitorIncomingConnectionRateMaximum", 1); }
        }

        public static int ProcessWaveformTimeDelayInSeconds
        {
            get { return Get<int>("ProcessWaveformTimeDelayInSeconds", 10); }
        }

        public static string PlaybackDirectoryName
        {
            get { return Get<string>("PlaybackDirectoryName", @"D:\aws_soft\Temp\Lightning Data\June"); }
        }

        public static DateTime PlaybackStartTime
        {
            get
            {
                return Config.Get<DateTime>("PlaybackStartTime", DateTime.SpecifyKind(DateTime.MinValue, DateTimeKind.Utc))
                    .ToUniversalTime();
            }
        }

        public static DateTime PlaybackEndTime
        {
            get
            {
                return Config.Get<DateTime>("PlaybackEndTime", DateTime.SpecifyKind(DateTime.MinValue, DateTimeKind.Utc))
                    .ToUniversalTime();
            }
        }

        public static bool PerfMonEnabled
        {
            get { return Get<bool>("PerfMonEnabled", true); }
        }

        public static bool PerfCsvWriterEnabled
        {
            get { return Get<bool>("PerfCsvWriterEnabled", false); }
        }

        public static string PerfCsvWriterOutputDirectory
        {
            get { return Get("PerfCsvWriterOutputDirectory", "."); }
        }

        public static int PerfCsvWriterIntervalMinutes
        {
            get { return Get("PerfCsvWriterIntervalMinutes", 1); }
        }

        public static string SystemType
        {
            get { return Get("SystemType", "TLN"); }
        }

        public static int ReceiverListenerMaxPendingConnections
        {
            get { return Get("ReceiverListenerMaxPendingConnections", 10); }
        }

        public static int ReceiverListenerRateSampleSize
        {
            get { return Get("ReceiverListenerRateSampleSize", 30); }
        }

        public static int ReceiverListenerConnectionRateSampleSize
        {
            get { return Get("ReceiverListenerConnectionRateSampleSize", 10); }
        }

        public static bool EnableGpsWriteDb
        {
            get { return Get("EnableGpsWriteDb", false); }
        }

        public static int ListenerIdleCheckIntervalSeconds
        {
            get { return Get("ListenerIdleCheckIntervalSeconds", 30); }
        }

        public static double ErrorEllipseEmpiricalFudgeFactor
        {
            get { return Get("ErrorEllipseEmpiricalFudgeFactor", 1.1); }
        }

        public static double ErrorEllipseMaxDistanceAllowedKm
        {
            get { return Get("ErrorEllipseMaxDistanceAllowedKm", 10); }
        }

        public static int PostLocatorTimeDelayMilliseconds
        {
            get { return Get("PostLocatorTimeDelayMilliseconds", 1000); }
        }

        public static int PostLocatorMaxAccumulatedPortions
        {
            get { return Get("PostLocatorMaxAccumulatedPortions", 100); }
        }

        public static int RawDataWaveformOfSecondSamplePoolSize
        {
            get { return Get("RawDataWaveformOfSecondSamplePoolSize", 2500); }
        }

        public static int RawDataPulseDataPoolSize
        {
            get { return Get("RawDataPulseDataPoolSize", 50); }
        }

        public static int StationWaveformPoolSize
        {
            get { return Get("StationWaveformPoolSize", 6000); }
        }

        public static int ShortPortionPoolSize
        {
            get { return Get("ShortPortionPoolSize", 75000); }
        }

        public static Rect IncludeSensorRegion
        {
            get { return _includeSensorRegion; }
        }

        public static Rect ExcludeSensorRegion
        {
            get { return _excludeSensorRegion; }
        }

        public static string AssemblyVersion
        {
            get { return _assemblyVersion; }
        }

        public static int FlashAgeMedianSamplerCapacity
        {
            get { return Get("FlashAgeMedianSamplerCapacity", 1000); }
        }

        public static int WaveformWorkItemPoolSize
        {
            get { return Get("WaveformWorkItemPoolSize", 10); }
        }

        public static int CrossCorrelatePoolSize
        {
            get { return Get("CrossCorrelatePoolSize", 50); }
        }

        public static int FVecLm3PoolSize
        {
            get { return Get("FVecLm3Pool", 20); }
        }

        public static int MinLmStatePoolSize
        {
            get { return Get("MinLmStatePool", 60); }
        }

        public static int MinLmReportPoolSize
        {
            get { return Get("MinLmReportPoolSize", 60); }
        }

        public static int SolutionsPoolSize
        {
            get { return Get("SolutionsPoolSize", 100); }
        }


        public static int OffsetProcessorMaxTasks
        {
            get { return Get("OffsetProcessorMaxTasks", 10); }
        }

        public static int OffsetProcessorSampleSize
        {
            get { return Get("OffsetProcessorSampleSize", 2000); }
        }

        public static int FlashLocatorDoubleListPoolSize
        {
            get { return Get("FlashLocatorDoubleListPoolSize", 100); }
        }

        public static int FlashLocatorMatrixPoolSize
        {
            get { return Get("FlashLocatorMatrixPoolSize", 50); }
        }

        public static int FlashLocatorJaggedPoolSize
        {
            get { return Get("FlashLocatorJaggedPoolSize", 50); }
        }

        public static int EValuesPoolSize
        {
            get { return Get("EValuesPoolSize", 50); }
        }

        public static int SensorDistancePoolSize
        {
            get { return Get("SensorDistancePoolSize", 50); }
        }

        public static int AngleListPoolSize
        {
            get { return Get("AngleListPoolSize", 50); }
        }

        public static int QuadrantListPoolSize
        {
            get { return Get("QuadrantListPoolSize", 50); }
        }

        public static int BinsPoolSize
        {
            get { return Get("BinsPoolSize", 50); }
        }

        public static int PostLocatorDegreeOfParallelism
        {
            get { return Get("PostLocatorDegreeOfParallelism", 5); }
        }

        public static long MaxLargeObjectHeapToCompactBytes
        {
            get { return Get("MaxLargeObjectHeapToCompactBytes", 5368709120); }
        }

        public static int LargeObjectHeapCheckIntervalMinutes
        {
            get { return Get("LargeObjectHeapCheckIntervalMinutes", 10); }
        }

        public static int MaxLargeObjectHeapCompactIntervalMinutes
        {
            get { return Get("MaxLargeObjectHeapCompactIntervalMinutes", 120); }
        }

        public static int MaxOffsetQueueCount
        {
            get { return Get("MaxOffsetQueueCount", 5000); }
        }

        public static int RefineSolutionMaxIterations
        {
            get { return Get("RefineSolutionMaxIterations", 20); }
        }

        public static int ClassifierMaxWaveformLengthMicroseconds
        {
            get { return Get("ClassifierMaxWaveformLengthMicroseconds", 280); }
        }

        public static int ClassifierMaxWaveformTimeBeforeToaMicroseconds
        {
            get { return Get("ClassifierMaxWaveformTimeBeforeToaMicroseconds", 80); }
        }

        public static int ClassifierMaxSensorForClassification
        {
            get { return Get("ClassifierMaxSensorForClassification", 3); }
        }

        public static int ClassifierMaxDistanceBeforeIonosphereReflectionMicroseconds
        {
            get { return Get("ClassifierMaxDistanceBeforeIonosphereReflectionMicroseconds", 2000); }
        }

        public static int ClassifierDistanceReversedCgPolarityMicroseconds
        {
            get { return Get("ClassifierDistanceReversedCgPolarityMicroseconds", 2400); }
        }

        public static int ClassifierMinFirstReturnStrokePeakCurrent
        {
            get { return Get("ClassifierMinFirstReturnStrokePeakCurrent", 10000); }
        }

        public static int ClassifierCgPercentage
        {
            get { return Get("ClassifierCgPercentage", 50); }
        }

        public static long MinimumReturnStrokeTimeDeltaNanoseconds
        {
            get { return Get("MinimumReturnStrokeTimeDeltaNanoseconds", 3000000); }
        }


        public static int SensorDensityGridSizeInDegrees
        {
            get { return Get("SensorDensityGridSizeInDegrees", 4); }
        }

        public static int SensorDensitySecondsToKeep
        {
            get { return Get("SensorDensitySecondsToKeep", 5); }
        }

        public static int MinDistanceOfTwoSensorsIncreaseFactorOnSensorDensity
        {
            get { return Get("MinDistanceOfTwoSensorsIncreaseFactorOnSensorDensity", 0); }
        }

        public static int AngleIncreaseRatioOnSensorDensity
        {
            get { return Get("AngleIncreaseRatioOnSensorDensity", 20); }
        }


        public static double MinimumSensorDensityForClosestSensorCheck
        {
            get { return Get("MinimumSensorDensityForClosestSensorCheck", 0.1); }
        }

    }
}
