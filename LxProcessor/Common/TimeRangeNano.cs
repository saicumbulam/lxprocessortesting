﻿using System;
using LxCommon.Models;

namespace LxProcessor.Common
{
    public struct TimeRangeNano : IEquatable<TimeRangeNano>, IComparable<TimeRangeNano>
    {
        public TimeRangeNano(DateTimeUtcNano start, DateTimeUtcNano end)
            : this()
        {
            Start = start;
            End = end;
        }

        public DateTimeUtcNano Start { get; private set; }

        public DateTimeUtcNano End { get; private set; }

        public bool Contains(DateTimeUtcNano dt)
        {
            return (dt >= Start && dt < End);
        }

        //public TimeSpan Duration
        //{
        //    get { return End - Start; }
        //}

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is TimeRangeNano && Equals((TimeRangeNano)obj);
        }

        public bool Equals(TimeRangeNano other)
        {
            return Start.Equals(other.Start) && End.Equals(other.End);
        }

        public static bool operator ==(TimeRangeNano left, TimeRangeNano right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(TimeRangeNano left, TimeRangeNano right)
        {
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Start.GetHashCode() * 397) ^ End.GetHashCode();
            }
        }

        public int CompareTo(TimeRangeNano other)
        {
            return Start.CompareTo(other.Start);
        }
    }
}
