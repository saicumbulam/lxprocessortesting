﻿using System;
using System.Collections.Generic;
using System.Text;

using Aws.Core.Utilities;

using LxCommon.DataStructures;
using LxCommon.Utilities;

namespace LxProcessor.Common
{
    public class Matrix<T>
    {
        private readonly JaggedList<T> _matrix;
        
        private int _rows;
        private int _columns;

        public Matrix() : this(0, 0)
        {

        }
    
        public Matrix(int rows, int columns)
        {
            _rows = rows;
            _columns = columns;
            _matrix = new JaggedList<T>(rows, columns);
        }

        private Matrix(int rows, int columns, JaggedList<T> matrix)
        {
            _rows = rows;
            _columns = columns;
            _matrix = matrix;
        }

        public void Add(int x, int y, T val)
        {
            if (x > (_rows - 1))
            {
                throw new ArgumentOutOfRangeException("x");
            }
            if (y > (_columns - 1))
            {
                throw new ArgumentOutOfRangeException("y");
            }

            _matrix[x][y] = val;
        }

        public T Get(int x, int y)
        {
            if (x > (_rows - 1))
            {
                throw new ArgumentOutOfRangeException("x");
            }
            if (y > (_columns - 1))
            {
                throw new ArgumentOutOfRangeException("y");
            }

            return _matrix[x][y];
        }

        public void Clear()
        {
            _matrix.Clear();
            _rows = 0;
            _columns = 0;
        }

        public void Resize(int rows, int columns, bool preserve = false)
        {
            _matrix.Resize(rows, columns);
            _rows = rows;
            _columns = columns;
        }

        public void SetLengthAtLeast(int rows, int columns)
        {
            if (rows > 0 && columns > 0)
            {
                if (_rows < rows || _columns < columns)
                {
                    Resize(rows, columns);
                }
            }
        }

        public T this[int x, int y]
        {
            get { return Get(x, y); }
            set { Add(x, y, value); }
        }

        public int Rows 
        {
            get { return _rows; }
        }

        public int Columns
        {
            get { return _columns; }
        }

        public Matrix<T> Clone()
        {
            return new Matrix<T>(_rows, _columns, _matrix.Clone());
        }

        public T[,] ToArray()
        {
            T[,] arr = new T[_rows, _columns];

            
            for (int i = 0; i < _rows; i++)
            {
                for (int j = 0; j < _columns; j++)
                {
                    arr[i, j] = Get(i, j);
                }
            }

            return arr;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < _rows; i++)
            {
                for (int j = 0; j < _columns; j++)
                {
                    sb.Append(Get(i, j));
                    if (j != _columns - 1) sb.Append(", ");
                }
                if (i != _rows - 1) sb.Append("; ");
            }   
        
            return sb.ToString();
        }

        public bool IsSquare
        {
            get
            {
                return (_rows == _columns);
            }
        }

        public static FluxList<double> SolveLinearEquations(Matrix<double> matrix, IList<double> rhs, ConcurrentSimplePool<Matrix<double>> matrixPool, ConcurrentSimplePool<FluxList<double>> listPool)
        {
            int dim = rhs.Count;
            FluxList<double> solution = null;
            try
            {

                double deter = Determinant(matrix, matrixPool);
                if (matrix.IsSquare && (int)deter != 0)
                {
                    Matrix<double> con = matrixPool.Take();
                    con.Resize(dim, 1);
                    for (int i = 0; i < dim; i++)
                    {
                        con[i, 0] = rhs[i];
                    }

                    Matrix<double> inverse = Inverse(matrix, deter, matrixPool);

                    Matrix<double> sol = Multiply(inverse, con, matrixPool);
                    
                    matrixPool.Return(con);
                    matrixPool.Return(inverse);

                    solution = listPool.Take();
                    solution.Resize(dim);

                    for (int i = 0; i < dim; i++)
                    {
                        solution[i] = sol[i, 0];
                    }
                    matrixPool.Return(sol);
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventId.Utilities.Matrix.LinearEquation, "Error in solving linear equations", ex);
            }

            return solution;
        }

        private static double Determinant(Matrix<double> matrix, ConcurrentSimplePool<Matrix<double>> matrixPool)
        {
            if (!matrix.IsSquare)
            {
                throw new ArgumentException("Matrix is not sqaure", "matrix");
            }

            if (matrix.Rows == 0)
            {
                throw new ArgumentException("Dimesion of the Matrix 0X0", "matrix");
            }

            double det = 1;

            
            if (matrix.Rows == 2)
            {
                det = ((matrix[0, 0] * matrix[1, 1]) - (matrix[0, 1] * matrix[1, 0]));
            }
            else
            {
                Matrix<double> clone = Clone(matrix, matrixPool);

                if (clone[0, 0] == 0)
                {
                    int i = 1;
                    while (i < clone.Rows)
                    {
                        if (clone[i, 0] != 0)
                        {
                            clone.InterchangeRow(0, i);		
                            det *= -1;
                            break;
                        }
                        i++;
                    }

                }

                // if all the elements in a row or column of matrix are 0, determient is equal to 0
                if (clone[0, 0] == 0)
                {
                    det = 0;							
                }
                else
                {
                    det *= clone[0, 0];
                    RowDivide(clone, 0, clone[0, 0]);
                    for (int p = 1; p < clone.Rows; p++)
                    {
                        int q = 0;
                        
                        // prepare an upper triangular matrix
                        while (q < p)							
                        {
                            RowSubtract(clone, p, q, clone[p, q]);
                            q++;
                        }
                        
                        // dividing the entire row with non zero diagonal element. Multiplying det with that factor.	
                        if (clone[p, p] != 0)
                        {
                            det *= clone[p, p];
                            RowDivide(clone, p, clone[p, p]);
                        }

                        // check if the diagonal elements are zeros
                        if (clone[p, p] == 0)						
                        {
                            for (int j = p + 1; j < clone.Columns; j++)
                            {
                                if (clone[p, j] != 0)
                                {
                                    // adding of columns does not change the determinent
                                    ColumnSubtract(clone, p, j, -1);

                                    det *= clone[p, p];
                                    RowDivide(clone, p, clone[p, p]);
                                    break;
                                }
                            }

                        }

                        // if diagonal element is still zero, Determinent is zero.
                        if (clone[p, p] == 0)						
                        {
                            det = 0;
                        }
                    }
                }

                matrixPool.Return(clone);
            }

            return det;
        }

        private static Matrix<double> Clone(Matrix<double> matrix, ConcurrentSimplePool<Matrix<double>> matrixPool)
        {
            Matrix<double> clone = matrixPool.Take();
            clone.Resize(matrix.Rows, matrix.Columns);
            for (int i = 0; i < matrix.Rows; i++)
            {
                for (int j = 0; j < matrix.Columns; j++)
                {
                    clone[i, j] = matrix[i, j];
                }
            }
            return clone;
        }

        private void InterchangeRow(int i, int j)
        {
            T temp = default(T);
            for (int q = 0; q < _columns; q++)
            {
                temp = this[i, q];
                this[i, q] = this[j, q];
                this[j, q] = temp;
            }
        }

        private static void RowDivide(Matrix<double> matrix, int row, double value)
        {   
            for (int col = 0; col < matrix.Columns; col++)
            {
                matrix[row, col] = matrix[row, col] / value;
            }
        }

        private static void RowSubtract(Matrix<double> matrix, int row1, int row2, double value)
        {
            for (int col = 0; col < matrix.Columns; col++)
            {
                matrix[row1, col] = matrix[row1, col] - (value * matrix[row2, col]);
            }
        }

        private static void ColumnSubtract(Matrix<double> matrix, int col1, int col2, double value)
        {
            for (int row = 0; row < matrix.Rows; row++)
            {
                matrix[row, col1] = matrix[row, col1] - (value * matrix[row, col2]);
            }

        }

        private static Matrix<double> Inverse(Matrix<double> matrix, double deter, ConcurrentSimplePool<Matrix<double>> matrixPool)
        {
            Matrix<double> inverse = matrixPool.Take();
            inverse.Resize(matrix.Rows, matrix.Columns);

            for (int p = 0; p < matrix.Rows; p++)
            {
                for (int q = 0; q < matrix.Columns; q++)
                {
                    Matrix<double> sub = SubMatrixExtraction(matrix, p, q, matrixPool);
                    double ds = Determinant(sub, matrixPool);
                    matrixPool.Return(sub);
                    inverse[p, q] = Math.Pow(-1, p + q + 2) * ds / deter;
                }
            }

            Matrix<double> transposed = Transpose(inverse, matrixPool);
            matrixPool.Return(inverse);

            return transposed;
        }

        private static Matrix<double> Transpose(Matrix<double> matrix, ConcurrentSimplePool<Matrix<double>> matrixPool)
        {

            Matrix<double> transposed = matrixPool.Take();
            transposed.Resize(matrix.Rows, matrix.Columns);

            for (int p = 0; p < matrix.Rows; p++)
            {
                for (int q = 0; q < matrix.Columns; q++)
                {
                    transposed[q, p] = matrix[p, q];
                }

            }

            return transposed;

        }

        private static Matrix<double> SubMatrixExtraction(Matrix<double> matrix, int row, int col, ConcurrentSimplePool<Matrix<double>> matrixPool)
        {
            Matrix<double> sub = matrixPool.Take();
            sub.Resize(matrix.Rows - 1, matrix.Columns - 1);
            
            int i = 0;
            for (int p = 0; p < matrix.Rows; p++)
            {
                int j = 0;
                if (p != row)
                {
                    for (int q = 0; q < matrix.Columns; q++)
                    {
                        if (q != col)
                        {
                            sub[i, j] = matrix[p, q];
                            j += 1;
                        }
                    }
                    i += 1;
                }
            }

            return sub;
        }

        private static Matrix<double> Multiply(Matrix<double> m1, Matrix<double> m2, ConcurrentSimplePool<Matrix<double>> matrixPool)
        {
            if (m1.Columns != m2.Rows)
            {
                throw new Exception("Matrix dimensions do not agree");
            }
            
            Matrix<double> result = matrixPool.Take();
            result.Resize(m1.Rows, m2.Columns);
            for (int i = 0; i < m1.Rows; i++)
            {
                for (int j = 0; j < m2.Columns; j++)
                {
                    for (int k = 0; k < m2.Rows; k++)
                    {
                        result[i, j] = result[i, j] + (m1[i, k] * m2[k, j]);
                    }
                }
            }

            return result;
        }
  
    }
}
