﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

using Aws.Core.Utilities;

namespace LxProcessor.Common
{
    public class ConcurrentSortedDictionary<TKey, TValue> : IDisposable
    {
        private readonly SortedDictionary<TKey, Tuple<TValue, object>> _dictionary;
        private readonly ReaderWriterLockSlim _lock;
        private readonly TimeSpan _lockTimeout;
        private const int LockTimeoutSeconds = 1;

        public ConcurrentSortedDictionary()
        {
            _dictionary = new SortedDictionary<TKey, Tuple<TValue, object>>();
            _lock = new ReaderWriterLockSlim();
            _lockTimeout = TimeSpan.FromSeconds(LockTimeoutSeconds);
        }

        public bool TryRemoveFirst(out TValue value)
        {
            bool success = false;
            value = default(TValue);
            if (_lock.TryEnterUpgradeableReadLock(_lockTimeout))
            {
                try
                {
                    if (_dictionary.Count > 0)
                    {
                        KeyValuePair<TKey, Tuple<TValue, object>> kv = _dictionary.First();
                        value = kv.Value.Item1;
                        if (_lock.TryEnterWriteLock(_lockTimeout))
                        {
                            try
                            {
                                success = _dictionary.Remove(kv.Key);
                            }
                            finally
                            {
                                _lock.ExitWriteLock();
                            }
                        }
                    }
                }
                finally
                {
                    _lock.ExitUpgradeableReadLock();
                }
            }

            return success;
        }

        public TValue RemoveFirst()
        {
            TValue value = default(TValue);
            
            if (_dictionary.Count > 0)
            {
                _lock.EnterWriteLock();
                try
                {
                    KeyValuePair<TKey, Tuple<TValue, object>> kv = _dictionary.First();
                    value = kv.Value.Item1;
                    _dictionary.Remove(kv.Key);
                }
                finally
                {
                    _lock.ExitWriteLock();
                }
            }

            return value;
        }

        public int Count
        {
            get
            {
                int count = -1;
                if (_lock.TryEnterReadLock(_lockTimeout))
                {
                    try
                    {
                        count = _dictionary.Count;
                    }
                    finally
                    {
                        _lock.ExitReadLock();
                    }
                }
                return count;
            }
        }

        public TValue AddOrUpdate(TKey key, TValue addValue, Func<TKey, TValue, TValue, TValue> updateValueFactory)
        {
            TValue valueAdded;
            _lock.EnterWriteLock();
            try
            {
                if (!_dictionary.ContainsKey(key))
                {
                    Tuple<TValue, object> tup = new Tuple<TValue, object>(addValue, new object());
                    _dictionary.Add(key, tup);
                    valueAdded = addValue;
                }
                else
                {
                    valueAdded = updateValueFactory(key, addValue, _dictionary[key].Item1);
                    Tuple<TValue, object> tup = new Tuple<TValue, object>(valueAdded, new object());
                    _dictionary[key] = tup;
                }
            }
            finally
            {
                _lock.ExitWriteLock();
            }

            return valueAdded;
        }

        public void AddOrUpdateBetween(TKey key, TValue addValue, TKey lowerValue, TKey upperValue, Func<TKey, TValue, IEnumerable<KeyValuePair<TKey, Tuple<TValue, object>>>, bool> updateValueFactory)
        {
            Tuple<TValue, object> tup = null;
            IEnumerable<KeyValuePair<TKey, Tuple<TValue, object>>> between  = null;    
            _lock.EnterReadLock();
            try
            {
                // Get the reference to the records between lowerValue, upperValue
                //between = GetDictionaryBetween(lowerValue, upperValue);

                between = GetBetween(lowerValue, upperValue);
                if (between != null)
                {
                    bool wasMerged = updateValueFactory(key, addValue, between);
                    //this indicates the values were not merged and we need to add a new row
                    if (!wasMerged)
                    {
                        tup = new Tuple<TValue, object>(addValue, new object());
                    }
                }
                else
                {
                    tup = new Tuple<TValue, object>(addValue, new object());
                }
            }
            finally
            {
                _lock.ExitReadLock();
            }

            if (tup != null)
            {
                _lock.EnterWriteLock();
                bool didAdd = false;
                try
                {
                    if (!_dictionary.ContainsKey(key))
                    {
                        try
                        {
                            _dictionary.Add(key, tup);
                            didAdd = true;
                        }
                        catch (Exception ex)
                        {
                            EventManager.LogWarning(999, "failure adding new row", ex);
                        }
                    }
                }
                finally
                {
                    _lock.ExitWriteLock();
                }

                //collision in trying to add the item, re-attempt
                if (!didAdd)
                {
                    AddOrUpdateBetween(key, addValue, lowerValue, upperValue, updateValueFactory);
                }
            }
        }

        private SortedDictionary<TKey, Tuple<TValue, object>> GetDictionaryBetween(TKey lowKey, TKey upperKey)
        {
            SortedDictionary<TKey, Tuple<TValue, object>> result = new SortedDictionary<TKey, Tuple<TValue, object>>();

            if (_dictionary.Keys.Count > 0)
            {
                //TKey[] keys = new TKey[_dictionary.Count];
                //_dictionary.Keys.CopyTo(keys, 0);
                //TKey foundKey = BinarySearch(keys, lowKey, upperKey, 0, keys.Length - 1);
                
                IComparer<TKey> comparer = _dictionary.Comparer;

                IEnumerable<TKey> betweenKeys = _dictionary.Keys.Where(key =>
                {
                    return (comparer.Compare(key, lowKey) >= 0 && comparer.Compare(key, upperKey) <= 0);
                });


                foreach (TKey key in betweenKeys)
                {
                    if (_dictionary.ContainsKey(key))
                    {
                        result.Add(key, _dictionary[key]);
                    }
                }
            }

            return result;
        }

        private SortedDictionary<TKey, Tuple<TValue, object>> GetBetween(TKey lower, TKey upper)
        {
            SortedDictionary<TKey, Tuple<TValue, object>> result = null;

            if (_dictionary != null && _dictionary.Count > 0)
            {
                result  = new SortedDictionary<TKey, Tuple<TValue, object>>();

                List<TKey> keys = new List<TKey>(_dictionary.Keys);
                IComparer<TKey> comparer = _dictionary.Comparer;
                int low = 0;
                int high = keys.Count - 1;
                int pivot = 0;

                while (low <= high)
                {
                    pivot = low + ((high - low) / 2);

                    int compare =  comparer.Compare(keys[pivot], lower);

                    if (compare == 0)
                    {
                        break;
                    }

                    if (compare < 0)
                    {
                        low = pivot + 1;
                    }
                    else
                    {
                        high = pivot - 1;
                    }
                }

                while (pivot < keys.Count)
                {
                    int compare = comparer.Compare(keys[pivot], upper);
                    if (comparer.Compare(keys[pivot], lower) >= 0 && compare <= 0)
                    {
                        result.Add(keys[pivot], _dictionary[keys[pivot]]);
                    }

                    if (compare >= 0)
                    {
                        break;
                    }
                    else
                    {
                        pivot++;
                    }
                }
            }

            return result;
        }

        public void Dispose()
        {
            _lock.Dispose();
        }

       
    }
}
