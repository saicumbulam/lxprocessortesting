﻿using System;

namespace LxProcessor.Common
{
    public class SyncronizedDateTime
    {
        private DateTime _dt;
        private object _sync;
        public SyncronizedDateTime(DateTime dt)
        {
            _dt = dt;
            _sync = new object();
        }

        public bool ReplaceIfEarlier(DateTime val)
        {
            bool didReplace = false;
            lock(_sync)
            {
                if (DateTime.Compare(val, _dt) < 0)
                {
                    _dt = val;
                    didReplace = true;
                }
            }
            return didReplace;
        }

        public bool ReplaceIfLater(DateTime val)
        {
            bool didReplace = false;
            lock (_sync)
            {
                if (DateTime.Compare(val, _dt) > 0)
                {
                    _dt = val;
                    didReplace = true;
                }
            }
            return didReplace;
        }

        public DateTime DateTimeValue
        {
            get 
            {
                DateTime val;
                lock(_sync)
                {
                    val = _dt;
                }
                return val;
            }
        }

        public void Replace(DateTime val)
        {
            lock (_sync)
            {
                _dt = val;
            }
        }
    }
}
