﻿using System;
using System.Collections.Generic;
using LxCommon.DataStructures;
using LxCommon.Utilities;
using LxProcessor.Common;

namespace LxProcessor.Optimizer
{
    public class Utilities
    {
        /*************************************************************************
        This subroutine is used to prepare threshold value which will be used for
        trimming of the target function (see comments on TrimFunction() for more
        information).

        This function accepts only one parameter: function value at the starting
        point. It returns threshold which will be used for trimming.

          -- ALGLIB --
             Copyright 10.05.2011 by Bochkanov Sergey
        *************************************************************************/
        public static void TrimPrepare(double f,
            ref double threshold)
        {
            threshold = 0;

            threshold = 10 * (Math.Abs(f) + 1);
        }


        /*************************************************************************
        This subroutine is used to "trim" target function, i.e. to do following
        transformation:

                           { {F,G}          if F<Threshold
            {F_tr, G_tr} = {
                           { {Threshold, 0} if F>=Threshold
                           
        Such transformation allows us to  solve  problems  with  singularities  by
        redefining function in such way that it becomes bounded from above.

          -- ALGLIB --
             Copyright 10.05.2011 by Bochkanov Sergey
        *************************************************************************/
        public static void TrimFunction(ref double f,
            ref FluxList<double> g,
            int n,
            double threshold)
        {
            int i = 0;

            if (f >= threshold)
            {
                f = threshold;
                for (i = 0; i <= n - 1; i++)
                {
                    g[i] = 0.0;
                }
            }
        }


        /*************************************************************************
        This function enforces boundary constraints in the X.

        This function correctly (although a bit inefficient) handles BL[i] which
        are -INF and BU[i] which are +INF.

        We have NMain+NSlack  dimensional  X,  with first NMain components bounded
        by BL/BU, and next NSlack ones bounded by non-negativity constraints.

        INPUT PARAMETERS
            X       -   array[NMain+NSlack], point
            BL      -   array[NMain], lower bounds
                        (may contain -INF, when bound is not present)
            HaveBL  -   array[NMain], if HaveBL[i] is False,
                        then i-th bound is not present
            BU      -   array[NMain], upper bounds
                        (may contain +INF, when bound is not present)
            HaveBU  -   array[NMain], if HaveBU[i] is False,
                        then i-th bound is not present

        OUTPUT PARAMETERS
            X       -   X with all constraints being enforced

        It returns True when constraints are consistent,
        False - when constraints are inconsistent.

          -- ALGLIB --
             Copyright 10.01.2012 by Bochkanov Sergey
        *************************************************************************/
        public static bool EnforceBoundaryConstraints(ref FluxList<double> x,
            FluxList<double> bl,
            FluxList<bool> havebl,
            FluxList<double> bu,
            FluxList<bool> havebu,
            int nmain,
            int nslack)
        {
            bool result = new bool();
            int i = 0;

            result = false;
            for (i = 0; i <= nmain - 1; i++)
            {
                if ((havebl[i] && havebu[i]) && bl[i] > bu[i])
                {
                    return result;
                }
                if (havebl[i] && x[i] < bl[i])
                {
                    x[i] = bl[i];
                }
                if (havebu[i] && x[i] > bu[i])
                {
                    x[i] = bu[i];
                }
            }
            for (i = 0; i <= nslack - 1; i++)
            {
                if (x[nmain + i] < 0)
                {
                    x[nmain + i] = 0;
                }
            }
            result = true;
            return result;
        }


        /*************************************************************************
        This function projects gradient into feasible area of boundary constrained
        optimization  problem.  X  can  be  infeasible  with  respect  to boundary
        constraints.  We  have  NMain+NSlack  dimensional  X,   with  first  NMain 
        components bounded by BL/BU, and next NSlack ones bounded by non-negativity
        constraints.

        INPUT PARAMETERS
            X       -   array[NMain+NSlack], point
            G       -   array[NMain+NSlack], gradient
            BL      -   lower bounds (may contain -INF, when bound is not present)
            HaveBL  -   if HaveBL[i] is False, then i-th bound is not present
            BU      -   upper bounds (may contain +INF, when bound is not present)
            HaveBU  -   if HaveBU[i] is False, then i-th bound is not present

        OUTPUT PARAMETERS
            G       -   projection of G. Components of G which satisfy one of the
                        following
                            (1) (X[I]<=BndL[I]) and (G[I]>0), OR
                            (2) (X[I]>=BndU[I]) and (G[I]<0)
                        are replaced by zeros.

        NOTE 1: this function assumes that constraints are feasible. It throws
        exception otherwise.

        NOTE 2: in fact, projection of ANTI-gradient is calculated,  because  this
        function trims components of -G which points outside of the feasible area.
        However, working with -G is considered confusing, because all optimization
        source work with G.

          -- ALGLIB --
             Copyright 10.01.2012 by Bochkanov Sergey
        *************************************************************************/

        private static void ProjectGradientIntoBoundaryConstraints(FluxList<double> x,
            ref double[] g,
            FluxList<double> bl,
            FluxList<bool> havebl,
            FluxList<double> bu,
            FluxList<bool> havebu,
            int nmain,
            int nslack)
        {
            int i = 0;

            for (i = 0; i <= nmain - 1; i++)
            {
                Assert((!havebl[i] || !havebu[i]) || bl[i] <= bu[i], "ProjectGradientIntoBC: internal error (infeasible constraints)");
                if ((havebl[i] && x[i] <= bl[i]) && g[i] > 0)
                {
                    g[i] = 0;
                }
                if ((havebu[i] && x[i] >= bu[i]) && g[i] < 0)
                {
                    g[i] = 0;
                }
            }
            for (i = 0; i <= nslack - 1; i++)
            {
                if (x[nmain + i] <= 0 && g[nmain + i] > 0)
                {
                    g[nmain + i] = 0;
                }
            }
        }


        /*************************************************************************
        Given
            a) initial point X0[NMain+NSlack]
               (feasible with respect to bound constraints)
            b) step vector alpha*D[NMain+NSlack]
            c) boundary constraints BndL[NMain], BndU[NMain]
            d) implicit non-negativity constraints for slack variables
        this  function  calculates  bound  on  the step length subject to boundary
        constraints.

        It returns:
            *  MaxStepLen - such step length that X0+MaxStepLen*alpha*D is exactly
               at the boundary given by constraints
            *  VariableToFreeze - index of the constraint to be activated,
               0 <= VariableToFreeze < NMain+NSlack
            *  ValueToFreeze - value of the corresponding constraint.

        Notes:
            * it is possible that several constraints can be activated by the step
              at once. In such cases only one constraint is returned. It is caller
              responsibility to check other constraints. This function makes  sure
              that we activate at least one constraint, and everything else is the
              responsibility of the caller.
            * steps smaller than MaxStepLen still can activate constraints due  to
              numerical errors. Thus purpose of this  function  is  not  to  guard 
              against accidental activation of the constraints - quite the reverse, 
              its purpose is to activate at least constraint upon performing  step
              which is too long.
            * in case there is no constraints to activate, we return negative
              VariableToFreeze and zero MaxStepLen and ValueToFreeze.
            * this function assumes that constraints are consistent; it throws
              exception otherwise.

        INPUT PARAMETERS
            X           -   array[NMain+NSlack], point. Must be feasible with respect 
                            to bound constraints (exception will be thrown otherwise)
            D           -   array[NMain+NSlack], step direction
            alpha       -   scalar multiplier before D, alpha<>0
            BndL        -   lower bounds, array[NMain]
                            (may contain -INF, when bound is not present)
            HaveBndL    -   array[NMain], if HaveBndL[i] is False,
                            then i-th bound is not present
            BndU        -   array[NMain], upper bounds
                            (may contain +INF, when bound is not present)
            HaveBndU    -   array[NMain], if HaveBndU[i] is False,
                            then i-th bound is not present
            NMain       -   number of main variables
            NSlack      -   number of slack variables
            
        OUTPUT PARAMETERS
            VariableToFreeze:
                            * negative value     = step is unbounded, ValueToFreeze=0,
                                                   MaxStepLen=0.
                            * non-negative value = at least one constraint, given by
                                                   this parameter, will  be  activated
                                                   upon performing maximum step.
            ValueToFreeze-  value of the variable which will be constrained
            MaxStepLen  -   maximum length of the step. Can be zero when step vector
                            looks outside of the feasible area.

          -- ALGLIB --
             Copyright 10.01.2012 by Bochkanov Sergey
        *************************************************************************/

        private static void CalculateStepBound(FluxList<double> x,
            double[] d,
            double alpha,
            FluxList<double> bndl,
            FluxList<bool> havebndl,
            FluxList<double> bndu,
            FluxList<bool> havebndu,
            int nmain,
            int nslack,
            ref int variabletofreeze,
            ref double valuetofreeze,
            ref double maxsteplen)
        {
            int i = 0;
            double prevmax = 0;
            double initval = 0;

            variabletofreeze = 0;
            valuetofreeze = 0;
            maxsteplen = 0;

            Assert(alpha != 0, "CalculateStepBound: zero alpha");
            variabletofreeze = -1;
            initval = MathExtensions.MaxRealNumber;
            maxsteplen = initval;
            for (i = 0; i <= nmain - 1; i++)
            {
                if (havebndl[i] && alpha * d[i] < 0)
                {
                    Assert(x[i] >= bndl[i], "CalculateStepBound: infeasible X");
                    prevmax = maxsteplen;
                    maxsteplen = SafeMin(x[i] - bndl[i], -(alpha * d[i]), maxsteplen);
                    if (maxsteplen < prevmax)
                    {
                        variabletofreeze = i;
                        valuetofreeze = bndl[i];
                    }
                }
                if (havebndu[i] && alpha * d[i] > 0)
                {
                    Assert(x[i] <= bndu[i], "CalculateStepBound: infeasible X");
                    prevmax = maxsteplen;
                    maxsteplen = SafeMin(bndu[i] - x[i], alpha * d[i], maxsteplen);
                    if (maxsteplen < prevmax)
                    {
                        variabletofreeze = i;
                        valuetofreeze = bndu[i];
                    }
                }
            }
            for (i = 0; i <= nslack - 1; i++)
            {
                if (alpha * d[nmain + i] < 0)
                {
                    Assert(x[nmain + i] >= 0, "CalculateStepBound: infeasible X");
                    prevmax = maxsteplen;
                    maxsteplen = SafeMin(x[nmain + i], -(alpha * d[nmain + i]), maxsteplen);
                    if (maxsteplen < prevmax)
                    {
                        variabletofreeze = nmain + i;
                        valuetofreeze = 0;
                    }
                }
            }
            if (maxsteplen == initval)
            {
                valuetofreeze = 0;
                maxsteplen = 0;
            }
        }


        /*************************************************************************
        This function postprocesses bounded step by:
        * analysing step length (whether it is equal to MaxStepLen) and activating 
          constraint given by VariableToFreeze if needed
        * checking for additional bound constraints to activate

        This function uses final point of the step, quantities calculated  by  the
        CalculateStepBound()  function.  As  result,  it  returns  point  which is 
        exactly feasible with respect to boundary constraints.

        NOTE 1: this function does NOT handle and check linear equality constraints
        NOTE 2: when StepTaken=MaxStepLen we always activate at least one constraint

        INPUT PARAMETERS
            X           -   array[NMain+NSlack], final point to postprocess
            XPrev       -   array[NMain+NSlack], initial point
            BndL        -   lower bounds, array[NMain]
                            (may contain -INF, when bound is not present)
            HaveBndL    -   array[NMain], if HaveBndL[i] is False,
                            then i-th bound is not present
            BndU        -   array[NMain], upper bounds
                            (may contain +INF, when bound is not present)
            HaveBndU    -   array[NMain], if HaveBndU[i] is False,
                            then i-th bound is not present
            NMain       -   number of main variables
            NSlack      -   number of slack variables
            VariableToFreeze-result of CalculateStepBound()
            ValueToFreeze-  result of CalculateStepBound()
            StepTaken   -   actual step length (actual step is equal to the possibly 
                            non-unit step direction vector times this parameter).
                            StepTaken<=MaxStepLen.
            MaxStepLen  -   result of CalculateStepBound()
            
        OUTPUT PARAMETERS
            X           -   point bounded with respect to constraints.
                            components corresponding to active constraints are exactly
                            equal to the boundary values.
                            
        RESULT:
            number of constraints activated in addition to previously active ones.
            Constraints which were DEACTIVATED are ignored (do not influence
            function value).

          -- ALGLIB --
             Copyright 10.01.2012 by Bochkanov Sergey
        *************************************************************************/

        private static int PostProcessBoundedStep(ref double[] x,
            FluxList<double> xprev,
            FluxList<double> bndl,
            FluxList<bool> havebndl,
            FluxList<double> bndu,
            FluxList<bool> havebndu,
            int nmain,
            int nslack,
            int variabletofreeze,
            double valuetofreeze,
            double steptaken,
            double maxsteplen)
        {
            int result = 0;
            int i = 0;
            bool wasactivated = new bool();

            Assert(variabletofreeze < 0 || steptaken <= maxsteplen);

            //
            // Activate constraints
            //
            if (variabletofreeze >= 0 && steptaken == maxsteplen)
            {
                x[variabletofreeze] = valuetofreeze;
            }
            for (i = 0; i <= nmain - 1; i++)
            {
                if (havebndl[i] && x[i] < bndl[i])
                {
                    x[i] = bndl[i];
                }
                if (havebndu[i] && x[i] > bndu[i])
                {
                    x[i] = bndu[i];
                }
            }
            for (i = 0; i <= nslack - 1; i++)
            {
                if (x[nmain + i] <= 0)
                {
                    x[nmain + i] = 0;
                }
            }

            //
            // Calculate number of constraints being activated
            //
            result = 0;
            for (i = 0; i <= nmain - 1; i++)
            {
                wasactivated = x[i] != xprev[i] && ((havebndl[i] && x[i] == bndl[i]) || (havebndu[i] && x[i] == bndu[i]));
                wasactivated = wasactivated || variabletofreeze == i;
                if (wasactivated)
                {
                    result = result + 1;
                }
            }
            for (i = 0; i <= nslack - 1; i++)
            {
                wasactivated = x[nmain + i] != xprev[nmain + i] && x[nmain + i] == 0.0;
                wasactivated = wasactivated || variabletofreeze == nmain + i;
                if (wasactivated)
                {
                    result = result + 1;
                }
            }
            return result;
        }


        /*************************************************************************
        The  purpose  of  this  function is to prevent algorithm from "unsticking" 
        from  the  active  bound  constraints  because  of  numerical noise in the
        gradient or Hessian.

        It is done by zeroing some components of the search direction D.  D[i]  is
        zeroed when both (a) and (b) are true:
        a) corresponding X[i] is exactly at the boundary
        b) |D[i]*S[i]| <= DropTol*Sqrt(SUM(D[i]^2*S[I]^2))

        D  can  be  step  direction , antigradient, gradient, or anything similar. 
        Sign of D does not matter, nor matters step length.

        NOTE 1: boundary constraints are expected to be consistent, as well as X
                is expected to be feasible. Exception will be thrown otherwise.

        INPUT PARAMETERS
            D           -   array[NMain+NSlack], direction
            X           -   array[NMain+NSlack], current point
            BndL        -   lower bounds, array[NMain]
                            (may contain -INF, when bound is not present)
            HaveBndL    -   array[NMain], if HaveBndL[i] is False,
                            then i-th bound is not present
            BndU        -   array[NMain], upper bounds
                            (may contain +INF, when bound is not present)
            HaveBndU    -   array[NMain], if HaveBndU[i] is False,
                            then i-th bound is not present
            S           -   array[NMain+NSlack], scaling of the variables
            NMain       -   number of main variables
            NSlack      -   number of slack variables
            DropTol     -   drop tolerance, >=0
            
        OUTPUT PARAMETERS
            X           -   point bounded with respect to constraints.
                            components corresponding to active constraints are exactly
                            equal to the boundary values.

          -- ALGLIB --
             Copyright 10.01.2012 by Bochkanov Sergey
        *************************************************************************/

        private static void FilterDirection(ref double[] d,
            FluxList<double> x,
            FluxList<double> bndl,
            FluxList<bool> havebndl,
            FluxList<double> bndu,
            FluxList<bool> havebndu,
            double[] s,
            int nmain,
            int nslack,
            double droptol)
        {
            int i = 0;
            double scalednorm = 0;
            bool isactive = new bool();

            scalednorm = 0.0;
            for (i = 0; i <= nmain + nslack - 1; i++)
            {
                scalednorm = scalednorm + MathExtensions.Squared(d[i] * s[i]);
            }
            scalednorm = Math.Sqrt(scalednorm);
            for (i = 0; i <= nmain - 1; i++)
            {
                Assert(!havebndl[i] || x[i] >= bndl[i], "FilterDirection: infeasible point");
                Assert(!havebndu[i] || x[i] <= bndu[i], "FilterDirection: infeasible point");
                isactive = (havebndl[i] && x[i] == bndl[i]) || (havebndu[i] && x[i] == bndu[i]);
                if (isactive && Math.Abs(d[i] * s[i]) <= droptol * scalednorm)
                {
                    d[i] = 0.0;
                }
            }
            for (i = 0; i <= nslack - 1; i++)
            {
                Assert(x[nmain + i] >= 0, "FilterDirection: infeasible point");
                if (x[nmain + i] == 0 && Math.Abs(d[nmain + i] * s[nmain + i]) <= droptol * scalednorm)
                {
                    d[nmain + i] = 0.0;
                }
            }
        }


        /*************************************************************************
        This function returns number of bound constraints whose state was  changed
        (either activated or deactivated) when making step from XPrev to X.

        Constraints are considered:
        * active - when we are exactly at the boundary
        * inactive - when we are not at the boundary

        You should note that antigradient direction is NOT taken into account when
        we make decions on the constraint status.

        INPUT PARAMETERS
            X           -   array[NMain+NSlack], final point.
                            Must be feasible with respect to bound constraints.
            XPrev       -   array[NMain+NSlack], initial point.
                            Must be feasible with respect to bound constraints.
            BndL        -   lower bounds, array[NMain]
                            (may contain -INF, when bound is not present)
            HaveBndL    -   array[NMain], if HaveBndL[i] is False,
                            then i-th bound is not present
            BndU        -   array[NMain], upper bounds
                            (may contain +INF, when bound is not present)
            HaveBndU    -   array[NMain], if HaveBndU[i] is False,
                            then i-th bound is not present
            NMain       -   number of main variables
            NSlack      -   number of slack variables
            
        RESULT:
            number of constraints whose state was changed.

          -- ALGLIB --
             Copyright 10.01.2012 by Bochkanov Sergey
        *************************************************************************/

        private static int NumberOfChangedConstraints(double[] x,
            FluxList<double> xprev,
            FluxList<double> bndl,
            FluxList<bool> havebndl,
            FluxList<double> bndu,
            FluxList<bool> havebndu,
            int nmain,
            int nslack)
        {
            int result = 0;
            int i = 0;
            bool statuschanged = new bool();

            result = 0;
            for (i = 0; i <= nmain - 1; i++)
            {
                if (x[i] != xprev[i])
                {
                    statuschanged = false;
                    if (havebndl[i] && (x[i] == bndl[i] || xprev[i] == bndl[i]))
                    {
                        statuschanged = true;
                    }
                    if (havebndu[i] && (x[i] == bndu[i] || xprev[i] == bndu[i]))
                    {
                        statuschanged = true;
                    }
                    if (statuschanged)
                    {
                        result = result + 1;
                    }
                }
            }
            for (i = 0; i <= nslack - 1; i++)
            {
                if (x[nmain + i] != xprev[nmain + i] && (x[nmain + i] == 0 || xprev[nmain + i] == 0))
                {
                    result = result + 1;
                }
            }
            return result;
        }


        /*************************************************************************
        This function finds feasible point of  (NMain+NSlack)-dimensional  problem
        subject to NMain explicit boundary constraints (some  constraints  can  be
        omitted), NSlack implicit non-negativity constraints,  K  linear  equality
        constraints.

        INPUT PARAMETERS
            X           -   array[NMain+NSlack], initial point.
            BndL        -   lower bounds, array[NMain]
                            (may contain -INF, when bound is not present)
            HaveBndL    -   array[NMain], if HaveBndL[i] is False,
                            then i-th bound is not present
            BndU        -   array[NMain], upper bounds
                            (may contain +INF, when bound is not present)
            HaveBndU    -   array[NMain], if HaveBndU[i] is False,
                            then i-th bound is not present
            NMain       -   number of main variables
            NSlack      -   number of slack variables
            CE          -   array[K,NMain+NSlack+1], equality  constraints CE*x=b.
                            Rows contain constraints, first  NMain+NSlack  columns
                            contain coefficients before X[], last  column  contain
                            right part.
            K           -   number of linear constraints
            EpsI        -   infeasibility (error in the right part) allowed in the
                            solution

        OUTPUT PARAMETERS:
            X           -   feasible point or best infeasible point found before
                            algorithm termination
            QPIts       -   number of QP iterations (for debug purposes)
            GPAIts      -   number of GPA iterations (for debug purposes)
            
        RESULT:
            True in case X is feasible, False - if it is infeasible.

          -- ALGLIB --
             Copyright 20.01.2012 by Bochkanov Sergey
        *************************************************************************/
       
        public static bool FindFeasiblePoint(ref FluxList<double> x,
            FluxList<double> bndl,
            FluxList<bool> havebndl,
            FluxList<double> bndu,
            FluxList<bool> havebndu,
            int nmain,
            int nslack,
            Matrix<double> ce,
            int k,
            double epsi,
            ref int qpits,
            ref int gpaits)
        {
            bool result = new bool();
            int i = 0;
            int j = 0;
            int idx0 = 0;
            int idx1 = 0;
            double[] permx = new double[0];
            double[] xn = new double[0];
            FluxList<double> xa = new FluxList<double>();
            double[] newtonstep = new double[0];
            double[] g = new double[0];
            double[] pg = new double[0];
            Matrix<double> a = new Matrix<double>(0, 0);
            double armijostep = 0;
            double armijobeststep = 0;
            double armijobestfeas = 0;
            double v = 0;
            double mx = 0;
            double feaserr = 0;
            double feasold = 0;
            double feasnew = 0;
            double pgnorm = 0;
            double vn = 0;
            double vd = 0;
            double stp = 0;
            int vartofreeze = 0;
            double valtofreeze = 0;
            double maxsteplen = 0;
            bool werechangesinconstraints = new bool();
            bool stage1isover = new bool();
            bool converged = new bool();
            double[] activeconstraints = new double[0];
            double[] tmpk = new double[0];
            double[] colnorms = new double[0];
            int nactive = 0;
            int nfree = 0;
            int nsvd = 0;
            int[] p1 = new int[0];
            int[] p2 = new int[0];
            Buffers buf = new Buffers();
            double[] w = new double[0];
            double[] s = new double[0];
            Matrix<double> u = new Matrix<double>(0, 0);
            Matrix<double> vt = new Matrix<double>(0, 0);
            int itscount = 0;
            int itswithintolerance = 0;
            int maxitswithintolerance = 0;
            int gparuns = 0;
            int maxarmijoruns = 0;
            int i_ = 0;

            ce = ce.Clone();
            qpits = 0;
            gpaits = 0;

            maxitswithintolerance = 3;
            maxarmijoruns = 5;
            qpits = 0;
            gpaits = 0;

            //
            // Initial enforcement of the feasibility with respect to boundary constraints
            // NOTE: after this block we assume that boundary constraints are consistent.
            //
            if (!EnforceBoundaryConstraints(ref x, bndl, havebndl, bndu, havebndu, nmain, nslack))
            {
                result = false;
                return result;
            }
            if (k == 0)
            {

                //
                // No linear constraints, we can exit right now
                //
                result = true;
                return result;
            }

            //
            // Scale rows of CE in such way that max(CE[i,0..nmain+nslack-1])=1 for any i=0..k-1
            //
            for (i = 0; i <= k - 1; i++)
            {
                v = 0.0;
                for (j = 0; j <= nmain + nslack - 1; j++)
                {
                    v = Math.Max(v, Math.Abs(ce[i, j]));
                }
                if (v != 0)
                {
                    v = 1 / v;
                    for (i_ = 0; i_ <= nmain + nslack; i_++)
                    {
                        ce[i, i_] = v * ce[i, i_];
                    }
                }
            }

            //
            // Allocate temporaries
            //
            xn = new double[nmain + nslack];
            xa = new FluxList<double>(); 
            xa.Resize(nmain + nslack);
            permx = new double[nmain + nslack];
            g = new double[nmain + nslack];
            pg = new double[nmain + nslack];
            tmpk = new double[k];
            a = new Matrix<double>(k, nmain + nslack);
            activeconstraints = new double[nmain + nslack];
            newtonstep = new double[nmain + nslack];
            s = new double[nmain + nslack];
            colnorms = new double[nmain + nslack];
            for (i = 0; i <= nmain + nslack - 1; i++)
            {
                s[i] = 1.0;
                colnorms[i] = 0.0;
                for (j = 0; j <= k - 1; j++)
                {
                    colnorms[i] = colnorms[i] + MathExtensions.Squared(ce[j, i]);
                }
            }

            //
            // K>0, we have linear equality constraints combined with bound constraints.
            //
            // Try to find feasible point as minimizer of the quadratic function
            //     F(x) = 0.5*||CE*x-b||^2 = 0.5*x'*(CE'*CE)*x - (b'*CE)*x + 0.5*b'*b
            // subject to boundary constraints given by BL, BU and non-negativity of
            // the slack variables. BTW, we drop constant term because it does not
            // actually influences on the solution.
            //
            // Below we will assume that K>0.
            //
            itswithintolerance = 0;
            itscount = 0;
            while (true)
            {

                //
                // Stage 0: check for exact convergence
                //
                converged = true;
                feaserr = 0;
                for (i = 0; i <= k - 1; i++)
                {

                    //
                    // Calculate:
                    // * V - error in the right part
                    // * MX - maximum term in the left part
                    //
                    // Terminate if error in the right part is not greater than 100*Eps*MX.
                    //
                    // IMPORTANT: we must perform check for non-strict inequality, i.e. to use <= instead of <.
                    //            it will allow us to easily handle situations with zero rows of CE.
                    //
                    mx = 0;
                    v = -ce[i, nmain + nslack];
                    for (j = 0; j <= nmain + nslack - 1; j++)
                    {
                        mx = Math.Max(mx, Math.Abs(ce[i, j] * x[j]));
                        v = v + ce[i, j] * x[j];
                    }
                    feaserr = feaserr + MathExtensions.Squared(v);
                    converged = converged && Math.Abs(v) <= 100 * MathExtensions.MachineEpsilon * mx;
                }
                feaserr = Math.Sqrt(feaserr);
                if (converged)
                {
                    result = feaserr <= epsi;
                    return result;
                }

                //
                // Stage 1: equality constrained quadratic programming
                //
                // * treat active bound constraints as equality ones (constraint is considered 
                //   active when we are at the boundary, independently of the antigradient direction)
                // * calculate unrestricted Newton step to point XM (which may be infeasible)
                //   calculate MaxStepLen = largest step in direction of XM which retains feasibility.
                // * perform bounded step from X to XN:
                //   a) XN=XM                  (if XM is feasible)
                //   b) XN=X-MaxStepLen*(XM-X) (otherwise)
                // * X := XN
                // * if XM (Newton step subject to currently active constraints) was feasible, goto Stage 2
                // * repeat Stage 1
                //
                // NOTE 1: in order to solve constrained qudratic subproblem we will have to reorder
                //         variables in such way that ones corresponding to inactive constraints will
                //         be first, and active ones will be last in the list. CE and X are now
                //                                                       [ xi ]
                //         separated into two parts: CE = [CEi CEa], x = [    ], where CEi/Xi correspond
                //                                                       [ xa ]
                //         to INACTIVE constraints, and CEa/Xa correspond to the ACTIVE ones.
                //
                //         Now, instead of F=0.5*x'*(CE'*CE)*x - (b'*CE)*x + 0.5*b'*b, we have
                //         F(xi) = 0.5*(CEi*xi,CEi*xi) + (CEa*xa-b,CEi*xi) + (0.5*CEa*xa-b,CEa*xa).
                //         Here xa is considered constant, i.e. we optimize with respect to xi, leaving xa fixed.
                //
                //         We can solve it by performing SVD of CEi and calculating pseudoinverse of the
                //         Hessian matrix. Of course, we do NOT calculate pseudoinverse explicitly - we
                //         just use singular vectors to perform implicit multiplication by it.
                //
                //
                while (true)
                {

                    //
                    // Calculate G - gradient subject to equality constraints,
                    // multiply it by inverse of the Hessian diagonal to obtain initial
                    // step vector.
                    //
                    // Bound step subject to constraints which can be activated,
                    // run Armijo search with increasing step size.
                    // Search is terminated when feasibility error stops to decrease.
                    //
                    // NOTE: it is important to test for "stops to decrease" instead
                    // of "starts to increase" in order to correctly handle cases with
                    // zero CE.
                    //
                    armijobeststep = 0.0;
                    armijobestfeas = 0.0;
                    for (i = 0; i <= nmain + nslack - 1; i++)
                    {
                        g[i] = 0;
                    }
                    for (i = 0; i <= k - 1; i++)
                    {
                        v = 0.0;
                        for (i_ = 0; i_ <= nmain + nslack - 1; i_++)
                        {
                            v += ce[i, i_] * x[i_];
                        }
                        v = v - ce[i, nmain + nslack];
                        armijobestfeas = armijobestfeas + MathExtensions.Squared(v);
                        for (i_ = 0; i_ <= nmain + nslack - 1; i_++)
                        {
                            g[i_] = g[i_] + v * ce[i, i_];
                        }
                    }
                    armijobestfeas = Math.Sqrt(armijobestfeas);
                    for (i = 0; i <= nmain - 1; i++)
                    {
                        if (havebndl[i] && x[i] == bndl[i])
                        {
                            g[i] = 0.0;
                        }
                        if (havebndu[i] && x[i] == bndu[i])
                        {
                            g[i] = 0.0;
                        }
                    }
                    for (i = 0; i <= nslack - 1; i++)
                    {
                        if (x[nmain + i] == 0.0)
                        {
                            g[nmain + i] = 0.0;
                        }
                    }
                    v = 0.0;
                    for (i = 0; i <= nmain + nslack - 1; i++)
                    {
                        if (MathExtensions.Squared(colnorms[i]) != 0)
                        {
                            newtonstep[i] = -(g[i] / MathExtensions.Squared(colnorms[i]));
                        }
                        else
                        {
                            newtonstep[i] = 0.0;
                        }
                        v = v + MathExtensions.Squared(newtonstep[i]);
                    }
                    if (v == 0)
                    {

                        //
                        // Constrained gradient is zero, QP iterations are over
                        //
                        break;
                    }
                    CalculateStepBound(x, newtonstep, 1.0, bndl, havebndl, bndu, havebndu, nmain, nslack, ref vartofreeze, ref valtofreeze, ref maxsteplen);
                    if (vartofreeze >= 0 && maxsteplen == 0)
                    {

                        //
                        // Can not perform step, QP iterations are over
                        //
                        break;
                    }
                    if (vartofreeze >= 0)
                    {
                        armijostep = Math.Min(1.0, maxsteplen);
                    }
                    else
                    {
                        armijostep = 1;
                    }
                    while (true)
                    {
                        for (i_ = 0; i_ <= nmain + nslack - 1; i_++)
                        {
                            xa[i_] = x[i_];
                        }
                        for (i_ = 0; i_ <= nmain + nslack - 1; i_++)
                        {
                            xa[i_] = xa[i_] + armijostep * newtonstep[i_];
                        }
                        EnforceBoundaryConstraints(ref xa, bndl, havebndl, bndu, havebndu, nmain, nslack);
                        feaserr = 0.0;
                        for (i = 0; i <= k - 1; i++)
                        {
                            v = 0.0;
                            for (i_ = 0; i_ <= nmain + nslack - 1; i_++)
                            {
                                v += ce[i, i_] * xa[i_];
                            }
                            v = v - ce[i, nmain + nslack];
                            feaserr = feaserr + MathExtensions.Squared(v);
                        }
                        feaserr = Math.Sqrt(feaserr);
                        if (feaserr >= armijobestfeas)
                        {
                            break;
                        }
                        armijobestfeas = feaserr;
                        armijobeststep = armijostep;
                        armijostep = 2.0 * armijostep;
                    }
                    for (i_ = 0; i_ <= nmain + nslack - 1; i_++)
                    {
                        x[i_] = x[i_] + armijobeststep * newtonstep[i_];
                    }
                    EnforceBoundaryConstraints(ref x, bndl, havebndl, bndu, havebndu, nmain, nslack);

                    //
                    // Determine number of active and free constraints
                    //
                    nactive = 0;
                    for (i = 0; i <= nmain - 1; i++)
                    {
                        activeconstraints[i] = 0;
                        if (havebndl[i] && x[i] == bndl[i])
                        {
                            activeconstraints[i] = 1;
                        }
                        if (havebndu[i] && x[i] == bndu[i])
                        {
                            activeconstraints[i] = 1;
                        }
                        if (activeconstraints[i] > 0)
                        {
                            nactive = nactive + 1;
                        }
                    }
                    for (i = 0; i <= nslack - 1; i++)
                    {
                        activeconstraints[nmain + i] = 0;
                        if (x[nmain + i] == 0.0)
                        {
                            activeconstraints[nmain + i] = 1;
                        }
                        if (activeconstraints[nmain + i] > 0)
                        {
                            nactive = nactive + 1;
                        }
                    }
                    nfree = nmain + nslack - nactive;
                    if (nfree == 0)
                    {
                        break;
                    }
                    qpits = qpits + 1;

                    //
                    // Reorder variables
                    //
                    TagSort.SortBuffered(ref activeconstraints, nmain + nslack, ref p1, ref p2, buf);
                    for (i = 0; i <= k - 1; i++)
                    {
                        for (j = 0; j <= nmain + nslack - 1; j++)
                        {
                            a[i, j] = ce[i, j];
                        }
                    }
                    for (j = 0; j <= nmain + nslack - 1; j++)
                    {
                        permx[j] = x[j];
                    }
                    for (j = 0; j <= nmain + nslack - 1; j++)
                    {
                        if (p2[j] != j)
                        {
                            idx0 = p2[j];
                            idx1 = j;
                            for (i = 0; i <= k - 1; i++)
                            {
                                v = a[i, idx0];
                                a[i, idx0] = a[i, idx1];
                                a[i, idx1] = v;
                            }
                            v = permx[idx0];
                            permx[idx0] = permx[idx1];
                            permx[idx1] = v;
                        }
                    }

                    //
                    // Calculate (unprojected) gradient:
                    // G(xi) = CEi'*(CEi*xi + CEa*xa - b)
                    //
                    for (i = 0; i <= nfree - 1; i++)
                    {
                        g[i] = 0;
                    }
                    for (i = 0; i <= k - 1; i++)
                    {
                        v = 0.0;
                        for (i_ = 0; i_ <= nmain + nslack - 1; i_++)
                        {
                            v += a[i, i_] * permx[i_];
                        }
                        tmpk[i] = v - ce[i, nmain + nslack];
                    }
                    for (i = 0; i <= k - 1; i++)
                    {
                        v = tmpk[i];
                        for (i_ = 0; i_ <= nfree - 1; i_++)
                        {
                            g[i_] = g[i_] + v * a[i, i_];
                        }
                    }

                    //
                    // Calculate Newton step using SVD of CEi:
                    //     F(xi)  = 0.5*xi'*H*xi + g'*xi    (Taylor decomposition)
                    //     XN     = -H^(-1)*g               (new point, solution of the QP subproblem)
                    //     H      = CEi'*CEi                
                    //     CEi    = U*W*V'                  (SVD of CEi)
                    //     H      = V*W^2*V'                 
                    //     H^(-1) = V*W^(-2)*V'
                    //     step     = -V*W^(-2)*V'*g          (it is better to perform multiplication from right to left)
                    //
                    // NOTE 1: we do NOT need left singular vectors to perform Newton step.
                    //
                    nsvd = Math.Min(k, nfree);
                    if (!SingularValueDecomposition.Calculate(a, k, nfree, 0, 1, 2, ref w, ref u, ref vt))
                    {
                        result = false;
                        return result;
                    }
                    for (i = 0; i <= nsvd - 1; i++)
                    {
                        v = 0.0;
                        for (i_ = 0; i_ <= nfree - 1; i_++)
                        {
                            v += vt[i, i_] * g[i_];
                        }
                        tmpk[i] = v;
                    }
                    for (i = 0; i <= nsvd - 1; i++)
                    {

                        //
                        // It is important to have strict ">" in order to correctly 
                        // handle zero singular values.
                        //
                        if (MathExtensions.Squared(w[i]) > MathExtensions.Squared(w[0]) * (nmain + nslack) * MathExtensions.MachineEpsilon)
                        {
                            tmpk[i] = tmpk[i] / MathExtensions.Squared(w[i]);
                        }
                        else
                        {
                            tmpk[i] = 0;
                        }
                    }
                    for (i = 0; i <= nmain + nslack - 1; i++)
                    {
                        newtonstep[i] = 0;
                    }
                    for (i = 0; i <= nsvd - 1; i++)
                    {
                        v = tmpk[i];
                        for (i_ = 0; i_ <= nfree - 1; i_++)
                        {
                            newtonstep[i_] = newtonstep[i_] - v * vt[i, i_];
                        }
                    }
                    for (j = nmain + nslack - 1; j >= 0; j--)
                    {
                        if (p2[j] != j)
                        {
                            idx0 = p2[j];
                            idx1 = j;
                            v = newtonstep[idx0];
                            newtonstep[idx0] = newtonstep[idx1];
                            newtonstep[idx1] = v;
                        }
                    }

                    //
                    // NewtonStep contains Newton step subject to active bound constraints.
                    //
                    // Such step leads us to the minimizer of the equality constrained F,
                    // but such minimizer may be infeasible because some constraints which
                    // are inactive at the initial point can be violated at the solution.
                    //
                    // Thus, we perform optimization in two stages:
                    // a) perform bounded Newton step, i.e. step in the Newton direction
                    //    until activation of the first constraint
                    // b) in case (MaxStepLen>0)and(MaxStepLen<1), perform additional iteration
                    //    of the Armijo line search in the rest of the Newton direction.
                    //
                    CalculateStepBound(x, newtonstep, 1.0, bndl, havebndl, bndu, havebndu, nmain, nslack, ref vartofreeze, ref valtofreeze, ref maxsteplen);
                    if (vartofreeze >= 0 && maxsteplen == 0)
                    {

                        //
                        // Activation of the constraints prevent us from performing step,
                        // QP iterations are over
                        //
                        break;
                    }
                    if (vartofreeze >= 0)
                    {
                        v = Math.Min(1.0, maxsteplen);
                    }
                    else
                    {
                        v = 1.0;
                    }
                    for (i_ = 0; i_ <= nmain + nslack - 1; i_++)
                    {
                        xn[i_] = v * newtonstep[i_];
                    }
                    for (i_ = 0; i_ <= nmain + nslack - 1; i_++)
                    {
                        xn[i_] = xn[i_] + x[i_];
                    }
                    PostProcessBoundedStep(ref xn, x, bndl, havebndl, bndu, havebndu, nmain, nslack, vartofreeze, valtofreeze, v, maxsteplen);
                    if (maxsteplen > 0 && maxsteplen < 1)
                    {

                        //
                        // Newton step was restricted by activation of the constraints,
                        // perform Armijo iteration.
                        //
                        // Initial estimate for best step is zero step. We try different
                        // step sizes, from the 1-MaxStepLen (residual of the full Newton
                        // step) to progressively smaller and smaller steps.
                        //
                        armijobeststep = 0.0;
                        armijobestfeas = 0.0;
                        for (i = 0; i <= k - 1; i++)
                        {
                            v = 0.0;
                            for (i_ = 0; i_ <= nmain + nslack - 1; i_++)
                            {
                                v += ce[i, i_] * xn[i_];
                            }
                            v = v - ce[i, nmain + nslack];
                            armijobestfeas = armijobestfeas + MathExtensions.Squared(v);
                        }
                        armijobestfeas = Math.Sqrt(armijobestfeas);
                        armijostep = 1 - maxsteplen;
                        for (j = 0; j <= maxarmijoruns - 1; j++)
                        {
                            for (i_ = 0; i_ <= nmain + nslack - 1; i_++)
                            {
                                xa[i_] = xn[i_];
                            }
                            for (i_ = 0; i_ <= nmain + nslack - 1; i_++)
                            {
                                xa[i_] = xa[i_] + armijostep * newtonstep[i_];
                            }
                            EnforceBoundaryConstraints(ref xa, bndl, havebndl, bndu, havebndu, nmain, nslack);
                            feaserr = 0.0;
                            for (i = 0; i <= k - 1; i++)
                            {
                                v = 0.0;
                                for (i_ = 0; i_ <= nmain + nslack - 1; i_++)
                                {
                                    v += ce[i, i_] * xa[i_];
                                }
                                v = v - ce[i, nmain + nslack];
                                feaserr = feaserr + MathExtensions.Squared(v);
                            }
                            feaserr = Math.Sqrt(feaserr);
                            if (feaserr < armijobestfeas)
                            {
                                armijobestfeas = feaserr;
                                armijobeststep = armijostep;
                            }
                            armijostep = 0.5 * armijostep;
                        }
                        for (i_ = 0; i_ <= nmain + nslack - 1; i_++)
                        {
                            xa[i_] = xn[i_];
                        }
                        for (i_ = 0; i_ <= nmain + nslack - 1; i_++)
                        {
                            xa[i_] = xa[i_] + armijobeststep * newtonstep[i_];
                        }
                        EnforceBoundaryConstraints(ref xa, bndl, havebndl, bndu, havebndu, nmain, nslack);
                    }
                    else
                    {

                        //
                        // Armijo iteration is not performed
                        //
                        for (i_ = 0; i_ <= nmain + nslack - 1; i_++)
                        {
                            xa[i_] = xn[i_];
                        }
                    }
                    stage1isover = maxsteplen >= 1 || maxsteplen == 0;

                    //
                    // Calculate feasibility errors for old and new X.
                    // These quantinies are used for debugging purposes only.
                    // However, we can leave them in release code because performance impact is insignificant.
                    //
                    // Update X. Exit if needed.
                    //
                    feasold = 0;
                    feasnew = 0;
                    for (i = 0; i <= k - 1; i++)
                    {
                        v = 0.0;
                        for (i_ = 0; i_ <= nmain + nslack - 1; i_++)
                        {
                            v += ce[i, i_] * x[i_];
                        }
                        feasold = feasold + MathExtensions.Squared(v - ce[i, nmain + nslack]);
                        v = 0.0;
                        for (i_ = 0; i_ <= nmain + nslack - 1; i_++)
                        {
                            v += ce[i, i_] * xa[i_];
                        }
                        feasnew = feasnew + MathExtensions.Squared(v - ce[i, nmain + nslack]);
                    }
                    feasold = Math.Sqrt(feasold);
                    feasnew = Math.Sqrt(feasnew);
                    if (feasnew >= feasold)
                    {
                        break;
                    }
                    for (i_ = 0; i_ <= nmain + nslack - 1; i_++)
                    {
                        x[i_] = xa[i_];
                    }
                    if (stage1isover)
                    {
                        break;
                    }
                }

                //
                // Stage 2: gradient projection algorithm (GPA)
                //
                // * calculate feasibility error (with respect to linear equality constraints)
                // * calculate gradient G of F, project it into feasible area (G => PG)
                // * exit if norm(PG) is exactly zero or feasibility error is smaller than EpsC
                // * let XM be exact minimum of F along -PG (XM may be infeasible).
                //   calculate MaxStepLen = largest step in direction of -PG which retains feasibility.
                // * perform bounded step from X to XN:
                //   a) XN=XM              (if XM is feasible)
                //   b) XN=X-MaxStepLen*PG (otherwise)
                // * X := XN
                // * stop after specified number of iterations or when no new constraints was activated
                //
                // NOTES:
                // * grad(F) = (CE'*CE)*x - (b'*CE)^T
                // * CE[i] denotes I-th row of CE
                // * XM = X+stp*(-PG) where stp=(grad(F(X)),PG)/(CE*PG,CE*PG).
                //   Here PG is a projected gradient, but in fact it can be arbitrary non-zero 
                //   direction vector - formula for minimum of F along PG still will be correct.
                //
                werechangesinconstraints = false;
                for (gparuns = 1; gparuns <= k; gparuns++)
                {

                    //
                    // calculate feasibility error and G
                    //
                    feaserr = 0;
                    for (i = 0; i <= nmain + nslack - 1; i++)
                    {
                        g[i] = 0;
                    }
                    for (i = 0; i <= k - 1; i++)
                    {

                        //
                        // G += CE[i]^T * (CE[i]*x-b[i])
                        //
                        v = 0.0;
                        for (i_ = 0; i_ <= nmain + nslack - 1; i_++)
                        {
                            v += ce[i, i_] * x[i_];
                        }
                        v = v - ce[i, nmain + nslack];
                        feaserr = feaserr + MathExtensions.Squared(v);
                        for (i_ = 0; i_ <= nmain + nslack - 1; i_++)
                        {
                            g[i_] = g[i_] + v * ce[i, i_];
                        }
                    }

                    //
                    // project G, filter it (strip numerical noise)
                    //
                    for (i_ = 0; i_ <= nmain + nslack - 1; i_++)
                    {
                        pg[i_] = g[i_];
                    }
                    ProjectGradientIntoBoundaryConstraints(x, ref pg, bndl, havebndl, bndu, havebndu, nmain, nslack);
                    FilterDirection(ref pg, x, bndl, havebndl, bndu, havebndu, s, nmain, nslack, 1.0E-9);
                    for (i = 0; i <= nmain + nslack - 1; i++)
                    {
                        if (MathExtensions.Squared(colnorms[i]) != 0)
                        {
                            pg[i] = pg[i] / MathExtensions.Squared(colnorms[i]);
                        }
                        else
                        {
                            pg[i] = 0.0;
                        }
                    }

                    //
                    // Check GNorm and feasibility.
                    // Exit when GNorm is exactly zero.
                    //
                    pgnorm = 0.0;
                    for (i_ = 0; i_ <= nmain + nslack - 1; i_++)
                    {
                        pgnorm += pg[i_] * pg[i_];
                    }
                    feaserr = Math.Sqrt(feaserr);
                    pgnorm = Math.Sqrt(pgnorm);
                    if (pgnorm == 0)
                    {
                        result = feaserr <= epsi;
                        return result;
                    }

                    //
                    // calculate planned step length
                    //
                    vn = 0.0;
                    for (i_ = 0; i_ <= nmain + nslack - 1; i_++)
                    {
                        vn += g[i_] * pg[i_];
                    }
                    vd = 0;
                    for (i = 0; i <= k - 1; i++)
                    {
                        v = 0.0;
                        for (i_ = 0; i_ <= nmain + nslack - 1; i_++)
                        {
                            v += ce[i, i_] * pg[i_];
                        }
                        vd = vd + MathExtensions.Squared(v);
                    }
                    stp = vn / vd;

                    //
                    // Calculate step bound.
                    // Perform bounded step and post-process it
                    //
                    CalculateStepBound(x, pg, -1.0, bndl, havebndl, bndu, havebndu, nmain, nslack, ref vartofreeze, ref valtofreeze, ref maxsteplen);
                    if (vartofreeze >= 0 && maxsteplen == 0)
                    {
                        result = false;
                        return result;
                    }
                    if (vartofreeze >= 0)
                    {
                        v = Math.Min(stp, maxsteplen);
                    }
                    else
                    {
                        v = stp;
                    }
                    for (i_ = 0; i_ <= nmain + nslack - 1; i_++)
                    {
                        xn[i_] = x[i_];
                    }
                    for (i_ = 0; i_ <= nmain + nslack - 1; i_++)
                    {
                        xn[i_] = xn[i_] - v * pg[i_];
                    }
                    PostProcessBoundedStep(ref xn, x, bndl, havebndl, bndu, havebndu, nmain, nslack, vartofreeze, valtofreeze, v, maxsteplen);

                    //
                    // update X
                    // check stopping criteria
                    //
                    werechangesinconstraints = werechangesinconstraints || NumberOfChangedConstraints(xn, x, bndl, havebndl, bndu, havebndu, nmain, nslack) > 0;
                    for (i_ = 0; i_ <= nmain + nslack - 1; i_++)
                    {
                        x[i_] = xn[i_];
                    }
                    gpaits = gpaits + 1;
                    if (!werechangesinconstraints)
                    {
                        break;
                    }
                }

                //
                // Stage 3: decide to stop algorithm or not to stop
                //
                // 1. we can stop when last GPA run did NOT changed constraints status.
                //    It means that we've found final set of the active constraints even
                //    before GPA made its run. And it means that Newton step moved us to
                //    the minimum subject to the present constraints.
                //    Depending on feasibility error, True or False is returned.
                //
                feaserr = 0;
                for (i = 0; i <= k - 1; i++)
                {
                    v = 0.0;
                    for (i_ = 0; i_ <= nmain + nslack - 1; i_++)
                    {
                        v += ce[i, i_] * x[i_];
                    }
                    v = v - ce[i, nmain + nslack];
                    feaserr = feaserr + MathExtensions.Squared(v);
                }
                feaserr = Math.Sqrt(feaserr);
                if (feaserr <= epsi)
                {
                    itswithintolerance = itswithintolerance + 1;
                }
                else
                {
                    itswithintolerance = 0;
                }
                if (!werechangesinconstraints || itswithintolerance >= maxitswithintolerance)
                {
                    result = feaserr <= epsi;
                    return result;
                }
                itscount = itscount + 1;
            }
            return result;
        }


        /*************************************************************************
            This function check, that input derivatives are right. First it scale
        parameters DF0 and DF1 from segment [A;B] to [0;1]. Than it build Hermite
        spline and derivative of it in 0,5. Search scale as Max(DF0,DF1, |F0-F1|).
        Right derivative has to satisfy condition:
            |H-F|/S<=0,01, |H'-F'|/S<=0,01.
            
        INPUT PARAMETERS:
            F0  -   function's value in X-TestStep point;
            DF0 -   derivative's value in X-TestStep point;
            F1  -   function's value in X+TestStep point;
            DF1 -   derivative's value in X+TestStep point;
            F   -   testing function's value;
            DF  -   testing derivative's value;
           Width-   width of verification segment.

        RESULT:
            If input derivatives is right then function returns true, else 
            function returns false.
            
          -- ALGLIB --
             Copyright 29.05.2012 by Bochkanov Sergey
        *************************************************************************/
        public static bool DerivativeCheck(double f0,
            double df0,
            double f1,
            double df1,
            double f,
            double df,
            double width)
        {
            bool result = new bool();
            double s = 0;
            double h = 0;
            double dh = 0;

            df = width * df;
            df0 = width * df0;
            df1 = width * df1;
            s = Math.Max(Math.Max(Math.Abs(df0), Math.Abs(df1)), Math.Abs(f1 - f0));
            h = 0.5 * f0 + 0.125 * df0 + 0.5 * f1 - 0.125 * df1;
            dh = -(1.5 * f0) - 0.25 * df0 + 1.5 * f1 - 0.25 * df1;
            if (s != 0)
            {
                if (Math.Abs(h - f) / s > 0.001 || Math.Abs(dh - df) / s > 0.001)
                {
                    result = false;
                    return result;
                }
            }
            else
            {
                if (h - f != 0.0 || dh - df != 0.0)
                {
                    result = false;
                    return result;
                }
            }
            result = true;
            return result;
        }


        public static int Len<T>(T[] a)
        { return a.Length; }
        
        public static void Assert(bool cond, string s)
        {
            if (!cond)
            {
                throw new Exception(s);
            }
        }

        public static void Assert(bool cond)
        {
            Assert(cond, "ALGLIB: assertion failed");
        }

        public static void SetLengthAtLeast<T>(ref T[] x, int n)
        {
            if (x.Length < n)
            {
                Array.Resize(ref x, n);
            }
        }

        public static bool IsFiniteVector(IList<double> x, int n)
        {
            bool result = new bool();
            int i = 0;

            Assert(n >= 0, "APSERVIsFiniteVector: internal error (N<0)");
            if (n == 0)
            {
                result = true;
                return result;
            }
            if (x.Count < n)
            {
                result = false;
                return result;
            }
            for (i = 0; i <= n - 1; i++)
            {
                if (!MathExtensions.IsFinite(x[i]))
                {
                    result = false;
                    return result;
                }
            }
            result = true;
            return result;
        }

        public static bool IsFiniteMatrix(Matrix<double> x, int m, int n)
        {
            bool result = new bool();
            int i = 0;
            int j = 0;

            Assert(n >= 0, "APSERVIsFiniteMatrix: internal error (N<0)");
            Assert(m >= 0, "APSERVIsFiniteMatrix: internal error (M<0)");
            if (m == 0 || n == 0)
            {
                result = true;
                return result;
            }
            if (x.Rows < m || x.Columns < n)
            {
                result = false;
                return result;
            }
            for (i = 0; i <= m - 1; i++)
            {
                for (j = 0; j <= n - 1; j++)
                {
                    if (!MathExtensions.IsFinite(x[i, j]))
                    {
                        result = false;
                        return result;
                    }
                }
            }
            result = true;
            return result;
        }

        public static bool IsFiniteRtrMatrix(Matrix<double> x, int n, bool isupper)
        {
            bool result = new bool();
            int i = 0;
            int j1 = 0;
            int j2 = 0;
            int j = 0;

            Assert(n >= 0, "APSERVIsFiniteRTRMatrix: internal error (N<0)");
            if (n == 0)
            {
                result = true;
                return result;
            }
            if (x.Rows < n || x.Columns < n)
            {
                result = false;
                return result;
            }
            for (i = 0; i <= n - 1; i++)
            {
                if (isupper)
                {
                    j1 = i;
                    j2 = n - 1;
                }
                else
                {
                    j1 = 0;
                    j2 = i;
                }
                for (j = j1; j <= j2; j++)
                {
                    if (!MathExtensions.IsFinite(x[i, j]))
                    {
                        result = false;
                        return result;
                    }
                }
            }
            result = true;
            return result;
        }

        /*************************************************************************
           This function calculates "safe" min(X/Y,V) for positive finite X, Y, V.
           No overflow is generated in any case.

             -- ALGLIB --
                Copyright by Bochkanov Sergey
           *************************************************************************/
        public static double SafeMin(double x, double y, double v)
        {
            double result = 0;
            double r = 0;

            if (y >= 1)
            {

                //
                // Y>=1, we can safely divide by Y
                //
                r = x / y;
                result = v;
                if (v > r)
                {
                    result = r;
                }
                else
                {
                    result = v;
                }
            }
            else
            {

                //
                // Y<1, we can safely multiply by Y
                //
                if (x < v * y)
                {
                    result = x / y;
                }
                else
                {
                    result = v;
                }
            }
            return result;
        }

        public static void Increment(ref int v)
        {
            v = v + 1;
        }

        /*************************************************************************
            'bounds' value: maps X to [B1,B2]

              -- ALGLIB --
                 Copyright 20.03.2009 by Bochkanov Sergey
            *************************************************************************/
        public static double BoundValue(double x, double b1, double b2)
        {
            double result = 0;

            if (x <= b1)
            {
                result = b1;
                return result;
            }
            if (x >= b2)
            {
                result = b2;
                return result;
            }
            result = x;
            return result;
        }
    }
}
