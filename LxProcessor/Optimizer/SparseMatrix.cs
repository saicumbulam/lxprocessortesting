using System;
using System.Collections.Generic;
using LxCommon.DataStructures;
using LxCommon.Utilities;
using LxProcessor.Common;

namespace LxProcessor.Optimizer
{
    public class SparseMatrix
    {
        public FluxList<double> vals;
        public FluxList<int> idx;
        public FluxList<int> ridx;
        public FluxList<int> didx;
        public FluxList<int> uidx;
        public int matrixtype;
        public int m;
        public int n;
        public int nfree;
        public int ninitialized;
        public SparseMatrix()
        {
            vals = new FluxList<double>();
            idx = new FluxList<int>();
            ridx = new FluxList<int>();
            didx = new FluxList<int>();
            uidx = new FluxList<int>();
        }

        internal void CleanForReuse()
        {
            vals.Clear();
            idx.Clear();
            ridx.Clear();
            didx.Clear();
            uidx.Clear();
            matrixtype = default(int);
            m = default(int);
            n = default(int);
            nfree = default(int);
            ninitialized = default(int);
        }

        

        private const double DdesiredLoadFactor = 0.66;
        private const double MaxLoadFactor = 0.75;
        private const double GrowthFactor = 2.00;
        private const int Additional = 10;

        /*************************************************************************
            This function modifies S[i,j] - element of the sparse matrix.

            For Hash-based storage format:
            * new value can be zero or non-zero.  In case new value of S[i,j] is zero,
              this element is deleted from the table.
            * this  function  has  no  effect when called with zero V for non-existent
              element.

            For CRS-bases storage format:
            * new value MUST be non-zero. Exception will be thrown for zero V.
            * elements must be initialized in correct order -  from top row to bottom,
              within row - from left to right.

            INPUT PARAMETERS
                S           -   sparse M*N matrix in Hash-Table or CRS representation.
                I           -   row index of the element to modify, 0<=I<M
                J           -   column index of the element to modify, 0<=J<N
                V           -   value to set, must be finite number, can be zero

            OUTPUT PARAMETERS
                S           -   modified matrix

              -- ALGLIB PROJECT --
                 Copyright 14.10.2011 by Bochkanov Sergey
            *************************************************************************/

        public static void SparseSet(SparseMatrix s,
            int i,
            int j,
            double v)
        {
            int hashcode = 0;
            int tcode = 0;
            int k = 0;

            Utilities.Assert(i >= 0, "SparseSet: I<0");
            Utilities.Assert(i < s.m, "SparseSet: I>=M");
            Utilities.Assert(j >= 0, "SparseSet: J<0");
            Utilities.Assert(j < s.n, "SparseSet: J>=N");
            Utilities.Assert(MathExtensions.IsFinite(v), "SparseSet: V is not finite number");

            //
            // Hash-table matrix
            //
            if (s.matrixtype == 0)
            {
                tcode = -1;
                k = s.vals.Count;
                if ((1 - MaxLoadFactor) * k >= s.nfree)
                {
                    ResizeMatrix(s);
                    k = s.vals.Count;
                }
                hashcode = Hash(i, j, k);
                while (true)
                {
                    if (s.idx[2 * hashcode] == -1)
                    {
                        if (v != 0)
                        {
                            if (tcode != -1)
                            {
                                hashcode = tcode;
                            }
                            s.vals[hashcode] = v;
                            s.idx[2 * hashcode] = i;
                            s.idx[2 * hashcode + 1] = j;
                            if (tcode == -1)
                            {
                                s.nfree = s.nfree - 1;
                            }
                        }
                        return;
                    }
                    else
                    {
                        if (s.idx[2 * hashcode] == i && s.idx[2 * hashcode + 1] == j)
                        {
                            if (v == 0)
                            {
                                s.idx[2 * hashcode] = -2;
                            }
                            else
                            {
                                s.vals[hashcode] = v;
                            }
                            return;
                        }
                        if (tcode == -1 && s.idx[2 * hashcode] == -2)
                        {
                            tcode = hashcode;
                        }

                        //
                        // Next step
                        //
                        hashcode = (hashcode + 1) % k;
                    }
                }
            }

            //
            // CRS matrix
            //
            if (s.matrixtype == 1)
            {
                Utilities.Assert(v != 0, "SparseSet: CRS format does not allow you to write zero elements");
                Utilities.Assert(s.ridx[i] <= s.ninitialized, "SparseSet: too few initialized elements at some row (you have promised more when called SparceCreateCRS)");
                Utilities.Assert(s.ridx[i + 1] > s.ninitialized, "SparseSet: too many initialized elements at some row (you have promised less when called SparceCreateCRS)");
                Utilities.Assert(s.ninitialized == s.ridx[i] || s.idx[s.ninitialized - 1] < j, "SparseSet: incorrect column order (you must fill every row from left to right)");
                s.vals[s.ninitialized] = v;
                s.idx[s.ninitialized] = j;
                s.ninitialized = s.ninitialized + 1;

                //
                // If matrix has been created then
                // initiale 'S.UIdx' and 'S.DIdx'
                //
                if (s.ninitialized == s.ridx[s.m])
                {
                    InitDIdxUIdx(s);
                }
            }
        }


        /*************************************************************************
            This function calculates matrix-vector product  S*x, when S is  symmetric
            matrix.  Matrix  S  must  be stored in  CRS  format  (exception  will  be
            thrown otherwise).

            INPUT PARAMETERS
                S           -   sparse M*M matrix in CRS format (you MUST convert  it
                                to CRS before calling this function).
                IsUpper     -   whether upper or lower triangle of S is given:
                                * if upper triangle is given,  only   S[i,j] for j>=i
                                  are used, and lower triangle is ignored (it can  be
                                  empty - these elements are not referenced at all).
                                * if lower triangle is given,  only   S[i,j] for j<=i
                                  are used, and upper triangle is ignored.
                X           -   array[N], input vector. For  performance  reasons  we 
                                make only quick checks - we check that array size  is
                                at least N, but we do not check for NAN's or INF's.
                Y           -   output buffer, possibly preallocated. In case  buffer
                                size is too small to store  result,  this  buffer  is
                                automatically resized.
            
            OUTPUT PARAMETERS
                Y           -   array[M], S*x
            
            NOTE: this function throws exception when called for non-CRS matrix.  You
            must convert your matrix  with  SparseConvertToCRS()  before  using  this
            function.

              -- ALGLIB PROJECT --
                 Copyright 14.10.2011 by Bochkanov Sergey
            *************************************************************************/
        public static void SymetricMatrixVectorProduct(SparseMatrix s,
            bool isupper,
            FluxList<double> x,
            ref FluxList<double> y)
        {
            int i = 0;
            int j = 0;
            int id = 0;
            int lt = 0;
            int rt = 0;
            double v = 0;
            double vy = 0;
            double vx = 0;

            Utilities.Assert(s.matrixtype == 1, "SparseSMV: incorrect matrix type (convert your matrix to CRS)");
            Utilities.Assert(s.ninitialized == s.ridx[s.m], "SparseSMV: some rows/elements of the CRS matrix were not initialized (you must initialize everything you promised to SparseCreateCRS)");
            Utilities.Assert(x.Count >= s.n, "SparseSMV: length(X)<N");
            Utilities.Assert(s.m == s.n, "SparseSMV: non-square matrix");
            y.SetLengthAtLeast(s.m);
            for (i = 0; i <= s.m - 1; i++)
            {
                y[i] = 0;
            }
            for (i = 0; i <= s.m - 1; i++)
            {
                if (s.didx[i] != s.uidx[i])
                {
                    y[i] = y[i] + s.vals[s.didx[i]] * x[s.idx[s.didx[i]]];
                }
                if (isupper)
                {
                    lt = s.uidx[i];
                    rt = s.ridx[i + 1];
                    vy = 0;
                    vx = x[i];
                    for (j = lt; j <= rt - 1; j++)
                    {
                        id = s.idx[j];
                        v = s.vals[j];
                        vy = vy + x[id] * v;
                        y[id] = y[id] + vx * v;
                    }
                    y[i] = y[i] + vy;
                }
                else
                {
                    lt = s.ridx[i];
                    rt = s.didx[i];
                    vy = 0;
                    vx = x[i];
                    for (j = lt; j <= rt - 1; j++)
                    {
                        id = s.idx[j];
                        v = s.vals[j];
                        vy = vy + x[id] * v;
                        y[id] = y[id] + vx * v;
                    }
                    y[i] = y[i] + vy;
                }
            }
        }

        private static void ResizeMatrix(SparseMatrix s)
        {
            int k = 0;
            int k1 = 0;
            int i = 0;
            FluxList<double> tvals = new FluxList<double>();
            FluxList<int> tidx = new FluxList<int>();

            Utilities.Assert(s.matrixtype == 0, "SparseResizeMatrix: incorrect matrix type");

            //
            // Initialization for length and number of non-null elementd
            //
            k = s.vals.Count;
            k1 = 0;

            //
            // Calculating number of non-null elements
            //
            for (i = 0; i <= k - 1; i++)
            {
                if (s.idx[2 * i] >= 0)
                {
                    k1 = k1 + 1;
                }
            }

            //
            // Initialization value for free space
            //
            s.nfree = (int)Math.Round(k1 /DdesiredLoadFactor *GrowthFactor + Additional) - k1;
            tvals.Resize(s.nfree + k1);
            tidx.Resize(2 * (s.nfree + k1));
            Swap(ref s.vals, ref tvals);
            Swap(ref s.idx, ref tidx);
            for (i = 0; i <= s.nfree + k1 - 1; i++)
            {
                s.idx[2 * i] = -1;
            }
            for (i = 0; i <= k - 1; i++)
            {
                if (tidx[2 * i] >= 0)
                {
                    SparseSet(s, tidx[2 * i], tidx[2 * i + 1], tvals[i]);
                }
            }
        }

        private static void Swap<T>(ref T a, ref T b)
        {
            T t = a;
            a = b;
            b = t;
        }

        

        private static void InitDIdxUIdx(SparseMatrix s)
        {
            int i = 0;
            int j = 0;
            int lt = 0;
            int rt = 0;

            s.didx.Resize(s.m);
            s.uidx.Resize(s.m);

            for (i = 0; i <= s.m - 1; i++)
            {
                s.uidx[i] = -1;
                s.didx[i] = -1;
                lt = s.ridx[i];
                rt = s.ridx[i + 1];
                for (j = lt; j <= rt - 1; j++)
                {
                    if (i < s.idx[j] && s.uidx[i] == -1)
                    {
                        s.uidx[i] = j;
                        break;
                    }
                    else
                    {
                        if (i == s.idx[j])
                        {
                            s.didx[i] = j;
                        }
                    }
                }
                if (s.uidx[i] == -1)
                {
                    s.uidx[i] = s.ridx[i + 1];
                }
                if (s.didx[i] == -1)
                {
                    s.didx[i] = s.uidx[i];
                }
            }
        }

        private static int Hash(int i,
            int j,
            int tabsize)
        {
            int result = 0;
            HighQualityRandom.State r = new HighQualityRandom.State();

            HighQualityRandom.Seed(i, j, r);
            result = HighQualityRandom.GenerateInt(r, tabsize);
            return result;
        }

        
    };
}