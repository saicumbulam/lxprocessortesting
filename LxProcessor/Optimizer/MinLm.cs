﻿using System;
using System.Collections.Generic;
using LxCommon.DataStructures;
using LxCommon.Utilities;
using LxProcessor.Common;

namespace LxProcessor.Optimizer
{
    public class MinLm
    {

        /*************************************************************************
        Levenberg-Marquardt optimizer.

        This structure should be created using one of the MinLMCreate???()
        functions. You should not access its fields directly; use ALGLIB functions
        to work with it.
        *************************************************************************/
        public class State
        {
            public int n;
            public int m;
            public double diffstep;
            public double epsg;
            public double epsf;
            public double epsx;
            public int maxits;
            public bool xrep;
            public double stpmax;
            public int maxmodelage;
            public bool makeadditers;
            public FluxList<double> x;
            public double f;
            public FluxList<double> fi;
            public Matrix<double> j;
            public Matrix<double> h;
            public FluxList<double> g;
            public bool needf;
            public bool needfg;
            public bool needfgh;
            public bool needfij;
            public bool needfi;
            public bool xupdated;
            public int algomode;
            public bool hasf;
            public bool hasfi;
            public bool hasg;
            public FluxList<double> xbase;
            public double fbase;
            public FluxList<double> fibase;
            public FluxList<double> gbase;
            public Matrix<double> quadraticmodel;
            public FluxList<double> bndl;
            public FluxList<double> bndu;
            public FluxList<bool> havebndl;
            public FluxList<bool> havebndu;
            public FluxList<double> s;
            public double lambdav;
            public double nu;
            public int modelage;
            public FluxList<double> xdir;
            public FluxList<double> deltax;
            public FluxList<double> deltaf;
            public bool deltaxready;
            public bool deltafready;
            public double teststep;
            public int repiterationscount;
            public int repterminationtype;
            public int repfuncidx;
            public int repvaridx;
            public int repnfunc;
            public int repnjac;
            public int repngrad;
            public int repnhess;
            public int repncholesky;
            public ReverseCommunicationStructure rstate;
            public FluxList<double> choleskybuf;
            public FluxList<double> tmp0;
            public double actualdecrease;
            public double predicteddecrease;
            public double xm1;
            public double xp1;
            public FluxList<double> fm1;
            public FluxList<double> fp1;
            public FluxList<double> fc1;
            public FluxList<double> gm1;
            public FluxList<double> gp1;
            public FluxList<double> gc1;
            public MinLbfgs.MinLbfgsState lbfgsState;
            public MinLbfgs.MinLbfgsReport lbfgsReport;
            public MinQuadraticProgramming.State qpState;
            public MinQuadraticProgramming.Report qpReport;
            public State()
            {
                x = new FluxList<double>();
                fi = new FluxList<double>();
                j = new Matrix<double>(0, 0);
                h = new Matrix<double>(0, 0);
                g = new FluxList<double>();
                xbase = new FluxList<double>();
                fibase = new FluxList<double>();
                gbase = new FluxList<double>();
                quadraticmodel = new Matrix<double>(0, 0);
                bndl = new FluxList<double>();
                bndu = new FluxList<double>();
                havebndl = new FluxList<bool>();
                havebndu = new FluxList<bool>();
                s = new FluxList<double>();
                xdir = new FluxList<double>();
                deltax = new FluxList<double>();
                deltaf = new FluxList<double>();
                rstate = new ReverseCommunicationStructure();
                choleskybuf = new FluxList<double>();
                tmp0 = new FluxList<double>();
                fm1 = new FluxList<double>();
                fp1 = new FluxList<double>();
                fc1 = new FluxList<double>();
                gm1 = new FluxList<double>();
                gp1 = new FluxList<double>();
                gc1 = new FluxList<double>();
                lbfgsState = new MinLbfgs.MinLbfgsState();
                lbfgsReport = new MinLbfgs.MinLbfgsReport();
                qpState = new MinQuadraticProgramming.State();
                qpReport = new MinQuadraticProgramming.Report();
            }

            public void CleanForReuse()
            {
                n = default(int);
                m = default(int);
                diffstep = default(double);
                epsg= default(double);
                epsf= default(double);
                epsx= default(double);
                maxits = default(int);
                xrep = default(bool);
                stpmax= default(double);
                maxmodelage = default(int);
                makeadditers = default(bool);
                x.Clear();
                f= default(double);
                fi.Clear();
                j.Clear();
                h.Clear();
                g.Clear();
                needf = default(bool);
                needfg = default(bool);
                needfgh = default(bool);
                needfij = default(bool);
                needfi = default(bool);
                xupdated = default(bool);
                algomode = default(int);
                hasf = default(bool);
                hasfi = default(bool);
                hasg = default(bool);
                xbase.Clear();
                fbase= default(double);
                fibase.Clear();
                gbase.Clear();
                quadraticmodel.Clear();
                bndl.Clear();
                bndu.Clear();
                havebndl.Clear();
                havebndu.Clear();
                s.Clear();
                lambdav= default(double);
                nu= default(double);
                modelage = default(int);
                xdir.Clear();
                deltax.Clear();
                deltaf.Clear();
                deltaxready = default(bool);
                deltafready = default(bool);
                teststep= default(double);
                repiterationscount = default(int);
                repterminationtype = default(int);
                repfuncidx = default(int);
                repvaridx = default(int);
                repnfunc = default(int);
                repnjac = default(int);
                repngrad = default(int);
                repnhess = default(int);
                repncholesky = default(int);
                rstate.CleanForReuse();
                choleskybuf.Clear();
                tmp0.Clear();
                actualdecrease= default(double);
                predicteddecrease= default(double);
                xm1= default(double);
                xp1= default(double);
                fm1.Clear();
                fp1.Clear();
                fc1.Clear();
                gm1.Clear();
                gp1.Clear();
                gc1.Clear();
                lbfgsState.CleanForReuse();
                lbfgsReport.CleanForReuse();;
                qpState.CleanForReuse();;
                qpReport.CleanForReuse();;
            }
        }


        /*************************************************************************
        Optimization report, filled by MinLMResults() function

        FIELDS:
        * TerminationType, completetion code:
            * -7    derivative correctness check failed;
                    see Rep.WrongNum, Rep.WrongI, Rep.WrongJ for
                    more information.
            *  1    relative function improvement is no more than
                    EpsF.
            *  2    relative step is no more than EpsX.
            *  4    gradient is no more than EpsG.
            *  5    MaxIts steps was taken
            *  7    stopping conditions are too stringent,
                    further improvement is impossible
        * IterationsCount, contains iterations count
        * NFunc, number of function calculations
        * NJac, number of Jacobi matrix calculations
        * NGrad, number of gradient calculations
        * NHess, number of Hessian calculations
        * NCholesky, number of Cholesky decomposition calculations
        *************************************************************************/
        public class Report
        {
            public int iterationscount;
            public int terminationtype;
            public int funcidx;
            public int varidx;
            public int nfunc;
            public int njac;
            public int ngrad;
            public int nhess;
            public int ncholesky;
            public Report()
            {
                
            }

            public void CleanForReuse()
            {
                iterationscount = default(int);
                terminationtype = default(int);
                funcidx = default(int);
                varidx = default(int);
                nfunc = default(int);
                njac = default(int);
                ngrad = default(int);
                nhess = default(int);
                ncholesky = default(int);
            }            
        }




        private const double LambdaUp = 2.0;
        private const double LambdaDown = 0.33;
        private const double SuspiciousNu = 16;
        private const int SmallModelAge = 3;
        private const int AddIterations = 5;


        /*************************************************************************
                        IMPROVED LEVENBERG-MARQUARDT METHOD FOR
                         NON-LINEAR LEAST SQUARES OPTIMIZATION

        DESCRIPTION:
        This function is used to find minimum of function which is represented  as
        sum of squares:
            F(x) = f[0]^2(x[0],...,x[n-1]) + ... + f[m-1]^2(x[0],...,x[n-1])
        using value of function vector f[] and Jacobian of f[].


        REQUIREMENTS:
        This algorithm will request following information during its operation:

        * function vector f[] at given point X
        * function vector f[] and Jacobian of f[] (simultaneously) at given point

        There are several overloaded versions of  MinLMOptimize()  function  which
        correspond  to  different LM-like optimization algorithms provided by this
        unit. You should choose version which accepts fvec()  and jac() callbacks.
        First  one  is used to calculate f[] at given point, second one calculates
        f[] and Jacobian df[i]/dx[j].

        You can try to initialize MinLMState structure with VJ  function and  then
        use incorrect version  of  MinLMOptimize()  (for  example,  version  which
        works  with  general  form function and does not provide Jacobian), but it
        will  lead  to  exception  being  thrown  after first attempt to calculate
        Jacobian.


        USAGE:
        1. User initializes algorithm state with MinLMCreateVJ() call
        2. User tunes solver parameters with MinLMSetCond(),  MinLMSetStpMax() and
           other functions
        3. User calls MinLMOptimize() function which  takes algorithm  state   and
           callback functions.
        4. User calls MinLMResults() to get solution
        5. Optionally, user may call MinLMRestartFrom() to solve  another  problem
           with same N/M but another starting point and/or another function.
           MinLMRestartFrom() allows to reuse already initialized structure.


        INPUT PARAMETERS:
            N       -   dimension, N>1
                        * if given, only leading N elements of X are used
                        * if not given, automatically determined from size of X
            M       -   number of functions f[i]
            X       -   initial solution, array[0..N-1]

        OUTPUT PARAMETERS:
            State   -   structure which stores algorithm state

        NOTES:
        1. you may tune stopping conditions with MinLMSetCond() function
        2. if target function contains exp() or other fast growing functions,  and
           optimization algorithm makes too large steps which leads  to  overflow,
           use MinLMSetStpMax() function to bound algorithm's steps.

          -- ALGLIB --
             Copyright 30.03.2009 by Bochkanov Sergey
        *************************************************************************/

        public static void CreateVj(int n, int m, IList<double> x, State state)
        {
            Utilities.Assert(n >= 1, "MinLMCreateVJ: N<1!");
            Utilities.Assert(m >= 1, "MinLMCreateVJ: M<1!");
            Utilities.Assert(x.Count >= n, "MinLMCreateVJ: Length(X)<N!");
            Utilities.Assert(Utilities.IsFiniteVector(x, n), "MinLMCreateVJ: X contains infinite or NaN values!");

            //
            // initialize, check parameters
            //
            state.teststep = 0;
            state.n = n;
            state.m = m;
            state.algomode = 1;
            state.hasf = false;
            state.hasfi = true;
            state.hasg = false;

            //
            // second stage of initialization
            //
            Prepare(n, m, false, state);
            SetAccelerationType(state, 0);
            SetConditions(state, 0, 0, 0, 0);
            SetReporting(state, false);
            SetStepMax(state, 0);
            RestartFrom(state, x);
        }

        /*************************************************************************
        This function sets stopping conditions for Levenberg-Marquardt optimization
        algorithm.

        INPUT PARAMETERS:
            State   -   structure which stores algorithm state
            EpsG    -   >=0
                        The  subroutine  finishes  its  work   if   the  condition
                        |v|<EpsG is satisfied, where:
                        * |.| means Euclidian norm
                        * v - scaled gradient vector, v[i]=g[i]*s[i]
                        * g - gradient
                        * s - scaling coefficients set by MinLMSetScale()
            EpsF    -   >=0
                        The  subroutine  finishes  its work if on k+1-th iteration
                        the  condition  |F(k+1)-F(k)|<=EpsF*max{|F(k)|,|F(k+1)|,1}
                        is satisfied.
            EpsX    -   >=0
                        The subroutine finishes its work if  on  k+1-th  iteration
                        the condition |v|<=EpsX is fulfilled, where:
                        * |.| means Euclidian norm
                        * v - scaled step vector, v[i]=dx[i]/s[i]
                        * dx - ste pvector, dx=X(k+1)-X(k)
                        * s - scaling coefficients set by MinLMSetScale()
            MaxIts  -   maximum number of iterations. If MaxIts=0, the  number  of
                        iterations   is    unlimited.   Only   Levenberg-Marquardt
                        iterations  are  counted  (L-BFGS/CG  iterations  are  NOT
                        counted because their cost is very low compared to that of
                        LM).

        Passing EpsG=0, EpsF=0, EpsX=0 and MaxIts=0 (simultaneously) will lead to
        automatic stopping criterion selection (small EpsX).

          -- ALGLIB --
             Copyright 02.04.2010 by Bochkanov Sergey
        *************************************************************************/
        public static void SetConditions(State state, double epsg, double epsf, double epsx, int maxits)
        {
            Utilities.Assert(MathExtensions.IsFinite(epsg), "MinLMSetCond: EpsG is not finite number!");
            Utilities.Assert(epsg >= 0, "MinLMSetCond: negative EpsG!");
            Utilities.Assert(MathExtensions.IsFinite(epsf), "MinLMSetCond: EpsF is not finite number!");
            Utilities.Assert(epsf >= 0, "MinLMSetCond: negative EpsF!");
            Utilities.Assert(MathExtensions.IsFinite(epsx), "MinLMSetCond: EpsX is not finite number!");
            Utilities.Assert(epsx >= 0, "MinLMSetCond: negative EpsX!");
            Utilities.Assert(maxits >= 0, "MinLMSetCond: negative MaxIts!");
            if (((epsg == 0 && epsf == 0) && epsx == 0) && maxits == 0)
            {
                epsx = 1.0E-6;
            }
            state.epsg = epsg;
            state.epsf = epsf;
            state.epsx = epsx;
            state.maxits = maxits;
        }


        /*************************************************************************
        This function turns on/off reporting.

        INPUT PARAMETERS:
            State   -   structure which stores algorithm state
            NeedXRep-   whether iteration reports are needed or not

        If NeedXRep is True, algorithm will call rep() callback function if  it is
        provided to MinLMOptimize(). Both Levenberg-Marquardt and internal  L-BFGS
        iterations are reported.

          -- ALGLIB --
             Copyright 02.04.2010 by Bochkanov Sergey
        *************************************************************************/
        private static void SetReporting(State state, bool needxrep)
        {
            state.xrep = needxrep;
        }


        /*************************************************************************
        This function sets maximum step length

        INPUT PARAMETERS:
            State   -   structure which stores algorithm state
            StpMax  -   maximum step length, >=0. Set StpMax to 0.0,  if you don't
                        want to limit step length.

        Use this subroutine when you optimize target function which contains exp()
        or  other  fast  growing  functions,  and optimization algorithm makes too
        large  steps  which  leads  to overflow. This function allows us to reject
        steps  that  are  too  large  (and  therefore  expose  us  to the possible
        overflow) without actually calculating function value at the x+stp*d.

        NOTE: non-zero StpMax leads to moderate  performance  degradation  because
        intermediate  step  of  preconditioned L-BFGS optimization is incompatible
        with limits on step size.

          -- ALGLIB --
             Copyright 02.04.2010 by Bochkanov Sergey
        *************************************************************************/
        private static void SetStepMax(State state, double stpmax)
        {
            Utilities.Assert(MathExtensions.IsFinite(stpmax), "MinLMSetStpMax: StpMax is not finite!");
            Utilities.Assert(stpmax >= 0, "MinLMSetStpMax: StpMax<0!");
            state.stpmax = stpmax;
        }


        /*************************************************************************
        NOTES:

        1. Depending on function used to create state  structure,  this  algorithm
           may accept Jacobian and/or Hessian and/or gradient.  According  to  the
           said above, there ase several versions of this function,  which  accept
           different sets of callbacks.

           This flexibility opens way to subtle errors - you may create state with
           MinLMCreateFGH() (optimization using Hessian), but call function  which
           does not accept Hessian. So when algorithm will request Hessian,  there
           will be no callback to call. In this case exception will be thrown.

           Be careful to avoid such errors because there is no way to find them at
           compile time - you can see them at runtime only.

          -- ALGLIB --
             Copyright 10.03.2009 by Bochkanov Sergey
        *************************************************************************/
        public static bool Iteration(State state)
        {
            bool result = new bool();
            int n = 0;
            int m = 0;
            bool bflag = new bool();
            int iflag = 0;
            double v = 0;
            double s = 0;
            double t = 0;
            int i = 0;
            int k = 0;
            int i_ = 0;


            //
            // Reverse communication preparations
            // I know it looks ugly, but it works the same way
            // anywhere from C++ to Python.
            //
            // This code initializes locals by:
            // * random values determined during code
            //   generation - on first subroutine call
            // * values from previous call - on subsequent calls
            //
            if (state.rstate.stage >= 0)
            {
                n = state.rstate.ia[0];
                m = state.rstate.ia[1];
                iflag = state.rstate.ia[2];
                i = state.rstate.ia[3];
                k = state.rstate.ia[4];
                bflag = state.rstate.ba[0];
                v = state.rstate.ra[0];
                s = state.rstate.ra[1];
                t = state.rstate.ra[2];
            }
            else
            {
                n = -983;
                m = -989;
                iflag = -834;
                i = 900;
                k = -287;
                bflag = false;
                v = 214;
                s = -338;
                t = -686;
            }
            if (state.rstate.stage == 0)
            {
                goto lbl_0;
            }
            if (state.rstate.stage == 1)
            {
                goto lbl_1;
            }
            if (state.rstate.stage == 2)
            {
                goto lbl_2;
            }
            if (state.rstate.stage == 3)
            {
                goto lbl_3;
            }
            if (state.rstate.stage == 4)
            {
                goto lbl_4;
            }
            if (state.rstate.stage == 5)
            {
                goto lbl_5;
            }
            if (state.rstate.stage == 6)
            {
                goto lbl_6;
            }
            if (state.rstate.stage == 7)
            {
                goto lbl_7;
            }
            if (state.rstate.stage == 8)
            {
                goto lbl_8;
            }
            if (state.rstate.stage == 9)
            {
                goto lbl_9;
            }
            if (state.rstate.stage == 10)
            {
                goto lbl_10;
            }
            if (state.rstate.stage == 11)
            {
                goto lbl_11;
            }
            if (state.rstate.stage == 12)
            {
                goto lbl_12;
            }
            if (state.rstate.stage == 13)
            {
                goto lbl_13;
            }
            if (state.rstate.stage == 14)
            {
                goto lbl_14;
            }
            if (state.rstate.stage == 15)
            {
                goto lbl_15;
            }
            if (state.rstate.stage == 16)
            {
                goto lbl_16;
            }
            if (state.rstate.stage == 17)
            {
                goto lbl_17;
            }
            if (state.rstate.stage == 18)
            {
                goto lbl_18;
            }

            //
            // Routine body
            //

            //
            // prepare
            //
            n = state.n;
            m = state.m;
            state.repiterationscount = 0;
            state.repterminationtype = 0;
            state.repfuncidx = -1;
            state.repvaridx = -1;
            state.repnfunc = 0;
            state.repnjac = 0;
            state.repngrad = 0;
            state.repnhess = 0;
            state.repncholesky = 0;

            //
            // check consistency of constraints,
            // enforce feasibility of the solution
            // set constraints
            //

            

            if (!Utilities.EnforceBoundaryConstraints(ref state.xbase, state.bndl, state.havebndl, state.bndu, state.havebndu, n, 0))
            {
                state.repterminationtype = -3;
                result = false;
                return result;
            }

            

            MinQuadraticProgramming.SetBoundaryConstraints(state.qpState, state.bndl, state.bndu);

            //
            //  Check, that transferred derivative value is right
            //
            ClearRequestFields(state);
            if (!(state.algomode == 1 && state.teststep > 0))
            {
                goto lbl_19;
            }
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.x[i_] = state.xbase[i_];
            }
            state.needfij = true;
            i = 0;
        lbl_21:
            if (i > n - 1)
            {
                goto lbl_23;
            }
            Utilities.Assert((state.havebndl[i] && state.bndl[i] <= state.x[i]) || !state.havebndl[i], "MinLM: internal error(State.X is out of bounds)");
            Utilities.Assert((state.havebndu[i] && state.x[i] <= state.bndu[i]) || !state.havebndu[i], "MinLMIteration: internal error(State.X is out of bounds)");
            v = state.x[i];
            state.x[i] = v - state.teststep * state.s[i];
            if (state.havebndl[i])
            {
                state.x[i] = Math.Max(state.x[i], state.bndl[i]);
            }
            state.xm1 = state.x[i];
            state.rstate.stage = 0;
            goto lbl_rcomm;
        lbl_0:
            for (i_ = 0; i_ <= m - 1; i_++)
            {
                state.fm1[i_] = state.fi[i_];
            }
            for (i_ = 0; i_ <= m - 1; i_++)
            {
                state.gm1[i_] = state.j[i_, i];
            }
            state.x[i] = v + state.teststep * state.s[i];
            if (state.havebndu[i])
            {
                state.x[i] = Math.Min(state.x[i], state.bndu[i]);
            }
            state.xp1 = state.x[i];
            state.rstate.stage = 1;
            goto lbl_rcomm;
        lbl_1:
            for (i_ = 0; i_ <= m - 1; i_++)
            {
                state.fp1[i_] = state.fi[i_];
            }
            for (i_ = 0; i_ <= m - 1; i_++)
            {
                state.gp1[i_] = state.j[i_, i];
            }
            state.x[i] = (state.xm1 + state.xp1) / 2;
            if (state.havebndl[i])
            {
                state.x[i] = Math.Max(state.x[i], state.bndl[i]);
            }
            if (state.havebndu[i])
            {
                state.x[i] = Math.Min(state.x[i], state.bndu[i]);
            }
            state.rstate.stage = 2;
            goto lbl_rcomm;
        lbl_2:
            for (i_ = 0; i_ <= m - 1; i_++)
            {
                state.fc1[i_] = state.fi[i_];
            }
            for (i_ = 0; i_ <= m - 1; i_++)
            {
                state.gc1[i_] = state.j[i_, i];
            }
            state.x[i] = v;
            for (k = 0; k <= m - 1; k++)
            {
                if (!Utilities.DerivativeCheck(state.fm1[k], state.gm1[k], state.fp1[k], state.gp1[k], state.fc1[k], state.gc1[k], state.xp1 - state.xm1))
                {
                    state.repfuncidx = k;
                    state.repvaridx = i;
                    state.repterminationtype = -7;
                    result = false;
                    return result;
                }
            }
            i = i + 1;
            goto lbl_21;
        lbl_23:
            state.needfij = false;
        lbl_19:

            //
            // Initial report of current point
            //
            // Note 1: we rewrite State.X twice because
            // user may accidentally change it after first call.
            //
            // Note 2: we set NeedF or NeedFI depending on what
            // information about function we have.
            //
            if (!state.xrep)
            {
                goto lbl_24;
            }
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.x[i_] = state.xbase[i_];
            }
            ClearRequestFields(state);
            if (!state.hasf)
            {
                goto lbl_26;
            }
            state.needf = true;
            state.rstate.stage = 3;
            goto lbl_rcomm;
        lbl_3:
            state.needf = false;
            goto lbl_27;
        lbl_26:
            Utilities.Assert(state.hasfi, "MinLM: internal error 2!");
            state.needfi = true;
            state.rstate.stage = 4;
            goto lbl_rcomm;
        lbl_4:
            state.needfi = false;
            v = 0.0;
            for (i_ = 0; i_ <= m - 1; i_++)
            {
                v += state.fi[i_] * state.fi[i_];
            }
            state.f = v;
        lbl_27:
            state.repnfunc = state.repnfunc + 1;
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.x[i_] = state.xbase[i_];
            }
            ClearRequestFields(state);
            state.xupdated = true;
            state.rstate.stage = 5;
            goto lbl_rcomm;
        lbl_5:
            state.xupdated = false;
        lbl_24:

            //
            // Prepare control variables
            //
            state.nu = 1;
            state.lambdav = -MathExtensions.MaxRealNumber;
            state.modelage = state.maxmodelage + 1;
            state.deltaxready = false;
            state.deltafready = false;

            //
        // Main cycle.
        //
        // We move through it until either:
        // * one of the stopping conditions is met
        // * we decide that stopping conditions are too stringent
        //   and break from cycle
        //
        //
        lbl_28:
            if (false)
            {
                goto lbl_29;
            }

            //
            // First, we have to prepare quadratic model for our function.
            // We use BFlag to ensure that model is prepared;
            // if it is false at the end of this block, something went wrong.
            //
            // We may either calculate brand new model or update old one.
            //
            // Before this block we have:
            // * State.XBase            - current position.
            // * State.DeltaX           - if DeltaXReady is True
            // * State.DeltaF           - if DeltaFReady is True
            //
            // After this block is over, we will have:
            // * State.XBase            - base point (unchanged)
            // * State.FBase            - F(XBase)
            // * State.GBase            - linear term
            // * State.QuadraticModel   - quadratic term
            // * State.LambdaV          - current estimate for lambda
            //
            // We also clear DeltaXReady/DeltaFReady flags
            // after initialization is done.
            //
            bflag = false;
            if (!(state.algomode == 0 || state.algomode == 1))
            {
                goto lbl_30;
            }

            //
            // Calculate f[] and Jacobian
            //
            if (!(state.modelage > state.maxmodelage || !(state.deltaxready && state.deltafready)))
            {
                goto lbl_32;
            }

            //
            // Refresh model (using either finite differences or analytic Jacobian)
            //
            if (state.algomode != 0)
            {
                goto lbl_34;
            }

            //
            // Optimization using F values only.
            // Use finite differences to estimate Jacobian.
            //
            Utilities.Assert(state.hasfi, "MinLMIteration: internal error when estimating Jacobian (no f[])");
            k = 0;
        lbl_36:
            if (k > n - 1)
            {
                goto lbl_38;
            }

            //
            // We guard X[k] from leaving [BndL,BndU].
            // In case BndL=BndU, we assume that derivative in this direction is zero.
            //
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.x[i_] = state.xbase[i_];
            }
            state.x[k] = state.x[k] - state.s[k] * state.diffstep;
            if (state.havebndl[k])
            {
                state.x[k] = Math.Max(state.x[k], state.bndl[k]);
            }
            if (state.havebndu[k])
            {
                state.x[k] = Math.Min(state.x[k], state.bndu[k]);
            }
            state.xm1 = state.x[k];
            ClearRequestFields(state);
            state.needfi = true;
            state.rstate.stage = 6;
            goto lbl_rcomm;
        lbl_6:
            state.repnfunc = state.repnfunc + 1;
            for (i_ = 0; i_ <= m - 1; i_++)
            {
                state.fm1[i_] = state.fi[i_];
            }
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.x[i_] = state.xbase[i_];
            }
            state.x[k] = state.x[k] + state.s[k] * state.diffstep;
            if (state.havebndl[k])
            {
                state.x[k] = Math.Max(state.x[k], state.bndl[k]);
            }
            if (state.havebndu[k])
            {
                state.x[k] = Math.Min(state.x[k], state.bndu[k]);
            }
            state.xp1 = state.x[k];
            ClearRequestFields(state);
            state.needfi = true;
            state.rstate.stage = 7;
            goto lbl_rcomm;
        lbl_7:
            state.repnfunc = state.repnfunc + 1;
            for (i_ = 0; i_ <= m - 1; i_++)
            {
                state.fp1[i_] = state.fi[i_];
            }
            v = state.xp1 - state.xm1;
            if (v != 0)
            {
                v = 1 / v;
                for (i_ = 0; i_ <= m - 1; i_++)
                {
                    state.j[i_, k] = v * state.fp1[i_];
                }
                for (i_ = 0; i_ <= m - 1; i_++)
                {
                    state.j[i_, k] = state.j[i_, k] - v * state.fm1[i_];
                }
            }
            else
            {
                for (i = 0; i <= m - 1; i++)
                {
                    state.j[i, k] = 0;
                }
            }
            k = k + 1;
            goto lbl_36;
        lbl_38:

            //
            // Calculate F(XBase)
            //
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.x[i_] = state.xbase[i_];
            }
            ClearRequestFields(state);
            state.needfi = true;
            state.rstate.stage = 8;
            goto lbl_rcomm;
        lbl_8:
            state.needfi = false;
            state.repnfunc = state.repnfunc + 1;
            state.repnjac = state.repnjac + 1;

            //
            // New model
            //
            state.modelage = 0;
            goto lbl_35;
        lbl_34:

            //
            // Obtain f[] and Jacobian
            //
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.x[i_] = state.xbase[i_];
            }
            ClearRequestFields(state);
            state.needfij = true;
            state.rstate.stage = 9;
            goto lbl_rcomm;
        lbl_9:
            state.needfij = false;
            state.repnfunc = state.repnfunc + 1;
            state.repnjac = state.repnjac + 1;

            //
            // New model
            //
            state.modelage = 0;
        lbl_35:
            goto lbl_33;
        lbl_32:

            //
            // State.J contains Jacobian or its current approximation;
            // refresh it using secant updates:
            //
            // f(x0+dx) = f(x0) + J*dx,
            // J_new = J_old + u*h'
            // h = x_new-x_old
            // u = (f_new - f_old - J_old*h)/(h'h)
            //
            // We can explicitly generate h and u, but it is
            // preferential to do in-place calculations. Only
            // I-th row of J_old is needed to calculate u[I],
            // so we can update J row by row in one pass.
            //
            // NOTE: we expect that State.XBase contains new point,
            // State.FBase contains old point, State.DeltaX and
            // State.DeltaY contain updates from last step.
            //
            Utilities.Assert(state.deltaxready && state.deltafready, "MinLMIteration: uninitialized DeltaX/DeltaF");
            t = 0.0;
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                t += state.deltax[i_] * state.deltax[i_];
            }
            Utilities.Assert(t != 0, "MinLM: internal error (T=0)");
            for (i = 0; i <= m - 1; i++)
            {
                v = 0.0;
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    v += state.j[i, i_] * state.deltax[i_];
                }
                v = (state.deltaf[i] - v) / t;
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    state.j[i, i_] = state.j[i, i_] + v * state.deltax[i_];
                }
            }
            for (i_ = 0; i_ <= m - 1; i_++)
            {
                state.fi[i_] = state.fibase[i_];
            }
            for (i_ = 0; i_ <= m - 1; i_++)
            {
                state.fi[i_] = state.fi[i_] + state.deltaf[i_];
            }

            //
            // Increase model age
            //
            state.modelage = state.modelage + 1;
        lbl_33:

            //
            // Generate quadratic model:
            //     f(xbase+dx) =
            //       = (f0 + J*dx)'(f0 + J*dx)
            //       = f0^2 + dx'J'f0 + f0*J*dx + dx'J'J*dx
            //       = f0^2 + 2*f0*J*dx + dx'J'J*dx
            //
            // Note that we calculate 2*(J'J) instead of J'J because
            // our quadratic model is based on Tailor decomposition,
            // i.e. it has 0.5 before quadratic term.
            //
            Ablas.RealGemm(n, n, m, 2.0, state.j, 0, 0, 1, state.j, 0, 0, 0, 0.0, state.quadraticmodel, 0, 0);
            Ablas.RealVectorProduct(n, m, state.j, 0, 0, 1, state.fi, 0, ref state.gbase, 0);
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.gbase[i_] = 2 * state.gbase[i_];
            }
            v = 0.0;
            for (i_ = 0; i_ <= m - 1; i_++)
            {
                v += state.fi[i_] * state.fi[i_];
            }
            state.fbase = v;
            for (i_ = 0; i_ <= m - 1; i_++)
            {
                state.fibase[i_] = state.fi[i_];
            }

            //
            // set control variables
            //
            bflag = true;
        lbl_30:
            if (state.algomode != 2)
            {
                goto lbl_39;
            }
            Utilities.Assert(!state.hasfi, "MinLMIteration: internal error (HasFI is True in Hessian-based mode)");

            //
            // Obtain F, G, H
            //
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.x[i_] = state.xbase[i_];
            }
            ClearRequestFields(state);
            state.needfgh = true;
            state.rstate.stage = 10;
            goto lbl_rcomm;
        lbl_10:
            state.needfgh = false;
            state.repnfunc = state.repnfunc + 1;
            state.repngrad = state.repngrad + 1;
            state.repnhess = state.repnhess + 1;
            Ablas.MatrixCopy(n, n, state.h, 0, 0, ref state.quadraticmodel, 0, 0);
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.gbase[i_] = state.g[i_];
            }
            state.fbase = state.f;

            //
            // set control variables
            //
            bflag = true;
            state.modelage = 0;
        lbl_39:
            Utilities.Assert(bflag, "MinLM: internal integrity check failed!");
            state.deltaxready = false;
            state.deltafready = false;

            //
            // If Lambda is not initialized, initialize it using quadratic model
            //
            if (state.lambdav < 0)
            {
                state.lambdav = 0;
                for (i = 0; i <= n - 1; i++)
                {
                    state.lambdav = Math.Max(state.lambdav, Math.Abs(state.quadraticmodel[i, i]) * MathExtensions.Squared(state.s[i]));
                }
                state.lambdav = 0.001 * state.lambdav;
                if (state.lambdav == 0)
                {
                    state.lambdav = 1;
                }
            }

            //
            // Test stopping conditions for function gradient
            //
            if (BoundedScaledAntiGradNorm(state, state.xbase, state.gbase) > state.epsg)
            {
                goto lbl_41;
            }
            if (state.modelage != 0)
            {
                goto lbl_43;
            }

            //
            // Model is fresh, we can rely on it and terminate algorithm
            //
            state.repterminationtype = 4;
            if (!state.xrep)
            {
                goto lbl_45;
            }
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.x[i_] = state.xbase[i_];
            }
            state.f = state.fbase;
            ClearRequestFields(state);
            state.xupdated = true;
            state.rstate.stage = 11;
            goto lbl_rcomm;
        lbl_11:
            state.xupdated = false;
        lbl_45:
            result = false;
            return result;
            goto lbl_44;
        lbl_43:

            //
            // Model is not fresh, we should refresh it and test
            // conditions once more
            //
            state.modelage = state.maxmodelage + 1;
            goto lbl_28;
        lbl_44:
        lbl_41:

            //
            // Find value of Levenberg-Marquardt damping parameter which:
            // * leads to positive definite damped model
            // * within bounds specified by StpMax
            // * generates step which decreases function value
            //
            // After this block IFlag is set to:
            // * -3, if constraints are infeasible
            // * -2, if model update is needed (either Lambda growth is too large
            //       or step is too short, but we can't rely on model and stop iterations)
            // * -1, if model is fresh, Lambda have grown too large, termination is needed
            // *  0, if everything is OK, continue iterations
            //
            // State.Nu can have any value on enter, but after exit it is set to 1.0
            //
            iflag = -99;
        lbl_47:
            if (false)
            {
                goto lbl_48;
            }

            //
            // Do we need model update?
            //
            if (state.modelage > 0 && state.nu >= SuspiciousNu)
            {
                iflag = -2;
                goto lbl_48;
            }

            //
            // Setup quadratic solver and solve quadratic programming problem.
            // After problem is solved we'll try to bound step by StpMax
            // (Lambda will be increased if step size is too large).
            //
            // We use BFlag variable to indicate that we have to increase Lambda.
            // If it is False, we will try to increase Lambda and move to new iteration.
            //
            bflag = true;
            MinQuadraticProgramming.SetStartingPoint(state.qpState, state.xbase);
            MinQuadraticProgramming.SetOrigin(state.qpState, state.xbase);
            MinQuadraticProgramming.SetLinearTerm(state.qpState, state.gbase);
            MinQuadraticProgramming.SetQuadraticTerm(state.qpState, state.quadraticmodel, true, 0.0);
            for (i = 0; i <= n - 1; i++)
            {
                state.tmp0[i] = state.quadraticmodel[i, i] + state.lambdav / MathExtensions.Squared(state.s[i]);
            }
            MinQuadraticProgramming.RewriteDiagonal(state.qpState, state.tmp0);
            MinQuadraticProgramming.Optimize(state.qpState);
            MinQuadraticProgramming.ResultsBuffered(state.qpState, ref state.xdir, state.qpReport);
            
            if (state.qpReport.terminationtype > 0)
            {

                //
                // successful solution of QP problem
                //
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    state.xdir[i_] = state.xdir[i_] - state.xbase[i_];
                }
                v = 0.0;
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    v += state.xdir[i_] * state.xdir[i_];
                }
                if (MathExtensions.IsFinite(v))
                {
                    v = Math.Sqrt(v);
                    if (state.stpmax > 0 && v > state.stpmax)
                    {
                        bflag = false;
                    }
                }
                else
                {
                    bflag = false;
                }
            }
            else
            {

                //
                // Either problem is non-convex (increase LambdaV) or constraints are inconsistent
                //
                Utilities.Assert(state.qpReport.terminationtype == -3 || state.qpReport.terminationtype == -5, "MinLM: unexpected completion code from QP solver");
                if (state.qpReport.terminationtype == -3)
                {
                    iflag = -3;
                    goto lbl_48;
                }
                bflag = false;
            }
            if (!bflag)
            {

                //
                // Solution failed:
                // try to increase lambda to make matrix positive definite and continue.
                //
                if (!IncreaseLambda(ref state.lambdav, ref state.nu))
                {
                    iflag = -1;
                    goto lbl_48;
                }
                goto lbl_47;
            }

            //
            // Step in State.XDir and it is bounded by StpMax.
            //
            // We should check stopping conditions on step size here.
            // DeltaX, which is used for secant updates, is initialized here.
            //
            // This code is a bit tricky because sometimes XDir<>0, but
            // it is so small that XDir+XBase==XBase (in finite precision
            // arithmetics). So we set DeltaX to XBase, then
            // add XDir, and then subtract XBase to get exact value of
            // DeltaX.
            //
            // Step length is estimated using DeltaX.
            //
            // NOTE: stopping conditions are tested
            // for fresh models only (ModelAge=0)
            //
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.deltax[i_] = state.xbase[i_];
            }
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.deltax[i_] = state.deltax[i_] + state.xdir[i_];
            }
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.deltax[i_] = state.deltax[i_] - state.xbase[i_];
            }
            state.deltaxready = true;
            v = 0.0;
            for (i = 0; i <= n - 1; i++)
            {
                v = v + MathExtensions.Squared(state.deltax[i] / state.s[i]);
            }
            v = Math.Sqrt(v);
            if (v > state.epsx)
            {
                goto lbl_49;
            }
            if (state.modelage != 0)
            {
                goto lbl_51;
            }

            //
            // Step is too short, model is fresh and we can rely on it.
            // Terminating.
            //
            state.repterminationtype = 2;
            if (!state.xrep)
            {
                goto lbl_53;
            }
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.x[i_] = state.xbase[i_];
            }
            state.f = state.fbase;
            ClearRequestFields(state);
            state.xupdated = true;
            state.rstate.stage = 12;
            goto lbl_rcomm;
        lbl_12:
            state.xupdated = false;
        lbl_53:
            result = false;
            return result;
            goto lbl_52;
        lbl_51:

            //
            // Step is suspiciously short, but model is not fresh
            // and we can't rely on it.
            //
            iflag = -2;
            goto lbl_48;
        lbl_52:
        lbl_49:

            //
            // Let's evaluate new step:
            // a) if we have Fi vector, we evaluate it using rcomm, and
            //    then we manually calculate State.F as sum of squares of Fi[]
            // b) if we have F value, we just evaluate it through rcomm interface
            //
            // We prefer (a) because we may need Fi vector for additional
            // iterations
            //
            Utilities.Assert(state.hasfi || state.hasf, "MinLM: internal error 2!");
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.x[i_] = state.xbase[i_];
            }
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.x[i_] = state.x[i_] + state.xdir[i_];
            }
            ClearRequestFields(state);
            if (!state.hasfi)
            {
                goto lbl_55;
            }
            state.needfi = true;
            state.rstate.stage = 13;
            goto lbl_rcomm;
        lbl_13:
            state.needfi = false;
            v = 0.0;
            for (i_ = 0; i_ <= m - 1; i_++)
            {
                v += state.fi[i_] * state.fi[i_];
            }
            state.f = v;
            for (i_ = 0; i_ <= m - 1; i_++)
            {
                state.deltaf[i_] = state.fi[i_];
            }
            for (i_ = 0; i_ <= m - 1; i_++)
            {
                state.deltaf[i_] = state.deltaf[i_] - state.fibase[i_];
            }
            state.deltafready = true;
            goto lbl_56;
        lbl_55:
            state.needf = true;
            state.rstate.stage = 14;
            goto lbl_rcomm;
        lbl_14:
            state.needf = false;
        lbl_56:
            state.repnfunc = state.repnfunc + 1;
            if (state.f >= state.fbase)
            {

                //
                // Increase lambda and continue
                //
                if (!IncreaseLambda(ref state.lambdav, ref state.nu))
                {
                    iflag = -1;
                    goto lbl_48;
                }
                goto lbl_47;
            }

            //
            // We've found our step!
            //
            iflag = 0;
            goto lbl_48;
            goto lbl_47;
        lbl_48:
            state.nu = 1;
            Utilities.Assert(iflag >= -3 && iflag <= 0, "MinLM: internal integrity check failed!");
            if (iflag == -3)
            {
                state.repterminationtype = -3;
                result = false;
                return result;
            }
            if (iflag == -2)
            {
                state.modelage = state.maxmodelage + 1;
                goto lbl_28;
            }
            if (iflag == -1)
            {
                goto lbl_29;
            }

            //
            // Levenberg-Marquardt step is ready.
            // Compare predicted vs. actual decrease and decide what to do with lambda.
            //
            // NOTE: we expect that State.DeltaX contains direction of step,
            // State.F contains function value at new point.
            //
            Utilities.Assert(state.deltaxready, "MinLM: deltaX is not ready");
            t = 0;
            for (i = 0; i <= n - 1; i++)
            {
                v = 0.0;
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    v += state.quadraticmodel[i, i_] * state.deltax[i_];
                }
                t = t + state.deltax[i] * state.gbase[i] + 0.5 * state.deltax[i] * v;
            }
            state.predicteddecrease = -t;
            state.actualdecrease = -(state.f - state.fbase);
            if (state.predicteddecrease <= 0)
            {
                goto lbl_29;
            }
            v = state.actualdecrease / state.predicteddecrease;
            if (v >= 0.1)
            {
                goto lbl_57;
            }
            if (IncreaseLambda(ref state.lambdav, ref state.nu))
            {
                goto lbl_59;
            }

            //
            // Lambda is too large, we have to break iterations.
            //
            state.repterminationtype = 7;
            if (!state.xrep)
            {
                goto lbl_61;
            }
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.x[i_] = state.xbase[i_];
            }
            state.f = state.fbase;
            ClearRequestFields(state);
            state.xupdated = true;
            state.rstate.stage = 15;
            goto lbl_rcomm;
        lbl_15:
            state.xupdated = false;
        lbl_61:
            result = false;
            return result;
        lbl_59:
        lbl_57:
            if (v > 0.5)
            {
                DecreaseLambda(ref state.lambdav, ref state.nu);
            }

            //
            // Accept step, report it and
            // test stopping conditions on iterations count and function decrease.
            //
            // NOTE: we expect that State.DeltaX contains direction of step,
            // State.F contains function value at new point.
            //
            // NOTE2: we should update XBase ONLY. In the beginning of the next
            // iteration we expect that State.FIBase is NOT updated and
            // contains old value of a function vector.
            //
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.xbase[i_] = state.xbase[i_] + state.deltax[i_];
            }
            if (!state.xrep)
            {
                goto lbl_63;
            }
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.x[i_] = state.xbase[i_];
            }
            ClearRequestFields(state);
            state.xupdated = true;
            state.rstate.stage = 16;
            goto lbl_rcomm;
        lbl_16:
            state.xupdated = false;
        lbl_63:
            state.repiterationscount = state.repiterationscount + 1;
            if (state.repiterationscount >= state.maxits && state.maxits > 0)
            {
                state.repterminationtype = 5;
            }
            if (state.modelage == 0)
            {
                if (Math.Abs(state.f - state.fbase) <= state.epsf * Math.Max(1, Math.Max(Math.Abs(state.f), Math.Abs(state.fbase))))
                {
                    state.repterminationtype = 1;
                }
            }
            if (state.repterminationtype <= 0)
            {
                goto lbl_65;
            }
            if (!state.xrep)
            {
                goto lbl_67;
            }

            //
            // Report: XBase contains new point, F contains function value at new point
            //
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.x[i_] = state.xbase[i_];
            }
            ClearRequestFields(state);
            state.xupdated = true;
            state.rstate.stage = 17;
            goto lbl_rcomm;
        lbl_17:
            state.xupdated = false;
        lbl_67:
            result = false;
            return result;
        lbl_65:
            state.modelage = state.modelage + 1;
            goto lbl_28;
        lbl_29:

            //
            // Lambda is too large, we have to break iterations.
            //
            state.repterminationtype = 7;
            if (!state.xrep)
            {
                goto lbl_69;
            }
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.x[i_] = state.xbase[i_];
            }
            state.f = state.fbase;
            ClearRequestFields(state);
            state.xupdated = true;
            state.rstate.stage = 18;
            goto lbl_rcomm;
        lbl_18:
            state.xupdated = false;
        lbl_69:
            result = false;
            return result;

            //
        // Saving state
        //
        lbl_rcomm:
            result = true;
            state.rstate.ia[0] = n;
            state.rstate.ia[1] = m;
            state.rstate.ia[2] = iflag;
            state.rstate.ia[3] = i;
            state.rstate.ia[4] = k;
            state.rstate.ba[0] = bflag;
            state.rstate.ra[0] = v;
            state.rstate.ra[1] = s;
            state.rstate.ra[2] = t;
            return result;
        }

        /*************************************************************************
        This function is used to change acceleration settings

        You can choose between three acceleration strategies:
        * AccType=0, no acceleration.
        * AccType=1, secant updates are used to update quadratic model after  each
          iteration. After fixed number of iterations (or after  model  breakdown)
          we  recalculate  quadratic  model  using  analytic  Jacobian  or  finite
          differences. Number of secant-based iterations depends  on  optimization
          settings: about 3 iterations - when we have analytic Jacobian, up to 2*N
          iterations - when we use finite differences to calculate Jacobian.

        AccType=1 is recommended when Jacobian  calculation  cost  is  prohibitive
        high (several Mx1 function vector calculations  followed  by  several  NxN
        Cholesky factorizations are faster than calculation of one M*N  Jacobian).
        It should also be used when we have no Jacobian, because finite difference
        approximation takes too much time to compute.

        Table below list  optimization  protocols  (XYZ  protocol  corresponds  to
        MinLMCreateXYZ) and acceleration types they support (and use by  default).

        ACCELERATION TYPES SUPPORTED BY OPTIMIZATION PROTOCOLS:

        protocol    0   1   comment
        V           +   +
        VJ          +   +
        FGH         +

        DAFAULT VALUES:

        protocol    0   1   comment
        V               x   without acceleration it is so slooooooooow
        VJ          x
        FGH         x

        NOTE: this  function should be called before optimization. Attempt to call
        it during algorithm iterations may result in unexpected behavior.

        NOTE: attempt to call this function with unsupported protocol/acceleration
        combination will result in exception being thrown.

          -- ALGLIB --
             Copyright 14.10.2010 by Bochkanov Sergey
        *************************************************************************/
        private static void SetAccelerationType(State state, int acctype)
        {
            Utilities.Assert((acctype == 0 || acctype == 1) || acctype == 2, "MinLMSetAccType: incorrect AccType!");
            if (acctype == 2)
            {
                acctype = 0;
            }
            if (acctype == 0)
            {
                state.maxmodelage = 0;
                state.makeadditers = false;
                return;
            }
            if (acctype == 1)
            {
                Utilities.Assert(state.hasfi, "MinLMSetAccType: AccType=1 is incompatible with current protocol!");
                if (state.algomode == 0)
                {
                    state.maxmodelage = 2 * state.n;
                }
                else
                {
                    state.maxmodelage = SmallModelAge;
                }
                state.makeadditers = false;
                return;
            }
        }


        /*************************************************************************
        Levenberg-Marquardt algorithm results

        INPUT PARAMETERS:
            State   -   algorithm state

        OUTPUT PARAMETERS:
            X       -   array[0..N-1], solution
            Rep     -   optimization report;
                        see comments for this structure for more info.

          -- ALGLIB --
             Copyright 10.03.2009 by Bochkanov Sergey
        *************************************************************************/
        public static void GetResults(State state, ref FluxList<double> x, Report rep)
        {
            x.Resize(0);

            GetResultsBuffered(state, ref x, rep);
        }


        /*************************************************************************
        Levenberg-Marquardt algorithm results

        Buffered implementation of MinLMResults(), which uses pre-allocated buffer
        to store X[]. If buffer size is  too  small,  it  resizes  buffer.  It  is
        intended to be used in the inner cycles of performance critical algorithms
        where array reallocation penalty is too large to be ignored.

          -- ALGLIB --
             Copyright 10.03.2009 by Bochkanov Sergey
        *************************************************************************/
        private static void GetResultsBuffered(State state, ref FluxList<double> x, Report rep)
        {
            int i_ = 0;

            if (x.Count < state.n)
            {
                x.Resize(state.n);
            }
            for (i_ = 0; i_ <= state.n - 1; i_++)
            {
                x[i_] = state.x[i_];
            }
            rep.iterationscount = state.repiterationscount;
            rep.terminationtype = state.repterminationtype;
            rep.funcidx = state.repfuncidx;
            rep.varidx = state.repvaridx;
            rep.nfunc = state.repnfunc;
            rep.njac = state.repnjac;
            rep.ngrad = state.repngrad;
            rep.nhess = state.repnhess;
            rep.ncholesky = state.repncholesky;
        }


        /*************************************************************************
        This  subroutine  restarts  LM  algorithm from new point. All optimization
        parameters are left unchanged.

        This  function  allows  to  solve multiple  optimization  problems  (which
        must have same number of dimensions) without object reallocation penalty.

        INPUT PARAMETERS:
            State   -   structure used for reverse communication previously
                        allocated with MinLMCreateXXX call.
            X       -   new starting point.

          -- ALGLIB --
             Copyright 30.07.2010 by Bochkanov Sergey
        *************************************************************************/
        private static void RestartFrom(State state, IList<double> x)
        {
            int i_ = 0;

            Utilities.Assert(x.Count >= state.n, "MinLMRestartFrom: Length(X)<N!");
            Utilities.Assert(Utilities.IsFiniteVector(x, state.n), "MinLMRestartFrom: X contains infinite or NaN values!");
            for (i_ = 0; i_ <= state.n - 1; i_++)
            {
                state.xbase[i_] = x[i_];
            }
            state.rstate.ia.Resize(5);
            state.rstate.ba.Resize(1);
            state.rstate.ra.Resize(3);
            state.rstate.stage = -1;
            ClearRequestFields(state);
        }


        /*************************************************************************
        Prepare internal structures (except for RComm).

        Note: M must be zero for FGH mode, non-zero for V/VJ/FJ/FGJ mode.
        *************************************************************************/
        private static void Prepare(int n, int m, bool havegrad, State state)
        {
            int i = 0;

            if (n <= 0 || m < 0)
            {
                return;
            }
            if (havegrad)
            {
                state.g.Resize(n);
            }
            if (m != 0)
            {
                state.j.Resize(m, n);
                state.fi.Resize(m);
                state.fibase.Resize(m);
                state.deltaf.Resize(m);
                state.fm1.Resize(m);
                state.fp1.Resize(m);
                state.fc1.Resize(m);
                state.gm1.Resize(m);
                state.gp1.Resize(m);
                state.gc1.Resize(m);
            }
            else
            {
                state.h.Resize(n, n);
            }
            //state.x = new double[n];
            state.x.Resize(n);
            state.deltax.Resize(n);
            state.quadraticmodel.Resize(n, n);
            state.xbase.Resize(n);
            state.gbase.Resize(n);
            state.xdir.Resize(n);
            state.tmp0.Resize(n);

            //
            // prepare internal L-BFGS
            //
            for (i = 0; i <= n - 1; i++)
            {
                state.x[i] = 0;
            }
            MinLbfgs.MinLbfgsCreate(n, Math.Min(AddIterations, n), state.x, state.lbfgsState);
            MinLbfgs.MinLbfgsSetConditions(state.lbfgsState, 0.0, 0.0, 0.0, Math.Min(AddIterations, n));

            //
            // Prepare internal QP solver
            //
            MinQuadraticProgramming.Create(n, state.qpState);
            MinQuadraticProgramming.SetCholeskyAlgorithm(state.qpState);

            //
            // Prepare boundary constraints
            //
            state.bndl.Resize(n);
            state.bndu.Resize(n);
            state.havebndl.Resize(n);
            state.havebndu.Resize(n);
            for (i = 0; i <= n - 1; i++)
            {
                state.bndl[i] = Double.NegativeInfinity;
                state.havebndl[i] = false;
                state.bndu[i] = Double.PositiveInfinity;
                state.havebndu[i] = false;
            }

            //
            // Prepare scaling matrix
            //
            state.s.Resize(n);
            for (i = 0; i <= n - 1; i++)
            {
                state.s[i] = 1.0;
            }
        }


        /*************************************************************************
        Clears request fileds (to be sure that we don't forgot to clear something)
        *************************************************************************/
        private static void ClearRequestFields(State state)
        {
            state.needf = false;
            state.needfg = false;
            state.needfgh = false;
            state.needfij = false;
            state.needfi = false;
            state.xupdated = false;
        }

        /*************************************************************************
    Increases lambda, returns False when there is a danger of overflow
    *************************************************************************/
        private static bool IncreaseLambda(ref double lambdav, ref double nu)
        {
            bool result = new bool();
            double lnlambda = 0;
            double lnnu = 0;
            double lnlambdaup = 0;
            double lnmax = 0;

            result = false;
            lnlambda = Math.Log(lambdav);
            lnlambdaup = Math.Log(LambdaUp);
            lnnu = Math.Log(nu);
            lnmax = Math.Log(MathExtensions.MaxRealNumber);
            if (lnlambda + lnlambdaup + lnnu > 0.25 * lnmax)
            {
                return result;
            }
            if (lnnu + Math.Log(2) > lnmax)
            {
                return result;
            }
            lambdav = lambdav * LambdaUp * nu;
            nu = nu * 2;
            result = true;
            return result;
        }


        /*************************************************************************
        Decreases lambda, but leaves it unchanged when there is danger of underflow.
        *************************************************************************/
        private static void DecreaseLambda(ref double lambdav, ref double nu)
        {
            nu = 1;
            if (Math.Log(lambdav) + Math.Log(LambdaDown) < Math.Log(MathExtensions.MinRealNumber))
            {
                lambdav = MathExtensions.MinRealNumber;
            }
            else
            {
                lambdav = lambdav * LambdaDown;
            }
        }

        private static double BoundedScaledAntiGradNorm(State state, FluxList<double> x, FluxList<double> g)
        {
            double result = 0;
            int n = 0;
            int i = 0;
            double v = 0;

            result = 0;
            n = state.n;
            for (i = 0; i <= n - 1; i++)
            {
                v = -(g[i] * state.s[i]);
                if (state.havebndl[i])
                {
                    if (x[i] <= state.bndl[i] && -g[i] < 0)
                    {
                        v = 0;
                    }
                }
                if (state.havebndu[i])
                {
                    if (x[i] >= state.bndu[i] && -g[i] > 0)
                    {
                        v = 0;
                    }
                }
                result = result + MathExtensions.Squared(v);
            }
            result = Math.Sqrt(result);
            return result;
        }

    }
}
