using System;
using LxProcessor.Common;

namespace LxProcessor.Optimizer
{
    public class SingularValueDecomposition
    {
        /*************************************************************************
            Singular value decomposition of a rectangular matrix.

            The algorithm calculates the singular value decomposition of a matrix of
            size MxN: A = U * S * V^T

            The algorithm finds the singular values and, optionally, matrices U and V^T.
            The algorithm can find both first min(M,N) columns of matrix U and rows of
            matrix V^T (singular vectors), and matrices U and V^T wholly (of sizes MxM
            and NxN respectively).

            Take into account that the subroutine does not return matrix V but V^T.

            Input parameters:
                A           -   matrix to be decomposed.
                                Array whose indexes range within [0..M-1, 0..N-1].
                M           -   number of rows in matrix A.
                N           -   number of columns in matrix A.
                UNeeded     -   0, 1 or 2. See the description of the parameter U.
                VTNeeded    -   0, 1 or 2. See the description of the parameter VT.
                AdditionalMemory -
                                If the parameter:
                                 * equals 0, the algorithm doesn�t use additional
                                   memory (lower requirements, lower performance).
                                 * equals 1, the algorithm uses additional
                                   memory of size min(M,N)*min(M,N) of real numbers.
                                   It often speeds up the algorithm.
                                 * equals 2, the algorithm uses additional
                                   memory of size M*min(M,N) of real numbers.
                                   It allows to get a maximum performance.
                                The recommended value of the parameter is 2.

            Output parameters:
                W           -   contains singular values in descending order.
                U           -   if UNeeded=0, U isn't changed, the left singular vectors
                                are not calculated.
                                if Uneeded=1, U contains left singular vectors (first
                                min(M,N) columns of matrix U). Array whose indexes range
                                within [0..M-1, 0..Min(M,N)-1].
                                if UNeeded=2, U contains matrix U wholly. Array whose
                                indexes range within [0..M-1, 0..M-1].
                VT          -   if VTNeeded=0, VT isn�t changed, the right singular vectors
                                are not calculated.
                                if VTNeeded=1, VT contains right singular vectors (first
                                min(M,N) rows of matrix V^T). Array whose indexes range
                                within [0..min(M,N)-1, 0..N-1].
                                if VTNeeded=2, VT contains matrix V^T wholly. Array whose
                                indexes range within [0..N-1, 0..N-1].

              -- ALGLIB --
                 Copyright 2005 by Bochkanov Sergey
            *************************************************************************/
        public static bool Calculate(Matrix<double> a,
            int m,
            int n,
            int uneeded,
            int vtneeded,
            int additionalmemory,
            ref double[] w,
            ref Matrix<double> u,
            ref Matrix<double> vt)
        {
            bool result = new bool();
            double[] tauq = new double[0];
            double[] taup = new double[0];
            double[] tau = new double[0];
            double[] e = new double[0];
            double[] work = new double[0];
            Matrix<double> t2 = new Matrix<double>(0, 0);
            bool isupper = new bool();
            int minmn = 0;
            int ncu = 0;
            int nrvt = 0;
            int nru = 0;
            int ncvt = 0;
            int i = 0;
            int j = 0;

            a = a.Clone();
            w = new double[0];
            u = new Matrix<double>(0, 0);
            vt = new Matrix<double>(0, 0);

            result = true;
            if (m == 0 || n == 0)
            {
                return result;
            }
            Utilities.Assert(uneeded >= 0 && uneeded <= 2, "SVDDecomposition: wrong parameters!");
            Utilities.Assert(vtneeded >= 0 && vtneeded <= 2, "SVDDecomposition: wrong parameters!");
            Utilities.Assert(additionalmemory >= 0 && additionalmemory <= 2, "SVDDecomposition: wrong parameters!");

            //
            // initialize
            //
            minmn = Math.Min(m, n);
            w = new double[minmn + 1];
            ncu = 0;
            nru = 0;
            if (uneeded == 1)
            {
                nru = m;
                ncu = minmn;
                u.Resize(nru - 1 + 1, ncu - 1 + 1);
            }
            if (uneeded == 2)
            {
                nru = m;
                ncu = m;
                u.Resize(nru - 1 + 1, ncu - 1 + 1);
            }
            nrvt = 0;
            ncvt = 0;
            if (vtneeded == 1)
            {
                nrvt = minmn;
                ncvt = n;
                vt.Resize(nrvt - 1 + 1, ncvt - 1 + 1);
            }
            if (vtneeded == 2)
            {
                nrvt = n;
                ncvt = n;
                vt.Resize(nrvt - 1 + 1, ncvt - 1 + 1);
            }

            //
            // M much larger than N
            // Use bidiagonal reduction with QR-decomposition
            //
            if (m > 1.6 * n)
            {
                if (uneeded == 0)
                {

                    //
                    // No left singular vectors to be computed
                    //
                    Ortfac.QrDecomposition(ref a, m, n, ref tau);
                    for (i = 0; i <= n - 1; i++)
                    {
                        for (j = 0; j <= i - 1; j++)
                        {
                            a[i, j] = 0;
                        }
                    }
                    Ortfac.ReductionToBidiagonalForm(ref a, n, n, ref tauq, ref taup);
                    Ortfac.BidiagonalUnpackingP(a, n, n, taup, nrvt, ref vt);
                    Ortfac.BidiagonalUnpackingDiagnols(a, n, n, ref isupper, ref w, ref e);
                    result = Bdmsvd.BidiagonalSingularValueDecomposition(ref w, e, n, isupper, false, ref u, 0, ref a, 0, ref vt, ncvt);
                    return result;
                }
                else
                {

                    //
                    // Left singular vectors (may be full matrix U) to be computed
                    //
                    Ortfac.QrDecomposition(ref a, m, n, ref tau);
                    Ortfac.PartialUnpackingFromQrDecomposition(a, m, n, tau, ncu, ref u);
                    for (i = 0; i <= n - 1; i++)
                    {
                        for (j = 0; j <= i - 1; j++)
                        {
                            a[i, j] = 0;
                        }
                    }
                    Ortfac.ReductionToBidiagonalForm(ref a, n, n, ref tauq, ref taup);
                    Ortfac.BidiagonalUnpackingP(a, n, n, taup, nrvt, ref vt);
                    Ortfac.BidiagonalUnpackingDiagnols(a, n, n, ref isupper, ref w, ref e);
                    if (additionalmemory < 1)
                    {

                        //
                        // No additional memory can be used
                        //
                        Ortfac.BidiagonalMultiplyByQ(a, n, n, tauq, ref u, m, n, true, false);
                        result = Bdmsvd.BidiagonalSingularValueDecomposition(ref w, e, n, isupper, false, ref u, m, ref a, 0, ref vt, ncvt);
                    }
                    else
                    {

                        //
                        // Large U. Transforming intermediate matrix T2
                        //
                        work = new double[Math.Max(m, n) + 1];
                        Ortfac.BidiagonalUnpackingQ(a, n, n, tauq, n, ref t2);
                        Blas.CopyMatrix(u, 0, m - 1, 0, n - 1, ref a, 0, m - 1, 0, n - 1);
                        Blas.InPlaceTranspose(ref t2, 0, n - 1, 0, n - 1, ref work);
                        result = Bdmsvd.BidiagonalSingularValueDecomposition(ref w, e, n, isupper, false, ref u, 0, ref t2, n, ref vt, ncvt);
                        Blas.MatrixMatrixMultiply(a, 0, m - 1, 0, n - 1, false, t2, 0, n - 1, 0, n - 1, true, 1.0, ref u, 0, m - 1, 0, n - 1, 0.0, ref work);
                    }
                    return result;
                }
            }

            //
            // N much larger than M
            // Use bidiagonal reduction with LQ-decomposition
            //
            if (n > 1.6 * m)
            {
                if (vtneeded == 0)
                {

                    //
                    // No right singular vectors to be computed
                    //
                    Ortfac.LqDecomposition(ref a, m, n, ref tau);
                    for (i = 0; i <= m - 1; i++)
                    {
                        for (j = i + 1; j <= m - 1; j++)
                        {
                            a[i, j] = 0;
                        }
                    }
                    Ortfac.ReductionToBidiagonalForm(ref a, m, m, ref tauq, ref taup);
                    Ortfac.BidiagonalUnpackingQ(a, m, m, tauq, ncu, ref u);
                    Ortfac.BidiagonalUnpackingDiagnols(a, m, m, ref isupper, ref w, ref e);
                    work = new double[m + 1];
                    Blas.InPlaceTranspose(ref u, 0, nru - 1, 0, ncu - 1, ref work);
                    result = Bdmsvd.BidiagonalSingularValueDecomposition(ref w, e, m, isupper, false, ref a, 0, ref u, nru, ref vt, 0);
                    Blas.InPlaceTranspose(ref u, 0, nru - 1, 0, ncu - 1, ref work);
                    return result;
                }
                else
                {

                    //
                    // Right singular vectors (may be full matrix VT) to be computed
                    //
                    Ortfac.LqDecomposition(ref a, m, n, ref tau);
                    Ortfac.PartialUnpackingFromLqDecomposition(a, m, n, tau, nrvt, ref vt);
                    for (i = 0; i <= m - 1; i++)
                    {
                        for (j = i + 1; j <= m - 1; j++)
                        {
                            a[i, j] = 0;
                        }
                    }
                    Ortfac.ReductionToBidiagonalForm(ref a, m, m, ref tauq, ref taup);
                    Ortfac.BidiagonalUnpackingQ(a, m, m, tauq, ncu, ref u);
                    Ortfac.BidiagonalUnpackingDiagnols(a, m, m, ref isupper, ref w, ref e);
                    work = new double[Math.Max(m, n) + 1];
                    Blas.InPlaceTranspose(ref u, 0, nru - 1, 0, ncu - 1, ref work);
                    if (additionalmemory < 1)
                    {

                        //
                        // No additional memory available
                        //
                        Ortfac.BidiagonalMultiplyByP(a, m, m, taup, ref vt, m, n, false, true);
                        result = Bdmsvd.BidiagonalSingularValueDecomposition(ref w, e, m, isupper, false, ref a, 0, ref u, nru, ref vt, n);
                    }
                    else
                    {

                        //
                        // Large VT. Transforming intermediate matrix T2
                        //
                        Ortfac.BidiagonalUnpackingP(a, m, m, taup, m, ref t2);
                        result = Bdmsvd.BidiagonalSingularValueDecomposition(ref w, e, m, isupper, false, ref a, 0, ref u, nru, ref t2, m);
                        Blas.CopyMatrix(vt, 0, m - 1, 0, n - 1, ref a, 0, m - 1, 0, n - 1);
                        Blas.MatrixMatrixMultiply(t2, 0, m - 1, 0, m - 1, false, a, 0, m - 1, 0, n - 1, false, 1.0, ref vt, 0, m - 1, 0, n - 1, 0.0, ref work);
                    }
                    Blas.InPlaceTranspose(ref u, 0, nru - 1, 0, ncu - 1, ref work);
                    return result;
                }
            }

            //
            // M<=N
            // We can use inplace transposition of U to get rid of columnwise operations
            //
            if (m <= n)
            {
                Ortfac.ReductionToBidiagonalForm(ref a, m, n, ref tauq, ref taup);
                Ortfac.BidiagonalUnpackingQ(a, m, n, tauq, ncu, ref u);
                Ortfac.BidiagonalUnpackingP(a, m, n, taup, nrvt, ref vt);
                Ortfac.BidiagonalUnpackingDiagnols(a, m, n, ref isupper, ref w, ref e);
                work = new double[m + 1];
                Blas.InPlaceTranspose(ref u, 0, nru - 1, 0, ncu - 1, ref work);
                result = Bdmsvd.BidiagonalSingularValueDecomposition(ref w, e, minmn, isupper, false, ref a, 0, ref u, nru, ref vt, ncvt);
                Blas.InPlaceTranspose(ref u, 0, nru - 1, 0, ncu - 1, ref work);
                return result;
            }

            //
            // Simple bidiagonal reduction
            //
            Ortfac.ReductionToBidiagonalForm(ref a, m, n, ref tauq, ref taup);
            Ortfac.BidiagonalUnpackingQ(a, m, n, tauq, ncu, ref u);
            Ortfac.BidiagonalUnpackingP(a, m, n, taup, nrvt, ref vt);
            Ortfac.BidiagonalUnpackingDiagnols(a, m, n, ref isupper, ref w, ref e);
            if (additionalmemory < 2 || uneeded == 0)
            {

                //
                // We cant use additional memory or there is no need in such operations
                //
                result = Bdmsvd.BidiagonalSingularValueDecomposition(ref w, e, minmn, isupper, false, ref u, nru, ref a, 0, ref vt, ncvt);
            }
            else
            {

                //
                // We can use additional memory
                //
                //t2 = new double[minmn - 1 + 1, m - 1 + 1];
                t2 = new Matrix<double>(minmn - 1 + 1, m - 1 + 1);
                Blas.CopyAndTranspose(u, 0, m - 1, 0, minmn - 1, ref t2, 0, minmn - 1, 0, m - 1);
                result = Bdmsvd.BidiagonalSingularValueDecomposition(ref w, e, minmn, isupper, false, ref u, 0, ref t2, m, ref vt, ncvt);
                Blas.CopyAndTranspose(t2, 0, minmn - 1, 0, m - 1, ref u, 0, m - 1, 0, minmn - 1);
            }
            return result;
        }


    }
}