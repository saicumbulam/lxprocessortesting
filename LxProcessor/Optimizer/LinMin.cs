using System;
using System.Collections.Generic;
using LxCommon.DataStructures;

namespace LxProcessor.Optimizer
{
    public class LinMin
    {
        public class State 
        {
            public bool brackt;
            public bool stage1;
            public int infoc;
            public double dg;
            public double dgm;
            public double dginit;
            public double dgtest;
            public double dgx;
            public double dgxm;
            public double dgy;
            public double dgym;
            public double finit;
            public double ftest1;
            public double fm;
            public double fx;
            public double fxm;
            public double fy;
            public double fym;
            public double stx;
            public double sty;
            public double stmin;
            public double stmax;
            public double width;
            public double width1;
            public double xtrapf;
            
            public State()
            {
                
            }

            internal void CleanForReuse()
            {
                brackt = default(bool);
                stage1 = default(bool);
                infoc = default(int);
                dg = default(double);
                dgm = default(double);;
                dginit = default(double);;
                dgtest = default(double);;
                dgx = default(double);;
                dgxm = default(double);;
                dgy = default(double);;
                dgym = default(double);;
                finit = default(double);;
                ftest1 = default(double);;
                fm = default(double);;
                fx = default(double);;
                fxm = default(double);;
                fy = default(double);;
                fym = default(double);;
                stx = default(double);;
                sty = default(double);;
                stmin = default(double);;
                stmax = default(double);;
                width = default(double);;
                width1 = default(double);;
                xtrapf = default(double);;
            }
        }


        private const double ftol = 0.001;
        private const double xtol = 100 * MathExtensions.MachineEpsilon;
        private const int maxfev = 20;
        private const double stpmin = 1.0E-50;
        private const double defstpmax = 1.0E+50;


        /*************************************************************************
            THE  PURPOSE  OF  MCSRCH  IS  TO  FIND A STEP WHICH SATISFIES A SUFFICIENT
            DECREASE CONDITION AND A CURVATURE CONDITION.

            AT EACH STAGE THE SUBROUTINE  UPDATES  AN  INTERVAL  OF  UNCERTAINTY  WITH
            ENDPOINTS  STX  AND  STY.  THE INTERVAL OF UNCERTAINTY IS INITIALLY CHOSEN
            SO THAT IT CONTAINS A MINIMIZER OF THE MODIFIED FUNCTION

                F(X+STP*S) - F(X) - FTOL*STP*(GRADF(X)'S).

            IF  A STEP  IS OBTAINED FOR  WHICH THE MODIFIED FUNCTION HAS A NONPOSITIVE
            FUNCTION  VALUE  AND  NONNEGATIVE  DERIVATIVE,   THEN   THE   INTERVAL  OF
            UNCERTAINTY IS CHOSEN SO THAT IT CONTAINS A MINIMIZER OF F(X+STP*S).

            THE  ALGORITHM  IS  DESIGNED TO FIND A STEP WHICH SATISFIES THE SUFFICIENT
            DECREASE CONDITION

                F(X+STP*S) .LE. F(X) + FTOL*STP*(GRADF(X)'S),

            AND THE CURVATURE CONDITION

                ABS(GRADF(X+STP*S)'S)) .LE. GTOL*ABS(GRADF(X)'S).

            IF  FTOL  IS  LESS  THAN GTOL AND IF, FOR EXAMPLE, THE FUNCTION IS BOUNDED
            BELOW,  THEN  THERE  IS  ALWAYS  A  STEP  WHICH SATISFIES BOTH CONDITIONS.
            IF  NO  STEP  CAN BE FOUND  WHICH  SATISFIES  BOTH  CONDITIONS,  THEN  THE
            ALGORITHM  USUALLY STOPS  WHEN  ROUNDING ERRORS  PREVENT FURTHER PROGRESS.
            IN THIS CASE STP ONLY SATISFIES THE SUFFICIENT DECREASE CONDITION.


            :::::::::::::IMPORTANT NOTES:::::::::::::

            NOTE 1:

            This routine  guarantees that it will stop at the last point where function
            value was calculated. It won't make several additional function evaluations
            after finding good point. So if you store function evaluations requested by
            this routine, you can be sure that last one is the point where we've stopped.

            NOTE 2:

            when 0<StpMax<StpMin, algorithm will terminate with INFO=5 and Stp=StpMax
            :::::::::::::::::::::::::::::::::::::::::


            PARAMETERS DESCRIPRION

            STAGE IS ZERO ON FIRST CALL, ZERO ON FINAL EXIT

            N IS A POSITIVE INTEGER INPUT VARIABLE SET TO THE NUMBER OF VARIABLES.

            X IS  AN  ARRAY  OF  LENGTH N. ON INPUT IT MUST CONTAIN THE BASE POINT FOR
            THE LINE SEARCH. ON OUTPUT IT CONTAINS X+STP*S.

            F IS  A  VARIABLE. ON INPUT IT MUST CONTAIN THE VALUE OF F AT X. ON OUTPUT
            IT CONTAINS THE VALUE OF F AT X + STP*S.

            G IS AN ARRAY OF LENGTH N. ON INPUT IT MUST CONTAIN THE GRADIENT OF F AT X.
            ON OUTPUT IT CONTAINS THE GRADIENT OF F AT X + STP*S.

            S IS AN INPUT ARRAY OF LENGTH N WHICH SPECIFIES THE SEARCH DIRECTION.

            STP  IS  A NONNEGATIVE VARIABLE. ON INPUT STP CONTAINS AN INITIAL ESTIMATE
            OF A SATISFACTORY STEP. ON OUTPUT STP CONTAINS THE FINAL ESTIMATE.

            FTOL AND GTOL ARE NONNEGATIVE INPUT VARIABLES. TERMINATION OCCURS WHEN THE
            SUFFICIENT DECREASE CONDITION AND THE DIRECTIONAL DERIVATIVE CONDITION ARE
            SATISFIED.

            XTOL IS A NONNEGATIVE INPUT VARIABLE. TERMINATION OCCURS WHEN THE RELATIVE
            WIDTH OF THE INTERVAL OF UNCERTAINTY IS AT MOST XTOL.

            STPMIN AND STPMAX ARE NONNEGATIVE INPUT VARIABLES WHICH SPECIFY LOWER  AND
            UPPER BOUNDS FOR THE STEP.

            MAXFEV IS A POSITIVE INTEGER INPUT VARIABLE. TERMINATION OCCURS WHEN THE
            NUMBER OF CALLS TO FCN IS AT LEAST MAXFEV BY THE END OF AN ITERATION.

            INFO IS AN INTEGER OUTPUT VARIABLE SET AS FOLLOWS:
                INFO = 0  IMPROPER INPUT PARAMETERS.

                INFO = 1  THE SUFFICIENT DECREASE CONDITION AND THE
                          DIRECTIONAL DERIVATIVE CONDITION HOLD.

                INFO = 2  RELATIVE WIDTH OF THE INTERVAL OF UNCERTAINTY
                          IS AT MOST XTOL.

                INFO = 3  NUMBER OF CALLS TO FCN HAS REACHED MAXFEV.

                INFO = 4  THE STEP IS AT THE LOWER BOUND STPMIN.

                INFO = 5  THE STEP IS AT THE UPPER BOUND STPMAX.

                INFO = 6  ROUNDING ERRORS PREVENT FURTHER PROGRESS.
                          THERE MAY NOT BE A STEP WHICH SATISFIES THE
                          SUFFICIENT DECREASE AND CURVATURE CONDITIONS.
                          TOLERANCES MAY BE TOO SMALL.

            NFEV IS AN INTEGER OUTPUT VARIABLE SET TO THE NUMBER OF CALLS TO FCN.

            WA IS A WORK ARRAY OF LENGTH N.

            ARGONNE NATIONAL LABORATORY. MINPACK PROJECT. JUNE 1983
            JORGE J. MORE', DAVID J. THUENTE
            *************************************************************************/
        public static void McSearch(int n,
            ref FluxList<double> x,
            ref double f,
            ref FluxList<double> g,
            FluxList<double> s,
            ref double stp,
            double stpmax,
            double gtol,
            ref int info,
            ref int nfev,
            ref FluxList<double> wa,
            State state,
            ref int stage)
        {
            double v = 0;
            double p5 = 0;
            double p66 = 0;
            double zero = 0;
            int i_ = 0;


            //
            // init
            //
            p5 = 0.5;
            p66 = 0.66;
            state.xtrapf = 4.0;
            zero = 0;
            if (stpmax == 0)
            {
                stpmax = defstpmax;
            }
            if (stp < stpmin)
            {
                stp = stpmin;
            }
            if (stp > stpmax)
            {
                stp = stpmax;
            }

            //
            // Main cycle
            //
            while (true)
            {
                if (stage == 0)
                {

                    //
                    // NEXT
                    //
                    stage = 2;
                    continue;
                }
                if (stage == 2)
                {
                    state.infoc = 1;
                    info = 0;

                    //
                    //     CHECK THE INPUT PARAMETERS FOR ERRORS.
                    //
                    if (stpmax < stpmin && stpmax > 0)
                    {
                        info = 5;
                        stp = stpmax;
                        stage = 0;
                        return;
                    }
                    if (((((((n <= 0 || stp <= 0) || ftol < 0) || gtol < zero) || xtol < zero) || stpmin < zero) || stpmax < stpmin) || maxfev <= 0)
                    {
                        stage = 0;
                        return;
                    }

                    //
                    //     COMPUTE THE INITIAL GRADIENT IN THE SEARCH DIRECTION
                    //     AND CHECK THAT S IS A DESCENT DIRECTION.
                    //
                    v = 0.0;
                    for (i_ = 0; i_ <= n - 1; i_++)
                    {
                        v += g[i_] * s[i_];
                    }
                    state.dginit = v;
                    if (state.dginit >= 0)
                    {
                        stage = 0;
                        return;
                    }

                    //
                    //     INITIALIZE LOCAL VARIABLES.
                    //
                    state.brackt = false;
                    state.stage1 = true;
                    nfev = 0;
                    state.finit = f;
                    state.dgtest = ftol * state.dginit;
                    state.width = stpmax - stpmin;
                    state.width1 = state.width / p5;
                    for (i_ = 0; i_ <= n - 1; i_++)
                    {
                        wa[i_] = x[i_];
                    }

                    //
                    //     THE VARIABLES STX, FX, DGX CONTAIN THE VALUES OF THE STEP,
                    //     FUNCTION, AND DIRECTIONAL DERIVATIVE AT THE BEST STEP.
                    //     THE VARIABLES STY, FY, DGY CONTAIN THE VALUE OF THE STEP,
                    //     FUNCTION, AND DERIVATIVE AT THE OTHER ENDPOINT OF
                    //     THE INTERVAL OF UNCERTAINTY.
                    //     THE VARIABLES STP, F, DG CONTAIN THE VALUES OF THE STEP,
                    //     FUNCTION, AND DERIVATIVE AT THE CURRENT STEP.
                    //
                    state.stx = 0;
                    state.fx = state.finit;
                    state.dgx = state.dginit;
                    state.sty = 0;
                    state.fy = state.finit;
                    state.dgy = state.dginit;

                    //
                    // NEXT
                    //
                    stage = 3;
                    continue;
                }
                if (stage == 3)
                {

                    //
                    //     START OF ITERATION.
                    //
                    //     SET THE MINIMUM AND MAXIMUM STEPS TO CORRESPOND
                    //     TO THE PRESENT INTERVAL OF UNCERTAINTY.
                    //
                    if (state.brackt)
                    {
                        if (state.stx < state.sty)
                        {
                            state.stmin = state.stx;
                            state.stmax = state.sty;
                        }
                        else
                        {
                            state.stmin = state.sty;
                            state.stmax = state.stx;
                        }
                    }
                    else
                    {
                        state.stmin = state.stx;
                        state.stmax = stp + state.xtrapf * (stp - state.stx);
                    }

                    //
                    //        FORCE THE STEP TO BE WITHIN THE BOUNDS STPMAX AND STPMIN.
                    //
                    if (stp > stpmax)
                    {
                        stp = stpmax;
                    }
                    if (stp < stpmin)
                    {
                        stp = stpmin;
                    }

                    //
                    //        IF AN UNUSUAL TERMINATION IS TO OCCUR THEN LET
                    //        STP BE THE LOWEST POINT OBTAINED SO FAR.
                    //
                    if ((((state.brackt && (stp <= state.stmin || stp >= state.stmax)) || nfev >= maxfev - 1) || state.infoc == 0) || (state.brackt && state.stmax - state.stmin <= xtol * state.stmax))
                    {
                        stp = state.stx;
                    }

                    //
                    //        EVALUATE THE FUNCTION AND GRADIENT AT STP
                    //        AND COMPUTE THE DIRECTIONAL DERIVATIVE.
                    //
                    for (i_ = 0; i_ <= n - 1; i_++)
                    {
                        x[i_] = wa[i_];
                    }
                    for (i_ = 0; i_ <= n - 1; i_++)
                    {
                        x[i_] = x[i_] + stp * s[i_];
                    }

                    //
                    // NEXT
                    //
                    stage = 4;
                    return;
                }
                if (stage == 4)
                {
                    info = 0;
                    nfev = nfev + 1;
                    v = 0.0;
                    for (i_ = 0; i_ <= n - 1; i_++)
                    {
                        v += g[i_] * s[i_];
                    }
                    state.dg = v;
                    state.ftest1 = state.finit + stp * state.dgtest;

                    //
                    //        TEST FOR CONVERGENCE.
                    //
                    if ((state.brackt && (stp <= state.stmin || stp >= state.stmax)) || state.infoc == 0)
                    {
                        info = 6;
                    }
                    if ((stp == stpmax && f <= state.ftest1) && state.dg <= state.dgtest)
                    {
                        info = 5;
                    }
                    if (stp == stpmin && (f > state.ftest1 || state.dg >= state.dgtest))
                    {
                        info = 4;
                    }
                    if (nfev >= maxfev)
                    {
                        info = 3;
                    }
                    if (state.brackt && state.stmax - state.stmin <= xtol * state.stmax)
                    {
                        info = 2;
                    }
                    if (f <= state.ftest1 && Math.Abs(state.dg) <= -(gtol * state.dginit))
                    {
                        info = 1;
                    }

                    //
                    //        CHECK FOR TERMINATION.
                    //
                    if (info != 0)
                    {
                        stage = 0;
                        return;
                    }

                    //
                    //        IN THE FIRST STAGE WE SEEK A STEP FOR WHICH THE MODIFIED
                    //        FUNCTION HAS A NONPOSITIVE VALUE AND NONNEGATIVE DERIVATIVE.
                    //
                    if ((state.stage1 && f <= state.ftest1) && state.dg >= Math.Min(ftol, gtol) * state.dginit)
                    {
                        state.stage1 = false;
                    }

                    //
                    //        A MODIFIED FUNCTION IS USED TO PREDICT THE STEP ONLY IF
                    //        WE HAVE NOT OBTAINED A STEP FOR WHICH THE MODIFIED
                    //        FUNCTION HAS A NONPOSITIVE FUNCTION VALUE AND NONNEGATIVE
                    //        DERIVATIVE, AND IF A LOWER FUNCTION VALUE HAS BEEN
                    //        OBTAINED BUT THE DECREASE IS NOT SUFFICIENT.
                    //
                    if ((state.stage1 && f <= state.fx) && f > state.ftest1)
                    {

                        //
                        //           DEFINE THE MODIFIED FUNCTION AND DERIVATIVE VALUES.
                        //
                        state.fm = f - stp * state.dgtest;
                        state.fxm = state.fx - state.stx * state.dgtest;
                        state.fym = state.fy - state.sty * state.dgtest;
                        state.dgm = state.dg - state.dgtest;
                        state.dgxm = state.dgx - state.dgtest;
                        state.dgym = state.dgy - state.dgtest;

                        //
                        //           CALL CSTEP TO UPDATE THE INTERVAL OF UNCERTAINTY
                        //           AND TO COMPUTE THE NEW STEP.
                        //
                        McStep(ref state.stx, ref state.fxm, ref state.dgxm, ref state.sty, ref state.fym, ref state.dgym, ref stp, state.fm, state.dgm, ref state.brackt, state.stmin, state.stmax, ref state.infoc);

                        //
                        //           RESET THE FUNCTION AND GRADIENT VALUES FOR F.
                        //
                        state.fx = state.fxm + state.stx * state.dgtest;
                        state.fy = state.fym + state.sty * state.dgtest;
                        state.dgx = state.dgxm + state.dgtest;
                        state.dgy = state.dgym + state.dgtest;
                    }
                    else
                    {

                        //
                        //           CALL MCSTEP TO UPDATE THE INTERVAL OF UNCERTAINTY
                        //           AND TO COMPUTE THE NEW STEP.
                        //
                        McStep(ref state.stx, ref state.fx, ref state.dgx, ref state.sty, ref state.fy, ref state.dgy, ref stp, f, state.dg, ref state.brackt, state.stmin, state.stmax, ref state.infoc);
                    }

                    //
                    //        FORCE A SUFFICIENT DECREASE IN THE SIZE OF THE
                    //        INTERVAL OF UNCERTAINTY.
                    //
                    if (state.brackt)
                    {
                        if (Math.Abs(state.sty - state.stx) >= p66 * state.width1)
                        {
                            stp = state.stx + p5 * (state.sty - state.stx);
                        }
                        state.width1 = state.width;
                        state.width = Math.Abs(state.sty - state.stx);
                    }

                    //
                    //  NEXT.
                    //
                    stage = 3;
                    continue;
                }
            }
        }



       
        private static void McStep(ref double stx,
            ref double fx,
            ref double dx,
            ref double sty,
            ref double fy,
            ref double dy,
            ref double stp,
            double fp,
            double dp,
            ref bool brackt,
            double stmin,
            double stmax,
            ref int info)
        {
            bool bound = new bool();
            double gamma = 0;
            double p = 0;
            double q = 0;
            double r = 0;
            double s = 0;
            double sgnd = 0;
            double stpc = 0;
            double stpf = 0;
            double stpq = 0;
            double theta = 0;

            info = 0;

            //
            //     CHECK THE INPUT PARAMETERS FOR ERRORS.
            //
            if (((brackt && (stp <= Math.Min(stx, sty) || stp >= Math.Max(stx, sty))) || dx * (stp - stx) >= 0) || stmax < stmin)
            {
                return;
            }

            //
            //     DETERMINE IF THE DERIVATIVES HAVE OPPOSITE SIGN.
            //
            sgnd = dp * (dx / Math.Abs(dx));

            //
            //     FIRST CASE. A HIGHER FUNCTION VALUE.
            //     THE MINIMUM IS BRACKETED. IF THE CUBIC STEP IS CLOSER
            //     TO STX THAN THE QUADRATIC STEP, THE CUBIC STEP IS TAKEN,
            //     ELSE THE AVERAGE OF THE CUBIC AND QUADRATIC STEPS IS TAKEN.
            //
            if (fp > fx)
            {
                info = 1;
                bound = true;
                theta = 3 * (fx - fp) / (stp - stx) + dx + dp;
                s = Math.Max(Math.Abs(theta), Math.Max(Math.Abs(dx), Math.Abs(dp)));
                gamma = s * Math.Sqrt(MathExtensions.Squared(theta / s) - dx / s * (dp / s));
                if (stp < stx)
                {
                    gamma = -gamma;
                }
                p = gamma - dx + theta;
                q = gamma - dx + gamma + dp;
                r = p / q;
                stpc = stx + r * (stp - stx);
                stpq = stx + dx / ((fx - fp) / (stp - stx) + dx) / 2 * (stp - stx);
                if (Math.Abs(stpc - stx) < Math.Abs(stpq - stx))
                {
                    stpf = stpc;
                }
                else
                {
                    stpf = stpc + (stpq - stpc) / 2;
                }
                brackt = true;
            }
            else
            {
                if (sgnd < 0)
                {

                    //
                    //     SECOND CASE. A LOWER FUNCTION VALUE AND DERIVATIVES OF
                    //     OPPOSITE SIGN. THE MINIMUM IS BRACKETED. IF THE CUBIC
                    //     STEP IS CLOSER TO STX THAN THE QUADRATIC (SECANT) STEP,
                    //     THE CUBIC STEP IS TAKEN, ELSE THE QUADRATIC STEP IS TAKEN.
                    //
                    info = 2;
                    bound = false;
                    theta = 3 * (fx - fp) / (stp - stx) + dx + dp;
                    s = Math.Max(Math.Abs(theta), Math.Max(Math.Abs(dx), Math.Abs(dp)));
                    gamma = s * Math.Sqrt(MathExtensions.Squared(theta / s) - dx / s * (dp / s));
                    if (stp > stx)
                    {
                        gamma = -gamma;
                    }
                    p = gamma - dp + theta;
                    q = gamma - dp + gamma + dx;
                    r = p / q;
                    stpc = stp + r * (stx - stp);
                    stpq = stp + dp / (dp - dx) * (stx - stp);
                    if (Math.Abs(stpc - stp) > Math.Abs(stpq - stp))
                    {
                        stpf = stpc;
                    }
                    else
                    {
                        stpf = stpq;
                    }
                    brackt = true;
                }
                else
                {
                    if (Math.Abs(dp) < Math.Abs(dx))
                    {

                        //
                        //     THIRD CASE. A LOWER FUNCTION VALUE, DERIVATIVES OF THE
                        //     SAME SIGN, AND THE MAGNITUDE OF THE DERIVATIVE DECREASES.
                        //     THE CUBIC STEP IS ONLY USED IF THE CUBIC TENDS TO INFINITY
                        //     IN THE DIRECTION OF THE STEP OR IF THE MINIMUM OF THE CUBIC
                        //     IS BEYOND STP. OTHERWISE THE CUBIC STEP IS DEFINED TO BE
                        //     EITHER STPMIN OR STPMAX. THE QUADRATIC (SECANT) STEP IS ALSO
                        //     COMPUTED AND IF THE MINIMUM IS BRACKETED THEN THE THE STEP
                        //     CLOSEST TO STX IS TAKEN, ELSE THE STEP FARTHEST AWAY IS TAKEN.
                        //
                        info = 3;
                        bound = true;
                        theta = 3 * (fx - fp) / (stp - stx) + dx + dp;
                        s = Math.Max(Math.Abs(theta), Math.Max(Math.Abs(dx), Math.Abs(dp)));

                        //
                        //        THE CASE GAMMA = 0 ONLY ARISES IF THE CUBIC DOES NOT TEND
                        //        TO INFINITY IN THE DIRECTION OF THE STEP.
                        //
                        gamma = s * Math.Sqrt(Math.Max(0, MathExtensions.Squared(theta / s) - dx / s * (dp / s)));
                        if (stp > stx)
                        {
                            gamma = -gamma;
                        }
                        p = gamma - dp + theta;
                        q = gamma + (dx - dp) + gamma;
                        r = p / q;
                        if (r < 0 && gamma != 0)
                        {
                            stpc = stp + r * (stx - stp);
                        }
                        else
                        {
                            if (stp > stx)
                            {
                                stpc = stmax;
                            }
                            else
                            {
                                stpc = stmin;
                            }
                        }
                        stpq = stp + dp / (dp - dx) * (stx - stp);
                        if (brackt)
                        {
                            if (Math.Abs(stp - stpc) < Math.Abs(stp - stpq))
                            {
                                stpf = stpc;
                            }
                            else
                            {
                                stpf = stpq;
                            }
                        }
                        else
                        {
                            if (Math.Abs(stp - stpc) > Math.Abs(stp - stpq))
                            {
                                stpf = stpc;
                            }
                            else
                            {
                                stpf = stpq;
                            }
                        }
                    }
                    else
                    {

                        //
                        //     FOURTH CASE. A LOWER FUNCTION VALUE, DERIVATIVES OF THE
                        //     SAME SIGN, AND THE MAGNITUDE OF THE DERIVATIVE DOES
                        //     NOT DECREASE. IF THE MINIMUM IS NOT BRACKETED, THE STEP
                        //     IS EITHER STPMIN OR STPMAX, ELSE THE CUBIC STEP IS TAKEN.
                        //
                        info = 4;
                        bound = false;
                        if (brackt)
                        {
                            theta = 3 * (fp - fy) / (sty - stp) + dy + dp;
                            s = Math.Max(Math.Abs(theta), Math.Max(Math.Abs(dy), Math.Abs(dp)));
                            gamma = s * Math.Sqrt(MathExtensions.Squared(theta / s) - dy / s * (dp / s));
                            if (stp > sty)
                            {
                                gamma = -gamma;
                            }
                            p = gamma - dp + theta;
                            q = gamma - dp + gamma + dy;
                            r = p / q;
                            stpc = stp + r * (sty - stp);
                            stpf = stpc;
                        }
                        else
                        {
                            if (stp > stx)
                            {
                                stpf = stmax;
                            }
                            else
                            {
                                stpf = stmin;
                            }
                        }
                    }
                }
            }

            //
            //     UPDATE THE INTERVAL OF UNCERTAINTY. THIS UPDATE DOES NOT
            //     DEPEND ON THE NEW STEP OR THE CASE ANALYSIS ABOVE.
            //
            if (fp > fx)
            {
                sty = stp;
                fy = fp;
                dy = dp;
            }
            else
            {
                if (sgnd < 0.0)
                {
                    sty = stx;
                    fy = fx;
                    dy = dx;
                }
                stx = stp;
                fx = fp;
                dx = dp;
            }

            //
            //     COMPUTE THE NEW STEP AND SAFEGUARD IT.
            //
            stpf = Math.Min(stmax, stpf);
            stpf = Math.Max(stmin, stpf);
            stp = stpf;
            if (brackt && bound)
            {
                if (sty > stx)
                {
                    stp = Math.Min(stx + 0.66 * (sty - stx), stp);
                }
                else
                {
                    stp = Math.Max(stx + 0.66 * (sty - stx), stp);
                }
            }
        }


    }
}