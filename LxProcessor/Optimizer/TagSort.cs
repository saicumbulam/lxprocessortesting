namespace LxProcessor.Optimizer
{
    public class TagSort
    {
           
        /*************************************************************************
            Buffered variant of TagSort, which accepts preallocated output arrays as
            well as special structure for buffered allocations. If arrays are too
            short, they are reallocated. If they are large enough, no memory
            allocation is done.

            It is intended to be used in the performance-critical parts of code, where
            additional allocations can lead to severe performance degradation

              -- ALGLIB --
                 Copyright 14.05.2008 by Bochkanov Sergey
            *************************************************************************/
        public static void SortBuffered(ref double[] a,
            int n,
            ref int[] p1,
            ref int[] p2,
            Buffers buf)
        {
            int i = 0;
            int lv = 0;
            int lp = 0;
            int rv = 0;
            int rp = 0;


            //
            // Special cases
            //
            if (n <= 0)
            {
                return;
            }
            if (n == 1)
            {
                Utilities.SetLengthAtLeast(ref p1, 1);
                Utilities.SetLengthAtLeast(ref p2, 1);
                p1[0] = 0;
                p2[0] = 0;
                return;
            }

            //
            // General case, N>1: prepare permutations table P1
            //
            Utilities.SetLengthAtLeast(ref p1, n);
            for (i = 0; i <= n - 1; i++)
            {
                p1[i] = i;
            }

            //
            // General case, N>1: sort, update P1
            //
            Utilities.SetLengthAtLeast(ref buf.ra0, n);
            Utilities.SetLengthAtLeast(ref buf.ia0, n);
            SortFastI(ref a, ref p1, ref buf.ra0, ref buf.ia0, n);

            //
            // General case, N>1: fill permutations table P2
            //
            // To fill P2 we maintain two arrays:
            // * PV (Buf.IA0), Position(Value). PV[i] contains position of I-th key at the moment
            // * VP (Buf.IA1), Value(Position). VP[i] contains key which has position I at the moment
            //
            // At each step we making permutation of two items:
            //   Left, which is given by position/value pair LP/LV
            //   and Right, which is given by RP/RV
            // and updating PV[] and VP[] correspondingly.
            //
            Utilities.SetLengthAtLeast(ref buf.ia0, n);
            Utilities.SetLengthAtLeast(ref buf.ia1, n);
            Utilities.SetLengthAtLeast(ref p2, n);
            for (i = 0; i <= n - 1; i++)
            {
                buf.ia0[i] = i;
                buf.ia1[i] = i;
            }
            for (i = 0; i <= n - 1; i++)
            {

                //
                // calculate LP, LV, RP, RV
                //
                lp = i;
                lv = buf.ia1[lp];
                rv = p1[i];
                rp = buf.ia0[rv];

                //
                // Fill P2
                //
                p2[i] = rp;

                //
                // update PV and VP
                //
                buf.ia1[lp] = rv;
                buf.ia1[rp] = lv;
                buf.ia0[lv] = rp;
                buf.ia0[rv] = lp;
            }
        }


        /*************************************************************************
            Same as TagSort, but optimized for real keys and integer labels.

            A is sorted, and same permutations are applied to B.

            NOTES:
            1.  this function assumes that A[] is finite; it doesn't checks that
                condition. All other conditions (size of input arrays, etc.) are not
                checked too.
            2.  this function uses two buffers, BufA and BufB, each is N elements large.
                They may be preallocated (which will save some time) or not, in which
                case function will automatically allocate memory.

              -- ALGLIB --
                 Copyright 11.12.2008 by Bochkanov Sergey
            *************************************************************************/
        private static void SortFastI(ref double[] a,
            ref int[] b,
            ref double[] bufa,
            ref int[] bufb,
            int n)
        {
            int i = 0;
            int j = 0;
            bool isascending = new bool();
            bool isdescending = new bool();
            double tmpr = 0;
            int tmpi = 0;


            //
            // Special case
            //
            if (n <= 1)
            {
                return;
            }

            //
            // Test for already sorted set
            //
            isascending = true;
            isdescending = true;
            for (i = 1; i <= n - 1; i++)
            {
                isascending = isascending && a[i] >= a[i - 1];
                isdescending = isdescending && a[i] <= a[i - 1];
            }
            if (isascending)
            {
                return;
            }
            if (isdescending)
            {
                for (i = 0; i <= n - 1; i++)
                {
                    j = n - 1 - i;
                    if (j <= i)
                    {
                        break;
                    }
                    tmpr = a[i];
                    a[i] = a[j];
                    a[j] = tmpr;
                    tmpi = b[i];
                    b[i] = b[j];
                    b[j] = tmpi;
                }
                return;
            }

            //
            // General case
            //
            if (Utilities.Len(bufa) < n)
            {
                bufa = new double[n];
            }
            if (Utilities.Len(bufb) < n)
            {
                bufb = new int[n];
            }
            SortFastIRecursive(ref a, ref b, ref bufa, ref bufb, 0, n - 1);
        }



        /*************************************************************************
            Internal TagSortFastI: sorts A[I1...I2] (both bounds are included),
            applies same permutations to B.

              -- ALGLIB --
                 Copyright 06.09.2010 by Bochkanov Sergey
            *************************************************************************/
        private static void SortFastIRecursive(ref double[] a,
            ref int[] b,
            ref double[] bufa,
            ref int[] bufb,
            int i1,
            int i2)
        {
            int i = 0;
            int j = 0;
            int k = 0;
            int cntless = 0;
            int cnteq = 0;
            int cntgreater = 0;
            double tmpr = 0;
            int tmpi = 0;
            double v0 = 0;
            double v1 = 0;
            double v2 = 0;
            double vp = 0;


            //
            // Fast exit
            //
            if (i2 <= i1)
            {
                return;
            }

            //
            // Non-recursive sort for small arrays
            //
            if (i2 - i1 <= 16)
            {
                for (j = i1 + 1; j <= i2; j++)
                {

                    //
                    // Search elements [I1..J-1] for place to insert Jth element.
                    //
                    // This code stops immediately if we can leave A[J] at J-th position
                    // (all elements have same value of A[J] larger than any of them)
                    //
                    tmpr = a[j];
                    tmpi = j;
                    for (k = j - 1; k >= i1; k--)
                    {
                        if (a[k] <= tmpr)
                        {
                            break;
                        }
                        tmpi = k;
                    }
                    k = tmpi;

                    //
                    // Insert Jth element into Kth position
                    //
                    if (k != j)
                    {
                        tmpr = a[j];
                        tmpi = b[j];
                        for (i = j - 1; i >= k; i--)
                        {
                            a[i + 1] = a[i];
                            b[i + 1] = b[i];
                        }
                        a[k] = tmpr;
                        b[k] = tmpi;
                    }
                }
                return;
            }

            //
            // Quicksort: choose pivot
            // Here we assume that I2-I1>=2
            //
            v0 = a[i1];
            v1 = a[i1 + (i2 - i1) / 2];
            v2 = a[i2];
            if (v0 > v1)
            {
                tmpr = v1;
                v1 = v0;
                v0 = tmpr;
            }
            if (v1 > v2)
            {
                tmpr = v2;
                v2 = v1;
                v1 = tmpr;
            }
            if (v0 > v1)
            {
                tmpr = v1;
                v1 = v0;
                v0 = tmpr;
            }
            vp = v1;

            //
            // now pass through A/B and:
            // * move elements that are LESS than VP to the left of A/B
            // * move elements that are EQUAL to VP to the right of BufA/BufB (in the reverse order)
            // * move elements that are GREATER than VP to the left of BufA/BufB (in the normal order
            // * move elements from the tail of BufA/BufB to the middle of A/B (restoring normal order)
            // * move elements from the left of BufA/BufB to the end of A/B
            //
            cntless = 0;
            cnteq = 0;
            cntgreater = 0;
            for (i = i1; i <= i2; i++)
            {
                v0 = a[i];
                if (v0 < vp)
                {

                    //
                    // LESS
                    //
                    k = i1 + cntless;
                    if (i != k)
                    {
                        a[k] = v0;
                        b[k] = b[i];
                    }
                    cntless = cntless + 1;
                    continue;
                }
                if (v0 == vp)
                {

                    //
                    // EQUAL
                    //
                    k = i2 - cnteq;
                    bufa[k] = v0;
                    bufb[k] = b[i];
                    cnteq = cnteq + 1;
                    continue;
                }

                //
                // GREATER
                //
                k = i1 + cntgreater;
                bufa[k] = v0;
                bufb[k] = b[i];
                cntgreater = cntgreater + 1;
            }
            for (i = 0; i <= cnteq - 1; i++)
            {
                j = i1 + cntless + cnteq - 1 - i;
                k = i2 + i - (cnteq - 1);
                a[j] = bufa[k];
                b[j] = bufb[k];
            }
            for (i = 0; i <= cntgreater - 1; i++)
            {
                j = i1 + cntless + cnteq + i;
                k = i1 + i;
                a[j] = bufa[k];
                b[j] = bufb[k];
            }

            //
            // Sort left and right parts of the array (ignoring middle part)
            //
            SortFastIRecursive(ref a, ref b, ref bufa, ref bufb, i1, i1 + cntless - 1);
            SortFastIRecursive(ref a, ref b, ref bufa, ref bufb, i1 + cntless + cnteq, i2);
        }
    }
}