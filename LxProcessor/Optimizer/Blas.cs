using System;
using LxProcessor.Common;

namespace LxProcessor.Optimizer
{
    public class Blas
    {
        public static void CopyMatrix(Matrix<double> a,
            int is1,
            int is2,
            int js1,
            int js2,
            ref Matrix<double> b,
            int id1,
            int id2,
            int jd1,
            int jd2)
        {
            int isrc = 0;
            int idst = 0;
            int i_ = 0;
            int i1_ = 0;

            if (is1 > is2 || js1 > js2)
            {
                return;
            }
            Utilities.Assert(is2 - is1 == id2 - id1, "CopyMatrix: different sizes!");
            Utilities.Assert(js2 - js1 == jd2 - jd1, "CopyMatrix: different sizes!");
            for (isrc = is1; isrc <= is2; isrc++)
            {
                idst = isrc - is1 + id1;
                i1_ = (js1) - (jd1);
                for (i_ = jd1; i_ <= jd2; i_++)
                {
                    b[idst, i_] = a[isrc, i_ + i1_];
                }
            }
        }


        public static void InPlaceTranspose(ref Matrix<double> a,
            int i1,
            int i2,
            int j1,
            int j2,
            ref double[] work)
        {
            int i = 0;
            int j = 0;
            int ips = 0;
            int jps = 0;
            int l = 0;
            int i_ = 0;
            int i1_ = 0;

            if (i1 > i2 || j1 > j2)
            {
                return;
            }
            Utilities.Assert(i1 - i2 == j1 - j2, "InplaceTranspose error: incorrect array size!");
            for (i = i1; i <= i2 - 1; i++)
            {
                j = j1 + i - i1;
                ips = i + 1;
                jps = j1 + ips - i1;
                l = i2 - i;
                i1_ = (ips) - (1);
                for (i_ = 1; i_ <= l; i_++)
                {
                    work[i_] = a[i_ + i1_, j];
                }
                i1_ = (jps) - (ips);
                for (i_ = ips; i_ <= i2; i_++)
                {
                    a[i_, j] = a[i, i_ + i1_];
                }
                i1_ = (1) - (jps);
                for (i_ = jps; i_ <= j2; i_++)
                {
                    a[i, i_] = work[i_ + i1_];
                }
            }
        }


        public static void CopyAndTranspose(Matrix<double> a,
            int is1,
            int is2,
            int js1,
            int js2,
            ref Matrix<double> b,
            int id1,
            int id2,
            int jd1,
            int jd2)
        {
            int isrc = 0;
            int jdst = 0;
            int i_ = 0;
            int i1_ = 0;

            if (is1 > is2 || js1 > js2)
            {
                return;
            }
            Utilities.Assert(is2 - is1 == jd2 - jd1, "CopyAndTranspose: different sizes!");
            Utilities.Assert(js2 - js1 == id2 - id1, "CopyAndTranspose: different sizes!");
            for (isrc = is1; isrc <= is2; isrc++)
            {
                jdst = isrc - is1 + jd1;
                i1_ = (js1) - (id1);
                for (i_ = id1; i_ <= id2; i_++)
                {
                    b[i_, jdst] = a[isrc, i_ + i1_];
                }
            }
        }

        public static void MatrixMatrixMultiply(Matrix<double> a,
            int ai1,
            int ai2,
            int aj1,
            int aj2,
            bool transa,
            Matrix<double> b,
            int bi1,
            int bi2,
            int bj1,
            int bj2,
            bool transb,
            double alpha,
            ref Matrix<double> c,
            int ci1,
            int ci2,
            int cj1,
            int cj2,
            double beta,
            ref double[] work)
        {
            int arows = 0;
            int acols = 0;
            int brows = 0;
            int bcols = 0;
            int crows = 0;
            int i = 0;
            int j = 0;
            int k = 0;
            int l = 0;
            int r = 0;
            double v = 0;
            int i_ = 0;
            int i1_ = 0;


            //
            // Setup
            //
            if (!transa)
            {
                arows = ai2 - ai1 + 1;
                acols = aj2 - aj1 + 1;
            }
            else
            {
                arows = aj2 - aj1 + 1;
                acols = ai2 - ai1 + 1;
            }
            if (!transb)
            {
                brows = bi2 - bi1 + 1;
                bcols = bj2 - bj1 + 1;
            }
            else
            {
                brows = bj2 - bj1 + 1;
                bcols = bi2 - bi1 + 1;
            }
            Utilities.Assert(acols == brows, "MatrixMatrixMultiply: incorrect matrix sizes!");
            if (((arows <= 0 || acols <= 0) || brows <= 0) || bcols <= 0)
            {
                return;
            }
            crows = arows;

            //
            // Test WORK
            //
            i = Math.Max(arows, acols);
            i = Math.Max(brows, i);
            i = Math.Max(i, bcols);
            work[1] = 0;
            work[i] = 0;

            //
            // Prepare C
            //
            if (beta == 0)
            {
                for (i = ci1; i <= ci2; i++)
                {
                    for (j = cj1; j <= cj2; j++)
                    {
                        c[i, j] = 0;
                    }
                }
            }
            else
            {
                for (i = ci1; i <= ci2; i++)
                {
                    for (i_ = cj1; i_ <= cj2; i_++)
                    {
                        c[i, i_] = beta * c[i, i_];
                    }
                }
            }

            //
            // A*B
            //
            if (!transa && !transb)
            {
                for (l = ai1; l <= ai2; l++)
                {
                    for (r = bi1; r <= bi2; r++)
                    {
                        v = alpha * a[l, aj1 + r - bi1];
                        k = ci1 + l - ai1;
                        i1_ = (bj1) - (cj1);
                        for (i_ = cj1; i_ <= cj2; i_++)
                        {
                            c[k, i_] = c[k, i_] + v * b[r, i_ + i1_];
                        }
                    }
                }
                return;
            }

            //
            // A*B'
            //
            if (!transa && transb)
            {
                if (arows * acols < brows * bcols)
                {
                    for (r = bi1; r <= bi2; r++)
                    {
                        for (l = ai1; l <= ai2; l++)
                        {
                            i1_ = (bj1) - (aj1);
                            v = 0.0;
                            for (i_ = aj1; i_ <= aj2; i_++)
                            {
                                v += a[l, i_] * b[r, i_ + i1_];
                            }
                            c[ci1 + l - ai1, cj1 + r - bi1] = c[ci1 + l - ai1, cj1 + r - bi1] + alpha * v;
                        }
                    }
                    return;
                }
                else
                {
                    for (l = ai1; l <= ai2; l++)
                    {
                        for (r = bi1; r <= bi2; r++)
                        {
                            i1_ = (bj1) - (aj1);
                            v = 0.0;
                            for (i_ = aj1; i_ <= aj2; i_++)
                            {
                                v += a[l, i_] * b[r, i_ + i1_];
                            }
                            c[ci1 + l - ai1, cj1 + r - bi1] = c[ci1 + l - ai1, cj1 + r - bi1] + alpha * v;
                        }
                    }
                    return;
                }
            }

            //
            // A'*B
            //
            if (transa && !transb)
            {
                for (l = aj1; l <= aj2; l++)
                {
                    for (r = bi1; r <= bi2; r++)
                    {
                        v = alpha * a[ai1 + r - bi1, l];
                        k = ci1 + l - aj1;
                        i1_ = (bj1) - (cj1);
                        for (i_ = cj1; i_ <= cj2; i_++)
                        {
                            c[k, i_] = c[k, i_] + v * b[r, i_ + i1_];
                        }
                    }
                }
                return;
            }

            //
            // A'*B'
            //
            if (transa && transb)
            {
                if (arows * acols < brows * bcols)
                {
                    for (r = bi1; r <= bi2; r++)
                    {
                        k = cj1 + r - bi1;
                        for (i = 1; i <= crows; i++)
                        {
                            work[i] = 0.0;
                        }
                        for (l = ai1; l <= ai2; l++)
                        {
                            v = alpha * b[r, bj1 + l - ai1];
                            i1_ = (aj1) - (1);
                            for (i_ = 1; i_ <= crows; i_++)
                            {
                                work[i_] = work[i_] + v * a[l, i_ + i1_];
                            }
                        }
                        i1_ = (1) - (ci1);
                        for (i_ = ci1; i_ <= ci2; i_++)
                        {
                            c[i_, k] = c[i_, k] + work[i_ + i1_];
                        }
                    }
                    return;
                }
                else
                {
                    for (l = aj1; l <= aj2; l++)
                    {
                        k = ai2 - ai1 + 1;
                        i1_ = (ai1) - (1);
                        for (i_ = 1; i_ <= k; i_++)
                        {
                            work[i_] = a[i_ + i1_, l];
                        }
                        for (r = bi1; r <= bi2; r++)
                        {
                            i1_ = (bj1) - (1);
                            v = 0.0;
                            for (i_ = 1; i_ <= k; i_++)
                            {
                                v += work[i_] * b[r, i_ + i1_];
                            }
                            c[ci1 + l - aj1, cj1 + r - bi1] = c[ci1 + l - aj1, cj1 + r - bi1] + alpha * v;
                        }
                    }
                    return;
                }
            }
        }


    }
}