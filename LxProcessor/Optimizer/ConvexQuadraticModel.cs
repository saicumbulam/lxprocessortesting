using System;
using System.Collections.Generic;
using LxCommon.DataStructures;
using LxCommon.Utilities;
using LxProcessor.Common;

namespace LxProcessor.Optimizer
{

    /*************************************************************************
          This structure describes convex quadratic model of the form:
              f(x) = 0.5*(Alpha*x'*A*x + Tau*x'*D*x) + 0.5*Theta*(Q*x-r)'*(Q*x-r) + b'*x
          where:
              * Alpha>=0, Tau>=0, Theta>=0, Alpha+Tau>0.
              * A is NxN matrix, Q is NxK matrix (N>=1, K>=0), b is Nx1 vector,
                D is NxN diagonal matrix.
              * "main" quadratic term Alpha*A+Lambda*D is symmetric
                positive definite
          Structure may contain optional equality constraints of the form x[i]=x0[i],
          in this case functions provided by this unit calculate Newton step subject
          to these equality constraints.
          *************************************************************************/
    public class ConvexQuadraticModel             
    {
        public int n;
        public int k;
        public double alpha;
        public double tau;
        public double theta;
        public Matrix<double> a;
        public Matrix<double> q;
        public FluxList<double> b;
        public FluxList<double> r;
        public FluxList<double> xc;
        public FluxList<double> d;
        public FluxList<bool> activeset;
        public Matrix<double> tq2dense;
        public Matrix<double> tk2;
        public FluxList<double> tq2diag;
        public FluxList<double> tq1;
        public FluxList<double> tk1;
        public double tq0;
        public double tk0;
        public FluxList<double> txc;
        public FluxList<double> tb;
        public int nfree;
        public int ecakind;
        public Matrix<double> ecadense;
        public Matrix<double> eq;
        public Matrix<double> eccm;
        public FluxList<double> ecadiag;
        public FluxList<double> eb;
        public double ec;
        public FluxList<double> tmp0;
        public FluxList<double> tmp1;
        public FluxList<double> tmpg;
        public Matrix<double> tmp2;
        public bool ismaintermchanged;
        public bool issecondarytermchanged;
        public bool islineartermchanged;
        public bool isactivesetchanged;
        public ConvexQuadraticModel()
        {
            a = new Matrix<double>(0, 0);
            q = new Matrix<double>(0, 0);
            b = new FluxList<double>();
            r = new FluxList<double>();
            xc = new FluxList<double>();
            d = new FluxList<double>();
            activeset = new FluxList<bool>();
            tq2dense = new Matrix<double>(0, 0);
            tk2 = new Matrix<double>(0, 0);
            tq2diag = new FluxList<double>();
            tq1 = new FluxList<double>();
            tk1 = new FluxList<double>();
            txc = new FluxList<double>();
            tb = new FluxList<double>();
            ecadense = new Matrix<double>(0, 0);
            eq = new Matrix<double>(0, 0);
            eccm = new Matrix<double>(0, 0);
            ecadiag = new FluxList<double>();
            eb = new FluxList<double>();
            tmp0 = new FluxList<double>();
            tmp1 = new FluxList<double>();
            tmpg = new FluxList<double>();
            tmp2 = new Matrix<double>(0, 0);
        }


        public void CleanForReuse()
        {
            n = default(int);
            k = default(int);
            alpha = default(double);
            tau = default(double);
            theta = default(double);
            a.Clear();
            q.Clear();
            b.Clear();
            r.Clear();
            xc.Clear();
            d.Clear();
            activeset.Clear();
            tq2dense.Clear();
            tk2.Clear();
            tq2diag.Clear();
            tq1.Clear();
            tk1.Clear();
            tq0 = default(double);
            tk0 = default(double);
            txc.Clear();
            tb.Clear();
            nfree = default(int);
            ecakind = default(int);
            ecadense.Clear();
            eq.Clear();
            eccm.Clear();
            ecadiag.Clear();
            eb.Clear();
            ec = default(double);
            tmp0.Clear();
            tmp1.Clear();
            tmpg.Clear();
            tmp2.Clear();
            ismaintermchanged = default(bool);
            issecondarytermchanged = default(bool);
            islineartermchanged = default(bool);
            isactivesetchanged = default(bool);
        }


        public const int NewtonRefinementIterations = 3;

        /*************************************************************************
           This subroutine is used to initialize CQM. By default, empty NxN model  is
           generated, with Alpha=Lambda=Theta=0.0 and zero b.

           Previously allocated buffer variables are reused as much as possible.

             -- ALGLIB --
                Copyright 12.06.2012 by Bochkanov Sergey
           *************************************************************************/
        public static void Init(int n,
            ConvexQuadraticModel s)
        {
            s.n = n;
            s.k = 0;
            s.nfree = n;
            s.ecakind = -1;
            s.alpha = 0.0;
            s.tau = 0.0;
            s.theta = 0.0;
            s.ismaintermchanged = true;
            s.issecondarytermchanged = true;
            s.islineartermchanged = true;
            s.isactivesetchanged = true;
            s.activeset.SetLengthAtLeast(n);
            s.xc.SetLengthAtLeast(n);
            s.eb.SetLengthAtLeast(n);
            s.tq1.SetLengthAtLeast(n);
            s.txc.SetLengthAtLeast(n);
            s.tb.SetLengthAtLeast(n);
            s.b.SetLengthAtLeast(s.n);
            s.tk1.SetLengthAtLeast(s.n);
            for (int i = 0; i <= n - 1; i++)
            {
                s.activeset[i] = false;
                s.xc[i] = 0.0;
                s.b[i] = 0.0;
            }
        }

        /*************************************************************************
         This subroutine changes main quadratic term of the model.

         INPUT PARAMETERS:
             S       -   model
             A       -   NxN matrix, only upper or lower triangle is referenced
             IsUpper -   True, when matrix is stored in upper triangle
             Alpha   -   multiplier; when Alpha=0, A is not referenced at all

           -- ALGLIB --
              Copyright 12.06.2012 by Bochkanov Sergey
         *************************************************************************/
        public static void ChangeMainQuadraticTerm(ConvexQuadraticModel s,
            Matrix<double> a,
            bool isupper,
            double alpha)
        {
            int i = 0;
            int j = 0;
            double v = 0;

            Utilities.Assert(MathExtensions.IsFinite(alpha) && alpha >= 0, "CQMSetA: Alpha<0 or is not finite number");
            Utilities.Assert(alpha == 0 || Utilities.IsFiniteRtrMatrix(a, s.n, isupper), "CQMSetA: A is not finite NxN matrix");
            s.alpha = alpha;
            if (alpha > 0)
            {
                s.a.SetLengthAtLeast(s.n, s.n);
                s.ecadense.SetLengthAtLeast(s.n, s.n);
                s.tq2dense.SetLengthAtLeast(s.n, s.n);
                for (i = 0; i <= s.n - 1; i++)
                {
                    for (j = i; j <= s.n - 1; j++)
                    {
                        if (isupper)
                        {
                            v = a[i, j];
                        }
                        else
                        {
                            v = a[j, i];
                        }
                        s.a[i, j] = v;
                        s.a[j, i] = v;
                    }
                }
            }
            s.ismaintermchanged = true;
        }

        /*************************************************************************
          This subroutine rewrites diagonal of the main quadratic term of the  model
          (dense  A)  by  vector  Z/Alpha (current value of the Alpha coefficient is
          used).

          IMPORTANT: in  case  model  has  no  dense  quadratic  term, this function
                     allocates N*N dense matrix of zeros, and fills its diagonal  by
                     non-zero values.

          INPUT PARAMETERS:
              S       -   model
              Z       -   new diagonal, array[N]

            -- ALGLIB --
               Copyright 12.06.2012 by Bochkanov Sergey
          *************************************************************************/
        public static void RewriteDenseDiagonal(ConvexQuadraticModel s,
            FluxList<double> z)
        {
            int n = 0;
            int i = 0;
            int j = 0;

            n = s.n;
            if (s.alpha == 0)
            {
                s.a.SetLengthAtLeast(s.n, s.n);
                s.ecadense.SetLengthAtLeast(s.n, s.n);
                s.tq2dense.SetLengthAtLeast(s.n, s.n);
                for (i = 0; i <= n - 1; i++)
                {
                    for (j = 0; j <= n - 1; j++)
                    {
                        s.a[i, j] = 0.0;
                    }
                }
                s.alpha = 1.0;
            }
            for (i = 0; i <= s.n - 1; i++)
            {
                s.a[i, i] = z[i] / s.alpha;
            }
            s.ismaintermchanged = true;
        }


        /*************************************************************************
          This subroutine changes linear term of the model

            -- ALGLIB --
               Copyright 12.06.2012 by Bochkanov Sergey
          *************************************************************************/
        public static void ChangeLinearTerm(ConvexQuadraticModel s,
            FluxList<double> b)
        {
            int i = 0;

            Utilities.Assert(Utilities.IsFiniteVector(b, s.n), "CQMSetB: B is not finite vector");
            s.b.SetLengthAtLeast(s.n);
            for (i = 0; i <= s.n - 1; i++)
            {
                s.b[i] = b[i];
            }
            s.islineartermchanged = true;
        }


        /*************************************************************************
     This subroutine changes linear term of the model

       -- ALGLIB --
          Copyright 12.06.2012 by Bochkanov Sergey
     *************************************************************************/
        public static void SetQ(ConvexQuadraticModel s,
            Matrix<double> q,
            FluxList<double> r,
            int k,
            double theta)
        {
            int i = 0;
            int j = 0;

            Utilities.Assert(k >= 0, "CQMSetQ: K<0");
            Utilities.Assert((k == 0 || theta == 0) || Utilities.IsFiniteMatrix(q, k, s.n), "CQMSetQ: Q is not finite matrix");
            Utilities.Assert((k == 0 || theta == 0) || Utilities.IsFiniteVector(r, k), "CQMSetQ: R is not finite vector");
            Utilities.Assert(MathExtensions.IsFinite(theta) && theta >= 0, "CQMSetQ: Theta<0 or is not finite number");

            //
            // degenerate case: K=0 or Theta=0
            //
            if (k == 0 || theta == 0)
            {
                s.k = 0;
                s.theta = 0;
                s.issecondarytermchanged = true;
                return;
            }

            //
            // General case: both Theta>0 and K>0
            //
            s.k = k;
            s.theta = theta;
            s.q.SetLengthAtLeast(s.k, s.n);
            s.r.SetLengthAtLeast(s.k);
            s.eq.SetLengthAtLeast(s.k, s.n);
            s.eccm.SetLengthAtLeast(s.k, s.k);
            s.tk2.SetLengthAtLeast(s.k, s.n);
            for (i = 0; i <= s.k - 1; i++)
            {
                for (j = 0; j <= s.n - 1; j++)
                {
                    s.q[i, j] = q[i, j];
                }
                s.r[i] = r[i];
            }
            s.issecondarytermchanged = true;
        }


        /*************************************************************************
    This subroutine changes active set

    INPUT PARAMETERS
        S       -   model
        X       -   array[N], constraint values
        ActiveSet-  array[N], active set. If ActiveSet[I]=True, then I-th
                    variables is constrained to X[I].

      -- ALGLIB --
         Copyright 12.06.2012 by Bochkanov Sergey
    *************************************************************************/
        public static void ChangeActiveSet(ConvexQuadraticModel s,
            FluxList<double> x,
            FluxList<bool> activeset)
        {
            int i = 0;

            Utilities.Assert(x.Count >= s.n, "CQMSetActiveSet: Length(X)<N");
            Utilities.Assert(activeset.Count >= s.n, "CQMSetActiveSet: Length(ActiveSet)<N");
            for (i = 0; i <= s.n - 1; i++)
            {
                s.isactivesetchanged = s.isactivesetchanged || (s.activeset[i] && !activeset[i]);
                s.isactivesetchanged = s.isactivesetchanged || (activeset[i] && !s.activeset[i]);
                s.activeset[i] = activeset[i];
                if (activeset[i])
                {
                    Utilities.Assert(MathExtensions.IsFinite(x[i]), "CQMSetActiveSet: X[] contains infinite constraints");
                    s.isactivesetchanged = s.isactivesetchanged || s.xc[i] != x[i];
                    s.xc[i] = x[i];
                }
            }
        }


        /*************************************************************************
           This subroutine evaluates model at X. Active constraints are ignored.
           It returns:
               R   -   model value
               Noise-  estimate of the numerical noise in data

             -- ALGLIB --
                Copyright 12.06.2012 by Bochkanov Sergey
           *************************************************************************/
        public static void EvaluateModel(ConvexQuadraticModel s,
            FluxList<double> x,
            ref double r,
            ref double noise)
        {
            int n = 0;
            int i = 0;
            int j = 0;
            double v = 0;
            double v2 = 0;
            double mxq = 0;
            double eps = 0;

            r = 0;
            noise = 0;

            n = s.n;
            Utilities.Assert(Utilities.IsFiniteVector(x, n), "CQMEval: X is not finite vector");
            r = 0.0;
            noise = 0.0;
            eps = 2 * MathExtensions.MachineEpsilon;
            mxq = 0.0;

            //
            // Main quadratic term.
            //
            // Noise from the main quadratic term is equal to the
            // maximum summand in the term.
            //
            if (s.alpha > 0)
            {
                for (i = 0; i <= n - 1; i++)
                {
                    for (j = 0; j <= n - 1; j++)
                    {
                        v = s.alpha * 0.5 * x[i] * s.a[i, j] * x[j];
                        r = r + v;
                        noise = Math.Max(noise, eps * Math.Abs(v));
                    }
                }
            }
            if (s.tau > 0)
            {
                for (i = 0; i <= n - 1; i++)
                {
                    v = 0.5 * MathExtensions.Squared(x[i]) * s.tau * s.d[i];
                    r = r + v;
                    noise = Math.Max(noise, eps * Math.Abs(v));
                }
            }

            //
            // secondary quadratic term
            //
            // Noise from the secondary quadratic term is estimated as follows:
            // * noise in qi*x-r[i] is estimated as
            //   Eps*MXQ = Eps*max(|r[i]|, |q[i,j]*x[j]|)
            // * noise in (qi*x-r[i])^2 is estimated as
            //   NOISE = (|qi*x-r[i]|+Eps*MXQ)^2-(|qi*x-r[i]|)^2
            //         = Eps*MXQ*(2*|qi*x-r[i]|+Eps*MXQ)
            //
            if (s.theta > 0)
            {
                for (i = 0; i <= s.k - 1; i++)
                {
                    v = 0.0;
                    mxq = Math.Abs(s.r[i]);
                    for (j = 0; j <= n - 1; j++)
                    {
                        v2 = s.q[i, j] * x[j];
                        v = v + v2;
                        mxq = Math.Max(mxq, Math.Abs(v2));
                    }
                    r = r + 0.5 * s.theta * MathExtensions.Squared(v - s.r[i]);
                    noise = Math.Max(noise, eps * mxq * (2 * Math.Abs(v - s.r[i]) + eps * mxq));
                }
            }

            //
            // linear term
            //
            for (i = 0; i <= s.n - 1; i++)
            {
                r = r + x[i] * s.b[i];
                noise = Math.Max(noise, eps * Math.Abs(x[i] * s.b[i]));
            }

            //
            // Final update of the noise
            //
            noise = n * noise;
        }


        /*************************************************************************
           This  subroutine  evaluates  gradient of the model; active constraints are
           ignored.

           INPUT PARAMETERS:
               S       -   convex model
               X       -   point, array[N]
               G       -   possibly preallocated buffer; resized, if too small

             -- ALGLIB --
                Copyright 12.06.2012 by Bochkanov Sergey
           *************************************************************************/
        private static void EvaluateGradientUnconstrained(ConvexQuadraticModel s,
            FluxList<double> x,
            ref FluxList<double> g)
        {
            int n = 0;
            int i = 0;
            int j = 0;
            double v = 0;
            int i_ = 0;

            n = s.n;
            Utilities.Assert(Utilities.IsFiniteVector(x, n), "CQMEvalGradUnconstrained: X is not finite vector");
            g.SetLengthAtLeast(n);
            for (i = 0; i <= n - 1; i++)
            {
                g[i] = 0;
            }

            //
            // main quadratic term
            //
            if (s.alpha > 0)
            {
                for (i = 0; i <= n - 1; i++)
                {
                    v = 0.0;
                    for (j = 0; j <= n - 1; j++)
                    {
                        v = v + s.alpha * s.a[i, j] * x[j];
                    }
                    g[i] = g[i] + v;
                }
            }
            if (s.tau > 0)
            {
                for (i = 0; i <= n - 1; i++)
                {
                    g[i] = g[i] + x[i] * s.tau * s.d[i];
                }
            }

            //
            // secondary quadratic term
            //
            if (s.theta > 0)
            {
                for (i = 0; i <= s.k - 1; i++)
                {
                    v = 0.0;
                    for (i_ = 0; i_ <= n - 1; i_++)
                    {
                        v += s.q[i, i_] * x[i_];
                    }
                    v = s.theta * (v - s.r[i]);
                    for (i_ = 0; i_ <= n - 1; i_++)
                    {
                        g[i_] = g[i_] + v * s.q[i, i_];
                    }
                }
            }

            //
            // linear term
            //
            for (i = 0; i <= n - 1; i++)
            {
                g[i] = g[i] + s.b[i];
            }
        }


        /*************************************************************************
        This subroutine evaluates x'*(0.5*alpha*A+tau*D)*x

          -- ALGLIB --
             Copyright 12.06.2012 by Bochkanov Sergey
        *************************************************************************/
        public static double XtAdX2(ConvexQuadraticModel s,
            FluxList<double> x)
        {
            double result = 0;
            int n = 0;
            int i = 0;
            int j = 0;

            n = s.n;
            Utilities.Assert(Utilities.IsFiniteVector(x, n), "CQMEval: X is not finite vector");
            result = 0.0;

            //
            // main quadratic term
            //
            if (s.alpha > 0)
            {
                for (i = 0; i <= n - 1; i++)
                {
                    for (j = 0; j <= n - 1; j++)
                    {
                        result = result + s.alpha * 0.5 * x[i] * s.a[i, j] * x[j];
                    }
                }
            }
            if (s.tau > 0)
            {
                for (i = 0; i <= n - 1; i++)
                {
                    result = result + 0.5 * MathExtensions.Squared(x[i]) * s.tau * s.d[i];
                }
            }
            return result;
        }

        /*************************************************************************
          This subroutine evaluates (0.5*alpha*A+tau*D)*x

          Y is automatically resized if needed

            -- ALGLIB --
               Copyright 12.06.2012 by Bochkanov Sergey
          *************************************************************************/
        public static void Adx(ConvexQuadraticModel s,
            FluxList<double> x,
            ref FluxList<double> y)
        {
            int n = 0;
            int i = 0;
            double v = 0;
            int i_ = 0;

            n = s.n;
            Utilities.Assert(Utilities.IsFiniteVector(x, n), "CQMEval: X is not finite vector");
            y.SetLengthAtLeast(n);

            //
            // main quadratic term
            //
            for (i = 0; i <= n - 1; i++)
            {
                y[i] = 0;
            }
            if (s.alpha > 0)
            {
                for (i = 0; i <= n - 1; i++)
                {
                    v = 0.0;
                    for (i_ = 0; i_ <= n - 1; i_++)
                    {
                        v += s.a[i, i_] * x[i_];
                    }
                    y[i] = y[i] + s.alpha * v;
                }
            }
            if (s.tau > 0)
            {
                for (i = 0; i <= n - 1; i++)
                {
                    y[i] = y[i] + x[i] * s.tau * s.d[i];
                }
            }
        }

        /*************************************************************************
           This subroutine finds optimum of the model. It returns  False  on  failure
           (indefinite/semidefinite matrix).  Optimum  is  found  subject  to  active
           constraints.

           INPUT PARAMETERS
               S       -   model
               X       -   possibly preallocated buffer; automatically resized, if
                           too small enough.

             -- ALGLIB --
                Copyright 12.06.2012 by Bochkanov Sergey
           *************************************************************************/
        public static bool FindOptimumConstrained(ConvexQuadraticModel s,
            ref FluxList<double> x)
        {
            bool result = new bool();
            int n = 0;
            int nfree = 0;
            int k = 0;
            int i = 0;
            double v = 0;
            int cidx0 = 0;
            int itidx = 0;
            int i_ = 0;


            //
            // Rebuild internal structures
            //
            if (!Rebuild(s))
            {
                result = false;
                return result;
            }
            n = s.n;
            k = s.k;
            nfree = s.nfree;
            result = true;

            //
            // Calculate initial point for the iterative refinement:
            // * free components are set to zero
            // * constrained components are set to their constrained values
            //
            x.SetLengthAtLeast(n);
            for (i = 0; i <= n - 1; i++)
            {
                if (s.activeset[i])
                {
                    x[i] = s.xc[i];
                }
                else
                {
                    x[i] = 0;
                }
            }

            //
            // Iterative refinement.
            //
            // In an ideal world without numerical errors it would be enough
            // to make just one Newton step from initial point:
            //   x_new = -H^(-1)*grad(x=0)
            // However, roundoff errors can significantly deteriorate quality
            // of the solution. So we have to recalculate gradient and to
            // perform Newton steps several times.
            //
            // Below we perform fixed number of Newton iterations.
            //
            for (itidx = 0; itidx <= NewtonRefinementIterations - 1; itidx++)
            {

                //
                // Calculate gradient at the current point.
                // Move free components of the gradient in the beginning.
                //
                EvaluateGradientUnconstrained(s, x, ref s.tmpg);
                cidx0 = 0;
                for (i = 0; i <= n - 1; i++)
                {
                    if (!s.activeset[i])
                    {
                        s.tmpg[cidx0] = s.tmpg[i];
                        cidx0 = cidx0 + 1;
                    }
                }

                //
                // Free components of the extrema are calculated in the first NFree elements of TXC.
                //
                // First, we have to calculate original Newton step, without rank-K perturbations
                //
                for (i_ = 0; i_ <= nfree - 1; i_++)
                {
                    s.txc[i_] = -s.tmpg[i_];
                }
                SolveEa(s, ref s.txc, ref s.tmp0);

                //
                // Then, we account for rank-K correction.
                // Woodbury matrix identity is used.
                //
                if (s.k > 0 && s.theta > 0)
                {
                    
                    s.tmp0.SetLengthAtLeast(Math.Max(nfree, k));
                    s.tmp1.SetLengthAtLeast(Math.Max(nfree, k));
                    for (i_ = 0; i_ <= nfree - 1; i_++)
                    {
                        s.tmp1[i_] = -s.tmpg[i_];
                    }
                    SolveEa(s, ref s.tmp1, ref s.tmp0);
                    for (i = 0; i <= k - 1; i++)
                    {
                        v = 0.0;
                        for (i_ = 0; i_ <= nfree - 1; i_++)
                        {
                            v += s.eq[i, i_] * s.tmp1[i_];
                        }
                        s.tmp0[i] = v;
                    }
                    Fbls.CholeskySolver(s.eccm, 1.0, k, true, ref s.tmp0, ref s.tmp1);
                    for (i = 0; i <= nfree - 1; i++)
                    {
                        s.tmp1[i] = 0.0;
                    }
                    for (i = 0; i <= k - 1; i++)
                    {
                        v = s.tmp0[i];
                        for (i_ = 0; i_ <= nfree - 1; i_++)
                        {
                            s.tmp1[i_] = s.tmp1[i_] + v * s.eq[i, i_];
                        }
                    }
                    SolveEa(s, ref s.tmp1, ref s.tmp0);
                    for (i_ = 0; i_ <= nfree - 1; i_++)
                    {
                        s.txc[i_] = s.txc[i_] - s.tmp1[i_];
                    }
                }

                //
                // Unpack components from TXC into X. We pass through all
                // free components of X and add our step.
                //
                cidx0 = 0;
                for (i = 0; i <= n - 1; i++)
                {
                    if (!s.activeset[i])
                    {
                        x[i] = x[i] + s.txc[cidx0];
                        cidx0 = cidx0 + 1;
                    }
                }
            }
            return result;
        }

        /*************************************************************************
         Internal function, rebuilds "effective" model subject to constraints.
         Returns False on failure (non-SPD main quadratic term)

           -- ALGLIB --
              Copyright 10.05.2011 by Bochkanov Sergey
         *************************************************************************/
        private static bool Rebuild(ConvexQuadraticModel s)
        {
            bool result = new bool();
            int n = 0;
            int nfree = 0;
            int k = 0;
            int i = 0;
            int j = 0;
            int ridx0 = 0;
            int ridx1 = 0;
            int cidx0 = 0;
            int cidx1 = 0;
            double v = 0;
            int i_ = 0;

            if (s.alpha == 0 && s.tau == 0)
            {

                //
                // Non-SPD model, quick exit
                //
                result = false;
                return result;
            }
            result = true;
            n = s.n;
            k = s.k;

            //
            // Determine number of free variables.
            // Fill TXC - array whose last N-NFree elements store constraints.
            //
            if (s.isactivesetchanged)
            {
                s.nfree = 0;
                for (i = 0; i <= n - 1; i++)
                {
                    if (!s.activeset[i])
                    {
                        s.nfree = s.nfree + 1;
                    }
                }
                j = s.nfree;
                for (i = 0; i <= n - 1; i++)
                {
                    if (s.activeset[i])
                    {
                        s.txc[j] = s.xc[i];
                        j = j + 1;
                    }
                }
            }
            nfree = s.nfree;

            //
            // Re-evaluate TQ2/TQ1/TQ0, if needed
            //
            if (s.isactivesetchanged || s.ismaintermchanged)
            {

                //
                // Handle cases Alpha>0 and Alpha=0 separately:
                // * in the first case we have dense matrix
                // * in the second one we have diagonal matrix, which can be
                //   handled more efficiently
                //
                if (s.alpha > 0)
                {

                    //
                    // Alpha>0, dense QP
                    //
                    // Split variables into two groups - free (F) and constrained (C). Reorder
                    // variables in such way that free vars come first, constrained are last:
                    // x = [xf, xc].
                    // 
                    // Main quadratic term x'*(alpha*A+tau*D)*x now splits into quadratic part,
                    // linear part and constant part:
                    //                   ( alpha*Aff+tau*Df  alpha*Afc        ) ( xf )              
                    //   0.5*( xf' xc' )*(                                    )*(    ) =
                    //                   ( alpha*Acf         alpha*Acc+tau*Dc ) ( xc )
                    //
                    //   = 0.5*xf'*(alpha*Aff+tau*Df)*xf + (alpha*Afc*xc)'*xf + 0.5*xc'(alpha*Acc+tau*Dc)*xc
                    //                    
                    // We store these parts into temporary variables:
                    // * alpha*Aff+tau*Df, alpha*Afc, alpha*Acc+tau*Dc are stored into upper
                    //   triangle of TQ2
                    // * alpha*Afc*xc is stored into TQ1
                    // * 0.5*xc'(alpha*Acc+tau*Dc)*xc is stored into TQ0
                    //
                    // Below comes first part of the work - generation of TQ2:
                    // * we pass through rows of A and copy I-th row into upper block (Aff/Afc) or
                    //   lower one (Acf/Acc) of TQ2, depending on presence of X[i] in the active set.
                    //   RIdx0 variable contains current position for insertion into upper block,
                    //   RIdx1 contains current position for insertion into lower one.
                    // * within each row, we copy J-th element into left half (Aff/Acf) or right
                    //   one (Afc/Acc), depending on presence of X[j] in the active set. CIdx0
                    //   contains current position for insertion into left block, CIdx1 contains
                    //   position for insertion into right one.
                    // * during copying, we multiply elements by alpha and add diagonal matrix D.
                    //
                    ridx0 = 0;
                    ridx1 = s.nfree;
                    for (i = 0; i <= n - 1; i++)
                    {
                        cidx0 = 0;
                        cidx1 = s.nfree;
                        for (j = 0; j <= n - 1; j++)
                        {
                            if (!s.activeset[i] && !s.activeset[j])
                            {

                                //
                                // Element belongs to Aff
                                //
                                v = s.alpha * s.a[i, j];
                                if (i == j && s.tau > 0)
                                {
                                    v = v + s.tau * s.d[i];
                                }
                                s.tq2dense[ridx0, cidx0] = v;
                            }
                            if (!s.activeset[i] && s.activeset[j])
                            {

                                //
                                // Element belongs to Afc
                                //
                                s.tq2dense[ridx0, cidx1] = s.alpha * s.a[i, j];
                            }
                            if (s.activeset[i] && !s.activeset[j])
                            {

                                //
                                // Element belongs to Acf
                                //
                                s.tq2dense[ridx1, cidx0] = s.alpha * s.a[i, j];
                            }
                            if (s.activeset[i] && s.activeset[j])
                            {

                                //
                                // Element belongs to Acc
                                //
                                v = s.alpha * s.a[i, j];
                                if (i == j && s.tau > 0)
                                {
                                    v = v + s.tau * s.d[i];
                                }
                                s.tq2dense[ridx1, cidx1] = v;
                            }
                            if (s.activeset[j])
                            {
                                cidx1 = cidx1 + 1;
                            }
                            else
                            {
                                cidx0 = cidx0 + 1;
                            }
                        }
                        if (s.activeset[i])
                        {
                            ridx1 = ridx1 + 1;
                        }
                        else
                        {
                            ridx0 = ridx0 + 1;
                        }
                    }

                    //
                    // Now we have TQ2, and we can evaluate TQ1.
                    // In the special case when we have Alpha=0, NFree=0 or NFree=N,
                    // TQ1 is filled by zeros.
                    //
                    for (i = 0; i <= n - 1; i++)
                    {
                        s.tq1[i] = 0.0;
                    }
                    if (s.nfree > 0 && s.nfree < n)
                    {
                        Ablas.RealVectorProduct(s.nfree, n - s.nfree, s.tq2dense, 0, s.nfree, 0, s.txc, s.nfree, ref s.tq1, 0);
                    }

                    //
                    // And finally, we evaluate TQ0.
                    //
                    v = 0.0;
                    for (i = s.nfree; i <= n - 1; i++)
                    {
                        for (j = s.nfree; j <= n - 1; j++)
                        {
                            v = v + 0.5 * s.txc[i] * s.tq2dense[i, j] * s.txc[j];
                        }
                    }
                    s.tq0 = v;
                }
                else
                {

                    //
                    // Alpha=0, diagonal QP
                    //
                    // Split variables into two groups - free (F) and constrained (C). Reorder
                    // variables in such way that free vars come first, constrained are last:
                    // x = [xf, xc].
                    // 
                    // Main quadratic term x'*(tau*D)*x now splits into quadratic and constant
                    // parts:
                    //                   ( tau*Df        ) ( xf )              
                    //   0.5*( xf' xc' )*(               )*(    ) =
                    //                   (        tau*Dc ) ( xc )
                    //
                    //   = 0.5*xf'*(tau*Df)*xf + 0.5*xc'(tau*Dc)*xc
                    //                    
                    // We store these parts into temporary variables:
                    // * tau*Df is stored in TQ2Diag
                    // * 0.5*xc'(tau*Dc)*xc is stored into TQ0
                    //
                    s.tq0 = 0.0;
                    ridx0 = 0;
                    for (i = 0; i <= n - 1; i++)
                    {
                        if (!s.activeset[i])
                        {
                            s.tq2diag[ridx0] = s.tau * s.d[i];
                            ridx0 = ridx0 + 1;
                        }
                        else
                        {
                            s.tq0 = s.tq0 + 0.5 * s.tau * s.d[i] * MathExtensions.Squared(s.xc[i]);
                        }
                    }
                    for (i = 0; i <= n - 1; i++)
                    {
                        s.tq1[i] = 0.0;
                    }
                }
            }

            //
            // Re-evaluate TK2/TK1/TK0, if needed
            //
            if (s.isactivesetchanged || s.issecondarytermchanged)
            {

                //
                // Split variables into two groups - free (F) and constrained (C). Reorder
                // variables in such way that free vars come first, constrained are last:
                // x = [xf, xc].
                // 
                // Secondary term theta*(Q*x-r)'*(Q*x-r) now splits into quadratic part,
                // linear part and constant part:
                //             (          ( xf )     )'  (          ( xf )     )
                //   0.5*theta*( (Qf Qc)'*(    ) - r ) * ( (Qf Qc)'*(    ) - r ) =
                //             (          ( xc )     )   (          ( xc )     )
                //
                //   = 0.5*theta*xf'*(Qf'*Qf)*xf + theta*((Qc*xc-r)'*Qf)*xf + 
                //     + theta*(-r'*(Qc*xc-r)-0.5*r'*r+0.5*xc'*Qc'*Qc*xc)
                //                    
                // We store these parts into temporary variables:
                // * sqrt(theta)*Qf is stored into TK2
                // * theta*((Qc*xc-r)'*Qf) is stored into TK1
                // * theta*(-r'*(Qc*xc-r)-0.5*r'*r+0.5*xc'*Qc'*Qc*xc) is stored into TK0
                //
                // We use several other temporaries to store intermediate results:
                // * Tmp0 - to store Qc*xc-r
                // * Tmp1 - to store Qc*xc
                //
                // Generation of TK2/TK1/TK0 is performed as follows:
                // * we fill TK2/TK1/TK0 (to handle K=0 or Theta=0)
                // * other steps are performed only for K>0 and Theta>0
                // * we pass through columns of Q and copy I-th column into left block (Qf) or
                //   right one (Qc) of TK2, depending on presence of X[i] in the active set.
                //   CIdx0 variable contains current position for insertion into upper block,
                //   CIdx1 contains current position for insertion into lower one.
                // * we calculate Qc*xc-r and store it into Tmp0
                // * we calculate TK0 and TK1
                // * we multiply leading part of TK2 which stores Qf by sqrt(theta)
                //   it is important to perform this step AFTER calculation of TK0 and TK1,
                //   because we need original (non-modified) Qf to calculate TK0 and TK1.
                //
                for (j = 0; j <= n - 1; j++)
                {
                    for (i = 0; i <= k - 1; i++)
                    {
                        s.tk2[i, j] = 0.0;
                    }
                    s.tk1[j] = 0.0;
                }
                s.tk0 = 0.0;
                if (s.k > 0 && s.theta > 0)
                {

                    //
                    // Split Q into Qf and Qc
                    // Calculate Qc*xc-r, store in Tmp0
                    //
                    s.tmp0.SetLengthAtLeast(k);
                    s.tmp1.SetLengthAtLeast(k);
                    cidx0 = 0;
                    cidx1 = nfree;
                    for (i = 0; i <= k - 1; i++)
                    {
                        s.tmp1[i] = 0.0;
                    }
                    for (j = 0; j <= n - 1; j++)
                    {
                        if (s.activeset[j])
                        {
                            for (i = 0; i <= k - 1; i++)
                            {
                                s.tk2[i, cidx1] = s.q[i, j];
                                s.tmp1[i] = s.tmp1[i] + s.q[i, j] * s.txc[cidx1];
                            }
                            cidx1 = cidx1 + 1;
                        }
                        else
                        {
                            for (i = 0; i <= k - 1; i++)
                            {
                                s.tk2[i, cidx0] = s.q[i, j];
                            }
                            cidx0 = cidx0 + 1;
                        }
                    }
                    for (i = 0; i <= k - 1; i++)
                    {
                        s.tmp0[i] = s.tmp1[i] - s.r[i];
                    }

                    //
                    // Calculate TK0
                    //
                    v = 0.0;
                    for (i = 0; i <= k - 1; i++)
                    {
                        v = v + s.theta * (0.5 * MathExtensions.Squared(s.tmp1[i]) - s.r[i] * s.tmp0[i] - 0.5 * MathExtensions.Squared(s.r[i]));
                    }
                    s.tk0 = v;

                    //
                    // Calculate TK1
                    //
                    if (nfree > 0)
                    {
                        for (i = 0; i <= k - 1; i++)
                        {
                            v = s.theta * s.tmp0[i];
                            for (i_ = 0; i_ <= nfree - 1; i_++)
                            {
                                s.tk1[i_] = s.tk1[i_] + v * s.tk2[i, i_];
                            }
                        }
                    }

                    //
                    // Calculate TK2
                    //
                    if (nfree > 0)
                    {
                        v = Math.Sqrt(s.theta);
                        for (i = 0; i <= k - 1; i++)
                        {
                            for (i_ = 0; i_ <= nfree - 1; i_++)
                            {
                                s.tk2[i, i_] = v * s.tk2[i, i_];
                            }
                        }
                    }
                }
            }

            //
            // Re-evaluate TB
            //
            if (s.isactivesetchanged || s.islineartermchanged)
            {
                ridx0 = 0;
                ridx1 = nfree;
                for (i = 0; i <= n - 1; i++)
                {
                    if (s.activeset[i])
                    {
                        s.tb[ridx1] = s.b[i];
                        ridx1 = ridx1 + 1;
                    }
                    else
                    {
                        s.tb[ridx0] = s.b[i];
                        ridx0 = ridx0 + 1;
                    }
                }
            }

            //
            // Compose ECA: either dense ECA or diagonal ECA
            //
            if ((s.isactivesetchanged || s.ismaintermchanged) && nfree > 0)
            {
                if (s.alpha > 0)
                {

                    //
                    // Dense ECA
                    //
                    s.ecakind = 0;
                    for (i = 0; i <= nfree - 1; i++)
                    {
                        for (j = i; j <= nfree - 1; j++)
                        {
                            s.ecadense[i, j] = s.tq2dense[i, j];
                        }
                    }
                    if (!Trfac.CholeskyRecursive(ref s.ecadense, 0, nfree, true, ref s.tmp0))
                    {
                        result = false;
                        return result;
                    }
                }
                else
                {

                    //
                    // Diagonal ECA
                    //
                    s.ecakind = 1;
                    for (i = 0; i <= nfree - 1; i++)
                    {
                        if (s.tq2diag[i] < 0)
                        {
                            result = false;
                            return result;
                        }
                        s.ecadiag[i] = Math.Sqrt(s.tq2diag[i]);
                    }
                }
            }

            //
            // Compose EQ
            //
            if (s.isactivesetchanged || s.issecondarytermchanged)
            {
                for (i = 0; i <= k - 1; i++)
                {
                    for (j = 0; j <= nfree - 1; j++)
                    {
                        s.eq[i, j] = s.tk2[i, j];
                    }
                }
            }

            //
            // Calculate ECCM
            //
            if (((((s.isactivesetchanged || s.ismaintermchanged) || s.issecondarytermchanged) && s.k > 0) && s.theta > 0) && nfree > 0)
            {

                //
                // Calculate ECCM - Cholesky factor of the "effective" capacitance
                // matrix CM = I + EQ*inv(EffectiveA)*EQ'.
                //
                // We calculate CM as follows:
                //   CM = I + EQ*inv(EffectiveA)*EQ'
                //      = I + EQ*ECA^(-1)*ECA^(-T)*EQ'
                //      = I + (EQ*ECA^(-1))*(EQ*ECA^(-1))'
                //
                // Then we perform Cholesky decomposition of CM.
                //
                //Utilities.MatrixSetLengthAtLeast(ref s.tmp2, k, n);
                s.tmp2.SetLengthAtLeast(k, n);
                Ablas.MatrixCopy(k, nfree, s.eq, 0, 0, ref s.tmp2, 0, 0);
                Utilities.Assert(s.ecakind == 0 || s.ecakind == 1, "CQMRebuild: unexpected ECAKind");
                if (s.ecakind == 0)
                {
                    Ablas.RealRightTrsm(k, nfree, s.ecadense, 0, 0, true, false, 0, s.tmp2, 0, 0);
                }
                if (s.ecakind == 1)
                {
                    for (i = 0; i <= k - 1; i++)
                    {
                        for (j = 0; j <= nfree - 1; j++)
                        {
                            s.tmp2[i, j] = s.tmp2[i, j] / s.ecadiag[j];
                        }
                    }
                }
                for (i = 0; i <= k - 1; i++)
                {
                    for (j = 0; j <= k - 1; j++)
                    {
                        s.eccm[i, j] = 0.0;
                    }
                    s.eccm[i, i] = 1.0;
                }
                Ablas.RealSyrk(k, nfree, 1.0, s.tmp2, 0, 0, 0, 1.0, s.eccm, 0, 0, true);
                if (!Trfac.CholeskyRecursive(ref s.eccm, 0, k, true, ref s.tmp0))
                {
                    result = false;
                    return result;
                }
            }

            //
            // Compose EB and EC
            //
            // NOTE: because these quantities are cheap to compute, we do not
            // use caching here.
            //
            for (i = 0; i <= nfree - 1; i++)
            {
                s.eb[i] = s.tq1[i] + s.tk1[i] + s.tb[i];
            }
            s.ec = s.tq0 + s.tk0;
            for (i = nfree; i <= n - 1; i++)
            {
                s.ec = s.ec + s.tb[i] * s.txc[i];
            }

            //
            // Change cache status - everything is cached 
            //
            s.ismaintermchanged = false;
            s.issecondarytermchanged = false;
            s.islineartermchanged = false;
            s.isactivesetchanged = false;
            return result;
        }

        /*************************************************************************
            Internal function, solves system Effective_A*x = b.
            It should be called after successful completion of CQMRebuild().

            INPUT PARAMETERS:
                S       -   quadratic model, after call to CQMRebuild()
                X       -   right part B, array[S.NFree]
                Tmp     -   temporary array, automatically reallocated if needed

            OUTPUT PARAMETERS:
                X       -   solution, array[S.NFree]
            
            NOTE: when called with zero S.NFree, returns silently
            NOTE: this function assumes that EA is non-degenerate

              -- ALGLIB --
                 Copyright 10.05.2011 by Bochkanov Sergey
            *************************************************************************/
        private static void SolveEa(ConvexQuadraticModel s,
            ref FluxList<double> x,
            ref FluxList<double> tmp)
        {
            int i = 0;

            Utilities.Assert((s.ecakind == 0 || s.ecakind == 1) || (s.ecakind == -1 && s.nfree == 0), "CQMSolveEA: unexpected ECAKind");
            if (s.ecakind == 0)
            {

                //
                // Dense ECA, use FBLSCholeskySolve() dense solver.
                //
                Fbls.CholeskySolver(s.ecadense, 1.0, s.nfree, true, ref x, ref tmp);
            }
            if (s.ecakind == 1)
            {

                //
                // Diagonal ECA
                //
                for (i = 0; i <= s.nfree - 1; i++)
                {
                    x[i] = x[i] / MathExtensions.Squared(s.ecadiag[i]);
                }
            }
        }
    };
}