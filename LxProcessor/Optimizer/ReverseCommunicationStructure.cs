﻿using System.Collections.Generic;
using LxCommon.DataStructures;
using LxCommon.Utilities;
using LxProcessor.Common;

namespace LxProcessor.Optimizer
{
    public class ReverseCommunicationStructure
    {
        public int stage;
        public FluxList<int> ia;
        public FluxList<bool> ba;
        public FluxList<double> ra;
        
        public ReverseCommunicationStructure()
        {
            stage = -1;
            ia = new FluxList<int>();
            ba = new FluxList<bool>();
            ra = new FluxList<double>();
        }

        public void CleanForReuse()
        {
            stage = -1;
            ia.Clear();
            ba.Clear();
            ra.Clear();
        }
    }
}
