using System;
using System.Collections.Generic;
using LxCommon.DataStructures;
using LxCommon.Utilities;
using LxProcessor.Common;

namespace LxProcessor.Optimizer
{
    public class NormEstimator
    {
        /*************************************************************************
            This object stores state of the iterative norm estimation algorithm.

            You should use ALGLIB functions to work with this object.
            *************************************************************************/
        public class State
        {
            public int n;
            public int m;
            public int nstart;
            public int nits;
            public int seedval;
            public FluxList<double> x0;
            public FluxList<double> x1;
            public FluxList<double> t;
            public FluxList<double> xbest;
            public HighQualityRandom.State r;
            public FluxList<double> x;
            public FluxList<double> mv;
            public FluxList<double> mtv;
            public bool needmv;
            public bool needmtv;
            public double repnorm;
            public ReverseCommunicationStructure rstate;
            public State()
            {
                x0 = new FluxList<double>();
                x1 = new FluxList<double>();
                t = new FluxList<double>();
                xbest = new FluxList<double>();
                r = new HighQualityRandom.State();
                x = new FluxList<double>();
                mv = new FluxList<double>();
                mtv = new FluxList<double>();
                rstate = new ReverseCommunicationStructure();
            }

          

            internal void CleanForReuse()
            {
                n = default(int);
                m = default(int);
                nstart = default(int);
                nits = default(int);
                seedval = default(int);
                x0.Clear();
                x1.Clear();;
                t.Clear();;
                xbest.Clear();;
                r.CleanForReuse();
                x.Clear();;
                mv.Clear();;
                mtv.Clear();;
                needmv = default(bool);
                needmtv = default(bool);
                repnorm = default(double);
                rstate.CleanForReuse();
            }
        }




        /*************************************************************************
            This procedure initializes matrix norm estimator.

            USAGE:
            1. User initializes algorithm state with NormEstimatorCreate() call
            2. User calls NormEstimatorEstimateSparse() (or NormEstimatorIteration())
            3. User calls NormEstimatorResults() to get solution.
           
            INPUT PARAMETERS:
                M       -   number of rows in the matrix being estimated, M>0
                N       -   number of columns in the matrix being estimated, N>0
                NStart  -   number of random starting vectors
                            recommended value - at least 5.
                NIts    -   number of iterations to do with best starting vector
                            recommended value - at least 5.

            OUTPUT PARAMETERS:
                State   -   structure which stores algorithm state

            
            NOTE: this algorithm is effectively deterministic, i.e. it always  returns
            same result when repeatedly called for the same matrix. In fact, algorithm
            uses randomized starting vectors, but internal  random  numbers  generator
            always generates same sequence of the random values (it is a  feature, not
            bug).

            Algorithm can be made non-deterministic with NormEstimatorSetSeed(0) call.

              -- ALGLIB --
                 Copyright 06.12.2011 by Bochkanov Sergey
            *************************************************************************/
        public static void Create(int m, int n, int nstart, int nits, State state)
        {
            Utilities.Assert(m > 0, "NormEstimatorCreate: M<=0");
            Utilities.Assert(n > 0, "NormEstimatorCreate: N<=0");
            Utilities.Assert(nstart > 0, "NormEstimatorCreate: NStart<=0");
            Utilities.Assert(nits > 0, "NormEstimatorCreate: NIts<=0");
            state.m = m;
            state.n = n;
            state.nstart = nstart;
            state.nits = nits;
            state.seedval = 11;
            HighQualityRandom.Randomize(state.r);
            state.x0.Resize(state.n);
            state.t.Resize(state.m);
            state.x1.Resize(state.n);
            state.xbest.Resize(state.n);
            state.x.Resize(Math.Max(state.n, state.m));
            state.mv.Resize(state.m);
            state.mtv.Remove(state.n);
            state.rstate.ia.Resize(4);
            state.rstate.ra.Resize(3);
            state.rstate.stage = -1;
        }
    }
}