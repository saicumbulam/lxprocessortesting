namespace LxProcessor.Optimizer
{
    public class HighQualityRandom
    {
        /*************************************************************************
            Portable high quality random number generator state.
            Initialized with HQRNDRandomize() or HQRNDSeed().

            Fields:
                S1, S2      -   seed values
                V           -   precomputed value
                MagicV      -   'magic' value used to determine whether State structure
                                was correctly initialized.
            *************************************************************************/
        public class State
        {
            public int s1;
            public int s2;
            public int magicv;
            public State()
            {
                
            }
            
            internal void CleanForReuse()
            {
                s1 = default(int);
                s2 = default(int);
                magicv = default(int);
            }
        };




        private const int Max = 2147483561;
        private const int M1 = 2147483563;
        private const int M2 = 2147483399;
        private const int Magic = 1634357784;


        /*************************************************************************
            HQRNDState  initialization  with  random  values  which come from standard
            RNG.

              -- ALGLIB --
                 Copyright 02.12.2009 by Bochkanov Sergey
            *************************************************************************/
        public static void Randomize(State state)
        {
            int s0 = 0;
            int s1 = 0;

            s0 = MathExtensions.GetRandomInteger(M1);
            s1 = MathExtensions.GetRandomInteger(M2);
            Seed(s0, s1, state);
        }


        /*************************************************************************
            HQRNDState initialization with seed values

              -- ALGLIB --
                 Copyright 02.12.2009 by Bochkanov Sergey
            *************************************************************************/
        public static void Seed(int s1,
            int s2,
            State state)
        {

            //
            // Protection against negative seeds:
            //
            //     SEED := -(SEED+1)
            //
            // We can use just "-SEED" because there exists such integer number  N
            // that N<0, -N=N<0 too. (This number is equal to 0x800...000).   Need
            // to handle such seed correctly forces us to use  a  bit  complicated
            // formula.
            //
            if (s1 < 0)
            {
                s1 = -(s1 + 1);
            }
            if (s2 < 0)
            {
                s2 = -(s2 + 1);
            }
            state.s1 = s1 % (M1 - 1) + 1;
            state.s2 = s2 % (M2 - 1) + 1;
            state.magicv = Magic;
        }


        /*************************************************************************
            This function generates random real number in (0,1),
            not including interval boundaries

            State structure must be initialized with HQRNDRandomize() or HQRNDSeed().

              -- ALGLIB --
                 Copyright 02.12.2009 by Bochkanov Sergey
            *************************************************************************/
        public static double GenerateDouble(State state)
        {
            double result = 0;

            result = (GenerateIntegerBase(state) + 1) / (double)(Max + 2);
            return result;
        }


        /*************************************************************************
            This function generates random integer number in [0, N)

            1. State structure must be initialized with HQRNDRandomize() or HQRNDSeed()
            2. N can be any positive number except for very large numbers:
               * close to 2^31 on 32-bit systems
               * close to 2^62 on 64-bit systems
               An exception will be generated if N is too large.

              -- ALGLIB --
                 Copyright 02.12.2009 by Bochkanov Sergey
            *************************************************************************/
        public static int GenerateInt(State state,
            int n)
        {
            int result = 0;
            int maxcnt = 0;
            int mx = 0;
            int a = 0;
            int b = 0;

            Utilities.Assert(n > 0, "HQRNDUniformI: N<=0!");
            maxcnt = Max + 1;

            //
            // Two branches: one for N<=MaxCnt, another for N>MaxCnt.
            //
            if (n > maxcnt)
            {

                //
                // N>=MaxCnt.
                //
                // We have two options here:
                // a) N is exactly divisible by MaxCnt
                // b) N is not divisible by MaxCnt
                //
                // In both cases we reduce problem on interval spanning [0,N)
                // to several subproblems on intervals spanning [0,MaxCnt).
                //
                if (n % maxcnt == 0)
                {

                    //
                    // N is exactly divisible by MaxCnt.
                    //
                    // [0,N) range is dividided into N/MaxCnt bins,
                    // each of them having length equal to MaxCnt.
                    //
                    // We generate:
                    // * random bin number B
                    // * random offset within bin A
                    // Both random numbers are generated by recursively
                    // calling HQRNDUniformI().
                    //
                    // Result is equal to A+MaxCnt*B.
                    //
                    Utilities.Assert(n / maxcnt <= maxcnt, "HQRNDUniformI: N is too large");
                    a = GenerateInt(state, maxcnt);
                    b = GenerateInt(state, n / maxcnt);
                    result = a + maxcnt * b;
                }
                else
                {

                    //
                    // N is NOT exactly divisible by MaxCnt.
                    //
                    // [0,N) range is dividided into Ceil(N/MaxCnt) bins,
                    // each of them having length equal to MaxCnt.
                    //
                    // We generate:
                    // * random bin number B in [0, Ceil(N/MaxCnt)-1]
                    // * random offset within bin A
                    // * if both of what is below is true
                    //   1) bin number B is that of the last bin
                    //   2) A >= N mod MaxCnt
                    //   then we repeat generation of A/B.
                    //   This stage is essential in order to avoid bias in the result.
                    // * otherwise, we return A*MaxCnt+N
                    //
                    Utilities.Assert(n / maxcnt + 1 <= maxcnt, "HQRNDUniformI: N is too large");
                    result = -1;
                    do
                    {
                        a = GenerateInt(state, maxcnt);
                        b = GenerateInt(state, n / maxcnt + 1);
                        if (b == n / maxcnt && a >= n % maxcnt)
                        {
                            continue;
                        }
                        result = a + maxcnt * b;
                    }
                    while (result < 0);
                }
            }
            else
            {

                //
                // N<=MaxCnt
                //
                // Code below is a bit complicated because we can not simply
                // return "HQRNDIntegerBase() mod N" - it will be skewed for
                // large N's in [0.1*HQRNDMax...HQRNDMax].
                //
                mx = maxcnt - maxcnt % n;
                do
                {
                    result = GenerateIntegerBase(state);
                }
                while (result >= mx);
                result = result % n;
            }
            return result;
        }


            
        /*************************************************************************
            This function returns random integer in [0,HQRNDMax]

            L'Ecuyer, Efficient and portable combined random number generators
            *************************************************************************/
        private static int GenerateIntegerBase(State state)
        {
            int result = 0;
            int k = 0;

            Utilities.Assert(state.magicv == Magic, "HQRNDIntegerBase: State is not correctly initialized!");
            k = state.s1 / 53668;
            state.s1 = 40014 * (state.s1 - k * 53668) - k * 12211;
            if (state.s1 < 0)
            {
                state.s1 = state.s1 + 2147483563;
            }
            k = state.s2 / 52774;
            state.s2 = 40692 * (state.s2 - k * 52774) - k * 3791;
            if (state.s2 < 0)
            {
                state.s2 = state.s2 + 2147483399;
            }

            //
            // Result
            //
            result = state.s1 - state.s2;
            if (result < 1)
            {
                result = result + 2147483562;
            }
            result = result - 1;
            return result;
        }


    }
}