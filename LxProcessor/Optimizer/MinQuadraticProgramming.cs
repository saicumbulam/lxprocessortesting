﻿using System;
using System.Collections.Generic;
using LxCommon.DataStructures;
using LxCommon.Utilities;
using LxProcessor.Common;


namespace LxProcessor.Optimizer
{

    public class MinQuadraticProgramming
    {
        /*************************************************************************
        This object stores nonlinear optimizer state.
        You should use functions provided by MinQP subpackage to work with this
        object
        *************************************************************************/
        public class State
        {
            public int n;
            public int algokind;
            public int akind;
            public ConvexQuadraticModel a;
            public SparseMatrix sparsea;
            public bool sparseaupper;
            public double anorm;
            public FluxList<double> b;
            public FluxList<double> bndl;
            public FluxList<double> bndu;
            public FluxList<double> s;
            public FluxList<bool> havebndl;
            public FluxList<bool> havebndu;
            public FluxList<double> xorigin;
            public FluxList<double> startx;
            public bool havex;
            public Matrix<double> cleic;
            public int nec;
            public int nic;
            public double bleicepsg;
            public double bleicepsf;
            public double bleicepsx;
            public int bleicmaxits;
            public ActiveSet sas;
            public FluxList<double> gc;
            public FluxList<double> xn;
            public FluxList<double> pg;
            public FluxList<double> workbndl;
            public FluxList<double> workbndu;
            public Matrix<double> workcleic;
            public FluxList<double> xs;
            public int repinneriterationscount;
            public int repouteriterationscount;
            public int repncholesky;
            public int repnmv;
            public int repterminationtype;
            public double debugphase1flops;
            public double debugphase2flops;
            public double debugphase3flops;
            public FluxList<double> tmp0;
            public FluxList<double> tmp1;
            public FluxList<bool> tmpb;
            public FluxList<double> rctmpg;
            public FluxList<int> tmpi;
            public NormEstimator.State estimator;
            public MinBleic.State solver;
            public MinBleic.Report solverrep;
            public State()
            {
                a = new ConvexQuadraticModel();
                sparsea = new SparseMatrix();
                b = new FluxList<double>();
                bndl = new FluxList<double>();
                bndu = new FluxList<double>();
                s = new FluxList<double>();
                havebndl = new FluxList<bool>();
                havebndu = new FluxList<bool>();
                xorigin = new FluxList<double>();
                startx = new FluxList<double>();
                cleic = new Matrix<double>(0, 0);
                sas = new ActiveSet();
                gc = new FluxList<double>();
                xn = new FluxList<double>();
                pg = new FluxList<double>();
                workbndl = new FluxList<double>();
                workbndu = new FluxList<double>();
                workcleic = new Matrix<double>(0, 0);
                xs = new FluxList<double>();
                tmp0 = new FluxList<double>();
                tmp1 = new FluxList<double>();
                tmpb = new FluxList<bool>();
                rctmpg = new FluxList<double>();
                tmpi = new FluxList<int>();
                estimator = new NormEstimator.State();
                solver = new MinBleic.State();
                solverrep = new MinBleic.Report();
            }

            public void CleanForReuse()
            {
                n = default(int);
                algokind = default(int);;
                akind = default(int);;
                a.CleanForReuse();
                sparsea.CleanForReuse();
                sparseaupper = default(bool);;
                anorm = default(double);
                b.Clear();
                bndl.Clear();
                bndu.Clear();
                s.Clear();
                havebndl.Clear();
                havebndu.Clear();
                xorigin.Clear();
                startx.Clear();
                havex = default(bool);
                cleic.Clear();
                nec = default(int);
                nic = default(int);
                bleicepsg = default(double);
                bleicepsf = default(double);
                bleicepsx = default(double);
                bleicmaxits = default(int);;
                sas.CleanForReuse();
                gc.Clear();
                xn.Clear();
                pg.Clear();
                workbndl.Clear();
                workbndu.Clear();
                workcleic.Clear();
                xs.Clear();
                repinneriterationscount = default(int);;
                repouteriterationscount = default(int);;
                repncholesky = default(int);;
                repnmv = default(int);;
                repterminationtype = default(int);;
                debugphase1flops = default(double);
                debugphase2flops = default(double);
                debugphase3flops = default(double);
                tmp0.Clear();
                tmp1.Clear();
                tmpb.Clear();
                rctmpg.Clear();
                tmpi.Clear();
                estimator.CleanForReuse();
                solver.CleanForReuse();
                solverrep.CleanForReuse();
            }
        }


        /*************************************************************************
        This structure stores optimization report:
        * InnerIterationsCount      number of inner iterations
        * OuterIterationsCount      number of outer iterations
        * NCholesky                 number of Cholesky decomposition
        * NMV                       number of matrix-vector products
                                    (only products calculated as part of iterative
                                    process are counted)
        * TerminationType           completion code (see below)

        Completion codes:
        * -5    inappropriate solver was used:
                * Cholesky solver for semidefinite or indefinite problems
                * Cholesky solver for problems with non-boundary constraints
        * -4    BLEIC-QP algorithm found unconstrained direction
                of negative curvature (function is unbounded from
                below  even  under  constraints),  no  meaningful
                minimum can be found.
        * -3    inconsistent constraints (or, maybe, feasible point is
                too hard to find). If you are sure that constraints are feasible,
                try to restart optimizer with better initial approximation.
        * -1    solver error
        *  4    successful completion
        *  5    MaxIts steps was taken
        *  7    stopping conditions are too stringent,
                further improvement is impossible,
                X contains best point found so far.
        *************************************************************************/
        public class Report
        {
            public int inneriterationscount;
            public int outeriterationscount;
            public int nmv;
            public int ncholesky;
            public int terminationtype;
            public Report()
            {
                
            }

            public void CleanForReuse()
            {
                inneriterationscount = default(int);
                outeriterationscount = default(int);
                nmv = default(int);
                ncholesky = default(int);
                terminationtype = default(int);
            }
        }




        private const int MaxLaGrangeIterations = 10;
        private const int MaxBadNewtonIterations = 7;
        private const double PenaltyFactor = 100.0;


        /*************************************************************************
                            CONSTRAINED QUADRATIC PROGRAMMING

        The subroutine creates QP optimizer. After initial creation,  it  contains
        default optimization problem with zero quadratic and linear terms  and  no
        constraints. You should set quadratic/linear terms with calls to functions
        provided by MinQP subpackage.

        INPUT PARAMETERS:
            N       -   problem size
            
        OUTPUT PARAMETERS:
            State   -   optimizer with zero quadratic/linear terms
                        and no constraints

          -- ALGLIB --
             Copyright 11.01.2011 by Bochkanov Sergey
        *************************************************************************/
        public static void Create(int n,
            State state)
        {
            int i = 0;

            Utilities.Assert(n >= 1, "MinQPCreate: N<1");

            //
            // initialize QP solver
            //
            state.n = n;
            state.nec = 0;
            state.nic = 0;
            state.repterminationtype = 0;
            state.anorm = 1;
            state.akind = 0;
            ConvexQuadraticModel.Init(n, state.a);
            ActiveSet.Initialize(n, state.sas);
            state.b.Resize(n);
            state.bndl.Resize(n);
            state.bndu.Resize(n);
            state.workbndl.Resize(n);
            state.workbndu.Resize(n);
            state.havebndl.Resize(n);
            state.havebndu.Resize(n);
            state.s.Resize(n);
            state.startx.Resize(n);
            state.xorigin.Resize(n);
            state.xs.Resize(n);
            state.xn.Resize(n);
            state.gc.Resize(n);
            state.pg.Resize(n);
            for (i = 0; i <= n - 1; i++)
            {
                state.bndl[i] = Double.NegativeInfinity;
                state.bndu[i] = Double.PositiveInfinity;
                state.havebndl[i] = false;
                state.havebndu[i] = false;
                state.b[i] = 0.0;
                state.startx[i] = 0.0;
                state.xorigin[i] = 0.0;
                state.s[i] = 1.0;
            }
            state.havex = false;
            SetCholeskyAlgorithm(state);
            NormEstimator.Create(n, n, 5, 5, state.estimator);
            MinBleic.Create(n, state.startx, state.solver);
        }

        /*************************************************************************
        This function tells solver to use Cholesky-based algorithm. This algorithm
        is active by default.

        DESCRIPTION:

        Cholesky-based algorithm can be used only for problems which:
        * have dense quadratic term, set  by  MinQPSetQuadraticTerm(),  sparse  or
          structured problems are not supported.
        * are strictly convex, i.e. quadratic term is symmetric positive definite,
          indefinite or semidefinite problems are not supported by this algorithm.

        If anything of what listed above is violated, you may use  BLEIC-based  QP
        algorithm which can be activated by MinQPSetAlgoBLEIC().

        BENEFITS AND DRAWBACKS:

        This  algorithm  gives  best  precision amongst all QP solvers provided by
        ALGLIB (Newton iterations  have  much  higher  precision  than  any  other
        optimization algorithm). This solver also gracefully handles problems with
        very large amount of constraints.

        Performance of the algorithm is good because internally  it  uses  Level 3
        Dense BLAS for its performance-critical parts.


        From the other side, algorithm has  O(N^3)  complexity  for  unconstrained
        problems and up to orders of  magnitude  slower  on  constrained  problems
        (these additional iterations are needed to identify  active  constraints).
        So, its running time depends on number of constraints active  at solution.

        Furthermore, this algorithm can not solve problems with sparse matrices or
        problems with semidefinite/indefinite matrices of any kind (dense/sparse).

        INPUT PARAMETERS:
            State   -   structure which stores algorithm state

          -- ALGLIB --
             Copyright 11.01.2011 by Bochkanov Sergey
        *************************************************************************/
        public static void SetCholeskyAlgorithm(State state)
        {
            state.algokind = 1;
        }





        /*************************************************************************
        This function sets boundary constraints for QP solver

        Boundary constraints are inactive by default (after initial creation).
        After  being  set,  they  are  preserved  until explicitly turned off with
        another SetBC() call.

        INPUT PARAMETERS:
            State   -   structure stores algorithm state
            BndL    -   lower bounds, array[N].
                        If some (all) variables are unbounded, you may specify
                        very small number or -INF (latter is recommended because
                        it will allow solver to use better algorithm).
            BndU    -   upper bounds, array[N].
                        If some (all) variables are unbounded, you may specify
                        very large number or +INF (latter is recommended because
                        it will allow solver to use better algorithm).
                        
        NOTE: it is possible to specify BndL[i]=BndU[i]. In this case I-th
        variable will be "frozen" at X[i]=BndL[i]=BndU[i].

          -- ALGLIB --
             Copyright 11.01.2011 by Bochkanov Sergey
        *************************************************************************/
        public static void SetBoundaryConstraints(State state,
            FluxList<double> bndl,
            FluxList<double> bndu)
        {
            int i = 0;
            int n = 0;

            n = state.n;
            Utilities.Assert(bndl.Count >= n, "MinQPSetBC: Length(BndL)<N");
            Utilities.Assert(bndu.Count >= n, "MinQPSetBC: Length(BndU)<N");
            for (i = 0; i <= n - 1; i++)
            {
                Utilities.Assert(MathExtensions.IsFinite(bndl[i]) || Double.IsNegativeInfinity(bndl[i]), "MinQPSetBC: BndL contains NAN or +INF");
                Utilities.Assert(MathExtensions.IsFinite(bndu[i]) || Double.IsPositiveInfinity(bndu[i]), "MinQPSetBC: BndU contains NAN or -INF");
                state.bndl[i] = bndl[i];
                state.havebndl[i] = MathExtensions.IsFinite(bndl[i]);
                state.bndu[i] = bndu[i];
                state.havebndu[i] = MathExtensions.IsFinite(bndu[i]);
            }
        }





        /*************************************************************************
        This function solves quadratic programming problem.
        You should call it after setting solver options with MinQPSet...() calls.

        INPUT PARAMETERS:
            State   -   algorithm state

        You should use MinQPResults() function to access results after calls
        to this function.

          -- ALGLIB --
             Copyright 11.01.2011 by Bochkanov Sergey.
             Special thanks to Elvira Illarionova  for  important  suggestions  on
             the linearly constrained QP algorithm.
        *************************************************************************/
        public static void Optimize(State state)
        {
            int n = 0;
            int i = 0;
            int nbc = 0;
            double v0 = 0;
            double v1 = 0;
            double v = 0;
            double d2 = 0;
            double d1 = 0;
            double d0 = 0;
            double noisetolerance = 0;
            double fprev = 0;
            double fcand = 0;
            double fcur = 0;
            int nextaction = 0;
            int actstatus = 0;
            double noiselevel = 0;
            int badnewtonits = 0;
            double maxscaledgrad = 0;
            int i_ = 0;

            noisetolerance = 10;
            n = state.n;
            state.repterminationtype = -5;
            state.repinneriterationscount = 0;
            state.repouteriterationscount = 0;
            state.repncholesky = 0;
            state.repnmv = 0;
            state.debugphase1flops = 0;
            state.debugphase2flops = 0;
            state.debugphase3flops = 0;
            state.rctmpg.SetLengthAtLeast(n);

            //
            // check correctness of constraints
            //
            for (i = 0; i <= n - 1; i++)
            {
                if (state.havebndl[i] && state.havebndu[i])
                {
                    if (state.bndl[i] > state.bndu[i])
                    {
                        state.repterminationtype = -3;
                        return;
                    }
                }
            }

            //
            // count number of bound and linear constraints
            //
            nbc = 0;
            for (i = 0; i <= n - 1; i++)
            {
                if (state.havebndl[i])
                {
                    nbc = nbc + 1;
                }
                if (state.havebndu[i])
                {
                    nbc = nbc + 1;
                }
            }

            //
            // Initial point:
            // * if we have starting point in StartX, we just have to bound it
            // * if we do not have StartX, deduce initial point from boundary constraints
            //
            if (state.havex)
            {
                for (i = 0; i <= n - 1; i++)
                {
                    state.xs[i] = state.startx[i];
                    if (state.havebndl[i] && state.xs[i] < state.bndl[i])
                    {
                        state.xs[i] = state.bndl[i];
                    }
                    if (state.havebndu[i] && state.xs[i] > state.bndu[i])
                    {
                        state.xs[i] = state.bndu[i];
                    }
                }
            }
            else
            {
                for (i = 0; i <= n - 1; i++)
                {
                    if (state.havebndl[i] && state.havebndu[i])
                    {
                        state.xs[i] = 0.5 * (state.bndl[i] + state.bndu[i]);
                        continue;
                    }
                    if (state.havebndl[i])
                    {
                        state.xs[i] = state.bndl[i];
                        continue;
                    }
                    if (state.havebndu[i])
                    {
                        state.xs[i] = state.bndu[i];
                        continue;
                    }
                    state.xs[i] = 0;
                }
            }

            //
            // Cholesky solver.
            //
            if (state.algokind == 1)
            {

                //
                // Check matrix type.
                // Cholesky solver supports only dense matrices.
                //
                if (state.akind != 0)
                {
                    state.repterminationtype = -5;
                    return;
                }

                //
                // Our formulation of quadratic problem includes origin point,
                // i.e. we have F(x-x_origin) which is minimized subject to
                // constraints on x, instead of having simply F(x).
                //
                // Here we make transition from non-zero origin to zero one.
                // In order to make such transition we have to:
                // 1. subtract x_origin from x_start
                // 2. modify constraints
                // 3. solve problem
                // 4. add x_origin to solution
                //
                // There is alternate solution - to modify quadratic function
                // by expansion of multipliers containing (x-x_origin), but
                // we prefer to modify constraints, because it is a) more precise
                // and b) easier to to.
                //
                // Parts (1)-(2) are done here. After this block is over,
                // we have:
                // * XS, which stores shifted XStart (if we don't have XStart,
                //   value of XS will be ignored later)
                // * WorkBndL, WorkBndU, which store modified boundary constraints.
                //
                for (i = 0; i <= n - 1; i++)
                {
                    if (state.havebndl[i])
                    {
                        state.workbndl[i] = state.bndl[i] - state.xorigin[i];
                    }
                    else
                    {
                        state.workbndl[i] = Double.NegativeInfinity;
                    }
                    if (state.havebndu[i])
                    {
                        state.workbndu[i] = state.bndu[i] - state.xorigin[i];
                    }
                    else
                    {
                        state.workbndu[i] = Double.PositiveInfinity;
                    }
                }
                state.workcleic.SetLengthAtLeast(state.nec + state.nic, n + 1);
                for (i = 0; i <= state.nec + state.nic - 1; i++)
                {
                    v = 0.0;
                    for (i_ = 0; i_ <= n - 1; i_++)
                    {
                        v += state.cleic[i, i_] * state.xorigin[i_];
                    }
                    for (i_ = 0; i_ <= n - 1; i_++)
                    {
                        state.workcleic[i, i_] = state.cleic[i, i_];
                    }
                    state.workcleic[i, n] = state.cleic[i, n] - v;
                }

                //
                // Starting point XS
                //
                if (state.havex)
                {

                    //
                    // We have starting point in StartX, so we just have to shift and bound it
                    //
                    for (i = 0; i <= n - 1; i++)
                    {
                        state.xs[i] = state.startx[i] - state.xorigin[i];
                        if (state.havebndl[i])
                        {
                            if (state.xs[i] < state.workbndl[i])
                            {
                                state.xs[i] = state.workbndl[i];
                            }
                        }
                        if (state.havebndu[i])
                        {
                            if (state.xs[i] > state.workbndu[i])
                            {
                                state.xs[i] = state.workbndu[i];
                            }
                        }
                    }
                }
                else
                {

                    //
                    // We don't have starting point, so we deduce it from
                    // constraints (if they are present).
                    //
                    // NOTE: XS contains some meaningless values from previous block
                    // which are ignored by code below.
                    //
                    for (i = 0; i <= n - 1; i++)
                    {
                        if (state.havebndl[i] && state.havebndu[i])
                        {
                            state.xs[i] = 0.5 * (state.workbndl[i] + state.workbndu[i]);
                            if (state.xs[i] < state.workbndl[i])
                            {
                                state.xs[i] = state.workbndl[i];
                            }
                            if (state.xs[i] > state.workbndu[i])
                            {
                                state.xs[i] = state.workbndu[i];
                            }
                            continue;
                        }
                        if (state.havebndl[i])
                        {
                            state.xs[i] = state.workbndl[i];
                            continue;
                        }
                        if (state.havebndu[i])
                        {
                            state.xs[i] = state.workbndu[i];
                            continue;
                        }
                        state.xs[i] = 0;
                    }
                }

                //
                // Handle special case - no constraints
                //
                if (nbc == 0 && state.nec + state.nic == 0)
                {

                    //
                    // "Simple" unconstrained Cholesky
                    //
                    state.tmpb.SetLengthAtLeast(n);
                    for (i = 0; i <= n - 1; i++)
                    {
                        state.tmpb[i] = false;
                    }
                    state.repncholesky = state.repncholesky + 1;
                    ConvexQuadraticModel.ChangeLinearTerm(state.a, state.b);
                    ConvexQuadraticModel.ChangeActiveSet(state.a, state.xs, state.tmpb);
                    if (!ConvexQuadraticModel.FindOptimumConstrained(state.a, ref state.xn))
                    {
                        state.repterminationtype = -5;
                        return;
                    }
                    for (i_ = 0; i_ <= n - 1; i_++)
                    {
                        state.xs[i_] = state.xn[i_];
                    }
                    for (i_ = 0; i_ <= n - 1; i_++)
                    {
                        state.xs[i_] = state.xs[i_] + state.xorigin[i_];
                    }
                    state.repinneriterationscount = 1;
                    state.repouteriterationscount = 1;
                    state.repterminationtype = 4;
                    return;
                }

                //
                // Prepare "active set" structure
                //
                ActiveSet.SetBoundaryConstraints(state.sas, state.workbndl, state.workbndu);
                ActiveSet.SetsLinearConstraintsX(state.sas, state.workcleic, state.nec, state.nic);
                ActiveSet.SetScale(state.sas, state.s);
                if (!ActiveSet.StartOptimization(state.sas, state.xs))
                {
                    state.repterminationtype = -3;
                    return;
                }

                //
                // Main cycle of CQP algorithm
                //
                state.repterminationtype = 4;
                badnewtonits = 0;
                maxscaledgrad = 0.0;
                while (true)
                {

                    //
                    // Update iterations count
                    //
                    Utilities.Increment(ref state.repouteriterationscount);
                    Utilities.Increment(ref state.repinneriterationscount);

                    //
                    // Phase 1.
                    //
                    // Determine active set.
                    // Update MaxScaledGrad.
                    //
                    ConvexQuadraticModel.Adx(state.a, state.sas.xc, ref state.rctmpg);
                    for (i_ = 0; i_ <= n - 1; i_++)
                    {
                        state.rctmpg[i_] = state.rctmpg[i_] + state.b[i_];
                    }
                    ActiveSet.RecalculateConstraints(state.sas, state.rctmpg);
                    v = 0.0;
                    for (i = 0; i <= n - 1; i++)
                    {
                        v = v + MathExtensions.Squared(state.rctmpg[i] * state.s[i]);
                    }
                    maxscaledgrad = Math.Max(maxscaledgrad, Math.Sqrt(v));

                    //
                    // Phase 2: perform penalized steepest descent step.
                    //
                    // NextAction control variable is set on exit from this loop:
                    // * NextAction>0 in case we have to proceed to Phase 3 (Newton step)
                    // * NextAction<0 in case we have to proceed to Phase 1 (recalculate active set)
                    // * NextAction=0 in case we found solution (step along projected gradient is small enough)
                    //
                    while (true)
                    {

                        //
                        // Calculate constrained descent direction, store to PG.
                        // Successful termination if PG is zero.
                        //
                        ConvexQuadraticModel.Adx(state.a, state.sas.xc, ref state.gc);
                        for (i_ = 0; i_ <= n - 1; i_++)
                        {
                            state.gc[i_] = state.gc[i_] + state.b[i_];
                        }
                        ActiveSet.ConstrainedDescent(state.sas, state.gc, ref state.pg);
                        state.debugphase2flops = state.debugphase2flops + 4 * (state.nec + state.nic) * n;
                        v0 = 0.0;
                        for (i_ = 0; i_ <= n - 1; i_++)
                        {
                            v0 += state.pg[i_] * state.pg[i_];
                        }
                        if (v0 == 0)
                        {

                            //
                            // Constrained derivative is zero.
                            // Solution found.
                            //
                            nextaction = 0;
                            break;
                        }

                        //
                        // Build quadratic model of F along descent direction:
                        //     F(xc+alpha*pg) = D2*alpha^2 + D1*alpha + D0
                        // Store noise level in the XC (noise level is used to classify
                        // step as singificant or insignificant).
                        //
                        // In case function curvature is negative or product of descent
                        // direction and gradient is non-negative, iterations are terminated.
                        //
                        // NOTE: D0 is not actually used, but we prefer to maintain it.
                        //
                        fprev = ModelValue(state.a, state.b, state.sas.xc, n, ref state.tmp0);
                        fprev = fprev + PenaltyFactor * maxscaledgrad * ActiveSet.ActiveLcPenalty1(state.sas, state.sas.xc);
                        ConvexQuadraticModel.EvaluateModel(state.a, state.sas.xc, ref v, ref noiselevel);
                        v0 = ConvexQuadraticModel.XtAdX2(state.a, state.pg);
                        state.debugphase2flops = state.debugphase2flops + 3 * 2 * n * n;
                        d2 = v0;
                        v1 = 0.0;
                        for (i_ = 0; i_ <= n - 1; i_++)
                        {
                            v1 += state.pg[i_] * state.gc[i_];
                        }
                        d1 = v1;
                        d0 = fprev;
                        if (d2 <= 0)
                        {

                            //
                            // Second derivative is non-positive, function is non-convex.
                            //
                            state.repterminationtype = -5;
                            nextaction = 0;
                            break;
                        }
                        if (d1 >= 0)
                        {

                            //
                            // Second derivative is positive, first derivative is non-negative.
                            // Solution found.
                            //
                            nextaction = 0;
                            break;
                        }

                        //
                        // Modify quadratic model - add penalty for violation of the active
                        // constraints.
                        //
                        // Boundary constraints are always satisfied exactly, so we do not
                        // add penalty term for them. General equality constraint of the
                        // form a'*(xc+alpha*d)=b adds penalty term:
                        //     P(alpha) = (a'*(xc+alpha*d)-b)^2
                        //              = (alpha*(a'*d) + (a'*xc-b))^2
                        //              = alpha^2*(a'*d)^2 + alpha*2*(a'*d)*(a'*xc-b) + (a'*xc-b)^2
                        // Each penalty term is multiplied by 100*Anorm before adding it to
                        // the 1-dimensional quadratic model.
                        //
                        // Penalization of the quadratic model improves behavior of the
                        // algorithm in the presense of the multiple degenerate constraints.
                        // In particular, it prevents algorithm from making large steps in
                        // directions which violate equality constraints.
                        //
                        for (i = 0; i <= state.nec + state.nic - 1; i++)
                        {
                            if (state.sas.activeset[n + i] > 0)
                            {
                                v0 = 0.0;
                                for (i_ = 0; i_ <= n - 1; i_++)
                                {
                                    v0 += state.workcleic[i, i_] * state.pg[i_];
                                }
                                v1 = 0.0;
                                for (i_ = 0; i_ <= n - 1; i_++)
                                {
                                    v1 += state.workcleic[i, i_] * state.sas.xc[i_];
                                }
                                v1 = v1 - state.workcleic[i, n];
                                v = 100 * state.anorm;
                                d2 = d2 + v * MathExtensions.Squared(v0);
                                d1 = d1 + v * 2 * v0 * v1;
                                d0 = d0 + v * MathExtensions.Squared(v1);
                            }
                        }
                        state.debugphase2flops = state.debugphase2flops + 2 * 2 * (state.nec + state.nic) * n;

                        //
                        // Try unbounded step.
                        // In case function change is dominated by noise or function actually increased
                        // instead of decreasing, we terminate iterations.
                        //
                        v = -(d1 / (2 * d2));
                        for (i_ = 0; i_ <= n - 1; i_++)
                        {
                            state.xn[i_] = state.sas.xc[i_];
                        }
                        for (i_ = 0; i_ <= n - 1; i_++)
                        {
                            state.xn[i_] = state.xn[i_] + v * state.pg[i_];
                        }
                        fcand = ModelValue(state.a, state.b, state.xn, n, ref state.tmp0);
                        fcand = fcand + PenaltyFactor * maxscaledgrad * ActiveSet.ActiveLcPenalty1(state.sas, state.xn);
                        state.debugphase2flops = state.debugphase2flops + 2 * n * n;
                        if (fcand >= fprev - noiselevel * noisetolerance)
                        {
                            nextaction = 0;
                            break;
                        }

                        //
                        // Save active set
                        // Perform bounded step with (possible) activation
                        //
                        actstatus = BoundedStepAndActivation(state, state.xn, ref state.tmp0);
                        fcur = ModelValue(state.a, state.b, state.sas.xc, n, ref state.tmp0);
                        state.debugphase2flops = state.debugphase2flops + 2 * n * n;

                        //
                        // Depending on results, decide what to do:
                        // 1. In case step was performed without activation of constraints,
                        //    we proceed to Newton method
                        // 2. In case there was activated at least one constraint with ActiveSet[I]<0,
                        //    we proceed to Phase 1 and re-evaluate active set.
                        // 3. Otherwise (activation of the constraints with ActiveSet[I]=0)
                        //    we try Phase 2 one more time.
                        //
                        if (actstatus < 0)
                        {

                            //
                            // Step without activation, proceed to Newton
                            //
                            nextaction = 1;
                            break;
                        }
                        if (actstatus == 0)
                        {

                            //
                            // No new constraints added during last activation - only
                            // ones which were at the boundary (ActiveSet[I]=0), but
                            // inactive due to numerical noise.
                            //
                            // Now, these constraints are added to the active set, and
                            // we try to perform steepest descent (Phase 2) one more time.
                            //
                            continue;
                        }
                        else
                        {

                            //
                            // Last step activated at least one significantly new
                            // constraint (ActiveSet[I]<0), we have to re-evaluate
                            // active set (Phase 1).
                            //
                            nextaction = -1;
                            break;
                        }
                    }
                    if (nextaction < 0)
                    {
                        continue;
                    }
                    if (nextaction == 0)
                    {
                        break;
                    }

                    //
                    // Phase 3: fast equality-constrained solver
                    //
                    // NOTE: this solver uses Augmented Lagrangian algorithm to solve
                    //       equality-constrained subproblems. This algorithm may
                    //       perform steps which increase function values instead of
                    //       decreasing it (in hard cases, like overconstrained problems).
                    //
                    //       Such non-monononic steps may create a loop, when Augmented
                    //       Lagrangian algorithm performs uphill step, and steepest
                    //       descent algorithm (Phase 2) performs downhill step in the
                    //       opposite direction.
                    //
                    //       In order to prevent iterations to continue forever we
                    //       count iterations when AL algorithm increased function
                    //       value instead of decreasing it. When number of such "bad"
                    //       iterations will increase beyong MaxBadNewtonIts, we will
                    //       terminate algorithm.
                    //
                    fprev = ModelValue(state.a, state.b, state.sas.xc, n, ref state.tmp0);
                    while (true)
                    {

                        //
                        // Calculate optimum subject to presently active constraints
                        //
                        state.repncholesky = state.repncholesky + 1;
                        state.debugphase3flops = state.debugphase3flops + Math.Pow(n, 3) / 3;
                        if (!ConstrainedOptimum(state, state.a, state.anorm, state.b, ref state.xn, ref state.tmp0, ref state.tmpb, ref state.tmp1))
                        {
                            state.repterminationtype = -5;
                            ActiveSet.StopOptimization(state.sas);
                            return;
                        }

                        //
                        // Add constraints.
                        // If no constraints was added, accept candidate point XN and move to next phase.
                        //
                        if (BoundedStepAndActivation(state, state.xn, ref state.tmp0) < 0)
                        {
                            break;
                        }
                    }
                    fcur = ModelValue(state.a, state.b, state.sas.xc, n, ref state.tmp0);
                    if (fcur >= fprev)
                    {
                        badnewtonits = badnewtonits + 1;
                    }
                    if (badnewtonits >= MaxBadNewtonIterations)
                    {

                        //
                        // Algorithm found solution, but keeps iterating because Newton
                        // algorithm performs uphill steps (noise in the Augmented Lagrangian
                        // algorithm). We terminate algorithm; it is considered normal
                        // termination.
                        //
                        break;
                    }
                }
                ActiveSet.StopOptimization(state.sas);

                //
                // Post-process: add XOrigin to XC
                //
                for (i = 0; i <= n - 1; i++)
                {
                    if (state.havebndl[i] && state.sas.xc[i] == state.workbndl[i])
                    {
                        state.xs[i] = state.bndl[i];
                        continue;
                    }
                    if (state.havebndu[i] && state.sas.xc[i] == state.workbndu[i])
                    {
                        state.xs[i] = state.bndu[i];
                        continue;
                    }
                    state.xs[i] = Utilities.BoundValue(state.sas.xc[i] + state.xorigin[i], state.bndl[i], state.bndu[i]);
                }
                return;
            }

            //
            // BLEIC solver
            //
            if (state.algokind == 2)
            {
                Utilities.Assert(state.akind == 0 || state.akind == 1, "MinQPOptimize: unexpected AKind");
                state.tmpi.SetLengthAtLeast(state.nec + state.nic);
                state.tmp0.SetLengthAtLeast(n);
                state.tmp1.SetLengthAtLeast(n);
                for (i = 0; i <= state.nec - 1; i++)
                {
                    state.tmpi[i] = 0;
                }
                for (i = 0; i <= state.nic - 1; i++)
                {
                    state.tmpi[state.nec + i] = -1;
                }
                MinBleic.SetLinearConstraints(state.solver, state.cleic, state.tmpi, state.nec + state.nic);
                MinBleic.SetBoundaryConstraints(state.solver, state.bndl, state.bndu);
                MinBleic.SetLineSearchReports(state.solver, true);
                MinBleic.SetStopConditions(state.solver, MathExtensions.MinRealNumber, 0.0, 0.0, state.bleicmaxits);
                MinBleic.SetScalingCoefficients(state.solver, state.s);
                MinBleic.SetScaleBasedDiagonalPreConditioning(state.solver);
                MinBleic.RestartFrom(state.solver, state.xs);
                state.repterminationtype = 0;
                while (MinBleic.Iteration(state.solver))
                {

                    //
                    // Line search started
                    //
                    if (state.solver.lsstart)
                    {

                        //
                        // Iteration counters:
                        // * inner iterations count is increased on every line search
                        // * outer iterations count is increased only at steepest descent line search
                        //
                        Utilities.Increment(ref state.repinneriterationscount);
                        if (!state.solver.lbfgssearch)
                        {
                            Utilities.Increment(ref state.repouteriterationscount);
                        }

                        //
                        // Build quadratic model of F along descent direction:
                        //     F(x+alpha*d) = D2*alpha^2 + D1*alpha + D0
                        //
                        d0 = state.solver.f;
                        d1 = 0.0;
                        for (i_ = 0; i_ <= n - 1; i_++)
                        {
                            d1 += state.solver.d[i_] * state.solver.g[i_];
                        }
                        d2 = 0;
                        if (state.akind == 0)
                        {
                            d2 = ConvexQuadraticModel.XtAdX2(state.a, state.solver.d);
                        }
                        if (state.akind == 1)
                        {
                            SparseMatrix.SymetricMatrixVectorProduct(state.sparsea, state.sparseaupper, state.solver.d, ref state.tmp0);
                            d2 = 0.0;
                            for (i = 0; i <= n - 1; i++)
                            {
                                d2 = d2 + state.solver.d[i] * state.tmp0[i];
                            }
                            d2 = 0.5 * d2;
                        }

                        //
                        // Suggest new step
                        //
                        if (d1 < 0 && d2 > 0)
                        {
                            state.solver.stp = Utilities.SafeMin(-d1, 2 * d2, state.solver.curstpmax);
                        }

                        //
                        // This line search may be started from steepest descent
                        // stage (stage 2) or from L-BFGS stage (stage 3) of the
                        // BLEIC algorithm. Depending on stage type, different
                        // checks are performed.
                        //
                        // Say, L-BFGS stage is an equality-constrained refinement
                        // stage of BLEIC. This stage refines current iterate
                        // under "frozen" equality constraints. We can terminate
                        // iterations at this stage only when we encounter
                        // unconstrained direction of negative curvature. In all
                        // other cases (say, when constrained gradient is zero)
                        // we should not terminate algorithm because everything may
                        // change after de-activating presently active constraints.
                        //
                        // At steepest descent stage of BLEIC we can terminate algorithm
                        // because it found minimum (steepest descent step is zero
                        // or too short). We also perform check for direction of
                        // negative curvature.
                        //
                        if ((d2 < 0 || (d2 == 0 && d1 < 0)) && !state.solver.boundedstep)
                        {

                            //
                            // Function is unbounded from below:
                            // * function will decrease along D, i.e. either:
                            //   * D2<0
                            //   * D2=0 and D1<0
                            // * step is unconstrained
                            //
                            // If these conditions are true, we abnormally terminate QP
                            // algorithm with return code -4 (we can do so at any stage
                            // of BLEIC - whether it is L-BFGS or steepest descent one).
                            //
                            state.repterminationtype = -4;
                            for (i = 0; i <= n - 1; i++)
                            {
                                state.xs[i] = state.solver.x[i];
                            }
                            break;
                        }
                        if (!state.solver.lbfgssearch && d2 >= 0)
                        {

                            //
                            // Tests for "normal" convergence.
                            //
                            // These tests are performed only at "steepest descent" stage
                            // of the BLEIC algorithm, and only when function is non-concave
                            // (D2>=0) along direction D.
                            //
                            // NOTE: we do not test iteration count (MaxIts) here, because
                            //       this stopping condition is tested by BLEIC itself.
                            //
                            if (d1 >= 0)
                            {

                                //
                                // "Emergency" stopping condition: D is non-descent direction.
                                // Sometimes it is possible because of numerical noise in the
                                // target function.
                                //
                                state.repterminationtype = 4;
                                for (i = 0; i <= n - 1; i++)
                                {
                                    state.xs[i] = state.solver.x[i];
                                }
                                break;
                            }
                            if (d2 > 0)
                            {

                                //
                                // Stopping condition #4 - gradient norm is small:
                                //
                                // 1. rescale State.Solver.D and State.Solver.G according to
                                //    current scaling, store results to Tmp0 and Tmp1.
                                // 2. Normalize Tmp0 (scaled direction vector).
                                // 3. compute directional derivative (in scaled variables),
                                //    which is equal to DOTPRODUCT(Tmp0,Tmp1).
                                //
                                v = 0;
                                for (i = 0; i <= n - 1; i++)
                                {
                                    state.tmp0[i] = state.solver.d[i] / state.s[i];
                                    state.tmp1[i] = state.solver.g[i] * state.s[i];
                                    v = v + MathExtensions.Squared(state.tmp0[i]);
                                }
                                Utilities.Assert(v > 0, "MinQPOptimize: inernal errror (scaled direction is zero)");
                                v = 1 / Math.Sqrt(v);
                                for (i_ = 0; i_ <= n - 1; i_++)
                                {
                                    state.tmp0[i_] = v * state.tmp0[i_];
                                }
                                v = 0.0;
                                for (i_ = 0; i_ <= n - 1; i_++)
                                {
                                    v += state.tmp0[i_] * state.tmp1[i_];
                                }
                                if (Math.Abs(v) <= state.bleicepsg)
                                {
                                    state.repterminationtype = 4;
                                    for (i = 0; i <= n - 1; i++)
                                    {
                                        state.xs[i] = state.solver.x[i];
                                    }
                                    break;
                                }

                                //
                                // Stopping condition #1 - relative function improvement is small:
                                //
                                // 1. calculate steepest descent step:   V = -D1/(2*D2)
                                // 2. calculate function change:         V1= D2*V^2 + D1*V
                                // 3. stop if function change is small enough
                                //
                                v = -(d1 / (2 * d2));
                                v1 = d2 * v * v + d1 * v;
                                if (Math.Abs(v1) <= state.bleicepsf * Math.Max(d0, 1.0))
                                {
                                    state.repterminationtype = 1;
                                    for (i = 0; i <= n - 1; i++)
                                    {
                                        state.xs[i] = state.solver.x[i];
                                    }
                                    break;
                                }

                                //
                                // Stopping condition #2 - scaled step is small:
                                //
                                // 1. calculate step multiplier V0 (step itself is D*V0)
                                // 2. calculate scaled step length V
                                // 3. stop if step is small enough
                                //
                                v0 = -(d1 / (2 * d2));
                                v = 0;
                                for (i = 0; i <= n - 1; i++)
                                {
                                    v = v + MathExtensions.Squared(v0 * state.solver.d[i] / state.s[i]);
                                }
                                if (Math.Sqrt(v) <= state.bleicepsx)
                                {
                                    state.repterminationtype = 2;
                                    for (i = 0; i <= n - 1; i++)
                                    {
                                        state.xs[i] = state.solver.x[i];
                                    }
                                    break;
                                }
                            }
                        }
                    }

                    //
                    // Gradient evaluation
                    //
                    if (state.solver.needfg)
                    {
                        for (i = 0; i <= n - 1; i++)
                        {
                            state.tmp0[i] = state.solver.x[i] - state.xorigin[i];
                        }
                        if (state.akind == 0)
                        {
                            ConvexQuadraticModel.Adx(state.a, state.tmp0, ref state.tmp1);
                        }
                        if (state.akind == 1)
                        {
                            SparseMatrix.SymetricMatrixVectorProduct(state.sparsea, state.sparseaupper, state.tmp0, ref state.tmp1);
                        }
                        v0 = 0.0;
                        for (i_ = 0; i_ <= n - 1; i_++)
                        {
                            v0 += state.tmp0[i_] * state.tmp1[i_];
                        }
                        v1 = 0.0;
                        for (i_ = 0; i_ <= n - 1; i_++)
                        {
                            v1 += state.tmp0[i_] * state.b[i_];
                        }
                        state.solver.f = 0.5 * v0 + v1;
                        for (i_ = 0; i_ <= n - 1; i_++)
                        {
                            state.solver.g[i_] = state.tmp1[i_];
                        }
                        for (i_ = 0; i_ <= n - 1; i_++)
                        {
                            state.solver.g[i_] = state.solver.g[i_] + state.b[i_];
                        }
                    }
                }
                if (state.repterminationtype == 0)
                {

                    //
                    // BLEIC optimizer was terminated by one of its inner stopping
                    // conditions. Usually it is iteration counter (if such
                    // stopping condition was specified by user).
                    //
                    MinBleic.Results(state.solver, ref state.xs, state.solverrep);
                    state.repterminationtype = state.solverrep.terminationtype;
                }
                else
                {

                    //
                    // BLEIC optimizer was terminated in "emergency" mode by QP
                    // solver.
                    //
                    // NOTE: such termination is "emergency" only when viewed from
                    //       BLEIC's position. QP solver sees such termination as
                    //       routine one, triggered by QP's stopping criteria.
                    //
                    MinBleic.EmergencyTermination(state.solver);
                }
                return;
            }
        }




        /*************************************************************************
        QP results

        Buffered implementation of MinQPResults() which uses pre-allocated  buffer
        to store X[]. If buffer size is  too  small,  it  resizes  buffer.  It  is
        intended to be used in the inner cycles of performance critical algorithms
        where array reallocation penalty is too large to be ignored.

          -- ALGLIB --
             Copyright 11.01.2011 by Bochkanov Sergey
        *************************************************************************/
        public static void ResultsBuffered(State state,
            ref FluxList<double> x,
            Report rep)
        {
            int i = 0;

            if (x.Count < state.n)
            {
                x.Resize(state.n);
            }
            for (i = 0; i <= state.n - 1; i++)
            {
                x[i] = state.xs[i];
            }
            rep.inneriterationscount = state.repinneriterationscount;
            rep.outeriterationscount = state.repouteriterationscount;
            rep.nmv = state.repnmv;
            rep.ncholesky = state.repncholesky;
            rep.terminationtype = state.repterminationtype;
        }


        /*************************************************************************
        Fast version of MinQPSetLinearTerm(), which doesn't check its arguments.
        For internal use only.

          -- ALGLIB --
             Copyright 11.01.2011 by Bochkanov Sergey
        *************************************************************************/
        public static void SetLinearTerm(State state,
            FluxList<double> b)
        {
            int i = 0;

            for (i = 0; i <= state.n - 1; i++)
            {
                state.b[i] = b[i];
            }
        }


        /*************************************************************************
        Fast version of MinQPSetQuadraticTerm(), which doesn't check its arguments.

        It accepts additional parameter - shift S, which allows to "shift"  matrix
        A by adding s*I to A. S must be positive (although it is not checked).

        For internal use only.

          -- ALGLIB --
             Copyright 11.01.2011 by Bochkanov Sergey
        *************************************************************************/
        public static void SetQuadraticTerm(State state,
            Matrix<double> a,
            bool isupper,
            double s)
        {
            int i = 0;
            int j = 0;
            int n = 0;

            n = state.n;
            state.akind = 0;
            ConvexQuadraticModel.ChangeMainQuadraticTerm(state.a, a, isupper, 1.0);
            if (s > 0)
            {
                state.tmp0.SetLengthAtLeast(n);
                for (i = 0; i <= n - 1; i++)
                {
                    state.tmp0[i] = a[i, i] + s;
                }
                ConvexQuadraticModel.RewriteDenseDiagonal(state.a, state.tmp0);
            }

            //
            // Estimate norm of A
            // (it will be used later in the quadratic penalty function)
            //
            state.anorm = 0;
            for (i = 0; i <= n - 1; i++)
            {
                if (isupper)
                {
                    for (j = i; j <= n - 1; j++)
                    {
                        state.anorm = Math.Max(state.anorm, Math.Abs(a[i, j]));
                    }
                }
                else
                {
                    for (j = 0; j <= i; j++)
                    {
                        state.anorm = Math.Max(state.anorm, Math.Abs(a[i, j]));
                    }
                }
            }
            state.anorm = state.anorm * n;
        }


        /*************************************************************************
        Internal function which allows to rewrite diagonal of quadratic term.
        For internal use only.

        This function can be used only when you have dense A and already made
        MinQPSetQuadraticTerm(Fast) call.

          -- ALGLIB --
             Copyright 16.01.2011 by Bochkanov Sergey
        *************************************************************************/
        public static void RewriteDiagonal(State state,
            FluxList<double> s)
        {
            ConvexQuadraticModel.RewriteDenseDiagonal(state.a, s);
        }


        /*************************************************************************
        Fast version of MinQPSetStartingPoint(), which doesn't check its arguments.
        For internal use only.

          -- ALGLIB --
             Copyright 11.01.2011 by Bochkanov Sergey
        *************************************************************************/
        public static void SetStartingPoint(State state,
            FluxList<double> x)
        {
            int n = 0;
            int i = 0;

            n = state.n;
            for (i = 0; i <= n - 1; i++)
            {
                state.startx[i] = x[i];
            }
            state.havex = true;
        }


        /*************************************************************************
        Fast version of MinQPSetOrigin(), which doesn't check its arguments.
        For internal use only.

          -- ALGLIB --
             Copyright 11.01.2011 by Bochkanov Sergey
        *************************************************************************/
        public static void SetOrigin(State state,
            FluxList<double> xorigin)
        {
            int n = 0;
            int i = 0;

            n = state.n;
            for (i = 0; i <= n - 1; i++)
            {
                state.xorigin[i] = xorigin[i];
            }
        }


        /*************************************************************************
        Having feasible current point XC and possibly infeasible candidate   point
        XN,  this  function  performs  longest  step  from  XC to XN which retains
        feasibility. In case XN is found to be infeasible, at least one constraint
        is activated.

        For example, if we have:
          XC=0.5
          XN=1.2
          x>=0, x<=1
        then this function will move us to X=1.0 and activate constraint "x<=1".

        INPUT PARAMETERS:
            State   -   MinQP state.
            XC      -   current point, must be feasible with respect to
                        all constraints
            XN      -   candidate point, can be infeasible with respect to some
                        constraints. Must be located in the subspace of current
                        active set, i.e. it is feasible with respect to already
                        active constraints.
            Buf     -   temporary buffer, automatically resized if needed

        OUTPUT PARAMETERS:
            State   -   this function changes following fields of State:
                        * State.ActiveSet
                        * State.ActiveC     -   active linear constraints
            XC      -   new position

        RESULT:
            >0, in case at least one inactive non-candidate constraint was activated
            =0, in case only "candidate" constraints were activated
            <0, in case no constraints were activated by the step


          -- ALGLIB --
             Copyright 29.02.2012 by Bochkanov Sergey
        *************************************************************************/
        private static int BoundedStepAndActivation(State state,
            FluxList<double> xn,
            ref FluxList<double> buf)
        {
            int result = 0;
            int n = 0;
            double stpmax = 0;
            int cidx = 0;
            double cval = 0;
            bool needact = new bool();
            double v = 0;
            int i = 0;

            n = state.n;
            buf.SetLengthAtLeast(n);
            for (i = 0; i <= n - 1; i++)
            {
                buf[i] = xn[i];
            }
            for (i = 0; i <= n - 1; i++)
            {
                buf[i] = buf[i] - state.sas.xc[i];
            }
            ActiveSet.ExploreDirection(state.sas, buf, ref stpmax, ref cidx, ref cval);
            needact = stpmax <= 1;
            v = Math.Min(stpmax, 1.0);
            for (i = 0; i <= n - 1; i++)
            {
                buf[i] = v * buf[i];
            }
            for (i = 0; i <= n - 1; i++)
            {
                buf[i] = buf[i] + state.sas.xc[i];
            }
            result = ActiveSet.MoveTo(state.sas, buf, needact, cidx, cval);
            return result;
        }


        /*************************************************************************
        Model value: f = 0.5*x'*A*x + b'*x

        INPUT PARAMETERS:
            A       -   convex quadratic model; only main quadratic term is used,
                        other parts of the model (D/Q/linear term) are ignored.
                        This function does not modify model state.
            B       -   right part
            XC      -   evaluation point
            Tmp     -   temporary buffer, automatically resized if needed

          -- ALGLIB --
             Copyright 20.06.2012 by Bochkanov Sergey
        *************************************************************************/
        private static double ModelValue(ConvexQuadraticModel a,
            FluxList<double> b,
            FluxList<double> xc,
            int n,
            ref FluxList<double> tmp)
        {
            double result = 0;
            double v0 = 0;
            double v1 = 0;
            int i = 0;

            tmp.SetLengthAtLeast(n);
            ConvexQuadraticModel.Adx(a, xc, ref tmp);
            v0 = 0.0;
            for (i = 0; i <= n - 1; i++)
            {
                v0 += xc[i] * tmp[i];
            }
            v1 = 0.0;
            for (i = 0; i <= n - 1; i++)
            {
                v1 += xc[i] * b[i];
            }
            result = 0.5 * v0 + v1;
            return result;
        }


        /*************************************************************************
        Optimum of A subject to:
        a) active boundary constraints (given by ActiveSet[] and corresponding
           elements of XC)
        b) active linear constraints (given by C, R, LagrangeC)

        INPUT PARAMETERS:
            A       -   main quadratic term of the model;
                        although structure may  store  linear  and  rank-K  terms,
                        these terms are ignored and rewritten  by  this  function.
            ANorm   -   estimate of ||A|| (2-norm is used)
            B       -   array[N], linear term of the model
            XN      -   possibly preallocated buffer
            Tmp     -   temporary buffer (automatically resized)
            Tmp1    -   temporary buffer (automatically resized)

        OUTPUT PARAMETERS:
            A       -   modified quadratic model (this function changes rank-K
                        term and linear term of the model)
            LagrangeC-  current estimate of the Lagrange coefficients
            XN      -   solution

        RESULT:
            True on success, False on failure (non-SPD model)

          -- ALGLIB --
             Copyright 20.06.2012 by Bochkanov Sergey
        *************************************************************************/
        private static bool ConstrainedOptimum(State state,
            ConvexQuadraticModel a,
            double anorm,
            FluxList<double> b,
            ref FluxList<double> xn,
            ref FluxList<double> tmp,
            ref FluxList<bool> tmpb,
            ref FluxList<double> lagrangec)
        {
            bool result = new bool();
            int itidx = 0;
            int i = 0;
            double v = 0;
            double feaserrold = 0;
            double feaserrnew = 0;
            double theta = 0;
            int n = 0;

            n = state.n;

            //
            // Rebuild basis accroding to current active set.
            // We call SASRebuildBasis() to make sure that fields of SAS
            // store up to date values.
            //
            ActiveSet.RebuildBasis(state.sas);

            //
            // Allocate temporaries.
            //
            tmp.SetLengthAtLeast(Math.Max(n, state.sas.basissize));
            tmpb.SetLengthAtLeast(n);
            lagrangec.SetLengthAtLeast(state.sas.basissize);

            //
            // Prepare model
            //
            for (i = 0; i <= state.sas.basissize - 1; i++)
            {
                tmp[i] = state.sas.pbasis[i, n];
            }
            theta = 100.0 * anorm;
            for (i = 0; i <= n - 1; i++)
            {
                if (state.sas.activeset[i] > 0)
                {
                    tmpb[i] = true;
                }
                else
                {
                    tmpb[i] = false;
                }
            }
            ConvexQuadraticModel.ChangeActiveSet(a, state.sas.xc, tmpb);
            ConvexQuadraticModel.SetQ(a, state.sas.pbasis, tmp, state.sas.basissize, theta);

            //
            // Iterate until optimal values of Lagrange multipliers are found
            //
            for (i = 0; i <= state.sas.basissize - 1; i++)
            {
                lagrangec[i] = 0;
            }
            feaserrnew = MathExtensions.MaxRealNumber;
            result = true;
            for (itidx = 1; itidx <= MaxLaGrangeIterations; itidx++)
            {

                //
                // Generate right part B using linear term and current
                // estimate of the Lagrange multipliers.
                //
                int j = 0;
                for (j = 0; j <= n - 1; j++)
                {
                    tmp[j] = b[j];
                }
                for (i = 0; i <= state.sas.basissize - 1; i++)
                {
                    v = lagrangec[i];
                    for (j = 0; j <= n - 1; j++)
                    {
                        tmp[j] = tmp[j] - v * state.sas.pbasis[i, j];
                    }
                }
                ConvexQuadraticModel.ChangeLinearTerm(a, tmp);

                //
                // Solve
                //
                result = ConvexQuadraticModel.FindOptimumConstrained(a, ref xn);
                if (!result)
                {
                    return result;
                }

                //
                // Compare feasibility errors.
                // Terminate if error decreased too slowly.
                //
                feaserrold = feaserrnew;
                feaserrnew = 0;
                for (i = 0; i <= state.sas.basissize - 1; i++)
                {
                    v = 0.0;
                    for (j = 0; j <= n - 1; j++)
                    {
                        v += state.sas.pbasis[i, j] * xn[j];
                    }
                    feaserrnew = feaserrnew + MathExtensions.Squared(v - state.sas.pbasis[i, n]);
                }
                feaserrnew = Math.Sqrt(feaserrnew);
                if (feaserrnew >= 0.2 * feaserrold)
                {
                    break;
                }

                //
                // Update Lagrange multipliers
                //
                for (i = 0; i <= state.sas.basissize - 1; i++)
                {
                    v = 0.0;
                    for (j = 0; j <= n - 1; j++)
                    {
                        v += state.sas.pbasis[i, j] * xn[j];
                    }
                    lagrangec[i] = lagrangec[i] - theta * (v - state.sas.pbasis[i, n]);
                }
            }
            return result;
        }


    }
}
