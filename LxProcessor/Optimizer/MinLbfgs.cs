﻿using System;
using System.Collections.Generic;
using LxCommon.DataStructures;
using LxCommon.Utilities;
using LxProcessor.Common;

namespace LxProcessor.Optimizer
{
    public class MinLbfgs
    {
        public class MinLbfgsState
        {
            public int n;
            public int m;
            public double epsg;
            public double epsf;
            public double epsx;
            public int maxits;
            public bool xrep;
            public double stpmax;
            public FluxList<double> s;
            public double diffstep;
            public int nfev;
            public int mcstage;
            public int k;
            public int q;
            public int p;
            public FluxList<double> rho;
            public Matrix<double> yk;
            public Matrix<double> sk;
            public FluxList<double> theta;
            public FluxList<double> d;
            public double stp;
            public FluxList<double> work;
            public double fold;
            public double trimthreshold;
            public int prectype;
            public double gammak;
            public Matrix<double> denseh;
            public FluxList<double> diagh;
            public double fbase;
            public double fm2;
            public double fm1;
            public double fp1;
            public double fp2;
            public FluxList<double> autobuf;
            public FluxList<double> x;
            public double f;
            public FluxList<double> g;
            public bool needf;
            public bool needfg;
            public bool xupdated;
            public double teststep;
            public ReverseCommunicationStructure rstate;
            public int repiterationscount;
            public int repnfev;
            public int repvaridx;
            public int repterminationtype;
            public LinMin.State lstate;
            public MinLbfgsState()
            {
                s = new FluxList<double>();
                rho = new FluxList<double>();
                yk = new Matrix<double>(0, 0);
                sk = new Matrix<double>(0, 0);
                theta = new FluxList<double>();
                d = new FluxList<double>();
                work = new FluxList<double>();
                denseh = new Matrix<double>(0, 0);
                diagh = new FluxList<double>();
                autobuf = new FluxList<double>();
                x = new FluxList<double>();
                g = new FluxList<double>();
                rstate = new ReverseCommunicationStructure();
                lstate = new LinMin.State();
            }


            public void CleanForReuse()
            {
                n = default(int);
                m = default(int);
                epsg = default(double);
                epsf = default(double);
                epsx = default(double);
                maxits = default(int);
                xrep = default(bool);
                stpmax = default(double);
                s.Clear();
                diffstep = default(double);
                nfev = default(int);
                mcstage = default(int);
                k = default(int);
                q = default(int);
                p = default(int);
                rho.Clear();;
                yk.Clear();;
                sk.Clear();;
                theta.Clear();;
                d.Clear();;
                stp = default(double);
                work.Clear();;
                fold = default(double);
                trimthreshold = default(double);
                prectype = default(int);
                gammak = default(double);
                denseh.Clear();;
                diagh.Clear();;
                fbase = default(double);
                fm2 = default(double);
                fm1 = default(double);
                fp1 = default(double);
                fp2 = default(double);
                autobuf.Clear();;
                x.Clear();;
                f = default(double);
                g.Clear();;
                needf = default(bool);
                needfg = default(bool);
                xupdated = default(bool);
                teststep = default(double);
                rstate.CleanForReuse();
                repiterationscount = default(int);
                repnfev = default(int);
                repvaridx = default(int);
                repterminationtype = default(int);
                lstate.CleanForReuse();
            }

        };


        public class MinLbfgsReport
        {
            public int iterationscount;
            public int nfev;
            public int varidx;
            public int terminationtype;
            public MinLbfgsReport()
            {
           
            }

            public void CleanForReuse()
            {
                iterationscount = default(int);
                nfev = default(int);
                varidx = default(int);
                terminationtype = default(int);
            }
        };




        //public const double gtol = 0.4;


        /*************************************************************************
                LIMITED MEMORY BFGS METHOD FOR LARGE SCALE OPTIMIZATION

        DESCRIPTION:
        The subroutine minimizes function F(x) of N arguments by  using  a  quasi-
        Newton method (LBFGS scheme) which is optimized to use  a  minimum  amount
        of memory.
        The subroutine generates the approximation of an inverse Hessian matrix by
        using information about the last M steps of the algorithm  (instead of N).
        It lessens a required amount of memory from a value  of  order  N^2  to  a
        value of order 2*N*M.


        REQUIREMENTS:
        Algorithm will request following information during its operation:
        * function value F and its gradient G (simultaneously) at given point X


        USAGE:
        1. User initializes algorithm state with MinLBFGSCreate() call
        2. User tunes solver parameters with MinLBFGSSetCond() MinLBFGSSetStpMax()
           and other functions
        3. User calls MinLBFGSOptimize() function which takes algorithm  state and
           pointer (delegate, etc.) to callback function which calculates F/G.
        4. User calls MinLBFGSResults() to get solution
        5. Optionally user may call MinLBFGSRestartFrom() to solve another problem
           with same N/M but another starting point and/or another function.
           MinLBFGSRestartFrom() allows to reuse already initialized structure.


        INPUT PARAMETERS:
            N       -   problem dimension. N>0
            M       -   number of corrections in the BFGS scheme of Hessian
                        approximation update. Recommended value:  3<=M<=7. The smaller
                        value causes worse convergence, the bigger will  not  cause  a
                        considerably better convergence, but will cause a fall in  the
                        performance. M<=N.
            X       -   initial solution approximation, array[0..N-1].


        OUTPUT PARAMETERS:
            State   -   structure which stores algorithm state
            

        NOTES:
        1. you may tune stopping conditions with MinLBFGSSetCond() function
        2. if target function contains exp() or other fast growing functions,  and
           optimization algorithm makes too large steps which leads  to  overflow,
           use MinLBFGSSetStpMax() function to bound algorithm's  steps.  However,
           L-BFGS rarely needs such a tuning.


          -- ALGLIB --
             Copyright 02.04.2010 by Bochkanov Sergey
        *************************************************************************/
        public static void MinLbfgsCreate(int n,
            int m,
            FluxList<double> x,
            MinLbfgsState state)
        {
            Utilities.Assert(n >= 1, "MinLBFGSCreate: N<1!");
            Utilities.Assert(m >= 1, "MinLBFGSCreate: M<1");
            Utilities.Assert(m <= n, "MinLBFGSCreate: M>N");
            Utilities.Assert(x.Count >= n, "MinLBFGSCreate: Length(X)<N!");
            Utilities.Assert(Utilities.IsFiniteVector(x, n), "MinLBFGSCreate: X contains infinite or NaN values!");
            MinLbfgsCreateExtended(n, m, x, 0, 0.0, state);
        }





        /*************************************************************************
        This function sets stopping conditions for L-BFGS optimization algorithm.

        INPUT PARAMETERS:
            State   -   structure which stores algorithm state
            EpsG    -   >=0
                        The  subroutine  finishes  its  work   if   the  condition
                        |v|<EpsG is satisfied, where:
                        * |.| means Euclidian norm
                        * v - scaled gradient vector, v[i]=g[i]*s[i]
                        * g - gradient
                        * s - scaling coefficients set by MinLBFGSSetScale()
            EpsF    -   >=0
                        The  subroutine  finishes  its work if on k+1-th iteration
                        the  condition  |F(k+1)-F(k)|<=EpsF*max{|F(k)|,|F(k+1)|,1}
                        is satisfied.
            EpsX    -   >=0
                        The subroutine finishes its work if  on  k+1-th  iteration
                        the condition |v|<=EpsX is fulfilled, where:
                        * |.| means Euclidian norm
                        * v - scaled step vector, v[i]=dx[i]/s[i]
                        * dx - ste pvector, dx=X(k+1)-X(k)
                        * s - scaling coefficients set by MinLBFGSSetScale()
            MaxIts  -   maximum number of iterations. If MaxIts=0, the  number  of
                        iterations is unlimited.

        Passing EpsG=0, EpsF=0, EpsX=0 and MaxIts=0 (simultaneously) will lead to
        automatic stopping criterion selection (small EpsX).

          -- ALGLIB --
             Copyright 02.04.2010 by Bochkanov Sergey
        *************************************************************************/
        public static void MinLbfgsSetConditions(MinLbfgsState state,
            double epsg,
            double epsf,
            double epsx,
            int maxits)
        {
            Utilities.Assert(MathExtensions.IsFinite(epsg), "MinLBFGSSetCond: EpsG is not finite number!");
            Utilities.Assert(epsg >= 0, "MinLBFGSSetCond: negative EpsG!");
            Utilities.Assert(MathExtensions.IsFinite(epsf), "MinLBFGSSetCond: EpsF is not finite number!");
            Utilities.Assert(epsf >= 0, "MinLBFGSSetCond: negative EpsF!");
            Utilities.Assert(MathExtensions.IsFinite(epsx), "MinLBFGSSetCond: EpsX is not finite number!");
            Utilities.Assert(epsx >= 0, "MinLBFGSSetCond: negative EpsX!");
            Utilities.Assert(maxits >= 0, "MinLBFGSSetCond: negative MaxIts!");
            if (((epsg == 0 && epsf == 0) && epsx == 0) && maxits == 0)
            {
                epsx = 1.0E-6;
            }
            state.epsg = epsg;
            state.epsf = epsf;
            state.epsx = epsx;
            state.maxits = maxits;
        }


        /*************************************************************************
        This function turns on/off reporting.

        INPUT PARAMETERS:
            State   -   structure which stores algorithm state
            NeedXRep-   whether iteration reports are needed or not

        If NeedXRep is True, algorithm will call rep() callback function if  it is
        provided to MinLBFGSOptimize().


          -- ALGLIB --
             Copyright 02.04.2010 by Bochkanov Sergey
        *************************************************************************/
        private static void MinLbfgsSetReporting(MinLbfgsState state,
            bool needxrep)
        {
            state.xrep = needxrep;
        }


        /*************************************************************************
        This function sets maximum step length

        INPUT PARAMETERS:
            State   -   structure which stores algorithm state
            StpMax  -   maximum step length, >=0. Set StpMax to 0.0 (default),  if
                        you don't want to limit step length.

        Use this subroutine when you optimize target function which contains exp()
        or  other  fast  growing  functions,  and optimization algorithm makes too
        large  steps  which  leads  to overflow. This function allows us to reject
        steps  that  are  too  large  (and  therefore  expose  us  to the possible
        overflow) without actually calculating function value at the x+stp*d.

          -- ALGLIB --
             Copyright 02.04.2010 by Bochkanov Sergey
        *************************************************************************/
        private static void MinLbfgsSetStepMax(MinLbfgsState state,
            double stpmax)
        {
            Utilities.Assert(MathExtensions.IsFinite(stpmax), "MinLBFGSSetStpMax: StpMax is not finite!");
            Utilities.Assert(stpmax >= 0, "MinLBFGSSetStpMax: StpMax<0!");
            state.stpmax = stpmax;
        }




        /*************************************************************************
        Extended subroutine for internal use only.

        Accepts additional parameters:

            Flags - additional settings:
                    * Flags = 0     means no additional settings
                    * Flags = 1     "do not allocate memory". used when solving
                                    a many subsequent tasks with  same N/M  values.
                                    First  call MUST  be without this flag bit set,
                                    subsequent  calls   of   MinLBFGS   with   same
                                    MinLBFGSState structure can set Flags to 1.
            DiffStep - numerical differentiation step

          -- ALGLIB --
             Copyright 02.04.2010 by Bochkanov Sergey
        *************************************************************************/
        private static void MinLbfgsCreateExtended(int n,
            int m,
            FluxList<double> x,
            int flags,
            double diffstep,
            MinLbfgsState state)
        {
            bool allocatemem = new bool();
            int i = 0;

            Utilities.Assert(n >= 1, "MinLBFGS: N too small!");
            Utilities.Assert(m >= 1, "MinLBFGS: M too small!");
            Utilities.Assert(m <= n, "MinLBFGS: M too large!");

            //
            // Initialize
            //
            state.teststep = 0;
            state.diffstep = diffstep;
            state.n = n;
            state.m = m;
            allocatemem = flags % 2 == 0;
            flags = flags / 2;
            if (allocatemem)
            {
                state.rho.Resize(m);
                state.theta.Resize(m);
                state.yk.Resize(m, n);
                state.sk.Resize(m, n);
                state.d.Resize(n);
                state.x.Resize(n);
                state.s.Resize(n);
                state.g.Resize(n);
                state.work.Resize(n);
            }
            MinLbfgsSetConditions(state, 0, 0, 0, 0);
            MinLbfgsSetReporting(state, false);
            MinLbfgsSetStepMax(state, 0);
            MinLbfgsRestartFrom(state, x);
            for (i = 0; i <= n - 1; i++)
            {
                state.s[i] = 1.0;
            }
            state.prectype = 0;
        }




        /*************************************************************************
        This  subroutine restarts LBFGS algorithm from new point. All optimization
        parameters are left unchanged.

        This  function  allows  to  solve multiple  optimization  problems  (which
        must have same number of dimensions) without object reallocation penalty.

        INPUT PARAMETERS:
            State   -   structure used to store algorithm state
            X       -   new starting point.

          -- ALGLIB --
             Copyright 30.07.2010 by Bochkanov Sergey
        *************************************************************************/
        private static void MinLbfgsRestartFrom(MinLbfgsState state,
            FluxList<double> x)
        {
            int i_ = 0;

            Utilities.Assert(x.Count >= state.n, "MinLBFGSRestartFrom: Length(X)<N!");
            Utilities.Assert(Utilities.IsFiniteVector(x, state.n), "MinLBFGSRestartFrom: X contains infinite or NaN values!");
            for (i_ = 0; i_ <= state.n - 1; i_++)
            {
                state.x[i_] = x[i_];
            }
            state.rstate.ia.Resize(6);
            state.rstate.ra.Resize(2);
            state.rstate.stage = -1;
            ClearRequestFields(state);
        }




        /*************************************************************************
        Clears request fileds (to be sure that we don't forgot to clear something)
        *************************************************************************/
        private static void ClearRequestFields(MinLbfgsState state)
        {
            state.needf = false;
            state.needfg = false;
            state.xupdated = false;
        }


    }
}
