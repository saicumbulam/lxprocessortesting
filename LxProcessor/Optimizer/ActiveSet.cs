using System;
using System.Collections.Generic;
using LxCommon.DataStructures;
using LxCommon.Utilities;
using LxProcessor.Common;

namespace LxProcessor.Optimizer
{
    
    /*************************************************************************
    This structure describes set of linear constraints (boundary  and  general
    ones) which can be active and inactive. It also has functionality to  work
    with current point and current  gradient  (determine  active  constraints,
    move current point, project gradient into  constrained  subspace,  perform
    constrained preconditioning and so on.

    This  structure  is  intended  to  be  used  by constrained optimizers for
    management of all constraint-related functionality.

    External code may access following internal fields of the structure:
        XC          -   stores current point, array[N].
                        can be accessed only in optimization mode
        ActiveSet   -   active set, array[N+NEC+NIC]:
                        * ActiveSet[I]>0    I-th constraint is in the active set
                        * ActiveSet[I]=0    I-th constraint is at the boundary, but inactive
                        * ActiveSet[I]<0    I-th constraint is far from the boundary (and inactive)
                        * elements from 0 to N-1 correspond to boundary constraints
                        * elements from N to N+NEC+NIC-1 correspond to linear constraints
                        * elements from N to N+NEC-1 are always +1
        PBasis,
        IBasis,
        SBasis      -   after call to SASRebuildBasis() these  matrices  store
                        active constraints, reorthogonalized with  respect  to
                        some inner product:
                        a) for PBasis - one  given  by  preconditioner  matrix
                            (inverse Hessian)
                        b) for SBasis - one given by square of the scale matrix
                        c) for IBasis - traditional dot product
                            
                        array[BasisSize,N+1], where BasisSize is a  number  of
                        positive elements in  ActiveSet[N:N+NEC+NIC-1].  First
                        N columns store linear term, last column stores  right
                        part. All  three  matrices  are linearly equivalent to
                        each other, span(PBasis)=span(IBasis)=span(SBasis).
                            
                        IMPORTANT: you have to call  SASRebuildBasis()  before
                                    accessing these arrays  in  order  to  make
                                    sure that they are up to date.
        BasisSize   -   basis size (PBasis/SBasis/IBasis)
    *************************************************************************/

    public class ActiveSet
    {
        public int n;
        public int algostate;
        public FluxList<double> xc;
        public bool hasxc;
        public FluxList<double> s;
        public FluxList<double> h;
        public FluxList<int> activeset;
        public bool basisisready;
        public Matrix<double> sbasis;
        public Matrix<double> pbasis;
        public Matrix<double> ibasis;
        public int basissize;
        public bool constraintschanged;
        public FluxList<bool> hasbndl;
        public FluxList<bool> hasbndu;
        public FluxList<double> bndl;
        public FluxList<double> bndu;
        public Matrix<double> cleic;
        public int nec;
        public int nic;
        public FluxList<double> mtx;
        public FluxList<int> mtas;
        public FluxList<double> cdtmp;
        public FluxList<double> corrtmp;
        public FluxList<double> unitdiagonal;
        public SnnlsSolver solver;
        public FluxList<double> scntmp;
        public FluxList<double> tmp0;
        public FluxList<double> tmpfeas;
        public Matrix<double> tmpm0;
        public FluxList<double> rctmps;
        public FluxList<double> rctmpg;
        public FluxList<double> rctmprightpart;
        public Matrix<double> rctmpdense0;
        public Matrix<double> rctmpdense1;
        public FluxList<bool> rctmpisequality;
        public FluxList<int> rctmpconstraintidx;
        public FluxList<double> rctmplambdas;
        public Matrix<double> tmpbasis;
        public ActiveSet()
        {
            xc = new FluxList<double>();
            s = new FluxList<double>();
            h = new FluxList<double>();
            activeset = new FluxList<int>();
            sbasis = new Matrix<double>(0, 0);
            pbasis = new Matrix<double>(0, 0);
            ibasis = new Matrix<double>(0, 0);
            hasbndl = new FluxList<bool>();
            hasbndu = new FluxList<bool>();
            bndl = new FluxList<double>();
            bndu = new FluxList<double>();
            cleic = new Matrix<double>();
            mtx = new FluxList<double>();
            mtas = new FluxList<int>();
            cdtmp = new FluxList<double>();
            corrtmp = new FluxList<double>();
            unitdiagonal = new FluxList<double>();
            solver = new SnnlsSolver();
            scntmp = new FluxList<double>();
            tmp0 = new FluxList<double>();
            tmpfeas = new FluxList<double>();
            tmpm0 = new Matrix<double>();
            rctmps = new FluxList<double>();
            rctmpg = new FluxList<double>();
            rctmprightpart = new FluxList<double>();
            rctmpdense0 = new Matrix<double>();
            rctmpdense1 = new Matrix<double>(0, 0);
            rctmpisequality = new FluxList<bool>();
            rctmpconstraintidx = new FluxList<int>();
            rctmplambdas = new FluxList<double>();
            tmpbasis = new Matrix<double>();
        }

        internal void CleanForReuse()
        {
            n = default(int);
            algostate = default(int);
            xc.Clear();
            hasxc = default(bool);
            s.Clear();
            h.Clear();
            activeset.Clear();
            basisisready = default(bool);
            sbasis.Clear();
            pbasis.Clear();
            ibasis.Clear();
            basissize = default(int);
            constraintschanged = default(bool);
            hasbndl.Clear();
            hasbndu.Clear();
            bndl.Clear();
            bndu.Clear();
            cleic.Clear();
            nec = default(int);
            nic = default(int);
            mtx.Clear();
            mtas.Clear();
            cdtmp.Clear();
            corrtmp.Clear();
            unitdiagonal.Clear();
            solver.CleanForReuse();
            scntmp.Clear();
            tmp0.Clear();
            tmpfeas.Clear();
            tmpm0.Clear();
            rctmps.Clear();
            rctmpg.Clear();
            rctmprightpart.Clear();
            rctmpdense0.Clear();
            rctmpdense1.Clear();
            rctmpisequality.Clear();
            rctmpconstraintidx.Clear();
            rctmplambdas.Clear();
            tmpbasis.Clear();
        }

        /*************************************************************************
          This   subroutine   is   used  to initialize active set. By default, empty
          N-variable model with no constraints is  generated.  Previously  allocated
          buffer variables are reused as much as possible.

          Two use cases for this object are described below.

          CASE 1 - STEEPEST DESCENT:

              SASInit()
              repeat:
                  SASReactivateConstraints()
                  SASDescentDirection()
                  SASExploreDirection()
                  SASMoveTo()
              until convergence

          CASE 1 - PRECONDITIONED STEEPEST DESCENT:

              SASInit()
              repeat:
                  SASReactivateConstraintsPrec()
                  SASDescentDirectionPrec()
                  SASExploreDirection()
                  SASMoveTo()
              until convergence

            -- ALGLIB --
               Copyright 21.12.2012 by Bochkanov Sergey
          *************************************************************************/
        public static void Initialize(int n, ActiveSet s)
        {
            s.n = n;
            s.algostate = 0;

            //
            // Constraints
            //
            s.constraintschanged = true;
            s.nec = 0;
            s.nic = 0;
            s.bndl.SetLengthAtLeast(n);
            s.hasbndl.SetLengthAtLeast(n);
            s.bndu.SetLengthAtLeast(n);
            s.hasbndu.SetLengthAtLeast(n);
            for (int i = 0; i <= n - 1; i++)
            {
                s.bndl[i] = Double.NegativeInfinity;
                s.bndu[i] = Double.PositiveInfinity;
                s.hasbndl[i] = false;
                s.hasbndu[i] = false;
            }

            //
            // current point, scale
            //
            s.hasxc = false;
            s.xc.SetLengthAtLeast(n);
            s.s.SetLengthAtLeast(n);
            s.h.SetLengthAtLeast(n);
            for (int i = 0; i <= n - 1; i++)
            {
                s.xc[i] = 0.0;
                s.s[i] = 1.0;
                s.h[i] = 1.0;
            }

            //
            // Other
            //
            s.unitdiagonal.SetLengthAtLeast(n);
            for (int i = 0; i <= n - 1; i++)
            {
                s.unitdiagonal[i] = 1.0;
            }
        }

        /*************************************************************************
           This function sets scaling coefficients for SAS object.

           ALGLIB optimizers use scaling matrices to test stopping  conditions  (step
           size and gradient are scaled before comparison with tolerances).  Scale of
           the I-th variable is a translation invariant measure of:
           a) "how large" the variable is
           b) how large the step should be to make significant changes in the function

           During orthogonalization phase, scale is used to calculate drop tolerances
           (whether vector is significantly non-zero or not).

           INPUT PARAMETERS:
               State   -   structure stores algorithm state
               S       -   array[N], non-zero scaling coefficients
                           S[i] may be negative, sign doesn't matter.

             -- ALGLIB --
                Copyright 21.12.2012 by Bochkanov Sergey
           *************************************************************************/
        public static void SetScale(ActiveSet state,
            FluxList<double> s)
        {
            int i = 0;

            Utilities.Assert(state.algostate == 0, "SASSetScale: you may change scale only in modification mode");
            Utilities.Assert(s.Count >= state.n, "SASSetScale: Length(S)<N");
            for (i = 0; i <= state.n - 1; i++)
            {
                Utilities.Assert(MathExtensions.IsFinite(s[i]), "SASSetScale: S contains infinite or NAN elements");
                Utilities.Assert(s[i] != 0, "SASSetScale: S contains zero elements");
            }
            for (i = 0; i <= state.n - 1; i++)
            {
                state.s[i] = Math.Abs(s[i]);
            }
        }

        /*************************************************************************
          Modification  of  the  preconditioner:  diagonal of approximate Hessian is
          used.

          INPUT PARAMETERS:
              State   -   structure which stores algorithm state
              D       -   diagonal of the approximate Hessian, array[0..N-1],
                          (if larger, only leading N elements are used).

          NOTE 1: D[i] should be positive. Exception will be thrown otherwise.

          NOTE 2: you should pass diagonal of approximate Hessian - NOT ITS INVERSE.

            -- ALGLIB --
               Copyright 21.12.2012 by Bochkanov Sergey
          *************************************************************************/
        public static void SetPreconditionerDiagnol(ActiveSet state,
            FluxList<double> d)
        {
            int i = 0;

            Utilities.Assert(state.algostate == 0, "SASSetPrecDiag: you may change preconditioner only in modification mode");
            Utilities.Assert(d.Count >= state.n, "SASSetPrecDiag: D is too short");
            for (i = 0; i <= state.n - 1; i++)
            {
                Utilities.Assert(MathExtensions.IsFinite(d[i]), "SASSetPrecDiag: D contains infinite or NAN elements");
                Utilities.Assert(d[i] > 0, "SASSetPrecDiag: D contains non-positive elements");
            }
            for (i = 0; i <= state.n - 1; i++)
            {
                state.h[i] = d[i];
            }
        }

        /*************************************************************************
           This function sets/changes boundary constraints.

           INPUT PARAMETERS:
               State   -   structure stores algorithm state
               BndL    -   lower bounds, array[N].
                           If some (all) variables are unbounded, you may specify
                           very small number or -INF.
               BndU    -   upper bounds, array[N].
                           If some (all) variables are unbounded, you may specify
                           very large number or +INF.

           NOTE 1: it is possible to specify BndL[i]=BndU[i]. In this case I-th
           variable will be "frozen" at X[i]=BndL[i]=BndU[i].

             -- ALGLIB --
                Copyright 21.12.2012 by Bochkanov Sergey
           *************************************************************************/
        public static void SetBoundaryConstraints(ActiveSet state,
            FluxList<double> bndl,
            FluxList<double> bndu)
        {
            int i = 0;
            int n = 0;

            Utilities.Assert(state.algostate == 0, "SASSetBC: you may change constraints only in modification mode");
            n = state.n;
            Utilities.Assert(bndl.Count >= n, "SASSetBC: Length(BndL)<N");
            Utilities.Assert(bndu.Count >= n, "SASSetBC: Length(BndU)<N");
            for (i = 0; i <= n - 1; i++)
            {
                Utilities.Assert(MathExtensions.IsFinite(bndl[i]) || Double.IsNegativeInfinity(bndl[i]), "SASSetBC: BndL contains NAN or +INF");
                Utilities.Assert(MathExtensions.IsFinite(bndu[i]) || Double.IsPositiveInfinity(bndu[i]), "SASSetBC: BndL contains NAN or -INF");
                state.bndl[i] = bndl[i];
                state.hasbndl[i] = MathExtensions.IsFinite(bndl[i]);
                state.bndu[i] = bndu[i];
                state.hasbndu[i] = MathExtensions.IsFinite(bndu[i]);
            }
            state.constraintschanged = true;
        }

        /*************************************************************************
           This function sets linear constraints for SAS object.

           Linear constraints are inactive by default (after initial creation).

           INPUT PARAMETERS:
               State   -   SAS structure
               C       -   linear constraints, array[K,N+1].
                           Each row of C represents one constraint, either equality
                           or inequality (see below):
                           * first N elements correspond to coefficients,
                           * last element corresponds to the right part.
                           All elements of C (including right part) must be finite.
               CT      -   type of constraints, array[K]:
                           * if CT[i]>0, then I-th constraint is C[i,*]*x >= C[i,n+1]
                           * if CT[i]=0, then I-th constraint is C[i,*]*x  = C[i,n+1]
                           * if CT[i]<0, then I-th constraint is C[i,*]*x <= C[i,n+1]
               K       -   number of equality/inequality constraints, K>=0

           NOTE 1: linear (non-bound) constraints are satisfied only approximately:
           * there always exists some minor violation (about Epsilon in magnitude)
             due to rounding errors
           * numerical differentiation, if used, may  lead  to  function  evaluations
             outside  of the feasible  area,   because   algorithm  does  NOT  change
             numerical differentiation formula according to linear constraints.
           If you want constraints to be  satisfied  exactly, try to reformulate your
           problem  in  such  manner  that  all constraints will become boundary ones
           (this kind of constraints is always satisfied exactly, both in  the  final
           solution and in all intermediate points).

             -- ALGLIB --
                Copyright 28.11.2010 by Bochkanov Sergey
           *************************************************************************/
        public static void SetsLinearConstraints(ActiveSet state,
            Matrix<double> c,
            FluxList<int> ct,
            int k)
        {
            int n = 0;
            int i = 0;
            int i_ = 0;

            Utilities.Assert(state.algostate == 0, "SASSetLC: you may change constraints only in modification mode");
            n = state.n;

            //
            // First, check for errors in the inputs
            //
            Utilities.Assert(k >= 0, "SASSetLC: K<0");
            Utilities.Assert(c.Columns >= n + 1 || k == 0, "SASSetLC: Cols(C)<N+1");
            Utilities.Assert(c.Rows >= k, "SASSetLC: Rows(C)<K");
            Utilities.Assert(ct.Count >= k, "SASSetLC: Length(CT)<K");
            Utilities.Assert(Utilities.IsFiniteMatrix(c, k, n + 1), "SASSetLC: C contains infinite or NaN values!");

            //
            // Handle zero K
            //
            if (k == 0)
            {
                state.nec = 0;
                state.nic = 0;
                state.constraintschanged = true;
                return;
            }

            //
            // Equality constraints are stored first, in the upper
            // NEC rows of State.CLEIC matrix. Inequality constraints
            // are stored in the next NIC rows.
            //
            // NOTE: we convert inequality constraints to the form
            // A*x<=b before copying them.
            //
            state.cleic.SetLengthAtLeast(k, n + 1);
            state.nec = 0;
            state.nic = 0;
            for (i = 0; i <= k - 1; i++)
            {
                if (ct[i] == 0)
                {
                    for (i_ = 0; i_ <= n; i_++)
                    {
                        state.cleic[state.nec, i_] = c[i, i_];
                    }
                    state.nec = state.nec + 1;
                }
            }
            for (i = 0; i <= k - 1; i++)
            {
                if (ct[i] != 0)
                {
                    if (ct[i] > 0)
                    {
                        for (i_ = 0; i_ <= n; i_++)
                        {
                            state.cleic[state.nec + state.nic, i_] = -c[i, i_];
                        }
                    }
                    else
                    {
                        for (i_ = 0; i_ <= n; i_++)
                        {
                            state.cleic[state.nec + state.nic, i_] = c[i, i_];
                        }
                    }
                    state.nic = state.nic + 1;
                }
            }

            //
            // Mark state as changed
            //
            state.constraintschanged = true;
        }

        /*************************************************************************
           Another variation of SASSetLC(), which accepts  linear  constraints  using
           another representation.

           Linear constraints are inactive by default (after initial creation).

           INPUT PARAMETERS:
               State   -   SAS structure
               CLEIC   -   linear constraints, array[NEC+NIC,N+1].
                           Each row of C represents one constraint:
                           * first N elements correspond to coefficients,
                           * last element corresponds to the right part.
                           First NEC rows store equality constraints, next NIC -  are
                           inequality ones.
                           All elements of C (including right part) must be finite.
               NEC     -   number of equality constraints, NEC>=0
               NIC     -   number of inequality constraints, NIC>=0

           NOTE 1: linear (non-bound) constraints are satisfied only approximately:
           * there always exists some minor violation (about Epsilon in magnitude)
             due to rounding errors
           * numerical differentiation, if used, may  lead  to  function  evaluations
             outside  of the feasible  area,   because   algorithm  does  NOT  change
             numerical differentiation formula according to linear constraints.
           If you want constraints to be  satisfied  exactly, try to reformulate your
           problem  in  such  manner  that  all constraints will become boundary ones
           (this kind of constraints is always satisfied exactly, both in  the  final
           solution and in all intermediate points).

             -- ALGLIB --
                Copyright 28.11.2010 by Bochkanov Sergey
           *************************************************************************/
        public static void SetsLinearConstraintsX(ActiveSet state,
            Matrix<double> cleic,
            int nec,
            int nic)
        {
            int n = 0;
            int i = 0;
            int j = 0;

            Utilities.Assert(state.algostate == 0, "SASSetLCX: you may change constraints only in modification mode");
            n = state.n;

            //
            // First, check for errors in the inputs
            //
            Utilities.Assert(nec >= 0, "SASSetLCX: NEC<0");
            Utilities.Assert(nic >= 0, "SASSetLCX: NIC<0");
            Utilities.Assert(cleic.Columns >= n + 1 || nec + nic == 0, "SASSetLCX: Cols(CLEIC)<N+1");
            Utilities.Assert(cleic.Rows >= nec + nic, "SASSetLCX: Rows(CLEIC)<NEC+NIC");
            Utilities.Assert(Utilities.IsFiniteMatrix(cleic, nec + nic, n + 1), "SASSetLCX: CLEIC contains infinite or NaN values!");

            //
            // Store constraints
            //
            state.cleic.SetLengthAtLeast(nec + nic, n + 1);
            state.nec = nec;
            state.nic = nic;
            for (i = 0; i <= nec + nic - 1; i++)
            {
                for (j = 0; j <= n; j++)
                {
                    state.cleic[i, j] = cleic[i, j];
                }
            }

            //
            // Mark state as changed
            //
            state.constraintschanged = true;
        }

        /*************************************************************************
         This subroutine turns on optimization mode:
         1. feasibility in X is enforced  (in case X=S.XC and constraints  have not
            changed, algorithm just uses X without any modifications at all)
         2. constraints are marked as "candidate" or "inactive"

         INPUT PARAMETERS:
             S   -   active set object
             X   -   initial point (candidate), array[N]. It is expected that X
                     contains only finite values (we do not check it).
            
         OUTPUT PARAMETERS:
             S   -   state is changed
             X   -   initial point can be changed to enforce feasibility
            
         RESULT:
             True in case feasible point was found (mode was changed to "optimization")
             False in case no feasible point was found (mode was not changed)

           -- ALGLIB --
              Copyright 21.12.2012 by Bochkanov Sergey
         *************************************************************************/
        public static bool StartOptimization(ActiveSet state,
            FluxList<double> x)
        {
            bool result = new bool();
            int n = 0;
            int nec = 0;
            int nic = 0;
            int i = 0;
            int j = 0;
            double v = 0;
            int i_ = 0;

            Utilities.Assert(state.algostate == 0, "SASStartOptimization: already in optimization mode");
            result = false;
            n = state.n;
            nec = state.nec;
            nic = state.nic;

            //
            // Enforce feasibility and calculate set of "candidate"/"active" constraints.
            // Always active equality constraints are marked as "active", all other constraints
            // are marked as "candidate".
            //
            state.activeset.SetLengthAtLeast(n + nec + nic);
            for (i = 0; i <= n - 1; i++)
            {
                if (state.hasbndl[i] && state.hasbndu[i])
                {
                    if (state.bndl[i] > state.bndu[i])
                    {
                        return result;
                    }
                }
            }
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.xc[i_] = x[i_];
            }
            if (state.nec + state.nic > 0)
            {

                //
                // General linear constraints are present; general code is used.
                //
                state.tmp0.SetLengthAtLeast(n);
                state.tmpfeas.SetLengthAtLeast(n + state.nic);
                state.tmpm0.SetLengthAtLeast(state.nec + state.nic, n + state.nic + 1);
                for (i = 0; i <= state.nec + state.nic - 1; i++)
                {
                    for (i_ = 0; i_ <= n - 1; i_++)
                    {
                        state.tmpm0[i, i_] = state.cleic[i, i_];
                    }
                    for (j = n; j <= n + state.nic - 1; j++)
                    {
                        state.tmpm0[i, j] = 0;
                    }
                    if (i >= state.nec)
                    {
                        state.tmpm0[i, n + i - state.nec] = 1.0;
                    }
                    state.tmpm0[i, n + state.nic] = state.cleic[i, n];
                }
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    state.tmpfeas[i_] = state.xc[i_];
                }
                for (i = 0; i <= state.nic - 1; i++)
                {
                    v = 0.0;
                    for (i_ = 0; i_ <= n - 1; i_++)
                    {
                        v += state.cleic[i + state.nec, i_] * state.xc[i_];
                    }
                    state.tmpfeas[i + n] = Math.Max(state.cleic[i + state.nec, n] - v, 0.0);
                }
                if (!Utilities.FindFeasiblePoint(ref state.tmpfeas, state.bndl, state.hasbndl, state.bndu, state.hasbndu, n, state.nic, state.tmpm0, state.nec + state.nic, 1.0E-6, ref i, ref j))
                {
                    return result;
                }
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    state.xc[i_] = state.tmpfeas[i_];
                }
                for (i = 0; i <= n - 1; i++)
                {
                    if ((state.hasbndl[i] && state.hasbndu[i]) && state.bndl[i] == state.bndu[i])
                    {
                        state.activeset[i] = 1;
                        continue;
                    }
                    if ((state.hasbndl[i] && state.xc[i] == state.bndl[i]) || (state.hasbndu[i] && state.xc[i] == state.bndu[i]))
                    {
                        state.activeset[i] = 0;
                        continue;
                    }
                    state.activeset[i] = -1;
                }
                for (i = 0; i <= state.nec - 1; i++)
                {
                    state.activeset[n + i] = 1;
                }
                for (i = 0; i <= state.nic - 1; i++)
                {
                    if (state.tmpfeas[n + i] == 0)
                    {
                        state.activeset[n + state.nec + i] = 0;
                    }
                    else
                    {
                        state.activeset[n + state.nec + i] = -1;
                    }
                }
            }
            else
            {

                //
                // Only bound constraints are present, quick code can be used
                //
                for (i = 0; i <= n - 1; i++)
                {
                    state.activeset[i] = -1;
                    if ((state.hasbndl[i] && state.hasbndu[i]) && state.bndl[i] == state.bndu[i])
                    {
                        state.activeset[i] = 1;
                        state.xc[i] = state.bndl[i];
                        continue;
                    }
                    if (state.hasbndl[i] && state.xc[i] <= state.bndl[i])
                    {
                        state.xc[i] = state.bndl[i];
                        state.activeset[i] = 0;
                        continue;
                    }
                    if (state.hasbndu[i] && state.xc[i] >= state.bndu[i])
                    {
                        state.xc[i] = state.bndu[i];
                        state.activeset[i] = 0;
                        continue;
                    }
                }
            }

            //
            // Change state, allocate temporaries
            //
            result = true;
            state.algostate = 1;
            state.basisisready = false;
            state.hasxc = true;
            state.pbasis.SetLengthAtLeast(Math.Min(nec + nic, n), n + 1);
            state.ibasis.SetLengthAtLeast(Math.Min(nec + nic, n), n + 1);
            state.sbasis.SetLengthAtLeast(Math.Min(nec + nic, n), n + 1);
            return result;
        }

        /*************************************************************************
           This function explores search direction and calculates bound for  step  as
           well as information for activation of constraints.

           INPUT PARAMETERS:
               State       -   SAS structure which stores current point and all other
                               active set related information
               D           -   descent direction to explore

           OUTPUT PARAMETERS:
               StpMax      -   upper  limit  on  step  length imposed by yet inactive
                               constraints. Can be  zero  in  case  some  constraints
                               can be activated by zero step.  Equal  to  some  large
                               value in case step is unlimited.
               CIdx        -   -1 for unlimited step, in [0,N+NEC+NIC) in case of
                               limited step.
               VVal        -   value which is assigned to X[CIdx] during activation.
                               For CIdx<0 or CIdx>=N some dummy value is assigned to
                               this parameter.
           *************************************************************************/
        public static void ExploreDirection(ActiveSet state,
            FluxList<double> d,
            ref double stpmax,
            ref int cidx,
            ref double vval)
        {
            int n = 0;
            int nec = 0;
            int nic = 0;
            int i = 0;
            double prevmax = 0;
            double vc = 0;
            double vd = 0;
            int i_ = 0;

            stpmax = 0;
            cidx = 0;
            vval = 0;

            Utilities.Assert(state.algostate == 1, "SASExploreDirection: is not in optimization mode");
            n = state.n;
            nec = state.nec;
            nic = state.nic;
            cidx = -1;
            vval = 0;
            stpmax = 1.0E50;
            for (i = 0; i <= n - 1; i++)
            {
                if (state.activeset[i] <= 0)
                {
                    Utilities.Assert(!state.hasbndl[i] || state.xc[i] >= state.bndl[i], "SASExploreDirection: internal error - infeasible X");
                    Utilities.Assert(!state.hasbndu[i] || state.xc[i] <= state.bndu[i], "SASExploreDirection: internal error - infeasible X");
                    if (state.hasbndl[i] && d[i] < 0)
                    {
                        prevmax = stpmax;
                        stpmax = Utilities.SafeMin(state.xc[i] - state.bndl[i], -d[i], stpmax);
                        if (stpmax < prevmax)
                        {
                            cidx = i;
                            vval = state.bndl[i];
                        }
                    }
                    if (state.hasbndu[i] && d[i] > 0)
                    {
                        prevmax = stpmax;
                        stpmax = Utilities.SafeMin(state.bndu[i] - state.xc[i], d[i], stpmax);
                        if (stpmax < prevmax)
                        {
                            cidx = i;
                            vval = state.bndu[i];
                        }
                    }
                }
            }
            for (i = nec; i <= nec + nic - 1; i++)
            {
                if (state.activeset[n + i] <= 0)
                {
                    vc = 0.0;
                    for (i_ = 0; i_ <= n - 1; i_++)
                    {
                        vc += state.cleic[i, i_] * state.xc[i_];
                    }
                    vc = vc - state.cleic[i, n];
                    vd = 0.0;
                    for (i_ = 0; i_ <= n - 1; i_++)
                    {
                        vd += state.cleic[i, i_] * d[i_];
                    }
                    if (vd <= 0)
                    {
                        continue;
                    }
                    if (vc < 0)
                    {

                        //
                        // XC is strictly feasible with respect to I-th constraint,
                        // we can perform non-zero step because there is non-zero distance
                        // between XC and bound.
                        //
                        prevmax = stpmax;
                        stpmax = Utilities.SafeMin(-vc, vd, stpmax);
                        if (stpmax < prevmax)
                        {
                            cidx = n + i;
                        }
                    }
                    else
                    {

                        //
                        // XC is at the boundary (or slightly beyond it), and step vector
                        // points beyond the boundary.
                        //
                        // The only thing we can do is to perform zero step and activate
                        // I-th constraint.
                        //
                        stpmax = 0;
                        cidx = n + i;
                    }
                }
            }
        }

        /*************************************************************************
          This subroutine moves current point to XN,  in  the  direction  previously
          explored with SASExploreDirection() function.

          Step may activate one constraint. It is assumed than XN  is  approximately
          feasible (small error as  large  as several  ulps  is  possible).   Strict
          feasibility  with  respect  to  bound  constraints  is  enforced    during
          activation, feasibility with respect to general linear constraints is  not
          enforced.

          INPUT PARAMETERS:
              S       -   active set object
              XN      -   new point.
              NeedAct -   True in case one constraint needs activation
              CIdx    -   index of constraint, in [0,N+NEC+NIC).
                          Ignored if NeedAct is false.
                          This value is calculated by SASExploreDirection().
              CVal    -   for CIdx in [0,N) this field stores value which is
                          assigned to XC[CIdx] during activation. CVal is ignored in
                          other cases.
                          This value is calculated by SASExploreDirection().
            
          OUTPUT PARAMETERS:
              S       -   current point and list of active constraints are changed.

          RESULT:
              >0, in case at least one inactive non-candidate constraint was activated
              =0, in case only "candidate" constraints were activated
              <0, in case no constraints were activated by the step

          NOTE: in general case State.XC<>XN because activation of  constraints  may
                slightly change current point (to enforce feasibility).

            -- ALGLIB --
               Copyright 21.12.2012 by Bochkanov Sergey
          *************************************************************************/
        public static int MoveTo(ActiveSet state,
            FluxList<double> xn,
            bool needact,
            int cidx,
            double cval)
        {
            int result = 0;
            int n = 0;
            int nec = 0;
            int nic = 0;
            int i = 0;
            bool wasactivation = new bool();

            Utilities.Assert(state.algostate == 1, "SASMoveTo: is not in optimization mode");
            n = state.n;
            nec = state.nec;
            nic = state.nic;

            //
            // Save previous state, update current point
            //
            state.mtx.SetLengthAtLeast(n);
            state.mtas.SetLengthAtLeast(n + nec + nic);
            for (i = 0; i <= n - 1; i++)
            {
                state.mtx[i] = state.xc[i];
                state.xc[i] = xn[i];
            }
            for (i = 0; i <= n + nec + nic - 1; i++)
            {
                state.mtas[i] = state.activeset[i];
            }

            //
            // Activate constraints
            //
            wasactivation = false;
            if (needact)
            {

                //
                // Activation
                //
                Utilities.Assert(cidx >= 0 && cidx < n + nec + nic, "SASMoveTo: incorrect CIdx");
                if (cidx < n)
                {

                    //
                    // CIdx in [0,N-1] means that bound constraint was activated.
                    // We activate it explicitly to avoid situation when roundoff-error
                    // prevents us from moving EXACTLY to x=CVal.
                    //
                    state.xc[cidx] = cval;
                }
                state.activeset[cidx] = 1;
                wasactivation = true;
            }
            for (i = 0; i <= n - 1; i++)
            {

                //
                // Post-check (some constraints may be activated because of numerical errors)
                //
                if (state.hasbndl[i] && state.xc[i] < state.bndl[i])
                {
                    state.xc[i] = state.bndl[i];
                    state.activeset[i] = 1;
                    wasactivation = true;
                }
                if (state.hasbndu[i] && state.xc[i] > state.bndu[i])
                {
                    state.xc[i] = state.bndu[i];
                    state.activeset[i] = 1;
                    wasactivation = true;
                }
            }

            //
            // Determine return status:
            // * -1 in case no constraints were activated
            // *  0 in case only "candidate" constraints were activated
            // * +1 in case at least one "non-candidate" constraint was activated
            //
            if (wasactivation)
            {

                //
                // Step activated one/several constraints, but sometimes it is spurious
                // activation - RecalculateConstraints() tells us that constraint is
                // inactive (negative Largrange multiplier), but step activates it
                // because of numerical noise.
                //
                // This block of code checks whether step activated truly new constraints
                // (ones which were not in the active set at the solution):
                //
                // * for non-boundary constraint it is enough to check that previous value
                //   of ActiveSet[i] is negative (=far from boundary), and new one is
                //   positive (=we are at the boundary, constraint is activated).
                //
                // * for boundary constraints previous criterion won't work. Each variable
                //   has two constraints, and simply checking their status is not enough -
                //   we have to correctly identify cases when we leave one boundary
                //   (PrevActiveSet[i]=0) and move to another boundary (ActiveSet[i]>0).
                //   Such cases can be identified if we compare previous X with new X.
                //
                // In case only "candidate" constraints were activated, result variable
                // is set to 0. In case at least one new constraint was activated, result
                // is set to 1.
                //
                result = 0;
                for (i = 0; i <= n - 1; i++)
                {
                    if (state.activeset[i] > 0 && state.xc[i] != state.mtx[i])
                    {
                        result = 1;
                    }
                }
                for (i = n; i <= n + state.nec + state.nic - 1; i++)
                {
                    if (state.mtas[i] < 0 && state.activeset[i] > 0)
                    {
                        result = 1;
                    }
                }
            }
            else
            {

                //
                // No activation, return -1
                //
                result = -1;
            }

            //
            // Invalidate basis
            //
            state.basisisready = false;
            return result;
        }

        /*************************************************************************
         This subroutine performs immediate activation of one constraint:
         * "immediate" means that we do not have to move to activate it
         * in case boundary constraint is activated, we enforce current point to be
           exactly at the boundary

         INPUT PARAMETERS:
             S       -   active set object
             CIdx    -   index of constraint, in [0,N+NEC+NIC).
                         This value is calculated by SASExploreDirection().
             CVal    -   for CIdx in [0,N) this field stores value which is
                         assigned to XC[CIdx] during activation. CVal is ignored in
                         other cases.
                         This value is calculated by SASExploreDirection().

           -- ALGLIB --
              Copyright 21.12.2012 by Bochkanov Sergey
         *************************************************************************/

        public static void ImmediateActivation(ActiveSet state,
            int cidx,
            double cval)
        {
            Utilities.Assert(state.algostate == 1, "SASMoveTo: is not in optimization mode");
            if (cidx < state.n)
            {
                state.xc[cidx] = cval;
            }
            state.activeset[cidx] = 1;
            state.basisisready = false;
        }

        /*************************************************************************
           This subroutine calculates descent direction subject to current active set.

           INPUT PARAMETERS:
               S       -   active set object
               G       -   array[N], gradient
               D       -   possibly prealocated buffer;
                           automatically resized if needed.
            
           OUTPUT PARAMETERS:
               D       -   descent direction projected onto current active set.
                           Components of D which correspond to active boundary
                           constraints are forced to be exactly zero.
                           In case D is non-zero, it is normalized to have unit norm.
                        
           NOTE: in  case active set has N  active  constraints  (or  more),  descent
                 direction is forced to be exactly zero.

             -- ALGLIB --
                Copyright 21.12.2012 by Bochkanov Sergey
           *************************************************************************/
        public static void ConstrainedDescent(ActiveSet state,
            FluxList<double> g,
            ref FluxList<double> d)
        {
            Utilities.Assert(state.algostate == 1, "SASConstrainedDescent: is not in optimization mode");
            RebuildBasis(state);
            ConstrainedDescent(state, g, state.unitdiagonal, state.ibasis, true, ref d);
        }

        /*************************************************************************
           This  subroutine  calculates  preconditioned  descent direction subject to
           current active set.

           INPUT PARAMETERS:
               S       -   active set object
               G       -   array[N], gradient
               D       -   possibly prealocated buffer;
                           automatically resized if needed.
            
           OUTPUT PARAMETERS:
               D       -   descent direction projected onto current active set.
                           Components of D which correspond to active boundary
                           constraints are forced to be exactly zero.
                           In case D is non-zero, it is normalized to have unit norm.
                        
           NOTE: in  case active set has N  active  constraints  (or  more),  descent
                 direction is forced to be exactly zero.

             -- ALGLIB --
                Copyright 21.12.2012 by Bochkanov Sergey
           *************************************************************************/

        public static void ConstrainedDescentPreconditioned(ActiveSet state,
            FluxList<double> g,
            ref FluxList<double> d)
        {
            Utilities.Assert(state.algostate == 1, "SASConstrainedDescentPrec: is not in optimization mode");
            RebuildBasis(state);
            ConstrainedDescent(state, g, state.h, state.pbasis, true, ref d);
        }

        /*************************************************************************
          This subroutine calculates product of direction vector and  preconditioner
          multiplied subject to current active set.

          INPUT PARAMETERS:
              S       -   active set object
              D       -   array[N], direction
            
          OUTPUT PARAMETERS:
              D       -   preconditioned direction projected onto current active set.
                          Components of D which correspond to active boundary
                          constraints are forced to be exactly zero.
                        
          NOTE: in  case active set has N  active  constraints  (or  more),  descent
                direction is forced to be exactly zero.

            -- ALGLIB --
               Copyright 21.12.2012 by Bochkanov Sergey
          *************************************************************************/
        public static void ConstrainedDirection(ActiveSet state,
            ref FluxList<double> d)
        {
            int i = 0;

            Utilities.Assert(state.algostate == 1, "SASConstrainedAntigradientPrec: is not in optimization mode");
            RebuildBasis(state);
            ConstrainedDescent(state, d, state.unitdiagonal, state.ibasis, false, ref state.cdtmp);
            for (i = 0; i <= state.n - 1; i++)
            {
                d[i] = -state.cdtmp[i];
            }
        }

        /*************************************************************************
            This subroutine calculates product of direction vector and  preconditioner
            multiplied subject to current active set.

            INPUT PARAMETERS:
                S       -   active set object
                D       -   array[N], direction
            
            OUTPUT PARAMETERS:
                D       -   preconditioned direction projected onto current active set.
                            Components of D which correspond to active boundary
                            constraints are forced to be exactly zero.
                        
            NOTE: in  case active set has N  active  constraints  (or  more),  descent
                  direction is forced to be exactly zero.

              -- ALGLIB --
                 Copyright 21.12.2012 by Bochkanov Sergey
            *************************************************************************/
        public static void ConstrainedDirectionPreconditioned(ActiveSet state,
            ref FluxList<double> d)
        {
            int i = 0;

            Utilities.Assert(state.algostate == 1, "SASConstrainedAntigradientPrec: is not in optimization mode");
            RebuildBasis(state);
            ConstrainedDescent(state, d, state.h, state.pbasis, false, ref state.cdtmp);
            for (i = 0; i <= state.n - 1; i++)
            {
                d[i] = -state.cdtmp[i];
            }
        }

        /*************************************************************************
            This  subroutine  performs  correction of some (possibly infeasible) point
            with respect to a) current active set, b) all boundary  constraints,  both
            active and inactive:

            0) we calculate L1 penalty term for violation of active linear constraints
               (one which is returned by SASActiveLCPenalty1() function).
            1) first, it performs projection (orthogonal with respect to scale  matrix
               S) of X into current active set: X -> X1.
            2) next, we perform projection with respect to  ALL  boundary  constraints
               which are violated at X1: X1 -> X2.
            3) X is replaced by X2.

            The idea is that this function can preserve and enforce feasibility during
            optimization, and additional penalty parameter can be used to prevent algo
            from leaving feasible set because of rounding errors.

            INPUT PARAMETERS:
                S       -   active set object
                X       -   array[N], candidate point
            
            OUTPUT PARAMETERS:
                X       -   "improved" candidate point:
                            a) feasible with respect to all boundary constraints
                            b) feasibility with respect to active set is retained at
                               good level.
                Penalty -   penalty term, which can be added to function value if user
                            wants to penalize violation of constraints (recommended).
                        
            NOTE: this function is not intended to find exact  projection  (i.e.  best
                  approximation) of X into feasible set. It just improves situation  a
                  bit.
                  Regular  use  of   this function will help you to retain feasibility
                  - if you already have something to start  with  and  constrain  your
                  steps is such way that the only source of infeasibility are roundoff
                  errors.

              -- ALGLIB --
                 Copyright 21.12.2012 by Bochkanov Sergey
            *************************************************************************/
        public static void Correction(ActiveSet state,
            FluxList<double> x,
            ref double penalty)
        {
            int i = 0;
            int j = 0;
            int n = 0;
            double v = 0;
            int i_ = 0;

            penalty = 0;

            Utilities.Assert(state.algostate == 1, "SASCorrection: is not in optimization mode");
            RebuildBasis(state);
            n = state.n;
            state.corrtmp.SetLengthAtLeast(n);

            //
            // Calculate penalty term.
            //
            penalty = ActiveLcPenalty1(state, x);

            //
            // Perform projection 1.
            //
            // This projecton is given by:
            //
            //     x_proj = x - S*S*As'*(As*x-b)
            //
            // where x is original x before projection, S is a scale matrix,
            // As is a matrix of equality constraints (active set) which were
            // orthogonalized with respect to inner product given by S (i.e. we
            // have As*S*S'*As'=I), b is a right part of the orthogonalized
            // constraints.
            //
            // NOTE: you can verify that x_proj is strictly feasible w.r.t.
            //       active set by multiplying it by As - you will get
            //       As*x_proj = As*x - As*x + b = b.
            //
            //       This formula for projection can be obtained by solving
            //       following minimization problem.
            //
            //           min ||inv(S)*(x_proj-x)||^2 s.t. As*x_proj=b
            //       
            //
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.corrtmp[i_] = x[i_];
            }
            for (i = 0; i <= state.basissize - 1; i++)
            {
                v = -state.sbasis[i, n];
                for (j = 0; j <= n - 1; j++)
                {
                    v = v + state.sbasis[i, j] * state.corrtmp[j];
                }
                for (j = 0; j <= n - 1; j++)
                {
                    state.corrtmp[j] = state.corrtmp[j] - v * state.sbasis[i, j] * MathExtensions.Squared(state.s[j]);
                }
            }
            for (i = 0; i <= n - 1; i++)
            {
                if (state.activeset[i] > 0)
                {
                    state.corrtmp[i] = state.xc[i];
                }
            }

            //
            // Perform projection 2
            //
            for (i = 0; i <= n - 1; i++)
            {
                x[i] = state.corrtmp[i];
                if (state.hasbndl[i] && x[i] < state.bndl[i])
                {
                    x[i] = state.bndl[i];
                }
                if (state.hasbndu[i] && x[i] > state.bndu[i])
                {
                    x[i] = state.bndu[i];
                }
            }
        }

        /*************************************************************************
            This  subroutine returns L1 penalty for violation of active general linear
            constraints (violation of boundary or inactive linear constraints  is  not
            added to penalty).

            Penalty term is equal to:
            
                Penalty = SUM( Abs((C_i*x-R_i)/Alpha_i) )
            
            Here:
            * summation is performed for I=0...NEC+NIC-1, ActiveSet[N+I]>0
              (only for rows of CLEIC which are in active set)
            * C_i is I-th row of CLEIC
            * R_i is corresponding right part
            * S is a scale matrix
            * Alpha_i = ||S*C_i|| - is a scaling coefficient which "normalizes"
              I-th summation term according to its scale.

            INPUT PARAMETERS:
                S       -   active set object
                X       -   array[N], candidate point

              -- ALGLIB --
                 Copyright 21.12.2012 by Bochkanov Sergey
            *************************************************************************/
        public static double ActiveLcPenalty1(ActiveSet state,
            FluxList<double> x)
        {
            double result = 0;
            int i = 0;
            int j = 0;
            int n = 0;
            int nec = 0;
            int nic = 0;
            double v = 0;
            double alpha = 0;
            double p = 0;

            Utilities.Assert(state.algostate == 1, "SASActiveLCPenalty1: is not in optimization mode");
            RebuildBasis(state);
            n = state.n;
            nec = state.nec;
            nic = state.nic;

            //
            // Calculate penalty term.
            //
            result = 0;
            for (i = 0; i <= nec + nic - 1; i++)
            {
                if (state.activeset[n + i] > 0)
                {
                    alpha = 0;
                    p = -state.cleic[i, n];
                    for (j = 0; j <= n - 1; j++)
                    {
                        v = state.cleic[i, j];
                        p = p + v * x[j];
                        alpha = alpha + MathExtensions.Squared(v * state.s[j]);
                    }
                    alpha = Math.Sqrt(alpha);
                    if (alpha != 0)
                    {
                        result = result + Math.Abs(p / alpha);
                    }
                }
            }
            return result;
        }

        /*************************************************************************
           This subroutine calculates scaled norm of  vector  after  projection  onto
           subspace of active constraints. Most often this function is used  to  test
           stopping conditions.

           INPUT PARAMETERS:
               S       -   active set object
               D       -   vector whose norm is calculated
            
           RESULT:
               Vector norm (after projection and scaling)
            
           NOTE: projection is performed first, scaling is performed after projection

             -- ALGLIB --
                Copyright 21.12.2012 by Bochkanov Sergey
           *************************************************************************/
        public static double ScaledConstrainedNorm(ActiveSet state, FluxList<double> d)
        {
            double result = 0;
            int i = 0;
            int n = 0;
            double v = 0;
            int i_ = 0;

            Utilities.Assert(state.algostate == 1, "SASMoveTo: is not in optimization mode");
            n = state.n;
            state.scntmp.SetLengthAtLeast(n);

            //
            // Prepare basis (if needed)
            //
            RebuildBasis(state);

            //
            // Calculate descent direction
            //
            for (i = 0; i <= n - 1; i++)
            {
                if (state.activeset[i] > 0)
                {
                    state.scntmp[i] = 0;
                }
                else
                {
                    state.scntmp[i] = d[i];
                }
            }
            for (i = 0; i <= state.basissize - 1; i++)
            {
                v = 0.0;
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    v += state.ibasis[i, i_] * state.scntmp[i_];
                }
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    state.scntmp[i_] = state.scntmp[i_] - v * state.ibasis[i, i_];
                }
            }
            v = 0.0;
            for (i = 0; i <= n - 1; i++)
            {
                v = v + MathExtensions.Squared(state.s[i] * state.scntmp[i]);
            }
            result = Math.Sqrt(v);
            return result;
        }

        /*************************************************************************
           This subroutine turns off optimization mode.

           INPUT PARAMETERS:
               S   -   active set object
            
           OUTPUT PARAMETERS:
               S   -   state is changed

           NOTE: this function can be called many times for optimizer which was
                 already stopped.

             -- ALGLIB --
                Copyright 21.12.2012 by Bochkanov Sergey
           *************************************************************************/

        public static void StopOptimization(ActiveSet state)
        {
            state.algostate = 0;
        }

        /*************************************************************************
           This function recalculates constraints - activates  and  deactivates  them
           according to gradient value at current point. Algorithm  assumes  that  we
           want to make steepest descent step from  current  point;  constraints  are
           activated and deactivated in such way that we won't violate any constraint
           by steepest descent step.

           After call to this function active set is ready to  try  steepest  descent
           step (SASDescentDirection-SASExploreDirection-SASMoveTo).

           Only already "active" and "candidate" elements of ActiveSet are  examined;
           constraints which are not active are not examined.

           INPUT PARAMETERS:
               State       -   active set object
               GC          -   array[N], gradient at XC
            
           OUTPUT PARAMETERS:
               State       -   active set object, with new set of constraint

             -- ALGLIB --
                Copyright 26.09.2012 by Bochkanov Sergey
           *************************************************************************/
        public static void RecalculateConstraints(ActiveSet state,
            FluxList<double> gc)
        {
            Utilities.Assert(state.algostate == 1, "SASReactivateConstraints: must be in optimization mode");
            ReactivateConstraints(state, gc, state.unitdiagonal);
        }

        /*************************************************************************
          This function recalculates constraints - activates  and  deactivates  them
          according to gradient value at current point.

          Algorithm  assumes  that  we  want  to make Quasi-Newton step from current
          point with diagonal Quasi-Newton matrix H. Constraints are  activated  and
          deactivated in such way that we won't violate any constraint by step.

          After call to  this  function  active set is ready to  try  preconditioned
          steepest descent step (SASDescentDirection-SASExploreDirection-SASMoveTo).

          Only already "active" and "candidate" elements of ActiveSet are  examined;
          constraints which are not active are not examined.

          INPUT PARAMETERS:
              State       -   active set object
              GC          -   array[N], gradient at XC
            
          OUTPUT PARAMETERS:
              State       -   active set object, with new set of constraint

            -- ALGLIB --
               Copyright 26.09.2012 by Bochkanov Sergey
          *************************************************************************/
        public static void RecalculateConstraintsPreconditioned(ActiveSet state,
            FluxList<double> gc)
        {
            Utilities.Assert(state.algostate == 1, "SASReactivateConstraintsPrec: must be in optimization mode");
            ReactivateConstraints(state, gc, state.h);
        }

        /*************************************************************************
           This function builds three orthonormal basises for current active set:
           * P-orthogonal one, which is orthogonalized with inner product
             (x,y) = x'*P*y, where P=inv(H) is current preconditioner
           * S-orthogonal one, which is orthogonalized with inner product
             (x,y) = x'*S'*S*y, where S is diagonal scaling matrix
           * I-orthogonal one, which is orthogonalized with standard dot product

           NOTE: all sets of orthogonal vectors are guaranteed  to  have  same  size.
                 P-orthogonal basis is built first, I/S-orthogonal basises are forced
                 to have same number of vectors as P-orthogonal one (padded  by  zero
                 vectors if needed).
              
           NOTE: this function tracks changes in active set; first call  will  result
                 in reorthogonalization

           INPUT PARAMETERS:
               State   -   active set object
               H       -   diagonal preconditioner, H[i]>0

           OUTPUT PARAMETERS:
               State   -   active set object with new basis
            
             -- ALGLIB --
                Copyright 20.06.2012 by Bochkanov Sergey
           *************************************************************************/
        public static void RebuildBasis(ActiveSet state)
        {
            int n = 0;
            int nec = 0;
            int nic = 0;
            int i = 0;
            int j = 0;
            int t = 0;
            int nactivelin = 0;
            int nactivebnd = 0;
            double v = 0;
            double vmax = 0;
            int kmax = 0;
            int i_ = 0;

            if (state.basisisready)
            {
                return;
            }
            n = state.n;
            nec = state.nec;
            nic = state.nic;
            state.tmpbasis.SetLengthAtLeast(nec + nic, n + 1);
            state.basissize = 0;
            state.basisisready = true;

            //
            // Determine number of active boundary and non-boundary
            // constraints, move them to TmpBasis. Quick exit if no
            // non-boundary constraints were detected.
            //
            nactivelin = 0;
            nactivebnd = 0;
            for (i = 0; i <= nec + nic - 1; i++)
            {
                if (state.activeset[n + i] > 0)
                {
                    nactivelin = nactivelin + 1;
                }
            }
            for (j = 0; j <= n - 1; j++)
            {
                if (state.activeset[j] > 0)
                {
                    nactivebnd = nactivebnd + 1;
                }
            }
            if (nactivelin == 0)
            {
                return;
            }

            //
            // Orthogonalize linear constraints (inner product is given by preconditioner)
            // with respect to each other and boundary ones:
            // * normalize all constraints
            // * orthogonalize with respect to boundary ones
            // * repeat:
            //   * if basisSize+nactivebnd=n - TERMINATE
            //   * choose largest row from TmpBasis
            //   * if row norm is too small  - TERMINATE
            //   * add row to basis, normalize
            //   * remove from TmpBasis, orthogonalize other constraints with respect to this one
            //
            nactivelin = 0;
            for (i = 0; i <= nec + nic - 1; i++)
            {
                if (state.activeset[n + i] > 0)
                {
                    for (i_ = 0; i_ <= n; i_++)
                    {
                        state.tmpbasis[nactivelin, i_] = state.cleic[i, i_];
                    }
                    nactivelin = nactivelin + 1;
                }
            }
            for (i = 0; i <= nactivelin - 1; i++)
            {
                v = 0.0;
                for (j = 0; j <= n - 1; j++)
                {
                    v = v + MathExtensions.Squared(state.tmpbasis[i, j]) / state.h[j];
                }
                if (v > 0)
                {
                    v = 1 / Math.Sqrt(v);
                    for (j = 0; j <= n; j++)
                    {
                        state.tmpbasis[i, j] = state.tmpbasis[i, j] * v;
                    }
                }
            }
            for (j = 0; j <= n - 1; j++)
            {
                if (state.activeset[j] > 0)
                {
                    for (i = 0; i <= nactivelin - 1; i++)
                    {
                        state.tmpbasis[i, n] = state.tmpbasis[i, n] - state.tmpbasis[i, j] * state.xc[j];
                        state.tmpbasis[i, j] = 0.0;
                    }
                }
            }
            while (state.basissize + nactivebnd < n)
            {

                //
                // Find largest vector, add to basis
                //
                vmax = -1;
                kmax = -1;
                for (i = 0; i <= nactivelin - 1; i++)
                {
                    v = 0.0;
                    for (j = 0; j <= n - 1; j++)
                    {
                        v = v + MathExtensions.Squared(state.tmpbasis[i, j]) / state.h[j];
                    }
                    v = Math.Sqrt(v);
                    if (v > vmax)
                    {
                        vmax = v;
                        kmax = i;
                    }
                }
                if (vmax < 1.0E4 * MathExtensions.MachineEpsilon)
                {
                    break;
                }
                v = 1 / vmax;
                for (i_ = 0; i_ <= n; i_++)
                {
                    state.pbasis[state.basissize, i_] = v * state.tmpbasis[kmax, i_];
                }
                state.basissize = state.basissize + 1;

                //
                // Reorthogonalize other vectors with respect to chosen one.
                // Remove it from the array.
                //
                for (i = 0; i <= nactivelin - 1; i++)
                {
                    if (i != kmax)
                    {
                        v = 0;
                        for (j = 0; j <= n - 1; j++)
                        {
                            v = v + state.pbasis[state.basissize - 1, j] * state.tmpbasis[i, j] / state.h[j];
                        }
                        for (i_ = 0; i_ <= n; i_++)
                        {
                            state.tmpbasis[i, i_] = state.tmpbasis[i, i_] - v * state.pbasis[state.basissize - 1, i_];
                        }
                    }
                }
                for (j = 0; j <= n; j++)
                {
                    state.tmpbasis[kmax, j] = 0;
                }
            }

            //
            // Orthogonalize linear constraints using traditional dot product
            // with respect to each other and boundary ones.
            //
            // NOTE: we force basis size to be equal to one which was computed
            //       at the previous step, with preconditioner-based inner product.
            //
            nactivelin = 0;
            for (i = 0; i <= nec + nic - 1; i++)
            {
                if (state.activeset[n + i] > 0)
                {
                    for (i_ = 0; i_ <= n; i_++)
                    {
                        state.tmpbasis[nactivelin, i_] = state.cleic[i, i_];
                    }
                    nactivelin = nactivelin + 1;
                }
            }
            for (i = 0; i <= nactivelin - 1; i++)
            {
                v = 0.0;
                for (j = 0; j <= n - 1; j++)
                {
                    v = v + MathExtensions.Squared(state.tmpbasis[i, j]);
                }
                if (v > 0)
                {
                    v = 1 / Math.Sqrt(v);
                    for (j = 0; j <= n; j++)
                    {
                        state.tmpbasis[i, j] = state.tmpbasis[i, j] * v;
                    }
                }
            }
            for (j = 0; j <= n - 1; j++)
            {
                if (state.activeset[j] > 0)
                {
                    for (i = 0; i <= nactivelin - 1; i++)
                    {
                        state.tmpbasis[i, n] = state.tmpbasis[i, n] - state.tmpbasis[i, j] * state.xc[j];
                        state.tmpbasis[i, j] = 0.0;
                    }
                }
            }
            for (t = 0; t <= state.basissize - 1; t++)
            {

                //
                // Find largest vector, add to basis.
                //
                vmax = -1;
                kmax = -1;
                for (i = 0; i <= nactivelin - 1; i++)
                {
                    v = 0.0;
                    for (j = 0; j <= n - 1; j++)
                    {
                        v = v + MathExtensions.Squared(state.tmpbasis[i, j]);
                    }
                    v = Math.Sqrt(v);
                    if (v > vmax)
                    {
                        vmax = v;
                        kmax = i;
                    }
                }
                if (vmax == 0)
                {
                    for (j = 0; j <= n; j++)
                    {
                        state.ibasis[t, j] = 0.0;
                    }
                    continue;
                }
                v = 1 / vmax;
                for (i_ = 0; i_ <= n; i_++)
                {
                    state.ibasis[t, i_] = v * state.tmpbasis[kmax, i_];
                }

                //
                // Reorthogonalize other vectors with respect to chosen one.
                // Remove it from the array.
                //
                for (i = 0; i <= nactivelin - 1; i++)
                {
                    if (i != kmax)
                    {
                        v = 0;
                        for (j = 0; j <= n - 1; j++)
                        {
                            v = v + state.ibasis[t, j] * state.tmpbasis[i, j];
                        }
                        for (i_ = 0; i_ <= n; i_++)
                        {
                            state.tmpbasis[i, i_] = state.tmpbasis[i, i_] - v * state.ibasis[t, i_];
                        }
                    }
                }
                for (j = 0; j <= n; j++)
                {
                    state.tmpbasis[kmax, j] = 0;
                }
            }

            //
            // Orthogonalize linear constraints using inner product given by
            // scale matrix.
            //
            // NOTE: we force basis size to be equal to one which was computed
            //       with preconditioner-based inner product.
            //
            nactivelin = 0;
            for (i = 0; i <= nec + nic - 1; i++)
            {
                if (state.activeset[n + i] > 0)
                {
                    for (i_ = 0; i_ <= n; i_++)
                    {
                        state.tmpbasis[nactivelin, i_] = state.cleic[i, i_];
                    }
                    nactivelin = nactivelin + 1;
                }
            }
            for (i = 0; i <= nactivelin - 1; i++)
            {
                v = 0.0;
                for (j = 0; j <= n - 1; j++)
                {
                    v = v + MathExtensions.Squared(state.tmpbasis[i, j] * state.s[j]);
                }
                if (v > 0)
                {
                    v = 1 / Math.Sqrt(v);
                    for (j = 0; j <= n; j++)
                    {
                        state.tmpbasis[i, j] = state.tmpbasis[i, j] * v;
                    }
                }
            }
            for (j = 0; j <= n - 1; j++)
            {
                if (state.activeset[j] > 0)
                {
                    for (i = 0; i <= nactivelin - 1; i++)
                    {
                        state.tmpbasis[i, n] = state.tmpbasis[i, n] - state.tmpbasis[i, j] * state.xc[j];
                        state.tmpbasis[i, j] = 0.0;
                    }
                }
            }
            for (t = 0; t <= state.basissize - 1; t++)
            {

                //
                // Find largest vector, add to basis.
                //
                vmax = -1;
                kmax = -1;
                for (i = 0; i <= nactivelin - 1; i++)
                {
                    v = 0.0;
                    for (j = 0; j <= n - 1; j++)
                    {
                        v = v + MathExtensions.Squared(state.tmpbasis[i, j] * state.s[j]);
                    }
                    v = Math.Sqrt(v);
                    if (v > vmax)
                    {
                        vmax = v;
                        kmax = i;
                    }
                }
                if (vmax == 0)
                {
                    for (j = 0; j <= n; j++)
                    {
                        state.sbasis[t, j] = 0.0;
                    }
                    continue;
                }
                v = 1 / vmax;
                for (i_ = 0; i_ <= n; i_++)
                {
                    state.sbasis[t, i_] = v * state.tmpbasis[kmax, i_];
                }

                //
                // Reorthogonalize other vectors with respect to chosen one.
                // Remove it from the array.
                //
                for (i = 0; i <= nactivelin - 1; i++)
                {
                    if (i != kmax)
                    {
                        v = 0;
                        for (j = 0; j <= n - 1; j++)
                        {
                            v = v + state.sbasis[t, j] * state.tmpbasis[i, j] * MathExtensions.Squared(state.s[j]);
                        }
                        for (i_ = 0; i_ <= n; i_++)
                        {
                            state.tmpbasis[i, i_] = state.tmpbasis[i, i_] - v * state.sbasis[t, i_];
                        }
                    }
                }
                for (j = 0; j <= n; j++)
                {
                    state.tmpbasis[kmax, j] = 0;
                }
            }
        }

        /*************************************************************************
          This  subroutine  calculates  preconditioned  descent direction subject to
          current active set.

          INPUT PARAMETERS:
              State   -   active set object
              G       -   array[N], gradient
              H       -   array[N], Hessian matrix
              HA      -   active constraints orthogonalized in such way
                          that HA*inv(H)*HA'= I.
              Normalize-  whether we need normalized descent or not
              D       -   possibly preallocated buffer; automatically resized.
            
          OUTPUT PARAMETERS:
              D       -   descent direction projected onto current active set.
                          Components of D which correspond to active boundary
                          constraints are forced to be exactly zero.
                          In case D is non-zero and Normalize is True, it is
                          normalized to have unit norm.

            -- ALGLIB --
               Copyright 21.12.2012 by Bochkanov Sergey
          *************************************************************************/
        private static void ConstrainedDescent(ActiveSet state,
            FluxList<double> g,
            FluxList<double> h,
            Matrix<double> ha,
            bool normalize,
            ref FluxList<double> d)
        {
            int i = 0;
            int j = 0;
            int n = 0;
            double v = 0;
            int nactive = 0;
            int i_ = 0;

            Utilities.Assert(state.algostate == 1, "SAS: internal error in ConstrainedDescent() - not in optimization mode");
            Utilities.Assert(state.basisisready, "SAS: internal error in ConstrainedDescent() - no basis");
            n = state.n;
            d.SetLengthAtLeast(n);

            //
            // Calculate preconditioned constrained descent direction:
            //
            //     d := -inv(H)*( g - HA'*(HA*inv(H)*g) )
            //
            // Formula above always gives direction which is orthogonal to rows of HA.
            // You can verify it by multiplication of both sides by HA[i] (I-th row),
            // taking into account that HA*inv(H)*HA'= I (by definition of HA - it is
            // orthogonal basis with inner product given by inv(H)).
            //
            nactive = 0;
            for (i = 0; i <= n - 1; i++)
            {
                if (state.activeset[i] > 0)
                {
                    d[i] = 0;
                    nactive = nactive + 1;
                }
                else
                {
                    d[i] = g[i];
                }
            }
            for (i = 0; i <= state.basissize - 1; i++)
            {
                v = 0.0;
                for (j = 0; j <= n - 1; j++)
                {
                    v = v + ha[i, j] * d[j] / h[j];
                }
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    d[i_] = d[i_] - v * ha[i, i_];
                }
                nactive = nactive + 1;
            }
            v = 0.0;
            for (i = 0; i <= n - 1; i++)
            {
                if (state.activeset[i] > 0)
                {
                    d[i] = 0;
                }
                else
                {
                    d[i] = -(d[i] / h[i]);
                    v = v + MathExtensions.Squared(d[i]);
                }
            }
            v = Math.Sqrt(v);
            if (nactive >= n)
            {
                v = 0;
                for (i = 0; i <= n - 1; i++)
                {
                    d[i] = 0;
                }
            }
            if (normalize && v > 0)
            {
                for (i = 0; i <= n - 1; i++)
                {
                    d[i] = d[i] / v;
                }
            }
        }

        /*************************************************************************
        This function recalculates constraints - activates  and  deactivates  them
        according to gradient value at current point.

        Algorithm  assumes  that  we  want  to make Quasi-Newton step from current
        point with diagonal Quasi-Newton matrix H. Constraints are  activated  and
        deactivated in such way that we won't violate any constraint by step.

        Only already "active" and "candidate" elements of ActiveSet are  examined;
        constraints which are not active are not examined.

        INPUT PARAMETERS:
        State       -   active set object
        GC          -   array[N], gradient at XC
        H           -   array[N], Hessian matrix
            
        OUTPUT PARAMETERS:
        State       -   active set object, with new set of constraint

        -- ALGLIB --
            Copyright 26.09.2012 by Bochkanov Sergey
        *************************************************************************/
        private static void ReactivateConstraints(ActiveSet state,
            FluxList<double> gc,
            FluxList<double> h)
        {
            int n = 0;
            int nec = 0;
            int nic = 0;
            int i = 0;
            int j = 0;
            int idx0 = 0;
            int idx1 = 0;
            double v = 0;
            int nactivebnd = 0;
            int nactivelin = 0;
            int nactiveconstraints = 0;
            double rowscale = 0;
            int i_ = 0;

            Utilities.Assert(state.algostate == 1, "SASReactivateConstraintsPrec: must be in optimization mode");

            //
            // Prepare
            //
            n = state.n;
            nec = state.nec;
            nic = state.nic;
            state.basisisready = false;

            //
            // Handle important special case - no linear constraints,
            // only boundary constraints are present
            //
            if (nec + nic == 0)
            {
                for (i = 0; i <= n - 1; i++)
                {
                    if ((state.hasbndl[i] && state.hasbndu[i]) && state.bndl[i] == state.bndu[i])
                    {
                        state.activeset[i] = 1;
                        continue;
                    }
                    if ((state.hasbndl[i] && state.xc[i] == state.bndl[i]) && gc[i] >= 0)
                    {
                        state.activeset[i] = 1;
                        continue;
                    }
                    if ((state.hasbndu[i] && state.xc[i] == state.bndu[i]) && gc[i] <= 0)
                    {
                        state.activeset[i] = 1;
                        continue;
                    }
                    state.activeset[i] = -1;
                }
                return;
            }

            //
            // General case.
            // Allocate temporaries.
            //
            state.rctmpg.SetLengthAtLeast(n);
            state.rctmprightpart.SetLengthAtLeast(n);
            state.rctmps.SetLengthAtLeast(n);
            state.rctmpdense0.SetLengthAtLeast(n, nec + nic);
            state.rctmpdense1.SetLengthAtLeast(n, nec + nic);
            state.rctmpisequality.SetLengthAtLeast(n + nec + nic);
            state.rctmpconstraintidx.SetLengthAtLeast(n + nec + nic);

            //
            // Calculate descent direction
            //
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.rctmpg[i_] = -gc[i_];
            }

            //
            // Determine candidates to the active set.
            //
            // After this block constraints become either "inactive" (ActiveSet[i]<0)
            // or "candidates" (ActiveSet[i]=0). Previously active constraints always
            // become "candidates".
            //
            for (i = 0; i <= n + nec + nic - 1; i++)
            {
                if (state.activeset[i] > 0)
                {
                    state.activeset[i] = 0;
                }
                else
                {
                    state.activeset[i] = -1;
                }
            }
            nactiveconstraints = 0;
            nactivebnd = 0;
            nactivelin = 0;
            for (i = 0; i <= n - 1; i++)
            {

                //
                // Activate boundary constraints:
                // * copy constraint index to RCTmpConstraintIdx
                // * set corresponding element of ActiveSet[] to "candidate"
                // * fill RCTmpS by either +1 (lower bound) or -1 (upper bound)
                // * set RCTmpIsEquality to False (BndL<BndU) or True (BndL=BndU)
                // * increase counters
                //
                if ((state.hasbndl[i] && state.hasbndu[i]) && state.bndl[i] == state.bndu[i])
                {

                    //
                    // Equality constraint is activated
                    //
                    state.rctmpconstraintidx[nactiveconstraints] = i;
                    state.activeset[i] = 0;
                    state.rctmps[i] = 1.0;
                    state.rctmpisequality[nactiveconstraints] = true;
                    nactiveconstraints = nactiveconstraints + 1;
                    nactivebnd = nactivebnd + 1;
                    continue;
                }
                if (state.hasbndl[i] && state.xc[i] == state.bndl[i])
                {

                    //
                    // Lower bound is activated
                    //
                    state.rctmpconstraintidx[nactiveconstraints] = i;
                    state.activeset[i] = 0;
                    state.rctmps[i] = -1.0;
                    state.rctmpisequality[nactiveconstraints] = false;
                    nactiveconstraints = nactiveconstraints + 1;
                    nactivebnd = nactivebnd + 1;
                    continue;
                }
                if (state.hasbndu[i] && state.xc[i] == state.bndu[i])
                {

                    //
                    // Upper bound is activated
                    //
                    state.rctmpconstraintidx[nactiveconstraints] = i;
                    state.activeset[i] = 0;
                    state.rctmps[i] = 1.0;
                    state.rctmpisequality[nactiveconstraints] = false;
                    nactiveconstraints = nactiveconstraints + 1;
                    nactivebnd = nactivebnd + 1;
                    continue;
                }
            }
            for (i = 0; i <= nec + nic - 1; i++)
            {
                if (i >= nec)
                {

                    //
                    // Inequality constraints are skipped if we too far away from
                    // the boundary.
                    //
                    rowscale = 0.0;
                    v = -state.cleic[i, n];
                    for (j = 0; j <= n - 1; j++)
                    {
                        v = v + state.cleic[i, j] * state.xc[j];
                        rowscale = Math.Max(rowscale, Math.Abs(state.cleic[i, j] * state.s[j]));
                    }
                    if (v <= -(1.0E5 * MathExtensions.MachineEpsilon * rowscale))
                    {

                        //
                        // NOTE: it is important to check for non-strict inequality
                        //       because we have to correctly handle zero constraint
                        //       0*x<=0
                        //
                        continue;
                    }
                }
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    state.rctmpdense0[i_, nactivelin] = state.cleic[i, i_];
                }
                state.rctmpconstraintidx[nactiveconstraints] = n + i;
                state.activeset[n + i] = 0;
                state.rctmpisequality[nactiveconstraints] = i < nec;
                nactiveconstraints = nactiveconstraints + 1;
                nactivelin = nactivelin + 1;
            }

            //
            // Skip if no "candidate" constraints was found
            //
            if (nactiveconstraints == 0)
            {
                for (i = 0; i <= n - 1; i++)
                {
                    if ((state.hasbndl[i] && state.hasbndu[i]) && state.bndl[i] == state.bndu[i])
                    {
                        state.activeset[i] = 1;
                        continue;
                    }
                    if ((state.hasbndl[i] && state.xc[i] == state.bndl[i]) && gc[i] >= 0)
                    {
                        state.activeset[i] = 1;
                        continue;
                    }
                    if ((state.hasbndu[i] && state.xc[i] == state.bndu[i]) && gc[i] <= 0)
                    {
                        state.activeset[i] = 1;
                        continue;
                    }
                }
                return;
            }

            //
            // General case.
            //
            // APPROACH TO CONSTRAINTS ACTIVATION/DEACTIVATION
            //
            // We have NActiveConstraints "candidates": NActiveBnd boundary candidates,
            // NActiveLin linear candidates. Indexes of boundary constraints are stored
            // in RCTmpConstraintIdx[0:NActiveBnd-1], indexes of linear ones are stored
            // in RCTmpConstraintIdx[NActiveBnd:NActiveBnd+NActiveLin-1]. Some of the
            // constraints are equality ones, some are inequality - as specified by 
            // RCTmpIsEquality[i].
            //
            // Now we have to determine active subset of "candidates" set. In order to
            // do so we solve following constrained minimization problem:
            //         (                         )^2
            //     min ( SUM(lambda[i]*A[i]) + G )
            //         (                         )
            // Here:
            // * G is a gradient (column vector)
            // * A[i] is a column vector, linear (left) part of I-th constraint.
            //   I=0..NActiveConstraints-1, first NActiveBnd elements of A are just
            //   subset of identity matrix (boundary constraints), next NActiveLin
            //   elements are subset of rows of the matrix of general linear constraints.
            // * lambda[i] is a Lagrange multiplier corresponding to I-th constraint
            //
            // NOTE: for preconditioned setting A is replaced by A*H^(-0.5), G is
            //       replaced by G*H^(-0.5). We apply this scaling at the last stage,
            //       before passing data to NNLS solver.
            //
            // Minimization is performed subject to non-negativity constraints on
            // lambda[i] corresponding to inequality constraints. Inequality constraints
            // which correspond to non-zero lambda are activated, equality constraints
            // are always considered active.
            //
            // Informally speaking, we "decompose" descent direction -G and represent
            // it as sum of constraint vectors and "residual" part (which is equal to
            // the actual descent direction subject to constraints).
            //
            // SOLUTION OF THE NNLS PROBLEM
            //
            // We solve this optimization problem with Non-Negative Least Squares solver,
            // which can efficiently solve least squares problems of the form
            //
            //         ( [ I | AU ]     )^2
            //     min ( [   |    ]*x-b )   s.t. non-negativity constraints on some x[i]
            //         ( [ 0 | AL ]     )
            //
            // In order to use this solver we have to rearrange rows of A[] and G in
            // such way that first NActiveBnd columns of A store identity matrix (before
            // sorting non-zero elements are randomly distributed in the first NActiveBnd
            // columns of A, during sorting we move them to first NActiveBnd rows).
            //
            // Then we create instance of NNLS solver (we reuse instance left from the
            // previous run of the optimization problem) and solve NNLS problem.
            //
            idx0 = 0;
            idx1 = nactivebnd;
            for (i = 0; i <= n - 1; i++)
            {
                if (state.activeset[i] >= 0)
                {
                    v = 1 / Math.Sqrt(h[i]);
                    for (j = 0; j <= nactivelin - 1; j++)
                    {
                        state.rctmpdense1[idx0, j] = state.rctmpdense0[i, j] / state.rctmps[i] * v;
                    }
                    state.rctmprightpart[idx0] = state.rctmpg[i] / state.rctmps[i] * v;
                    idx0 = idx0 + 1;
                }
                else
                {
                    v = 1 / Math.Sqrt(h[i]);
                    for (j = 0; j <= nactivelin - 1; j++)
                    {
                        state.rctmpdense1[idx1, j] = state.rctmpdense0[i, j] * v;
                    }
                    state.rctmprightpart[idx1] = state.rctmpg[i] * v;
                    idx1 = idx1 + 1;
                }
            }
            SnnlsSolver.Initialize(n, nec + nic, n, state.solver);
            SnnlsSolver.SetProblem(state.solver, state.rctmpdense1, state.rctmprightpart, nactivebnd, nactiveconstraints - nactivebnd, n);
            for (i = 0; i <= nactiveconstraints - 1; i++)
            {
                if (state.rctmpisequality[i])
                {
                    SnnlsSolver.DropNonNegativityConstraint(state.solver, i);
                }
            }
            SnnlsSolver.Solve(state.solver, ref state.rctmplambdas);

            //
            // After solution of the problem we activate equality constraints (always active)
            // and inequality constraints with non-zero Lagrange multipliers. Then we reorthogonalize
            // active constraints.
            //
            for (i = 0; i <= nactiveconstraints - 1; i++)
            {
                if (state.rctmpisequality[i] || state.rctmplambdas[i] > 0)
                {
                    state.activeset[state.rctmpconstraintidx[i]] = 1;
                }
                else
                {
                    state.activeset[state.rctmpconstraintidx[i]] = 0;
                }
            }
            RebuildBasis(state);
        }

        
    };
}