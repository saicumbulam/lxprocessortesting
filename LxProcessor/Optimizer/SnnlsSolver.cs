using System;
using System.Collections.Generic;
using LxCommon.DataStructures;
using LxCommon.Utilities;
using LxProcessor.Common;

namespace LxProcessor.Optimizer
{

    /*************************************************************************
    This structure is a SNNLS (Specialized Non-Negative Least Squares) solver.

    It solves problems of the form |A*x-b|^2 => min subject to  non-negativity
    constraints on SOME components of x, with structured A (first  NS  columns
    are just unit matrix, next ND columns store dense part).

    This solver is suited for solution of many sequential NNLS  subproblems  -
    it keeps track of previously allocated memory and reuses  it  as  much  as
    possible.
    *************************************************************************/
    public class SnnlsSolver 
    {
        public int ns;
        public int nd;
        public int nr;
        public Matrix<double> densea;
        public FluxList<double> b;
        public FluxList<bool> nnc;
        public int refinementits;
        public double debugflops;
        public int debugmaxnewton;
        public FluxList<double> xn;
        public Matrix<double> tmpz;
        public Matrix<double> tmpca;
        public FluxList<double> g;
        public FluxList<double> d;
        public FluxList<double> dx;
        public FluxList<double> diagaa;
        public FluxList<double> cb;
        public FluxList<double> cx;
        public FluxList<double> cborg;
        public FluxList<int> columnmap;
        public FluxList<int> rowmap;
        public FluxList<double> tmpcholesky;
        public FluxList<double> r;

        public SnnlsSolver()
        {
            densea = new Matrix<double>();
            b = new FluxList<double>();
            nnc = new FluxList<bool>();
            xn = new FluxList<double>();
            tmpz = new Matrix<double>(0, 0);
            tmpca = new Matrix<double>(0, 0);
            g = new FluxList<double>();
            d = new FluxList<double>();
            dx = new FluxList<double>();
            diagaa = new FluxList<double>();
            cb = new FluxList<double>();
            cx = new FluxList<double>();
            cborg = new FluxList<double>();
            columnmap = new FluxList<int>();
            rowmap = new FluxList<int>();
            tmpcholesky = new FluxList<double>();
            r = new FluxList<double>();
        }

        internal void CleanForReuse()
        {
            ns = default(int);
            nd = default(int);
            nr = default(int);
            densea.Clear();
            b.Clear();
            nnc.Clear();
            refinementits = default(int);
            debugflops = default(double);
            debugmaxnewton = default(int);
            xn.Clear();
            tmpz.Clear();
            tmpca.Clear();
            g.Clear();
            d.Clear();
            dx.Clear();
            diagaa.Clear();
            cb.Clear();
            cx.Clear();
            cborg.Clear();
            columnmap.Clear();
            rowmap.Clear();
            tmpcholesky.Clear();
            r.Clear();
        }


        private const int iterativerefinementits = 3;

        /*************************************************************************
        This subroutine is used to initialize SNNLS solver.

        By default, empty NNLS problem is produced, but we allocated enough  space
        to store problems with NSMax+NDMax columns and  NRMax  rows.  It  is  good
        place to provide algorithm with initial estimate of the space requirements,
        although you may underestimate problem size or even pass zero estimates  -
        in this case buffer variables will be resized automatically  when  you set
        NNLS problem.

        Previously allocated buffer variables are reused as much as possible. This
        function does not clear structure completely, it tries to preserve as much
        dynamically allocated memory as possible.

        -- ALGLIB --
        Copyright 10.10.2012 by Bochkanov Sergey
    *************************************************************************/
        public static void Initialize(int nsmax,
            int ndmax,
            int nrmax,
            SnnlsSolver s)
        {
            s.ns = 0;
            s.nd = 0;
            s.nr = 0;
            s.densea.Resize(nrmax, ndmax);
            s.tmpca.Resize(nrmax, ndmax);
            s.tmpz.Resize(ndmax, ndmax);
            s.b.Resize(nrmax);
            s.nnc.Resize(nsmax + ndmax);
            s.debugflops = 0.0;
            s.debugmaxnewton = 0;
            s.refinementits = iterativerefinementits;
        }


        /*************************************************************************
           This subroutine is used to set NNLS problem:

                   ( [ 1     |      ]   [   ]   [   ] )^2
                   ( [   1   |      ]   [   ]   [   ] )
               min ( [     1 |  Ad  ] * [ x ] - [ b ] )    s.t. x>=0
                   ( [       |      ]   [   ]   [   ] )
                   ( [       |      ]   [   ]   [   ] )

           where:
           * identity matrix has NS*NS size (NS<=NR, NS can be zero)
           * dense matrix Ad has NR*ND size
           * b is NR*1 vector
           * x is (NS+ND)*1 vector
           * all elements of x are non-negative (this constraint can be removed later
             by calling SNNLSDropNNC() function)

           Previously allocated buffer variables are reused as much as possible.
           After you set problem, you can solve it with SNNLSSolve().

           INPUT PARAMETERS:
               S   -   SNNLS solver, must be initialized with SNNLSInit() call
               A   -   array[NR,ND], dense part of the system
               B   -   array[NR], right part
               NS  -   size of the sparse part of the system, 0<=NS<=NR
               ND  -   size of the dense part of the system, ND>=0
               NR  -   rows count, NR>0

           NOTE:
               1. You can have NS+ND=0, solver will correctly accept such combination
                  and return empty array as problem solution.
            
             -- ALGLIB --
                Copyright 10.10.2012 by Bochkanov Sergey
           *************************************************************************/
        public static void SetProblem(SnnlsSolver s, Matrix<double> a, FluxList<double> b, int ns, int nd, int nr)
        {
            int i = 0;
            int i_ = 0;

            Utilities.Assert(nd >= 0, "SNNLSSetProblem: ND<0");
            Utilities.Assert(ns >= 0, "SNNLSSetProblem: NS<0");
            Utilities.Assert(nr > 0, "SNNLSSetProblem: NR<=0");
            Utilities.Assert(ns <= nr, "SNNLSSetProblem: NS>NR");
            Utilities.Assert(a.Rows >= nr || nd == 0, "SNNLSSetProblem: rows(A)<NR");
            Utilities.Assert(a.Columns >= nd, "SNNLSSetProblem: cols(A)<ND");
            Utilities.Assert(b.Count >= nr, "SNNLSSetProblem: length(B)<NR");
            Utilities.Assert(Utilities.IsFiniteMatrix(a, nr, nd), "SNNLSSetProblem: A contains INF/NAN");
            Utilities.Assert(Utilities.IsFiniteVector(b, nr), "SNNLSSetProblem: B contains INF/NAN");

            //
            // Copy problem
            //
            s.ns = ns;
            s.nd = nd;
            s.nr = nr;
            if (nd > 0)
            {
                s.densea.SetLengthAtLeast(nr, nd);
                for (i = 0; i <= nr - 1; i++)
                {
                    for (i_ = 0; i_ <= nd - 1; i_++)
                    {
                        s.densea[i, i_] = a[i, i_];
                    }
                }
            }
            s.b.SetLengthAtLeast(nr);
            for (i_ = 0; i_ <= nr - 1; i_++)
            {
                s.b[i_] = b[i_];
            }
             s.nnc.SetLengthAtLeast(ns + nd);
            for (i = 0; i <= ns + nd - 1; i++)
            {
                s.nnc[i] = true;
            }
        }


        /*************************************************************************
            This subroutine drops non-negativity constraint from the  problem  set  by
            SNNLSSetProblem() call. This function must be called AFTER problem is set,
            because each SetProblem() call resets constraints to their  default  state
            (all constraints are present).

            INPUT PARAMETERS:
                S   -   SNNLS solver, must be initialized with SNNLSInit() call,
                        problem must be set with SNNLSSetProblem() call.
                Idx -   constraint index, 0<=IDX<NS+ND
            
              -- ALGLIB --
                 Copyright 10.10.2012 by Bochkanov Sergey
            *************************************************************************/
        public static void DropNonNegativityConstraint(SnnlsSolver s, int idx)
        {
            Utilities.Assert(idx >= 0, "SNNLSDropNNC: Idx<0");
            Utilities.Assert(idx < s.ns + s.nd, "SNNLSDropNNC: Idx>=NS+ND");
            s.nnc[idx] = false;
        }

        /*************************************************************************
           This subroutine is used to solve NNLS problem.

           INPUT PARAMETERS:
               S   -   SNNLS solver, must be initialized with SNNLSInit() call and
                       problem must be set up with SNNLSSetProblem() call.
               X   -   possibly preallocated buffer, automatically resized if needed

           OUTPUT PARAMETERS:
               X   -   array[NS+ND], solution
            
           NOTE:
               1. You can have NS+ND=0, solver will correctly accept such combination
                  and return empty array as problem solution.
            
               2. Internal field S.DebugFLOPS contains rough estimate of  FLOPs  used
                  to solve problem. It can be used for debugging purposes. This field
                  is real-valued.
            
             -- ALGLIB --
                Copyright 10.10.2012 by Bochkanov Sergey
           *************************************************************************/
        public static void Solve(SnnlsSolver s, ref FluxList<double> x)
        {
            int i = 0;
            int j = 0;
            int ns = 0;
            int nd = 0;
            int nr = 0;
            int nsc = 0;
            int ndc = 0;
            int newtoncnt = 0;
            bool terminationneeded = new bool();
            double eps = 0;
            double fcur = 0;
            double fprev = 0;
            double fcand = 0;
            double noiselevel = 0;
            double noisetolerance = 0;
            double stplen = 0;
            double d2 = 0;
            double d1 = 0;
            double d0 = 0;
            bool wasactivation = new bool();
            int rfsits = 0;
            double lambdav = 0;
            double v0 = 0;
            double v1 = 0;
            double v = 0;
            int i_ = 0;
            int i1_ = 0;


            //
            // Prepare
            //
            ns = s.ns;
            nd = s.nd;
            nr = s.nr;
            s.debugflops = 0.0;

            //
            // Handle special cases:
            // * NS+ND=0
            // * ND=0
            //
            if (ns + nd == 0)
            {
                return;
            }
            if (nd == 0)
            {
                x.SetLengthAtLeast(ns);
                for (i = 0; i <= ns - 1; i++)
                {
                    x[i] = s.b[i];
                    if (s.nnc[i])
                    {
                        x[i] = Math.Max(x[i], 0.0);
                    }
                }
                return;
            }

            //
            // Main cycle of BLEIC-SNNLS algorithm.
            // Below we assume that ND>0.
            //
            x.SetLengthAtLeast(ns + nd);
            s.xn.SetLengthAtLeast(ns + nd);
            s.g.SetLengthAtLeast(ns + nd);
            s.d.SetLengthAtLeast(ns + nd);
            s.r.SetLengthAtLeast(nr);
            s.diagaa.SetLengthAtLeast(nd);
            s.dx.SetLengthAtLeast(ns + nd);
            for (i = 0; i <= ns + nd - 1; i++)
            {
                x[i] = 0.0;
            }
            eps = 2 * MathExtensions.MachineEpsilon;
            noisetolerance = 10.0;
            lambdav = 1.0E6 * MathExtensions.MachineEpsilon;
            newtoncnt = 0;
            while (true)
            {

                //
                // Phase 1: perform steepest descent step.
                //
                // TerminationNeeded control variable is set on exit from this loop:
                // * TerminationNeeded=False in case we have to proceed to Phase 2 (Newton step)
                // * TerminationNeeded=True in case we found solution (step along projected gradient is small enough)
                //
                // Temporaries used:
                // * R      (I|A)*x-b
                //
                // NOTE 1. It is assumed that initial point X is feasible. This feasibility
                //         is retained during all iterations.
                //
                terminationneeded = false;
                while (true)
                {

                    //
                    // Calculate gradient G and constrained descent direction D
                    //
                    for (i = 0; i <= nr - 1; i++)
                    {
                        i1_ = (ns) - (0);
                        v = 0.0;
                        for (i_ = 0; i_ <= nd - 1; i_++)
                        {
                            v += s.densea[i, i_] * x[i_ + i1_];
                        }
                        if (i < ns)
                        {
                            v = v + x[i];
                        }
                        s.r[i] = v - s.b[i];
                    }
                    for (i = 0; i <= ns - 1; i++)
                    {
                        s.g[i] = s.r[i];
                    }
                    for (i = ns; i <= ns + nd - 1; i++)
                    {
                        s.g[i] = 0.0;
                    }
                    for (i = 0; i <= nr - 1; i++)
                    {
                        v = s.r[i];
                        i1_ = (0) - (ns);
                        for (i_ = ns; i_ <= ns + nd - 1; i_++)
                        {
                            s.g[i_] = s.g[i_] + v * s.densea[i, i_ + i1_];
                        }
                    }
                    for (i = 0; i <= ns + nd - 1; i++)
                    {
                        if ((s.nnc[i] && x[i] <= 0) && s.g[i] > 0)
                        {
                            s.d[i] = 0.0;
                        }
                        else
                        {
                            s.d[i] = -s.g[i];
                        }
                    }
                    s.debugflops = s.debugflops + 2 * 2 * nr * nd;

                    //
                    // Build quadratic model of F along descent direction:
                    //     F(x+alpha*d) = D2*alpha^2 + D1*alpha + D0
                    //
                    // Estimate numerical noise in the X (noise level is used
                    // to classify step as singificant or insignificant). Noise
                    // comes from two sources:
                    // * noise when calculating rows of (I|A)*x
                    // * noise when calculating norm of residual
                    //
                    // In case function curvature is negative or product of descent
                    // direction and gradient is non-negative, iterations are terminated.
                    //
                    // NOTE: D0 is not actually used, but we prefer to maintain it.
                    //
                    fprev = 0.0;
                    for (i_ = 0; i_ <= nr - 1; i_++)
                    {
                        fprev += s.r[i_] * s.r[i_];
                    }
                    fprev = fprev / 2;
                    noiselevel = 0.0;
                    for (i = 0; i <= nr - 1; i++)
                    {

                        //
                        // Estimate noise introduced by I-th row of (I|A)*x
                        //
                        v = 0.0;
                        if (i < ns)
                        {
                            v = eps * x[i];
                        }
                        for (j = 0; j <= nd - 1; j++)
                        {
                            v = Math.Max(v, eps * Math.Abs(s.densea[i, j] * x[ns + j]));
                        }
                        v = 2 * Math.Abs(s.r[i] * v) + v * v;

                        //
                        // Add to summary noise in the model
                        //
                        noiselevel = noiselevel + v;
                    }
                    noiselevel = Math.Max(noiselevel, eps * fprev);
                    d2 = 0.0;
                    for (i = 0; i <= nr - 1; i++)
                    {
                        i1_ = (ns) - (0);
                        v = 0.0;
                        for (i_ = 0; i_ <= nd - 1; i_++)
                        {
                            v += s.densea[i, i_] * s.d[i_ + i1_];
                        }
                        if (i < ns)
                        {
                            v = v + s.d[i];
                        }
                        d2 = d2 + 0.5 * MathExtensions.Squared(v);
                    }
                    v = 0.0;
                    for (i_ = 0; i_ <= ns + nd - 1; i_++)
                    {
                        v += s.d[i_] * s.g[i_];
                    }
                    d1 = v;
                    d0 = fprev;
                    if (d2 <= 0 || d1 >= 0)
                    {
                        terminationneeded = true;
                        break;
                    }
                    s.debugflops = s.debugflops + 2 * nr * nd;
                    //apserv.touchreal(ref d0);

                    //
                    // Perform full (unconstrained) step with length StpLen in direction D.
                    //
                    // We can terminate iterations in case one of two criteria is met:
                    // 1. function change is dominated by noise (or function actually increased
                    //    instead of decreasing)
                    // 2. relative change in X is small enough
                    //
                    // First condition is not enough to guarantee algorithm termination because
                    // sometimes our noise estimate is too optimistic (say, in situations when
                    // function value at solition is zero).
                    //
                    stplen = -(d1 / (2 * d2));
                    for (i_ = 0; i_ <= ns + nd - 1; i_++)
                    {
                        s.xn[i_] = x[i_];
                    }
                    for (i_ = 0; i_ <= ns + nd - 1; i_++)
                    {
                        s.xn[i_] = s.xn[i_] + stplen * s.d[i_];
                    }
                    fcand = 0.0;
                    for (i = 0; i <= nr - 1; i++)
                    {
                        i1_ = (ns) - (0);
                        v = 0.0;
                        for (i_ = 0; i_ <= nd - 1; i_++)
                        {
                            v += s.densea[i, i_] * s.xn[i_ + i1_];
                        }
                        if (i < ns)
                        {
                            v = v + s.xn[i];
                        }
                        fcand = fcand + 0.5 * MathExtensions.Squared(v - s.b[i]);
                    }
                    s.debugflops = s.debugflops + 2 * nr * nd;
                    if (fcand >= fprev - noiselevel * noisetolerance)
                    {
                        terminationneeded = true;
                        break;
                    }
                    v = 0;
                    for (i = 0; i <= ns + nd - 1; i++)
                    {
                        v0 = Math.Abs(x[i]);
                        v1 = Math.Abs(s.xn[i]);
                        if (v0 != 0 || v1 != 0)
                        {
                            v = Math.Max(v, Math.Abs(x[i] - s.xn[i]) / Math.Max(v0, v1));
                        }
                    }
                    if (v <= eps * noisetolerance)
                    {
                        terminationneeded = true;
                        break;
                    }

                    //
                    // Perform step one more time, now with non-negativity constraints.
                    //
                    // NOTE: complicated code below which deals with VarIdx temporary makes
                    //       sure that in case unconstrained step leads us outside of feasible
                    //       area, we activate at least one constraint.
                    //
                    wasactivation = BoundedStepAndActivation(x, s.xn, s.nnc, ns + nd);
                    fcur = 0.0;
                    for (i = 0; i <= nr - 1; i++)
                    {
                        i1_ = (ns) - (0);
                        v = 0.0;
                        for (i_ = 0; i_ <= nd - 1; i_++)
                        {
                            v += s.densea[i, i_] * x[i_ + i1_];
                        }
                        if (i < ns)
                        {
                            v = v + x[i];
                        }
                        fcur = fcur + 0.5 * MathExtensions.Squared(v - s.b[i]);
                    }
                    s.debugflops = s.debugflops + 2 * nr * nd;

                    //
                    // Depending on results, decide what to do:
                    // 1. In case step was performed without activation of constraints,
                    //    we proceed to Newton method
                    // 2. In case there was activated at least one constraint, we repeat
                    //    steepest descent step.
                    //
                    if (!wasactivation)
                    {

                        //
                        // Step without activation, proceed to Newton
                        //
                        break;
                    }
                }
                if (terminationneeded)
                {
                    break;
                }

                //
                // Phase 2: Newton method.
                //
                s.cx.SetLengthAtLeast(ns + nd);
                s.columnmap.SetLengthAtLeast(ns + nd);
                s.rowmap.SetLengthAtLeast(nr);
                s.tmpca.SetLengthAtLeast(nr, nd);
                s.tmpz.SetLengthAtLeast(nd, nd);
                s.cborg.SetLengthAtLeast(nr);
                s.cb.SetLengthAtLeast(nr);
                terminationneeded = false;
                while (true)
                {

                    //
                    // Prepare equality constrained subproblem with NSC<=NS "sparse"
                    // variables and NDC<=ND "dense" variables.
                    //
                    // First, we reorder variables (columns) and move all unconstrained
                    // variables "to the left", ColumnMap stores this permutation.
                    //
                    // Then, we reorder first NS rows of A and first NS elements of B in
                    // such way that we still have identity matrix in first NSC columns
                    // of problem. This permutation is stored in RowMap.
                    //
                    nsc = 0;
                    ndc = 0;
                    for (i = 0; i <= ns - 1; i++)
                    {
                        if (!(s.nnc[i] && x[i] == 0))
                        {
                            s.columnmap[nsc] = i;
                            nsc = nsc + 1;
                        }
                    }
                    for (i = ns; i <= ns + nd - 1; i++)
                    {
                        if (!(s.nnc[i] && x[i] == 0))
                        {
                            s.columnmap[nsc + ndc] = i;
                            ndc = ndc + 1;
                        }
                    }
                    for (i = 0; i <= nsc - 1; i++)
                    {
                        s.rowmap[i] = s.columnmap[i];
                    }
                    j = nsc;
                    for (i = 0; i <= ns - 1; i++)
                    {
                        if (s.nnc[i] && x[i] == 0)
                        {
                            s.rowmap[j] = i;
                            j = j + 1;
                        }
                    }
                    for (i = ns; i <= nr - 1; i++)
                    {
                        s.rowmap[i] = i;
                    }

                    //
                    // Now, permutations are ready, and we can copy/reorder
                    // A, B and X to CA, CB and CX.
                    //
                    for (i = 0; i <= nsc + ndc - 1; i++)
                    {
                        s.cx[i] = x[s.columnmap[i]];
                    }
                    for (i = 0; i <= nr - 1; i++)
                    {
                        for (j = 0; j <= ndc - 1; j++)
                        {
                            s.tmpca[i, j] = s.densea[s.rowmap[i], s.columnmap[nsc + j] - ns];
                        }
                        s.cb[i] = s.b[s.rowmap[i]];
                    }

                    //
                    // Solve equality constrained subproblem.
                    //
                    if (ndc > 0)
                    {

                        //
                        // NDC>0.
                        //
                        // Solve subproblem using Newton-type algorithm. We have a
                        // NR*(NSC+NDC) linear least squares subproblem
                        //
                        //         | ( I  AU )   ( XU )   ( BU ) |^2
                        //     min | (       ) * (    ) - (    ) |
                        //         | ( 0  AL )   ( XL )   ( BL ) |
                        //
                        // where:
                        // * I is a NSC*NSC identity matrix
                        // * AU is NSC*NDC dense matrix (first NSC rows of CA)
                        // * AL is (NR-NSC)*NDC dense matrix (next NR-NSC rows of CA)
                        // * BU and BL are correspondingly sized parts of CB
                        //
                        // After conversion to normal equations and small regularization,
                        // we get:
                        //
                        //     ( I   AU ) (  XU )   ( BU            )
                        //     (        )*(     ) = (               )
                        //     ( AU' Y  ) (  XL )   ( AU'*BU+AL'*BL )
                        //
                        // where Y = AU'*AU + AL'*AL + lambda*diag(AU'*AU+AL'*AL).
                        //
                        // With Schur Complement Method this system can be solved in
                        // O(NR*NDC^2+NDC^3) operations. In order to solve it we multiply
                        // first row by AU' and subtract it from the second one. As result,
                        // we get system
                        //
                        //     Z*XL = AL'*BL, where Z=AL'*AL+lambda*diag(AU'*AU+AL'*AL)
                        //
                        // We can easily solve it for XL, and we can get XU as XU = BU-AU*XL.
                        //
                        // We will start solution from calculating Cholesky decomposition of Z.
                        //
                        for (i = 0; i <= nr - 1; i++)
                        {
                            s.cborg[i] = s.cb[i];
                        }
                        for (i = 0; i <= ndc - 1; i++)
                        {
                            s.diagaa[i] = 0;
                        }
                        for (i = 0; i <= nr - 1; i++)
                        {
                            for (j = 0; j <= ndc - 1; j++)
                            {
                                s.diagaa[j] = s.diagaa[j] + MathExtensions.Squared(s.tmpca[i, j]);
                            }
                        }
                        for (j = 0; j <= ndc - 1; j++)
                        {
                            if (s.diagaa[j] == 0)
                            {
                                s.diagaa[j] = 1;
                            }
                        }
                        while (true)
                        {

                            //
                            // NOTE: we try to factorize Z. In case of failure we increase
                            //       regularization parameter and try again.
                            //
                            s.debugflops = s.debugflops + 2 * (nr - nsc) * MathExtensions.Squared(ndc) + Math.Pow(ndc, 3) / 3;
                            for (i = 0; i <= ndc - 1; i++)
                            {
                                for (j = 0; j <= ndc - 1; j++)
                                {
                                    s.tmpz[i, j] = 0.0;
                                }
                            }
                            Ablas.RealSyrk(ndc, nr - nsc, 1.0, s.tmpca, nsc, 0, 2, 0.0, s.tmpz, 0, 0, true);
                            for (i = 0; i <= ndc - 1; i++)
                            {
                                s.tmpz[i, i] = s.tmpz[i, i] + lambdav * s.diagaa[i];
                            }
                            if (Trfac.CholeskyRecursive(ref s.tmpz, 0, ndc, true, ref s.tmpcholesky))
                            {
                                break;
                            }
                            lambdav = lambdav * 10;
                        }

                        //
                        // We have Cholesky decomposition of Z, now we can solve system:
                        // * we start from initial point CX
                        // * we perform several iterations of refinement:
                        //   * BU_new := BU_orig - XU_cur - AU*XL_cur
                        //   * BL_new := BL_orig - AL*XL_cur
                        //   * solve for BU_new/BL_new, obtain solution dx
                        //   * XU_cur := XU_cur + dx_u
                        //   * XL_cur := XL_cur + dx_l
                        // * BU_new/BL_new are stored in CB, original right part is
                        //   stored in CBOrg, correction to X is stored in DX, current
                        //   X is stored in CX
                        //
                        for (rfsits = 1; rfsits <= s.refinementits; rfsits++)
                        {
                            for (i = 0; i <= nr - 1; i++)
                            {
                                i1_ = (nsc) - (0);
                                v = 0.0;
                                for (i_ = 0; i_ <= ndc - 1; i_++)
                                {
                                    v += s.tmpca[i, i_] * s.cx[i_ + i1_];
                                }
                                s.cb[i] = s.cborg[i] - v;
                                if (i < nsc)
                                {
                                    s.cb[i] = s.cb[i] - s.cx[i];
                                }
                            }
                            s.debugflops = s.debugflops + 2 * nr * ndc;
                            for (i = 0; i <= ndc - 1; i++)
                            {
                                s.dx[i] = 0.0;
                            }
                            for (i = nsc; i <= nr - 1; i++)
                            {
                                v = s.cb[i];
                                for (i_ = 0; i_ <= ndc - 1; i_++)
                                {
                                    s.dx[i_] = s.dx[i_] + v * s.tmpca[i, i_];
                                }
                            }
                            Fbls.CholeskySolver(s.tmpz, 1.0, ndc, true, ref s.dx, ref s.tmpcholesky);
                            s.debugflops = s.debugflops + 2 * ndc * ndc;
                            i1_ = (0) - (nsc);
                            for (i_ = nsc; i_ <= nsc + ndc - 1; i_++)
                            {
                                s.cx[i_] = s.cx[i_] + s.dx[i_ + i1_];
                            }
                            for (i = 0; i <= nsc - 1; i++)
                            {
                                v = 0.0;
                                for (i_ = 0; i_ <= ndc - 1; i_++)
                                {
                                    v += s.tmpca[i, i_] * s.dx[i_];
                                }
                                s.cx[i] = s.cx[i] + s.cb[i] - v;
                            }
                            s.debugflops = s.debugflops + 2 * nsc * ndc;
                        }
                    }
                    else
                    {

                        //
                        // NDC=0.
                        //
                        // We have a NR*NSC linear least squares subproblem
                        //
                        //     min |XU-BU|^2
                        //
                        // solution is easy to find - it is XU=BU!
                        //
                        for (i = 0; i <= nsc - 1; i++)
                        {
                            s.cx[i] = s.cb[i];
                        }
                    }
                    for (i = 0; i <= ns + nd - 1; i++)
                    {
                        s.xn[i] = x[i];
                    }
                    for (i = 0; i <= nsc + ndc - 1; i++)
                    {
                        s.xn[s.columnmap[i]] = s.cx[i];
                    }
                    newtoncnt = newtoncnt + 1;

                    //
                    // Step to candidate point.
                    // If no constraints was added, accept candidate point XN and move to next phase.
                    // Terminate, if number of Newton iterations exceeded DebugMaxNewton counter.
                    //
                    terminationneeded = s.debugmaxnewton > 0 && newtoncnt >= s.debugmaxnewton;
                    if (!BoundedStepAndActivation(x, s.xn, s.nnc, ns + nd))
                    {
                        break;
                    }
                    if (terminationneeded)
                    {
                        break;
                    }
                }
                if (terminationneeded)
                {
                    break;
                }
            }
        }

        /*************************************************************************
         Having feasible current point XC and possibly infeasible candidate   point
         XN,  this  function  performs  longest  step  from  XC to XN which retains
         feasibility. In case XN is found to be infeasible, at least one constraint
         is activated.

         For example, if we have:
           XC=0.5
           XN=-1.2
           x>=0
         then this function will move us to X=0 and activate constraint "x>=0".

         INPUT PARAMETERS:
             XC      -   current point, must be feasible with respect to
                         all constraints
             XN      -   candidate point, can be infeasible with respect to some
                         constraints
             NNC     -   NNC[i] is True when I-th variable is non-negatively
                         constrained
             N       -   variable count

         OUTPUT PARAMETERS:
             XC      -   new position

         RESULT:
             True in case at least one constraint was activated by step

           -- ALGLIB --
              Copyright 19.10.2012 by Bochkanov Sergey
         *************************************************************************/
        private static bool BoundedStepAndActivation(FluxList<double> xc,
            FluxList<double> xn,
            FluxList<bool> nnc,
            int n)
        {
            bool result = new bool();
            int i = 0;
            int varidx = 0;
            double vmax = 0;
            double v = 0;
            double stplen = 0;


            //
            // Check constraints.
            //
            // NOTE: it is important to test for XN[i]<XC[i] (strict inequality,
            //       allows to handle correctly situations with XC[i]=0 without
            //       activating already active constraints), but to check for
            //       XN[i]<=0 (non-strict inequality, correct handling of some
            //       special cases when unconstrained step ends at the boundary).
            //
            result = false;
            varidx = -1;
            vmax = MathExtensions.MaxRealNumber;
            for (i = 0; i <= n - 1; i++)
            {
                if ((nnc[i] && xn[i] < xc[i]) && xn[i] <= 0.0)
                {
                    v = vmax;
                    vmax = Utilities.SafeMin(xc[i], xc[i] - xn[i], vmax);
                    if (vmax < v)
                    {
                        varidx = i;
                    }
                }
            }
            stplen = Math.Min(vmax, 1.0);

            //
            // Perform step with activation.
            //
            // NOTE: it is important to use (1-StpLen)*XC + StpLen*XN because
            //       it allows us to step exactly to XN when StpLen=1, even in
            //       the presence of numerical errors.
            //
            for (i = 0; i <= n - 1; i++)
            {
                xc[i] = (1 - stplen) * xc[i] + stplen * xn[i];
            }
            if (varidx >= 0)
            {
                xc[varidx] = 0.0;
                result = true;
            }
            for (i = 0; i <= n - 1; i++)
            {
                if (nnc[i] && xc[i] < 0.0)
                {
                    xc[i] = 0.0;
                    result = true;
                }
            }
            return result;
        }

     
    };
}