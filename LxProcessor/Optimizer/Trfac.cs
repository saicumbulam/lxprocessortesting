using System;
using System.Collections.Generic;
using LxCommon.DataStructures;
using LxCommon.Utilities;
using LxProcessor.Common;

namespace LxProcessor.Optimizer
{
    public class Trfac
    {
        
        /*************************************************************************
            Recursive computational subroutine for SPDMatrixCholesky.

            INPUT PARAMETERS:
                A       -   matrix given by upper or lower triangle
                Offs    -   offset of diagonal block to decompose
                N       -   diagonal block size
                IsUpper -   what half is given
                Tmp     -   temporary array; allocated by function, if its size is too
                            small; can be reused on subsequent calls.
                        
            OUTPUT PARAMETERS:
                A       -   upper (or lower) triangle contains Cholesky decomposition

            RESULT:
                True, on success
                False, on failure

              -- ALGLIB routine --
                 15.12.2009
                 Bochkanov Sergey
            *************************************************************************/
        public static bool CholeskyRecursive(ref Matrix<double> a,
            int offs,
            int n,
            bool isupper,
            ref FluxList<double> tmp)
        {
            bool result = new bool();
            int n1 = 0;
            int n2 = 0;


            //
            // check N
            //
            if (n < 1)
            {
                result = false;
                return result;
            }

            //
            // Prepare buffer
            //
            if (tmp.Count < 2 * n)
            {
                tmp.Resize(2 * n);
            }

            //
            // special cases
            //
            if (n == 1)
            {
                if (a[offs, offs] > 0)
                {
                    a[offs, offs] = Math.Sqrt(a[offs, offs]);
                    result = true;
                }
                else
                {
                    result = false;
                }
                return result;
            }
            if (n <= Ablas.BlockSize(a))
            {
                result = CholeskyLevel2(ref a, offs, n, isupper, ref tmp);
                return result;
            }

            //
            // general case: split task in cache-oblivious manner
            //
            result = true;
            Ablas.SplitLength(a, n, ref n1, ref n2);
            result = CholeskyRecursive(ref a, offs, n1, isupper, ref tmp);
            if (!result)
            {
                return result;
            }
            if (n2 > 0)
            {
                if (isupper)
                {
                    Ablas.RealLeftTrSm(n1, n2, a, offs, offs, isupper, false, 1, a, offs, offs + n1);
                    Ablas.RealSyrk(n2, n1, -1.0, a, offs, offs + n1, 1, 1.0, a, offs + n1, offs + n1, isupper);
                }
                else
                {
                    Ablas.RealRightTrsm(n2, n1, a, offs, offs, isupper, false, 1, a, offs + n1, offs);
                    Ablas.RealSyrk(n2, n1, -1.0, a, offs + n1, offs, 0, 1.0, a, offs + n1, offs + n1, isupper);
                }
                result = CholeskyRecursive(ref a, offs + n1, n2, isupper, ref tmp);
                if (!result)
                {
                    return result;
                }
            }
            return result;
        }


        /*************************************************************************
            Level-2 Cholesky subroutine

              -- LAPACK routine (version 3.0) --
                 Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
                 Courant Institute, Argonne National Lab, and Rice University
                 February 29, 1992
            *************************************************************************/
        private static bool CholeskyLevel2(ref Matrix<double> aaa,
            int offs,
            int n,
            bool isupper,
            ref FluxList<double> tmp)
        {
            bool result = new bool();
            int i = 0;
            int j = 0;
            double ajj = 0;
            double v = 0;
            double r = 0;
            int i_ = 0;
            int i1_ = 0;

            result = true;
            if (n < 0)
            {
                result = false;
                return result;
            }

            //
            // Quick return if possible
            //
            if (n == 0)
            {
                return result;
            }
            if (isupper)
            {

                //
                // Compute the Cholesky factorization A = U'*U.
                //
                for (j = 0; j <= n - 1; j++)
                {

                    //
                    // Compute U(J,J) and test for non-positive-definiteness.
                    //
                    v = 0.0;
                    for (i_ = offs; i_ <= offs + j - 1; i_++)
                    {
                        v += aaa[i_, offs + j] * aaa[i_, offs + j];
                    }
                    ajj = aaa[offs + j, offs + j] - v;
                    if (ajj <= 0)
                    {
                        aaa[offs + j, offs + j] = ajj;
                        result = false;
                        return result;
                    }
                    ajj = Math.Sqrt(ajj);
                    aaa[offs + j, offs + j] = ajj;

                    //
                    // Compute elements J+1:N-1 of row J.
                    //
                    if (j < n - 1)
                    {
                        if (j > 0)
                        {
                            i1_ = (offs) - (0);
                            for (i_ = 0; i_ <= j - 1; i_++)
                            {
                                tmp[i_] = -aaa[i_ + i1_, offs + j];
                            }
                            Ablas.RealVectorProduct(n - j - 1, j, aaa, offs, offs + j + 1, 1, tmp, 0, ref tmp, n);
                            i1_ = (n) - (offs + j + 1);
                            for (i_ = offs + j + 1; i_ <= offs + n - 1; i_++)
                            {
                                aaa[offs + j, i_] = aaa[offs + j, i_] + tmp[i_ + i1_];
                            }
                        }
                        r = 1 / ajj;
                        for (i_ = offs + j + 1; i_ <= offs + n - 1; i_++)
                        {
                            aaa[offs + j, i_] = r * aaa[offs + j, i_];
                        }
                    }
                }
            }
            else
            {

                //
                // Compute the Cholesky factorization A = L*L'.
                //
                for (j = 0; j <= n - 1; j++)
                {

                    //
                    // Compute L(J+1,J+1) and test for non-positive-definiteness.
                    //
                    v = 0.0;
                    for (i_ = offs; i_ <= offs + j - 1; i_++)
                    {
                        v += aaa[offs + j, i_] * aaa[offs + j, i_];
                    }
                    ajj = aaa[offs + j, offs + j] - v;
                    if (ajj <= 0)
                    {
                        aaa[offs + j, offs + j] = ajj;
                        result = false;
                        return result;
                    }
                    ajj = Math.Sqrt(ajj);
                    aaa[offs + j, offs + j] = ajj;

                    //
                    // Compute elements J+1:N of column J.
                    //
                    if (j < n - 1)
                    {
                        if (j > 0)
                        {
                            i1_ = (offs) - (0);
                            for (i_ = 0; i_ <= j - 1; i_++)
                            {
                                tmp[i_] = aaa[offs + j, i_ + i1_];
                            }
                            Ablas.RealVectorProduct(n - j - 1, j, aaa, offs + j + 1, offs, 0, tmp, 0, ref tmp, n);
                            for (i = 0; i <= n - j - 2; i++)
                            {
                                aaa[offs + j + 1 + i, offs + j] = (aaa[offs + j + 1 + i, offs + j] - tmp[n + i]) / ajj;
                            }
                        }
                        else
                        {
                            for (i = 0; i <= n - j - 2; i++)
                            {
                                aaa[offs + j + 1 + i, offs + j] = aaa[offs + j + 1 + i, offs + j] / ajj;
                            }
                        }
                    }
                }
            }
            return result;
        }
    }
}