namespace LxProcessor.Optimizer
{
    /*************************************************************************
          Buffers for internal functions which need buffers:
          * check for size of the buffer you want to use.
          * if buffer is too small, resize it; leave unchanged, if it is larger than
            needed.
          * use it.

          We can pass this structure to multiple functions;  after first run through
          functions buffer sizes will be finally determined,  and  on  a next run no
          allocation will be required.
          *************************************************************************/
    public class Buffers
    {
        public int[] ia0;
        public int[] ia1;
        public int[] ia2;
        public int[] ia3;
        public double[] ra0;
        public double[] ra1;
        public double[] ra2;
        public double[] ra3;
        public Buffers()
        {
            init();
        }
        public void init()
        {
            ia0 = new int[0];
            ia1 = new int[0];
            ia2 = new int[0];
            ia3 = new int[0];
            ra0 = new double[0];
            ra1 = new double[0];
            ra2 = new double[0];
            ra3 = new double[0];
        }
       
    }
}