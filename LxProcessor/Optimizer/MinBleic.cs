using System;
using System.Collections.Generic;
using LxCommon.DataStructures;
using LxCommon.Utilities;
using LxProcessor.Common;

namespace LxProcessor.Optimizer
{
    public class MinBleic
    {
        /*************************************************************************
            This object stores nonlinear optimizer state.
            You should use functions provided by MinBLEIC subpackage to work with this
            object
            *************************************************************************/
        public class State
        {
            public int nmain;
            public int nslack;
            public double epsg;
            public double epsf;
            public double epsx;
            public int maxits;
            public bool xrep;
            public bool drep;
            public double stpmax;
            public double diffstep;
            public ActiveSet sas;
            public FluxList<double> s;
            public int prectype;
            public FluxList<double> diagh;
            public FluxList<double> x;
            public double f;
            public FluxList<double> g;
            public bool needf;
            public bool needfg;
            public bool xupdated;
            public bool lsstart;
            public bool lbfgssearch;
            public bool boundedstep;
            public double teststep;
            public ReverseCommunicationStructure rstate;
            public FluxList<double> gc;
            public FluxList<double> xn;
            public FluxList<double> gn;
            public FluxList<double> xp;
            public FluxList<double> gp;
            public double fc;
            public double fn;
            public double fp;
            public FluxList<double> d;
            public Matrix<double> cleic;
            public int nec;
            public int nic;
            public double lastgoodstep;
            public double lastscaledgoodstep;
            public double maxscaledgrad;
            public FluxList<bool> hasbndl;
            public FluxList<bool> hasbndu;
            public FluxList<double> bndl;
            public FluxList<double> bndu;
            public int repinneriterationscount;
            public int repouteriterationscount;
            public int repnfev;
            public int repvaridx;
            public int repterminationtype;
            public double repdebugeqerr;
            public double repdebugfs;
            public double repdebugff;
            public double repdebugdx;
            public int repdebugfeasqpits;
            public int repdebugfeasgpaits;
            public FluxList<double> xstart;
            public SnnlsSolver solver;
            public double fbase;
            public double fm2;
            public double fm1;
            public double fp1;
            public double fp2;
            public double xm1;
            public double xp1;
            public double gm1;
            public double gp1;
            public int cidx;
            public double cval;
            public FluxList<double> tmpprec;
            public int nfev;
            public int mcstage;
            public double stp;
            public double curstpmax;
            public double activationstep;
            public FluxList<double> work;
            public LinMin.State lstate;
            public double trimthreshold;
            public int nonmonotoniccnt;
            public int k;
            public int q;
            public int p;
            public FluxList<double> rho;
            public Matrix<double> yk;
            public Matrix<double> sk;
            public FluxList<double> theta;
            public Matrix<double> c;
            public FluxList<int> ct;

            public State()
            {
                sas = new ActiveSet();
                s = new FluxList<double>();
                diagh = new FluxList<double>();
                x = new FluxList<double>();
                g = new FluxList<double>();
                rstate = new ReverseCommunicationStructure();
                gc = new FluxList<double>();
                xn = new FluxList<double>();
                gn = new FluxList<double>();
                xp = new FluxList<double>();
                gp = new FluxList<double>();
                d = new FluxList<double>();
                cleic = new Matrix<double>();
                hasbndl = new FluxList<bool>();
                hasbndu = new FluxList<bool>();
                bndl = new FluxList<double>();
                bndu = new FluxList<double>();
                xstart = new FluxList<double>();
                solver = new SnnlsSolver();
                tmpprec = new FluxList<double>();
                work = new FluxList<double>();
                lstate = new LinMin.State();
                rho = new FluxList<double>();
                yk = new Matrix<double>();
                sk = new Matrix<double>();
                theta = new FluxList<double>();
                c = new Matrix<double>();
                ct = new FluxList<int>();
            }

            internal void CleanForReuse()
            {
                nmain = default(int);
                nslack = default(int);
                epsg = default(double);
                epsf = default(double);
                epsx = default(double);
                maxits = default(int);
                xrep = default(bool);
                drep = default(bool);
                stpmax = default(double);
                diffstep = default(double);
                sas.CleanForReuse();
                s.Clear();
                prectype = default(int);
                diagh.Clear();
                x.Clear();
                f = default(double);
                g.Clear();
                needf = default(bool);
                needfg = default(bool);
                xupdated = default(bool);
                lsstart = default(bool);
                lbfgssearch = default(bool);
                boundedstep = default(bool);
                teststep = default(double);
                rstate.CleanForReuse();
                gc.Clear();
                xn.Clear();
                gn.Clear();
                xp.Clear();
                gp.Clear();
                fc = default(double);
                fn = default(double);
                fp = default(double);
                d.Clear();
                cleic.Clear();
                nec = default(int);
                nic = default(int);
                lastgoodstep = default(double);
                lastscaledgoodstep = default(double);
                maxscaledgrad = default(double);
                hasbndl.Clear();
                hasbndu.Clear();
                bndl.Clear();
                bndu.Clear();
                repinneriterationscount = default(int);
                repouteriterationscount = default(int);
                repnfev = default(int);
                repvaridx = default(int);
                repterminationtype = default(int);
                repdebugeqerr = default(double);
                repdebugfs = default(double);
                repdebugff = default(double);
                repdebugdx = default(double);
                repdebugfeasqpits = default(int);
                repdebugfeasgpaits = default(int);
                xstart.Clear();
                solver.CleanForReuse();
                fbase = default(double);
                fm2 = default(double);
                fm1 = default(double);
                fp1 = default(double);
                fp2 = default(double);
                xm1 = default(double);
                xp1 = default(double);
                gm1 = default(double);
                gp1 = default(double);
                cidx = default(int);
                cval = default(double);
                tmpprec.Clear();
                nfev = default(int);
                mcstage = default(int);
                stp = default(double);
                curstpmax = default(double);
                activationstep = default(double);
                work.Clear();
                lstate.CleanForReuse();
                trimthreshold = default(double);
                nonmonotoniccnt = default(int);
                k = default(int);
                q = default(int);
                p = default(int);
                rho.Clear();
                yk.Clear();
                sk.Clear();
                theta.Clear();
                c.Clear();
                ct.Clear();
            }
        };


        /*************************************************************************
            This structure stores optimization report:
            * IterationsCount           number of iterations
            * NFEV                      number of gradient evaluations
            * TerminationType           termination type (see below)

            TERMINATION CODES

            TerminationType field contains completion code, which can be:
              -7    gradient verification failed.
                    See MinBLEICSetGradientCheck() for more information.
              -3    inconsistent constraints. Feasible point is
                    either nonexistent or too hard to find. Try to
                    restart optimizer with better initial approximation
               1    relative function improvement is no more than EpsF.
               2    relative step is no more than EpsX.
               4    gradient norm is no more than EpsG
               5    MaxIts steps was taken
               7    stopping conditions are too stringent,
                    further improvement is impossible,
                    X contains best point found so far.

            ADDITIONAL FIELDS

            There are additional fields which can be used for debugging:
            * DebugEqErr                error in the equality constraints (2-norm)
            * DebugFS                   f, calculated at projection of initial point
                                        to the feasible set
            * DebugFF                   f, calculated at the final point
            * DebugDX                   |X_start-X_final|
            *************************************************************************/
        public class Report
        {
            public int iterationscount;
            public int nfev;
            public int varidx;
            public int terminationtype;
            public double debugeqerr;
            public double debugfs;
            public double debugff;
            public double debugdx;
            public int debugfeasqpits;
            public int debugfeasgpaits;
            public int inneriterationscount;
            public int outeriterationscount;
            
            public Report()
            {

            }

            internal void CleanForReuse()
            {
                iterationscount = default(int);
                nfev = default(int);
                varidx = default(int);
                terminationtype = default(int);
                debugeqerr = default(double);
                debugfs = default(double);
                debugff = default(double);
                debugdx = default(double);
                debugfeasqpits = default(int);
                debugfeasgpaits = default(int);
                inneriterationscount = default(int);
                outeriterationscount = default(int);
            }
        }




        private const double gtol = 0.4;
        private const double maxnonmonotoniclen = 1.0E-5;
        private const double initialdecay = 0.5;
        private const double mindecay = 0.1;
        private const double decaycorrection = 0.8;
        private const double penaltyfactor = 100;


        /*************************************************************************
                                 BOUND CONSTRAINED OPTIMIZATION
                   WITH ADDITIONAL LINEAR EQUALITY AND INEQUALITY CONSTRAINTS

            DESCRIPTION:
            The  subroutine  minimizes  function   F(x)  of N arguments subject to any
            combination of:
            * bound constraints
            * linear inequality constraints
            * linear equality constraints

            REQUIREMENTS:
            * user must provide function value and gradient
            * starting point X0 must be feasible or
              not too far away from the feasible set
            * grad(f) must be Lipschitz continuous on a level set:
              L = { x : f(x)<=f(x0) }
            * function must be defined everywhere on the feasible set F

            USAGE:

            Constrained optimization if far more complex than the unconstrained one.
            Here we give very brief outline of the BLEIC optimizer. We strongly recommend
            you to read examples in the ALGLIB Reference Manual and to read ALGLIB User Guide
            on optimization, which is available at http://www.alglib.net/optimization/

            1. User initializes algorithm state with MinBLEICCreate() call

            2. USer adds boundary and/or linear constraints by calling
               MinBLEICSetBC() and MinBLEICSetLC() functions.

            3. User sets stopping conditions with MinBLEICSetCond().

            4. User calls MinBLEICOptimize() function which takes algorithm  state and
               pointer (delegate, etc.) to callback function which calculates F/G.

            5. User calls MinBLEICResults() to get solution

            6. Optionally user may call MinBLEICRestartFrom() to solve another problem
               with same N but another starting point.
               MinBLEICRestartFrom() allows to reuse already initialized structure.


            INPUT PARAMETERS:
                N       -   problem dimension, N>0:
                            * if given, only leading N elements of X are used
                            * if not given, automatically determined from size ofX
                X       -   starting point, array[N]:
                            * it is better to set X to a feasible point
                            * but X can be infeasible, in which case algorithm will try
                              to find feasible point first, using X as initial
                              approximation.

            OUTPUT PARAMETERS:
                State   -   structure stores algorithm state

              -- ALGLIB --
                 Copyright 28.11.2010 by Bochkanov Sergey
            *************************************************************************/
        public static void Create(int n,
            FluxList<double> x,
            State state)
        {
            Utilities.Assert(n >= 1, "MinBLEICCreate: N<1");
            Utilities.Assert(x.Count >= n, "MinBLEICCreate: Length(X)<N");
            Utilities.Assert(Utilities.IsFiniteVector(x, n), "MinBLEICCreate: X contains infinite or NaN values!");
            InitInternal(n, x, 0.0, state);
        }

        /*************************************************************************
            This function sets boundary constraints for BLEIC optimizer.

            Boundary constraints are inactive by default (after initial creation).
            They are preserved after algorithm restart with MinBLEICRestartFrom().

            INPUT PARAMETERS:
                State   -   structure stores algorithm state
                BndL    -   lower bounds, array[N].
                            If some (all) variables are unbounded, you may specify
                            very small number or -INF.
                BndU    -   upper bounds, array[N].
                            If some (all) variables are unbounded, you may specify
                            very large number or +INF.

            NOTE 1: it is possible to specify BndL[i]=BndU[i]. In this case I-th
            variable will be "frozen" at X[i]=BndL[i]=BndU[i].

            NOTE 2: this solver has following useful properties:
            * bound constraints are always satisfied exactly
            * function is evaluated only INSIDE area specified by  bound  constraints,
              even  when  numerical  differentiation is used (algorithm adjusts  nodes
              according to boundary constraints)

              -- ALGLIB --
                 Copyright 28.11.2010 by Bochkanov Sergey
            *************************************************************************/
        public static void SetBoundaryConstraints(State state,
            FluxList<double> bndl,
            FluxList<double> bndu)
        {
            int i = 0;
            int n = 0;

            n = state.nmain;
            Utilities.Assert(bndl.Count >= n, "MinBLEICSetBC: Length(BndL)<N");
            Utilities.Assert(bndu.Count >= n, "MinBLEICSetBC: Length(BndU)<N");
            for (i = 0; i <= n - 1; i++)
            {
                Utilities.Assert(MathExtensions.IsFinite(bndl[i]) || Double.IsNegativeInfinity(bndl[i]), "MinBLEICSetBC: BndL contains NAN or +INF");
                Utilities.Assert(MathExtensions.IsFinite(bndu[i]) || Double.IsPositiveInfinity(bndu[i]), "MinBLEICSetBC: BndL contains NAN or -INF");
                state.bndl[i] = bndl[i];
                state.hasbndl[i] = MathExtensions.IsFinite(bndl[i]);
                state.bndu[i] = bndu[i];
                state.hasbndu[i] = MathExtensions.IsFinite(bndu[i]);
            }
            ActiveSet.SetBoundaryConstraints(state.sas, bndl, bndu);
        }


        /*************************************************************************
            This function sets linear constraints for BLEIC optimizer.

            Linear constraints are inactive by default (after initial creation).
            They are preserved after algorithm restart with MinBLEICRestartFrom().

            INPUT PARAMETERS:
                State   -   structure previously allocated with MinBLEICCreate call.
                C       -   linear constraints, array[K,N+1].
                            Each row of C represents one constraint, either equality
                            or inequality (see below):
                            * first N elements correspond to coefficients,
                            * last element corresponds to the right part.
                            All elements of C (including right part) must be finite.
                CT      -   type of constraints, array[K]:
                            * if CT[i]>0, then I-th constraint is C[i,*]*x >= C[i,n+1]
                            * if CT[i]=0, then I-th constraint is C[i,*]*x  = C[i,n+1]
                            * if CT[i]<0, then I-th constraint is C[i,*]*x <= C[i,n+1]
                K       -   number of equality/inequality constraints, K>=0:
                            * if given, only leading K elements of C/CT are used
                            * if not given, automatically determined from sizes of C/CT

            NOTE 1: linear (non-bound) constraints are satisfied only approximately:
            * there always exists some minor violation (about Epsilon in magnitude)
              due to rounding errors
            * numerical differentiation, if used, may  lead  to  function  evaluations
              outside  of the feasible  area,   because   algorithm  does  NOT  change
              numerical differentiation formula according to linear constraints.
            If you want constraints to be  satisfied  exactly, try to reformulate your
            problem  in  such  manner  that  all constraints will become boundary ones
            (this kind of constraints is always satisfied exactly, both in  the  final
            solution and in all intermediate points).

              -- ALGLIB --
                 Copyright 28.11.2010 by Bochkanov Sergey
            *************************************************************************/
        public static void SetLinearConstraints(State state,
            Matrix<double> c,
            FluxList<int> ct,
            int k)
        {
            int n = 0;
            int i = 0;
            int j = 0;
            double v = 0;
            int i_ = 0;

            n = state.nmain;

            //
            // First, check for errors in the inputs
            //
            Utilities.Assert(k >= 0, "MinBLEICSetLC: K<0");
            Utilities.Assert(c.Columns >= n + 1 || k == 0, "MinBLEICSetLC: Cols(C)<N+1");
            Utilities.Assert(c.Rows >= k, "MinBLEICSetLC: Rows(C)<K");
            Utilities.Assert(ct.Count >= k, "MinBLEICSetLC: Length(CT)<K");
            Utilities.Assert(Utilities.IsFiniteMatrix(c, k, n + 1), "MinBLEICSetLC: C contains infinite or NaN values!");

            //
            // Handle zero K
            //
            if (k == 0)
            {
                state.nec = 0;
                state.nic = 0;
                return;
            }

            //
            // Equality constraints are stored first, in the upper
            // NEC rows of State.CLEIC matrix. Inequality constraints
            // are stored in the next NIC rows.
            //
            // NOTE: we convert inequality constraints to the form
            // A*x<=b before copying them.
            //
            state.cleic.SetLengthAtLeast(k, n + 1);
            state.nec = 0;
            state.nic = 0;
            for (i = 0; i <= k - 1; i++)
            {
                if (ct[i] == 0)
                {
                    for (i_ = 0; i_ <= n; i_++)
                    {
                        state.cleic[state.nec, i_] = c[i, i_];
                    }
                    state.nec = state.nec + 1;
                }
            }
            for (i = 0; i <= k - 1; i++)
            {
                if (ct[i] != 0)
                {
                    if (ct[i] > 0)
                    {
                        for (i_ = 0; i_ <= n; i_++)
                        {
                            state.cleic[state.nec + state.nic, i_] = -c[i, i_];
                        }
                    }
                    else
                    {
                        for (i_ = 0; i_ <= n; i_++)
                        {
                            state.cleic[state.nec + state.nic, i_] = c[i, i_];
                        }
                    }
                    state.nic = state.nic + 1;
                }
            }

            //
            // Normalize rows of State.CLEIC: each row must have unit norm.
            // Norm is calculated using first N elements (i.e. right part is
            // not counted when we calculate norm).
            //
            for (i = 0; i <= k - 1; i++)
            {
                v = 0;
                for (j = 0; j <= n - 1; j++)
                {
                    v = v + MathExtensions.Squared(state.cleic[i, j]);
                }
                if (v == 0)
                {
                    continue;
                }
                v = 1 / Math.Sqrt(v);
                for (i_ = 0; i_ <= n; i_++)
                {
                    state.cleic[i, i_] = v * state.cleic[i, i_];
                }
            }
            ActiveSet.SetsLinearConstraints(state.sas, c, ct, k);
        }


        /*************************************************************************
            This function sets stopping conditions for the optimizer.

            INPUT PARAMETERS:
                State   -   structure which stores algorithm state
                EpsG    -   >=0
                            The  subroutine  finishes  its  work   if   the  condition
                            |v|<EpsG is satisfied, where:
                            * |.| means Euclidian norm
                            * v - scaled gradient vector, v[i]=g[i]*s[i]
                            * g - gradient
                            * s - scaling coefficients set by MinBLEICSetScale()
                EpsF    -   >=0
                            The  subroutine  finishes  its work if on k+1-th iteration
                            the  condition  |F(k+1)-F(k)|<=EpsF*max{|F(k)|,|F(k+1)|,1}
                            is satisfied.
                EpsX    -   >=0
                            The subroutine finishes its work if  on  k+1-th  iteration
                            the condition |v|<=EpsX is fulfilled, where:
                            * |.| means Euclidian norm
                            * v - scaled step vector, v[i]=dx[i]/s[i]
                            * dx - step vector, dx=X(k+1)-X(k)
                            * s - scaling coefficients set by MinBLEICSetScale()
                MaxIts  -   maximum number of iterations. If MaxIts=0, the  number  of
                            iterations is unlimited.

            Passing EpsG=0, EpsF=0 and EpsX=0 and MaxIts=0 (simultaneously) will lead
            to automatic stopping criterion selection.

            NOTE: when SetCond() called with non-zero MaxIts, BLEIC solver may perform
                  slightly more than MaxIts iterations. I.e., MaxIts  sets  non-strict
                  limit on iterations count.

              -- ALGLIB --
                 Copyright 28.11.2010 by Bochkanov Sergey
            *************************************************************************/
        public static void SetStopConditions(State state,
            double epsg,
            double epsf,
            double epsx,
            int maxits)
        {
            Utilities.Assert(MathExtensions.IsFinite(epsg), "MinBLEICSetCond: EpsG is not finite number");
            Utilities.Assert(epsg >= 0, "MinBLEICSetCond: negative EpsG");
            Utilities.Assert(MathExtensions.IsFinite(epsf), "MinBLEICSetCond: EpsF is not finite number");
            Utilities.Assert(epsf >= 0, "MinBLEICSetCond: negative EpsF");
            Utilities.Assert(MathExtensions.IsFinite(epsx), "MinBLEICSetCond: EpsX is not finite number");
            Utilities.Assert(epsx >= 0, "MinBLEICSetCond: negative EpsX");
            Utilities.Assert(maxits >= 0, "MinBLEICSetCond: negative MaxIts!");
            if (((epsg == 0 && epsf == 0) && epsx == 0) && maxits == 0)
            {
                epsx = 1.0E-6;
            }
            state.epsg = epsg;
            state.epsf = epsf;
            state.epsx = epsx;
            state.maxits = maxits;
        }


        /*************************************************************************
            This function sets scaling coefficients for BLEIC optimizer.

            ALGLIB optimizers use scaling matrices to test stopping  conditions  (step
            size and gradient are scaled before comparison with tolerances).  Scale of
            the I-th variable is a translation invariant measure of:
            a) "how large" the variable is
            b) how large the step should be to make significant changes in the function

            Scaling is also used by finite difference variant of the optimizer  - step
            along I-th axis is equal to DiffStep*S[I].

            In  most  optimizers  (and  in  the  BLEIC  too)  scaling is NOT a form of
            preconditioning. It just  affects  stopping  conditions.  You  should  set
            preconditioner  by  separate  call  to  one  of  the  MinBLEICSetPrec...()
            functions.

            There is a special  preconditioning  mode, however,  which  uses   scaling
            coefficients to form diagonal preconditioning matrix. You  can  turn  this
            mode on, if you want.   But  you should understand that scaling is not the
            same thing as preconditioning - these are two different, although  related
            forms of tuning solver.

            INPUT PARAMETERS:
                State   -   structure stores algorithm state
                S       -   array[N], non-zero scaling coefficients
                            S[i] may be negative, sign doesn't matter.

              -- ALGLIB --
                 Copyright 14.01.2011 by Bochkanov Sergey
            *************************************************************************/
        public static void SetScalingCoefficients(State state,
            FluxList<double> s)
        {
            int i = 0;

            Utilities.Assert(s.Count >= state.nmain, "MinBLEICSetScale: Length(S)<N");
            for (i = 0; i <= state.nmain - 1; i++)
            {
                Utilities.Assert(MathExtensions.IsFinite(s[i]), "MinBLEICSetScale: S contains infinite or NAN elements");
                Utilities.Assert(s[i] != 0, "MinBLEICSetScale: S contains zero elements");
                state.s[i] = Math.Abs(s[i]);
            }
            ActiveSet.SetScale(state.sas, s);
        }


        /*************************************************************************
            Modification of the preconditioner: preconditioning is turned off.

            INPUT PARAMETERS:
                State   -   structure which stores algorithm state

              -- ALGLIB --
                 Copyright 13.10.2010 by Bochkanov Sergey
            *************************************************************************/
        public static void SetPreConditioner(State state)
        {
            state.prectype = 0;
        }


        /*************************************************************************
            Modification of the preconditioner: scale-based diagonal preconditioning.

            This preconditioning mode can be useful when you  don't  have  approximate
            diagonal of Hessian, but you know that your  variables  are  badly  scaled
            (for  example,  one  variable is in [1,10], and another in [1000,100000]),
            and most part of the ill-conditioning comes from different scales of vars.

            In this case simple  scale-based  preconditioner,  with H[i] = 1/(s[i]^2),
            can greatly improve convergence.

            IMPRTANT: you should set scale of your variables  with  MinBLEICSetScale()
            call  (before  or after MinBLEICSetPrecScale() call). Without knowledge of
            the scale of your variables scale-based preconditioner will be  just  unit
            matrix.

            INPUT PARAMETERS:
                State   -   structure which stores algorithm state

              -- ALGLIB --
                 Copyright 13.10.2010 by Bochkanov Sergey
            *************************************************************************/
        public static void SetScaleBasedDiagonalPreConditioning(State state)
        {
            state.prectype = 3;
        }


        /*************************************************************************
            This function turns on/off reporting.

            INPUT PARAMETERS:
                State   -   structure which stores algorithm state
                NeedXRep-   whether iteration reports are needed or not

            If NeedXRep is True, algorithm will call rep() callback function if  it is
            provided to MinBLEICOptimize().

              -- ALGLIB --
                 Copyright 28.11.2010 by Bochkanov Sergey
            *************************************************************************/
        private static void SetReporting(State state,
            bool needxrep)
        {
            state.xrep = needxrep;
        }


        /*************************************************************************
            This function turns on/off line search reports.
            These reports are described in more details in developer-only  comments on
            MinBLEICState object.

            INPUT PARAMETERS:
                State   -   structure which stores algorithm state
                NeedDRep-   whether line search reports are needed or not

            This function is intended for private use only. Turning it on artificially
            may cause program failure.

              -- ALGLIB --
                 Copyright 02.04.2010 by Bochkanov Sergey
            *************************************************************************/
        public static void SetLineSearchReports(State state,
            bool needdrep)
        {
            state.drep = needdrep;
        }


        /*************************************************************************
            This function sets maximum step length

            IMPORTANT: this feature is hard to combine with preconditioning. You can't
            set upper limit on step length, when you solve optimization  problem  with
            linear (non-boundary) constraints AND preconditioner turned on.

            When  non-boundary  constraints  are  present,  you  have to either a) use
            preconditioner, or b) use upper limit on step length.  YOU CAN'T USE BOTH!
            In this case algorithm will terminate with appropriate error code.

            INPUT PARAMETERS:
                State   -   structure which stores algorithm state
                StpMax  -   maximum step length, >=0. Set StpMax to 0.0,  if you don't
                            want to limit step length.

            Use this subroutine when you optimize target function which contains exp()
            or  other  fast  growing  functions,  and optimization algorithm makes too
            large  steps  which  lead   to overflow. This function allows us to reject
            steps  that  are  too  large  (and  therefore  expose  us  to the possible
            overflow) without actually calculating function value at the x+stp*d.

              -- ALGLIB --
                 Copyright 02.04.2010 by Bochkanov Sergey
            *************************************************************************/
        private static void SetMaxStepLength(State state,
            double stpmax)
        {
            Utilities.Assert(MathExtensions.IsFinite(stpmax), "MinBLEICSetStpMax: StpMax is not finite!");
            Utilities.Assert(stpmax >= 0, "MinBLEICSetStpMax: StpMax<0!");
            state.stpmax = stpmax;
        }


        /*************************************************************************
            NOTES:

            1. This function has two different implementations: one which  uses  exact
               (analytical) user-supplied gradient,  and one which uses function value
               only  and  numerically  differentiates  function  in  order  to  obtain
               gradient.

               Depending  on  the  specific  function  used to create optimizer object
               (either  MinBLEICCreate() for analytical gradient or  MinBLEICCreateF()
               for numerical differentiation) you should choose appropriate variant of
               MinBLEICOptimize() - one  which  accepts  function  AND gradient or one
               which accepts function ONLY.

               Be careful to choose variant of MinBLEICOptimize() which corresponds to
               your optimization scheme! Table below lists different  combinations  of
               callback (function/gradient) passed to MinBLEICOptimize()  and specific
               function used to create optimizer.


                                 |         USER PASSED TO MinBLEICOptimize()
               CREATED WITH      |  function only   |  function and gradient
               ------------------------------------------------------------
               MinBLEICCreateF() |     work                FAIL
               MinBLEICCreate()  |     FAIL                work

               Here "FAIL" denotes inappropriate combinations  of  optimizer  creation
               function  and  MinBLEICOptimize()  version.   Attemps   to   use   such
               combination (for  example,  to  create optimizer with MinBLEICCreateF()
               and  to  pass  gradient  information  to  MinCGOptimize()) will lead to
               exception being thrown. Either  you  did  not pass gradient when it WAS
               needed or you passed gradient when it was NOT needed.

              -- ALGLIB --
                 Copyright 28.11.2010 by Bochkanov Sergey
            *************************************************************************/
        public static bool Iteration(State state)
        {
            bool result = new bool();
            int n = 0;
            int m = 0;
            int i = 0;
            int j = 0;
            double v = 0;
            double vv = 0;
            int badbfgsits = 0;
            bool b = new bool();
            int nextaction = 0;
            int mcinfo = 0;
            int actstatus = 0;
            int ic = 0;
            double penalty = 0;
            double ginit = 0;
            double gdecay = 0;
            int i_ = 0;


            //
            // Reverse communication preparations
            // I know it looks ugly, but it works the same way
            // anywhere from C++ to Python.
            //
            // This code initializes locals by:
            // * random values determined during code
            //   generation - on first subroutine call
            // * values from previous call - on subsequent calls
            //
            if (state.rstate.stage >= 0)
            {
                n = state.rstate.ia[0];
                m = state.rstate.ia[1];
                i = state.rstate.ia[2];
                j = state.rstate.ia[3];
                badbfgsits = state.rstate.ia[4];
                nextaction = state.rstate.ia[5];
                mcinfo = state.rstate.ia[6];
                actstatus = state.rstate.ia[7];
                ic = state.rstate.ia[8];
                b = state.rstate.ba[0];
                v = state.rstate.ra[0];
                vv = state.rstate.ra[1];
                penalty = state.rstate.ra[2];
                ginit = state.rstate.ra[3];
                gdecay = state.rstate.ra[4];
            }
            else
            {
                n = -983;
                m = -989;
                i = -834;
                j = 900;
                badbfgsits = -287;
                nextaction = 364;
                mcinfo = 214;
                actstatus = -338;
                ic = -686;
                b = false;
                v = 585;
                vv = 497;
                penalty = -271;
                ginit = -581;
                gdecay = 745;
            }
            if (state.rstate.stage == 0)
            {
                goto lbl_0;
            }
            if (state.rstate.stage == 1)
            {
                goto lbl_1;
            }
            if (state.rstate.stage == 2)
            {
                goto lbl_2;
            }
            if (state.rstate.stage == 3)
            {
                goto lbl_3;
            }
            if (state.rstate.stage == 4)
            {
                goto lbl_4;
            }
            if (state.rstate.stage == 5)
            {
                goto lbl_5;
            }
            if (state.rstate.stage == 6)
            {
                goto lbl_6;
            }
            if (state.rstate.stage == 7)
            {
                goto lbl_7;
            }
            if (state.rstate.stage == 8)
            {
                goto lbl_8;
            }
            if (state.rstate.stage == 9)
            {
                goto lbl_9;
            }
            if (state.rstate.stage == 10)
            {
                goto lbl_10;
            }
            if (state.rstate.stage == 11)
            {
                goto lbl_11;
            }
            if (state.rstate.stage == 12)
            {
                goto lbl_12;
            }
            if (state.rstate.stage == 13)
            {
                goto lbl_13;
            }
            if (state.rstate.stage == 14)
            {
                goto lbl_14;
            }
            if (state.rstate.stage == 15)
            {
                goto lbl_15;
            }
            if (state.rstate.stage == 16)
            {
                goto lbl_16;
            }
            if (state.rstate.stage == 17)
            {
                goto lbl_17;
            }
            if (state.rstate.stage == 18)
            {
                goto lbl_18;
            }
            if (state.rstate.stage == 19)
            {
                goto lbl_19;
            }
            if (state.rstate.stage == 20)
            {
                goto lbl_20;
            }
            if (state.rstate.stage == 21)
            {
                goto lbl_21;
            }
            if (state.rstate.stage == 22)
            {
                goto lbl_22;
            }
            if (state.rstate.stage == 23)
            {
                goto lbl_23;
            }
            if (state.rstate.stage == 24)
            {
                goto lbl_24;
            }
            if (state.rstate.stage == 25)
            {
                goto lbl_25;
            }
            if (state.rstate.stage == 26)
            {
                goto lbl_26;
            }
            if (state.rstate.stage == 27)
            {
                goto lbl_27;
            }
            if (state.rstate.stage == 28)
            {
                goto lbl_28;
            }
            if (state.rstate.stage == 29)
            {
                goto lbl_29;
            }
            if (state.rstate.stage == 30)
            {
                goto lbl_30;
            }
            if (state.rstate.stage == 31)
            {
                goto lbl_31;
            }
            if (state.rstate.stage == 32)
            {
                goto lbl_32;
            }
            if (state.rstate.stage == 33)
            {
                goto lbl_33;
            }
            if (state.rstate.stage == 34)
            {
                goto lbl_34;
            }
            if (state.rstate.stage == 35)
            {
                goto lbl_35;
            }
            if (state.rstate.stage == 36)
            {
                goto lbl_36;
            }
            if (state.rstate.stage == 37)
            {
                goto lbl_37;
            }
            if (state.rstate.stage == 38)
            {
                goto lbl_38;
            }
            if (state.rstate.stage == 39)
            {
                goto lbl_39;
            }
            if (state.rstate.stage == 40)
            {
                goto lbl_40;
            }
            if (state.rstate.stage == 41)
            {
                goto lbl_41;
            }

            //
            // Routine body
            //

            //
            // Algorithm parameters:
            // * M          number of L-BFGS corrections.
            //              This coefficient remains fixed during iterations.
            // * GDecay     desired decrease of constrained gradient during L-BFGS iterations.
            //              This coefficient is decreased after each L-BFGS round until
            //              it reaches minimum decay.
            //
            m = Math.Min(5, state.nmain);
            gdecay = initialdecay;

            //
            // Init
            //
            n = state.nmain;
            state.repterminationtype = 0;
            state.repinneriterationscount = 0;
            state.repouteriterationscount = 0;
            state.repnfev = 0;
            state.repvaridx = -1;
            state.repdebugeqerr = 0.0;
            state.repdebugfs = Double.NaN;
            state.repdebugff = Double.NaN;
            state.repdebugdx = Double.NaN;
            if (state.stpmax != 0 && state.prectype != 0)
            {
                state.repterminationtype = -10;
                result = false;
                return result;
            }
            state.rho.SetLengthAtLeast(m);
            state.theta.SetLengthAtLeast(m);
            state.yk.SetLengthAtLeast(m, n);
            state.sk.SetLengthAtLeast(m, n);

            //
            // Fill TmpPrec with current preconditioner
            //
            state.tmpprec.SetLengthAtLeast(n);
            for (i = 0; i <= n - 1; i++)
            {
                if (state.prectype == 2)
                {
                    state.tmpprec[i] = state.diagh[i];
                    continue;
                }
                if (state.prectype == 3)
                {
                    state.tmpprec[i] = 1 / MathExtensions.Squared(state.s[i]);
                    continue;
                }
                state.tmpprec[i] = 1;
            }
            ActiveSet.SetPreconditionerDiagnol(state.sas, state.tmpprec);

            //
            // Start optimization
            //
            if (!ActiveSet.StartOptimization(state.sas, state.xstart))
            {
                state.repterminationtype = -3;
                result = false;
                return result;
            }

            //
            //  Check correctness of user-supplied gradient
            //
            if (!(state.diffstep == 0 && state.teststep > 0))
            {
                goto lbl_42;
            }
            ClearRequestFields(state);
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.x[i_] = state.sas.xc[i_];
            }
            state.needfg = true;
            i = 0;
            lbl_44:
            if (i > n - 1)
            {
                goto lbl_46;
            }
            Utilities.Assert(!state.hasbndl[i] || state.sas.xc[i] >= state.bndl[i], "MinBLEICIteration: internal error(State.X is out of bounds)");
            Utilities.Assert(!state.hasbndu[i] || state.sas.xc[i] <= state.bndu[i], "MinBLEICIteration: internal error(State.X is out of bounds)");
            v = state.x[i];
            state.x[i] = v - state.teststep * state.s[i];
            if (state.hasbndl[i])
            {
                state.x[i] = Math.Max(state.x[i], state.bndl[i]);
            }
            state.xm1 = state.x[i];
            state.rstate.stage = 0;
            goto lbl_rcomm;
            lbl_0:
            state.fm1 = state.f;
            state.gm1 = state.g[i];
            state.x[i] = v + state.teststep * state.s[i];
            if (state.hasbndu[i])
            {
                state.x[i] = Math.Min(state.x[i], state.bndu[i]);
            }
            state.xp1 = state.x[i];
            state.rstate.stage = 1;
            goto lbl_rcomm;
            lbl_1:
            state.fp1 = state.f;
            state.gp1 = state.g[i];
            state.x[i] = (state.xm1 + state.xp1) / 2;
            if (state.hasbndl[i])
            {
                state.x[i] = Math.Max(state.x[i], state.bndl[i]);
            }
            if (state.hasbndu[i])
            {
                state.x[i] = Math.Min(state.x[i], state.bndu[i]);
            }
            state.rstate.stage = 2;
            goto lbl_rcomm;
            lbl_2:
            state.x[i] = v;
            if (!Optimizer.Utilities.DerivativeCheck(state.fm1, state.gm1, state.fp1, state.gp1, state.f, state.g[i], state.xp1 - state.xm1))
            {
                state.repvaridx = i;
                state.repterminationtype = -7;
                ActiveSet.StopOptimization(state.sas);
                result = false;
                return result;
            }
            i = i + 1;
            goto lbl_44;
            lbl_46:
            state.needfg = false;
            lbl_42:

            //
            // Main cycle of BLEIC-PG algorithm
            //
            state.repterminationtype = 4;
            badbfgsits = 0;
            state.lastgoodstep = 0;
            state.lastscaledgoodstep = 0;
            state.maxscaledgrad = 0;
            state.nonmonotoniccnt = n + state.nic;
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.x[i_] = state.sas.xc[i_];
            }
            ClearRequestFields(state);
            if (state.diffstep != 0)
            {
                goto lbl_47;
            }
            state.needfg = true;
            state.rstate.stage = 3;
            goto lbl_rcomm;
            lbl_3:
            state.needfg = false;
            goto lbl_48;
            lbl_47:
            state.needf = true;
            state.rstate.stage = 4;
            goto lbl_rcomm;
            lbl_4:
            state.needf = false;
            lbl_48:
            state.fc = state.f;
            Optimizer.Utilities.TrimPrepare(state.f, ref state.trimthreshold);
            state.repnfev = state.repnfev + 1;
            if (!state.xrep)
            {
                goto lbl_49;
            }

            //
            // Report current point
            //
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.x[i_] = state.sas.xc[i_];
            }
            state.f = state.fc;
            state.xupdated = true;
            state.rstate.stage = 5;
            goto lbl_rcomm;
            lbl_5:
            state.xupdated = false;
            lbl_49:
            lbl_51:
            if (false)
            {
                goto lbl_52;
            }

            //
            // Phase 1
            //
            // (a) calculate unconstrained gradient
            // (b) determine active set
            // (c) update MaxScaledGrad
            //
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.x[i_] = state.sas.xc[i_];
            }
            ClearRequestFields(state);
            if (state.diffstep != 0)
            {
                goto lbl_53;
            }

            //
            // Analytic gradient
            //
            state.needfg = true;
            state.rstate.stage = 6;
            goto lbl_rcomm;
            lbl_6:
            state.needfg = false;
            goto lbl_54;
            lbl_53:

            //
            // Numerical differentiation
            //
            state.needf = true;
            state.rstate.stage = 7;
            goto lbl_rcomm;
            lbl_7:
            state.fbase = state.f;
            i = 0;
            lbl_55:
            if (i > n - 1)
            {
                goto lbl_57;
            }
            v = state.x[i];
            b = false;
            if (state.hasbndl[i])
            {
                b = b || v - state.diffstep * state.s[i] < state.bndl[i];
            }
            if (state.hasbndu[i])
            {
                b = b || v + state.diffstep * state.s[i] > state.bndu[i];
            }
            if (b)
            {
                goto lbl_58;
            }
            state.x[i] = v - state.diffstep * state.s[i];
            state.rstate.stage = 8;
            goto lbl_rcomm;
            lbl_8:
            state.fm2 = state.f;
            state.x[i] = v - 0.5 * state.diffstep * state.s[i];
            state.rstate.stage = 9;
            goto lbl_rcomm;
            lbl_9:
            state.fm1 = state.f;
            state.x[i] = v + 0.5 * state.diffstep * state.s[i];
            state.rstate.stage = 10;
            goto lbl_rcomm;
            lbl_10:
            state.fp1 = state.f;
            state.x[i] = v + state.diffstep * state.s[i];
            state.rstate.stage = 11;
            goto lbl_rcomm;
            lbl_11:
            state.fp2 = state.f;
            state.g[i] = (8 * (state.fp1 - state.fm1) - (state.fp2 - state.fm2)) / (6 * state.diffstep * state.s[i]);
            goto lbl_59;
            lbl_58:
            state.xm1 = v - state.diffstep * state.s[i];
            state.xp1 = v + state.diffstep * state.s[i];
            if (state.hasbndl[i] && state.xm1 < state.bndl[i])
            {
                state.xm1 = state.bndl[i];
            }
            if (state.hasbndu[i] && state.xp1 > state.bndu[i])
            {
                state.xp1 = state.bndu[i];
            }
            state.x[i] = state.xm1;
            state.rstate.stage = 12;
            goto lbl_rcomm;
            lbl_12:
            state.fm1 = state.f;
            state.x[i] = state.xp1;
            state.rstate.stage = 13;
            goto lbl_rcomm;
            lbl_13:
            state.fp1 = state.f;
            if (state.xm1 != state.xp1)
            {
                state.g[i] = (state.fp1 - state.fm1) / (state.xp1 - state.xm1);
            }
            else
            {
                state.g[i] = 0;
            }
            lbl_59:
            state.x[i] = v;
            i = i + 1;
            goto lbl_55;
            lbl_57:
            state.f = state.fbase;
            state.needf = false;
            lbl_54:
            state.fc = state.f;
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.gc[i_] = state.g[i_];
            }
            ActiveSet.RecalculateConstraintsPreconditioned(state.sas, state.gc);
            v = 0.0;
            for (i = 0; i <= n - 1; i++)
            {
                v = v + MathExtensions.Squared(state.gc[i] * state.s[i]);
            }
            state.maxscaledgrad = Math.Max(state.maxscaledgrad, Math.Sqrt(v));

            //
            // Phase 2: perform steepest descent step.
            //
            // NextAction control variable is set on exit from this loop:
            // * NextAction>0 in case we have to proceed to Phase 3 (L-BFGS step)
            // * NextAction<0 in case we have to proceed to Phase 1 (recalculate active set)
            // * NextAction=0 in case we found solution (step size or function change are small enough)
            //
            nextaction = 0;
            lbl_60:
            if (false)
            {
                goto lbl_61;
            }

            //
            // Check gradient-based stopping criteria
            //
            if (ActiveSet.ScaledConstrainedNorm(state.sas, state.gc) <= state.epsg)
            {

                //
                // Gradient is small enough, stop iterations
                //
                state.repterminationtype = 4;
                nextaction = 0;
                goto lbl_61;
            }

            //
            // Calculate normalized constrained descent direction, store to D.
            // Try to use previous scaled step length as initial estimate for new step.
            //
            // NOTE: D can be exactly zero, in this case Stp is set to 1.0
            //
            ActiveSet.ConstrainedDescentPreconditioned(state.sas, state.gc, ref state.d);
            v = 0;
            for (i = 0; i <= n - 1; i++)
            {
                v = v + MathExtensions.Squared(state.d[i] / state.s[i]);
            }
            v = Math.Sqrt(v);
            if (state.lastscaledgoodstep > 0 && v > 0)
            {
                state.stp = state.lastscaledgoodstep / v;
            }
            else
            {
                state.stp = 1.0;
            }

            //
            // Calculate bound on step length.
            // Enforce user-supplied limit on step length.
            //
            ActiveSet.ExploreDirection(state.sas, state.d, ref state.curstpmax, ref state.cidx, ref state.cval);
            state.activationstep = state.curstpmax;
            if (state.cidx >= 0 && state.activationstep == 0)
            {
                ActiveSet.ImmediateActivation(state.sas, state.cidx, state.cval);
                goto lbl_60;
            }
            if (state.stpmax > 0)
            {
                state.curstpmax = Math.Min(state.curstpmax, state.stpmax);
            }

            //
            // Report beginning of line search (if requested by caller).
            // See description of the MinBLEICState for more information
            // about fields accessible to caller.
            //
            // Caller may do following:
            // * change State.Stp and load better initial estimate of
            //   the step length.
            //
            if (!state.drep)
            {
                goto lbl_62;
            }
            ClearRequestFields(state);
            state.lsstart = true;
            state.lbfgssearch = false;
            state.boundedstep = state.cidx >= 0;
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.x[i_] = state.sas.xc[i_];
            }
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.g[i_] = state.gc[i_];
            }
            state.f = state.fc;
            state.rstate.stage = 14;
            goto lbl_rcomm;
            lbl_14:
            state.lsstart = false;
            lbl_62:

            //
            // Perform optimization of F along XC+alpha*D.
            //
            state.mcstage = 0;
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.xn[i_] = state.sas.xc[i_];
            }
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.gn[i_] = state.gc[i_];
            }
            state.fn = state.fc;
            LinMin.McSearch(n, ref state.xn, ref state.fn, ref state.gn, state.d, ref state.stp, state.curstpmax, gtol, ref mcinfo, ref state.nfev, ref state.work, state.lstate, ref state.mcstage);
            lbl_64:
            if (state.mcstage == 0)
            {
                goto lbl_65;
            }

            //
            // Enforce constraints (correction) in XN.
            // Copy current point from XN to X.
            //
            ActiveSet.Correction(state.sas, state.xn, ref penalty);
            for (i = 0; i <= n - 1; i++)
            {
                state.x[i] = state.xn[i];
            }

            //
            // Gradient, either user-provided or numerical differentiation
            //
            ClearRequestFields(state);
            if (state.diffstep != 0)
            {
                goto lbl_66;
            }

            //
            // Analytic gradient
            //
            state.needfg = true;
            state.rstate.stage = 15;
            goto lbl_rcomm;
            lbl_15:
            state.needfg = false;
            state.repnfev = state.repnfev + 1;
            goto lbl_67;
            lbl_66:

            //
            // Numerical differentiation
            //
            state.needf = true;
            state.rstate.stage = 16;
            goto lbl_rcomm;
            lbl_16:
            state.fbase = state.f;
            i = 0;
            lbl_68:
            if (i > n - 1)
            {
                goto lbl_70;
            }
            v = state.x[i];
            b = false;
            if (state.hasbndl[i])
            {
                b = b || v - state.diffstep * state.s[i] < state.bndl[i];
            }
            if (state.hasbndu[i])
            {
                b = b || v + state.diffstep * state.s[i] > state.bndu[i];
            }
            if (b)
            {
                goto lbl_71;
            }
            state.x[i] = v - state.diffstep * state.s[i];
            state.rstate.stage = 17;
            goto lbl_rcomm;
            lbl_17:
            state.fm2 = state.f;
            state.x[i] = v - 0.5 * state.diffstep * state.s[i];
            state.rstate.stage = 18;
            goto lbl_rcomm;
            lbl_18:
            state.fm1 = state.f;
            state.x[i] = v + 0.5 * state.diffstep * state.s[i];
            state.rstate.stage = 19;
            goto lbl_rcomm;
            lbl_19:
            state.fp1 = state.f;
            state.x[i] = v + state.diffstep * state.s[i];
            state.rstate.stage = 20;
            goto lbl_rcomm;
            lbl_20:
            state.fp2 = state.f;
            state.g[i] = (8 * (state.fp1 - state.fm1) - (state.fp2 - state.fm2)) / (6 * state.diffstep * state.s[i]);
            state.repnfev = state.repnfev + 4;
            goto lbl_72;
            lbl_71:
            state.xm1 = v - state.diffstep * state.s[i];
            state.xp1 = v + state.diffstep * state.s[i];
            if (state.hasbndl[i] && state.xm1 < state.bndl[i])
            {
                state.xm1 = state.bndl[i];
            }
            if (state.hasbndu[i] && state.xp1 > state.bndu[i])
            {
                state.xp1 = state.bndu[i];
            }
            state.x[i] = state.xm1;
            state.rstate.stage = 21;
            goto lbl_rcomm;
            lbl_21:
            state.fm1 = state.f;
            state.x[i] = state.xp1;
            state.rstate.stage = 22;
            goto lbl_rcomm;
            lbl_22:
            state.fp1 = state.f;
            if (state.xm1 != state.xp1)
            {
                state.g[i] = (state.fp1 - state.fm1) / (state.xp1 - state.xm1);
            }
            else
            {
                state.g[i] = 0;
            }
            state.repnfev = state.repnfev + 2;
            lbl_72:
            state.x[i] = v;
            i = i + 1;
            goto lbl_68;
            lbl_70:
            state.f = state.fbase;
            state.needf = false;
            lbl_67:

            //
            // Back to MCSRCH
            //
            // NOTE: penalty term from correction is added to FN in order
            //       to penalize increase in infeasibility.
            //
            state.fn = state.f + penaltyfactor * state.maxscaledgrad * penalty;
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.gn[i_] = state.g[i_];
            }
            Optimizer.Utilities.TrimFunction(ref state.fn, ref state.gn, n, state.trimthreshold);
            LinMin.McSearch(n, ref state.xn, ref state.fn, ref state.gn, state.d, ref state.stp, state.curstpmax, gtol, ref mcinfo, ref state.nfev, ref state.work, state.lstate, ref state.mcstage);
            goto lbl_64;
            lbl_65:

            //
            // Handle possible failure of the line search
            //
            if (mcinfo != 1 && mcinfo != 5)
            {

                //
                // We can not find step which decreases function value. We have
                // two possibilities:
                // (a) numerical properties of the function do not allow us to
                //     find good solution.
                // (b) we are close to activation of some constraint, and it is
                //     so close that step which activates it leads to change in
                //     target function which is smaller than numerical noise.
                //
                // Optimization algorithm must be able to handle case (b), because
                // inability to handle it will cause failure when algorithm
                // started very close to boundary of the feasible area.
                //
                // In order to correctly handle such cases we allow limited amount
                // of small steps which increase function value.
                //
                v = 0.0;
                for (i = 0; i <= n - 1; i++)
                {
                    v = v + MathExtensions.Squared(state.d[i] * state.curstpmax / state.s[i]);
                }
                v = Math.Sqrt(v);
                if ((state.cidx >= 0 && v <= maxnonmonotoniclen) && state.nonmonotoniccnt > 0)
                {

                    //
                    // We enforce non-monotonic step:
                    // * Stp    := CurStpMax
                    // * MCINFO := 5
                    // * XN     := XC+CurStpMax*D
                    // * non-monotonic counter is decreased
                    //
                    state.stp = state.curstpmax;
                    mcinfo = 5;
                    v = state.curstpmax;
                    for (i_ = 0; i_ <= n - 1; i_++)
                    {
                        state.xn[i_] = state.sas.xc[i_];
                    }
                    for (i_ = 0; i_ <= n - 1; i_++)
                    {
                        state.xn[i_] = state.xn[i_] + v * state.d[i_];
                    }
                    state.nonmonotoniccnt = state.nonmonotoniccnt - 1;
                }
                else
                {

                    //
                    // Numerical properties of the function does not allow us to solve problem
                    //
                    state.repterminationtype = 7;
                    nextaction = 0;
                    goto lbl_61;
                }
            }

            //
            // Current point is updated.
            //
            // NOTE: below we rely on fact that for MCINFO=5 we ALWAYS have
            //       Stp=StpMax returned by MCSRCH, even when StpMax<StpMin (it
            //       is possible because StpMin=1E-50, and sometimes we have to
            //       perform steps less than 1E-50).
            //
            //       Thus, when step activates constraints, we ALWAYS have Stp=StpMax.
            //       It was not true before bug #570 was fixed.
            //
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.xp[i_] = state.sas.xc[i_];
            }
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.gp[i_] = state.gc[i_];
            }
            state.fp = state.fc;
            actstatus = ActiveSet.MoveTo(state.sas, state.xn, state.cidx >= 0 && state.stp >= state.activationstep, state.cidx, state.cval);
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.gc[i_] = state.gn[i_];
            }
            state.fc = state.fn;
            state.repinneriterationscount = state.repinneriterationscount + 1;
            if (!state.xrep)
            {
                goto lbl_73;
            }
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.x[i_] = state.sas.xc[i_];
            }
            ClearRequestFields(state);
            state.xupdated = true;
            state.rstate.stage = 23;
            goto lbl_rcomm;
            lbl_23:
            state.xupdated = false;
            lbl_73:

            //
            // Check for stopping.
            //
            // Step, gradient and function-based stopping criteria are tested only
            // for steps which satisfy Wolfe conditions.
            //
            // MaxIts-based stopping condition is checked for all steps
            //
            if (mcinfo == 1)
            {

                //
                // Step is small enough
                //
                v = 0;
                vv = 0;
                for (i = 0; i <= n - 1; i++)
                {
                    v = v + MathExtensions.Squared((state.sas.xc[i] - state.xp[i]) / state.s[i]);
                    vv = vv + MathExtensions.Squared(state.sas.xc[i] - state.xp[i]);
                }
                v = Math.Sqrt(v);
                vv = Math.Sqrt(vv);
                if (v <= state.epsx)
                {
                    state.repterminationtype = 2;
                    nextaction = 0;
                    goto lbl_61;
                }
                state.lastgoodstep = vv;
                UpdateEstimateOfGoodStep(ref state.lastscaledgoodstep, v);

                //
                // Function change is small enough
                //
                if (Math.Abs(state.fp - state.fc) <= state.epsf * Math.Max(Math.Abs(state.fc), Math.Max(Math.Abs(state.fp), 1.0)))
                {

                    //
                    // Function change is small enough
                    //
                    state.repterminationtype = 1;
                    nextaction = 0;
                    goto lbl_61;
                }
            }
            if (state.maxits > 0 && state.repinneriterationscount >= state.maxits)
            {

                //
                // Required number of iterations was performed
                //
                state.repterminationtype = 5;
                nextaction = 0;
                goto lbl_61;
            }

            //
            // Decide where to move:
            // * in case only "candidate" constraints were activated, repeat stage 2
            // * in case no constraints was activated, move to stage 3
            // * otherwise, move to stage 1 (re-evaluation of the active set)
            //
            if (actstatus == 0)
            {
                goto lbl_60;
            }
            if (actstatus < 0)
            {
                nextaction = 1;
            }
            else
            {
                nextaction = -1;
            }
            goto lbl_61;
            goto lbl_60;
            lbl_61:
            if (nextaction < 0)
            {
                goto lbl_51;
            }
            if (nextaction == 0)
            {
                goto lbl_52;
            }

            //
            // Phase 3: L-BFGS step
            //
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.x[i_] = state.sas.xc[i_];
            }
            ClearRequestFields(state);
            if (state.diffstep != 0)
            {
                goto lbl_75;
            }

            //
            // Analytic gradient
            //
            state.needfg = true;
            state.rstate.stage = 24;
            goto lbl_rcomm;
            lbl_24:
            state.needfg = false;
            state.repnfev = state.repnfev + 1;
            goto lbl_76;
            lbl_75:

            //
            // Numerical differentiation
            //
            state.needf = true;
            state.rstate.stage = 25;
            goto lbl_rcomm;
            lbl_25:
            state.fbase = state.f;
            i = 0;
            lbl_77:
            if (i > n - 1)
            {
                goto lbl_79;
            }
            v = state.x[i];
            b = false;
            if (state.hasbndl[i])
            {
                b = b || v - state.diffstep * state.s[i] < state.bndl[i];
            }
            if (state.hasbndu[i])
            {
                b = b || v + state.diffstep * state.s[i] > state.bndu[i];
            }
            if (b)
            {
                goto lbl_80;
            }
            state.x[i] = v - state.diffstep * state.s[i];
            state.rstate.stage = 26;
            goto lbl_rcomm;
            lbl_26:
            state.fm2 = state.f;
            state.x[i] = v - 0.5 * state.diffstep * state.s[i];
            state.rstate.stage = 27;
            goto lbl_rcomm;
            lbl_27:
            state.fm1 = state.f;
            state.x[i] = v + 0.5 * state.diffstep * state.s[i];
            state.rstate.stage = 28;
            goto lbl_rcomm;
            lbl_28:
            state.fp1 = state.f;
            state.x[i] = v + state.diffstep * state.s[i];
            state.rstate.stage = 29;
            goto lbl_rcomm;
            lbl_29:
            state.fp2 = state.f;
            state.g[i] = (8 * (state.fp1 - state.fm1) - (state.fp2 - state.fm2)) / (6 * state.diffstep * state.s[i]);
            state.repnfev = state.repnfev + 4;
            goto lbl_81;
            lbl_80:
            state.xm1 = v - state.diffstep * state.s[i];
            state.xp1 = v + state.diffstep * state.s[i];
            if (state.hasbndl[i] && state.xm1 < state.bndl[i])
            {
                state.xm1 = state.bndl[i];
            }
            if (state.hasbndu[i] && state.xp1 > state.bndu[i])
            {
                state.xp1 = state.bndu[i];
            }
            state.x[i] = state.xm1;
            state.rstate.stage = 30;
            goto lbl_rcomm;
            lbl_30:
            state.fm1 = state.f;
            state.x[i] = state.xp1;
            state.rstate.stage = 31;
            goto lbl_rcomm;
            lbl_31:
            state.fp1 = state.f;
            if (state.xm1 != state.xp1)
            {
                state.g[i] = (state.fp1 - state.fm1) / (state.xp1 - state.xm1);
            }
            else
            {
                state.g[i] = 0;
            }
            state.repnfev = state.repnfev + 2;
            lbl_81:
            state.x[i] = v;
            i = i + 1;
            goto lbl_77;
            lbl_79:
            state.f = state.fbase;
            state.needf = false;
            lbl_76:
            state.fc = state.f;
            Optimizer.Utilities.TrimPrepare(state.fc, ref state.trimthreshold);
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.gc[i_] = state.g[i_];
            }
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.d[i_] = -state.g[i_];
            }
            ActiveSet.ConstrainedDirection(state.sas, ref state.gc);
            ActiveSet.ConstrainedDirectionPreconditioned(state.sas, ref state.d);
            ginit = 0.0;
            for (i = 0; i <= n - 1; i++)
            {
                ginit = ginit + MathExtensions.Squared(state.gc[i] * state.s[i]);
            }
            ginit = Math.Sqrt(ginit);
            state.k = 0;
            lbl_82:
            if (state.k > n)
            {
                goto lbl_83;
            }

            //
            // Main cycle: prepare to 1-D line search
            //
            state.p = state.k % m;
            state.q = Math.Min(state.k, m - 1);

            //
            // Store X[k], G[k]
            //
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.sk[state.p, i_] = -state.sas.xc[i_];
            }
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.yk[state.p, i_] = -state.gc[i_];
            }

            //
            // Try to use previous scaled step length as initial estimate for new step.
            //
            v = 0;
            for (i = 0; i <= n - 1; i++)
            {
                v = v + MathExtensions.Squared(state.d[i] / state.s[i]);
            }
            v = Math.Sqrt(v);
            if (state.lastscaledgoodstep > 0 && v > 0)
            {
                state.stp = state.lastscaledgoodstep / v;
            }
            else
            {
                state.stp = 1.0;
            }

            //
            // Calculate bound on step length
            //
            ActiveSet.ExploreDirection(state.sas, state.d, ref state.curstpmax, ref state.cidx, ref state.cval);
            state.activationstep = state.curstpmax;
            if (state.cidx >= 0 && state.activationstep == 0)
            {
                goto lbl_83;
            }
            if (state.stpmax > 0)
            {
                v = 0.0;
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    v += state.d[i_] * state.d[i_];
                }
                v = Math.Sqrt(v);
                if (v > 0)
                {
                    state.curstpmax = Math.Min(state.curstpmax, state.stpmax / v);
                }
            }

            //
            // Report beginning of line search (if requested by caller).
            // See description of the MinBLEICState for more information
            // about fields accessible to caller.
            //
            // Caller may do following:
            // * change State.Stp and load better initial estimate of
            //   the step length.
            // Caller may not terminate algorithm.
            //
            if (!state.drep)
            {
                goto lbl_84;
            }
            ClearRequestFields(state);
            state.lsstart = true;
            state.lbfgssearch = true;
            state.boundedstep = state.cidx >= 0;
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.x[i_] = state.sas.xc[i_];
            }
            state.rstate.stage = 32;
            goto lbl_rcomm;
            lbl_32:
            state.lsstart = false;
            lbl_84:

            //
            // Minimize F(x+alpha*d)
            //
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.xn[i_] = state.sas.xc[i_];
            }
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.gn[i_] = state.gc[i_];
            }
            state.fn = state.fc;
            state.mcstage = 0;
            LinMin.McSearch(n, ref state.xn, ref state.fn, ref state.gn, state.d, ref state.stp, state.curstpmax, gtol, ref mcinfo, ref state.nfev, ref state.work, state.lstate, ref state.mcstage);
            lbl_86:
            if (state.mcstage == 0)
            {
                goto lbl_87;
            }

            //
            // Perform correction (constraints are enforced)
            // Copy XN to X
            //
            ActiveSet.Correction(state.sas, state.xn, ref penalty);
            for (i = 0; i <= n - 1; i++)
            {
                state.x[i] = state.xn[i];
            }

            //
            // Gradient, either user-provided or numerical differentiation
            //
            ClearRequestFields(state);
            if (state.diffstep != 0)
            {
                goto lbl_88;
            }

            //
            // Analytic gradient
            //
            state.needfg = true;
            state.rstate.stage = 33;
            goto lbl_rcomm;
            lbl_33:
            state.needfg = false;
            state.repnfev = state.repnfev + 1;
            goto lbl_89;
            lbl_88:

            //
            // Numerical differentiation
            //
            state.needf = true;
            state.rstate.stage = 34;
            goto lbl_rcomm;
            lbl_34:
            state.fbase = state.f;
            i = 0;
            lbl_90:
            if (i > n - 1)
            {
                goto lbl_92;
            }
            v = state.x[i];
            b = false;
            if (state.hasbndl[i])
            {
                b = b || v - state.diffstep * state.s[i] < state.bndl[i];
            }
            if (state.hasbndu[i])
            {
                b = b || v + state.diffstep * state.s[i] > state.bndu[i];
            }
            if (b)
            {
                goto lbl_93;
            }
            state.x[i] = v - state.diffstep * state.s[i];
            state.rstate.stage = 35;
            goto lbl_rcomm;
            lbl_35:
            state.fm2 = state.f;
            state.x[i] = v - 0.5 * state.diffstep * state.s[i];
            state.rstate.stage = 36;
            goto lbl_rcomm;
            lbl_36:
            state.fm1 = state.f;
            state.x[i] = v + 0.5 * state.diffstep * state.s[i];
            state.rstate.stage = 37;
            goto lbl_rcomm;
            lbl_37:
            state.fp1 = state.f;
            state.x[i] = v + state.diffstep * state.s[i];
            state.rstate.stage = 38;
            goto lbl_rcomm;
            lbl_38:
            state.fp2 = state.f;
            state.g[i] = (8 * (state.fp1 - state.fm1) - (state.fp2 - state.fm2)) / (6 * state.diffstep * state.s[i]);
            state.repnfev = state.repnfev + 4;
            goto lbl_94;
            lbl_93:
            state.xm1 = v - state.diffstep * state.s[i];
            state.xp1 = v + state.diffstep * state.s[i];
            if (state.hasbndl[i] && state.xm1 < state.bndl[i])
            {
                state.xm1 = state.bndl[i];
            }
            if (state.hasbndu[i] && state.xp1 > state.bndu[i])
            {
                state.xp1 = state.bndu[i];
            }
            state.x[i] = state.xm1;
            state.rstate.stage = 39;
            goto lbl_rcomm;
            lbl_39:
            state.fm1 = state.f;
            state.x[i] = state.xp1;
            state.rstate.stage = 40;
            goto lbl_rcomm;
            lbl_40:
            state.fp1 = state.f;
            if (state.xm1 != state.xp1)
            {
                state.g[i] = (state.fp1 - state.fm1) / (state.xp1 - state.xm1);
            }
            else
            {
                state.g[i] = 0;
            }
            state.repnfev = state.repnfev + 2;
            lbl_94:
            state.x[i] = v;
            i = i + 1;
            goto lbl_90;
            lbl_92:
            state.f = state.fbase;
            state.needf = false;
            lbl_89:

            //
            // Back to MCSRCH
            //
            // NOTE: penalty term from correction is added to FN in order
            //       to penalize increase in infeasibility.
            //
            state.fn = state.f + penaltyfactor * state.maxscaledgrad * penalty;
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.gn[i_] = state.g[i_];
            }
            ActiveSet.ConstrainedDirection(state.sas, ref state.gn);
            Optimizer.Utilities.TrimFunction(ref state.fn, ref state.gn, n, state.trimthreshold);
            LinMin.McSearch(n, ref state.xn, ref state.fn, ref state.gn, state.d, ref state.stp, state.curstpmax, gtol, ref mcinfo, ref state.nfev, ref state.work, state.lstate, ref state.mcstage);
            goto lbl_86;
            lbl_87:
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.sk[state.p, i_] = state.sk[state.p, i_] + state.xn[i_];
            }
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.yk[state.p, i_] = state.yk[state.p, i_] + state.gn[i_];
            }

            //
            // Handle possible failure of the line search
            //
            if (mcinfo != 1 && mcinfo != 5)
            {
                goto lbl_83;
            }

            //
            // Current point is updated.
            //
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.xp[i_] = state.sas.xc[i_];
            }
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.gp[i_] = state.gc[i_];
            }
            state.fp = state.fc;
            actstatus = ActiveSet.MoveTo(state.sas, state.xn, state.cidx >= 0 && state.stp >= state.activationstep, state.cidx, state.cval);
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.gc[i_] = state.gn[i_];
            }
            state.fc = state.fn;
            if (!state.xrep)
            {
                goto lbl_95;
            }
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.x[i_] = state.sas.xc[i_];
            }
            ClearRequestFields(state);
            state.xupdated = true;
            state.rstate.stage = 41;
            goto lbl_rcomm;
            lbl_41:
            state.xupdated = false;
            lbl_95:
            state.repinneriterationscount = state.repinneriterationscount + 1;

            //
            // Update length of the good step
            //
            if (mcinfo == 1)
            {
                v = 0;
                vv = 0;
                for (i = 0; i <= n - 1; i++)
                {
                    v = v + MathExtensions.Squared((state.sas.xc[i] - state.xp[i]) / state.s[i]);
                    vv = vv + MathExtensions.Squared(state.sas.xc[i] - state.xp[i]);
                }
                state.lastgoodstep = Math.Sqrt(vv);
                UpdateEstimateOfGoodStep(ref state.lastscaledgoodstep, Math.Sqrt(v));
            }

            //
            // Termination of the L-BFGS algorithm:
            // a) line search was performed with activation of constraint
            // b) scaled gradient decreased below GDecay
            // c) iterations counter >= MaxIts
            //
            if (actstatus >= 0)
            {
                goto lbl_83;
            }
            v = 0.0;
            for (i = 0; i <= n - 1; i++)
            {
                v = v + MathExtensions.Squared(state.gc[i] * state.s[i]);
            }
            if (Math.Sqrt(v) < gdecay * ginit)
            {
                goto lbl_83;
            }
            if (state.maxits > 0 && state.repinneriterationscount >= state.maxits)
            {
                goto lbl_83;
            }

            //
            // Update L-BFGS model:
            // * calculate Rho[k]
            // * calculate d(k+1) = -H(k+1)*g(k+1)
            //   (use constrained preconditioner to perform multiplication)
            //
            v = 0.0;
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                v += state.yk[state.p, i_] * state.sk[state.p, i_];
            }
            vv = 0.0;
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                vv += state.yk[state.p, i_] * state.yk[state.p, i_];
            }
            if (v == 0 || vv == 0)
            {
                goto lbl_83;
            }
            state.rho[state.p] = 1 / v;
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.work[i_] = state.gn[i_];
            }
            for (i = state.k; i >= state.k - state.q; i--)
            {
                ic = i % m;
                v = 0.0;
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    v += state.sk[ic, i_] * state.work[i_];
                }
                state.theta[ic] = v;
                vv = v * state.rho[ic];
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    state.work[i_] = state.work[i_] - vv * state.yk[ic, i_];
                }
            }
            ActiveSet.ConstrainedDirectionPreconditioned(state.sas, ref state.work);
            for (i = state.k - state.q; i <= state.k; i++)
            {
                ic = i % m;
                v = 0.0;
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    v += state.yk[ic, i_] * state.work[i_];
                }
                vv = state.rho[ic] * (-v + state.theta[ic]);
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    state.work[i_] = state.work[i_] + vv * state.sk[ic, i_];
                }
            }
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.d[i_] = -state.work[i_];
            }
            state.k = state.k + 1;
            goto lbl_82;
            lbl_83:

            //
            // Decrease decay coefficient. Subsequent L-BFGS stages will
            // have more stringent stopping criteria.
            //
            gdecay = Math.Max(gdecay * decaycorrection, mindecay);
            goto lbl_51;
            lbl_52:
            ActiveSet.StopOptimization(state.sas);
            state.repouteriterationscount = 1;
            result = false;
            return result;

            //
            // Saving state
            //
            lbl_rcomm:
            result = true;
            state.rstate.ia[0] = n;
            state.rstate.ia[1] = m;
            state.rstate.ia[2] = i;
            state.rstate.ia[3] = j;
            state.rstate.ia[4] = badbfgsits;
            state.rstate.ia[5] = nextaction;
            state.rstate.ia[6] = mcinfo;
            state.rstate.ia[7] = actstatus;
            state.rstate.ia[8] = ic;
            state.rstate.ba[0] = b;
            state.rstate.ra[0] = v;
            state.rstate.ra[1] = vv;
            state.rstate.ra[2] = penalty;
            state.rstate.ra[3] = ginit;
            state.rstate.ra[4] = gdecay;
            return result;
        }


        /*************************************************************************
            BLEIC results

            INPUT PARAMETERS:
                State   -   algorithm state

            OUTPUT PARAMETERS:
                X       -   array[0..N-1], solution
                Rep     -   optimization report. You should check Rep.TerminationType
                            in  order  to  distinguish  successful  termination  from
                            unsuccessful one:
                            * -7   gradient verification failed.
                                   See MinBLEICSetGradientCheck() for more information.
                            * -3   inconsistent constraints. Feasible point is
                                   either nonexistent or too hard to find. Try to
                                   restart optimizer with better initial approximation
                            *  1   relative function improvement is no more than EpsF.
                            *  2   scaled step is no more than EpsX.
                            *  4   scaled gradient norm is no more than EpsG.
                            *  5   MaxIts steps was taken
                            More information about fields of this  structure  can  be
                            found in the comments on MinBLEICReport datatype.
           
              -- ALGLIB --
                 Copyright 28.11.2010 by Bochkanov Sergey
            *************************************************************************/
        public static void Results(State state,
            ref FluxList<double> x,
            Report rep)
        {
            x.Clear();

            ResultsBuffered(state, ref x, rep);
        }


        /*************************************************************************
            BLEIC results

            Buffered implementation of MinBLEICResults() which uses pre-allocated buffer
            to store X[]. If buffer size is  too  small,  it  resizes  buffer.  It  is
            intended to be used in the inner cycles of performance critical algorithms
            where array reallocation penalty is too large to be ignored.

              -- ALGLIB --
                 Copyright 28.11.2010 by Bochkanov Sergey
            *************************************************************************/
        public static void ResultsBuffered(State state,
            ref FluxList<double> x,
            Report rep)
        {
            int i = 0;
            int i_ = 0;

            if (x.Count < state.nmain)
            {
                x.Resize(state.nmain);
            }
            rep.iterationscount = state.repinneriterationscount;
            rep.inneriterationscount = state.repinneriterationscount;
            rep.outeriterationscount = state.repouteriterationscount;
            rep.nfev = state.repnfev;
            rep.varidx = state.repvaridx;
            rep.terminationtype = state.repterminationtype;
            if (state.repterminationtype > 0)
            {
                for (i_ = 0; i_ <= state.nmain - 1; i_++)
                {
                    x[i_] = state.sas.xc[i_];
                }
            }
            else
            {
                for (i = 0; i <= state.nmain - 1; i++)
                {
                    x[i] = Double.NaN;
                }
            }
            rep.debugeqerr = state.repdebugeqerr;
            rep.debugfs = state.repdebugfs;
            rep.debugff = state.repdebugff;
            rep.debugdx = state.repdebugdx;
            rep.debugfeasqpits = state.repdebugfeasqpits;
            rep.debugfeasgpaits = state.repdebugfeasgpaits;
        }


        /*************************************************************************
            This subroutine restarts algorithm from new point.
            All optimization parameters (including constraints) are left unchanged.

            This  function  allows  to  solve multiple  optimization  problems  (which
            must have  same number of dimensions) without object reallocation penalty.

            INPUT PARAMETERS:
                State   -   structure previously allocated with MinBLEICCreate call.
                X       -   new starting point.

              -- ALGLIB --
                 Copyright 28.11.2010 by Bochkanov Sergey
            *************************************************************************/
        public static void RestartFrom(State state,
            FluxList<double> x)
        {
            int n = 0;
            int i_ = 0;

            n = state.nmain;

            //
            // First, check for errors in the inputs
            //
            Utilities.Assert(x.Count >= n, "MinBLEICRestartFrom: Length(X)<N");
            Utilities.Assert(Utilities.IsFiniteVector(x, n), "MinBLEICRestartFrom: X contains infinite or NaN values!");

            //
            // Set XC
            //
            for (i_ = 0; i_ <= n - 1; i_++)
            {
                state.xstart[i_] = x[i_];
            }

            //
            // prepare RComm facilities
            //
            state.rstate.ia.Resize(9);
            state.rstate.ba.Resize(1);
            state.rstate.ra.Resize(5);
            state.rstate.stage = -1;
            ClearRequestFields(state);
            ActiveSet.StopOptimization(state.sas);
        }


        /*************************************************************************
            This subroutine finalizes internal structures after emergency  termination
            from State.LSStart report (see comments on MinBLEICState for more information).

            INPUT PARAMETERS:
                State   -   structure after exit from LSStart report

              -- ALGLIB --
                 Copyright 28.11.2010 by Bochkanov Sergey
            *************************************************************************/
        public static void EmergencyTermination(State state)
        {
            ActiveSet.StopOptimization(state.sas);
        }

        /*************************************************************************
            Clears request fileds (to be sure that we don't forget to clear something)
            *************************************************************************/
        private static void ClearRequestFields(State state)
        {
            state.needf = false;
            state.needfg = false;
            state.xupdated = false;
            state.lsstart = false;
        }


        /*************************************************************************
            Internal initialization subroutine
            *************************************************************************/
        private static void InitInternal(int n, FluxList<double> x, double diffstep, State state)
        {
            //
            // Initialize
            //
            state.teststep = 0;
            state.nmain = n;
            state.diffstep = diffstep;
            ActiveSet.Initialize(n, state.sas);
            state.bndl.Resize(n);
            state.hasbndl.Resize(n);
            state.bndu.Resize(n);
            state.hasbndu.Resize(n);
            state.xstart.Resize(n);
            state.gc.Resize(n);
            state.xn.Resize(n);
            state.gn.Resize(n);
            state.xp.Resize(n);
            state.gp.Resize(n);
            state.d.Resize(n);
            state.s.Resize(n);
            state.x.Resize(n);
            state.g.Resize(n);
            state.work.Resize(n);
            for (int i = 0; i <= n - 1; i++)
            {
                state.bndl[i] = Double.NegativeInfinity;
                state.hasbndl[i] = false;
                state.bndu[i] = Double.PositiveInfinity;
                state.hasbndu[i] = false;
                state.s[i] = 1.0;
            }
            SetLinearConstraints(state, state.c, state.ct, 0);
            SetStopConditions(state, 0.0, 0.0, 0.0, 0);
            SetReporting(state, false);
            SetLineSearchReports(state, false);
            SetMaxStepLength(state, 0.0);
            SetPreConditioner(state);
            RestartFrom(state, x);
        }


        /*************************************************************************
            This subroutine updates estimate of the good step length given:
            1) previous estimate
            2) new length of the good step

            It makes sure that estimate does not change too rapidly - ratio of new and
            old estimates will be at least 0.01, at most 100.0

            In case previous estimate of good step is zero (no estimate), new estimate
            is used unconditionally.

              -- ALGLIB --
                 Copyright 16.01.2013 by Bochkanov Sergey
            *************************************************************************/
        private static void UpdateEstimateOfGoodStep(ref double estimate,
            double newstep)
        {
            if (estimate == 0)
            {
                estimate = newstep;
                return;
            }
            if (newstep < estimate * 0.01)
            {
                estimate = estimate * 0.01;
                return;
            }
            if (newstep > estimate * 100)
            {
                estimate = estimate * 100;
                return;
            }
            estimate = newstep;
        }


    }
}