using System;

namespace LxProcessor.Optimizer
{
    public class MathExtensions
    {
        
        public const double MachineEpsilon = 5E-16;
        public const double MaxRealNumber = 1E300;
        public const double MinRealNumber = 1E-300;
        private static readonly Random _random;

        static MathExtensions()
        {
            _random = new Random(DateTime.Now.Millisecond + 1000 * DateTime.Now.Second + 60 * 1000 * DateTime.Now.Minute);
        }
        
        public static bool IsFinite(double d)
        {
            return !Double.IsNaN(d) && !Double.IsInfinity(d);
        }
        
        public static int GetRandomInteger(int N)
        {
            int r = 0;
            lock (_random) { r = _random.Next(N); }
            return r;
        }
        
        public static double Squared(double X)
        {
            return X * X;
        }
    }
}