using System;
using System.Collections.Generic;
using LxCommon.DataStructures;
using LxProcessor.Common;

namespace LxProcessor.Optimizer
{
    public class Ablas
    {
        
        /*************************************************************************
            Splits matrix length in two parts, left part should match ABLAS block size

            INPUT PARAMETERS
                A   -   real matrix, is passed to ensure that we didn't split
                        complex matrix using real splitting subroutine.
                        matrix itself is not changed.
                N   -   length, N>0

            OUTPUT PARAMETERS
                N1  -   length
                N2  -   length

            N1+N2=N, N1>=N2, N2 may be zero

              -- ALGLIB routine --
                 15.12.2009
                 Bochkanov Sergey
            *************************************************************************/
        public static void SplitLength(Matrix<double> a,
            int n,
            ref int n1,
            ref int n2)
        {
            n1 = 0;
            n2 = 0;

            if (n > BlockSize(a))
            {
                InternalSplitLength(n, BlockSize(a), ref n1, ref n2);
            }
            else
            {
                InternalSplitLength(n, MicroBlockSize(), ref n1, ref n2);
            }
        }


        /*************************************************************************
            Returns block size - subdivision size where  cache-oblivious  soubroutines
            switch to the optimized kernel.

            INPUT PARAMETERS
                A   -   real matrix, is passed to ensure that we didn't split
                        complex matrix using real splitting subroutine.
                        matrix itself is not changed.

              -- ALGLIB routine --
                 15.12.2009
                 Bochkanov Sergey
            *************************************************************************/
        public static int BlockSize(Matrix<double> a)
        {
            int result = 0;

            result = 32;
            return result;
        }

        /*************************************************************************
            Microblock size

              -- ALGLIB routine --
                 15.12.2009
                 Bochkanov Sergey
            *************************************************************************/
        private static int MicroBlockSize()
        {
            int result = 0;

            result = 8;
            return result;
        }


        /*************************************************************************
            Copy

            Input parameters:
                M   -   number of rows
                N   -   number of columns
                A   -   source matrix, MxN submatrix is copied and transposed
                IA  -   submatrix offset (row index)
                JA  -   submatrix offset (column index)
                B   -   destination matrix, must be large enough to store result
                IB  -   submatrix offset (row index)
                JB  -   submatrix offset (column index)
            *************************************************************************/
        public static void MatrixCopy(int m,
            int n,
            Matrix<double> a,
            int ia,
            int ja,
            ref Matrix<double> b,
            int ib,
            int jb)
        {
            int i = 0;
            int i_ = 0;
            int i1_ = 0;

            if (m == 0 || n == 0)
            {
                return;
            }
            for (i = 0; i <= m - 1; i++)
            {
                i1_ = (ja) - (jb);
                for (i_ = jb; i_ <= jb + n - 1; i_++)
                {
                    b[ib + i, i_] = a[ia + i, i_ + i1_];
                }
            }
        }


        /*************************************************************************
            Matrix-vector product: y := op(A)*x

            INPUT PARAMETERS:
                M   -   number of rows of op(A)
                N   -   number of columns of op(A)
                A   -   target matrix
                IA  -   submatrix offset (row index)
                JA  -   submatrix offset (column index)
                OpA -   operation type:
                        * OpA=0     =>  op(A) = A
                        * OpA=1     =>  op(A) = A^T
                X   -   input vector
                IX  -   subvector offset
                IY  -   subvector offset
                Y   -   preallocated matrix, must be large enough to store result

            OUTPUT PARAMETERS:
                Y   -   vector which stores result

            if M=0, then subroutine does nothing.
            if N=0, Y is filled by zeros.


              -- ALGLIB routine --

                 28.01.2010
                 Bochkanov Sergey
            *************************************************************************/
        public static void RealVectorProduct(int m,
            int n,
            Matrix<double> a,
            int ia,
            int ja,
            int opa,
            FluxList<double> x,
            int ix,
            ref FluxList<double> y,
            int iy)
        {
            int i = 0;
            double v = 0;
            int i_ = 0;
            int i1_ = 0;

            if (m == 0)
            {
                return;
            }
            if (n == 0)
            {
                for (i = 0; i <= m - 1; i++)
                {
                    y[iy + i] = 0;
                }
                return;
            }
            //if (ablasf.rmatrixmvf(m, n, a, ia, ja, opa, x, ix, ref y, iy))
            //{
            //    return;
            //}
            if (opa == 0)
            {

                //
                // y = A*x
                //
                for (i = 0; i <= m - 1; i++)
                {
                    i1_ = (ix) - (ja);
                    v = 0.0;
                    for (i_ = ja; i_ <= ja + n - 1; i_++)
                    {
                        v += a[ia + i, i_] * x[i_ + i1_];
                    }
                    y[iy + i] = v;
                }
                return;
            }
            if (opa == 1)
            {

                //
                // y = A^T*x
                //
                for (i = 0; i <= m - 1; i++)
                {
                    y[iy + i] = 0;
                }
                for (i = 0; i <= n - 1; i++)
                {
                    v = x[ix + i];
                    i1_ = (ja) - (iy);
                    for (i_ = iy; i_ <= iy + m - 1; i_++)
                    {
                        y[i_] = y[i_] + v * a[ia + i, i_ + i1_];
                    }
                }
                return;
            }
        }

        public static void RealRightTrsm(int m,
            int n,
            Matrix<double> a,
            int i1,
            int j1,
            bool isupper,
            bool isunit,
            int optype,
            Matrix<double> x,
            int i2,
            int j2)
        {
            int s1 = 0;
            int s2 = 0;
            int bs = 0;

            bs = BlockSize(a);
            if (m <= bs && n <= bs)
            {
                RealRightTrsmLevel2(m, n, a, i1, j1, isupper, isunit, optype, x, i2, j2);
                return;
            }
            if (m >= n)
            {

                //
                // Split X: X*A = (X1 X2)^T*A
                //
                SplitLength(a, m, ref s1, ref s2);
                RealRightTrsm(s1, n, a, i1, j1, isupper, isunit, optype, x, i2, j2);
                RealRightTrsm(s2, n, a, i1, j1, isupper, isunit, optype, x, i2 + s1, j2);
                return;
            }
            else
            {

                //
                // Split A:
                //               (A1  A12)
                // X*op(A) = X*op(       )
                //               (     A2)
                //
                // Different variants depending on
                // IsUpper/OpType combinations
                //
                SplitLength(a, n, ref s1, ref s2);
                if (isupper && optype == 0)
                {

                    //
                    //                  (A1  A12)-1
                    // X*A^-1 = (X1 X2)*(       )
                    //                  (     A2)
                    //
                    RealRightTrsm(m, s1, a, i1, j1, isupper, isunit, optype, x, i2, j2);
                    RealGemm(m, s2, s1, -1.0, x, i2, j2, 0, a, i1, j1 + s1, 0, 1.0, x, i2, j2 + s1);
                    RealRightTrsm(m, s2, a, i1 + s1, j1 + s1, isupper, isunit, optype, x, i2, j2 + s1);
                    return;
                }
                if (isupper && optype != 0)
                {

                    //
                    //                  (A1'     )-1
                    // X*A^-1 = (X1 X2)*(        )
                    //                  (A12' A2')
                    //
                    RealRightTrsm(m, s2, a, i1 + s1, j1 + s1, isupper, isunit, optype, x, i2, j2 + s1);
                    RealGemm(m, s1, s2, -1.0, x, i2, j2 + s1, 0, a, i1, j1 + s1, optype, 1.0, x, i2, j2);
                    RealRightTrsm(m, s1, a, i1, j1, isupper, isunit, optype, x, i2, j2);
                    return;
                }
                if (!isupper && optype == 0)
                {

                    //
                    //                  (A1     )-1
                    // X*A^-1 = (X1 X2)*(       )
                    //                  (A21  A2)
                    //
                    RealRightTrsm(m, s2, a, i1 + s1, j1 + s1, isupper, isunit, optype, x, i2, j2 + s1);
                    RealGemm(m, s1, s2, -1.0, x, i2, j2 + s1, 0, a, i1 + s1, j1, 0, 1.0, x, i2, j2);
                    RealRightTrsm(m, s1, a, i1, j1, isupper, isunit, optype, x, i2, j2);
                    return;
                }
                if (!isupper && optype != 0)
                {

                    //
                    //                  (A1' A21')-1
                    // X*A^-1 = (X1 X2)*(        )
                    //                  (     A2')
                    //
                    RealRightTrsm(m, s1, a, i1, j1, isupper, isunit, optype, x, i2, j2);
                    RealGemm(m, s2, s1, -1.0, x, i2, j2, 0, a, i1 + s1, j1, optype, 1.0, x, i2, j2 + s1);
                    RealRightTrsm(m, s2, a, i1 + s1, j1 + s1, isupper, isunit, optype, x, i2, j2 + s1);
                    return;
                }
            }
        }


        public static void RealLeftTrSm(int m,
            int n,
            Matrix<double> a,
            int i1,
            int j1,
            bool isupper,
            bool isunit,
            int optype,
            Matrix<double> x,
            int i2,
            int j2)
        {
            int s1 = 0;
            int s2 = 0;
            int bs = 0;

            bs = BlockSize(a);
            if (m <= bs && n <= bs)
            {
                RealLeftTrsmLevel2(m, n, a, i1, j1, isupper, isunit, optype, x, i2, j2);
                return;
            }
            if (n >= m)
            {

                //
                // Split X: op(A)^-1*X = op(A)^-1*(X1 X2)
                //
                SplitLength(x, n, ref s1, ref s2);
                RealLeftTrSm(m, s1, a, i1, j1, isupper, isunit, optype, x, i2, j2);
                RealLeftTrSm(m, s2, a, i1, j1, isupper, isunit, optype, x, i2, j2 + s1);
            }
            else
            {

                //
                // Split A
                //
                SplitLength(a, m, ref s1, ref s2);
                if (isupper && optype == 0)
                {

                    //
                    //           (A1  A12)-1  ( X1 )
                    // A^-1*X* = (       )   *(    )
                    //           (     A2)    ( X2 )
                    //
                    RealLeftTrSm(s2, n, a, i1 + s1, j1 + s1, isupper, isunit, optype, x, i2 + s1, j2);
                    RealGemm(s1, n, s2, -1.0, a, i1, j1 + s1, 0, x, i2 + s1, j2, 0, 1.0, x, i2, j2);
                    RealLeftTrSm(s1, n, a, i1, j1, isupper, isunit, optype, x, i2, j2);
                    return;
                }
                if (isupper && optype != 0)
                {

                    //
                    //          (A1'     )-1 ( X1 )
                    // A^-1*X = (        )  *(    )
                    //          (A12' A2')   ( X2 )
                    //
                    RealLeftTrSm(s1, n, a, i1, j1, isupper, isunit, optype, x, i2, j2);
                    RealGemm(s2, n, s1, -1.0, a, i1, j1 + s1, optype, x, i2, j2, 0, 1.0, x, i2 + s1, j2);
                    RealLeftTrSm(s2, n, a, i1 + s1, j1 + s1, isupper, isunit, optype, x, i2 + s1, j2);
                    return;
                }
                if (!isupper && optype == 0)
                {

                    //
                    //          (A1     )-1 ( X1 )
                    // A^-1*X = (       )  *(    )
                    //          (A21  A2)   ( X2 )
                    //
                    RealLeftTrSm(s1, n, a, i1, j1, isupper, isunit, optype, x, i2, j2);
                    RealGemm(s2, n, s1, -1.0, a, i1 + s1, j1, 0, x, i2, j2, 0, 1.0, x, i2 + s1, j2);
                    RealLeftTrSm(s2, n, a, i1 + s1, j1 + s1, isupper, isunit, optype, x, i2 + s1, j2);
                    return;
                }
                if (!isupper && optype != 0)
                {

                    //
                    //          (A1' A21')-1 ( X1 )
                    // A^-1*X = (        )  *(    )
                    //          (     A2')   ( X2 )
                    //
                    RealLeftTrSm(s2, n, a, i1 + s1, j1 + s1, isupper, isunit, optype, x, i2 + s1, j2);
                    RealGemm(s1, n, s2, -1.0, a, i1 + s1, j1, optype, x, i2 + s1, j2, 0, 1.0, x, i2, j2);
                    RealLeftTrSm(s1, n, a, i1, j1, isupper, isunit, optype, x, i2, j2);
                    return;
                }
            }
        }


        public static void RealSyrk(int n,
            int k,
            double alpha,
            Matrix<double> a,
            int ia,
            int ja,
            int optypea,
            double beta,
            Matrix<double> c,
            int ic,
            int jc,
            bool isupper)
        {
            int s1 = 0;
            int s2 = 0;
            int bs = 0;

            bs = BlockSize(a);

            //
            // Use MKL or generic basecase code
            //
            //if (ablasmkl.rmatrixsyrkmkl(n, k, alpha, a, ia, ja, optypea, beta, c, ic, jc, isupper))
            //{
            //    return;
            //}
            if (n <= bs && k <= bs)
            {
                RealSyrkLevel2(n, k, alpha, a, ia, ja, optypea, beta, c, ic, jc, isupper);
                return;
            }

            //
            // Recursive subdivision of the problem
            //
            if (k >= n)
            {

                //
                // Split K
                //
                SplitLength(a, k, ref s1, ref s2);
                if (optypea == 0)
                {
                    RealSyrk(n, s1, alpha, a, ia, ja, optypea, beta, c, ic, jc, isupper);
                    RealSyrk(n, s2, alpha, a, ia, ja + s1, optypea, 1.0, c, ic, jc, isupper);
                }
                else
                {
                    RealSyrk(n, s1, alpha, a, ia, ja, optypea, beta, c, ic, jc, isupper);
                    RealSyrk(n, s2, alpha, a, ia + s1, ja, optypea, 1.0, c, ic, jc, isupper);
                }
            }
            else
            {

                //
                // Split N
                //
                SplitLength(a, n, ref s1, ref s2);
                if (optypea == 0 && isupper)
                {
                    RealSyrk(s1, k, alpha, a, ia, ja, optypea, beta, c, ic, jc, isupper);
                    RealGemm(s1, s2, k, alpha, a, ia, ja, 0, a, ia + s1, ja, 1, beta, c, ic, jc + s1);
                    RealSyrk(s2, k, alpha, a, ia + s1, ja, optypea, beta, c, ic + s1, jc + s1, isupper);
                    return;
                }
                if (optypea == 0 && !isupper)
                {
                    RealSyrk(s1, k, alpha, a, ia, ja, optypea, beta, c, ic, jc, isupper);
                    RealGemm(s2, s1, k, alpha, a, ia + s1, ja, 0, a, ia, ja, 1, beta, c, ic + s1, jc);
                    RealSyrk(s2, k, alpha, a, ia + s1, ja, optypea, beta, c, ic + s1, jc + s1, isupper);
                    return;
                }
                if (optypea != 0 && isupper)
                {
                    RealSyrk(s1, k, alpha, a, ia, ja, optypea, beta, c, ic, jc, isupper);
                    RealGemm(s1, s2, k, alpha, a, ia, ja, 1, a, ia, ja + s1, 0, beta, c, ic, jc + s1);
                    RealSyrk(s2, k, alpha, a, ia, ja + s1, optypea, beta, c, ic + s1, jc + s1, isupper);
                    return;
                }
                if (optypea != 0 && !isupper)
                {
                    RealSyrk(s1, k, alpha, a, ia, ja, optypea, beta, c, ic, jc, isupper);
                    RealGemm(s2, s1, k, alpha, a, ia, ja + s1, 1, a, ia, ja, 0, beta, c, ic + s1, jc);
                    RealSyrk(s2, k, alpha, a, ia, ja + s1, optypea, beta, c, ic + s1, jc + s1, isupper);
                    return;
                }
            }
        }


        public static void RealGemm(int m,
            int n,
            int k,
            double alpha,
            Matrix<double> a,
            int ia,
            int ja,
            int optypea,
            Matrix<double> b,
            int ib,
            int jb,
            int optypeb,
            double beta,
            Matrix<double> c,
            int ic,
            int jc)
        {
            int s1 = 0;
            int s2 = 0;
            int bs = 0;

            bs = BlockSize(a);

            //
            // Check input sizes for correctness
            //
            Utilities.Assert(optypea == 0 || optypea == 1, "RMatrixGEMM: incorrect OpTypeA (must be 0 or 1)");
            Utilities.Assert(optypeb == 0 || optypeb == 1, "RMatrixGEMM: incorrect OpTypeB (must be 0 or 1)");
            Utilities.Assert(ic + m <= c.Rows, "RMatrixGEMM: incorect size of output matrix C");
            Utilities.Assert(jc + n <= c.Columns, "RMatrixGEMM: incorect size of output matrix C");

            //
            // Use MKL or ALGLIB basecase code
            //
            //if (ablasmkl.rmatrixgemmmkl(m, n, k, alpha, a, ia, ja, optypea, b, ib, jb, optypeb, beta, c, ic, jc))
            //{
            //    return;
            //}
            if ((m <= bs && n <= bs) && k <= bs)
            {
                RMatrixGemm(m, n, k, alpha, a, ia, ja, optypea, b, ib, jb, optypeb, beta, c, ic, jc);
                return;
            }

            //
            // SMP support is turned on when M or N are larger than some boundary value.
            // Magnitude of K is not taken into account because splitting on K does not
            // allow us to spawn child tasks.
            //

            //
            // Recursive algorithm: split on M or N
            //
            if (m >= n && m >= k)
            {

                //
                // A*B = (A1 A2)^T*B
                //
                SplitLength(a, m, ref s1, ref s2);
                if (optypea == 0)
                {
                    RealGemm(s1, n, k, alpha, a, ia, ja, optypea, b, ib, jb, optypeb, beta, c, ic, jc);
                    RealGemm(s2, n, k, alpha, a, ia + s1, ja, optypea, b, ib, jb, optypeb, beta, c, ic + s1, jc);
                }
                else
                {
                    RealGemm(s1, n, k, alpha, a, ia, ja, optypea, b, ib, jb, optypeb, beta, c, ic, jc);
                    RealGemm(s2, n, k, alpha, a, ia, ja + s1, optypea, b, ib, jb, optypeb, beta, c, ic + s1, jc);
                }
                return;
            }
            if (n >= m && n >= k)
            {

                //
                // A*B = A*(B1 B2)
                //
                SplitLength(a, n, ref s1, ref s2);
                if (optypeb == 0)
                {
                    RealGemm(m, s1, k, alpha, a, ia, ja, optypea, b, ib, jb, optypeb, beta, c, ic, jc);
                    RealGemm(m, s2, k, alpha, a, ia, ja, optypea, b, ib, jb + s1, optypeb, beta, c, ic, jc + s1);
                }
                else
                {
                    RealGemm(m, s1, k, alpha, a, ia, ja, optypea, b, ib, jb, optypeb, beta, c, ic, jc);
                    RealGemm(m, s2, k, alpha, a, ia, ja, optypea, b, ib + s1, jb, optypeb, beta, c, ic, jc + s1);
                }
                return;
            }

            //
            // Recursive algorithm: split on K
            //

            //
            // A*B = (A1 A2)*(B1 B2)^T
            //
            SplitLength(a, k, ref s1, ref s2);
            if (optypea == 0 && optypeb == 0)
            {
                RealGemm(m, n, s1, alpha, a, ia, ja, optypea, b, ib, jb, optypeb, beta, c, ic, jc);
                RealGemm(m, n, s2, alpha, a, ia, ja + s1, optypea, b, ib + s1, jb, optypeb, 1.0, c, ic, jc);
            }
            if (optypea == 0 && optypeb != 0)
            {
                RealGemm(m, n, s1, alpha, a, ia, ja, optypea, b, ib, jb, optypeb, beta, c, ic, jc);
                RealGemm(m, n, s2, alpha, a, ia, ja + s1, optypea, b, ib, jb + s1, optypeb, 1.0, c, ic, jc);
            }
            if (optypea != 0 && optypeb == 0)
            {
                RealGemm(m, n, s1, alpha, a, ia, ja, optypea, b, ib, jb, optypeb, beta, c, ic, jc);
                RealGemm(m, n, s2, alpha, a, ia + s1, ja, optypea, b, ib + s1, jb, optypeb, 1.0, c, ic, jc);
            }
            if (optypea != 0 && optypeb != 0)
            {
                RealGemm(m, n, s1, alpha, a, ia, ja, optypea, b, ib, jb, optypeb, beta, c, ic, jc);
                RealGemm(m, n, s2, alpha, a, ia + s1, ja, optypea, b, ib, jb + s1, optypeb, 1.0, c, ic, jc);
            }
            return;
        }

     


        /*************************************************************************
            Complex ABLASSplitLength

              -- ALGLIB routine --
                 15.12.2009
                 Bochkanov Sergey
            *************************************************************************/
        private static void InternalSplitLength(int n,
            int nb,
            ref int n1,
            ref int n2)
        {
            int r = 0;

            n1 = 0;
            n2 = 0;

            if (n <= nb)
            {

                //
                // Block size, no further splitting
                //
                n1 = n;
                n2 = 0;
            }
            else
            {

                //
                // Greater than block size
                //
                if (n % nb != 0)
                {

                    //
                    // Split remainder
                    //
                    n2 = n % nb;
                    n1 = n - n2;
                }
                else
                {

                    //
                    // Split on block boundaries
                    //
                    n2 = n / 2;
                    n1 = n - n2;
                    if (n1 % nb == 0)
                    {
                        return;
                    }
                    r = nb - n1 % nb;
                    n1 = n1 + r;
                    n2 = n2 - r;
                }
            }
        }


        /*************************************************************************
            Level 2 subroutine

              -- ALGLIB routine --
                 15.12.2009
                 Bochkanov Sergey
            *************************************************************************/
        private static void RealRightTrsmLevel2(int m,
            int n,
            Matrix<double> a,
            int i1,
            int j1,
            bool isupper,
            bool isunit,
            int optype,
            Matrix<double> x,
            int i2,
            int j2)
        {
            int i = 0;
            int j = 0;
            double vr = 0;
            double vd = 0;
            int i_ = 0;
            int i1_ = 0;


            //
            // Special case
            //
            if (n * m == 0)
            {
                return;
            }

            //
            // Try to use "fast" code
            //
            //if (ablasf.rmatrixrighttrsmf(m, n, a, i1, j1, isupper, isunit, optype, x, i2, j2))
            //{
            //    return;
            //}

            //
            // General case
            //
            if (isupper)
            {

                //
                // Upper triangular matrix
                //
                if (optype == 0)
                {

                    //
                    // X*A^(-1)
                    //
                    for (i = 0; i <= m - 1; i++)
                    {
                        for (j = 0; j <= n - 1; j++)
                        {
                            if (isunit)
                            {
                                vd = 1;
                            }
                            else
                            {
                                vd = a[i1 + j, j1 + j];
                            }
                            x[i2 + i, j2 + j] = x[i2 + i, j2 + j] / vd;
                            if (j < n - 1)
                            {
                                vr = x[i2 + i, j2 + j];
                                i1_ = (j1 + j + 1) - (j2 + j + 1);
                                for (i_ = j2 + j + 1; i_ <= j2 + n - 1; i_++)
                                {
                                    x[i2 + i, i_] = x[i2 + i, i_] - vr * a[i1 + j, i_ + i1_];
                                }
                            }
                        }
                    }
                    return;
                }
                if (optype == 1)
                {

                    //
                    // X*A^(-T)
                    //
                    for (i = 0; i <= m - 1; i++)
                    {
                        for (j = n - 1; j >= 0; j--)
                        {
                            vr = 0;
                            vd = 1;
                            if (j < n - 1)
                            {
                                i1_ = (j1 + j + 1) - (j2 + j + 1);
                                vr = 0.0;
                                for (i_ = j2 + j + 1; i_ <= j2 + n - 1; i_++)
                                {
                                    vr += x[i2 + i, i_] * a[i1 + j, i_ + i1_];
                                }
                            }
                            if (!isunit)
                            {
                                vd = a[i1 + j, j1 + j];
                            }
                            x[i2 + i, j2 + j] = (x[i2 + i, j2 + j] - vr) / vd;
                        }
                    }
                    return;
                }
            }
            else
            {

                //
                // Lower triangular matrix
                //
                if (optype == 0)
                {

                    //
                    // X*A^(-1)
                    //
                    for (i = 0; i <= m - 1; i++)
                    {
                        for (j = n - 1; j >= 0; j--)
                        {
                            if (isunit)
                            {
                                vd = 1;
                            }
                            else
                            {
                                vd = a[i1 + j, j1 + j];
                            }
                            x[i2 + i, j2 + j] = x[i2 + i, j2 + j] / vd;
                            if (j > 0)
                            {
                                vr = x[i2 + i, j2 + j];
                                i1_ = (j1) - (j2);
                                for (i_ = j2; i_ <= j2 + j - 1; i_++)
                                {
                                    x[i2 + i, i_] = x[i2 + i, i_] - vr * a[i1 + j, i_ + i1_];
                                }
                            }
                        }
                    }
                    return;
                }
                if (optype == 1)
                {

                    //
                    // X*A^(-T)
                    //
                    for (i = 0; i <= m - 1; i++)
                    {
                        for (j = 0; j <= n - 1; j++)
                        {
                            vr = 0;
                            vd = 1;
                            if (j > 0)
                            {
                                i1_ = (j1) - (j2);
                                vr = 0.0;
                                for (i_ = j2; i_ <= j2 + j - 1; i_++)
                                {
                                    vr += x[i2 + i, i_] * a[i1 + j, i_ + i1_];
                                }
                            }
                            if (!isunit)
                            {
                                vd = a[i1 + j, j1 + j];
                            }
                            x[i2 + i, j2 + j] = (x[i2 + i, j2 + j] - vr) / vd;
                        }
                    }
                    return;
                }
            }
        }


        /*************************************************************************
            Level 2 subroutine
            *************************************************************************/
        private static void RealLeftTrsmLevel2(int m,
            int n,
            Matrix<double> a,
            int i1,
            int j1,
            bool isupper,
            bool isunit,
            int optype,
            Matrix<double> x,
            int i2,
            int j2)
        {
            int i = 0;
            int j = 0;
            double vr = 0;
            double vd = 0;
            int i_ = 0;


            //
            // Special case
            //
            if (n == 0 || m == 0)
            {
                return;
            }

            //
            // Try fast code
            //
            //if (ablasf.rmatrixlefttrsmf(m, n, a, i1, j1, isupper, isunit, optype, x, i2, j2))
            //{
            //    return;
            //}

            //
            // General case
            //
            if (isupper)
            {

                //
                // Upper triangular matrix
                //
                if (optype == 0)
                {

                    //
                    // A^(-1)*X
                    //
                    for (i = m - 1; i >= 0; i--)
                    {
                        for (j = i + 1; j <= m - 1; j++)
                        {
                            vr = a[i1 + i, j1 + j];
                            for (i_ = j2; i_ <= j2 + n - 1; i_++)
                            {
                                x[i2 + i, i_] = x[i2 + i, i_] - vr * x[i2 + j, i_];
                            }
                        }
                        if (!isunit)
                        {
                            vd = 1 / a[i1 + i, j1 + i];
                            for (i_ = j2; i_ <= j2 + n - 1; i_++)
                            {
                                x[i2 + i, i_] = vd * x[i2 + i, i_];
                            }
                        }
                    }
                    return;
                }
                if (optype == 1)
                {

                    //
                    // A^(-T)*X
                    //
                    for (i = 0; i <= m - 1; i++)
                    {
                        if (isunit)
                        {
                            vd = 1;
                        }
                        else
                        {
                            vd = 1 / a[i1 + i, j1 + i];
                        }
                        for (i_ = j2; i_ <= j2 + n - 1; i_++)
                        {
                            x[i2 + i, i_] = vd * x[i2 + i, i_];
                        }
                        for (j = i + 1; j <= m - 1; j++)
                        {
                            vr = a[i1 + i, j1 + j];
                            for (i_ = j2; i_ <= j2 + n - 1; i_++)
                            {
                                x[i2 + j, i_] = x[i2 + j, i_] - vr * x[i2 + i, i_];
                            }
                        }
                    }
                    return;
                }
            }
            else
            {

                //
                // Lower triangular matrix
                //
                if (optype == 0)
                {

                    //
                    // A^(-1)*X
                    //
                    for (i = 0; i <= m - 1; i++)
                    {
                        for (j = 0; j <= i - 1; j++)
                        {
                            vr = a[i1 + i, j1 + j];
                            for (i_ = j2; i_ <= j2 + n - 1; i_++)
                            {
                                x[i2 + i, i_] = x[i2 + i, i_] - vr * x[i2 + j, i_];
                            }
                        }
                        if (isunit)
                        {
                            vd = 1;
                        }
                        else
                        {
                            vd = 1 / a[i1 + j, j1 + j];
                        }
                        for (i_ = j2; i_ <= j2 + n - 1; i_++)
                        {
                            x[i2 + i, i_] = vd * x[i2 + i, i_];
                        }
                    }
                    return;
                }
                if (optype == 1)
                {

                    //
                    // A^(-T)*X
                    //
                    for (i = m - 1; i >= 0; i--)
                    {
                        if (isunit)
                        {
                            vd = 1;
                        }
                        else
                        {
                            vd = 1 / a[i1 + i, j1 + i];
                        }
                        for (i_ = j2; i_ <= j2 + n - 1; i_++)
                        {
                            x[i2 + i, i_] = vd * x[i2 + i, i_];
                        }
                        for (j = i - 1; j >= 0; j--)
                        {
                            vr = a[i1 + i, j1 + j];
                            for (i_ = j2; i_ <= j2 + n - 1; i_++)
                            {
                                x[i2 + j, i_] = x[i2 + j, i_] - vr * x[i2 + i, i_];
                            }
                        }
                    }
                    return;
                }
            }
        }




        /*************************************************************************
            Level 2 subrotuine
            *************************************************************************/
        private static void RealSyrkLevel2(int n,
            int k,
            double alpha,
            Matrix<double> a,
            int ia,
            int ja,
            int optypea,
            double beta,
            Matrix<double> c,
            int ic,
            int jc,
            bool isupper)
        {
            int i = 0;
            int j = 0;
            int j1 = 0;
            int j2 = 0;
            double v = 0;
            int i_ = 0;
            int i1_ = 0;


            //
            // Fast exit (nothing to be done)
            //
            if ((alpha == 0 || k == 0) && beta == 1)
            {
                return;
            }

            //
            // Try to call fast SYRK
            //
            //if (ablasf.rmatrixsyrkf(n, k, alpha, a, ia, ja, optypea, beta, c, ic, jc, isupper))
            //{
            //    return;
            //}

            //
            // SYRK
            //
            if (optypea == 0)
            {

                //
                // C=alpha*A*A^H+beta*C
                //
                for (i = 0; i <= n - 1; i++)
                {
                    if (isupper)
                    {
                        j1 = i;
                        j2 = n - 1;
                    }
                    else
                    {
                        j1 = 0;
                        j2 = i;
                    }
                    for (j = j1; j <= j2; j++)
                    {
                        if (alpha != 0 && k > 0)
                        {
                            v = 0.0;
                            for (i_ = ja; i_ <= ja + k - 1; i_++)
                            {
                                v += a[ia + i, i_] * a[ia + j, i_];
                            }
                        }
                        else
                        {
                            v = 0;
                        }
                        if (beta == 0)
                        {
                            c[ic + i, jc + j] = alpha * v;
                        }
                        else
                        {
                            c[ic + i, jc + j] = beta * c[ic + i, jc + j] + alpha * v;
                        }
                    }
                }
                return;
            }
            else
            {

                //
                // C=alpha*A^H*A+beta*C
                //
                for (i = 0; i <= n - 1; i++)
                {
                    if (isupper)
                    {
                        j1 = i;
                        j2 = n - 1;
                    }
                    else
                    {
                        j1 = 0;
                        j2 = i;
                    }
                    if (beta == 0)
                    {
                        for (j = j1; j <= j2; j++)
                        {
                            c[ic + i, jc + j] = 0;
                        }
                    }
                    else
                    {
                        for (i_ = jc + j1; i_ <= jc + j2; i_++)
                        {
                            c[ic + i, i_] = beta * c[ic + i, i_];
                        }
                    }
                }
                for (i = 0; i <= k - 1; i++)
                {
                    for (j = 0; j <= n - 1; j++)
                    {
                        if (isupper)
                        {
                            j1 = j;
                            j2 = n - 1;
                        }
                        else
                        {
                            j1 = 0;
                            j2 = j;
                        }
                        v = alpha * a[ia + i, ja + j];
                        i1_ = (ja + j1) - (jc + j1);
                        for (i_ = jc + j1; i_ <= jc + j2; i_++)
                        {
                            c[ic + j, i_] = c[ic + j, i_] + v * a[ia + i, i_ + i1_];
                        }
                    }
                }
                return;
            }
        }


        /*************************************************************************
        RMatrixGEMM kernel, basecase code for RMatrixGEMM.

        This subroutine calculates C = alpha*op1(A)*op2(B) +beta*C where:
        * C is MxN general matrix
        * op1(A) is MxK matrix
        * op2(B) is KxN matrix
        * "op" may be identity transformation, transposition

        Additional info:
        * multiplication result replaces C. If Beta=0, C elements are not used in
          calculations (not multiplied by zero - just not referenced)
        * if Alpha=0, A is not used (not multiplied by zero - just not referenced)
        * if both Beta and Alpha are zero, C is filled by zeros.

        IMPORTANT:

        This function does NOT preallocate output matrix C, it MUST be preallocated
        by caller prior to calling this function. In case C does not have  enough
        space to store result, exception will be generated.

        INPUT PARAMETERS
            M       -   matrix size, M>0
            N       -   matrix size, N>0
            K       -   matrix size, K>0
            Alpha   -   coefficient
            A       -   matrix
            IA      -   submatrix offset
            JA      -   submatrix offset
            OpTypeA -   transformation type:
                        * 0 - no transformation
                        * 1 - transposition
            B       -   matrix
            IB      -   submatrix offset
            JB      -   submatrix offset
            OpTypeB -   transformation type:
                        * 0 - no transformation
                        * 1 - transposition
            Beta    -   coefficient
            C       -   PREALLOCATED output matrix
            IC      -   submatrix offset
            JC      -   submatrix offset

          -- ALGLIB routine --
             27.03.2013
             Bochkanov Sergey
        *************************************************************************/
        private static void RMatrixGemm(int m,
            int n,
            int k,
            double alpha,
            Matrix<double> a,
            int ia,
            int ja,
            int optypea,
            Matrix<double> b,
            int ib,
            int jb,
            int optypeb,
            double beta,
            Matrix<double> c,
            int ic,
            int jc)
        {
            int i = 0;
            int j = 0;


            //
            // if matrix size is zero
            //
            if (m == 0 || n == 0)
            {
                return;
            }

            ////
            //// Try optimized code
            ////
            //if (rmatrixgemmf(m, n, k, alpha, a, ia, ja, optypea, b, ib, jb, optypeb, beta, c, ic, jc))
            //{
            //    return;
            //}

            //
            // if K=0, then C=Beta*C
            //
            if (k == 0 || alpha == 0)
            {
                if (beta != 1)
                {
                    if (beta != 0)
                    {
                        for (i = 0; i <= m - 1; i++)
                        {
                            for (j = 0; j <= n - 1; j++)
                            {
                                c[ic + i, jc + j] = beta * c[ic + i, jc + j];
                            }
                        }
                    }
                    else
                    {
                        for (i = 0; i <= m - 1; i++)
                        {
                            for (j = 0; j <= n - 1; j++)
                            {
                                c[ic + i, jc + j] = 0;
                            }
                        }
                    }
                }
                return;
            }

            //
            // Call specialized code.
            //
            // NOTE: specialized code was moved to separate function because of strange
            //       issues with instructions cache on some systems; Having too long
            //       functions significantly slows down internal loop of the algorithm.
            //
            if (optypea == 0 && optypeb == 0)
            {
                RMatrixGemmK44V00(m, n, k, alpha, a, ia, ja, b, ib, jb, beta, c, ic, jc);
            }
            if (optypea == 0 && optypeb != 0)
            {
                RMatrixGemmK44V01(m, n, k, alpha, a, ia, ja, b, ib, jb, beta, c, ic, jc);
            }
            if (optypea != 0 && optypeb == 0)
            {
                RMatrixGemmK44V10(m, n, k, alpha, a, ia, ja, b, ib, jb, beta, c, ic, jc);
            }
            if (optypea != 0 && optypeb != 0)
            {
                RMatrixGemmK44V11(m, n, k, alpha, a, ia, ja, b, ib, jb, beta, c, ic, jc);
            }
        }

        /*************************************************************************
           RMatrixGEMM kernel, basecase code for RMatrixGEMM, specialized for sitation
           with OpTypeA=0 and OpTypeB=0.

           Additional info:
           * this function requires that Alpha<>0 (assertion is thrown otherwise)

           INPUT PARAMETERS
               M       -   matrix size, M>0
               N       -   matrix size, N>0
               K       -   matrix size, K>0
               Alpha   -   coefficient
               A       -   matrix
               IA      -   submatrix offset
               JA      -   submatrix offset
               B       -   matrix
               IB      -   submatrix offset
               JB      -   submatrix offset
               Beta    -   coefficient
               C       -   PREALLOCATED output matrix
               IC      -   submatrix offset
               JC      -   submatrix offset

             -- ALGLIB routine --
                27.03.2013
                Bochkanov Sergey
           *************************************************************************/
        private static void RMatrixGemmK44V00(int m,
            int n,
            int k,
            double alpha,
            Matrix<double> a,
            int ia,
            int ja,
            Matrix<double> b,
            int ib,
            int jb,
            double beta,
            Matrix<double> c,
            int ic,
            int jc)
        {
            int i = 0;
            int j = 0;
            double v = 0;
            double v00 = 0;
            double v01 = 0;
            double v02 = 0;
            double v03 = 0;
            double v10 = 0;
            double v11 = 0;
            double v12 = 0;
            double v13 = 0;
            double v20 = 0;
            double v21 = 0;
            double v22 = 0;
            double v23 = 0;
            double v30 = 0;
            double v31 = 0;
            double v32 = 0;
            double v33 = 0;
            double a0 = 0;
            double a1 = 0;
            double a2 = 0;
            double a3 = 0;
            double b0 = 0;
            double b1 = 0;
            double b2 = 0;
            double b3 = 0;
            int idxa0 = 0;
            int idxa1 = 0;
            int idxa2 = 0;
            int idxa3 = 0;
            int idxb0 = 0;
            int idxb1 = 0;
            int idxb2 = 0;
            int idxb3 = 0;
            int i0 = 0;
            int i1 = 0;
            int ik = 0;
            int j0 = 0;
            int j1 = 0;
            int jk = 0;
            int t = 0;
            int offsa = 0;
            int offsb = 0;
            int i_ = 0;
            int i1_ = 0;

            Utilities.Assert(alpha != 0, "RMatrixGEMMK44V00: internal error (Alpha=0)");

            //
            // if matrix size is zero
            //
            if (m == 0 || n == 0)
            {
                return;
            }

            //
            // A*B
            //
            i = 0;
            while (i < m)
            {
                j = 0;
                while (j < n)
                {

                    //
                    // Choose between specialized 4x4 code and general code
                    //
                    if (i + 4 <= m && j + 4 <= n)
                    {

                        //
                        // Specialized 4x4 code for [I..I+3]x[J..J+3] submatrix of C.
                        //
                        // This submatrix is calculated as sum of K rank-1 products,
                        // with operands cached in local variables in order to speed
                        // up operations with arrays.
                        //
                        idxa0 = ia + i + 0;
                        idxa1 = ia + i + 1;
                        idxa2 = ia + i + 2;
                        idxa3 = ia + i + 3;
                        offsa = ja;
                        idxb0 = jb + j + 0;
                        idxb1 = jb + j + 1;
                        idxb2 = jb + j + 2;
                        idxb3 = jb + j + 3;
                        offsb = ib;
                        v00 = 0.0;
                        v01 = 0.0;
                        v02 = 0.0;
                        v03 = 0.0;
                        v10 = 0.0;
                        v11 = 0.0;
                        v12 = 0.0;
                        v13 = 0.0;
                        v20 = 0.0;
                        v21 = 0.0;
                        v22 = 0.0;
                        v23 = 0.0;
                        v30 = 0.0;
                        v31 = 0.0;
                        v32 = 0.0;
                        v33 = 0.0;

                        //
                        // Different variants of internal loop
                        //
                        for (t = 0; t <= k - 1; t++)
                        {
                            a0 = a[idxa0, offsa];
                            a1 = a[idxa1, offsa];
                            b0 = b[offsb, idxb0];
                            b1 = b[offsb, idxb1];
                            v00 = v00 + a0 * b0;
                            v01 = v01 + a0 * b1;
                            v10 = v10 + a1 * b0;
                            v11 = v11 + a1 * b1;
                            a2 = a[idxa2, offsa];
                            a3 = a[idxa3, offsa];
                            v20 = v20 + a2 * b0;
                            v21 = v21 + a2 * b1;
                            v30 = v30 + a3 * b0;
                            v31 = v31 + a3 * b1;
                            b2 = b[offsb, idxb2];
                            b3 = b[offsb, idxb3];
                            v22 = v22 + a2 * b2;
                            v23 = v23 + a2 * b3;
                            v32 = v32 + a3 * b2;
                            v33 = v33 + a3 * b3;
                            v02 = v02 + a0 * b2;
                            v03 = v03 + a0 * b3;
                            v12 = v12 + a1 * b2;
                            v13 = v13 + a1 * b3;
                            offsa = offsa + 1;
                            offsb = offsb + 1;
                        }
                        if (beta == 0)
                        {
                            c[ic + i + 0, jc + j + 0] = alpha * v00;
                            c[ic + i + 0, jc + j + 1] = alpha * v01;
                            c[ic + i + 0, jc + j + 2] = alpha * v02;
                            c[ic + i + 0, jc + j + 3] = alpha * v03;
                            c[ic + i + 1, jc + j + 0] = alpha * v10;
                            c[ic + i + 1, jc + j + 1] = alpha * v11;
                            c[ic + i + 1, jc + j + 2] = alpha * v12;
                            c[ic + i + 1, jc + j + 3] = alpha * v13;
                            c[ic + i + 2, jc + j + 0] = alpha * v20;
                            c[ic + i + 2, jc + j + 1] = alpha * v21;
                            c[ic + i + 2, jc + j + 2] = alpha * v22;
                            c[ic + i + 2, jc + j + 3] = alpha * v23;
                            c[ic + i + 3, jc + j + 0] = alpha * v30;
                            c[ic + i + 3, jc + j + 1] = alpha * v31;
                            c[ic + i + 3, jc + j + 2] = alpha * v32;
                            c[ic + i + 3, jc + j + 3] = alpha * v33;
                        }
                        else
                        {
                            c[ic + i + 0, jc + j + 0] = beta * c[ic + i + 0, jc + j + 0] + alpha * v00;
                            c[ic + i + 0, jc + j + 1] = beta * c[ic + i + 0, jc + j + 1] + alpha * v01;
                            c[ic + i + 0, jc + j + 2] = beta * c[ic + i + 0, jc + j + 2] + alpha * v02;
                            c[ic + i + 0, jc + j + 3] = beta * c[ic + i + 0, jc + j + 3] + alpha * v03;
                            c[ic + i + 1, jc + j + 0] = beta * c[ic + i + 1, jc + j + 0] + alpha * v10;
                            c[ic + i + 1, jc + j + 1] = beta * c[ic + i + 1, jc + j + 1] + alpha * v11;
                            c[ic + i + 1, jc + j + 2] = beta * c[ic + i + 1, jc + j + 2] + alpha * v12;
                            c[ic + i + 1, jc + j + 3] = beta * c[ic + i + 1, jc + j + 3] + alpha * v13;
                            c[ic + i + 2, jc + j + 0] = beta * c[ic + i + 2, jc + j + 0] + alpha * v20;
                            c[ic + i + 2, jc + j + 1] = beta * c[ic + i + 2, jc + j + 1] + alpha * v21;
                            c[ic + i + 2, jc + j + 2] = beta * c[ic + i + 2, jc + j + 2] + alpha * v22;
                            c[ic + i + 2, jc + j + 3] = beta * c[ic + i + 2, jc + j + 3] + alpha * v23;
                            c[ic + i + 3, jc + j + 0] = beta * c[ic + i + 3, jc + j + 0] + alpha * v30;
                            c[ic + i + 3, jc + j + 1] = beta * c[ic + i + 3, jc + j + 1] + alpha * v31;
                            c[ic + i + 3, jc + j + 2] = beta * c[ic + i + 3, jc + j + 2] + alpha * v32;
                            c[ic + i + 3, jc + j + 3] = beta * c[ic + i + 3, jc + j + 3] + alpha * v33;
                        }
                    }
                    else
                    {

                        //
                        // Determine submatrix [I0..I1]x[J0..J1] to process
                        //
                        i0 = i;
                        i1 = Math.Min(i + 3, m - 1);
                        j0 = j;
                        j1 = Math.Min(j + 3, n - 1);

                        //
                        // Process submatrix
                        //
                        for (ik = i0; ik <= i1; ik++)
                        {
                            for (jk = j0; jk <= j1; jk++)
                            {
                                if (k == 0 || alpha == 0)
                                {
                                    v = 0;
                                }
                                else
                                {
                                    i1_ = (ib) - (ja);
                                    v = 0.0;
                                    for (i_ = ja; i_ <= ja + k - 1; i_++)
                                    {
                                        v += a[ia + ik, i_] * b[i_ + i1_, jb + jk];
                                    }
                                }
                                if (beta == 0)
                                {
                                    c[ic + ik, jc + jk] = alpha * v;
                                }
                                else
                                {
                                    c[ic + ik, jc + jk] = beta * c[ic + ik, jc + jk] + alpha * v;
                                }
                            }
                        }
                    }
                    j = j + 4;
                }
                i = i + 4;
            }
        }

        /*************************************************************************
           RMatrixGEMM kernel, basecase code for RMatrixGEMM, specialized for sitation
           with OpTypeA=0 and OpTypeB=1.

           Additional info:
           * this function requires that Alpha<>0 (assertion is thrown otherwise)

           INPUT PARAMETERS
               M       -   matrix size, M>0
               N       -   matrix size, N>0
               K       -   matrix size, K>0
               Alpha   -   coefficient
               A       -   matrix
               IA      -   submatrix offset
               JA      -   submatrix offset
               B       -   matrix
               IB      -   submatrix offset
               JB      -   submatrix offset
               Beta    -   coefficient
               C       -   PREALLOCATED output matrix
               IC      -   submatrix offset
               JC      -   submatrix offset

             -- ALGLIB routine --
                27.03.2013
                Bochkanov Sergey
           *************************************************************************/



        private static void RMatrixGemmK44V01(int m,
            int n,
            int k,
            double alpha,
            Matrix<double> a,
            int ia,
            int ja,
            Matrix<double> b,
            int ib,
            int jb,
            double beta,
            Matrix<double> c,
            int ic,
            int jc)
        {
            int i = 0;
            int j = 0;
            double v = 0;
            double v00 = 0;
            double v01 = 0;
            double v02 = 0;
            double v03 = 0;
            double v10 = 0;
            double v11 = 0;
            double v12 = 0;
            double v13 = 0;
            double v20 = 0;
            double v21 = 0;
            double v22 = 0;
            double v23 = 0;
            double v30 = 0;
            double v31 = 0;
            double v32 = 0;
            double v33 = 0;
            double a0 = 0;
            double a1 = 0;
            double a2 = 0;
            double a3 = 0;
            double b0 = 0;
            double b1 = 0;
            double b2 = 0;
            double b3 = 0;
            int idxa0 = 0;
            int idxa1 = 0;
            int idxa2 = 0;
            int idxa3 = 0;
            int idxb0 = 0;
            int idxb1 = 0;
            int idxb2 = 0;
            int idxb3 = 0;
            int i0 = 0;
            int i1 = 0;
            int ik = 0;
            int j0 = 0;
            int j1 = 0;
            int jk = 0;
            int t = 0;
            int offsa = 0;
            int offsb = 0;
            int i_ = 0;
            int i1_ = 0;

            Utilities.Assert(alpha != 0, "RMatrixGEMMK44V00: internal error (Alpha=0)");

            //
            // if matrix size is zero
            //
            if (m == 0 || n == 0)
            {
                return;
            }

            //
            // A*B'
            //
            i = 0;
            while (i < m)
            {
                j = 0;
                while (j < n)
                {

                    //
                    // Choose between specialized 4x4 code and general code
                    //
                    if (i + 4 <= m && j + 4 <= n)
                    {

                        //
                        // Specialized 4x4 code for [I..I+3]x[J..J+3] submatrix of C.
                        //
                        // This submatrix is calculated as sum of K rank-1 products,
                        // with operands cached in local variables in order to speed
                        // up operations with arrays.
                        //
                        idxa0 = ia + i + 0;
                        idxa1 = ia + i + 1;
                        idxa2 = ia + i + 2;
                        idxa3 = ia + i + 3;
                        offsa = ja;
                        idxb0 = ib + j + 0;
                        idxb1 = ib + j + 1;
                        idxb2 = ib + j + 2;
                        idxb3 = ib + j + 3;
                        offsb = jb;
                        v00 = 0.0;
                        v01 = 0.0;
                        v02 = 0.0;
                        v03 = 0.0;
                        v10 = 0.0;
                        v11 = 0.0;
                        v12 = 0.0;
                        v13 = 0.0;
                        v20 = 0.0;
                        v21 = 0.0;
                        v22 = 0.0;
                        v23 = 0.0;
                        v30 = 0.0;
                        v31 = 0.0;
                        v32 = 0.0;
                        v33 = 0.0;
                        for (t = 0; t <= k - 1; t++)
                        {
                            a0 = a[idxa0, offsa];
                            a1 = a[idxa1, offsa];
                            b0 = b[idxb0, offsb];
                            b1 = b[idxb1, offsb];
                            v00 = v00 + a0 * b0;
                            v01 = v01 + a0 * b1;
                            v10 = v10 + a1 * b0;
                            v11 = v11 + a1 * b1;
                            a2 = a[idxa2, offsa];
                            a3 = a[idxa3, offsa];
                            v20 = v20 + a2 * b0;
                            v21 = v21 + a2 * b1;
                            v30 = v30 + a3 * b0;
                            v31 = v31 + a3 * b1;
                            b2 = b[idxb2, offsb];
                            b3 = b[idxb3, offsb];
                            v22 = v22 + a2 * b2;
                            v23 = v23 + a2 * b3;
                            v32 = v32 + a3 * b2;
                            v33 = v33 + a3 * b3;
                            v02 = v02 + a0 * b2;
                            v03 = v03 + a0 * b3;
                            v12 = v12 + a1 * b2;
                            v13 = v13 + a1 * b3;
                            offsa = offsa + 1;
                            offsb = offsb + 1;
                        }
                        if (beta == 0)
                        {
                            c[ic + i + 0, jc + j + 0] = alpha * v00;
                            c[ic + i + 0, jc + j + 1] = alpha * v01;
                            c[ic + i + 0, jc + j + 2] = alpha * v02;
                            c[ic + i + 0, jc + j + 3] = alpha * v03;
                            c[ic + i + 1, jc + j + 0] = alpha * v10;
                            c[ic + i + 1, jc + j + 1] = alpha * v11;
                            c[ic + i + 1, jc + j + 2] = alpha * v12;
                            c[ic + i + 1, jc + j + 3] = alpha * v13;
                            c[ic + i + 2, jc + j + 0] = alpha * v20;
                            c[ic + i + 2, jc + j + 1] = alpha * v21;
                            c[ic + i + 2, jc + j + 2] = alpha * v22;
                            c[ic + i + 2, jc + j + 3] = alpha * v23;
                            c[ic + i + 3, jc + j + 0] = alpha * v30;
                            c[ic + i + 3, jc + j + 1] = alpha * v31;
                            c[ic + i + 3, jc + j + 2] = alpha * v32;
                            c[ic + i + 3, jc + j + 3] = alpha * v33;
                        }
                        else
                        {
                            c[ic + i + 0, jc + j + 0] = beta * c[ic + i + 0, jc + j + 0] + alpha * v00;
                            c[ic + i + 0, jc + j + 1] = beta * c[ic + i + 0, jc + j + 1] + alpha * v01;
                            c[ic + i + 0, jc + j + 2] = beta * c[ic + i + 0, jc + j + 2] + alpha * v02;
                            c[ic + i + 0, jc + j + 3] = beta * c[ic + i + 0, jc + j + 3] + alpha * v03;
                            c[ic + i + 1, jc + j + 0] = beta * c[ic + i + 1, jc + j + 0] + alpha * v10;
                            c[ic + i + 1, jc + j + 1] = beta * c[ic + i + 1, jc + j + 1] + alpha * v11;
                            c[ic + i + 1, jc + j + 2] = beta * c[ic + i + 1, jc + j + 2] + alpha * v12;
                            c[ic + i + 1, jc + j + 3] = beta * c[ic + i + 1, jc + j + 3] + alpha * v13;
                            c[ic + i + 2, jc + j + 0] = beta * c[ic + i + 2, jc + j + 0] + alpha * v20;
                            c[ic + i + 2, jc + j + 1] = beta * c[ic + i + 2, jc + j + 1] + alpha * v21;
                            c[ic + i + 2, jc + j + 2] = beta * c[ic + i + 2, jc + j + 2] + alpha * v22;
                            c[ic + i + 2, jc + j + 3] = beta * c[ic + i + 2, jc + j + 3] + alpha * v23;
                            c[ic + i + 3, jc + j + 0] = beta * c[ic + i + 3, jc + j + 0] + alpha * v30;
                            c[ic + i + 3, jc + j + 1] = beta * c[ic + i + 3, jc + j + 1] + alpha * v31;
                            c[ic + i + 3, jc + j + 2] = beta * c[ic + i + 3, jc + j + 2] + alpha * v32;
                            c[ic + i + 3, jc + j + 3] = beta * c[ic + i + 3, jc + j + 3] + alpha * v33;
                        }
                    }
                    else
                    {

                        //
                        // Determine submatrix [I0..I1]x[J0..J1] to process
                        //
                        i0 = i;
                        i1 = Math.Min(i + 3, m - 1);
                        j0 = j;
                        j1 = Math.Min(j + 3, n - 1);

                        //
                        // Process submatrix
                        //
                        for (ik = i0; ik <= i1; ik++)
                        {
                            for (jk = j0; jk <= j1; jk++)
                            {
                                if (k == 0 || alpha == 0)
                                {
                                    v = 0;
                                }
                                else
                                {
                                    i1_ = (jb) - (ja);
                                    v = 0.0;
                                    for (i_ = ja; i_ <= ja + k - 1; i_++)
                                    {
                                        v += a[ia + ik, i_] * b[ib + jk, i_ + i1_];
                                    }
                                }
                                if (beta == 0)
                                {
                                    c[ic + ik, jc + jk] = alpha * v;
                                }
                                else
                                {
                                    c[ic + ik, jc + jk] = beta * c[ic + ik, jc + jk] + alpha * v;
                                }
                            }
                        }
                    }
                    j = j + 4;
                }
                i = i + 4;
            }
        }


        /*************************************************************************
            RMatrixGEMM kernel, basecase code for RMatrixGEMM, specialized for sitation
            with OpTypeA=1 and OpTypeB=0.

            Additional info:
            * this function requires that Alpha<>0 (assertion is thrown otherwise)

            INPUT PARAMETERS
                M       -   matrix size, M>0
                N       -   matrix size, N>0
                K       -   matrix size, K>0
                Alpha   -   coefficient
                A       -   matrix
                IA      -   submatrix offset
                JA      -   submatrix offset
                B       -   matrix
                IB      -   submatrix offset
                JB      -   submatrix offset
                Beta    -   coefficient
                C       -   PREALLOCATED output matrix
                IC      -   submatrix offset
                JC      -   submatrix offset

              -- ALGLIB routine --
                 27.03.2013
                 Bochkanov Sergey
            *************************************************************************/
        private static void RMatrixGemmK44V10(int m,
            int n,
            int k,
            double alpha,
            Matrix<double> a,
            int ia,
            int ja,
            Matrix<double> b,
            int ib,
            int jb,
            double beta,
            Matrix<double> c,
            int ic,
            int jc)
        {
            int i = 0;
            int j = 0;
            double v = 0;
            double v00 = 0;
            double v01 = 0;
            double v02 = 0;
            double v03 = 0;
            double v10 = 0;
            double v11 = 0;
            double v12 = 0;
            double v13 = 0;
            double v20 = 0;
            double v21 = 0;
            double v22 = 0;
            double v23 = 0;
            double v30 = 0;
            double v31 = 0;
            double v32 = 0;
            double v33 = 0;
            double a0 = 0;
            double a1 = 0;
            double a2 = 0;
            double a3 = 0;
            double b0 = 0;
            double b1 = 0;
            double b2 = 0;
            double b3 = 0;
            int idxa0 = 0;
            int idxa1 = 0;
            int idxa2 = 0;
            int idxa3 = 0;
            int idxb0 = 0;
            int idxb1 = 0;
            int idxb2 = 0;
            int idxb3 = 0;
            int i0 = 0;
            int i1 = 0;
            int ik = 0;
            int j0 = 0;
            int j1 = 0;
            int jk = 0;
            int t = 0;
            int offsa = 0;
            int offsb = 0;
            int i_ = 0;
            int i1_ = 0;

            Utilities.Assert(alpha != 0, "RMatrixGEMMK44V00: internal error (Alpha=0)");

            //
            // if matrix size is zero
            //
            if (m == 0 || n == 0)
            {
                return;
            }

            //
            // A'*B
            //
            i = 0;
            while (i < m)
            {
                j = 0;
                while (j < n)
                {

                    //
                    // Choose between specialized 4x4 code and general code
                    //
                    if (i + 4 <= m && j + 4 <= n)
                    {

                        //
                        // Specialized 4x4 code for [I..I+3]x[J..J+3] submatrix of C.
                        //
                        // This submatrix is calculated as sum of K rank-1 products,
                        // with operands cached in local variables in order to speed
                        // up operations with arrays.
                        //
                        idxa0 = ja + i + 0;
                        idxa1 = ja + i + 1;
                        idxa2 = ja + i + 2;
                        idxa3 = ja + i + 3;
                        offsa = ia;
                        idxb0 = jb + j + 0;
                        idxb1 = jb + j + 1;
                        idxb2 = jb + j + 2;
                        idxb3 = jb + j + 3;
                        offsb = ib;
                        v00 = 0.0;
                        v01 = 0.0;
                        v02 = 0.0;
                        v03 = 0.0;
                        v10 = 0.0;
                        v11 = 0.0;
                        v12 = 0.0;
                        v13 = 0.0;
                        v20 = 0.0;
                        v21 = 0.0;
                        v22 = 0.0;
                        v23 = 0.0;
                        v30 = 0.0;
                        v31 = 0.0;
                        v32 = 0.0;
                        v33 = 0.0;
                        for (t = 0; t <= k - 1; t++)
                        {
                            a0 = a[offsa, idxa0];
                            a1 = a[offsa, idxa1];
                            b0 = b[offsb, idxb0];
                            b1 = b[offsb, idxb1];
                            v00 = v00 + a0 * b0;
                            v01 = v01 + a0 * b1;
                            v10 = v10 + a1 * b0;
                            v11 = v11 + a1 * b1;
                            a2 = a[offsa, idxa2];
                            a3 = a[offsa, idxa3];
                            v20 = v20 + a2 * b0;
                            v21 = v21 + a2 * b1;
                            v30 = v30 + a3 * b0;
                            v31 = v31 + a3 * b1;
                            b2 = b[offsb, idxb2];
                            b3 = b[offsb, idxb3];
                            v22 = v22 + a2 * b2;
                            v23 = v23 + a2 * b3;
                            v32 = v32 + a3 * b2;
                            v33 = v33 + a3 * b3;
                            v02 = v02 + a0 * b2;
                            v03 = v03 + a0 * b3;
                            v12 = v12 + a1 * b2;
                            v13 = v13 + a1 * b3;
                            offsa = offsa + 1;
                            offsb = offsb + 1;
                        }
                        if (beta == 0)
                        {
                            c[ic + i + 0, jc + j + 0] = alpha * v00;
                            c[ic + i + 0, jc + j + 1] = alpha * v01;
                            c[ic + i + 0, jc + j + 2] = alpha * v02;
                            c[ic + i + 0, jc + j + 3] = alpha * v03;
                            c[ic + i + 1, jc + j + 0] = alpha * v10;
                            c[ic + i + 1, jc + j + 1] = alpha * v11;
                            c[ic + i + 1, jc + j + 2] = alpha * v12;
                            c[ic + i + 1, jc + j + 3] = alpha * v13;
                            c[ic + i + 2, jc + j + 0] = alpha * v20;
                            c[ic + i + 2, jc + j + 1] = alpha * v21;
                            c[ic + i + 2, jc + j + 2] = alpha * v22;
                            c[ic + i + 2, jc + j + 3] = alpha * v23;
                            c[ic + i + 3, jc + j + 0] = alpha * v30;
                            c[ic + i + 3, jc + j + 1] = alpha * v31;
                            c[ic + i + 3, jc + j + 2] = alpha * v32;
                            c[ic + i + 3, jc + j + 3] = alpha * v33;
                        }
                        else
                        {
                            c[ic + i + 0, jc + j + 0] = beta * c[ic + i + 0, jc + j + 0] + alpha * v00;
                            c[ic + i + 0, jc + j + 1] = beta * c[ic + i + 0, jc + j + 1] + alpha * v01;
                            c[ic + i + 0, jc + j + 2] = beta * c[ic + i + 0, jc + j + 2] + alpha * v02;
                            c[ic + i + 0, jc + j + 3] = beta * c[ic + i + 0, jc + j + 3] + alpha * v03;
                            c[ic + i + 1, jc + j + 0] = beta * c[ic + i + 1, jc + j + 0] + alpha * v10;
                            c[ic + i + 1, jc + j + 1] = beta * c[ic + i + 1, jc + j + 1] + alpha * v11;
                            c[ic + i + 1, jc + j + 2] = beta * c[ic + i + 1, jc + j + 2] + alpha * v12;
                            c[ic + i + 1, jc + j + 3] = beta * c[ic + i + 1, jc + j + 3] + alpha * v13;
                            c[ic + i + 2, jc + j + 0] = beta * c[ic + i + 2, jc + j + 0] + alpha * v20;
                            c[ic + i + 2, jc + j + 1] = beta * c[ic + i + 2, jc + j + 1] + alpha * v21;
                            c[ic + i + 2, jc + j + 2] = beta * c[ic + i + 2, jc + j + 2] + alpha * v22;
                            c[ic + i + 2, jc + j + 3] = beta * c[ic + i + 2, jc + j + 3] + alpha * v23;
                            c[ic + i + 3, jc + j + 0] = beta * c[ic + i + 3, jc + j + 0] + alpha * v30;
                            c[ic + i + 3, jc + j + 1] = beta * c[ic + i + 3, jc + j + 1] + alpha * v31;
                            c[ic + i + 3, jc + j + 2] = beta * c[ic + i + 3, jc + j + 2] + alpha * v32;
                            c[ic + i + 3, jc + j + 3] = beta * c[ic + i + 3, jc + j + 3] + alpha * v33;
                        }
                    }
                    else
                    {

                        //
                        // Determine submatrix [I0..I1]x[J0..J1] to process
                        //
                        i0 = i;
                        i1 = Math.Min(i + 3, m - 1);
                        j0 = j;
                        j1 = Math.Min(j + 3, n - 1);

                        //
                        // Process submatrix
                        //
                        for (ik = i0; ik <= i1; ik++)
                        {
                            for (jk = j0; jk <= j1; jk++)
                            {
                                if (k == 0 || alpha == 0)
                                {
                                    v = 0;
                                }
                                else
                                {
                                    v = 0.0;
                                    i1_ = (ib) - (ia);
                                    v = 0.0;
                                    for (i_ = ia; i_ <= ia + k - 1; i_++)
                                    {
                                        v += a[i_, ja + ik] * b[i_ + i1_, jb + jk];
                                    }
                                }
                                if (beta == 0)
                                {
                                    c[ic + ik, jc + jk] = alpha * v;
                                }
                                else
                                {
                                    c[ic + ik, jc + jk] = beta * c[ic + ik, jc + jk] + alpha * v;
                                }
                            }
                        }
                    }
                    j = j + 4;
                }
                i = i + 4;
            }
        }




        /*************************************************************************
            RMatrixGEMM kernel, basecase code for RMatrixGEMM, specialized for sitation
            with OpTypeA=1 and OpTypeB=1.

            Additional info:
            * this function requires that Alpha<>0 (assertion is thrown otherwise)

            INPUT PARAMETERS
                M       -   matrix size, M>0
                N       -   matrix size, N>0
                K       -   matrix size, K>0
                Alpha   -   coefficient
                A       -   matrix
                IA      -   submatrix offset
                JA      -   submatrix offset
                B       -   matrix
                IB      -   submatrix offset
                JB      -   submatrix offset
                Beta    -   coefficient
                C       -   PREALLOCATED output matrix
                IC      -   submatrix offset
                JC      -   submatrix offset

              -- ALGLIB routine --
                 27.03.2013
                 Bochkanov Sergey
            *************************************************************************/
        private static void RMatrixGemmK44V11(int m,
            int n,
            int k,
            double alpha,
            Matrix<double> a,
            int ia,
            int ja,
            Matrix<double> b,
            int ib,
            int jb,
            double beta,
            Matrix<double> c,
            int ic,
            int jc)
        {
            int i = 0;
            int j = 0;
            double v = 0;
            double v00 = 0;
            double v01 = 0;
            double v02 = 0;
            double v03 = 0;
            double v10 = 0;
            double v11 = 0;
            double v12 = 0;
            double v13 = 0;
            double v20 = 0;
            double v21 = 0;
            double v22 = 0;
            double v23 = 0;
            double v30 = 0;
            double v31 = 0;
            double v32 = 0;
            double v33 = 0;
            double a0 = 0;
            double a1 = 0;
            double a2 = 0;
            double a3 = 0;
            double b0 = 0;
            double b1 = 0;
            double b2 = 0;
            double b3 = 0;
            int idxa0 = 0;
            int idxa1 = 0;
            int idxa2 = 0;
            int idxa3 = 0;
            int idxb0 = 0;
            int idxb1 = 0;
            int idxb2 = 0;
            int idxb3 = 0;
            int i0 = 0;
            int i1 = 0;
            int ik = 0;
            int j0 = 0;
            int j1 = 0;
            int jk = 0;
            int t = 0;
            int offsa = 0;
            int offsb = 0;
            int i_ = 0;
            int i1_ = 0;

            Utilities.Assert(alpha != 0, "RMatrixGEMMK44V00: internal error (Alpha=0)");

            //
            // if matrix size is zero
            //
            if (m == 0 || n == 0)
            {
                return;
            }

            //
            // A'*B'
            //
            i = 0;
            while (i < m)
            {
                j = 0;
                while (j < n)
                {

                    //
                    // Choose between specialized 4x4 code and general code
                    //
                    if (i + 4 <= m && j + 4 <= n)
                    {

                        //
                        // Specialized 4x4 code for [I..I+3]x[J..J+3] submatrix of C.
                        //
                        // This submatrix is calculated as sum of K rank-1 products,
                        // with operands cached in local variables in order to speed
                        // up operations with arrays.
                        //
                        idxa0 = ja + i + 0;
                        idxa1 = ja + i + 1;
                        idxa2 = ja + i + 2;
                        idxa3 = ja + i + 3;
                        offsa = ia;
                        idxb0 = ib + j + 0;
                        idxb1 = ib + j + 1;
                        idxb2 = ib + j + 2;
                        idxb3 = ib + j + 3;
                        offsb = jb;
                        v00 = 0.0;
                        v01 = 0.0;
                        v02 = 0.0;
                        v03 = 0.0;
                        v10 = 0.0;
                        v11 = 0.0;
                        v12 = 0.0;
                        v13 = 0.0;
                        v20 = 0.0;
                        v21 = 0.0;
                        v22 = 0.0;
                        v23 = 0.0;
                        v30 = 0.0;
                        v31 = 0.0;
                        v32 = 0.0;
                        v33 = 0.0;
                        for (t = 0; t <= k - 1; t++)
                        {
                            a0 = a[offsa, idxa0];
                            a1 = a[offsa, idxa1];
                            b0 = b[idxb0, offsb];
                            b1 = b[idxb1, offsb];
                            v00 = v00 + a0 * b0;
                            v01 = v01 + a0 * b1;
                            v10 = v10 + a1 * b0;
                            v11 = v11 + a1 * b1;
                            a2 = a[offsa, idxa2];
                            a3 = a[offsa, idxa3];
                            v20 = v20 + a2 * b0;
                            v21 = v21 + a2 * b1;
                            v30 = v30 + a3 * b0;
                            v31 = v31 + a3 * b1;
                            b2 = b[idxb2, offsb];
                            b3 = b[idxb3, offsb];
                            v22 = v22 + a2 * b2;
                            v23 = v23 + a2 * b3;
                            v32 = v32 + a3 * b2;
                            v33 = v33 + a3 * b3;
                            v02 = v02 + a0 * b2;
                            v03 = v03 + a0 * b3;
                            v12 = v12 + a1 * b2;
                            v13 = v13 + a1 * b3;
                            offsa = offsa + 1;
                            offsb = offsb + 1;
                        }
                        if (beta == 0)
                        {
                            c[ic + i + 0, jc + j + 0] = alpha * v00;
                            c[ic + i + 0, jc + j + 1] = alpha * v01;
                            c[ic + i + 0, jc + j + 2] = alpha * v02;
                            c[ic + i + 0, jc + j + 3] = alpha * v03;
                            c[ic + i + 1, jc + j + 0] = alpha * v10;
                            c[ic + i + 1, jc + j + 1] = alpha * v11;
                            c[ic + i + 1, jc + j + 2] = alpha * v12;
                            c[ic + i + 1, jc + j + 3] = alpha * v13;
                            c[ic + i + 2, jc + j + 0] = alpha * v20;
                            c[ic + i + 2, jc + j + 1] = alpha * v21;
                            c[ic + i + 2, jc + j + 2] = alpha * v22;
                            c[ic + i + 2, jc + j + 3] = alpha * v23;
                            c[ic + i + 3, jc + j + 0] = alpha * v30;
                            c[ic + i + 3, jc + j + 1] = alpha * v31;
                            c[ic + i + 3, jc + j + 2] = alpha * v32;
                            c[ic + i + 3, jc + j + 3] = alpha * v33;
                        }
                        else
                        {
                            c[ic + i + 0, jc + j + 0] = beta * c[ic + i + 0, jc + j + 0] + alpha * v00;
                            c[ic + i + 0, jc + j + 1] = beta * c[ic + i + 0, jc + j + 1] + alpha * v01;
                            c[ic + i + 0, jc + j + 2] = beta * c[ic + i + 0, jc + j + 2] + alpha * v02;
                            c[ic + i + 0, jc + j + 3] = beta * c[ic + i + 0, jc + j + 3] + alpha * v03;
                            c[ic + i + 1, jc + j + 0] = beta * c[ic + i + 1, jc + j + 0] + alpha * v10;
                            c[ic + i + 1, jc + j + 1] = beta * c[ic + i + 1, jc + j + 1] + alpha * v11;
                            c[ic + i + 1, jc + j + 2] = beta * c[ic + i + 1, jc + j + 2] + alpha * v12;
                            c[ic + i + 1, jc + j + 3] = beta * c[ic + i + 1, jc + j + 3] + alpha * v13;
                            c[ic + i + 2, jc + j + 0] = beta * c[ic + i + 2, jc + j + 0] + alpha * v20;
                            c[ic + i + 2, jc + j + 1] = beta * c[ic + i + 2, jc + j + 1] + alpha * v21;
                            c[ic + i + 2, jc + j + 2] = beta * c[ic + i + 2, jc + j + 2] + alpha * v22;
                            c[ic + i + 2, jc + j + 3] = beta * c[ic + i + 2, jc + j + 3] + alpha * v23;
                            c[ic + i + 3, jc + j + 0] = beta * c[ic + i + 3, jc + j + 0] + alpha * v30;
                            c[ic + i + 3, jc + j + 1] = beta * c[ic + i + 3, jc + j + 1] + alpha * v31;
                            c[ic + i + 3, jc + j + 2] = beta * c[ic + i + 3, jc + j + 2] + alpha * v32;
                            c[ic + i + 3, jc + j + 3] = beta * c[ic + i + 3, jc + j + 3] + alpha * v33;
                        }
                    }
                    else
                    {

                        //
                        // Determine submatrix [I0..I1]x[J0..J1] to process
                        //
                        i0 = i;
                        i1 = Math.Min(i + 3, m - 1);
                        j0 = j;
                        j1 = Math.Min(j + 3, n - 1);

                        //
                        // Process submatrix
                        //
                        for (ik = i0; ik <= i1; ik++)
                        {
                            for (jk = j0; jk <= j1; jk++)
                            {
                                if (k == 0 || alpha == 0)
                                {
                                    v = 0;
                                }
                                else
                                {
                                    v = 0.0;
                                    i1_ = (jb) - (ia);
                                    v = 0.0;
                                    for (i_ = ia; i_ <= ia + k - 1; i_++)
                                    {
                                        v += a[i_, ja + ik] * b[ib + jk, i_ + i1_];
                                    }
                                }
                                if (beta == 0)
                                {
                                    c[ic + ik, jc + jk] = alpha * v;
                                }
                                else
                                {
                                    c[ic + ik, jc + jk] = beta * c[ic + ik, jc + jk] + alpha * v;
                                }
                            }
                        }
                    }
                    j = j + 4;
                }
                i = i + 4;
            }
        }
    }
}