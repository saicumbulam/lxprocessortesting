using System;
using LxProcessor.Common;

namespace LxProcessor.Optimizer
{
    public class Ortfac
    {
        /*************************************************************************
            QR decomposition of a rectangular matrix of size MxN

            Input parameters:
                A   -   matrix A whose indexes range within [0..M-1, 0..N-1].
                M   -   number of rows in matrix A.
                N   -   number of columns in matrix A.

            Output parameters:
                A   -   matrices Q and R in compact form (see below).
                Tau -   array of scalar factors which are used to form
                        matrix Q. Array whose index ranges within [0.. Min(M-1,N-1)].

            Matrix A is represented as A = QR, where Q is an orthogonal matrix of size
            MxM, R - upper triangular (or upper trapezoid) matrix of size M x N.

            The elements of matrix R are located on and above the main diagonal of
            matrix A. The elements which are located in Tau array and below the main
            diagonal of matrix A are used to form matrix Q as follows:

            Matrix Q is represented as a product of elementary reflections

            Q = H(0)*H(2)*...*H(k-1),

            where k = min(m,n), and each H(i) is in the form

            H(i) = 1 - tau * v * (v^T)

            where tau is a scalar stored in Tau[I]; v - real vector,
            so that v(0:i-1) = 0, v(i) = 1, v(i+1:m-1) stored in A(i+1:m-1,i).

              -- ALGLIB routine --
                 17.02.2010
                 Bochkanov Sergey
            *************************************************************************/
        public static void QrDecomposition(ref Matrix<double> a,
            int m,
            int n,
            ref double[] tau)
        {
            double[] work = new double[0];
            double[] t = new double[0];
            double[] taubuf = new double[0];
            int minmn = 0;
            Matrix<double> tmpa = new Matrix<double>(0, 0);
            Matrix<double> tmpt = new Matrix<double>(0, 0);
            Matrix<double> tmpr = new Matrix<double>(0, 0);
            int blockstart = 0;
            int blocksize = 0;
            int rowscount = 0;
            int i = 0;
            int i_ = 0;
            int i1_ = 0;

            tau = new double[0];

            if (m <= 0 || n <= 0)
            {
                return;
            }
            minmn = Math.Min(m, n);
            work = new double[Math.Max(m, n) + 1];
            t = new double[Math.Max(m, n) + 1];
            tau = new double[minmn];
            taubuf = new double[minmn];
            tmpa.Resize(m, Ablas.BlockSize(a));
            tmpt.Resize(Ablas.BlockSize(a), 2 * Ablas.BlockSize(a));
            tmpr.Resize(2 * Ablas.BlockSize(a), n);

            //
            // Blocked code
            //
            blockstart = 0;
            while (blockstart != minmn)
            {

                //
                // Determine block size
                //
                blocksize = minmn - blockstart;
                if (blocksize > Ablas.BlockSize(a))
                {
                    blocksize = Ablas.BlockSize(a);
                }
                rowscount = m - blockstart;

                //
                // QR decomposition of submatrix.
                // Matrix is copied to temporary storage to solve
                // some TLB issues arising from non-contiguous memory
                // access pattern.
                //
                Ablas.MatrixCopy(rowscount, blocksize, a, blockstart, blockstart, ref tmpa, 0, 0);
                QrBaseCase(ref tmpa, rowscount, blocksize, ref work, ref t, ref taubuf);
                Ablas.MatrixCopy(rowscount, blocksize, tmpa, 0, 0, ref a, blockstart, blockstart);
                i1_ = (0) - (blockstart);
                for (i_ = blockstart; i_ <= blockstart + blocksize - 1; i_++)
                {
                    tau[i_] = taubuf[i_ + i1_];
                }

                //
                // Update the rest, choose between:
                // a) Level 2 algorithm (when the rest of the matrix is small enough)
                // b) blocked algorithm, see algorithm 5 from  'A storage efficient WY
                //    representation for products of Householder transformations',
                //    by R. Schreiber and C. Van Loan.
                //
                if (blockstart + blocksize <= n - 1)
                {
                    if (n - blockstart - blocksize >= 2 * Ablas.BlockSize(a) || rowscount >= 4 * Ablas.BlockSize(a))
                    {

                        //
                        // Prepare block reflector
                        //
                        GenerateBlockReflector(ref tmpa, ref taubuf, true, rowscount, blocksize, ref tmpt, ref work);

                        //
                        // Multiply the rest of A by Q'.
                        //
                        // Q  = E + Y*T*Y'  = E + TmpA*TmpT*TmpA'
                        // Q' = E + Y*T'*Y' = E + TmpA*TmpT'*TmpA'
                        //
                        Ablas.RealGemm(blocksize, n - blockstart - blocksize, rowscount, 1.0, tmpa, 0, 0, 1, a, blockstart, blockstart + blocksize, 0, 0.0, tmpr, 0, 0);
                        Ablas.RealGemm(blocksize, n - blockstart - blocksize, blocksize, 1.0, tmpt, 0, 0, 1, tmpr, 0, 0, 0, 0.0, tmpr, blocksize, 0);
                        Ablas.RealGemm(rowscount, n - blockstart - blocksize, blocksize, 1.0, tmpa, 0, 0, 0, tmpr, blocksize, 0, 0, 1.0, a, blockstart, blockstart + blocksize);
                    }
                    else
                    {

                        //
                        // Level 2 algorithm
                        //
                        for (i = 0; i <= blocksize - 1; i++)
                        {
                            i1_ = (i) - (1);
                            for (i_ = 1; i_ <= rowscount - i; i_++)
                            {
                                t[i_] = tmpa[i_ + i1_, i];
                            }
                            t[1] = 1;
                            Reflections.ApplyFromTheLeft(ref a, taubuf[i], t, blockstart + i, m - 1, blockstart + blocksize, n - 1, ref work);
                        }
                    }
                }

                //
                // Advance
                //
                blockstart = blockstart + blocksize;
            }
        }


        /*************************************************************************
            LQ decomposition of a rectangular matrix of size MxN

            Input parameters:
                A   -   matrix A whose indexes range within [0..M-1, 0..N-1].
                M   -   number of rows in matrix A.
                N   -   number of columns in matrix A.

            Output parameters:
                A   -   matrices L and Q in compact form (see below)
                Tau -   array of scalar factors which are used to form
                        matrix Q. Array whose index ranges within [0..Min(M,N)-1].

            Matrix A is represented as A = LQ, where Q is an orthogonal matrix of size
            MxM, L - lower triangular (or lower trapezoid) matrix of size M x N.

            The elements of matrix L are located on and below  the  main  diagonal  of
            matrix A. The elements which are located in Tau array and above  the  main
            diagonal of matrix A are used to form matrix Q as follows:

            Matrix Q is represented as a product of elementary reflections

            Q = H(k-1)*H(k-2)*...*H(1)*H(0),

            where k = min(m,n), and each H(i) is of the form

            H(i) = 1 - tau * v * (v^T)

            where tau is a scalar stored in Tau[I]; v - real vector, so that v(0:i-1)=0,
            v(i) = 1, v(i+1:n-1) stored in A(i,i+1:n-1).

              -- ALGLIB routine --
                 17.02.2010
                 Bochkanov Sergey
            *************************************************************************/
        public static void LqDecomposition(ref Matrix<double> a,
            int m,
            int n,
            ref double[] tau)
        {
            double[] work = new double[0];
            double[] t = new double[0];
            double[] taubuf = new double[0];
            int minmn = 0;
            Matrix<double> tmpa = new Matrix<double>(0, 0);
            Matrix<double> tmpt = new Matrix<double>(0, 0);
            Matrix<double> tmpr = new Matrix<double>(0, 0);
            int blockstart = 0;
            int blocksize = 0;
            int columnscount = 0;
            int i = 0;
            int i_ = 0;
            int i1_ = 0;

            tau = new double[0];

            if (m <= 0 || n <= 0)
            {
                return;
            }
            minmn = Math.Min(m, n);
            work = new double[Math.Max(m, n) + 1];
            t = new double[Math.Max(m, n) + 1];
            tau = new double[minmn];
            taubuf = new double[minmn];
            //tmpa = new double[Ablas.BlockSize(a), n];
            tmpa.Resize(Ablas.BlockSize(a), n);
            //tmpt = new double[Ablas.BlockSize(a), 2 * Ablas.BlockSize(a)];
            tmpt.Resize(Ablas.BlockSize(a), 2 * Ablas.BlockSize(a));
            //tmpr = new double[m, 2 * Ablas.BlockSize(a)];
            tmpr.Resize(m, 2 * Ablas.BlockSize(a));

            //
            // Blocked code
            //
            blockstart = 0;
            while (blockstart != minmn)
            {

                //
                // Determine block size
                //
                blocksize = minmn - blockstart;
                if (blocksize > Ablas.BlockSize(a))
                {
                    blocksize = Ablas.BlockSize(a);
                }
                columnscount = n - blockstart;

                //
                // LQ decomposition of submatrix.
                // Matrix is copied to temporary storage to solve
                // some TLB issues arising from non-contiguous memory
                // access pattern.
                //
                Ablas.MatrixCopy(blocksize, columnscount, a, blockstart, blockstart, ref tmpa, 0, 0);
                LqBaseVase(ref tmpa, blocksize, columnscount, ref work, ref t, ref taubuf);
                Ablas.MatrixCopy(blocksize, columnscount, tmpa, 0, 0, ref a, blockstart, blockstart);
                i1_ = (0) - (blockstart);
                for (i_ = blockstart; i_ <= blockstart + blocksize - 1; i_++)
                {
                    tau[i_] = taubuf[i_ + i1_];
                }

                //
                // Update the rest, choose between:
                // a) Level 2 algorithm (when the rest of the matrix is small enough)
                // b) blocked algorithm, see algorithm 5 from  'A storage efficient WY
                //    representation for products of Householder transformations',
                //    by R. Schreiber and C. Van Loan.
                //
                if (blockstart + blocksize <= m - 1)
                {
                    if (m - blockstart - blocksize >= 2 * Ablas.BlockSize(a))
                    {

                        //
                        // Prepare block reflector
                        //
                        GenerateBlockReflector(ref tmpa, ref taubuf, false, columnscount, blocksize, ref tmpt, ref work);

                        //
                        // Multiply the rest of A by Q.
                        //
                        // Q  = E + Y*T*Y'  = E + TmpA'*TmpT*TmpA
                        //
                        Ablas.RealGemm(m - blockstart - blocksize, blocksize, columnscount, 1.0, a, blockstart + blocksize, blockstart, 0, tmpa, 0, 0, 1, 0.0, tmpr, 0, 0);
                        Ablas.RealGemm(m - blockstart - blocksize, blocksize, blocksize, 1.0, tmpr, 0, 0, 0, tmpt, 0, 0, 0, 0.0, tmpr, 0, blocksize);
                        Ablas.RealGemm(m - blockstart - blocksize, columnscount, blocksize, 1.0, tmpr, 0, blocksize, 0, tmpa, 0, 0, 0, 1.0, a, blockstart + blocksize, blockstart);
                    }
                    else
                    {

                        //
                        // Level 2 algorithm
                        //
                        for (i = 0; i <= blocksize - 1; i++)
                        {
                            i1_ = (i) - (1);
                            for (i_ = 1; i_ <= columnscount - i; i_++)
                            {
                                t[i_] = tmpa[i, i_ + i1_];
                            }
                            t[1] = 1;
                            Reflections.ApplyFromTheRight(ref a, taubuf[i], t, blockstart + blocksize, m - 1, blockstart + i, n - 1, ref work);
                        }
                    }
                }

                //
                // Advance
                //
                blockstart = blockstart + blocksize;
            }
        }



        /*************************************************************************
            Partial unpacking of matrix Q from the QR decomposition of a matrix A

            Input parameters:
                A       -   matrices Q and R in compact form.
                            Output of RMatrixQR subroutine.
                M       -   number of rows in given matrix A. M>=0.
                N       -   number of columns in given matrix A. N>=0.
                Tau     -   scalar factors which are used to form Q.
                            Output of the RMatrixQR subroutine.
                QColumns -  required number of columns of matrix Q. M>=QColumns>=0.

            Output parameters:
                Q       -   first QColumns columns of matrix Q.
                            Array whose indexes range within [0..M-1, 0..QColumns-1].
                            If QColumns=0, the array remains unchanged.

              -- ALGLIB routine --
                 17.02.2010
                 Bochkanov Sergey
            *************************************************************************/
        public static void PartialUnpackingFromQrDecomposition(Matrix<double> a,
            int m,
            int n,
            double[] tau,
            int qcolumns,
            ref Matrix<double> q)
        {
            double[] work = new double[0];
            double[] t = new double[0];
            double[] taubuf = new double[0];
            int minmn = 0;
            int refcnt = 0;
            Matrix<double> tmpa = new Matrix<double>(0, 0);
            Matrix<double> tmpt = new Matrix<double>(0, 0);
            Matrix<double> tmpr = new Matrix<double>(0, 0);
            int blockstart = 0;
            int blocksize = 0;
            int rowscount = 0;
            int i = 0;
            int j = 0;
            int i_ = 0;
            int i1_ = 0;

            q = new Matrix<double>(0, 0);

            Utilities.Assert(qcolumns <= m, "UnpackQFromQR: QColumns>M!");
            if ((m <= 0 || n <= 0) || qcolumns <= 0)
            {
                return;
            }

            //
            // init
            //
            minmn = Math.Min(m, n);
            refcnt = Math.Min(minmn, qcolumns);
            q.Resize(m, qcolumns);
            for (i = 0; i <= m - 1; i++)
            {
                for (j = 0; j <= qcolumns - 1; j++)
                {
                    if (i == j)
                    {
                        q[i, j] = 1;
                    }
                    else
                    {
                        q[i, j] = 0;
                    }
                }
            }
            work = new double[Math.Max(m, qcolumns) + 1];
            t = new double[Math.Max(m, qcolumns) + 1];
            taubuf = new double[minmn];
            tmpa.Resize(m, Ablas.BlockSize(a));
            tmpt.Resize(Ablas.BlockSize(a), 2 * Ablas.BlockSize(a));
            tmpr.Resize(2 * Ablas.BlockSize(a), qcolumns);

            //
            // Blocked code
            //
            blockstart = Ablas.BlockSize(a) * (refcnt / Ablas.BlockSize(a));
            blocksize = refcnt - blockstart;
            while (blockstart >= 0)
            {
                rowscount = m - blockstart;
                if (blocksize > 0)
                {

                    //
                    // Copy current block
                    //
                    Ablas.MatrixCopy(rowscount, blocksize, a, blockstart, blockstart, ref tmpa, 0, 0);
                    i1_ = (blockstart) - (0);
                    for (i_ = 0; i_ <= blocksize - 1; i_++)
                    {
                        taubuf[i_] = tau[i_ + i1_];
                    }

                    //
                    // Update, choose between:
                    // a) Level 2 algorithm (when the rest of the matrix is small enough)
                    // b) blocked algorithm, see algorithm 5 from  'A storage efficient WY
                    //    representation for products of Householder transformations',
                    //    by R. Schreiber and C. Van Loan.
                    //
                    if (qcolumns >= 2 * Ablas.BlockSize(a))
                    {

                        //
                        // Prepare block reflector
                        //
                        GenerateBlockReflector(ref tmpa, ref taubuf, true, rowscount, blocksize, ref tmpt, ref work);

                        //
                        // Multiply matrix by Q.
                        //
                        // Q  = E + Y*T*Y'  = E + TmpA*TmpT*TmpA'
                        //
                        Ablas.RealGemm(blocksize, qcolumns, rowscount, 1.0, tmpa, 0, 0, 1, q, blockstart, 0, 0, 0.0, tmpr, 0, 0);
                        Ablas.RealGemm(blocksize, qcolumns, blocksize, 1.0, tmpt, 0, 0, 0, tmpr, 0, 0, 0, 0.0, tmpr, blocksize, 0);
                        Ablas.RealGemm(rowscount, qcolumns, blocksize, 1.0, tmpa, 0, 0, 0, tmpr, blocksize, 0, 0, 1.0, q, blockstart, 0);
                    }
                    else
                    {

                        //
                        // Level 2 algorithm
                        //
                        for (i = blocksize - 1; i >= 0; i--)
                        {
                            i1_ = (i) - (1);
                            for (i_ = 1; i_ <= rowscount - i; i_++)
                            {
                                t[i_] = tmpa[i_ + i1_, i];
                            }
                            t[1] = 1;
                            Reflections.ApplyFromTheLeft(ref q, taubuf[i], t, blockstart + i, m - 1, 0, qcolumns - 1, ref work);
                        }
                    }
                }

                //
                // Advance
                //
                blockstart = blockstart - Ablas.BlockSize(a);
                blocksize = Ablas.BlockSize(a);
            }
        }


            

        /*************************************************************************
            Partial unpacking of matrix Q from the LQ decomposition of a matrix A

            Input parameters:
                A       -   matrices L and Q in compact form.
                            Output of RMatrixLQ subroutine.
                M       -   number of rows in given matrix A. M>=0.
                N       -   number of columns in given matrix A. N>=0.
                Tau     -   scalar factors which are used to form Q.
                            Output of the RMatrixLQ subroutine.
                QRows   -   required number of rows in matrix Q. N>=QRows>=0.

            Output parameters:
                Q       -   first QRows rows of matrix Q. Array whose indexes range
                            within [0..QRows-1, 0..N-1]. If QRows=0, the array remains
                            unchanged.

              -- ALGLIB routine --
                 17.02.2010
                 Bochkanov Sergey
            *************************************************************************/
        public static void PartialUnpackingFromLqDecomposition(Matrix<double> a,
            int m,
            int n,
            double[] tau,
            int qrows,
            ref Matrix<double> q)
        {
            double[] work = new double[0];
            double[] t = new double[0];
            double[] taubuf = new double[0];
            int minmn = 0;
            int refcnt = 0;
            Matrix<double> tmpa = new Matrix<double>(0, 0);
            Matrix<double> tmpt = new Matrix<double>(0, 0);
            Matrix<double> tmpr = new Matrix<double>(0, 0);
            int blockstart = 0;
            int blocksize = 0;
            int columnscount = 0;
            int i = 0;
            int j = 0;
            int i_ = 0;
            int i1_ = 0;

            q = new Matrix<double>(0, 0);

            Utilities.Assert(qrows <= n, "RMatrixLQUnpackQ: QRows>N!");
            if ((m <= 0 || n <= 0) || qrows <= 0)
            {
                return;
            }

            //
            // init
            //
            minmn = Math.Min(m, n);
            refcnt = Math.Min(minmn, qrows);
            work = new double[Math.Max(m, n) + 1];
            t = new double[Math.Max(m, n) + 1];
            taubuf = new double[minmn];
            tmpa.Resize(Ablas.BlockSize(a), n);
            tmpt.Resize(Ablas.BlockSize(a), 2 * Ablas.BlockSize(a));
            tmpr.Resize(qrows, 2 * Ablas.BlockSize(a));
            q.Resize(qrows, n);
            for (i = 0; i <= qrows - 1; i++)
            {
                for (j = 0; j <= n - 1; j++)
                {
                    if (i == j)
                    {
                        q[i, j] = 1;
                    }
                    else
                    {
                        q[i, j] = 0;
                    }
                }
            }

            //
            // Blocked code
            //
            blockstart = Ablas.BlockSize(a) * (refcnt / Ablas.BlockSize(a));
            blocksize = refcnt - blockstart;
            while (blockstart >= 0)
            {
                columnscount = n - blockstart;
                if (blocksize > 0)
                {

                    //
                    // Copy submatrix
                    //
                    Ablas.MatrixCopy(blocksize, columnscount, a, blockstart, blockstart, ref tmpa, 0, 0);
                    i1_ = (blockstart) - (0);
                    for (i_ = 0; i_ <= blocksize - 1; i_++)
                    {
                        taubuf[i_] = tau[i_ + i1_];
                    }

                    //
                    // Update matrix, choose between:
                    // a) Level 2 algorithm (when the rest of the matrix is small enough)
                    // b) blocked algorithm, see algorithm 5 from  'A storage efficient WY
                    //    representation for products of Householder transformations',
                    //    by R. Schreiber and C. Van Loan.
                    //
                    if (qrows >= 2 * Ablas.BlockSize(a))
                    {

                        //
                        // Prepare block reflector
                        //
                        GenerateBlockReflector(ref tmpa, ref taubuf, false, columnscount, blocksize, ref tmpt, ref work);

                        //
                        // Multiply the rest of A by Q'.
                        //
                        // Q'  = E + Y*T'*Y'  = E + TmpA'*TmpT'*TmpA
                        //
                        Ablas.RealGemm(qrows, blocksize, columnscount, 1.0, q, 0, blockstart, 0, tmpa, 0, 0, 1, 0.0, tmpr, 0, 0);
                        Ablas.RealGemm(qrows, blocksize, blocksize, 1.0, tmpr, 0, 0, 0, tmpt, 0, 0, 1, 0.0, tmpr, 0, blocksize);
                        Ablas.RealGemm(qrows, columnscount, blocksize, 1.0, tmpr, 0, blocksize, 0, tmpa, 0, 0, 0, 1.0, q, 0, blockstart);
                    }
                    else
                    {

                        //
                        // Level 2 algorithm
                        //
                        for (i = blocksize - 1; i >= 0; i--)
                        {
                            i1_ = (i) - (1);
                            for (i_ = 1; i_ <= columnscount - i; i_++)
                            {
                                t[i_] = tmpa[i, i_ + i1_];
                            }
                            t[1] = 1;
                            Reflections.ApplyFromTheRight(ref q, taubuf[i], t, 0, qrows - 1, blockstart + i, n - 1, ref work);
                        }
                    }
                }

                //
                // Advance
                //
                blockstart = blockstart - Ablas.BlockSize(a);
                blocksize = Ablas.BlockSize(a);
            }
        }


            

        /*************************************************************************
            Base case for real QR

              -- LAPACK routine (version 3.0) --
                 Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
                 Courant Institute, Argonne National Lab, and Rice University
                 September 30, 1994.
                 Sergey Bochkanov, ALGLIB project, translation from FORTRAN to
                 pseudocode, 2007-2010.
            *************************************************************************/
        private static void QrBaseCase(ref Matrix<double> a,
            int m,
            int n,
            ref double[] work,
            ref double[] t,
            ref double[] tau)
        {
            int i = 0;
            int k = 0;
            int minmn = 0;
            double tmp = 0;
            int i_ = 0;
            int i1_ = 0;

            minmn = Math.Min(m, n);

            //
            // Test the input arguments
            //
            k = minmn;
            for (i = 0; i <= k - 1; i++)
            {

                //
                // Generate elementary reflector H(i) to annihilate A(i+1:m,i)
                //
                i1_ = (i) - (1);
                for (i_ = 1; i_ <= m - i; i_++)
                {
                    t[i_] = a[i_ + i1_, i];
                }
                Reflections.Generate(ref t, m - i, ref tmp);
                tau[i] = tmp;
                i1_ = (1) - (i);
                for (i_ = i; i_ <= m - 1; i_++)
                {
                    a[i_, i] = t[i_ + i1_];
                }
                t[1] = 1;
                if (i < n)
                {

                    //
                    // Apply H(i) to A(i:m-1,i+1:n-1) from the left
                    //
                    Reflections.ApplyFromTheLeft(ref a, tau[i], t, i, m - 1, i + 1, n - 1, ref work);
                }
            }
        }


        /*************************************************************************
            Base case for real LQ

              -- LAPACK routine (version 3.0) --
                 Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
                 Courant Institute, Argonne National Lab, and Rice University
                 September 30, 1994.
                 Sergey Bochkanov, ALGLIB project, translation from FORTRAN to
                 pseudocode, 2007-2010.
            *************************************************************************/
        private static void LqBaseVase(ref Matrix<double> a,
            int m,
            int n,
            ref double[] work,
            ref double[] t,
            ref double[] tau)
        {
            int i = 0;
            int k = 0;
            double tmp = 0;
            int i_ = 0;
            int i1_ = 0;

            k = Math.Min(m, n);
            for (i = 0; i <= k - 1; i++)
            {

                //
                // Generate elementary reflector H(i) to annihilate A(i,i+1:n-1)
                //
                i1_ = (i) - (1);
                for (i_ = 1; i_ <= n - i; i_++)
                {
                    t[i_] = a[i, i_ + i1_];
                }
                Reflections.Generate(ref t, n - i, ref tmp);
                tau[i] = tmp;
                i1_ = (1) - (i);
                for (i_ = i; i_ <= n - 1; i_++)
                {
                    a[i, i_] = t[i_ + i1_];
                }
                t[1] = 1;
                if (i < n)
                {

                    //
                    // Apply H(i) to A(i+1:m,i:n) from the right
                    //
                    Reflections.ApplyFromTheRight(ref a, tau[i], t, i + 1, m - 1, i, n - 1, ref work);
                }
            }
        }


        /*************************************************************************
            Reduction of a rectangular matrix to  bidiagonal form

            The algorithm reduces the rectangular matrix A to  bidiagonal form by
            orthogonal transformations P and Q: A = Q*B*P.

            Input parameters:
                A       -   source matrix. array[0..M-1, 0..N-1]
                M       -   number of rows in matrix A.
                N       -   number of columns in matrix A.

            Output parameters:
                A       -   matrices Q, B, P in compact form (see below).
                TauQ    -   scalar factors which are used to form matrix Q.
                TauP    -   scalar factors which are used to form matrix P.

            The main diagonal and one of the  secondary  diagonals  of  matrix  A  are
            replaced with bidiagonal  matrix  B.  Other  elements  contain  elementary
            reflections which form MxM matrix Q and NxN matrix P, respectively.

            If M>=N, B is the upper  bidiagonal  MxN  matrix  and  is  stored  in  the
            corresponding  elements  of  matrix  A.  Matrix  Q  is  represented  as  a
            product   of   elementary   reflections   Q = H(0)*H(1)*...*H(n-1),  where
            H(i) = 1-tau*v*v'. Here tau is a scalar which is stored  in  TauQ[i],  and
            vector v has the following  structure:  v(0:i-1)=0, v(i)=1, v(i+1:m-1)  is
            stored   in   elements   A(i+1:m-1,i).   Matrix   P  is  as  follows:  P =
            G(0)*G(1)*...*G(n-2), where G(i) = 1 - tau*u*u'. Tau is stored in TauP[i],
            u(0:i)=0, u(i+1)=1, u(i+2:n-1) is stored in elements A(i,i+2:n-1).

            If M<N, B is the  lower  bidiagonal  MxN  matrix  and  is  stored  in  the
            corresponding   elements  of  matrix  A.  Q = H(0)*H(1)*...*H(m-2),  where
            H(i) = 1 - tau*v*v', tau is stored in TauQ, v(0:i)=0, v(i+1)=1, v(i+2:m-1)
            is    stored    in   elements   A(i+2:m-1,i).    P = G(0)*G(1)*...*G(m-1),
            G(i) = 1-tau*u*u', tau is stored in  TauP,  u(0:i-1)=0, u(i)=1, u(i+1:n-1)
            is stored in A(i,i+1:n-1).

            EXAMPLE:

            m=6, n=5 (m > n):               m=5, n=6 (m < n):

            (  d   e   u1  u1  u1 )         (  d   u1  u1  u1  u1  u1 )
            (  v1  d   e   u2  u2 )         (  e   d   u2  u2  u2  u2 )
            (  v1  v2  d   e   u3 )         (  v1  e   d   u3  u3  u3 )
            (  v1  v2  v3  d   e  )         (  v1  v2  e   d   u4  u4 )
            (  v1  v2  v3  v4  d  )         (  v1  v2  v3  e   d   u5 )
            (  v1  v2  v3  v4  v5 )

            Here vi and ui are vectors which form H(i) and G(i), and d and e -
            are the diagonal and off-diagonal elements of matrix B.

              -- LAPACK routine (version 3.0) --
                 Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
                 Courant Institute, Argonne National Lab, and Rice University
                 September 30, 1994.
                 Sergey Bochkanov, ALGLIB project, translation from FORTRAN to
                 pseudocode, 2007-2010.
            *************************************************************************/
        public static void ReductionToBidiagonalForm(ref Matrix<double> a,
            int m,
            int n,
            ref double[] tauq,
            ref double[] taup)
        {
            double[] work = new double[0];
            double[] t = new double[0];
            int maxmn = 0;
            int i = 0;
            double ltau = 0;
            int i_ = 0;
            int i1_ = 0;

            tauq = new double[0];
            taup = new double[0];


            //
            // Prepare
            //
            if (n <= 0 || m <= 0)
            {
                return;
            }
            maxmn = Math.Max(m, n);
            work = new double[maxmn + 1];
            t = new double[maxmn + 1];
            if (m >= n)
            {
                tauq = new double[n];
                taup = new double[n];
            }
            else
            {
                tauq = new double[m];
                taup = new double[m];
            }
            if (m >= n)
            {

                //
                // Reduce to upper bidiagonal form
                //
                for (i = 0; i <= n - 1; i++)
                {

                    //
                    // Generate elementary reflector H(i) to annihilate A(i+1:m-1,i)
                    //
                    i1_ = (i) - (1);
                    for (i_ = 1; i_ <= m - i; i_++)
                    {
                        t[i_] = a[i_ + i1_, i];
                    }
                    Reflections.Generate(ref t, m - i, ref ltau);
                    tauq[i] = ltau;
                    i1_ = (1) - (i);
                    for (i_ = i; i_ <= m - 1; i_++)
                    {
                        a[i_, i] = t[i_ + i1_];
                    }
                    t[1] = 1;

                    //
                    // Apply H(i) to A(i:m-1,i+1:n-1) from the left
                    //
                    Reflections.ApplyFromTheLeft(ref a, ltau, t, i, m - 1, i + 1, n - 1, ref work);
                    if (i < n - 1)
                    {

                        //
                        // Generate elementary reflector G(i) to annihilate
                        // A(i,i+2:n-1)
                        //
                        i1_ = (i + 1) - (1);
                        for (i_ = 1; i_ <= n - i - 1; i_++)
                        {
                            t[i_] = a[i, i_ + i1_];
                        }
                        Reflections.Generate(ref t, n - 1 - i, ref ltau);
                        taup[i] = ltau;
                        i1_ = (1) - (i + 1);
                        for (i_ = i + 1; i_ <= n - 1; i_++)
                        {
                            a[i, i_] = t[i_ + i1_];
                        }
                        t[1] = 1;

                        //
                        // Apply G(i) to A(i+1:m-1,i+1:n-1) from the right
                        //
                        Reflections.ApplyFromTheRight(ref a, ltau, t, i + 1, m - 1, i + 1, n - 1, ref work);
                    }
                    else
                    {
                        taup[i] = 0;
                    }
                }
            }
            else
            {

                //
                // Reduce to lower bidiagonal form
                //
                for (i = 0; i <= m - 1; i++)
                {

                    //
                    // Generate elementary reflector G(i) to annihilate A(i,i+1:n-1)
                    //
                    i1_ = (i) - (1);
                    for (i_ = 1; i_ <= n - i; i_++)
                    {
                        t[i_] = a[i, i_ + i1_];
                    }
                    Reflections.Generate(ref t, n - i, ref ltau);
                    taup[i] = ltau;
                    i1_ = (1) - (i);
                    for (i_ = i; i_ <= n - 1; i_++)
                    {
                        a[i, i_] = t[i_ + i1_];
                    }
                    t[1] = 1;

                    //
                    // Apply G(i) to A(i+1:m-1,i:n-1) from the right
                    //
                    Reflections.ApplyFromTheRight(ref a, ltau, t, i + 1, m - 1, i, n - 1, ref work);
                    if (i < m - 1)
                    {

                        //
                        // Generate elementary reflector H(i) to annihilate
                        // A(i+2:m-1,i)
                        //
                        i1_ = (i + 1) - (1);
                        for (i_ = 1; i_ <= m - 1 - i; i_++)
                        {
                            t[i_] = a[i_ + i1_, i];
                        }
                        Reflections.Generate(ref t, m - 1 - i, ref ltau);
                        tauq[i] = ltau;
                        i1_ = (1) - (i + 1);
                        for (i_ = i + 1; i_ <= m - 1; i_++)
                        {
                            a[i_, i] = t[i_ + i1_];
                        }
                        t[1] = 1;

                        //
                        // Apply H(i) to A(i+1:m-1,i+1:n-1) from the left
                        //
                        Reflections.ApplyFromTheLeft(ref a, ltau, t, i + 1, m - 1, i + 1, n - 1, ref work);
                    }
                    else
                    {
                        tauq[i] = 0;
                    }
                }
            }
        }


        /*************************************************************************
            Unpacking matrix Q which reduces a matrix to bidiagonal form.

            Input parameters:
                QP          -   matrices Q and P in compact form.
                                Output of ToBidiagonal subroutine.
                M           -   number of rows in matrix A.
                N           -   number of columns in matrix A.
                TAUQ        -   scalar factors which are used to form Q.
                                Output of ToBidiagonal subroutine.
                QColumns    -   required number of columns in matrix Q.
                                M>=QColumns>=0.

            Output parameters:
                Q           -   first QColumns columns of matrix Q.
                                Array[0..M-1, 0..QColumns-1]
                                If QColumns=0, the array is not modified.

              -- ALGLIB --
                 2005-2010
                 Bochkanov Sergey
            *************************************************************************/
        public static void BidiagonalUnpackingQ(Matrix<double> qp,
            int m,
            int n,
            double[] tauq,
            int qcolumns,
            ref Matrix<double> q)
        {
            int i = 0;
            int j = 0;

            q = new Matrix<double>(0, 0);

            Utilities.Assert(qcolumns <= m, "RMatrixBDUnpackQ: QColumns>M!");
            Utilities.Assert(qcolumns >= 0, "RMatrixBDUnpackQ: QColumns<0!");
            if ((m == 0 || n == 0) || qcolumns == 0)
            {
                return;
            }

            //
            // prepare Q
            //
            q.Resize(m, qcolumns);
            for (i = 0; i <= m - 1; i++)
            {
                for (j = 0; j <= qcolumns - 1; j++)
                {
                    if (i == j)
                    {
                        q[i, j] = 1;
                    }
                    else
                    {
                        q[i, j] = 0;
                    }
                }
            }

            //
            // Calculate
            //
            BidiagonalMultiplyByQ(qp, m, n, tauq, ref q, m, qcolumns, false, false);
        }


        /*************************************************************************
            Multiplication by matrix Q which reduces matrix A to  bidiagonal form.

            The algorithm allows pre- or post-multiply by Q or Q'.

            Input parameters:
                QP          -   matrices Q and P in compact form.
                                Output of ToBidiagonal subroutine.
                M           -   number of rows in matrix A.
                N           -   number of columns in matrix A.
                TAUQ        -   scalar factors which are used to form Q.
                                Output of ToBidiagonal subroutine.
                Z           -   multiplied matrix.
                                array[0..ZRows-1,0..ZColumns-1]
                ZRows       -   number of rows in matrix Z. If FromTheRight=False,
                                ZRows=M, otherwise ZRows can be arbitrary.
                ZColumns    -   number of columns in matrix Z. If FromTheRight=True,
                                ZColumns=M, otherwise ZColumns can be arbitrary.
                FromTheRight -  pre- or post-multiply.
                DoTranspose -   multiply by Q or Q'.

            Output parameters:
                Z           -   product of Z and Q.
                                Array[0..ZRows-1,0..ZColumns-1]
                                If ZRows=0 or ZColumns=0, the array is not modified.

              -- ALGLIB --
                 2005-2010
                 Bochkanov Sergey
            *************************************************************************/
        public static void BidiagonalMultiplyByQ(Matrix<double> qp,
            int m,
            int n,
            double[] tauq,
            ref Matrix<double> z,
            int zrows,
            int zcolumns,
            bool fromtheright,
            bool dotranspose)
        {
            int i = 0;
            int i1 = 0;
            int i2 = 0;
            int istep = 0;
            double[] v = new double[0];
            double[] work = new double[0];
            int mx = 0;
            int i_ = 0;
            int i1_ = 0;

            if (((m <= 0 || n <= 0) || zrows <= 0) || zcolumns <= 0)
            {
                return;
            }
            Utilities.Assert((fromtheright && zcolumns == m) || (!fromtheright && zrows == m), "RMatrixBDMultiplyByQ: incorrect Z size!");

            //
            // init
            //
            mx = Math.Max(m, n);
            mx = Math.Max(mx, zrows);
            mx = Math.Max(mx, zcolumns);
            v = new double[mx + 1];
            work = new double[mx + 1];
            if (m >= n)
            {

                //
                // setup
                //
                if (fromtheright)
                {
                    i1 = 0;
                    i2 = n - 1;
                    istep = 1;
                }
                else
                {
                    i1 = n - 1;
                    i2 = 0;
                    istep = -1;
                }
                if (dotranspose)
                {
                    i = i1;
                    i1 = i2;
                    i2 = i;
                    istep = -istep;
                }

                //
                // Process
                //
                i = i1;
                do
                {
                    i1_ = (i) - (1);
                    for (i_ = 1; i_ <= m - i; i_++)
                    {
                        v[i_] = qp[i_ + i1_, i];
                    }
                    v[1] = 1;
                    if (fromtheright)
                    {
                        Reflections.ApplyFromTheRight(ref z, tauq[i], v, 0, zrows - 1, i, m - 1, ref work);
                    }
                    else
                    {
                        Reflections.ApplyFromTheLeft(ref z, tauq[i], v, i, m - 1, 0, zcolumns - 1, ref work);
                    }
                    i = i + istep;
                }
                while (i != i2 + istep);
            }
            else
            {

                //
                // setup
                //
                if (fromtheright)
                {
                    i1 = 0;
                    i2 = m - 2;
                    istep = 1;
                }
                else
                {
                    i1 = m - 2;
                    i2 = 0;
                    istep = -1;
                }
                if (dotranspose)
                {
                    i = i1;
                    i1 = i2;
                    i2 = i;
                    istep = -istep;
                }

                //
                // Process
                //
                if (m - 1 > 0)
                {
                    i = i1;
                    do
                    {
                        i1_ = (i + 1) - (1);
                        for (i_ = 1; i_ <= m - i - 1; i_++)
                        {
                            v[i_] = qp[i_ + i1_, i];
                        }
                        v[1] = 1;
                        if (fromtheright)
                        {
                            Reflections.ApplyFromTheRight(ref z, tauq[i], v, 0, zrows - 1, i + 1, m - 1, ref work);
                        }
                        else
                        {
                            Reflections.ApplyFromTheLeft(ref z, tauq[i], v, i + 1, m - 1, 0, zcolumns - 1, ref work);
                        }
                        i = i + istep;
                    }
                    while (i != i2 + istep);
                }
            }
        }


        /*************************************************************************
            Unpacking matrix P which reduces matrix A to bidiagonal form.
            The subroutine returns transposed matrix P.

            Input parameters:
                QP      -   matrices Q and P in compact form.
                            Output of ToBidiagonal subroutine.
                M       -   number of rows in matrix A.
                N       -   number of columns in matrix A.
                TAUP    -   scalar factors which are used to form P.
                            Output of ToBidiagonal subroutine.
                PTRows  -   required number of rows of matrix P^T. N >= PTRows >= 0.

            Output parameters:
                PT      -   first PTRows columns of matrix P^T
                            Array[0..PTRows-1, 0..N-1]
                            If PTRows=0, the array is not modified.

              -- ALGLIB --
                 2005-2010
                 Bochkanov Sergey
            *************************************************************************/
        public static void BidiagonalUnpackingP(Matrix<double> qp,
            int m,
            int n,
            double[] taup,
            int ptrows,
            ref Matrix<double> pt)
        {
            int i = 0;
            int j = 0;

            Utilities.Assert(ptrows <= n, "RMatrixBDUnpackPT: PTRows>N!");
            Utilities.Assert(ptrows >= 0, "RMatrixBDUnpackPT: PTRows<0!");
            if ((m == 0 || n == 0) || ptrows == 0)
            {
                return;
            }

            //
            // prepare PT
            //
            pt.Resize(ptrows, n);
            for (i = 0; i <= ptrows - 1; i++)
            {
                for (j = 0; j <= n - 1; j++)
                {
                    if (i == j)
                    {
                        pt[i, j] = 1;
                    }
                    else
                    {
                        pt[i, j] = 0;
                    }
                }
            }

            //
            // Calculate
            //
            BidiagonalMultiplyByP(qp, m, n, taup, ref pt, ptrows, n, true, true);
        }


        /*************************************************************************
            Multiplication by matrix P which reduces matrix A to  bidiagonal form.

            The algorithm allows pre- or post-multiply by P or P'.

            Input parameters:
                QP          -   matrices Q and P in compact form.
                                Output of RMatrixBD subroutine.
                M           -   number of rows in matrix A.
                N           -   number of columns in matrix A.
                TAUP        -   scalar factors which are used to form P.
                                Output of RMatrixBD subroutine.
                Z           -   multiplied matrix.
                                Array whose indexes range within [0..ZRows-1,0..ZColumns-1].
                ZRows       -   number of rows in matrix Z. If FromTheRight=False,
                                ZRows=N, otherwise ZRows can be arbitrary.
                ZColumns    -   number of columns in matrix Z. If FromTheRight=True,
                                ZColumns=N, otherwise ZColumns can be arbitrary.
                FromTheRight -  pre- or post-multiply.
                DoTranspose -   multiply by P or P'.

            Output parameters:
                Z - product of Z and P.
                            Array whose indexes range within [0..ZRows-1,0..ZColumns-1].
                            If ZRows=0 or ZColumns=0, the array is not modified.

              -- ALGLIB --
                 2005-2010
                 Bochkanov Sergey
            *************************************************************************/
        public static void BidiagonalMultiplyByP(Matrix<double> qp,
            int m,
            int n,
            double[] taup,
            ref Matrix<double> z,
            int zrows,
            int zcolumns,
            bool fromtheright,
            bool dotranspose)
        {
            int i = 0;
            double[] v = new double[0];
            double[] work = new double[0];
            int mx = 0;
            int i1 = 0;
            int i2 = 0;
            int istep = 0;
            int i_ = 0;
            int i1_ = 0;

            if (((m <= 0 || n <= 0) || zrows <= 0) || zcolumns <= 0)
            {
                return;
            }
            Utilities.Assert((fromtheright && zcolumns == n) || (!fromtheright && zrows == n), "RMatrixBDMultiplyByP: incorrect Z size!");

            //
            // init
            //
            mx = Math.Max(m, n);
            mx = Math.Max(mx, zrows);
            mx = Math.Max(mx, zcolumns);
            v = new double[mx + 1];
            work = new double[mx + 1];
            if (m >= n)
            {

                //
                // setup
                //
                if (fromtheright)
                {
                    i1 = n - 2;
                    i2 = 0;
                    istep = -1;
                }
                else
                {
                    i1 = 0;
                    i2 = n - 2;
                    istep = 1;
                }
                if (!dotranspose)
                {
                    i = i1;
                    i1 = i2;
                    i2 = i;
                    istep = -istep;
                }

                //
                // Process
                //
                if (n - 1 > 0)
                {
                    i = i1;
                    do
                    {
                        i1_ = (i + 1) - (1);
                        for (i_ = 1; i_ <= n - 1 - i; i_++)
                        {
                            v[i_] = qp[i, i_ + i1_];
                        }
                        v[1] = 1;
                        if (fromtheright)
                        {
                            Reflections.ApplyFromTheRight(ref z, taup[i], v, 0, zrows - 1, i + 1, n - 1, ref work);
                        }
                        else
                        {
                            Reflections.ApplyFromTheLeft(ref z, taup[i], v, i + 1, n - 1, 0, zcolumns - 1, ref work);
                        }
                        i = i + istep;
                    }
                    while (i != i2 + istep);
                }
            }
            else
            {

                //
                // setup
                //
                if (fromtheright)
                {
                    i1 = m - 1;
                    i2 = 0;
                    istep = -1;
                }
                else
                {
                    i1 = 0;
                    i2 = m - 1;
                    istep = 1;
                }
                if (!dotranspose)
                {
                    i = i1;
                    i1 = i2;
                    i2 = i;
                    istep = -istep;
                }

                //
                // Process
                //
                i = i1;
                do
                {
                    i1_ = (i) - (1);
                    for (i_ = 1; i_ <= n - i; i_++)
                    {
                        v[i_] = qp[i, i_ + i1_];
                    }
                    v[1] = 1;
                    if (fromtheright)
                    {
                        Reflections.ApplyFromTheRight(ref z, taup[i], v, 0, zrows - 1, i, n - 1, ref work);
                    }
                    else
                    {
                        Reflections.ApplyFromTheLeft(ref z, taup[i], v, i, n - 1, 0, zcolumns - 1, ref work);
                    }
                    i = i + istep;
                }
                while (i != i2 + istep);
            }
        }


        /*************************************************************************
            Unpacking of the main and secondary diagonals of bidiagonal decomposition
            of matrix A.

            Input parameters:
                B   -   output of RMatrixBD subroutine.
                M   -   number of rows in matrix B.
                N   -   number of columns in matrix B.

            Output parameters:
                IsUpper -   True, if the matrix is upper bidiagonal.
                            otherwise IsUpper is False.
                D       -   the main diagonal.
                            Array whose index ranges within [0..Min(M,N)-1].
                E       -   the secondary diagonal (upper or lower, depending on
                            the value of IsUpper).
                            Array index ranges within [0..Min(M,N)-1], the last
                            element is not used.

              -- ALGLIB --
                 2005-2010
                 Bochkanov Sergey
            *************************************************************************/
        public static void BidiagonalUnpackingDiagnols(Matrix<double> b,
            int m,
            int n,
            ref bool isupper,
            ref double[] d,
            ref double[] e)
        {
            int i = 0;

            isupper = new bool();
            d = new double[0];
            e = new double[0];

            isupper = m >= n;
            if (m <= 0 || n <= 0)
            {
                return;
            }
            if (isupper)
            {
                d = new double[n];
                e = new double[n];
                for (i = 0; i <= n - 2; i++)
                {
                    d[i] = b[i, i];
                    e[i] = b[i, i + 1];
                }
                d[n - 1] = b[n - 1, n - 1];
            }
            else
            {
                d = new double[m];
                e = new double[m];
                for (i = 0; i <= m - 2; i++)
                {
                    d[i] = b[i, i];
                    e[i] = b[i + 1, i];
                }
                d[m - 1] = b[m - 1, m - 1];
            }
        }


        /*************************************************************************
            Generate block reflector:
            * fill unused parts of reflectors matrix by zeros
            * fill diagonal of reflectors matrix by ones
            * generate triangular factor T

            PARAMETERS:
                A           -   either LengthA*BlockSize (if ColumnwiseA) or
                                BlockSize*LengthA (if not ColumnwiseA) matrix of
                                elementary reflectors.
                                Modified on exit.
                Tau         -   scalar factors
                ColumnwiseA -   reflectors are stored in rows or in columns
                LengthA     -   length of largest reflector
                BlockSize   -   number of reflectors
                T           -   array[BlockSize,2*BlockSize]. Left BlockSize*BlockSize
                                submatrix stores triangular factor on exit.
                WORK        -   array[BlockSize]
            
              -- ALGLIB routine --
                 17.02.2010
                 Bochkanov Sergey
            *************************************************************************/
        private static void GenerateBlockReflector(ref Matrix<double> a,
            ref double[] tau,
            bool columnwisea,
            int lengtha,
            int blocksize,
            ref Matrix<double> t,
            ref double[] work)
        {
            int i = 0;
            int j = 0;
            int k = 0;
            double v = 0;
            int i_ = 0;
            int i1_ = 0;


            //
            // fill beginning of new column with zeros,
            // load 1.0 in the first non-zero element
            //
            for (k = 0; k <= blocksize - 1; k++)
            {
                if (columnwisea)
                {
                    for (i = 0; i <= k - 1; i++)
                    {
                        a[i, k] = 0;
                    }
                }
                else
                {
                    for (i = 0; i <= k - 1; i++)
                    {
                        a[k, i] = 0;
                    }
                }
                a[k, k] = 1;
            }

            //
            // Calculate Gram matrix of A
            //
            for (i = 0; i <= blocksize - 1; i++)
            {
                for (j = 0; j <= blocksize - 1; j++)
                {
                    t[i, blocksize + j] = 0;
                }
            }
            for (k = 0; k <= lengtha - 1; k++)
            {
                for (j = 1; j <= blocksize - 1; j++)
                {
                    if (columnwisea)
                    {
                        v = a[k, j];
                        if (v != 0)
                        {
                            i1_ = (0) - (blocksize);
                            for (i_ = blocksize; i_ <= blocksize + j - 1; i_++)
                            {
                                t[j, i_] = t[j, i_] + v * a[k, i_ + i1_];
                            }
                        }
                    }
                    else
                    {
                        v = a[j, k];
                        if (v != 0)
                        {
                            i1_ = (0) - (blocksize);
                            for (i_ = blocksize; i_ <= blocksize + j - 1; i_++)
                            {
                                t[j, i_] = t[j, i_] + v * a[i_ + i1_, k];
                            }
                        }
                    }
                }
            }

            //
            // Prepare Y (stored in TmpA) and T (stored in TmpT)
            //
            for (k = 0; k <= blocksize - 1; k++)
            {

                //
                // fill non-zero part of T, use pre-calculated Gram matrix
                //
                i1_ = (blocksize) - (0);
                for (i_ = 0; i_ <= k - 1; i_++)
                {
                    work[i_] = t[k, i_ + i1_];
                }
                for (i = 0; i <= k - 1; i++)
                {
                    v = 0.0;
                    for (i_ = i; i_ <= k - 1; i_++)
                    {
                        v += t[i, i_] * work[i_];
                    }
                    t[i, k] = -(tau[k] * v);
                }
                t[k, k] = -tau[k];

                //
                // Rest of T is filled by zeros
                //
                for (i = k + 1; i <= blocksize - 1; i++)
                {
                    t[i, k] = 0;
                }
            }
        }



    }
}