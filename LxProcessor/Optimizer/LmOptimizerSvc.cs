﻿using System;
using System.Collections.Generic;
using System.Linq;
using Aws.Core.Utilities;
using LxCommon.DataStructures;
using LxProcessor.Common;

namespace LxProcessor.Optimizer
{
    public static class LmOptimizerSvc
    {
        public delegate void NdimensionalFvec(IList<double> arg, IList<double> fi, object obj);
        public delegate void NdimensionalJac(IList<double> arg, IList<double> fi, Matrix<double> jac, object obj);
        public delegate void NdimensionalRep(IList<double> arg, double func, object obj);

        private readonly static ConcurrentSimplePool<MinLm.State> _minLmStatePool;
        private readonly static ConcurrentSimplePool<MinLm.Report> _minLmReportPool;
        private readonly static ConcurrentSimplePool<FluxList<double>> _solutionsPool;

        static LmOptimizerSvc()
        {
            _minLmStatePool = new ConcurrentSimplePool<MinLm.State>(Config.MinLmStatePoolSize, 
                () => { return new MinLm.State(); }, 
                (state) => { state.CleanForReuse(); },
                "_minLmStatePool");

            _minLmReportPool = new ConcurrentSimplePool<MinLm.Report>(Config.MinLmReportPoolSize,
                () => { return new MinLm.Report(); },
                (rep) => { rep.CleanForReuse(); },
                "_minLmReportPool");


            _solutionsPool = new ConcurrentSimplePool<FluxList<double>>(Config.SolutionsPoolSize,
                () => { return new FluxList<double>(); },
                (list) => { list.Clear(); },
                "_solutionsPool");
        }

        
        /* This is the Levenberg-Marquardt algorithmn used to curve fit the solution.
        Common optimization methods of the algoritmn:
        This a optimization algoritmn. it should be supplied with the starting point
        begiining at starting point , it generates a sequence of iterates. Process of iteration
        terminates when there is no more progress aor the solution has been approximated to 
        sufficient accuracy. 




            */
        public static double[] RefineSolution(double[,] observations, double[] solutions, NdimensionalFvec fvec, NdimensionalJac jac, double epsg, 
            double epsf, double epsx, int maxIterations)
        {
            //define a minlm state pool
            MinLm.State state = _minLmStatePool.Take();
            //define a flux list solutions pool
            FluxList<double> slns = _solutionsPool.Take();
            //add the solutions to the slns flux list created above
            slns.AddRange(solutions);
            //get the total count of the slns varaible
            int n = slns.Count;
            /*       This function is used to find minimum of function which is represented  as
            sum of squares:
            F(x) = f[0] ^ 2(x[0],..., x[n - 1]) + ... + f[m - 1] ^ 2(x[0],..., x[n - 1])
            using value of function vector f[] and Jacobian of f[].*/
            MinLm.CreateVj(n, observations.GetLength(0), slns, state);
            //This function sets stopping conditions for Levenberg-Marquardt optimization
            //algorithm.
            MinLm.SetConditions(state, epsg, epsf, epsx, maxIterations);
            //
            Optimize(state, fvec, jac, null, observations);

            FluxList<double> refined = _solutionsPool.Take();
            MinLm.Report rep = _minLmReportPool.Take();
            MinLm.GetResults(state, ref refined, rep);
            
            _minLmStatePool.Return(state);
            _minLmReportPool.Return(rep);
            _solutionsPool.Return(slns);
            double[] solution = refined.ToArray();
            _solutionsPool.Return(refined);

            return solution;
        }

        private static void Optimize(MinLm.State state, NdimensionalFvec fvec, NdimensionalJac jac, NdimensionalRep rep, object obj)
        {
            //check for the f-vector and the jacobian
            if (fvec == null)
            {
                throw new ArgumentNullException("fvec");
            }
            if (jac == null)
            {
                throw new ArgumentNullException("jac");
            }        
            
            while (MinLm.Iteration(state))
            {
                if (state.needfi)
                {
                    fvec(state.x, state.fi, obj);
                    continue;
                }
                if (state.needfij)
                {
                    jac(state.x, state.fi, state.j, obj);
                    continue;
                }
                if (state.xupdated)
                {
                    if (rep != null)
                    {
                        rep(state.x, state.f, obj);
                    }
                    continue;
                }
                throw new Exception("LmOptimizerSvc.Optimize (some derivatives were not provided?)");
            }
        }
    }
}
