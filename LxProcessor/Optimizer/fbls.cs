using System.Collections.Generic;
using LxCommon.DataStructures;
using LxCommon.Utilities;
using LxProcessor.Common;

namespace LxProcessor.Optimizer
{
    public class Fbls
    {
        /*************************************************************************
            Basic Cholesky solver for ScaleA*Cholesky(A)'*x = y.

            This subroutine assumes that:
            * A*ScaleA is well scaled
            * A is well-conditioned, so no zero divisions or overflow may occur

            INPUT PARAMETERS:
                CHA     -   Cholesky decomposition of A
                SqrtScaleA- square root of scale factor ScaleA
                N       -   matrix size, N>=0.
                IsUpper -   storage type
                XB      -   right part
                Tmp     -   buffer; function automatically allocates it, if it is  too
                            small.  It  can  be  reused  if function is called several
                            times.
                        
            OUTPUT PARAMETERS:
                XB      -   solution

            NOTE 1: no assertion or tests are done during algorithm operation
            NOTE 2: N=0 will force algorithm to silently return

              -- ALGLIB --
                 Copyright 13.10.2010 by Bochkanov Sergey
            *************************************************************************/
        public static void CholeskySolver(Matrix<double> cha,
            double sqrtscalea,
            int n,
            bool isupper,
            ref FluxList<double> xb,
            ref FluxList<double> tmp)
        {
            int i = 0;
            double v = 0;
            int i_ = 0;

            if (n == 0)
            {
                return;
            }
            if (tmp.Count < n)
            {
                tmp.Resize(n);
            }

            //
            // A = L*L' or A=U'*U
            //
            if (isupper)
            {

                //
                // Solve U'*y=b first.
                //
                for (i = 0; i <= n - 1; i++)
                {
                    xb[i] = xb[i] / (sqrtscalea * cha[i, i]);
                    if (i < n - 1)
                    {
                        v = xb[i];
                        for (i_ = i + 1; i_ <= n - 1; i_++)
                        {
                            tmp[i_] = sqrtscalea * cha[i, i_];
                        }
                        for (i_ = i + 1; i_ <= n - 1; i_++)
                        {
                            xb[i_] = xb[i_] - v * tmp[i_];
                        }
                    }
                }

                //
                // Solve U*x=y then.
                //
                for (i = n - 1; i >= 0; i--)
                {
                    if (i < n - 1)
                    {
                        for (i_ = i + 1; i_ <= n - 1; i_++)
                        {
                            tmp[i_] = sqrtscalea * cha[i, i_];
                        }
                        v = 0.0;
                        for (i_ = i + 1; i_ <= n - 1; i_++)
                        {
                            v += tmp[i_] * xb[i_];
                        }
                        xb[i] = xb[i] - v;
                    }
                    xb[i] = xb[i] / (sqrtscalea * cha[i, i]);
                }
            }
            else
            {

                //
                // Solve L*y=b first
                //
                for (i = 0; i <= n - 1; i++)
                {
                    if (i > 0)
                    {
                        for (i_ = 0; i_ <= i - 1; i_++)
                        {
                            tmp[i_] = sqrtscalea * cha[i, i_];
                        }
                        v = 0.0;
                        for (i_ = 0; i_ <= i - 1; i_++)
                        {
                            v += tmp[i_] * xb[i_];
                        }
                        xb[i] = xb[i] - v;
                    }
                    xb[i] = xb[i] / (sqrtscalea * cha[i, i]);
                }

                //
                // Solve L'*x=y then.
                //
                for (i = n - 1; i >= 0; i--)
                {
                    xb[i] = xb[i] / (sqrtscalea * cha[i, i]);
                    if (i > 0)
                    {
                        v = xb[i];
                        for (i_ = 0; i_ <= i - 1; i_++)
                        {
                            tmp[i_] = sqrtscalea * cha[i, i_];
                        }
                        for (i_ = 0; i_ <= i - 1; i_++)
                        {
                            xb[i_] = xb[i_] - v * tmp[i_];
                        }
                    }
                }
            }
        }



    }
}