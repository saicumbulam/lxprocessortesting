﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;

using LxCommon;
using LxCommon.Models;

namespace LxProcessor.RawData
{
    public class PulseData
    {
        private List<Sample> _highFrequencySamples;
        //private List<Sample> _lowFrequencySamples;
        //private List<Sample> _spectraSamples;
        private string _sensorId;
        private SensorVersion _major;
        private SensorVersion _minor;
        private DateTime _timeStampUtc;
        private int _offsetInNanoseconds;
        private int _ticksToFirstSample;
        private int _ticksBetweenSamples;
        private int _ticksInThisSeconds;
        private int _sensorAttenuation;
        private int _epochTime;

        public PulseData()
        {
            _highFrequencySamples = new List<Sample>(2000);
            //_lowFrequencySamples = new List<Sample>(1000);
            //_spectraSamples = new List<Sample>(1000);
            _sensorId = string.Empty;
            _major = SensorVersion.ID_VERSION_UNKNOW;
            _minor = SensorVersion.ID_VERSION_UNKNOW;
            _epochTime = 0;
            _timeStampUtc = LtgTimeUtils.GetUtcDateTimeFrom1970Seconds(EpochTime);
            _offsetInNanoseconds = 0;
            _ticksToFirstSample = 0;
            _ticksBetweenSamples = 0;
            _ticksInThisSeconds = 0;
            _sensorAttenuation = 0;
        }

        public bool Initalize(PacketHeaderInfo packet)
        {
            bool isValid = false;
            if (packet.MsgType == PacketType.MsgTypeLtg)
            {
                if (packet.MinorVersion == 0)
                {
                    isValid = GetSensorPulseData9_0(packet.RawData, _highFrequencySamples);
                }
                else if (packet.MinorVersion == SensorVersion.ID_VERSION_SENSOR_9_MINOR1)
                {
                    isValid = GetSensorPulseData9_1(packet.RawData, _highFrequencySamples);

                }
                else if (packet.MinorVersion == SensorVersion.ID_VERSION_SENSOR_9_MINOR2)
                {
                    isValid = GetSensorPulseData9_2(packet.RawData, _highFrequencySamples);

                }
                else if (packet.MinorVersion == SensorVersion.ID_VERSION_SENSOR_9_MINOR3)
                {
                    isValid = GetSensorPulseData9_3(packet, _highFrequencySamples);
                }
                else if (packet.MinorVersion == SensorVersion.ID_VERSION_SENSOR_9_MINOR4)
                {
                    isValid = GetSensorPulseData9_4(packet, _highFrequencySamples);
                }

                if (isValid)
                {
                    _sensorId = packet.SensorId;
                    _epochTime = packet.PacketTimeInSeconds;
                    _timeStampUtc = LtgTimeUtils.GetUtcDateTimeFrom1970Seconds(EpochTime);
                    _major = packet.MajorVersion;
                    _minor = packet.MinorVersion;
                    _offsetInNanoseconds = packet.OffsetInNanoseconds;
                    _ticksToFirstSample = packet.HFSclkTicksPpsToFirstSample;
                    _ticksBetweenSamples = packet.HFSclkTicksBetweenSamples;
                    _ticksInThisSeconds = packet.HFSclkTicksInThisSec;
                    _sensorAttenuation = packet.SensorAttenuation;
                }
            }

            return isValid;
        }

        public void CleanForReuse()
        {
            _sensorId = string.Empty;
            _major = SensorVersion.ID_VERSION_UNKNOW;
            _minor = SensorVersion.ID_VERSION_UNKNOW;
            _epochTime = 0;
            _timeStampUtc = LtgTimeUtils.GetUtcDateTimeFrom1970Seconds(EpochTime);
            _offsetInNanoseconds = 0;
            _ticksToFirstSample = 0;
            _ticksBetweenSamples = 0;
            _ticksInThisSeconds = 0;
            _sensorAttenuation = 0;
            if (_highFrequencySamples != null)
            {
                _highFrequencySamples.Clear();
            }
        }

        private bool GetSensorPulseData9_0(byte[] data, List<Sample> highFrequencySamples)
        {
            //index for Initial Tick Number
            int j = 0;
            byte[] b2b = new byte[2];
            int nDataBytes = 0;
            int nIndexData = 0;
            bool isValid = false;

            nIndexData = 27;
            if (data.Length > nIndexData)
            {
                j = nIndexData - 2;
                b2b[0] = data[j++];
                b2b[1] = data[j++];
                nDataBytes = IPAddress.NetworkToHostOrder((short) RawPacketUtil.ByteArrayToInt(b2b, 2));

                if (nDataBytes >= 7)
                {
                    while (j + 1 < nIndexData + nDataBytes)
                    {
                        GetPulseData(highFrequencySamples, data, ref j);
                    }
                }

                isValid = true;
            }

            return isValid;
        }

        private void GetPulseData(List<Sample> highFrequencySamples, byte[] data, ref int nPos)
        {

            if (nPos + 7 <= data.Length)
            {
                byte[] b1b = new byte[1];
                byte[] b2b = new byte[2];
                byte[] b4b = new byte[4];
                int j = nPos;

                UInt32 initTick;
                short initE;
                short prevE;
                int nRemainBytes = 0;

                //pulse section header
                b4b[0] = data[j++];
                b4b[1] = data[j++];
                b4b[2] = data[j++];
                b4b[3] = data[j++];
                initTick = (UInt32)IPAddress.NetworkToHostOrder(RawPacketUtil.ByteArrayToInt(b4b, 4));
                b2b[0] = data[j++];
                b2b[1] = data[j++];
                initE = IPAddress.NetworkToHostOrder((short)RawPacketUtil.ByteArrayToInt(b2b, 2));
                prevE = initE;
                b1b[0] = data[j++];
                nRemainBytes = RawPacketUtil.ByteArrayToInt(b1b, 1);
                Sample sample0 = new Sample();

                sample0.E = initE;
                sample0.Tick = initTick;
                highFrequencySamples.Add(sample0);

                //Delt Es
                for (int i = 0; i < nRemainBytes; i++)
                {
                    Sample sample = new Sample();
                    sample.Tick = (uint)(initTick + i + 1);

                    if (j >= data.Length)
                    {
                        string msg = string.Format("Partial packet found, length:{0}, bytes missing:{1}", data.Length,
                            nRemainBytes - j);
                        throw new Exception(msg);
                    }

                    b1b[0] = data[j++];
                    sbyte c = (sbyte)(b1b[0]);
                    sample.E = (short)(prevE + c);

                    highFrequencySamples.Add(sample);

                    prevE = sample.E;
                }
                nPos = j;
            }
        }

        private bool GetSensorPulseData9_1(byte[] data, List<Sample> highFrequencySamples)
        {
            //index for Initial Tick Number
            int j = 0;
            byte[] b2b = new byte[2];
            byte[] b4b = new byte[4];
            int nDataBytes = 0;
            int nIndexData = 0;
            bool isValid = false;

            nIndexData = 38;
            if (data.Length > nIndexData)
            {
                j = nIndexData - 2;
                b2b[0] = data[j++];
                b2b[1] = data[j++];
                nDataBytes = IPAddress.NetworkToHostOrder((short) RawPacketUtil.ByteArrayToInt(b2b, 2));

                if (nDataBytes >= 6)
                {
                    int nTotalData = nIndexData + nDataBytes - 1;
                    int nDataArraySize = nDataBytes/6;

                    Sample sample = new Sample();
                    int i = 0;
                    while (j < nTotalData && i < nDataArraySize)
                    {
                        b4b[0] = data[j++];
                        b4b[1] = data[j++];
                        b4b[2] = data[j++];
                        b4b[3] = data[j++];
                        sample.Tick = (UInt32)IPAddress.NetworkToHostOrder(RawPacketUtil.ByteArrayToInt(b4b, 4));
                        b2b[0] = data[j++];
                        b2b[1] = data[j++];
                        sample.E = IPAddress.NetworkToHostOrder((short)RawPacketUtil.ByteArrayToInt(b2b, 2));
                        highFrequencySamples.Add(sample);
                        i++;
                    }

                    isValid = true;
                }    
            }

            return isValid;
        }

        private bool GetSensorPulseData9_2(byte[] data, List<Sample> highFrequencySamples)
        {
            bool isValid = false;
            //index for Initial Tick Number
            int j = 0;
            byte[] b2b = new byte[2];
            byte[] b4b = new byte[4];
            int nDataBytesHF = 0;
            int nDataBytesLF = 0;
            int nIndexData = 0;

            nIndexData = (int)PacketHeaderLength.HeaderLengthLtg92;
            if (data.Length > nIndexData)
            {
                j = 46;
                b2b[0] = data[j++];
                b2b[1] = data[j++];
                nDataBytesHF = IPAddress.NetworkToHostOrder((short) RawPacketUtil.ByteArrayToInt(b2b, 2));
                    // Total HF bytes of data

                b2b[0] = data[j++];
                b2b[1] = data[j++];
                nDataBytesLF = IPAddress.NetworkToHostOrder((short) RawPacketUtil.ByteArrayToInt(b2b, 2));
                    // Total HF bytes of data

                // Get HF data
                int nDataArraySize = nDataBytesHF/6;
                int i = 0;
                j = (int) PacketHeaderLength.HeaderLengthLtg92; // Start byte of HF data
                Sample sample = new Sample();
                while (i < nDataArraySize)
                {
                    // Sample theSample = new Sample();
                    b4b[0] = data[j++];
                    b4b[1] = data[j++];
                    b4b[2] = data[j++];
                    b4b[3] = data[j++];
                    sample.Tick = (UInt32)IPAddress.NetworkToHostOrder(RawPacketUtil.ByteArrayToInt(b4b, 4));
                    b2b[0] = data[j++];
                    b2b[1] = data[j++];
                    sample.E = IPAddress.NetworkToHostOrder((short)RawPacketUtil.ByteArrayToInt(b2b, 2));
                    highFrequencySamples.Add(sample);
                    i++;
                }
                

                // Get LF data
                //nDataArraySize = nDataBytesLF/6;
            
                //i = 0;
                //j = (int) PacketHeaderLength.HeaderLengthLtg92 + nDataBytesHF; // Start byte of LF data
                
                //while (i < nDataArraySize)
                //{
                //    b4b[0] = data[j++];
                //    b4b[1] = data[j++];
                //    b4b[2] = data[j++];
                //    b4b[3] = data[j++];
                //    sample.Tick = (UInt32)IPAddress.NetworkToHostOrder(RawPacketUtil.ByteArrayToInt(b4b, 4));
                //    b2b[0] = data[j++];
                //    b2b[1] = data[j++];
                //    sample.E = IPAddress.NetworkToHostOrder((short)RawPacketUtil.ByteArrayToInt(b2b, 2));
                //    lowFrequencySamples.Add(sample);
                //    i++;
                //}
                
                isValid = true;
            }


            return isValid;
        }

        private bool GetSensorPulseData9_3(PacketHeaderInfo header, List<Sample> highFrequencySamples)
        {
            bool isValid = false;

            byte[] data = header.RawData;

            int j = 0;
            byte[] b2b = new byte[2];
            byte[] b4b = new byte[4];
            int nDataBytesHF = 0;
            int nDataBytesLF = 0;
            int nIndexData = 0;

            nIndexData = (int)PacketHeaderLength.HeaderLengthLtg93;

            if (data.Length > nIndexData)
            {
                j = 52;
                b2b[0] = data[j++];
                b2b[1] = data[j++];
                nDataBytesHF = IPAddress.NetworkToHostOrder((short) RawPacketUtil.ByteArrayToInt(b2b, 2));
                    // Total HF bytes of data

                // Get LF sample data size
                j = 54;
                b2b[0] = data[j++];
                b2b[1] = data[j++];
                nDataBytesLF = IPAddress.NetworkToHostOrder((short)RawPacketUtil.ByteArrayToInt(b2b, 2));
                    // Total LF bytes of data

                // Get LF sample data size
                j = 56;
                b2b[0] = data[j++];
                b2b[1] = data[j++];

                // Get HF data
                int nDataArraySize = header.HFSampleNum;

                Sample sample = new Sample();
                int i = 0;
                j = (int) PacketHeaderLength.HeaderLengthLtg93; // Start byte of HF data
                while (i < nDataArraySize)
                {
                    // Sample theSample = new Sample();
                    b4b[0] = data[j++];
                    b4b[1] = data[j++];
                    b4b[2] = data[j++];
                    b4b[3] = data[j++];
                    sample.Tick = (UInt32)IPAddress.NetworkToHostOrder(RawPacketUtil.ByteArrayToInt(b4b, 4));
                    b2b[0] = data[j++];
                    b2b[1] = data[j++];
                    sample.E = IPAddress.NetworkToHostOrder((short)RawPacketUtil.ByteArrayToInt(b2b, 2));
                    highFrequencySamples.Add(sample);
                    i++;
                }
                     
                // Get LF data
                //nDataArraySize = header.LFSampleNum;
                    
                //i = 0;
                //j = (int) PacketHeaderLength.HeaderLengthLtg93 + nDataBytesHF; // Start byte of LF data
                    
                //while (i < nDataArraySize)
                //{
                //    b4b[0] = data[j++];
                //    b4b[1] = data[j++];
                //    b4b[2] = data[j++];
                //    b4b[3] = data[j++];
                //    sample.Tick = (UInt32) IPAddress.NetworkToHostOrder(RawPacketUtil.ByteArrayToInt(b4b, 4));
                //    b2b[0] = data[j++];
                //    b2b[1] = data[j++];
                //    sample.E = IPAddress.NetworkToHostOrder((short)RawPacketUtil.ByteArrayToInt(b2b, 2));
                //    lowFrequencySamples.Add(sample);
                //    i++;
                //}
                    
                // Get Spectra Data
                //nDataArraySize = header.SpectraSampleNum;
                    
                //i = 0;
                //j = (int) PacketHeaderLength.HeaderLengthLtg93 + nDataBytesHF + nDataBytesLF;
                    
                //while (i < nDataArraySize)
                //{
                //    b4b[0] = data[j++];
                //    b4b[1] = data[j++];
                //    b4b[2] = data[j++];
                //    b4b[3] = data[j++];
                //    sample.Tick = (UInt32)IPAddress.NetworkToHostOrder(RawPacketUtil.ByteArrayToInt(b4b, 4));
                //    b2b[0] = data[j++];
                //    b2b[1] = data[j++];
                //    sample.E = IPAddress.NetworkToHostOrder((short)RawPacketUtil.ByteArrayToInt(b2b, 2));
                //    spectraSamples.Add(sample);
                //    i++;
                //}

                isValid = true;
            }

            return isValid;
        }

        private  bool GetSensorPulseData9_4(PacketHeaderInfo header, List<Sample> highFrequencySamples)
        {
            bool isValid = false;

            //index for Initial Tick Number
            byte[] data = header.RawData;
            int j = 0;
            byte[] b2b = new byte[2];
            byte[] b4b = new byte[4];
            int nDataBytesHF = 0;
            int nDataBytesLF = 0;
            int nDataBytesSpectra = 0;
            int nIndexData = 0;
            byte[] theSample;
            ArrayList tempList;
            RawdataDecompressor decompressor = new RawdataDecompressor();
            
            // Get HF sample data size
            j = 52;
            b2b[0] = data[j++]; b2b[1] = data[j++];
            nDataBytesHF = IPAddress.NetworkToHostOrder((short)RawPacketUtil.ByteArrayToInt(b2b, 2)); // Total HF bytes of data

            // Get LF sample data size
            //j = 54;
            //b2b[0] = data[j++]; b2b[1] = data[j++];
            //nDataBytesLF = IPAddress.NetworkToHostOrder((short)RawPacketUtil.ByteArrayToInt(b2b, 2)); // Total LF bytes of data

            // Get Spectra data size
            //j = 56;
            //b2b[0] = data[j++]; b2b[1] = data[j++];
            //nDataBytesSpectra = IPAddress.NetworkToHostOrder((short)RawPacketUtil.ByteArrayToInt(b2b, 2)); // Total Spectra bytes of data

            // Get HF data
            if (nDataBytesHF != 0)
            {
                nIndexData = (int)PacketHeaderLength.HeaderLengthLtg94;
                theSample = new byte[nDataBytesHF];
                Array.Copy(data, nIndexData, theSample, 0, nDataBytesHF);
                // decompressor = new RawdataDecompressor();
                // New compression for the byte 50 in header
                decompressor.setMantissas(data[50]);
                decompressor.SetRawData(theSample);
                    
                decompressor.Decompress(highFrequencySamples);
                    
                header.HFSampleNum = highFrequencySamples.Count;
            }

            //if (nDataBytesLF != 0)
            //{
            //    // Get LF data
            //    nIndexData = (int)PacketHeaderLength.HeaderLengthLtg94 + nDataBytesHF;
            //    theSample = new byte[nDataBytesLF];
            //    Array.Copy(data, nIndexData, theSample, 0, nDataBytesLF);
            //    decompressor.setMantissas(data[50]);
            //    decompressor.SetRawData(theSample);

            //    decompressor.Decompress(lowFrequencySamples);
                    
            //    header.LFSampleNum = lowFrequencySamples.Count;
            //}

            // Spectra data is not compressed
            //if (nDataBytesSpectra != 0)
            //{
            //    int nDataArraySize = header.SpectraSampleNum;
            //    Sample sample = new Sample();
            //    int i = 0;
            //    j = (int)PacketHeaderLength.HeaderLengthLtg93 + nDataBytesHF + nDataBytesLF; // Start byte of Spectra data
            //    while (i < nDataArraySize)
            //    {
            //        b4b[0] = data[j++]; b4b[1] = data[j++]; b4b[2] = data[j++]; b4b[3] = data[j++];
            //        sample.Tick = (UInt32)IPAddress.NetworkToHostOrder(RawPacketUtil.ByteArrayToInt(b4b, 4));
            //        b2b[0] = data[j++]; b2b[1] = data[j++];
            //        sample.E = IPAddress.NetworkToHostOrder((short)RawPacketUtil.ByteArrayToInt(b2b, 2));
            //        spectraSamples.Add(sample);
            //        i++;
            //    }
            //}
            
            isValid = (highFrequencySamples.Count > 0);
            

            return isValid;
        }

        public void ConvertToWaveforms(List<Waveform> waveforms)
        {
            if (_major == SensorVersion.ID_VERSION_SENSOR_9_MAJOR)
            {
                if (_minor == SensorVersion.ID_VERSION_SENSOR_9_MINOR0)
                {
                    ConvertToWaveforms9_0(_highFrequencySamples, _ticksToFirstSample, _ticksBetweenSamples, _ticksInThisSeconds, _offsetInNanoseconds, _timeStampUtc, waveforms);
                }
                else if (_minor >= SensorVersion.ID_VERSION_SENSOR_9_MINOR1)
                {
                    ConvertToWaveforms9_1(_highFrequencySamples, _ticksToFirstSample, _ticksBetweenSamples, _ticksInThisSeconds, _offsetInNanoseconds, _timeStampUtc, waveforms);
                }
            }
        }

        private void ConvertToWaveforms9_0(List<Sample> highFrequencySamples, int ticksToFirstSample, int ticksBetweenSamples, int ticksInThisSeconds, 
            int offsetInNanoseconds, DateTime timeStampUtc, List<Waveform> waveforms)
        {
            if (highFrequencySamples.Count > 0)
            {
                foreach (Sample sample in highFrequencySamples)
                {
                    uint sampleNumber = sample.Tick;

                    int nanoseconds = (int)(1e9 * (float)(ticksToFirstSample + sampleNumber * ticksBetweenSamples)
                                            / ticksInThisSeconds) + offsetInNanoseconds;

                    if (nanoseconds < 0) continue;
                    if (nanoseconds > 999999999) continue;

                    Waveform waveform = new Waveform
                    {
                        TimeStamp = new DateTimeUtcNano(timeStampUtc, nanoseconds),
                        Amplitude = sample.E
                    };
                    waveforms.Add(waveform);
                }
            }
        }

        private void ConvertToWaveforms9_1(List<Sample> highFrequencySamples, int ticksToFirstSample, int ticksBetweenSamples, int ticksInThisSeconds,
            int offsetInNanoseconds, DateTime timeStampUtc, List<Waveform> waveforms)
        {
            if (highFrequencySamples.Count > 0)
            {
                Waveform prewaveform = new Waveform();

                foreach (Sample sample in highFrequencySamples)
                {
                    uint sampleNumber = sample.Tick;

                    int nanoseconds = (int)(1e9 * (float)(ticksToFirstSample + sampleNumber * ticksBetweenSamples)
                                            / ticksInThisSeconds) + offsetInNanoseconds;

                    if (nanoseconds < 0) continue;
                    if (nanoseconds > 999999999) continue;

                    Waveform waveform = new Waveform();
                    DateTimeUtcNano timeStamp = new DateTimeUtcNano(timeStampUtc, nanoseconds);

                    waveform.TimeStamp = timeStamp;
                    waveform.Amplitude = sample.E;

                    if (waveform.TimeStamp > prewaveform.TimeStamp)
                    {
                        waveforms.Add(waveform);
                        prewaveform = waveform;
                    }
                }
            }
        }

        public string SensorId
        {
            get { return _sensorId; }
        }

        public SensorVersion Major
        {
            get { return _major; }
        }

        public SensorVersion Minor
        {
            get { return _minor; }
        }

        public DateTime TimeStampUtc
        {
            get { return _timeStampUtc; }
        }

        public int OffsetInNanoseconds
        {
            get { return _offsetInNanoseconds; }
        }

        public int TicksToFirstSample
        {
            get { return _ticksToFirstSample; }
        }

        public int TicksBetweenSamples
        {
            get { return _ticksBetweenSamples; }
        }

        public int TicksInThisSeconds
        {
            get { return _ticksInThisSeconds; }
        }

        public int SensorAttenuation
        {
            get { return _sensorAttenuation; }
        }

        public List<Sample> HighFrequencySamples
        {
            get { return _highFrequencySamples; }
        }

        public int EpochTime
        {
            get { return _epochTime; }
        }
    }
}
