﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Aws.Core.Utilities;

using LxCommon;
using LxCommon.DataStructures;
using LxCommon.Models;
using LxCommon.Utilities;
using LxProcessor.Common;
using LxProcessor.Utilities;
using LxProcessor.WaveformData;
using Newtonsoft.Json;

namespace LxProcessor.RawData
{
    public class RawDataQueueProcessor : ParallelQueueProcessor<PacketHeaderInfo>
    {
        private int _lastPacketTime;
        private int _lastWaveformConvertedTime;

        private ConcurrentDictionary<string, StationData> _stationData;
        private WaveformProcessor _waveformProcessor;
        private PerfMon _perfMon;
        private MedianSampleFinder _msf;
        private ConcurrentSimplePool<WaveformOfSecondSample> _wossPool;
        private ConcurrentSimplePool<PulseData> _pulseDataPool;

        private ConcurrentQueue<string> _linesToWrite10;
        private ConcurrentQueue<string> _linesToWrite9;
        private int _writerLock10;
        private int _writerLock9;
        private string _tenLog;
        private string _nineFourLog;

        public RawDataQueueProcessor(ConcurrentDictionary<string, StationData> stationData, WaveformProcessor waveformProcessor, PerfMon perfMon,
            int waveformOfSecondSamplePoolSize, int pulseDataPoolSize, ManualResetEventSlim stopEvent, int maxActiveWorkItems)
            : base(stopEvent, maxActiveWorkItems)
        {
            _stationData = stationData;
            _waveformProcessor = waveformProcessor;
            _perfMon = perfMon;

            _wossPool = new ConcurrentSimplePool<WaveformOfSecondSample>(waveformOfSecondSamplePoolSize, 
                () => { return new WaveformOfSecondSample(); }, 
                woss => woss.CleanForReuse(),
                "_wossPool");

            _pulseDataPool = new ConcurrentSimplePool<PulseData>(pulseDataPoolSize, 
                () => { return new PulseData(); }, 
                pd => pd.CleanForReuse(),
                "_pulseDataPool");

            _msf = new MedianSampleFinder(Config.WaveformDelayMedianSampleSize);

            //FileHelper.CreateDirectory(@"E:\Data\RawData");

            //_linesToWrite10 = new ConcurrentQueue<string>();
            //_writerLock10 = 0;
            //_tenLog = @"E:\Data\RawData\Sensor10.txt";
            
            //_linesToWrite9 = new ConcurrentQueue<string>();
            //_writerLock9 = 0;
            //_nineFourLog = @"E:\Data\RawData\Sensor9.4.txt";

        }

        

        protected override bool IsSafeToProcess()
        {
            bool isSafe = true;

            //int maxTotalWaveforms = _waveformProcessor.StationWaveforms.Keys.Count * MaxWaveformsTableSeconds;

            //if (_waveformProcessor.WaveformCount > maxTotalWaveforms)
            //{
            //    isSafe = false;

            //    if (!Config.IsPlayback)
            //    {
            //        PacketHeaderInfo phi;
            //        while (_queue.TryPeek(out phi))
            //        {
            //            if (!LtgTimeUtils.PacketGoodForProcessing(phi.TimeStamp))
            //            {
            //                if (_queue.TryDequeue(out phi))
            //                    EventManager.LogWarning(TaskMonitor.ProcTCPData, string.Format("Waveform table overloaded, discarding data Station: {0}, Time: {1}", phi.SensorId, phi.TimeStamp));
            //            }
            //            else
            //                break;

            //            if (_stopEvent.Wait(0)) break;
            //        }
            //    }
            //}

            return isSafe;
        }

        protected override void DoWork(PacketHeaderInfo packet)
        {
            try
            {
                if (packet != null)
                {
                    if (packet.PacketTimeInSeconds > _lastPacketTime)
                        _lastPacketTime = packet.PacketTimeInSeconds;

                    if (_perfMon != null)
                        _perfMon.UpdateNumberOfItemsCounter("Raw UTC Offset", TimeSvc.GetUnixTime(DateTime.UtcNow) - packet.PacketTimeInSeconds);

                    if (LtgTimeUtils.PacketGoodForProcessing(packet.TimeStamp) || Config.IsPlayback)
                    {
                        PulseData pd = GetPulseData(packet, _pulseDataPool);
                        if (pd != null)
                        {

                            //if (packet.MajorVersion == SensorVersion.ID_VERSION_SENSOR_10_MAJOR && packet.MinorVersion == SensorVersion.ID_VERSION_SENSOR_10_MINOR1)
                            //{
                            //    string pulse = JsonConvert.SerializeObject(pd);
                            //    Write(pulse, _linesToWrite10, ref _writerLock10, _tenLog);
                            //}

                            //if (packet.MajorVersion == SensorVersion.ID_VERSION_SENSOR_9_MAJOR && packet.MinorVersion == SensorVersion.ID_VERSION_SENSOR_9_MINOR4 &&
                            //    (packet.SensorId == "KGLRW" || packet.SensorId == "ENTBB" || packet.SensorId == "BUKOB" || packet.SensorId == "MWNZA" || packet.SensorId == "AWSHQ5"))
                            //{
                            //    string pulse = JsonConvert.SerializeObject(pd);
                            //    Write(pulse, _linesToWrite9, ref _writerLock9, _nineFourLog);
                            //}

                            WaveformOfSecondSample woss = _wossPool.Take();
                            GetStationWaveforms(pd, woss);

                            if (woss.WaveformOfSecondSampleList != null && woss.WaveformOfSecondSampleList.Count > 0)
                            {
                                if (!LtgTimeUtils.PacketTooNewOrTooOld(woss.Time) || Config.IsPlayback)
                                {
                                    _waveformProcessor.AddWaveforms(woss);
                                    _msf.AddSample(woss.Time);
                                    _lastWaveformConvertedTime = _msf.Median;
                                    
                                }
                                else
                                {
                                    EventManager.LogInfo(string.Format("Before adding to waveform processor, bad time for station id= {0} time= {1}", woss.StationId, woss.Time));
                                }
                            }
                            _wossPool.Return(woss);

                            UpdateStationAttenuation(pd.SensorId, pd.SensorAttenuation);
                            _pulseDataPool.Return(pd);
                        }   
                    }
                    else
                    {
                        EventManager.LogInfo(string.Format("Before raw data process, bad time for station id={0} time={1}", packet.SensorId, packet.TimeStamp));
                    }
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError("RawDataQueueProcessor error:", ex);
            }
        }

        //private void Write(string line, ConcurrentQueue<string> linesToWrite, ref int writerLock, string fileName)
        //{
        //    linesToWrite.Enqueue(line);
        //    if (0 == Interlocked.CompareExchange(ref writerLock, 1, 0))
        //    {
        //        Task.Run(() => WriteLines(linesToWrite, fileName));
        //        Interlocked.Decrement(ref writerLock);
        //    }
        //}

        //private void WriteLines(ConcurrentQueue<string> linesToWrite, string fileName)
        //{
        //    try
        //    {
        //        string lineToWrite;
        //        using (StreamWriter textWriter = new StreamWriter(fileName, true))
        //        {
        //            while (linesToWrite.TryDequeue(out lineToWrite))
        //            {
        //                textWriter.WriteLine(lineToWrite);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        AppLogger.LogError("An error occured while writing the file in FlashDataLogger", ex);
        //    }
        //}

        private PulseData GetPulseData(PacketHeaderInfo packet, ConcurrentSimplePool<PulseData> pulseDataPool)
        {
            PulseData pulseData = pulseDataPool.Take();

            bool isValid = pulseData.Initalize(packet);

            if (isValid)
            {
                return pulseData;
            }
            else
            {
                pulseDataPool.Return(pulseData);
                return null;
            }
        }

        private void GetStationWaveforms(PulseData pd, WaveformOfSecondSample woss)
        {
            pd.ConvertToWaveforms(woss.WaveformOfSecondSampleList);    
            woss.StationId = pd.SensorId;
            woss.Time = pd.EpochTime;
            woss.MajorVersion = pd.Major;
            woss.MinorVersion = pd.Minor;
        }

        private void UpdateStationAttenuation(string stationId, int dB)
        {
            StationData station;
            if (_stationData.TryGetValue(stationId, out station))
            {
                station.CalibrationData.UpdateActiveCalirationNum(dB);
            }
        }
        
        public DateTime LastPacketTime
        {
            get { return TimeSvc.GetTime(_lastPacketTime); }
        }

        public int LastWaveformConvertedTime
        {
            get { return _lastWaveformConvertedTime; }
        }
    }
}
