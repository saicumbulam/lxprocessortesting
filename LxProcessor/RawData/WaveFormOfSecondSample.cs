using System.Collections.Generic;

using LxCommon;
using LxCommon.DataStructures;
using LxCommon.Models;
using LxCommon.Utilities;


namespace LxProcessor.RawData
{
    public class WaveformOfSecondSample
    {
        
        public SensorVersion MajorVersion { get; set; }

        public SensorVersion MinorVersion { get; set; }

        public FluxList<Waveform> WaveformOfSecondSampleList { get; set; }

        public string StationId { get; set; }

        public int Time { get; set; }

        public WaveformOfSecondSample()
        {
            MajorVersion = SensorVersion.ID_VERSION_UNKNOW;
            MinorVersion = SensorVersion.ID_VERSION_UNKNOW;
            Time = 0;
            WaveformOfSecondSampleList = new FluxList<Waveform>();
        }

        public void CleanForReuse()
        {
            MajorVersion = SensorVersion.ID_VERSION_UNKNOW;
            MinorVersion = SensorVersion.ID_VERSION_UNKNOW;
            Time = 0;

            if (WaveformOfSecondSampleList != null)
            {
                WaveformOfSecondSampleList.Clear();
            }
        }
    }
}