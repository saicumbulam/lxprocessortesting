using System;
using System.Configuration;
using System.IO;
using System.Threading;
using Aws.Core.Utilities;
using LxCommon;
using LxProcessor.Common;
using LxProcessor.Locator;
using LxProcessor.MainController;
using LxProcessor.PostLocator;
using LxProcessor.RawData;
using LxProcessor.WaveformData;
/*This module is about the playback of the code. From this if all the files to be read are valid , 
 * the data is ent to the datacontoller processor 
 * where the data is processed and then written to the export file 
 * */
namespace LxProcessor.Playback
{
    public class PlaybackFiles
    {
        private readonly ManualResetEventSlim _stopEvent;
        private readonly DataController _dataController;
        private RawDataQueueProcessor _rawDataQueueProcessor;
        private PostLocatorProcessor _postLocatorProcessor;
        private WaveformProcessor _waveformProcessor;
        private OffsetProcessor _offsetProcessor;
        private ProcessController _processController;
        private readonly DateTime _playbackStartTime;
        private readonly DateTime _playbackEndTime;

        public PlaybackFiles(DataController dataController, RawDataQueueProcessor rawDataQueueProcessor, WaveformProcessor waveformProcessor, OffsetProcessor offsetProcessor, PostLocatorProcessor postLocatorProcessor,
            ManualResetEventSlim stopEvent, ProcessController processController)
        {
            _dataController = dataController;
            _rawDataQueueProcessor = rawDataQueueProcessor;
            _postLocatorProcessor = postLocatorProcessor;
            _waveformProcessor = waveformProcessor;
            _offsetProcessor = offsetProcessor;
            _processController = processController;
            _stopEvent = stopEvent;
            _playbackStartTime = Config.PlaybackStartTime;
            _playbackEndTime = Config.PlaybackEndTime;
        }

        public void Start(Object stateInfo)
        {
            Console.WriteLine("Waiting 4 seconds to begin playback");
            Thread.Sleep(TimeSpan.FromSeconds(2));

            FileInfo[] files = FindFiles(Config.PlaybackDirectoryName);

            if (files != null)
            {
                foreach (FileInfo file in files)
                {
                    ReadFile(file.FullName);
                    if (_stopEvent.Wait(0))
                        break;
                }
            }
            else
                Console.WriteLine("No playback files found");

            Console.WriteLine("End of file playback");



            //_processController.Stop();

        }

        private FileInfo[] FindFiles(string dirName)
        {
            FileInfo[] files = null;
            DirectoryInfo di = new DirectoryInfo(dirName);
            if (di.Exists)
                files = di.GetFiles("*.LTG", SearchOption.AllDirectories);

            return files;
        }

        private const int MinimumFileLengthBytes = 6;
        private void ReadFile(string fileName)
        {
            Console.WriteLine("Starting playback of {0}", fileName);
            try
            {
                // File reading using filestream 
                using (FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    //checking the length of the file is greater than the minimum length of the file (that is the size of the file)
                    if (fs.Length >= MinimumFileLengthBytes) 
                    {
                        byte[] bytes = new byte[4]; // initialize the empty 4 bytes for reading from the file and transferring to the variable
                        fs.Read(bytes, 0, 4); // read the first 4 bytes of the file
                        
                        /* Total number of packets preesnt oin the file is found from the 4 bytes of the file */
                        int numOfPackets = BitConverter.ToInt32(bytes, 0); // convert the bytes to integer for the file
                        int i = 0;
                        //sai-mod
                        //numOfPackets = 10000;
                        for (i = 0; i < numOfPackets; i++) // for loop until the num of packets is finished
                        {
                            fs.Read(bytes, 0, 2); // from the first 2 bytes the size of a single packet is determined
                            int packetSize = BitConverter.ToInt16(bytes, 0); //bytes pulled are converted to the integer
                            byte[] rawData = new byte[packetSize];

                            int bytesRead = 0;

                            try
                            {
                                bytesRead = fs.Read(rawData, 0, packetSize); //all the packet is read into the rawdata variable. 
                                //Bytes varibale is used to check the size of the packet 
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine("Unable to read {0} bytes from file: {1} Exception: {2}", packetSize, fileName, ex.Message);
                            }
                            // check to see if the bytesread is equal to the packetsize
                            if (bytesRead == packetSize)
                            {
                                byte[] copyOfBytes = new byte[bytesRead];
                                rawData.CopyTo(copyOfBytes, 0); // copy the rawdata to the copybytes and sent to the readpacket function 
                                ReadPacket(copyOfBytes);
                            }
                            if (_stopEvent.Wait(0))
                                break;
                        }

                        // show progress
                        Console.WriteLine("Read {0} of {1} packets", i, numOfPackets);
                    }
                    else
                        Console.WriteLine("File {0} is less than {1} bytes", fileName, MinimumFileLengthBytes);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Unable to read file: {0} Exception: {1}", fileName, ex.Message);
            }
        }

        private const int MaxRawDataQueueSize = 500;
        private const int MaxOffsetQueueSize = 500;
        private const int MaxPostLocatorQueueSize = 500;
        private const int MaxProcessingLagSeconds = 15;
        private void ReadPacket(object o)
        {
            byte[] packet = o as byte[];
            if (packet != null)
            {
                PacketHeaderInfo header = new PacketHeaderInfo(); //create a variable with class packetheader
                if (RawPacketUtil.GetPacketHeaderInfo(packet, header)) //check to see if the header from the file is equla to packetheader 
                {
                    //checking the timestamp of the header whether the playback is read whithin the playback time associated. 
                    if (header != null && header.TimeStamp >= _playbackStartTime && header.TimeStamp <= _playbackEndTime)
                    {
                        //general check about the packages
                        while (_rawDataQueueProcessor.Count > MaxRawDataQueueSize || _offsetProcessor.Count > MaxOffsetQueueSize || _postLocatorProcessor.PortionCount > MaxPostLocatorQueueSize ||
                            ((_rawDataQueueProcessor.Count > 0 || _postLocatorProcessor.PortionCount > 0) && IsWaveformProcessorBehind(_rawDataQueueProcessor, _waveformProcessor)))
                        {
                            if (_stopEvent.Wait(TimeSpan.FromMilliseconds(100)))
                                break;
                        }
                        //send to the next module datacontroller for the processing to take place.
                        _dataController.ProcessIncomingPacket(header); 
                    }
                }
            }
        }

        private bool IsWaveformProcessorBehind(RawDataQueueProcessor rawDataQueueProcessor, WaveformProcessor waveformProcessor)
        {
            bool isBehind = false;

            if (rawDataQueueProcessor.LastWaveformConvertedTime != 0 && _waveformProcessor.LastWaveformProcessed != DateTime.MinValue)
            {
                double delta = (TimeSvc.GetTime(rawDataQueueProcessor.LastWaveformConvertedTime) - waveformProcessor.LastWaveformProcessed).TotalSeconds;
                isBehind = delta > MaxProcessingLagSeconds;
            }

            return isBehind;

        }
    }
}

