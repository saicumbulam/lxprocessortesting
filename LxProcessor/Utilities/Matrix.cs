using System;
using Aws.Core.Utilities;

namespace LxProcessor.Utilities
{
    public class Matrix
    {
        private readonly double[,] _data;
        
        public Matrix(int size)
        {
            _data = new double[size, size];
        }

        public Matrix(int rows, int cols)
        {
            _data = new double[rows, cols];
        }

        public Matrix(double[,] data)
        {
            _data = data;
        }

        protected bool Equals(Matrix other)
        {
            return Equals(_data, other._data);
        }

        public override int GetHashCode()
        {
            return (_data != null ? _data.GetHashCode() : 0);
        }

        public static Matrix operator +(Matrix m1, Matrix m2)
        {
            int r1 = m1._data.GetLength(0);
            int r2 = m2._data.GetLength(0);
            int c1 = m1._data.GetLength(1);
            int c2 = m2._data.GetLength(1);
            if ((r1 != r2) || (c1 != c2))
            {
                throw new Exception("Matrix dimensions do not agree");
            }
            double[,] res = new double[r1, c1];
            for (int i = 0; i < r1; i++)
            {
                for (int j = 0; j < c1; j++)
                {
                    res[i, j] = m1._data[i, j] + m2._data[i, j];
                }
            }
            return new Matrix(res);
        }

        public static Matrix operator -(Matrix m1, Matrix m2)
        {
            int r1 = m1._data.GetLength(0); 
            int r2 = m2._data.GetLength(0);
            int c1 = m1._data.GetLength(1); 
            int c2 = m2._data.GetLength(1);
            
            if ((r1 != r2) || (c1 != c2))
            {
                throw new Exception("Matrix dimensions do not agree");
            }
            
            double[,] res = new double[r1, c1];
            
            for (int i = 0; i < r1; i++)
            {
                for (int j = 0; j < c1; j++)
                {
                    res[i, j] = m1._data[i, j] - m2._data[i, j];
                }
            }
            return new Matrix(res);
        }

        public static Matrix operator *(Matrix m1, Matrix m2)
        {
            int r1 = m1._data.GetLength(0); 
            int r2 = m2._data.GetLength(0);
            int c1 = m1._data.GetLength(1); 
            int c2 = m2._data.GetLength(1);
            if (c1 != r2)
            {
                throw new Exception("Matrix dimensions do not agree");
            }
            double[,] res = new double[r1, c2];
            for (int i = 0; i < r1; i++)
            {
                for (int j = 0; j < c2; j++)
                {
                    for (int k = 0; k < r2; k++)
                    {
                        res[i, j] = res[i, j] + (m1._data[i, k] * m2._data[k, j]);
                    }
                }
            }
            return new Matrix(res);
        }

        public static Matrix operator /(double i, Matrix m)
        {
            return new Matrix(ScalarMultiply(i, Invert(m._data)));
        }

        public static bool operator ==(Matrix m1, Matrix m2)
        {
            bool b = true;
            
            int r1 = m1._data.GetLength(0); 
            int r2 = m2._data.GetLength(0);
            int c1 = m1._data.GetLength(1); 
            int c2 = m2._data.GetLength(1);
            
            if ((r1 != r2) || (c1 != c2))
            {
                b = false;
            }
            else
            {
                for (int i = 0; i < r1; i++)
                {
                    for (int j = 0; j < c1; j++)
                    {
                        if (m1._data[i, j] != m2._data[i, j])
                            b = false;
                    }
                }
            }
            return b;
        }

        public static bool operator !=(Matrix m1, Matrix m2)
        {
            return !(m1 == m2);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Matrix))
            {
                return false;
            }
            return this == (Matrix)obj;
        }

        public double this[int i, int j]
        {
            get
            {
                return _data[i, j];
            }
            set
            {
                _data[i, j] = value;
            }
        }

        public void Display()
        {
            Display("N2");
        }

        public void Display(string format)
        {
            int r1 = _data.GetLength(0); 
            int c1 = _data.GetLength(1);
            
            for (int i = 0; i < r1; i++)
            {
                for (int j = 0; j < c1; j++)
                {
                    Console.Write("{0}\t", _data[i, j].ToString(format));
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }

        public Matrix Inverse()
        {
            if ((IsSquare) && (!IsSingular))
            {
                return new Matrix(Invert(_data));
            }
            
            throw new Exception(@"Cannot find inverse for non square/singular matrix");
        }

        public Matrix Transpose()
        {
            double[,] d = Transpose(_data);
            return new Matrix(d);
        }

        public static Matrix Zeros(int size)
        {
            double[,] d = new double[size, size];
            return new Matrix(d);
        }
        
        public static Matrix Zeros(int rows, int cols)
        {
            double[,] d = new double[rows, cols];
            return new Matrix(d);
        }
        
        public static Matrix LinSolve(Matrix cof, Matrix con)
        {
            return cof.Inverse() * con;
        }

        public double Determinant()
        {
            if (IsSquare)
            {
                return Determinant(_data);
            }
            
            throw new Exception("Cannot determine the DET for a non square matrix");
        }

        public bool IsSquare
        {
            get
            {
                return (_data.GetLength(0) == _data.GetLength(1));
            }
        }

        public bool IsSingular
        {
            get
            {
                return ((int)Determinant() == 0);
            }
        }

        public int Rows { get { return _data.GetLength(0); } }
        
        public int Cols { get { return _data.GetLength(1); } }


        private static double[,] Invert(double[,] a)
        {
            int ro = a.GetLength(0);
            int co = a.GetLength(1);
            try
            {
                if (ro != co) { throw new Exception(); }

            }
            catch { Console.WriteLine("Cannot find inverse for a non square matrix"); }

            int q; double[,] b = new double[ro, co]; double[,] I = Eyes(ro);
            for (int p = 0; p < ro; p++) { for (q = 0; q < co; q++) { b[p, q] = a[p, q]; } }
            int i;
            if (a[0, 0] == 0)
            {
                i = 1;
                while (i < ro)
                {
                    if (a[i, 0] != 0)
                    {
                        InterchangeRow(a, 0, i);
                        InterchangeRow(I, 0, i);
                        break;
                    }
                    i++;
                }
            }
            RowDivide(I, 0, a[0, 0]);
            RowDivide(a, 0, a[0, 0]);
            for (int p = 1; p < ro; p++)
            {
                q = 0;
                while (q < p)
                {
                    RowSubtract(I, p, q, a[p, q]);
                    RowSubtract(a, p, q, a[p, q]);
                    q++;
                }
                if (a[p, p] != 0)
                {
                    RowDivide(I, p, a[p, p]);
                    RowDivide(a, p, a[p, p]);

                }
                if (a[p, p] == 0)
                {
                    for (int j = p + 1; j < co; j++)
                    {
                        if (a[p, j] != 0)			// Column pivotting not supported
                        {
                            for (int p1 = 0; p1 < ro; p1++)
                            {
                                for (q = 0; q < co; q++)
                                {
                                    a[p1, q] = b[p1, q];
                                }
                            }
                            return Inverse(b);

                        }
                    }

                }
            }
            for (int p = ro - 1; p > 0; p--)
            {
                for (q = p - 1; q >= 0; q--)
                {
                    RowSubtract(I, q, p, a[q, p]);
                    RowSubtract(a, q, p, a[q, p]);
                }
            }
            for (int p = 0; p < ro; p++)
            {
                for (q = 0; q < co; q++)
                {
                    a[p, q] = b[p, q];
                }
            }

            return (I);
        }

        private static double[,] Inverse(double[,] a)
        {
            int ro = a.GetLength(0);
            int co = a.GetLength(1);
            double[,] ai = new double[ro, co];
            for (int p = 0; p < ro; p++)
            {
                for (int q = 0; q < co; q++)
                {
                    ai[p, q] = 0;
                }
            }
            try
            {
                if (ro != co)
                {
                    throw new Exception();

                }
            }
            catch
            {
                Console.WriteLine("Cannot find inverse for a non square matrix");
            }

            double de = Determinant(a);

            try
            {
                if (de == 0)
                {
                    new Exception("Cannot Perform Inversion. Matrix Singular");

                }
            }
            catch (Exception e1)
            {
                Console.WriteLine("Error:" + e1.Message);
            }


            for (int p = 0; p < ro; p++)
            {
                for (int q = 0; q < co; q++)
                {
                    double[,] s = SubMatrixExtraction(a, p, q);
                    double ds = Determinant(s);
                    ai[p, q] = Math.Pow(-1, p + q + 2) * ds / de;

                }
            }

            ai = Transpose(ai);
            
            return (ai);
        }

        private static void RowDivide(double[,] a, int r, double s)
        {
            int co = a.GetLength(1);
            for (int q = 0; q < co; q++)
            {
                a[r, q] = a[r, q] / s;
            }
        }

        private static void RowSubtract(double[,] a, int i, int j, double s)
        {
            int co = a.GetLength(1);
            for (int q = 0; q < co; q++)
            {
                a[i, q] = a[i, q] - (s * a[j, q]);
            }
        }

        private static void InterchangeRow(double[,] a, int i, int j)
        {
            int co = a.GetLength(1);
            double temp = 0;
            for (int q = 0; q < co; q++)
            {
                temp = a[i, q];
                a[i, q] = a[j, q];
                a[j, q] = temp;
            }
        }

        private static double[,] Eyes(int n)
        {
            double[,] a = new double[n, n];
            for (int p = 0; p < n; p++)
            {
                for (int q = 0; q < n; q++)
                {
                    if (p == q)
                    {
                        a[p, q] = 1;
                    }
                    else
                    {
                        a[p, q] = 0;
                    }

                }
            }
            return (a);
        }

        private static double[,] ScalarMultiply(double scalar, double[,] a)
        {
            int ro = a.GetLength(0);
            int co = a.GetLength(1);
            double[,] b = new double[ro, co];
            for (int p = 0; p < ro; p++)
            {
                for (int q = 0; q < co; q++)
                {
                    b[p, q] = scalar * a[p, q];
                }
            }
            return (b);
        }

        private static double Determinant(double[,] a)//new det
        {
            int q = 0;
            int ro = a.GetLength(0);
            int co = a.GetLength(1);
            double[,] b = new double[ro, co];
            for (int p = 0; p < ro; p++)
            {
                for (q = 0; q < co; q++)
                {
                    b[p, q] = a[p, q];
                }
            }
            int i = 0;
            double det = 1;
            try
            {
                if (ro != co)
                {
                    Exception ex = new Exception("Error: Matrix Not Square");
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            try
            {
                if (ro == 0)
                {
                    Exception ex = new Exception("Dimesion of the Matrix 0X0");
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            if (ro == 2)
            {
                return ((a[0, 0] * a[1, 1]) - (a[0, 1] * a[1, 0]));
            }

            if (a[0, 0] == 0)
            {
                i = 1;
                while (i < ro)
                {
                    if (a[i, 0] != 0)
                    {
                        InterchangeRow(a, 0, i);		//Interchange of rows changes. determinent = determinent * -1
                        det *= -1;
                        break;
                    }
                    i++;
                }

            }
            if (a[0, 0] == 0)
            {
                return (0);							//If all the elements in a row or column of matrix are 0, determient is equal to 0
            }
            det *= a[0, 0];
            RowDivide(a, 0, a[0, 0]);
            for (int p = 1; p < ro; p++)
            {
                q = 0;
                while (q < p)							//preparring an upper triangular matrix
                {
                    RowSubtract(a, p, q, a[p, q]);
                    q++;
                }
                if (a[p, p] != 0)
                {
                    det *= a[p, p];					//Dividing the entire row with non zero diagonal element. Multiplying det with that factor.	
                    RowDivide(a, p, a[p, p]);
                }
                if (a[p, p] == 0)						// Chcek if the diagonal elements are zeros
                {
                    for (int j = p + 1; j < co; j++)
                    {
                        if (a[p, j] != 0)
                        {
                            ColumnSubtract(a, p, j, -1);//Adding of columns donot change the determinent

                            det *= a[p, p];
                            RowDivide(a, p, a[p, p]);
                            break;
                        }
                    }

                }
                if (a[p, p] == 0)						//if diagonal element is still zero, Determinent is zero.
                {
                    return (0);
                }


            }

            for (int p = 0; p < ro; p++)
            {
                for (q = 0; q < co; q++)
                {
                    a[p, q] = b[p, q];
                }
            }
            return (det);
        }

        private static double[,] SubMatrixExtraction(double[,] a, int ro, int co)
        {
            int n = a.GetLength(0);
            double[,] c = new double[n - 1, n - 1];
            int i = 0;
            for (int p = 0; p < n; p++)
            {
                int j = 0;
                if (p != ro)
                {
                    for (int q = 0; q < n; q++)
                    {
                        if (q != co)
                        {
                            c[i, j] = a[p, q];
                            j += 1;
                        }
                    }
                    i += 1;
                }
            }

            return (c);
        }

        private static double[,] Transpose(double[,] a)
        {

            int ro = a.GetLength(0);
            int co = a.GetLength(1);
            double[,] c = new double[co, ro];
            for (int p = 0; p < ro; p++)
            {
                for (int q = 0; q < co; q++)
                {
                    c[q, p] = a[p, q];
                }

            }

            return (c);

        }

        private static void ColumnSubtract(double[,] a, int i, int j, double s)
        {
            int ro = a.GetLength(0);
            int co = a.GetLength(1);
            for (int p = 0; p < ro; p++)
            {
                a[p, i] = a[p, i] - (s * a[p, j]);
            }

        }

        public static double[] SolveLinearEquations(double[,] matrix, double[] rhs)
        {
            int nDim = rhs.Length;
            double[] solution = null;
            try
            {
                Matrix cof = new Matrix(matrix);
                if (cof.IsSquare && !cof.IsSingular)
                {
                    Matrix con = new Matrix(nDim, 1);
                    int i = 0;
                    for (i = 0; i < nDim; i++)
                    {
                        con[i, 0] = rhs[i];
                    }

                    Matrix sol = cof.Inverse() * con;
                    solution = new double[nDim];
                    for (i = 0; i < nDim; i++)
                    {
                        solution[i] = sol[i, 0];
                    }
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventId.Utilities.Matrix.LinearEquation, "Error in solving linear equations", ex);
            }
            return solution;
        }
    }
}
