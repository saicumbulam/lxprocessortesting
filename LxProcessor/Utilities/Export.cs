using System;
using System.Collections.Generic;
using System.IO;
using Aws.Core.Utilities;
using LxCommon;
using LxProcessor.Common;

namespace LxProcessor.Utilities
{
    public static class Export
    {
        public static void ExportFlashPortions(List<LTGFlashPortion> flashList, string optionString)
        {
            if (flashList != null && flashList.Count > 0)
            {
                FlashData firstFlash = flashList[0] as FlashData;
                DateTime startTime = firstFlash.TimeStamp.BaseTime;
                TextWriter textWriter = GetTextWriter(startTime);
                if (textWriter != null)
                {
                    using (textWriter)
                    {
                        if (optionString != null)
                            textWriter.WriteLine(optionString);

                        textWriter.WriteLine("Flash Portions:");
                        foreach (FlashData flash in flashList)
                        {
                            try
                            {
                                string text = string.Format("{0}  Location:({1:f5},{2:f5},{3:f3}), Confidence:{4}, Amplitude:{5}, Type:{6} ",
                                        flash.FlashTime, flash.Latitude, flash.Longitude, flash.Height, flash.Confidence, flash.Amplitude, flash.FlashType);
                                textWriter.WriteLine(text);
                                textWriter.WriteLine("  " + flash.Description);
                                textWriter.WriteLine(string.Empty);
                            }
                            catch (Exception ex)
                            {
                                EventManager.LogWarning(EventId.Utilities.Export.ExportingFlashPortions,
                                    "Unable to export Detection Flash Portion in Ligtning Core Service.", ex);
                            }
                        }
                    }
                }
            }            
        }

        private static TextWriter GetTextWriter(DateTime utcTime)
        {
            string path = GetDataHourPath(utcTime);
            string fileName = Path.Combine(path, String.Format("{0:D4}-{1:D2}-{2:D2}T{3:D2}-{4:D2}.ltg", utcTime.Year, utcTime.Month, utcTime.Day, utcTime.Hour, utcTime.Minute));
            
            TextWriter textWriter = null;
            try
            {
                textWriter = new StreamWriter(fileName, true);
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventId.Utilities.Export.CreatingTextWriter, "Failure creating text writer", ex);
            }

            return textWriter;
        }

        public static string GetDataHourPath(DateTime dataTime)
        {
            return GetDataHourPath(dataTime, null);
        }

        public static string GetDataHourPath(DateTime dataTime, string pathPrefix)
        {
            string day = string.Format("{0:D4}-{1:D2}-{2:D2}", dataTime.Year, dataTime.Month, dataTime.Day);
            string hour = string.Format("{0:D2}", dataTime.Hour);
            string path = Config.LtgFileDirArchive;

            if (!string.IsNullOrWhiteSpace(pathPrefix))
                path = Path.Combine(path, pathPrefix);

            try
            {
                path = Path.Combine(path, day, hour);
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

            }
            catch (Exception ex)
            {
                EventManager.LogWarning(EventId.Utilities.Export.DataHourPath, "Failed to get the path for hourly data", ex);
            }

            return path;
        }

        public static void RawData(PacketHeaderInfo data)
        {
            DateTime dt = LtgTimeUtils.GetUtcDateTimeFrom1970Seconds(data.PacketTimeInSeconds);
            string path = GetDataHourPath(dt, "Raw");
            int min = dt.Minute / 10;

            string fileName = Path.Combine(path, string.Format("{0:D4}-{1:D2}-{2:D2}T{3:D2}-{4:D2}.ltg", dt.Year, dt.Month, dt.Day, dt.Hour, min));

            if (!string.IsNullOrWhiteSpace(fileName))
            {
                try
                {
                    using (FileStream fs = new FileStream(fileName, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite))
                    {
                        byte[] b4b = new byte[4];
                        byte[] b2b = new byte[2];
                        int num = 0;

                        if (fs.Read(b4b, 0, 4) == 4)
                            num = BitConverter.ToInt32(b4b, 0);
                        
                        fs.Seek(0, SeekOrigin.End);
                        short size = (short)data.RawData.Length;
                        b2b = BitConverter.GetBytes(size);
                        fs.Write(b2b, 0, 2);
                        fs.Write(data.RawData, 0, data.RawData.Length);
                        num++;
                        b4b = BitConverter.GetBytes(num);
                        fs.Seek(0, SeekOrigin.Begin);
                        fs.Write(b4b, 0, 4);
                    }
                }
                catch(Exception ex)
                {
                    EventManager.LogWarning(EventId.Utilities.Export.RawData, "Failed to export raw data", ex);
                }
            }
        }

    }
}
