﻿using System.Collections.Generic;
using System.Diagnostics;
using LxProcessor.Common;

namespace LxProcessor.Utilities
{
    public class PerfMon
    {
        private string _instanceName;
        Dictionary<string, PerformanceCounter> _counters;

        private const string CategoryName = "EN Lightning Processor";
        public PerfMon()
        {
            _instanceName = string.Format("Processor #{0}", (Config.ProcessorNum + 1));
        }

        public void Init()
        {
            CreateCategory();
            CreateCounters();
        }

        private void CreateCounters()
        {
            _counters = new Dictionary<string, PerformanceCounter>
                {
                    {"Raw Data Queue", new PerformanceCounter(CategoryName, "Raw Data Queue", _instanceName, false)},
                    {"Stations with waveforms", new PerformanceCounter(CategoryName, "Stations with waveforms", _instanceName, false)},
                    {"Total waveforms", new PerformanceCounter(CategoryName, "Total waveforms", _instanceName, false)},
                    {"Waveforms", new PerformanceCounter(CategoryName, "Waveforms", _instanceName, false)},
                    {"Post Locator Queue", new PerformanceCounter(CategoryName, "Post Locator Queue", _instanceName, false)},
                    {"Grouper Queue", new PerformanceCounter(CategoryName, "Grouper Queue", _instanceName, false)},
                    {"Flash UTC Offset", new PerformanceCounter(CategoryName, "Flash UTC Offset", _instanceName, false)},
                    {"Slice UTC Offset", new PerformanceCounter(CategoryName, "Slice UTC Offset", _instanceName, false)},
                    {"Raw UTC Offset", new PerformanceCounter(CategoryName, "Raw UTC Offset", _instanceName, false)},
                    {"Incoming TCP Rate", new PerformanceCounter(CategoryName, "Incoming TCP Rate", _instanceName, false)}
                };

            foreach (string key in _counters.Keys)
            {
                UpdateNumberOfItemsCounter(key, 0);
            }
        }

        private void CreateCategory()
        {
            if (!PerformanceCounterCategory.Exists(CategoryName))
            {
                CounterCreationDataCollection ccds = new CounterCreationDataCollection
                {
                    new CounterCreationData("Raw Data Queue", "Number of raw packets waiting to be added to waveform table", PerformanceCounterType.NumberOfItems32),
                    new CounterCreationData("Stations with waveforms", "Number of stations with active waveforms", PerformanceCounterType.NumberOfItems32),
                    new CounterCreationData("Total waveforms", "Number of total waveforms in the waveform table", PerformanceCounterType.NumberOfItems32),
                    new CounterCreationData("Waveforms", "Number of total waveforms in the waveform table", PerformanceCounterType.NumberOfItems32),
                    new CounterCreationData("Post Locator Queue", "Number of portions waiting to be post processed", PerformanceCounterType.NumberOfItems32),
                    new CounterCreationData("Grouper Queue", "Number of flashes waiting to be grouped", PerformanceCounterType.NumberOfItems32),
                    new CounterCreationData("Flash UTC Offset", string.Empty, PerformanceCounterType.NumberOfItems32),
                    new CounterCreationData("Slice UTC Offset", string.Empty, PerformanceCounterType.NumberOfItems32),
                    new CounterCreationData("Raw UTC Offset", string.Empty, PerformanceCounterType.NumberOfItems32),
                    new CounterCreationData("Incoming TCP Rate", string.Empty, PerformanceCounterType.NumberOfItems32)

                };

                PerformanceCounterCategory.Create(CategoryName, "Lightning Processor counters", PerformanceCounterCategoryType.MultiInstance, ccds);
            }
        }

        public void UpdateNumberOfItemsCounter(string counterName, int value)
        {
            if (_counters != null && _counters.Count > 0)
            {
                PerformanceCounter pc;
                if (_counters.TryGetValue(counterName, out pc))
                {
                    pc.RawValue = value;
                }
            }
        }

    }
}
