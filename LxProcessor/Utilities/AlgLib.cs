﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LxProcessor.Utilities
{
    public class alglib
    {

        /// <summary>
        ///  ALGLIB object, parent  class  for  all  internal  AlgoPascal  objects managed by ALGLIB. Any internal AlgoPascal object inherits from this class. 
        /// User-visible objects inherit from alglibobject (see below).
        /// </summary>
        public abstract class apobject
        {
            public abstract void init();
            public abstract apobject make_copy();
        }

        /// <summary>
        /// ALGLIB object, parent class for all user-visible objects  managed  by ALGLIB.
        /// </summary>
        public abstract class alglibobject
        {
            /// <summary>
            /// in managed ALGLIB it does nothing
            /// </summary>
            public virtual void _deallocate() { }
        }

      

        /*************************************************************************
   Optimization report, filled by MinLMResults() function

   FIELDS:
   * TerminationType, completetion code:
       * -7    derivative correctness check failed;
               see Rep.WrongNum, Rep.WrongI, Rep.WrongJ for
               more information.
       *  1    relative function improvement is no more than
               EpsF.
       *  2    relative step is no more than EpsX.
       *  4    gradient is no more than EpsG.
       *  5    MaxIts steps was taken
       *  7    stopping conditions are too stringent,
               further improvement is impossible
   * IterationsCount, contains iterations count
   * NFunc, number of function calculations
   * NJac, number of Jacobi matrix calculations
   * NGrad, number of gradient calculations
   * NHess, number of Hessian calculations
   * NCholesky, number of Cholesky decomposition calculations
   *************************************************************************/
        public class minlmreport : alglibobject
        {
            //
            // Public declarations
            //
            public int iterationscount { get { return _innerobj.iterationscount; } set { _innerobj.iterationscount = value; } }
            public int terminationtype { get { return _innerobj.terminationtype; } set { _innerobj.terminationtype = value; } }
            public int funcidx { get { return _innerobj.funcidx; } set { _innerobj.funcidx = value; } }
            public int varidx { get { return _innerobj.varidx; } set { _innerobj.varidx = value; } }
            public int nfunc { get { return _innerobj.nfunc; } set { _innerobj.nfunc = value; } }
            public int njac { get { return _innerobj.njac; } set { _innerobj.njac = value; } }
            public int ngrad { get { return _innerobj.ngrad; } set { _innerobj.ngrad = value; } }
            public int nhess { get { return _innerobj.nhess; } set { _innerobj.nhess = value; } }
            public int ncholesky { get { return _innerobj.ncholesky; } set { _innerobj.ncholesky = value; } }

            public minlmreport()
            {
                _innerobj = new minlm.minlmreport();
            }

            //
            // Although some of declarations below are public, you should not use them
            // They are intended for internal use only
            //
            private minlm.minlmreport _innerobj;
            public minlm.minlmreport innerobj { get { return _innerobj; } }
            public minlmreport(minlm.minlmreport obj)
            {
                _innerobj = obj;
            }
        }


        public delegate void ndimensional_fvec(double[] arg, double[] fi, object obj);
        public delegate void ndimensional_jac(double[] arg, double[] fi, double[,] jac, object obj);
        public delegate void ndimensional_rep(double[] arg, double func, object obj);

        public static void minlmcreatevj(int m, double[] x, out minlmstate state)
        {
            int n;

            state = new minlmstate();
            n = ap.len(x);
            minlm.minlmcreatevj(n, m, x, state.innerobj);

            return;
        }

        public static void minlmsetcond(minlmstate state, double epsg, double epsf, double epsx, int maxits)
        {

            minlm.minlmsetcond(state.innerobj, epsg, epsf, epsx, maxits);
            return;
        }

        public static void minlmoptimize(minlmstate state, ndimensional_fvec fvec, ndimensional_jac jac, ndimensional_rep rep, object obj)
        {
            if (fvec == null)
                throw new alglibexception("ALGLIB: error in 'minlmoptimize()' (fvec is null)");
            if (jac == null)
                throw new alglibexception("ALGLIB: error in 'minlmoptimize()' (jac is null)");
            while (alglib.minlmiteration(state))
            {
                if (state.needfi)
                {
                    fvec(state.x, state.innerobj.fi, obj);
                    continue;
                }
                if (state.needfij)
                {
                    jac(state.x, state.innerobj.fi, state.innerobj.j, obj);
                    continue;
                }
                if (state.innerobj.xupdated)
                {
                    if (rep != null)
                        rep(state.innerobj.x, state.innerobj.f, obj);
                    continue;
                }
                throw new alglibexception("ALGLIB: error in 'minlmoptimize' (some derivatives were not provided?)");
            }
        }


        /*************************************************************************
        This function provides reverse communication interface
        Reverse communication interface is not documented or recommended to use.
        See below for functions which provide better documented API
        *************************************************************************/
        public static bool minlmiteration(minlmstate state)
        {

            bool result = minlm.minlmiteration(state.innerobj);
            return result;
        }

        public static void minlmresults(minlmstate state, out double[] x, out minlmreport rep)
        {
            x = new double[0];
            rep = new minlmreport();
            minlm.minlmresults(state.innerobj, ref x, rep.innerobj);
            return;
        }


        /// <summary>
        /// Levenberg-Marquardt optimizer. 
        /// This structure should be created using one of the MinLMCreate???() functions. 
        /// You should not access its fields directly; use ALGLIB functions to work with it.
        /// </summary>
        public class minlmstate : alglibobject
        {
            public bool needf { get { return _innerobj.needf; } set { _innerobj.needf = value; } }
            public bool needfg { get { return _innerobj.needfg; } set { _innerobj.needfg = value; } }
            public bool needfgh { get { return _innerobj.needfgh; } set { _innerobj.needfgh = value; } }
            public bool needfi { get { return _innerobj.needfi; } set { _innerobj.needfi = value; } }
            public bool needfij { get { return _innerobj.needfij; } set { _innerobj.needfij = value; } }
            public bool xupdated { get { return _innerobj.xupdated; } set { _innerobj.xupdated = value; } }
            public double f { get { return _innerobj.f; } set { _innerobj.f = value; } }
            public double[] fi { get { return _innerobj.fi; } }
            public double[] g { get { return _innerobj.g; } }
            public double[,] h { get { return _innerobj.h; } }
            public double[,] j { get { return _innerobj.j; } }
            public double[] x { get { return _innerobj.x; } }

            public minlmstate()
            {
                _innerobj = new minlm.minlmstate();
            }

            //
            // Although some of declarations below are public, you should not use them
            // They are intended for internal use only
            //
            private minlm.minlmstate _innerobj;
            public minlm.minlmstate innerobj { get { return _innerobj; } }
            public minlmstate(minlm.minlmstate obj)
            {
                _innerobj = obj;
            }
        }


        public class minlm
        {
            /*************************************************************************
            Levenberg-Marquardt optimizer.

            This structure should be created using one of the MinLMCreate???()
            functions. You should not access its fields directly; use ALGLIB functions
            to work with it.
            *************************************************************************/
            public class minlmstate : apobject
            {
                public int n;
                public int m;
                public double diffstep;
                public double epsg;
                public double epsf;
                public double epsx;
                public int maxits;
                public bool xrep;
                public double stpmax;
                public int maxmodelage;
                public bool makeadditers;
                public double[] x;
                public double f;
                public double[] fi;
                public double[,] j;
                public double[,] h;
                public double[] g;
                public bool needf;
                public bool needfg;
                public bool needfgh;
                public bool needfij;
                public bool needfi;
                public bool xupdated;
                public int algomode;
                public bool hasf;
                public bool hasfi;
                public bool hasg;
                public double[] xbase;
                public double fbase;
                public double[] fibase;
                public double[] gbase;
                public double[,] quadraticmodel;
                public double[] bndl;
                public double[] bndu;
                public bool[] havebndl;
                public bool[] havebndu;
                public double[] s;
                public double lambdav;
                public double nu;
                public int modelage;
                public double[] xdir;
                public double[] deltax;
                public double[] deltaf;
                public bool deltaxready;
                public bool deltafready;
                public double teststep;
                public int repiterationscount;
                public int repterminationtype;
                public int repfuncidx;
                public int repvaridx;
                public int repnfunc;
                public int repnjac;
                public int repngrad;
                public int repnhess;
                public int repncholesky;
                public rcommstate rstate;
                public double[] choleskybuf;
                public double[] tmp0;
                public double actualdecrease;
                public double predicteddecrease;
                public double xm1;
                public double xp1;
                public double[] fm1;
                public double[] fp1;
                public double[] fc1;
                public double[] gm1;
                public double[] gp1;
                public double[] gc1;
                public minlbfgs.minlbfgsstate internalstate;
                public minlbfgs.minlbfgsreport internalrep;
                public minqp.minqpstate qpstate;
                public minqp.minqpreport qprep;
                public minlmstate()
                {
                    init();
                }
                public override void init()
                {
                    x = new double[0];
                    fi = new double[0];
                    j = new double[0, 0];
                    h = new double[0, 0];
                    g = new double[0];
                    xbase = new double[0];
                    fibase = new double[0];
                    gbase = new double[0];
                    quadraticmodel = new double[0, 0];
                    bndl = new double[0];
                    bndu = new double[0];
                    havebndl = new bool[0];
                    havebndu = new bool[0];
                    s = new double[0];
                    xdir = new double[0];
                    deltax = new double[0];
                    deltaf = new double[0];
                    rstate = new rcommstate();
                    choleskybuf = new double[0];
                    tmp0 = new double[0];
                    fm1 = new double[0];
                    fp1 = new double[0];
                    fc1 = new double[0];
                    gm1 = new double[0];
                    gp1 = new double[0];
                    gc1 = new double[0];
                    internalstate = new minlbfgs.minlbfgsstate();
                    internalrep = new minlbfgs.minlbfgsreport();
                    qpstate = new minqp.minqpstate();
                    qprep = new minqp.minqpreport();
                }
                public override alglib.apobject make_copy()
                {
                    minlmstate _result = new minlmstate();
                    _result.n = n;
                    _result.m = m;
                    _result.diffstep = diffstep;
                    _result.epsg = epsg;
                    _result.epsf = epsf;
                    _result.epsx = epsx;
                    _result.maxits = maxits;
                    _result.xrep = xrep;
                    _result.stpmax = stpmax;
                    _result.maxmodelage = maxmodelage;
                    _result.makeadditers = makeadditers;
                    _result.x = (double[])x.Clone();
                    _result.f = f;
                    _result.fi = (double[])fi.Clone();
                    _result.j = (double[,])j.Clone();
                    _result.h = (double[,])h.Clone();
                    _result.g = (double[])g.Clone();
                    _result.needf = needf;
                    _result.needfg = needfg;
                    _result.needfgh = needfgh;
                    _result.needfij = needfij;
                    _result.needfi = needfi;
                    _result.xupdated = xupdated;
                    _result.algomode = algomode;
                    _result.hasf = hasf;
                    _result.hasfi = hasfi;
                    _result.hasg = hasg;
                    _result.xbase = (double[])xbase.Clone();
                    _result.fbase = fbase;
                    _result.fibase = (double[])fibase.Clone();
                    _result.gbase = (double[])gbase.Clone();
                    _result.quadraticmodel = (double[,])quadraticmodel.Clone();
                    _result.bndl = (double[])bndl.Clone();
                    _result.bndu = (double[])bndu.Clone();
                    _result.havebndl = (bool[])havebndl.Clone();
                    _result.havebndu = (bool[])havebndu.Clone();
                    _result.s = (double[])s.Clone();
                    _result.lambdav = lambdav;
                    _result.nu = nu;
                    _result.modelage = modelage;
                    _result.xdir = (double[])xdir.Clone();
                    _result.deltax = (double[])deltax.Clone();
                    _result.deltaf = (double[])deltaf.Clone();
                    _result.deltaxready = deltaxready;
                    _result.deltafready = deltafready;
                    _result.teststep = teststep;
                    _result.repiterationscount = repiterationscount;
                    _result.repterminationtype = repterminationtype;
                    _result.repfuncidx = repfuncidx;
                    _result.repvaridx = repvaridx;
                    _result.repnfunc = repnfunc;
                    _result.repnjac = repnjac;
                    _result.repngrad = repngrad;
                    _result.repnhess = repnhess;
                    _result.repncholesky = repncholesky;
                    _result.rstate = (rcommstate)rstate.make_copy();
                    _result.choleskybuf = (double[])choleskybuf.Clone();
                    _result.tmp0 = (double[])tmp0.Clone();
                    _result.actualdecrease = actualdecrease;
                    _result.predicteddecrease = predicteddecrease;
                    _result.xm1 = xm1;
                    _result.xp1 = xp1;
                    _result.fm1 = (double[])fm1.Clone();
                    _result.fp1 = (double[])fp1.Clone();
                    _result.fc1 = (double[])fc1.Clone();
                    _result.gm1 = (double[])gm1.Clone();
                    _result.gp1 = (double[])gp1.Clone();
                    _result.gc1 = (double[])gc1.Clone();
                    _result.internalstate = (minlbfgs.minlbfgsstate)internalstate.make_copy();
                    _result.internalrep = (minlbfgs.minlbfgsreport)internalrep.make_copy();
                    _result.qpstate = (minqp.minqpstate)qpstate.make_copy();
                    _result.qprep = (minqp.minqpreport)qprep.make_copy();
                    return _result;
                }
            };


            /*************************************************************************
            Optimization report, filled by MinLMResults() function

            FIELDS:
            * TerminationType, completetion code:
                * -7    derivative correctness check failed;
                        see Rep.WrongNum, Rep.WrongI, Rep.WrongJ for
                        more information.
                *  1    relative function improvement is no more than
                        EpsF.
                *  2    relative step is no more than EpsX.
                *  4    gradient is no more than EpsG.
                *  5    MaxIts steps was taken
                *  7    stopping conditions are too stringent,
                        further improvement is impossible
            * IterationsCount, contains iterations count
            * NFunc, number of function calculations
            * NJac, number of Jacobi matrix calculations
            * NGrad, number of gradient calculations
            * NHess, number of Hessian calculations
            * NCholesky, number of Cholesky decomposition calculations
            *************************************************************************/
            public class minlmreport : apobject
            {
                public int iterationscount;
                public int terminationtype;
                public int funcidx;
                public int varidx;
                public int nfunc;
                public int njac;
                public int ngrad;
                public int nhess;
                public int ncholesky;
                public minlmreport()
                {
                    init();
                }
                public override void init()
                {
                }
                public override alglib.apobject make_copy()
            {
                minlmreport _result = new minlmreport();
                _result.iterationscount = iterationscount;
                _result.terminationtype = terminationtype;
                _result.funcidx = funcidx;
                _result.varidx = varidx;
                _result.nfunc = nfunc;
                _result.njac = njac;
                _result.ngrad = ngrad;
                _result.nhess = nhess;
                _result.ncholesky = ncholesky;
                return _result;cqmodels
            }
            };




            public const double lambdaup = 2.0;
            public const double lambdadown = 0.33;
            public const double suspiciousnu = 16;
            public const int smallmodelage = 3;
            public const int additers = 5;


            /*************************************************************************
                            IMPROVED LEVENBERG-MARQUARDT METHOD FOR
                             NON-LINEAR LEAST SQUARES OPTIMIZATION

            DESCRIPTION:
            This function is used to find minimum of function which is represented  as
            sum of squares:
                F(x) = f[0]^2(x[0],...,x[n-1]) + ... + f[m-1]^2(x[0],...,x[n-1])
            using value of function vector f[] and Jacobian of f[].


            REQUIREMENTS:
            This algorithm will request following information during its operation:

            * function vector f[] at given point X
            * function vector f[] and Jacobian of f[] (simultaneously) at given point

            There are several overloaded versions of  MinLMOptimize()  function  which
            correspond  to  different LM-like optimization algorithms provided by this
            unit. You should choose version which accepts fvec()  and jac() callbacks.
            First  one  is used to calculate f[] at given point, second one calculates
            f[] and Jacobian df[i]/dx[j].

            You can try to initialize MinLMState structure with VJ  function and  then
            use incorrect version  of  MinLMOptimize()  (for  example,  version  which
            works  with  general  form function and does not provide Jacobian), but it
            will  lead  to  exception  being  thrown  after first attempt to calculate
            Jacobian.


            USAGE:
            1. User initializes algorithm state with MinLMCreateVJ() call
            2. User tunes solver parameters with MinLMSetCond(),  MinLMSetStpMax() and
               other functions
            3. User calls MinLMOptimize() function which  takes algorithm  state   and
               callback functions.
            4. User calls MinLMResults() to get solution
            5. Optionally, user may call MinLMRestartFrom() to solve  another  problem
               with same N/M but another starting point and/or another function.
               MinLMRestartFrom() allows to reuse already initialized structure.


            INPUT PARAMETERS:
                N       -   dimension, N>1
                            * if given, only leading N elements of X are used
                            * if not given, automatically determined from size of X
                M       -   number of functions f[i]
                X       -   initial solution, array[0..N-1]

            OUTPUT PARAMETERS:
                State   -   structure which stores algorithm state

            NOTES:
            1. you may tune stopping conditions with MinLMSetCond() function
            2. if target function contains exp() or other fast growing functions,  and
               optimization algorithm makes too large steps which leads  to  overflow,
               use MinLMSetStpMax() function to bound algorithm's steps.

              -- ALGLIB --
                 Copyright 30.03.2009 by Bochkanov Sergey
            *************************************************************************/
            public static void minlmcreatevj(int n,
                int m,
                double[] x,
                minlmstate state)
            {
                alglib.ap.assert(n >= 1, "MinLMCreateVJ: N<1!");
                alglib.ap.assert(m >= 1, "MinLMCreateVJ: M<1!");
                alglib.ap.assert(alglib.ap.len(x) >= n, "MinLMCreateVJ: Length(X)<N!");
                alglib.ap.assert(apserv.isfinitevector(x, n), "MinLMCreateVJ: X contains infinite or NaN values!");

                //
                // initialize, check parameters
                //
                state.teststep = 0;
                state.n = n;
                state.m = m;
                state.algomode = 1;
                state.hasf = false;
                state.hasfi = true;
                state.hasg = false;

                //
                // second stage of initialization
                //
                lmprepare(n, m, false, state);
                minlmsetacctype(state, 0);
                minlmsetcond(state, 0, 0, 0, 0);
                minlmsetxrep(state, false);
                minlmsetstpmax(state, 0);
                minlmrestartfrom(state, x);
            }


           


            /*************************************************************************
            This function sets stopping conditions for Levenberg-Marquardt optimization
            algorithm.

            INPUT PARAMETERS:
                State   -   structure which stores algorithm state
                EpsG    -   >=0
                            The  subroutine  finishes  its  work   if   the  condition
                            |v|<EpsG is satisfied, where:
                            * |.| means Euclidian norm
                            * v - scaled gradient vector, v[i]=g[i]*s[i]
                            * g - gradient
                            * s - scaling coefficients set by MinLMSetScale()
                EpsF    -   >=0
                            The  subroutine  finishes  its work if on k+1-th iteration
                            the  condition  |F(k+1)-F(k)|<=EpsF*max{|F(k)|,|F(k+1)|,1}
                            is satisfied.
                EpsX    -   >=0
                            The subroutine finishes its work if  on  k+1-th  iteration
                            the condition |v|<=EpsX is fulfilled, where:
                            * |.| means Euclidian norm
                            * v - scaled step vector, v[i]=dx[i]/s[i]
                            * dx - ste pvector, dx=X(k+1)-X(k)
                            * s - scaling coefficients set by MinLMSetScale()
                MaxIts  -   maximum number of iterations. If MaxIts=0, the  number  of
                            iterations   is    unlimited.   Only   Levenberg-Marquardt
                            iterations  are  counted  (L-BFGS/CG  iterations  are  NOT
                            counted because their cost is very low compared to that of
                            LM).

            Passing EpsG=0, EpsF=0, EpsX=0 and MaxIts=0 (simultaneously) will lead to
            automatic stopping criterion selection (small EpsX).

              -- ALGLIB --
                 Copyright 02.04.2010 by Bochkanov Sergey
            *************************************************************************/
            public static void minlmsetcond(minlmstate state,
                double epsg,
                double epsf,
                double epsx,
                int maxits)
            {
                alglib.ap.assert(math.isfinite(epsg), "MinLMSetCond: EpsG is not finite number!");
                alglib.ap.assert((double)(epsg) >= (double)(0), "MinLMSetCond: negative EpsG!");
                alglib.ap.assert(math.isfinite(epsf), "MinLMSetCond: EpsF is not finite number!");
                alglib.ap.assert((double)(epsf) >= (double)(0), "MinLMSetCond: negative EpsF!");
                alglib.ap.assert(math.isfinite(epsx), "MinLMSetCond: EpsX is not finite number!");
                alglib.ap.assert((double)(epsx) >= (double)(0), "MinLMSetCond: negative EpsX!");
                alglib.ap.assert(maxits >= 0, "MinLMSetCond: negative MaxIts!");
                if ((((double)(epsg) == (double)(0) && (double)(epsf) == (double)(0)) && (double)(epsx) == (double)(0)) && maxits == 0)
                {
                    epsx = 1.0E-6;
                }
                state.epsg = epsg;
                state.epsf = epsf;
                state.epsx = epsx;
                state.maxits = maxits;
            }


            /*************************************************************************
            This function turns on/off reporting.

            INPUT PARAMETERS:
                State   -   structure which stores algorithm state
                NeedXRep-   whether iteration reports are needed or not

            If NeedXRep is True, algorithm will call rep() callback function if  it is
            provided to MinLMOptimize(). Both Levenberg-Marquardt and internal  L-BFGS
            iterations are reported.

              -- ALGLIB --
                 Copyright 02.04.2010 by Bochkanov Sergey
            *************************************************************************/
            public static void minlmsetxrep(minlmstate state,
                bool needxrep)
            {
                state.xrep = needxrep;
            }


            /*************************************************************************
            This function sets maximum step length

            INPUT PARAMETERS:
                State   -   structure which stores algorithm state
                StpMax  -   maximum step length, >=0. Set StpMax to 0.0,  if you don't
                            want to limit step length.

            Use this subroutine when you optimize target function which contains exp()
            or  other  fast  growing  functions,  and optimization algorithm makes too
            large  steps  which  leads  to overflow. This function allows us to reject
            steps  that  are  too  large  (and  therefore  expose  us  to the possible
            overflow) without actually calculating function value at the x+stp*d.

            NOTE: non-zero StpMax leads to moderate  performance  degradation  because
            intermediate  step  of  preconditioned L-BFGS optimization is incompatible
            with limits on step size.

              -- ALGLIB --
                 Copyright 02.04.2010 by Bochkanov Sergey
            *************************************************************************/
            public static void minlmsetstpmax(minlmstate state,
                double stpmax)
            {
                alglib.ap.assert(math.isfinite(stpmax), "MinLMSetStpMax: StpMax is not finite!");
                alglib.ap.assert((double)(stpmax) >= (double)(0), "MinLMSetStpMax: StpMax<0!");
                state.stpmax = stpmax;
            }




            /*************************************************************************
            NOTES:

            1. Depending on function used to create state  structure,  this  algorithm
               may accept Jacobian and/or Hessian and/or gradient.  According  to  the
               said above, there ase several versions of this function,  which  accept
               different sets of callbacks.

               This flexibility opens way to subtle errors - you may create state with
               MinLMCreateFGH() (optimization using Hessian), but call function  which
               does not accept Hessian. So when algorithm will request Hessian,  there
               will be no callback to call. In this case exception will be thrown.

               Be careful to avoid such errors because there is no way to find them at
               compile time - you can see them at runtime only.

              -- ALGLIB --
                 Copyright 10.03.2009 by Bochkanov Sergey
            *************************************************************************/
            public static bool minlmiteration(minlmstate state)
            {
                bool result = new bool();
                int n = 0;
                int m = 0;
                bool bflag = new bool();
                int iflag = 0;
                double v = 0;
                double s = 0;
                double t = 0;
                int i = 0;
                int k = 0;
                int i_ = 0;


                //
                // Reverse communication preparations
                // I know it looks ugly, but it works the same way
                // anywhere from C++ to Python.
                //
                // This code initializes locals by:
                // * random values determined during code
                //   generation - on first subroutine call
                // * values from previous call - on subsequent calls
                //
                if (state.rstate.stage >= 0)
                {
                    n = state.rstate.ia[0];
                    m = state.rstate.ia[1];
                    iflag = state.rstate.ia[2];
                    i = state.rstate.ia[3];
                    k = state.rstate.ia[4];
                    bflag = state.rstate.ba[0];
                    v = state.rstate.ra[0];
                    s = state.rstate.ra[1];
                    t = state.rstate.ra[2];
                }
                else
                {
                    n = -983;
                    m = -989;
                    iflag = -834;
                    i = 900;
                    k = -287;
                    bflag = false;
                    v = 214;
                    s = -338;
                    t = -686;
                }
                if (state.rstate.stage == 0)
                {
                    goto lbl_0;
                }
                if (state.rstate.stage == 1)
                {
                    goto lbl_1;
                }
                if (state.rstate.stage == 2)
                {
                    goto lbl_2;
                }
                if (state.rstate.stage == 3)
                {
                    goto lbl_3;
                }
                if (state.rstate.stage == 4)
                {
                    goto lbl_4;
                }
                if (state.rstate.stage == 5)
                {
                    goto lbl_5;
                }
                if (state.rstate.stage == 6)
                {
                    goto lbl_6;
                }
                if (state.rstate.stage == 7)
                {
                    goto lbl_7;
                }
                if (state.rstate.stage == 8)
                {
                    goto lbl_8;
                }
                if (state.rstate.stage == 9)
                {
                    goto lbl_9;
                }
                if (state.rstate.stage == 10)
                {
                    goto lbl_10;
                }
                if (state.rstate.stage == 11)
                {
                    goto lbl_11;
                }
                if (state.rstate.stage == 12)
                {
                    goto lbl_12;
                }
                if (state.rstate.stage == 13)
                {
                    goto lbl_13;
                }
                if (state.rstate.stage == 14)
                {
                    goto lbl_14;
                }
                if (state.rstate.stage == 15)
                {
                    goto lbl_15;
                }
                if (state.rstate.stage == 16)
                {
                    goto lbl_16;
                }
                if (state.rstate.stage == 17)
                {
                    goto lbl_17;
                }
                if (state.rstate.stage == 18)
                {
                    goto lbl_18;
                }

                //
                // Routine body
                //

                //
                // prepare
                //
                n = state.n;
                m = state.m;
                state.repiterationscount = 0;
                state.repterminationtype = 0;
                state.repfuncidx = -1;
                state.repvaridx = -1;
                state.repnfunc = 0;
                state.repnjac = 0;
                state.repngrad = 0;
                state.repnhess = 0;
                state.repncholesky = 0;

                //
                // check consistency of constraints,
                // enforce feasibility of the solution
                // set constraints
                //
                if (!optserv.enforceboundaryconstraints(ref state.xbase, state.bndl, state.havebndl, state.bndu, state.havebndu, n, 0))
                {
                    state.repterminationtype = -3;
                    result = false;
                    return result;
                }
                minqp.minqpsetbc(state.qpstate, state.bndl, state.bndu);

                //
                //  Check, that transferred derivative value is right
                //
                clearrequestfields(state);
                if (!(state.algomode == 1 && (double)(state.teststep) > (double)(0)))
                {
                    goto lbl_19;
                }
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    state.x[i_] = state.xbase[i_];
                }
                state.needfij = true;
                i = 0;
            lbl_21:
                if (i > n - 1)
                {
                    goto lbl_23;
                }
                alglib.ap.assert((state.havebndl[i] && (double)(state.bndl[i]) <= (double)(state.x[i])) || !state.havebndl[i], "MinLM: internal error(State.X is out of bounds)");
                alglib.ap.assert((state.havebndu[i] && (double)(state.x[i]) <= (double)(state.bndu[i])) || !state.havebndu[i], "MinLMIteration: internal error(State.X is out of bounds)");
                v = state.x[i];
                state.x[i] = v - state.teststep * state.s[i];
                if (state.havebndl[i])
                {
                    state.x[i] = Math.Max(state.x[i], state.bndl[i]);
                }
                state.xm1 = state.x[i];
                state.rstate.stage = 0;
                goto lbl_rcomm;
            lbl_0:
                for (i_ = 0; i_ <= m - 1; i_++)
                {
                    state.fm1[i_] = state.fi[i_];
                }
                for (i_ = 0; i_ <= m - 1; i_++)
                {
                    state.gm1[i_] = state.j[i_, i];
                }
                state.x[i] = v + state.teststep * state.s[i];
                if (state.havebndu[i])
                {
                    state.x[i] = Math.Min(state.x[i], state.bndu[i]);
                }
                state.xp1 = state.x[i];
                state.rstate.stage = 1;
                goto lbl_rcomm;
            lbl_1:
                for (i_ = 0; i_ <= m - 1; i_++)
                {
                    state.fp1[i_] = state.fi[i_];
                }
                for (i_ = 0; i_ <= m - 1; i_++)
                {
                    state.gp1[i_] = state.j[i_, i];
                }
                state.x[i] = (state.xm1 + state.xp1) / 2;
                if (state.havebndl[i])
                {
                    state.x[i] = Math.Max(state.x[i], state.bndl[i]);
                }
                if (state.havebndu[i])
                {
                    state.x[i] = Math.Min(state.x[i], state.bndu[i]);
                }
                state.rstate.stage = 2;
                goto lbl_rcomm;
            lbl_2:
                for (i_ = 0; i_ <= m - 1; i_++)
                {
                    state.fc1[i_] = state.fi[i_];
                }
                for (i_ = 0; i_ <= m - 1; i_++)
                {
                    state.gc1[i_] = state.j[i_, i];
                }
                state.x[i] = v;
                for (k = 0; k <= m - 1; k++)
                {
                    if (!optserv.derivativecheck(state.fm1[k], state.gm1[k], state.fp1[k], state.gp1[k], state.fc1[k], state.gc1[k], state.xp1 - state.xm1))
                    {
                        state.repfuncidx = k;
                        state.repvaridx = i;
                        state.repterminationtype = -7;
                        result = false;
                        return result;
                    }
                }
                i = i + 1;
                goto lbl_21;
            lbl_23:
                state.needfij = false;
            lbl_19:

                //
                // Initial report of current point
                //
                // Note 1: we rewrite State.X twice because
                // user may accidentally change it after first call.
                //
                // Note 2: we set NeedF or NeedFI depending on what
                // information about function we have.
                //
                if (!state.xrep)
                {
                    goto lbl_24;
                }
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    state.x[i_] = state.xbase[i_];
                }
                clearrequestfields(state);
                if (!state.hasf)
                {
                    goto lbl_26;
                }
                state.needf = true;
                state.rstate.stage = 3;
                goto lbl_rcomm;
            lbl_3:
                state.needf = false;
                goto lbl_27;
            lbl_26:
                alglib.ap.assert(state.hasfi, "MinLM: internal error 2!");
                state.needfi = true;
                state.rstate.stage = 4;
                goto lbl_rcomm;
            lbl_4:
                state.needfi = false;
                v = 0.0;
                for (i_ = 0; i_ <= m - 1; i_++)
                {
                    v += state.fi[i_] * state.fi[i_];
                }
                state.f = v;
            lbl_27:
                state.repnfunc = state.repnfunc + 1;
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    state.x[i_] = state.xbase[i_];
                }
                clearrequestfields(state);
                state.xupdated = true;
                state.rstate.stage = 5;
                goto lbl_rcomm;
            lbl_5:
                state.xupdated = false;
            lbl_24:

                //
                // Prepare control variables
                //
                state.nu = 1;
                state.lambdav = -math.maxrealnumber;
                state.modelage = state.maxmodelage + 1;
                state.deltaxready = false;
                state.deltafready = false;

                //
            // Main cycle.
            //
            // We move through it until either:
            // * one of the stopping conditions is met
            // * we decide that stopping conditions are too stringent
            //   and break from cycle
            //
            //
            lbl_28:
                if (false)
                {
                    goto lbl_29;
                }

                //
                // First, we have to prepare quadratic model for our function.
                // We use BFlag to ensure that model is prepared;
                // if it is false at the end of this block, something went wrong.
                //
                // We may either calculate brand new model or update old one.
                //
                // Before this block we have:
                // * State.XBase            - current position.
                // * State.DeltaX           - if DeltaXReady is True
                // * State.DeltaF           - if DeltaFReady is True
                //
                // After this block is over, we will have:
                // * State.XBase            - base point (unchanged)
                // * State.FBase            - F(XBase)
                // * State.GBase            - linear term
                // * State.QuadraticModel   - quadratic term
                // * State.LambdaV          - current estimate for lambda
                //
                // We also clear DeltaXReady/DeltaFReady flags
                // after initialization is done.
                //
                bflag = false;
                if (!(state.algomode == 0 || state.algomode == 1))
                {
                    goto lbl_30;
                }

                //
                // Calculate f[] and Jacobian
                //
                if (!(state.modelage > state.maxmodelage || !(state.deltaxready && state.deltafready)))
                {
                    goto lbl_32;
                }

                //
                // Refresh model (using either finite differences or analytic Jacobian)
                //
                if (state.algomode != 0)
                {
                    goto lbl_34;
                }

                //
                // Optimization using F values only.
                // Use finite differences to estimate Jacobian.
                //
                alglib.ap.assert(state.hasfi, "MinLMIteration: internal error when estimating Jacobian (no f[])");
                k = 0;
            lbl_36:
                if (k > n - 1)
                {
                    goto lbl_38;
                }

                //
                // We guard X[k] from leaving [BndL,BndU].
                // In case BndL=BndU, we assume that derivative in this direction is zero.
                //
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    state.x[i_] = state.xbase[i_];
                }
                state.x[k] = state.x[k] - state.s[k] * state.diffstep;
                if (state.havebndl[k])
                {
                    state.x[k] = Math.Max(state.x[k], state.bndl[k]);
                }
                if (state.havebndu[k])
                {
                    state.x[k] = Math.Min(state.x[k], state.bndu[k]);
                }
                state.xm1 = state.x[k];
                clearrequestfields(state);
                state.needfi = true;
                state.rstate.stage = 6;
                goto lbl_rcomm;
            lbl_6:
                state.repnfunc = state.repnfunc + 1;
                for (i_ = 0; i_ <= m - 1; i_++)
                {
                    state.fm1[i_] = state.fi[i_];
                }
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    state.x[i_] = state.xbase[i_];
                }
                state.x[k] = state.x[k] + state.s[k] * state.diffstep;
                if (state.havebndl[k])
                {
                    state.x[k] = Math.Max(state.x[k], state.bndl[k]);
                }
                if (state.havebndu[k])
                {
                    state.x[k] = Math.Min(state.x[k], state.bndu[k]);
                }
                state.xp1 = state.x[k];
                clearrequestfields(state);
                state.needfi = true;
                state.rstate.stage = 7;
                goto lbl_rcomm;
            lbl_7:
                state.repnfunc = state.repnfunc + 1;
                for (i_ = 0; i_ <= m - 1; i_++)
                {
                    state.fp1[i_] = state.fi[i_];
                }
                v = state.xp1 - state.xm1;
                if ((double)(v) != (double)(0))
                {
                    v = 1 / v;
                    for (i_ = 0; i_ <= m - 1; i_++)
                    {
                        state.j[i_, k] = v * state.fp1[i_];
                    }
                    for (i_ = 0; i_ <= m - 1; i_++)
                    {
                        state.j[i_, k] = state.j[i_, k] - v * state.fm1[i_];
                    }
                }
                else
                {
                    for (i = 0; i <= m - 1; i++)
                    {
                        state.j[i, k] = 0;
                    }
                }
                k = k + 1;
                goto lbl_36;
            lbl_38:

                //
                // Calculate F(XBase)
                //
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    state.x[i_] = state.xbase[i_];
                }
                clearrequestfields(state);
                state.needfi = true;
                state.rstate.stage = 8;
                goto lbl_rcomm;
            lbl_8:
                state.needfi = false;
                state.repnfunc = state.repnfunc + 1;
                state.repnjac = state.repnjac + 1;

                //
                // New model
                //
                state.modelage = 0;
                goto lbl_35;
            lbl_34:

                //
                // Obtain f[] and Jacobian
                //
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    state.x[i_] = state.xbase[i_];
                }
                clearrequestfields(state);
                state.needfij = true;
                state.rstate.stage = 9;
                goto lbl_rcomm;
            lbl_9:
                state.needfij = false;
                state.repnfunc = state.repnfunc + 1;
                state.repnjac = state.repnjac + 1;

                //
                // New model
                //
                state.modelage = 0;
            lbl_35:
                goto lbl_33;
            lbl_32:

                //
                // State.J contains Jacobian or its current approximation;
                // refresh it using secant updates:
                //
                // f(x0+dx) = f(x0) + J*dx,
                // J_new = J_old + u*h'
                // h = x_new-x_old
                // u = (f_new - f_old - J_old*h)/(h'h)
                //
                // We can explicitly generate h and u, but it is
                // preferential to do in-place calculations. Only
                // I-th row of J_old is needed to calculate u[I],
                // so we can update J row by row in one pass.
                //
                // NOTE: we expect that State.XBase contains new point,
                // State.FBase contains old point, State.DeltaX and
                // State.DeltaY contain updates from last step.
                //
                alglib.ap.assert(state.deltaxready && state.deltafready, "MinLMIteration: uninitialized DeltaX/DeltaF");
                t = 0.0;
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    t += state.deltax[i_] * state.deltax[i_];
                }
                alglib.ap.assert((double)(t) != (double)(0), "MinLM: internal error (T=0)");
                for (i = 0; i <= m - 1; i++)
                {
                    v = 0.0;
                    for (i_ = 0; i_ <= n - 1; i_++)
                    {
                        v += state.j[i, i_] * state.deltax[i_];
                    }
                    v = (state.deltaf[i] - v) / t;
                    for (i_ = 0; i_ <= n - 1; i_++)
                    {
                        state.j[i, i_] = state.j[i, i_] + v * state.deltax[i_];
                    }
                }
                for (i_ = 0; i_ <= m - 1; i_++)
                {
                    state.fi[i_] = state.fibase[i_];
                }
                for (i_ = 0; i_ <= m - 1; i_++)
                {
                    state.fi[i_] = state.fi[i_] + state.deltaf[i_];
                }

                //
                // Increase model age
                //
                state.modelage = state.modelage + 1;
            lbl_33:

                //
                // Generate quadratic model:
                //     f(xbase+dx) =
                //       = (f0 + J*dx)'(f0 + J*dx)
                //       = f0^2 + dx'J'f0 + f0*J*dx + dx'J'J*dx
                //       = f0^2 + 2*f0*J*dx + dx'J'J*dx
                //
                // Note that we calculate 2*(J'J) instead of J'J because
                // our quadratic model is based on Tailor decomposition,
                // i.e. it has 0.5 before quadratic term.
                //
                ablas.rmatrixgemm(n, n, m, 2.0, state.j, 0, 0, 1, state.j, 0, 0, 0, 0.0, state.quadraticmodel, 0, 0);
                ablas.rmatrixmv(n, m, state.j, 0, 0, 1, state.fi, 0, ref state.gbase, 0);
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    state.gbase[i_] = 2 * state.gbase[i_];
                }
                v = 0.0;
                for (i_ = 0; i_ <= m - 1; i_++)
                {
                    v += state.fi[i_] * state.fi[i_];
                }
                state.fbase = v;
                for (i_ = 0; i_ <= m - 1; i_++)
                {
                    state.fibase[i_] = state.fi[i_];
                }

                //
                // set control variables
                //
                bflag = true;
            lbl_30:
                if (state.algomode != 2)
                {
                    goto lbl_39;
                }
                alglib.ap.assert(!state.hasfi, "MinLMIteration: internal error (HasFI is True in Hessian-based mode)");

                //
                // Obtain F, G, H
                //
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    state.x[i_] = state.xbase[i_];
                }
                clearrequestfields(state);
                state.needfgh = true;
                state.rstate.stage = 10;
                goto lbl_rcomm;
            lbl_10:
                state.needfgh = false;
                state.repnfunc = state.repnfunc + 1;
                state.repngrad = state.repngrad + 1;
                state.repnhess = state.repnhess + 1;
                ablas.rmatrixcopy(n, n, state.h, 0, 0, ref state.quadraticmodel, 0, 0);
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    state.gbase[i_] = state.g[i_];
                }
                state.fbase = state.f;

                //
                // set control variables
                //
                bflag = true;
                state.modelage = 0;
            lbl_39:
                alglib.ap.assert(bflag, "MinLM: internal integrity check failed!");
                state.deltaxready = false;
                state.deltafready = false;

                //
                // If Lambda is not initialized, initialize it using quadratic model
                //
                if ((double)(state.lambdav) < (double)(0))
                {
                    state.lambdav = 0;
                    for (i = 0; i <= n - 1; i++)
                    {
                        state.lambdav = Math.Max(state.lambdav, Math.Abs(state.quadraticmodel[i, i]) * math.sqr(state.s[i]));
                    }
                    state.lambdav = 0.001 * state.lambdav;
                    if ((double)(state.lambdav) == (double)(0))
                    {
                        state.lambdav = 1;
                    }
                }

                //
                // Test stopping conditions for function gradient
                //
                if ((double)(boundedscaledantigradnorm(state, state.xbase, state.gbase)) > (double)(state.epsg))
                {
                    goto lbl_41;
                }
                if (state.modelage != 0)
                {
                    goto lbl_43;
                }

                //
                // Model is fresh, we can rely on it and terminate algorithm
                //
                state.repterminationtype = 4;
                if (!state.xrep)
                {
                    goto lbl_45;
                }
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    state.x[i_] = state.xbase[i_];
                }
                state.f = state.fbase;
                clearrequestfields(state);
                state.xupdated = true;
                state.rstate.stage = 11;
                goto lbl_rcomm;
            lbl_11:
                state.xupdated = false;
            lbl_45:
                result = false;
                return result;
                goto lbl_44;
            lbl_43:

                //
                // Model is not fresh, we should refresh it and test
                // conditions once more
                //
                state.modelage = state.maxmodelage + 1;
                goto lbl_28;
            lbl_44:
            lbl_41:

                //
                // Find value of Levenberg-Marquardt damping parameter which:
                // * leads to positive definite damped model
                // * within bounds specified by StpMax
                // * generates step which decreases function value
                //
                // After this block IFlag is set to:
                // * -3, if constraints are infeasible
                // * -2, if model update is needed (either Lambda growth is too large
                //       or step is too short, but we can't rely on model and stop iterations)
                // * -1, if model is fresh, Lambda have grown too large, termination is needed
                // *  0, if everything is OK, continue iterations
                //
                // State.Nu can have any value on enter, but after exit it is set to 1.0
                //
                iflag = -99;
            lbl_47:
                if (false)
                {
                    goto lbl_48;
                }

                //
                // Do we need model update?
                //
                if (state.modelage > 0 && (double)(state.nu) >= (double)(suspiciousnu))
                {
                    iflag = -2;
                    goto lbl_48;
                }

                //
                // Setup quadratic solver and solve quadratic programming problem.
                // After problem is solved we'll try to bound step by StpMax
                // (Lambda will be increased if step size is too large).
                //
                // We use BFlag variable to indicate that we have to increase Lambda.
                // If it is False, we will try to increase Lambda and move to new iteration.
                //
                bflag = true;
                minqp.minqpsetstartingpointfast(state.qpstate, state.xbase);
                minqp.minqpsetoriginfast(state.qpstate, state.xbase);
                minqp.minqpsetlineartermfast(state.qpstate, state.gbase);
                minqp.minqpsetquadratictermfast(state.qpstate, state.quadraticmodel, true, 0.0);
                for (i = 0; i <= n - 1; i++)
                {
                    state.tmp0[i] = state.quadraticmodel[i, i] + state.lambdav / math.sqr(state.s[i]);
                }
                minqp.minqprewritediagonal(state.qpstate, state.tmp0);
                minqp.minqpoptimize(state.qpstate);
                minqp.minqpresultsbuf(state.qpstate, ref state.xdir, state.qprep);
                if (state.qprep.terminationtype > 0)
                {

                    //
                    // successful solution of QP problem
                    //
                    for (i_ = 0; i_ <= n - 1; i_++)
                    {
                        state.xdir[i_] = state.xdir[i_] - state.xbase[i_];
                    }
                    v = 0.0;
                    for (i_ = 0; i_ <= n - 1; i_++)
                    {
                        v += state.xdir[i_] * state.xdir[i_];
                    }
                    if (math.isfinite(v))
                    {
                        v = Math.Sqrt(v);
                        if ((double)(state.stpmax) > (double)(0) && (double)(v) > (double)(state.stpmax))
                        {
                            bflag = false;
                        }
                    }
                    else
                    {
                        bflag = false;
                    }
                }
                else
                {

                    //
                    // Either problem is non-convex (increase LambdaV) or constraints are inconsistent
                    //
                    alglib.ap.assert(state.qprep.terminationtype == -3 || state.qprep.terminationtype == -5, "MinLM: unexpected completion code from QP solver");
                    if (state.qprep.terminationtype == -3)
                    {
                        iflag = -3;
                        goto lbl_48;
                    }
                    bflag = false;
                }
                if (!bflag)
                {

                    //
                    // Solution failed:
                    // try to increase lambda to make matrix positive definite and continue.
                    //
                    if (!increaselambda(ref state.lambdav, ref state.nu))
                    {
                        iflag = -1;
                        goto lbl_48;
                    }
                    goto lbl_47;
                }

                //
                // Step in State.XDir and it is bounded by StpMax.
                //
                // We should check stopping conditions on step size here.
                // DeltaX, which is used for secant updates, is initialized here.
                //
                // This code is a bit tricky because sometimes XDir<>0, but
                // it is so small that XDir+XBase==XBase (in finite precision
                // arithmetics). So we set DeltaX to XBase, then
                // add XDir, and then subtract XBase to get exact value of
                // DeltaX.
                //
                // Step length is estimated using DeltaX.
                //
                // NOTE: stopping conditions are tested
                // for fresh models only (ModelAge=0)
                //
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    state.deltax[i_] = state.xbase[i_];
                }
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    state.deltax[i_] = state.deltax[i_] + state.xdir[i_];
                }
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    state.deltax[i_] = state.deltax[i_] - state.xbase[i_];
                }
                state.deltaxready = true;
                v = 0.0;
                for (i = 0; i <= n - 1; i++)
                {
                    v = v + math.sqr(state.deltax[i] / state.s[i]);
                }
                v = Math.Sqrt(v);
                if ((double)(v) > (double)(state.epsx))
                {
                    goto lbl_49;
                }
                if (state.modelage != 0)
                {
                    goto lbl_51;
                }

                //
                // Step is too short, model is fresh and we can rely on it.
                // Terminating.
                //
                state.repterminationtype = 2;
                if (!state.xrep)
                {
                    goto lbl_53;
                }
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    state.x[i_] = state.xbase[i_];
                }
                state.f = state.fbase;
                clearrequestfields(state);
                state.xupdated = true;
                state.rstate.stage = 12;
                goto lbl_rcomm;
            lbl_12:
                state.xupdated = false;
            lbl_53:
                result = false;
                return result;
                goto lbl_52;
            lbl_51:

                //
                // Step is suspiciously short, but model is not fresh
                // and we can't rely on it.
                //
                iflag = -2;
                goto lbl_48;
            lbl_52:
            lbl_49:

                //
                // Let's evaluate new step:
                // a) if we have Fi vector, we evaluate it using rcomm, and
                //    then we manually calculate State.F as sum of squares of Fi[]
                // b) if we have F value, we just evaluate it through rcomm interface
                //
                // We prefer (a) because we may need Fi vector for additional
                // iterations
                //
                alglib.ap.assert(state.hasfi || state.hasf, "MinLM: internal error 2!");
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    state.x[i_] = state.xbase[i_];
                }
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    state.x[i_] = state.x[i_] + state.xdir[i_];
                }
                clearrequestfields(state);
                if (!state.hasfi)
                {
                    goto lbl_55;
                }
                state.needfi = true;
                state.rstate.stage = 13;
                goto lbl_rcomm;
            lbl_13:
                state.needfi = false;
                v = 0.0;
                for (i_ = 0; i_ <= m - 1; i_++)
                {
                    v += state.fi[i_] * state.fi[i_];
                }
                state.f = v;
                for (i_ = 0; i_ <= m - 1; i_++)
                {
                    state.deltaf[i_] = state.fi[i_];
                }
                for (i_ = 0; i_ <= m - 1; i_++)
                {
                    state.deltaf[i_] = state.deltaf[i_] - state.fibase[i_];
                }
                state.deltafready = true;
                goto lbl_56;
            lbl_55:
                state.needf = true;
                state.rstate.stage = 14;
                goto lbl_rcomm;
            lbl_14:
                state.needf = false;
            lbl_56:
                state.repnfunc = state.repnfunc + 1;
                if ((double)(state.f) >= (double)(state.fbase))
                {

                    //
                    // Increase lambda and continue
                    //
                    if (!increaselambda(ref state.lambdav, ref state.nu))
                    {
                        iflag = -1;
                        goto lbl_48;
                    }
                    goto lbl_47;
                }

                //
                // We've found our step!
                //
                iflag = 0;
                goto lbl_48;
                goto lbl_47;
            lbl_48:
                state.nu = 1;
                alglib.ap.assert(iflag >= -3 && iflag <= 0, "MinLM: internal integrity check failed!");
                if (iflag == -3)
                {
                    state.repterminationtype = -3;
                    result = false;
                    return result;
                }
                if (iflag == -2)
                {
                    state.modelage = state.maxmodelage + 1;
                    goto lbl_28;
                }
                if (iflag == -1)
                {
                    goto lbl_29;
                }

                //
                // Levenberg-Marquardt step is ready.
                // Compare predicted vs. actual decrease and decide what to do with lambda.
                //
                // NOTE: we expect that State.DeltaX contains direction of step,
                // State.F contains function value at new point.
                //
                alglib.ap.assert(state.deltaxready, "MinLM: deltaX is not ready");
                t = 0;
                for (i = 0; i <= n - 1; i++)
                {
                    v = 0.0;
                    for (i_ = 0; i_ <= n - 1; i_++)
                    {
                        v += state.quadraticmodel[i, i_] * state.deltax[i_];
                    }
                    t = t + state.deltax[i] * state.gbase[i] + 0.5 * state.deltax[i] * v;
                }
                state.predicteddecrease = -t;
                state.actualdecrease = -(state.f - state.fbase);
                if ((double)(state.predicteddecrease) <= (double)(0))
                {
                    goto lbl_29;
                }
                v = state.actualdecrease / state.predicteddecrease;
                if ((double)(v) >= (double)(0.1))
                {
                    goto lbl_57;
                }
                if (increaselambda(ref state.lambdav, ref state.nu))
                {
                    goto lbl_59;
                }

                //
                // Lambda is too large, we have to break iterations.
                //
                state.repterminationtype = 7;
                if (!state.xrep)
                {
                    goto lbl_61;
                }
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    state.x[i_] = state.xbase[i_];
                }
                state.f = state.fbase;
                clearrequestfields(state);
                state.xupdated = true;
                state.rstate.stage = 15;
                goto lbl_rcomm;
            lbl_15:
                state.xupdated = false;
            lbl_61:
                result = false;
                return result;
            lbl_59:
            lbl_57:
                if ((double)(v) > (double)(0.5))
                {
                    decreaselambda(ref state.lambdav, ref state.nu);
                }

                //
                // Accept step, report it and
                // test stopping conditions on iterations count and function decrease.
                //
                // NOTE: we expect that State.DeltaX contains direction of step,
                // State.F contains function value at new point.
                //
                // NOTE2: we should update XBase ONLY. In the beginning of the next
                // iteration we expect that State.FIBase is NOT updated and
                // contains old value of a function vector.
                //
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    state.xbase[i_] = state.xbase[i_] + state.deltax[i_];
                }
                if (!state.xrep)
                {
                    goto lbl_63;
                }
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    state.x[i_] = state.xbase[i_];
                }
                clearrequestfields(state);
                state.xupdated = true;
                state.rstate.stage = 16;
                goto lbl_rcomm;
            lbl_16:
                state.xupdated = false;
            lbl_63:
                state.repiterationscount = state.repiterationscount + 1;
                if (state.repiterationscount >= state.maxits && state.maxits > 0)
                {
                    state.repterminationtype = 5;
                }
                if (state.modelage == 0)
                {
                    if ((double)(Math.Abs(state.f - state.fbase)) <= (double)(state.epsf * Math.Max(1, Math.Max(Math.Abs(state.f), Math.Abs(state.fbase)))))
                    {
                        state.repterminationtype = 1;
                    }
                }
                if (state.repterminationtype <= 0)
                {
                    goto lbl_65;
                }
                if (!state.xrep)
                {
                    goto lbl_67;
                }

                //
                // Report: XBase contains new point, F contains function value at new point
                //
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    state.x[i_] = state.xbase[i_];
                }
                clearrequestfields(state);
                state.xupdated = true;
                state.rstate.stage = 17;
                goto lbl_rcomm;
            lbl_17:
                state.xupdated = false;
            lbl_67:
                result = false;
                return result;
            lbl_65:
                state.modelage = state.modelage + 1;
                goto lbl_28;
            lbl_29:

                //
                // Lambda is too large, we have to break iterations.
                //
                state.repterminationtype = 7;
                if (!state.xrep)
                {
                    goto lbl_69;
                }
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    state.x[i_] = state.xbase[i_];
                }
                state.f = state.fbase;
                clearrequestfields(state);
                state.xupdated = true;
                state.rstate.stage = 18;
                goto lbl_rcomm;
            lbl_18:
                state.xupdated = false;
            lbl_69:
                result = false;
                return result;

                //
            // Saving state
            //
            lbl_rcomm:
                result = true;
                state.rstate.ia[0] = n;
                state.rstate.ia[1] = m;
                state.rstate.ia[2] = iflag;
                state.rstate.ia[3] = i;
                state.rstate.ia[4] = k;
                state.rstate.ba[0] = bflag;
                state.rstate.ra[0] = v;
                state.rstate.ra[1] = s;
                state.rstate.ra[2] = t;
                return result;
            }



            /*************************************************************************
            This function is used to change acceleration settings

            You can choose between three acceleration strategies:
            * AccType=0, no acceleration.
            * AccType=1, secant updates are used to update quadratic model after  each
              iteration. After fixed number of iterations (or after  model  breakdown)
              we  recalculate  quadratic  model  using  analytic  Jacobian  or  finite
              differences. Number of secant-based iterations depends  on  optimization
              settings: about 3 iterations - when we have analytic Jacobian, up to 2*N
              iterations - when we use finite differences to calculate Jacobian.

            AccType=1 is recommended when Jacobian  calculation  cost  is  prohibitive
            high (several Mx1 function vector calculations  followed  by  several  NxN
            Cholesky factorizations are faster than calculation of one M*N  Jacobian).
            It should also be used when we have no Jacobian, because finite difference
            approximation takes too much time to compute.

            Table below list  optimization  protocols  (XYZ  protocol  corresponds  to
            MinLMCreateXYZ) and acceleration types they support (and use by  default).

            ACCELERATION TYPES SUPPORTED BY OPTIMIZATION PROTOCOLS:

            protocol    0   1   comment
            V           +   +
            VJ          +   +
            FGH         +

            DAFAULT VALUES:

            protocol    0   1   comment
            V               x   without acceleration it is so slooooooooow
            VJ          x
            FGH         x

            NOTE: this  function should be called before optimization. Attempt to call
            it during algorithm iterations may result in unexpected behavior.

            NOTE: attempt to call this function with unsupported protocol/acceleration
            combination will result in exception being thrown.

              -- ALGLIB --
                 Copyright 14.10.2010 by Bochkanov Sergey
            *************************************************************************/
            public static void minlmsetacctype(minlmstate state,
                int acctype)
            {
                alglib.ap.assert((acctype == 0 || acctype == 1) || acctype == 2, "MinLMSetAccType: incorrect AccType!");
                if (acctype == 2)
                {
                    acctype = 0;
                }
                if (acctype == 0)
                {
                    state.maxmodelage = 0;
                    state.makeadditers = false;
                    return;
                }
                if (acctype == 1)
                {
                    alglib.ap.assert(state.hasfi, "MinLMSetAccType: AccType=1 is incompatible with current protocol!");
                    if (state.algomode == 0)
                    {
                        state.maxmodelage = 2 * state.n;
                    }
                    else
                    {
                        state.maxmodelage = smallmodelage;
                    }
                    state.makeadditers = false;
                    return;
                }
            }


            /*************************************************************************
            Levenberg-Marquardt algorithm results

            INPUT PARAMETERS:
                State   -   algorithm state

            OUTPUT PARAMETERS:
                X       -   array[0..N-1], solution
                Rep     -   optimization report;
                            see comments for this structure for more info.

              -- ALGLIB --
                 Copyright 10.03.2009 by Bochkanov Sergey
            *************************************************************************/
            public static void minlmresults(minlmstate state,
                ref double[] x,
                minlmreport rep)
            {
                x = new double[0];

                minlmresultsbuf(state, ref x, rep);
            }


            /*************************************************************************
            Levenberg-Marquardt algorithm results

            Buffered implementation of MinLMResults(), which uses pre-allocated buffer
            to store X[]. If buffer size is  too  small,  it  resizes  buffer.  It  is
            intended to be used in the inner cycles of performance critical algorithms
            where array reallocation penalty is too large to be ignored.

              -- ALGLIB --
                 Copyright 10.03.2009 by Bochkanov Sergey
            *************************************************************************/
            public static void minlmresultsbuf(minlmstate state,
                ref double[] x,
                minlmreport rep)
            {
                int i_ = 0;

                if (alglib.ap.len(x) < state.n)
                {
                    x = new double[state.n];
                }
                for (i_ = 0; i_ <= state.n - 1; i_++)
                {
                    x[i_] = state.x[i_];
                }
                rep.iterationscount = state.repiterationscount;
                rep.terminationtype = state.repterminationtype;
                rep.funcidx = state.repfuncidx;
                rep.varidx = state.repvaridx;
                rep.nfunc = state.repnfunc;
                rep.njac = state.repnjac;
                rep.ngrad = state.repngrad;
                rep.nhess = state.repnhess;
                rep.ncholesky = state.repncholesky;
            }


            /*************************************************************************
            This  subroutine  restarts  LM  algorithm from new point. All optimization
            parameters are left unchanged.

            This  function  allows  to  solve multiple  optimization  problems  (which
            must have same number of dimensions) without object reallocation penalty.

            INPUT PARAMETERS:
                State   -   structure used for reverse communication previously
                            allocated with MinLMCreateXXX call.
                X       -   new starting point.

              -- ALGLIB --
                 Copyright 30.07.2010 by Bochkanov Sergey
            *************************************************************************/
            public static void minlmrestartfrom(minlmstate state,
                double[] x)
            {
                int i_ = 0;

                alglib.ap.assert(alglib.ap.len(x) >= state.n, "MinLMRestartFrom: Length(X)<N!");
                alglib.ap.assert(apserv.isfinitevector(x, state.n), "MinLMRestartFrom: X contains infinite or NaN values!");
                for (i_ = 0; i_ <= state.n - 1; i_++)
                {
                    state.xbase[i_] = x[i_];
                }
                state.rstate.ia = new int[4 + 1];
                state.rstate.ba = new bool[0 + 1];
                state.rstate.ra = new double[2 + 1];
                state.rstate.stage = -1;
                clearrequestfields(state);
            }


            /*************************************************************************
            Prepare internal structures (except for RComm).

            Note: M must be zero for FGH mode, non-zero for V/VJ/FJ/FGJ mode.
            *************************************************************************/
            private static void lmprepare(int n,
                int m,
                bool havegrad,
                minlmstate state)
            {
                int i = 0;

                if (n <= 0 || m < 0)
                {
                    return;
                }
                if (havegrad)
                {
                    state.g = new double[n];
                }
                if (m != 0)
                {
                    state.j = new double[m, n];
                    state.fi = new double[m];
                    state.fibase = new double[m];
                    state.deltaf = new double[m];
                    state.fm1 = new double[m];
                    state.fp1 = new double[m];
                    state.fc1 = new double[m];
                    state.gm1 = new double[m];
                    state.gp1 = new double[m];
                    state.gc1 = new double[m];
                }
                else
                {
                    state.h = new double[n, n];
                }
                state.x = new double[n];
                state.deltax = new double[n];
                state.quadraticmodel = new double[n, n];
                state.xbase = new double[n];
                state.gbase = new double[n];
                state.xdir = new double[n];
                state.tmp0 = new double[n];

                //
                // prepare internal L-BFGS
                //
                for (i = 0; i <= n - 1; i++)
                {
                    state.x[i] = 0;
                }
                minlbfgs.minlbfgscreate(n, Math.Min(additers, n), state.x, state.internalstate);
                minlbfgs.minlbfgssetcond(state.internalstate, 0.0, 0.0, 0.0, Math.Min(additers, n));

                //
                // Prepare internal QP solver
                //
                minqp.minqpcreate(n, state.qpstate);
                minqp.minqpsetalgocholesky(state.qpstate);

                //
                // Prepare boundary constraints
                //
                state.bndl = new double[n];
                state.bndu = new double[n];
                state.havebndl = new bool[n];
                state.havebndu = new bool[n];
                for (i = 0; i <= n - 1; i++)
                {
                    state.bndl[i] = Double.NegativeInfinity;
                    state.havebndl[i] = false;
                    state.bndu[i] = Double.PositiveInfinity;
                    state.havebndu[i] = false;
                }

                //
                // Prepare scaling matrix
                //
                state.s = new double[n];
                for (i = 0; i <= n - 1; i++)
                {
                    state.s[i] = 1.0;
                }
            }


            /*************************************************************************
            Clears request fileds (to be sure that we don't forgot to clear something)
            *************************************************************************/
            private static void clearrequestfields(minlmstate state)
            {
                state.needf = false;
                state.needfg = false;
                state.needfgh = false;
                state.needfij = false;
                state.needfi = false;
                state.xupdated = false;
            }

            /*************************************************************************
        Increases lambda, returns False when there is a danger of overflow
        *************************************************************************/
            private static bool increaselambda(ref double lambdav,
                ref double nu)
            {
                bool result = new bool();
                double lnlambda = 0;
                double lnnu = 0;
                double lnlambdaup = 0;
                double lnmax = 0;

                result = false;
                lnlambda = Math.Log(lambdav);
                lnlambdaup = Math.Log(lambdaup);
                lnnu = Math.Log(nu);
                lnmax = Math.Log(math.maxrealnumber);
                if ((double)(lnlambda + lnlambdaup + lnnu) > (double)(0.25 * lnmax))
                {
                    return result;
                }
                if ((double)(lnnu + Math.Log(2)) > (double)(lnmax))
                {
                    return result;
                }
                lambdav = lambdav * lambdaup * nu;
                nu = nu * 2;
                result = true;
                return result;
            }


            /*************************************************************************
            Decreases lambda, but leaves it unchanged when there is danger of underflow.
            *************************************************************************/
            private static void decreaselambda(ref double lambdav,
                ref double nu)
            {
                nu = 1;
                if ((double)(Math.Log(lambdav) + Math.Log(lambdadown)) < (double)(Math.Log(math.minrealnumber)))
                {
                    lambdav = math.minrealnumber;
                }
                else
                {
                    lambdav = lambdav * lambdadown;
                }
            }

            private static double boundedscaledantigradnorm(minlmstate state,
          double[] x,
          double[] g)
            {
                double result = 0;
                int n = 0;
                int i = 0;
                double v = 0;

                result = 0;
                n = state.n;
                for (i = 0; i <= n - 1; i++)
                {
                    v = -(g[i] * state.s[i]);
                    if (state.havebndl[i])
                    {
                        if ((double)(x[i]) <= (double)(state.bndl[i]) && (double)(-g[i]) < (double)(0))
                        {
                            v = 0;
                        }
                    }
                    if (state.havebndu[i])
                    {
                        if ((double)(x[i]) >= (double)(state.bndu[i]) && (double)(-g[i]) > (double)(0))
                        {
                            v = 0;
                        }
                    }
                    result = result + math.sqr(v);
                }
                result = Math.Sqrt(result);
                return result;
            }

        }

        public class optserv
        {
            


            /*************************************************************************
            This function enforces boundary constraints in the X.

            This function correctly (although a bit inefficient) handles BL[i] which
            are -INF and BU[i] which are +INF.

            We have NMain+NSlack  dimensional  X,  with first NMain components bounded
            by BL/BU, and next NSlack ones bounded by non-negativity constraints.

            INPUT PARAMETERS
                X       -   array[NMain+NSlack], point
                BL      -   array[NMain], lower bounds
                            (may contain -INF, when bound is not present)
                HaveBL  -   array[NMain], if HaveBL[i] is False,
                            then i-th bound is not present
                BU      -   array[NMain], upper bounds
                            (may contain +INF, when bound is not present)
                HaveBU  -   array[NMain], if HaveBU[i] is False,
                            then i-th bound is not present

            OUTPUT PARAMETERS
                X       -   X with all constraints being enforced

            It returns True when constraints are consistent,
            False - when constraints are inconsistent.

              -- ALGLIB --
                 Copyright 10.01.2012 by Bochkanov Sergey
            *************************************************************************/
            public static bool enforceboundaryconstraints(ref double[] x,
                double[] bl,
                bool[] havebl,
                double[] bu,
                bool[] havebu,
                int nmain,
                int nslack)
            {
                bool result = new bool();
                int i = 0;

                result = false;
                for (i = 0; i <= nmain - 1; i++)
                {
                    if ((havebl[i] && havebu[i]) && (double)(bl[i]) > (double)(bu[i]))
                    {
                        return result;
                    }
                    if (havebl[i] && (double)(x[i]) < (double)(bl[i]))
                    {
                        x[i] = bl[i];
                    }
                    if (havebu[i] && (double)(x[i]) > (double)(bu[i]))
                    {
                        x[i] = bu[i];
                    }
                }
                for (i = 0; i <= nslack - 1; i++)
                {
                    if ((double)(x[nmain + i]) < (double)(0))
                    {
                        x[nmain + i] = 0;
                    }
                }
                result = true;
                return result;
            }


            

            /*************************************************************************
                This function check, that input derivatives are right. First it scale
            parameters DF0 and DF1 from segment [A;B] to [0;1]. Than it build Hermite
            spline and derivative of it in 0,5. Search scale as Max(DF0,DF1, |F0-F1|).
            Right derivative has to satisfy condition:
                |H-F|/S<=0,01, |H'-F'|/S<=0,01.
            
            INPUT PARAMETERS:
                F0  -   function's value in X-TestStep point;
                DF0 -   derivative's value in X-TestStep point;
                F1  -   function's value in X+TestStep point;
                DF1 -   derivative's value in X+TestStep point;
                F   -   testing function's value;
                DF  -   testing derivative's value;
               Width-   width of verification segment.

            RESULT:
                If input derivatives is right then function returns true, else 
                function returns false.
            
              -- ALGLIB --
                 Copyright 29.05.2012 by Bochkanov Sergey
            *************************************************************************/
            public static bool derivativecheck(double f0,
                double df0,
                double f1,
                double df1,
                double f,
                double df,
                double width)
            {
                bool result = new bool();
                double s = 0;
                double h = 0;
                double dh = 0;

                df = width * df;
                df0 = width * df0;
                df1 = width * df1;
                s = Math.Max(Math.Max(Math.Abs(df0), Math.Abs(df1)), Math.Abs(f1 - f0));
                h = 0.5 * f0 + 0.125 * df0 + 0.5 * f1 - 0.125 * df1;
                dh = -(1.5 * f0) - 0.25 * df0 + 1.5 * f1 - 0.25 * df1;
                if ((double)(s) != (double)(0))
                {
                    if ((double)(Math.Abs(h - f) / s) > (double)(0.001) || (double)(Math.Abs(dh - df) / s) > (double)(0.001))
                    {
                        result = false;
                        return result;
                    }
                }
                else
                {
                    if ((double)(h - f) != (double)(0.0) || (double)(dh - df) != (double)(0.0))
                    {
                        result = false;
                        return result;
                    }
                }
                result = true;
                return result;
            }


        }


        /********************************************************************
        reverse communication structure
        ********************************************************************/
        public class rcommstate : apobject
        {
            public rcommstate()
            {
                init();
            }
            public override void init()
            {
                stage = -1;
                ia = new int[0];
                ba = new bool[0];
                ra = new double[0];
                ca = new alglib.complex[0];
            }
            public override apobject make_copy()
            {
                rcommstate result = new rcommstate();
                result.stage = stage;
                result.ia = (int[])ia.Clone();
                result.ba = (bool[])ba.Clone();
                result.ra = (double[])ra.Clone();
                result.ca = (alglib.complex[])ca.Clone();
                return result;
            }
            public int stage;
            public int[] ia;
            public bool[] ba;
            public double[] ra;
            public alglib.complex[] ca;
        };

        public class minlbfgs
        {
            public class minlbfgsstate : apobject
            {
                public int n;
                public int m;
                public double epsg;
                public double epsf;
                public double epsx;
                public int maxits;
                public bool xrep;
                public double stpmax;
                public double[] s;
                public double diffstep;
                public int nfev;
                public int mcstage;
                public int k;
                public int q;
                public int p;
                public double[] rho;
                public double[,] yk;
                public double[,] sk;
                public double[] theta;
                public double[] d;
                public double stp;
                public double[] work;
                public double fold;
                public double trimthreshold;
                public int prectype;
                public double gammak;
                public double[,] denseh;
                public double[] diagh;
                public double fbase;
                public double fm2;
                public double fm1;
                public double fp1;
                public double fp2;
                public double[] autobuf;
                public double[] x;
                public double f;
                public double[] g;
                public bool needf;
                public bool needfg;
                public bool xupdated;
                public double teststep;
                public rcommstate rstate;
                public int repiterationscount;
                public int repnfev;
                public int repvaridx;
                public int repterminationtype;
                public linmin.linminstate lstate;
                public minlbfgsstate()
                {
                    init();
                }
                public override void init()
                {
                    s = new double[0];
                    rho = new double[0];
                    yk = new double[0, 0];
                    sk = new double[0, 0];
                    theta = new double[0];
                    d = new double[0];
                    work = new double[0];
                    denseh = new double[0, 0];
                    diagh = new double[0];
                    autobuf = new double[0];
                    x = new double[0];
                    g = new double[0];
                    rstate = new rcommstate();
                    lstate = new linmin.linminstate();
                }
                public override alglib.apobject make_copy()
                {
                    minlbfgsstate _result = new minlbfgsstate();
                    _result.n = n;
                    _result.m = m;
                    _result.epsg = epsg;
                    _result.epsf = epsf;
                    _result.epsx = epsx;
                    _result.maxits = maxits;
                    _result.xrep = xrep;
                    _result.stpmax = stpmax;
                    _result.s = (double[])s.Clone();
                    _result.diffstep = diffstep;
                    _result.nfev = nfev;
                    _result.mcstage = mcstage;
                    _result.k = k;
                    _result.q = q;
                    _result.p = p;
                    _result.rho = (double[])rho.Clone();
                    _result.yk = (double[,])yk.Clone();
                    _result.sk = (double[,])sk.Clone();
                    _result.theta = (double[])theta.Clone();
                    _result.d = (double[])d.Clone();
                    _result.stp = stp;
                    _result.work = (double[])work.Clone();
                    _result.fold = fold;
                    _result.trimthreshold = trimthreshold;
                    _result.prectype = prectype;
                    _result.gammak = gammak;
                    _result.denseh = (double[,])denseh.Clone();
                    _result.diagh = (double[])diagh.Clone();
                    _result.fbase = fbase;
                    _result.fm2 = fm2;
                    _result.fm1 = fm1;
                    _result.fp1 = fp1;
                    _result.fp2 = fp2;
                    _result.autobuf = (double[])autobuf.Clone();
                    _result.x = (double[])x.Clone();
                    _result.f = f;
                    _result.g = (double[])g.Clone();
                    _result.needf = needf;
                    _result.needfg = needfg;
                    _result.xupdated = xupdated;
                    _result.teststep = teststep;
                    _result.rstate = (rcommstate)rstate.make_copy();
                    _result.repiterationscount = repiterationscount;
                    _result.repnfev = repnfev;
                    _result.repvaridx = repvaridx;
                    _result.repterminationtype = repterminationtype;
                    _result.lstate = (linmin.linminstate)lstate.make_copy();
                    return _result;
                }
            };


            public class minlbfgsreport : apobject
            {
                public int iterationscount;
                public int nfev;
                public int varidx;
                public int terminationtype;
                public minlbfgsreport()
                {
                    init();
                }
                public override void init()
                {
                }
                public override alglib.apobject make_copy()
                {
                    minlbfgsreport _result = new minlbfgsreport();
                    _result.iterationscount = iterationscount;
                    _result.nfev = nfev;
                    _result.varidx = varidx;
                    _result.terminationtype = terminationtype;
                    return _result;
                }
            };




            public const double gtol = 0.4;


            /*************************************************************************
                    LIMITED MEMORY BFGS METHOD FOR LARGE SCALE OPTIMIZATION

            DESCRIPTION:
            The subroutine minimizes function F(x) of N arguments by  using  a  quasi-
            Newton method (LBFGS scheme) which is optimized to use  a  minimum  amount
            of memory.
            The subroutine generates the approximation of an inverse Hessian matrix by
            using information about the last M steps of the algorithm  (instead of N).
            It lessens a required amount of memory from a value  of  order  N^2  to  a
            value of order 2*N*M.


            REQUIREMENTS:
            Algorithm will request following information during its operation:
            * function value F and its gradient G (simultaneously) at given point X


            USAGE:
            1. User initializes algorithm state with MinLBFGSCreate() call
            2. User tunes solver parameters with MinLBFGSSetCond() MinLBFGSSetStpMax()
               and other functions
            3. User calls MinLBFGSOptimize() function which takes algorithm  state and
               pointer (delegate, etc.) to callback function which calculates F/G.
            4. User calls MinLBFGSResults() to get solution
            5. Optionally user may call MinLBFGSRestartFrom() to solve another problem
               with same N/M but another starting point and/or another function.
               MinLBFGSRestartFrom() allows to reuse already initialized structure.


            INPUT PARAMETERS:
                N       -   problem dimension. N>0
                M       -   number of corrections in the BFGS scheme of Hessian
                            approximation update. Recommended value:  3<=M<=7. The smaller
                            value causes worse convergence, the bigger will  not  cause  a
                            considerably better convergence, but will cause a fall in  the
                            performance. M<=N.
                X       -   initial solution approximation, array[0..N-1].


            OUTPUT PARAMETERS:
                State   -   structure which stores algorithm state
            

            NOTES:
            1. you may tune stopping conditions with MinLBFGSSetCond() function
            2. if target function contains exp() or other fast growing functions,  and
               optimization algorithm makes too large steps which leads  to  overflow,
               use MinLBFGSSetStpMax() function to bound algorithm's  steps.  However,
               L-BFGS rarely needs such a tuning.


              -- ALGLIB --
                 Copyright 02.04.2010 by Bochkanov Sergey
            *************************************************************************/
            public static void minlbfgscreate(int n,
                int m,
                double[] x,
                minlbfgsstate state)
            {
                alglib.ap.assert(n >= 1, "MinLBFGSCreate: N<1!");
                alglib.ap.assert(m >= 1, "MinLBFGSCreate: M<1");
                alglib.ap.assert(m <= n, "MinLBFGSCreate: M>N");
                alglib.ap.assert(alglib.ap.len(x) >= n, "MinLBFGSCreate: Length(X)<N!");
                alglib.ap.assert(apserv.isfinitevector(x, n), "MinLBFGSCreate: X contains infinite or NaN values!");
                minlbfgscreatex(n, m, x, 0, 0.0, state);
            }


         


            /*************************************************************************
            This function sets stopping conditions for L-BFGS optimization algorithm.

            INPUT PARAMETERS:
                State   -   structure which stores algorithm state
                EpsG    -   >=0
                            The  subroutine  finishes  its  work   if   the  condition
                            |v|<EpsG is satisfied, where:
                            * |.| means Euclidian norm
                            * v - scaled gradient vector, v[i]=g[i]*s[i]
                            * g - gradient
                            * s - scaling coefficients set by MinLBFGSSetScale()
                EpsF    -   >=0
                            The  subroutine  finishes  its work if on k+1-th iteration
                            the  condition  |F(k+1)-F(k)|<=EpsF*max{|F(k)|,|F(k+1)|,1}
                            is satisfied.
                EpsX    -   >=0
                            The subroutine finishes its work if  on  k+1-th  iteration
                            the condition |v|<=EpsX is fulfilled, where:
                            * |.| means Euclidian norm
                            * v - scaled step vector, v[i]=dx[i]/s[i]
                            * dx - ste pvector, dx=X(k+1)-X(k)
                            * s - scaling coefficients set by MinLBFGSSetScale()
                MaxIts  -   maximum number of iterations. If MaxIts=0, the  number  of
                            iterations is unlimited.

            Passing EpsG=0, EpsF=0, EpsX=0 and MaxIts=0 (simultaneously) will lead to
            automatic stopping criterion selection (small EpsX).

              -- ALGLIB --
                 Copyright 02.04.2010 by Bochkanov Sergey
            *************************************************************************/
            public static void minlbfgssetcond(minlbfgsstate state,
                double epsg,
                double epsf,
                double epsx,
                int maxits)
            {
                alglib.ap.assert(math.isfinite(epsg), "MinLBFGSSetCond: EpsG is not finite number!");
                alglib.ap.assert((double)(epsg) >= (double)(0), "MinLBFGSSetCond: negative EpsG!");
                alglib.ap.assert(math.isfinite(epsf), "MinLBFGSSetCond: EpsF is not finite number!");
                alglib.ap.assert((double)(epsf) >= (double)(0), "MinLBFGSSetCond: negative EpsF!");
                alglib.ap.assert(math.isfinite(epsx), "MinLBFGSSetCond: EpsX is not finite number!");
                alglib.ap.assert((double)(epsx) >= (double)(0), "MinLBFGSSetCond: negative EpsX!");
                alglib.ap.assert(maxits >= 0, "MinLBFGSSetCond: negative MaxIts!");
                if ((((double)(epsg) == (double)(0) && (double)(epsf) == (double)(0)) && (double)(epsx) == (double)(0)) && maxits == 0)
                {
                    epsx = 1.0E-6;
                }
                state.epsg = epsg;
                state.epsf = epsf;
                state.epsx = epsx;
                state.maxits = maxits;
            }


            /*************************************************************************
            This function turns on/off reporting.

            INPUT PARAMETERS:
                State   -   structure which stores algorithm state
                NeedXRep-   whether iteration reports are needed or not

            If NeedXRep is True, algorithm will call rep() callback function if  it is
            provided to MinLBFGSOptimize().


              -- ALGLIB --
                 Copyright 02.04.2010 by Bochkanov Sergey
            *************************************************************************/
            public static void minlbfgssetxrep(minlbfgsstate state,
                bool needxrep)
            {
                state.xrep = needxrep;
            }


            /*************************************************************************
            This function sets maximum step length

            INPUT PARAMETERS:
                State   -   structure which stores algorithm state
                StpMax  -   maximum step length, >=0. Set StpMax to 0.0 (default),  if
                            you don't want to limit step length.

            Use this subroutine when you optimize target function which contains exp()
            or  other  fast  growing  functions,  and optimization algorithm makes too
            large  steps  which  leads  to overflow. This function allows us to reject
            steps  that  are  too  large  (and  therefore  expose  us  to the possible
            overflow) without actually calculating function value at the x+stp*d.

              -- ALGLIB --
                 Copyright 02.04.2010 by Bochkanov Sergey
            *************************************************************************/
            public static void minlbfgssetstpmax(minlbfgsstate state,
                double stpmax)
            {
                alglib.ap.assert(math.isfinite(stpmax), "MinLBFGSSetStpMax: StpMax is not finite!");
                alglib.ap.assert((double)(stpmax) >= (double)(0), "MinLBFGSSetStpMax: StpMax<0!");
                state.stpmax = stpmax;
            }


           

            /*************************************************************************
            Extended subroutine for internal use only.

            Accepts additional parameters:

                Flags - additional settings:
                        * Flags = 0     means no additional settings
                        * Flags = 1     "do not allocate memory". used when solving
                                        a many subsequent tasks with  same N/M  values.
                                        First  call MUST  be without this flag bit set,
                                        subsequent  calls   of   MinLBFGS   with   same
                                        MinLBFGSState structure can set Flags to 1.
                DiffStep - numerical differentiation step

              -- ALGLIB --
                 Copyright 02.04.2010 by Bochkanov Sergey
            *************************************************************************/
            public static void minlbfgscreatex(int n,
                int m,
                double[] x,
                int flags,
                double diffstep,
                minlbfgsstate state)
            {
                bool allocatemem = new bool();
                int i = 0;

                alglib.ap.assert(n >= 1, "MinLBFGS: N too small!");
                alglib.ap.assert(m >= 1, "MinLBFGS: M too small!");
                alglib.ap.assert(m <= n, "MinLBFGS: M too large!");

                //
                // Initialize
                //
                state.teststep = 0;
                state.diffstep = diffstep;
                state.n = n;
                state.m = m;
                allocatemem = flags % 2 == 0;
                flags = flags / 2;
                if (allocatemem)
                {
                    state.rho = new double[m];
                    state.theta = new double[m];
                    state.yk = new double[m, n];
                    state.sk = new double[m, n];
                    state.d = new double[n];
                    state.x = new double[n];
                    state.s = new double[n];
                    state.g = new double[n];
                    state.work = new double[n];
                }
                minlbfgssetcond(state, 0, 0, 0, 0);
                minlbfgssetxrep(state, false);
                minlbfgssetstpmax(state, 0);
                minlbfgsrestartfrom(state, x);
                for (i = 0; i <= n - 1; i++)
                {
                    state.s[i] = 1.0;
                }
                state.prectype = 0;
            }


           

            /*************************************************************************
            This  subroutine restarts LBFGS algorithm from new point. All optimization
            parameters are left unchanged.

            This  function  allows  to  solve multiple  optimization  problems  (which
            must have same number of dimensions) without object reallocation penalty.

            INPUT PARAMETERS:
                State   -   structure used to store algorithm state
                X       -   new starting point.

              -- ALGLIB --
                 Copyright 30.07.2010 by Bochkanov Sergey
            *************************************************************************/
            public static void minlbfgsrestartfrom(minlbfgsstate state,
                double[] x)
            {
                int i_ = 0;

                alglib.ap.assert(alglib.ap.len(x) >= state.n, "MinLBFGSRestartFrom: Length(X)<N!");
                alglib.ap.assert(apserv.isfinitevector(x, state.n), "MinLBFGSRestartFrom: X contains infinite or NaN values!");
                for (i_ = 0; i_ <= state.n - 1; i_++)
                {
                    state.x[i_] = x[i_];
                }
                state.rstate.ia = new int[5 + 1];
                state.rstate.ra = new double[1 + 1];
                state.rstate.stage = -1;
                clearrequestfields(state);
            }


           

            /*************************************************************************
            Clears request fileds (to be sure that we don't forgot to clear something)
            *************************************************************************/
            private static void clearrequestfields(minlbfgsstate state)
            {
                state.needf = false;
                state.needfg = false;
                state.xupdated = false;
            }


        }

        public class minqp
        {
            /*************************************************************************
            This object stores nonlinear optimizer state.
            You should use functions provided by MinQP subpackage to work with this
            object
            *************************************************************************/
            public class minqpstate : apobject
            {
                public int n;
                public int algokind;
                public int akind;
                public cqmodels.convexquadraticmodel a;
                public sparse.sparsematrix sparsea;
                public bool sparseaupper;
                public double anorm;
                public double[] b;
                public double[] bndl;
                public double[] bndu;
                public double[] s;
                public bool[] havebndl;
                public bool[] havebndu;
                public double[] xorigin;
                public double[] startx;
                public bool havex;
                public double[,] cleic;
                public int nec;
                public int nic;
                public double bleicepsg;
                public double bleicepsf;
                public double bleicepsx;
                public int bleicmaxits;
                public sactivesets.sactiveset sas;
                public double[] gc;
                public double[] xn;
                public double[] pg;
                public double[] workbndl;
                public double[] workbndu;
                public double[,] workcleic;
                public double[] xs;
                public int repinneriterationscount;
                public int repouteriterationscount;
                public int repncholesky;
                public int repnmv;
                public int repterminationtype;
                public double debugphase1flops;
                public double debugphase2flops;
                public double debugphase3flops;
                public double[] tmp0;
                public double[] tmp1;
                public bool[] tmpb;
                public double[] rctmpg;
                public int[] tmpi;
                public normestimator.normestimatorstate estimator;
                public minbleic.minbleicstate solver;
                public minbleic.minbleicreport solverrep;
                public minqpstate()
                {
                    init();
                }
                public override void init()
                {
                    a = new cqmodels.convexquadraticmodel();
                    sparsea = new sparse.sparsematrix();
                    b = new double[0];
                    bndl = new double[0];
                    bndu = new double[0];
                    s = new double[0];
                    havebndl = new bool[0];
                    havebndu = new bool[0];
                    xorigin = new double[0];
                    startx = new double[0];
                    cleic = new double[0, 0];
                    sas = new sactivesets.sactiveset();
                    gc = new double[0];
                    xn = new double[0];
                    pg = new double[0];
                    workbndl = new double[0];
                    workbndu = new double[0];
                    workcleic = new double[0, 0];
                    xs = new double[0];
                    tmp0 = new double[0];
                    tmp1 = new double[0];
                    tmpb = new bool[0];
                    rctmpg = new double[0];
                    tmpi = new int[0];
                    estimator = new normestimator.normestimatorstate();
                    solver = new minbleic.minbleicstate();
                    solverrep = new minbleic.minbleicreport();
                }
                public override alglib.apobject make_copy()
                {
                    minqpstate _result = new minqpstate();
                    _result.n = n;
                    _result.algokind = algokind;
                    _result.akind = akind;
                    _result.a = (cqmodels.convexquadraticmodel)a.make_copy();
                    _result.sparsea = (sparse.sparsematrix)sparsea.make_copy();
                    _result.sparseaupper = sparseaupper;
                    _result.anorm = anorm;
                    _result.b = (double[])b.Clone();
                    _result.bndl = (double[])bndl.Clone();
                    _result.bndu = (double[])bndu.Clone();
                    _result.s = (double[])s.Clone();
                    _result.havebndl = (bool[])havebndl.Clone();
                    _result.havebndu = (bool[])havebndu.Clone();
                    _result.xorigin = (double[])xorigin.Clone();
                    _result.startx = (double[])startx.Clone();
                    _result.havex = havex;
                    _result.cleic = (double[,])cleic.Clone();
                    _result.nec = nec;
                    _result.nic = nic;
                    _result.bleicepsg = bleicepsg;
                    _result.bleicepsf = bleicepsf;
                    _result.bleicepsx = bleicepsx;
                    _result.bleicmaxits = bleicmaxits;
                    _result.sas = (sactivesets.sactiveset)sas.make_copy();
                    _result.gc = (double[])gc.Clone();
                    _result.xn = (double[])xn.Clone();
                    _result.pg = (double[])pg.Clone();
                    _result.workbndl = (double[])workbndl.Clone();
                    _result.workbndu = (double[])workbndu.Clone();
                    _result.workcleic = (double[,])workcleic.Clone();
                    _result.xs = (double[])xs.Clone();
                    _result.repinneriterationscount = repinneriterationscount;
                    _result.repouteriterationscount = repouteriterationscount;
                    _result.repncholesky = repncholesky;
                    _result.repnmv = repnmv;
                    _result.repterminationtype = repterminationtype;
                    _result.debugphase1flops = debugphase1flops;
                    _result.debugphase2flops = debugphase2flops;
                    _result.debugphase3flops = debugphase3flops;
                    _result.tmp0 = (double[])tmp0.Clone();
                    _result.tmp1 = (double[])tmp1.Clone();
                    _result.tmpb = (bool[])tmpb.Clone();
                    _result.rctmpg = (double[])rctmpg.Clone();
                    _result.tmpi = (int[])tmpi.Clone();
                    _result.estimator = (normestimator.normestimatorstate)estimator.make_copy();
                    _result.solver = (minbleic.minbleicstate)solver.make_copy();
                    _result.solverrep = (minbleic.minbleicreport)solverrep.make_copy();
                    return _result;
                }
            };


            /*************************************************************************
            This structure stores optimization report:
            * InnerIterationsCount      number of inner iterations
            * OuterIterationsCount      number of outer iterations
            * NCholesky                 number of Cholesky decomposition
            * NMV                       number of matrix-vector products
                                        (only products calculated as part of iterative
                                        process are counted)
            * TerminationType           completion code (see below)

            Completion codes:
            * -5    inappropriate solver was used:
                    * Cholesky solver for semidefinite or indefinite problems
                    * Cholesky solver for problems with non-boundary constraints
            * -4    BLEIC-QP algorithm found unconstrained direction
                    of negative curvature (function is unbounded from
                    below  even  under  constraints),  no  meaningful
                    minimum can be found.
            * -3    inconsistent constraints (or, maybe, feasible point is
                    too hard to find). If you are sure that constraints are feasible,
                    try to restart optimizer with better initial approximation.
            * -1    solver error
            *  4    successful completion
            *  5    MaxIts steps was taken
            *  7    stopping conditions are too stringent,
                    further improvement is impossible,
                    X contains best point found so far.
            *************************************************************************/
            public class minqpreport : apobject
            {
                public int inneriterationscount;
                public int outeriterationscount;
                public int nmv;
                public int ncholesky;
                public int terminationtype;
                public minqpreport()
                {
                    init();
                }
                public override void init()
                {
                }
                public override alglib.apobject make_copy()
                {
                    minqpreport _result = new minqpreport();
                    _result.inneriterationscount = inneriterationscount;
                    _result.outeriterationscount = outeriterationscount;
                    _result.nmv = nmv;
                    _result.ncholesky = ncholesky;
                    _result.terminationtype = terminationtype;
                    return _result;
                }
            };




            public const int maxlagrangeits = 10;
            public const int maxbadnewtonits = 7;
            public const double penaltyfactor = 100.0;


            /*************************************************************************
                                CONSTRAINED QUADRATIC PROGRAMMING

            The subroutine creates QP optimizer. After initial creation,  it  contains
            default optimization problem with zero quadratic and linear terms  and  no
            constraints. You should set quadratic/linear terms with calls to functions
            provided by MinQP subpackage.

            INPUT PARAMETERS:
                N       -   problem size
            
            OUTPUT PARAMETERS:
                State   -   optimizer with zero quadratic/linear terms
                            and no constraints

              -- ALGLIB --
                 Copyright 11.01.2011 by Bochkanov Sergey
            *************************************************************************/
            public static void minqpcreate(int n,
                minqpstate state)
            {
                int i = 0;

                alglib.ap.assert(n >= 1, "MinQPCreate: N<1");

                //
                // initialize QP solver
                //
                state.n = n;
                state.nec = 0;
                state.nic = 0;
                state.repterminationtype = 0;
                state.anorm = 1;
                state.akind = 0;
                cqmodels.cqminit(n, state.a);
                sactivesets.sasinit(n, state.sas);
                state.b = new double[n];
                state.bndl = new double[n];
                state.bndu = new double[n];
                state.workbndl = new double[n];
                state.workbndu = new double[n];
                state.havebndl = new bool[n];
                state.havebndu = new bool[n];
                state.s = new double[n];
                state.startx = new double[n];
                state.xorigin = new double[n];
                state.xs = new double[n];
                state.xn = new double[n];
                state.gc = new double[n];
                state.pg = new double[n];
                for (i = 0; i <= n - 1; i++)
                {
                    state.bndl[i] = Double.NegativeInfinity;
                    state.bndu[i] = Double.PositiveInfinity;
                    state.havebndl[i] = false;
                    state.havebndu[i] = false;
                    state.b[i] = 0.0;
                    state.startx[i] = 0.0;
                    state.xorigin[i] = 0.0;
                    state.s[i] = 1.0;
                }
                state.havex = false;
                minqpsetalgocholesky(state);
                normestimator.normestimatorcreate(n, n, 5, 5, state.estimator);
                minbleic.minbleiccreate(n, state.startx, state.solver);
            }

            /*************************************************************************
            This function tells solver to use Cholesky-based algorithm. This algorithm
            is active by default.

            DESCRIPTION:

            Cholesky-based algorithm can be used only for problems which:
            * have dense quadratic term, set  by  MinQPSetQuadraticTerm(),  sparse  or
              structured problems are not supported.
            * are strictly convex, i.e. quadratic term is symmetric positive definite,
              indefinite or semidefinite problems are not supported by this algorithm.

            If anything of what listed above is violated, you may use  BLEIC-based  QP
            algorithm which can be activated by MinQPSetAlgoBLEIC().

            BENEFITS AND DRAWBACKS:

            This  algorithm  gives  best  precision amongst all QP solvers provided by
            ALGLIB (Newton iterations  have  much  higher  precision  than  any  other
            optimization algorithm). This solver also gracefully handles problems with
            very large amount of constraints.

            Performance of the algorithm is good because internally  it  uses  Level 3
            Dense BLAS for its performance-critical parts.


            From the other side, algorithm has  O(N^3)  complexity  for  unconstrained
            problems and up to orders of  magnitude  slower  on  constrained  problems
            (these additional iterations are needed to identify  active  constraints).
            So, its running time depends on number of constraints active  at solution.

            Furthermore, this algorithm can not solve problems with sparse matrices or
            problems with semidefinite/indefinite matrices of any kind (dense/sparse).

            INPUT PARAMETERS:
                State   -   structure which stores algorithm state

              -- ALGLIB --
                 Copyright 11.01.2011 by Bochkanov Sergey
            *************************************************************************/
            public static void minqpsetalgocholesky(minqpstate state)
            {
                state.algokind = 1;
            }





            /*************************************************************************
            This function sets boundary constraints for QP solver

            Boundary constraints are inactive by default (after initial creation).
            After  being  set,  they  are  preserved  until explicitly turned off with
            another SetBC() call.

            INPUT PARAMETERS:
                State   -   structure stores algorithm state
                BndL    -   lower bounds, array[N].
                            If some (all) variables are unbounded, you may specify
                            very small number or -INF (latter is recommended because
                            it will allow solver to use better algorithm).
                BndU    -   upper bounds, array[N].
                            If some (all) variables are unbounded, you may specify
                            very large number or +INF (latter is recommended because
                            it will allow solver to use better algorithm).
                        
            NOTE: it is possible to specify BndL[i]=BndU[i]. In this case I-th
            variable will be "frozen" at X[i]=BndL[i]=BndU[i].

              -- ALGLIB --
                 Copyright 11.01.2011 by Bochkanov Sergey
            *************************************************************************/
            public static void minqpsetbc(minqpstate state,
                double[] bndl,
                double[] bndu)
            {
                int i = 0;
                int n = 0;

                n = state.n;
                alglib.ap.assert(alglib.ap.len(bndl) >= n, "MinQPSetBC: Length(BndL)<N");
                alglib.ap.assert(alglib.ap.len(bndu) >= n, "MinQPSetBC: Length(BndU)<N");
                for (i = 0; i <= n - 1; i++)
                {
                    alglib.ap.assert(math.isfinite(bndl[i]) || Double.IsNegativeInfinity(bndl[i]), "MinQPSetBC: BndL contains NAN or +INF");
                    alglib.ap.assert(math.isfinite(bndu[i]) || Double.IsPositiveInfinity(bndu[i]), "MinQPSetBC: BndU contains NAN or -INF");
                    state.bndl[i] = bndl[i];
                    state.havebndl[i] = math.isfinite(bndl[i]);
                    state.bndu[i] = bndu[i];
                    state.havebndu[i] = math.isfinite(bndu[i]);
                }
            }





            /*************************************************************************
            This function solves quadratic programming problem.
            You should call it after setting solver options with MinQPSet...() calls.

            INPUT PARAMETERS:
                State   -   algorithm state

            You should use MinQPResults() function to access results after calls
            to this function.

              -- ALGLIB --
                 Copyright 11.01.2011 by Bochkanov Sergey.
                 Special thanks to Elvira Illarionova  for  important  suggestions  on
                 the linearly constrained QP algorithm.
            *************************************************************************/
            public static void minqpoptimize(minqpstate state)
            {
                int n = 0;
                int i = 0;
                int nbc = 0;
                double v0 = 0;
                double v1 = 0;
                double v = 0;
                double d2 = 0;
                double d1 = 0;
                double d0 = 0;
                double noisetolerance = 0;
                double fprev = 0;
                double fcand = 0;
                double fcur = 0;
                int nextaction = 0;
                int actstatus = 0;
                double noiselevel = 0;
                int badnewtonits = 0;
                double maxscaledgrad = 0;
                int i_ = 0;

                noisetolerance = 10;
                n = state.n;
                state.repterminationtype = -5;
                state.repinneriterationscount = 0;
                state.repouteriterationscount = 0;
                state.repncholesky = 0;
                state.repnmv = 0;
                state.debugphase1flops = 0;
                state.debugphase2flops = 0;
                state.debugphase3flops = 0;
                apserv.rvectorsetlengthatleast(ref state.rctmpg, n);

                //
                // check correctness of constraints
                //
                for (i = 0; i <= n - 1; i++)
                {
                    if (state.havebndl[i] && state.havebndu[i])
                    {
                        if ((double)(state.bndl[i]) > (double)(state.bndu[i]))
                        {
                            state.repterminationtype = -3;
                            return;
                        }
                    }
                }

                //
                // count number of bound and linear constraints
                //
                nbc = 0;
                for (i = 0; i <= n - 1; i++)
                {
                    if (state.havebndl[i])
                    {
                        nbc = nbc + 1;
                    }
                    if (state.havebndu[i])
                    {
                        nbc = nbc + 1;
                    }
                }

                //
                // Initial point:
                // * if we have starting point in StartX, we just have to bound it
                // * if we do not have StartX, deduce initial point from boundary constraints
                //
                if (state.havex)
                {
                    for (i = 0; i <= n - 1; i++)
                    {
                        state.xs[i] = state.startx[i];
                        if (state.havebndl[i] && (double)(state.xs[i]) < (double)(state.bndl[i]))
                        {
                            state.xs[i] = state.bndl[i];
                        }
                        if (state.havebndu[i] && (double)(state.xs[i]) > (double)(state.bndu[i]))
                        {
                            state.xs[i] = state.bndu[i];
                        }
                    }
                }
                else
                {
                    for (i = 0; i <= n - 1; i++)
                    {
                        if (state.havebndl[i] && state.havebndu[i])
                        {
                            state.xs[i] = 0.5 * (state.bndl[i] + state.bndu[i]);
                            continue;
                        }
                        if (state.havebndl[i])
                        {
                            state.xs[i] = state.bndl[i];
                            continue;
                        }
                        if (state.havebndu[i])
                        {
                            state.xs[i] = state.bndu[i];
                            continue;
                        }
                        state.xs[i] = 0;
                    }
                }

                //
                // Cholesky solver.
                //
                if (state.algokind == 1)
                {

                    //
                    // Check matrix type.
                    // Cholesky solver supports only dense matrices.
                    //
                    if (state.akind != 0)
                    {
                        state.repterminationtype = -5;
                        return;
                    }

                    //
                    // Our formulation of quadratic problem includes origin point,
                    // i.e. we have F(x-x_origin) which is minimized subject to
                    // constraints on x, instead of having simply F(x).
                    //
                    // Here we make transition from non-zero origin to zero one.
                    // In order to make such transition we have to:
                    // 1. subtract x_origin from x_start
                    // 2. modify constraints
                    // 3. solve problem
                    // 4. add x_origin to solution
                    //
                    // There is alternate solution - to modify quadratic function
                    // by expansion of multipliers containing (x-x_origin), but
                    // we prefer to modify constraints, because it is a) more precise
                    // and b) easier to to.
                    //
                    // Parts (1)-(2) are done here. After this block is over,
                    // we have:
                    // * XS, which stores shifted XStart (if we don't have XStart,
                    //   value of XS will be ignored later)
                    // * WorkBndL, WorkBndU, which store modified boundary constraints.
                    //
                    for (i = 0; i <= n - 1; i++)
                    {
                        if (state.havebndl[i])
                        {
                            state.workbndl[i] = state.bndl[i] - state.xorigin[i];
                        }
                        else
                        {
                            state.workbndl[i] = Double.NegativeInfinity;
                        }
                        if (state.havebndu[i])
                        {
                            state.workbndu[i] = state.bndu[i] - state.xorigin[i];
                        }
                        else
                        {
                            state.workbndu[i] = Double.PositiveInfinity;
                        }
                    }
                    apserv.rmatrixsetlengthatleast(ref state.workcleic, state.nec + state.nic, n + 1);
                    for (i = 0; i <= state.nec + state.nic - 1; i++)
                    {
                        v = 0.0;
                        for (i_ = 0; i_ <= n - 1; i_++)
                        {
                            v += state.cleic[i, i_] * state.xorigin[i_];
                        }
                        for (i_ = 0; i_ <= n - 1; i_++)
                        {
                            state.workcleic[i, i_] = state.cleic[i, i_];
                        }
                        state.workcleic[i, n] = state.cleic[i, n] - v;
                    }

                    //
                    // Starting point XS
                    //
                    if (state.havex)
                    {

                        //
                        // We have starting point in StartX, so we just have to shift and bound it
                        //
                        for (i = 0; i <= n - 1; i++)
                        {
                            state.xs[i] = state.startx[i] - state.xorigin[i];
                            if (state.havebndl[i])
                            {
                                if ((double)(state.xs[i]) < (double)(state.workbndl[i]))
                                {
                                    state.xs[i] = state.workbndl[i];
                                }
                            }
                            if (state.havebndu[i])
                            {
                                if ((double)(state.xs[i]) > (double)(state.workbndu[i]))
                                {
                                    state.xs[i] = state.workbndu[i];
                                }
                            }
                        }
                    }
                    else
                    {

                        //
                        // We don't have starting point, so we deduce it from
                        // constraints (if they are present).
                        //
                        // NOTE: XS contains some meaningless values from previous block
                        // which are ignored by code below.
                        //
                        for (i = 0; i <= n - 1; i++)
                        {
                            if (state.havebndl[i] && state.havebndu[i])
                            {
                                state.xs[i] = 0.5 * (state.workbndl[i] + state.workbndu[i]);
                                if ((double)(state.xs[i]) < (double)(state.workbndl[i]))
                                {
                                    state.xs[i] = state.workbndl[i];
                                }
                                if ((double)(state.xs[i]) > (double)(state.workbndu[i]))
                                {
                                    state.xs[i] = state.workbndu[i];
                                }
                                continue;
                            }
                            if (state.havebndl[i])
                            {
                                state.xs[i] = state.workbndl[i];
                                continue;
                            }
                            if (state.havebndu[i])
                            {
                                state.xs[i] = state.workbndu[i];
                                continue;
                            }
                            state.xs[i] = 0;
                        }
                    }

                    //
                    // Handle special case - no constraints
                    //
                    if (nbc == 0 && state.nec + state.nic == 0)
                    {

                        //
                        // "Simple" unconstrained Cholesky
                        //
                        apserv.bvectorsetlengthatleast(ref state.tmpb, n);
                        for (i = 0; i <= n - 1; i++)
                        {
                            state.tmpb[i] = false;
                        }
                        state.repncholesky = state.repncholesky + 1;
                        cqmodels.cqmsetb(state.a, state.b);
                        cqmodels.cqmsetactiveset(state.a, state.xs, state.tmpb);
                        if (!cqmodels.cqmconstrainedoptimum(state.a, ref state.xn))
                        {
                            state.repterminationtype = -5;
                            return;
                        }
                        for (i_ = 0; i_ <= n - 1; i_++)
                        {
                            state.xs[i_] = state.xn[i_];
                        }
                        for (i_ = 0; i_ <= n - 1; i_++)
                        {
                            state.xs[i_] = state.xs[i_] + state.xorigin[i_];
                        }
                        state.repinneriterationscount = 1;
                        state.repouteriterationscount = 1;
                        state.repterminationtype = 4;
                        return;
                    }

                    //
                    // Prepare "active set" structure
                    //
                    sactivesets.sassetbc(state.sas, state.workbndl, state.workbndu);
                    sactivesets.sassetlcx(state.sas, state.workcleic, state.nec, state.nic);
                    sactivesets.sassetscale(state.sas, state.s);
                    if (!sactivesets.sasstartoptimization(state.sas, state.xs))
                    {
                        state.repterminationtype = -3;
                        return;
                    }

                    //
                    // Main cycle of CQP algorithm
                    //
                    state.repterminationtype = 4;
                    badnewtonits = 0;
                    maxscaledgrad = 0.0;
                    while (true)
                    {

                        //
                        // Update iterations count
                        //
                        apserv.inc(ref state.repouteriterationscount);
                        apserv.inc(ref state.repinneriterationscount);

                        //
                        // Phase 1.
                        //
                        // Determine active set.
                        // Update MaxScaledGrad.
                        //
                        cqmodels.cqmadx(state.a, state.sas.xc, ref state.rctmpg);
                        for (i_ = 0; i_ <= n - 1; i_++)
                        {
                            state.rctmpg[i_] = state.rctmpg[i_] + state.b[i_];
                        }
                        sactivesets.sasreactivateconstraints(state.sas, state.rctmpg);
                        v = 0.0;
                        for (i = 0; i <= n - 1; i++)
                        {
                            v = v + math.sqr(state.rctmpg[i] * state.s[i]);
                        }
                        maxscaledgrad = Math.Max(maxscaledgrad, Math.Sqrt(v));

                        //
                        // Phase 2: perform penalized steepest descent step.
                        //
                        // NextAction control variable is set on exit from this loop:
                        // * NextAction>0 in case we have to proceed to Phase 3 (Newton step)
                        // * NextAction<0 in case we have to proceed to Phase 1 (recalculate active set)
                        // * NextAction=0 in case we found solution (step along projected gradient is small enough)
                        //
                        while (true)
                        {

                            //
                            // Calculate constrained descent direction, store to PG.
                            // Successful termination if PG is zero.
                            //
                            cqmodels.cqmadx(state.a, state.sas.xc, ref state.gc);
                            for (i_ = 0; i_ <= n - 1; i_++)
                            {
                                state.gc[i_] = state.gc[i_] + state.b[i_];
                            }
                            sactivesets.sasconstraineddescent(state.sas, state.gc, ref state.pg);
                            state.debugphase2flops = state.debugphase2flops + 4 * (state.nec + state.nic) * n;
                            v0 = 0.0;
                            for (i_ = 0; i_ <= n - 1; i_++)
                            {
                                v0 += state.pg[i_] * state.pg[i_];
                            }
                            if ((double)(v0) == (double)(0))
                            {

                                //
                                // Constrained derivative is zero.
                                // Solution found.
                                //
                                nextaction = 0;
                                break;
                            }

                            //
                            // Build quadratic model of F along descent direction:
                            //     F(xc+alpha*pg) = D2*alpha^2 + D1*alpha + D0
                            // Store noise level in the XC (noise level is used to classify
                            // step as singificant or insignificant).
                            //
                            // In case function curvature is negative or product of descent
                            // direction and gradient is non-negative, iterations are terminated.
                            //
                            // NOTE: D0 is not actually used, but we prefer to maintain it.
                            //
                            fprev = minqpmodelvalue(state.a, state.b, state.sas.xc, n, ref state.tmp0);
                            fprev = fprev + penaltyfactor * maxscaledgrad * sactivesets.sasactivelcpenalty1(state.sas, state.sas.xc);
                            cqmodels.cqmevalx(state.a, state.sas.xc, ref v, ref noiselevel);
                            v0 = cqmodels.cqmxtadx2(state.a, state.pg);
                            state.debugphase2flops = state.debugphase2flops + 3 * 2 * n * n;
                            d2 = v0;
                            v1 = 0.0;
                            for (i_ = 0; i_ <= n - 1; i_++)
                            {
                                v1 += state.pg[i_] * state.gc[i_];
                            }
                            d1 = v1;
                            d0 = fprev;
                            if ((double)(d2) <= (double)(0))
                            {

                                //
                                // Second derivative is non-positive, function is non-convex.
                                //
                                state.repterminationtype = -5;
                                nextaction = 0;
                                break;
                            }
                            if ((double)(d1) >= (double)(0))
                            {

                                //
                                // Second derivative is positive, first derivative is non-negative.
                                // Solution found.
                                //
                                nextaction = 0;
                                break;
                            }

                            //
                            // Modify quadratic model - add penalty for violation of the active
                            // constraints.
                            //
                            // Boundary constraints are always satisfied exactly, so we do not
                            // add penalty term for them. General equality constraint of the
                            // form a'*(xc+alpha*d)=b adds penalty term:
                            //     P(alpha) = (a'*(xc+alpha*d)-b)^2
                            //              = (alpha*(a'*d) + (a'*xc-b))^2
                            //              = alpha^2*(a'*d)^2 + alpha*2*(a'*d)*(a'*xc-b) + (a'*xc-b)^2
                            // Each penalty term is multiplied by 100*Anorm before adding it to
                            // the 1-dimensional quadratic model.
                            //
                            // Penalization of the quadratic model improves behavior of the
                            // algorithm in the presense of the multiple degenerate constraints.
                            // In particular, it prevents algorithm from making large steps in
                            // directions which violate equality constraints.
                            //
                            for (i = 0; i <= state.nec + state.nic - 1; i++)
                            {
                                if (state.sas.activeset[n + i] > 0)
                                {
                                    v0 = 0.0;
                                    for (i_ = 0; i_ <= n - 1; i_++)
                                    {
                                        v0 += state.workcleic[i, i_] * state.pg[i_];
                                    }
                                    v1 = 0.0;
                                    for (i_ = 0; i_ <= n - 1; i_++)
                                    {
                                        v1 += state.workcleic[i, i_] * state.sas.xc[i_];
                                    }
                                    v1 = v1 - state.workcleic[i, n];
                                    v = 100 * state.anorm;
                                    d2 = d2 + v * math.sqr(v0);
                                    d1 = d1 + v * 2 * v0 * v1;
                                    d0 = d0 + v * math.sqr(v1);
                                }
                            }
                            state.debugphase2flops = state.debugphase2flops + 2 * 2 * (state.nec + state.nic) * n;

                            //
                            // Try unbounded step.
                            // In case function change is dominated by noise or function actually increased
                            // instead of decreasing, we terminate iterations.
                            //
                            v = -(d1 / (2 * d2));
                            for (i_ = 0; i_ <= n - 1; i_++)
                            {
                                state.xn[i_] = state.sas.xc[i_];
                            }
                            for (i_ = 0; i_ <= n - 1; i_++)
                            {
                                state.xn[i_] = state.xn[i_] + v * state.pg[i_];
                            }
                            fcand = minqpmodelvalue(state.a, state.b, state.xn, n, ref state.tmp0);
                            fcand = fcand + penaltyfactor * maxscaledgrad * sactivesets.sasactivelcpenalty1(state.sas, state.xn);
                            state.debugphase2flops = state.debugphase2flops + 2 * n * n;
                            if ((double)(fcand) >= (double)(fprev - noiselevel * noisetolerance))
                            {
                                nextaction = 0;
                                break;
                            }

                            //
                            // Save active set
                            // Perform bounded step with (possible) activation
                            //
                            actstatus = minqpboundedstepandactivation(state, state.xn, ref state.tmp0);
                            fcur = minqpmodelvalue(state.a, state.b, state.sas.xc, n, ref state.tmp0);
                            state.debugphase2flops = state.debugphase2flops + 2 * n * n;

                            //
                            // Depending on results, decide what to do:
                            // 1. In case step was performed without activation of constraints,
                            //    we proceed to Newton method
                            // 2. In case there was activated at least one constraint with ActiveSet[I]<0,
                            //    we proceed to Phase 1 and re-evaluate active set.
                            // 3. Otherwise (activation of the constraints with ActiveSet[I]=0)
                            //    we try Phase 2 one more time.
                            //
                            if (actstatus < 0)
                            {

                                //
                                // Step without activation, proceed to Newton
                                //
                                nextaction = 1;
                                break;
                            }
                            if (actstatus == 0)
                            {

                                //
                                // No new constraints added during last activation - only
                                // ones which were at the boundary (ActiveSet[I]=0), but
                                // inactive due to numerical noise.
                                //
                                // Now, these constraints are added to the active set, and
                                // we try to perform steepest descent (Phase 2) one more time.
                                //
                                continue;
                            }
                            else
                            {

                                //
                                // Last step activated at least one significantly new
                                // constraint (ActiveSet[I]<0), we have to re-evaluate
                                // active set (Phase 1).
                                //
                                nextaction = -1;
                                break;
                            }
                        }
                        if (nextaction < 0)
                        {
                            continue;
                        }
                        if (nextaction == 0)
                        {
                            break;
                        }

                        //
                        // Phase 3: fast equality-constrained solver
                        //
                        // NOTE: this solver uses Augmented Lagrangian algorithm to solve
                        //       equality-constrained subproblems. This algorithm may
                        //       perform steps which increase function values instead of
                        //       decreasing it (in hard cases, like overconstrained problems).
                        //
                        //       Such non-monononic steps may create a loop, when Augmented
                        //       Lagrangian algorithm performs uphill step, and steepest
                        //       descent algorithm (Phase 2) performs downhill step in the
                        //       opposite direction.
                        //
                        //       In order to prevent iterations to continue forever we
                        //       count iterations when AL algorithm increased function
                        //       value instead of decreasing it. When number of such "bad"
                        //       iterations will increase beyong MaxBadNewtonIts, we will
                        //       terminate algorithm.
                        //
                        fprev = minqpmodelvalue(state.a, state.b, state.sas.xc, n, ref state.tmp0);
                        while (true)
                        {

                            //
                            // Calculate optimum subject to presently active constraints
                            //
                            state.repncholesky = state.repncholesky + 1;
                            state.debugphase3flops = state.debugphase3flops + Math.Pow(n, 3) / 3;
                            if (!minqpconstrainedoptimum(state, state.a, state.anorm, state.b, ref state.xn, ref state.tmp0, ref state.tmpb, ref state.tmp1))
                            {
                                state.repterminationtype = -5;
                                sactivesets.sasstopoptimization(state.sas);
                                return;
                            }

                            //
                            // Add constraints.
                            // If no constraints was added, accept candidate point XN and move to next phase.
                            //
                            if (minqpboundedstepandactivation(state, state.xn, ref state.tmp0) < 0)
                            {
                                break;
                            }
                        }
                        fcur = minqpmodelvalue(state.a, state.b, state.sas.xc, n, ref state.tmp0);
                        if ((double)(fcur) >= (double)(fprev))
                        {
                            badnewtonits = badnewtonits + 1;
                        }
                        if (badnewtonits >= maxbadnewtonits)
                        {

                            //
                            // Algorithm found solution, but keeps iterating because Newton
                            // algorithm performs uphill steps (noise in the Augmented Lagrangian
                            // algorithm). We terminate algorithm; it is considered normal
                            // termination.
                            //
                            break;
                        }
                    }
                    sactivesets.sasstopoptimization(state.sas);

                    //
                    // Post-process: add XOrigin to XC
                    //
                    for (i = 0; i <= n - 1; i++)
                    {
                        if (state.havebndl[i] && (double)(state.sas.xc[i]) == (double)(state.workbndl[i]))
                        {
                            state.xs[i] = state.bndl[i];
                            continue;
                        }
                        if (state.havebndu[i] && (double)(state.sas.xc[i]) == (double)(state.workbndu[i]))
                        {
                            state.xs[i] = state.bndu[i];
                            continue;
                        }
                        state.xs[i] = apserv.boundval(state.sas.xc[i] + state.xorigin[i], state.bndl[i], state.bndu[i]);
                    }
                    return;
                }

                //
                // BLEIC solver
                //
                if (state.algokind == 2)
                {
                    alglib.ap.assert(state.akind == 0 || state.akind == 1, "MinQPOptimize: unexpected AKind");
                    apserv.ivectorsetlengthatleast(ref state.tmpi, state.nec + state.nic);
                    apserv.rvectorsetlengthatleast(ref state.tmp0, n);
                    apserv.rvectorsetlengthatleast(ref state.tmp1, n);
                    for (i = 0; i <= state.nec - 1; i++)
                    {
                        state.tmpi[i] = 0;
                    }
                    for (i = 0; i <= state.nic - 1; i++)
                    {
                        state.tmpi[state.nec + i] = -1;
                    }
                    minbleic.minbleicsetlc(state.solver, state.cleic, state.tmpi, state.nec + state.nic);
                    minbleic.minbleicsetbc(state.solver, state.bndl, state.bndu);
                    minbleic.minbleicsetdrep(state.solver, true);
                    minbleic.minbleicsetcond(state.solver, math.minrealnumber, 0.0, 0.0, state.bleicmaxits);
                    minbleic.minbleicsetscale(state.solver, state.s);
                    minbleic.minbleicsetprecscale(state.solver);
                    minbleic.minbleicrestartfrom(state.solver, state.xs);
                    state.repterminationtype = 0;
                    while (minbleic.minbleiciteration(state.solver))
                    {

                        //
                        // Line search started
                        //
                        if (state.solver.lsstart)
                        {

                            //
                            // Iteration counters:
                            // * inner iterations count is increased on every line search
                            // * outer iterations count is increased only at steepest descent line search
                            //
                            apserv.inc(ref state.repinneriterationscount);
                            if (!state.solver.lbfgssearch)
                            {
                                apserv.inc(ref state.repouteriterationscount);
                            }

                            //
                            // Build quadratic model of F along descent direction:
                            //     F(x+alpha*d) = D2*alpha^2 + D1*alpha + D0
                            //
                            d0 = state.solver.f;
                            d1 = 0.0;
                            for (i_ = 0; i_ <= n - 1; i_++)
                            {
                                d1 += state.solver.d[i_] * state.solver.g[i_];
                            }
                            d2 = 0;
                            if (state.akind == 0)
                            {
                                d2 = cqmodels.cqmxtadx2(state.a, state.solver.d);
                            }
                            if (state.akind == 1)
                            {
                                sparse.sparsesmv(state.sparsea, state.sparseaupper, state.solver.d, ref state.tmp0);
                                d2 = 0.0;
                                for (i = 0; i <= n - 1; i++)
                                {
                                    d2 = d2 + state.solver.d[i] * state.tmp0[i];
                                }
                                d2 = 0.5 * d2;
                            }

                            //
                            // Suggest new step
                            //
                            if ((double)(d1) < (double)(0) && (double)(d2) > (double)(0))
                            {
                                state.solver.stp = apserv.safeminposrv(-d1, 2 * d2, state.solver.curstpmax);
                            }

                            //
                            // This line search may be started from steepest descent
                            // stage (stage 2) or from L-BFGS stage (stage 3) of the
                            // BLEIC algorithm. Depending on stage type, different
                            // checks are performed.
                            //
                            // Say, L-BFGS stage is an equality-constrained refinement
                            // stage of BLEIC. This stage refines current iterate
                            // under "frozen" equality constraints. We can terminate
                            // iterations at this stage only when we encounter
                            // unconstrained direction of negative curvature. In all
                            // other cases (say, when constrained gradient is zero)
                            // we should not terminate algorithm because everything may
                            // change after de-activating presently active constraints.
                            //
                            // At steepest descent stage of BLEIC we can terminate algorithm
                            // because it found minimum (steepest descent step is zero
                            // or too short). We also perform check for direction of
                            // negative curvature.
                            //
                            if (((double)(d2) < (double)(0) || ((double)(d2) == (double)(0) && (double)(d1) < (double)(0))) && !state.solver.boundedstep)
                            {

                                //
                                // Function is unbounded from below:
                                // * function will decrease along D, i.e. either:
                                //   * D2<0
                                //   * D2=0 and D1<0
                                // * step is unconstrained
                                //
                                // If these conditions are true, we abnormally terminate QP
                                // algorithm with return code -4 (we can do so at any stage
                                // of BLEIC - whether it is L-BFGS or steepest descent one).
                                //
                                state.repterminationtype = -4;
                                for (i = 0; i <= n - 1; i++)
                                {
                                    state.xs[i] = state.solver.x[i];
                                }
                                break;
                            }
                            if (!state.solver.lbfgssearch && (double)(d2) >= (double)(0))
                            {

                                //
                                // Tests for "normal" convergence.
                                //
                                // These tests are performed only at "steepest descent" stage
                                // of the BLEIC algorithm, and only when function is non-concave
                                // (D2>=0) along direction D.
                                //
                                // NOTE: we do not test iteration count (MaxIts) here, because
                                //       this stopping condition is tested by BLEIC itself.
                                //
                                if ((double)(d1) >= (double)(0))
                                {

                                    //
                                    // "Emergency" stopping condition: D is non-descent direction.
                                    // Sometimes it is possible because of numerical noise in the
                                    // target function.
                                    //
                                    state.repterminationtype = 4;
                                    for (i = 0; i <= n - 1; i++)
                                    {
                                        state.xs[i] = state.solver.x[i];
                                    }
                                    break;
                                }
                                if ((double)(d2) > (double)(0))
                                {

                                    //
                                    // Stopping condition #4 - gradient norm is small:
                                    //
                                    // 1. rescale State.Solver.D and State.Solver.G according to
                                    //    current scaling, store results to Tmp0 and Tmp1.
                                    // 2. Normalize Tmp0 (scaled direction vector).
                                    // 3. compute directional derivative (in scaled variables),
                                    //    which is equal to DOTPRODUCT(Tmp0,Tmp1).
                                    //
                                    v = 0;
                                    for (i = 0; i <= n - 1; i++)
                                    {
                                        state.tmp0[i] = state.solver.d[i] / state.s[i];
                                        state.tmp1[i] = state.solver.g[i] * state.s[i];
                                        v = v + math.sqr(state.tmp0[i]);
                                    }
                                    alglib.ap.assert((double)(v) > (double)(0), "MinQPOptimize: inernal errror (scaled direction is zero)");
                                    v = 1 / Math.Sqrt(v);
                                    for (i_ = 0; i_ <= n - 1; i_++)
                                    {
                                        state.tmp0[i_] = v * state.tmp0[i_];
                                    }
                                    v = 0.0;
                                    for (i_ = 0; i_ <= n - 1; i_++)
                                    {
                                        v += state.tmp0[i_] * state.tmp1[i_];
                                    }
                                    if ((double)(Math.Abs(v)) <= (double)(state.bleicepsg))
                                    {
                                        state.repterminationtype = 4;
                                        for (i = 0; i <= n - 1; i++)
                                        {
                                            state.xs[i] = state.solver.x[i];
                                        }
                                        break;
                                    }

                                    //
                                    // Stopping condition #1 - relative function improvement is small:
                                    //
                                    // 1. calculate steepest descent step:   V = -D1/(2*D2)
                                    // 2. calculate function change:         V1= D2*V^2 + D1*V
                                    // 3. stop if function change is small enough
                                    //
                                    v = -(d1 / (2 * d2));
                                    v1 = d2 * v * v + d1 * v;
                                    if ((double)(Math.Abs(v1)) <= (double)(state.bleicepsf * Math.Max(d0, 1.0)))
                                    {
                                        state.repterminationtype = 1;
                                        for (i = 0; i <= n - 1; i++)
                                        {
                                            state.xs[i] = state.solver.x[i];
                                        }
                                        break;
                                    }

                                    //
                                    // Stopping condition #2 - scaled step is small:
                                    //
                                    // 1. calculate step multiplier V0 (step itself is D*V0)
                                    // 2. calculate scaled step length V
                                    // 3. stop if step is small enough
                                    //
                                    v0 = -(d1 / (2 * d2));
                                    v = 0;
                                    for (i = 0; i <= n - 1; i++)
                                    {
                                        v = v + math.sqr(v0 * state.solver.d[i] / state.s[i]);
                                    }
                                    if ((double)(Math.Sqrt(v)) <= (double)(state.bleicepsx))
                                    {
                                        state.repterminationtype = 2;
                                        for (i = 0; i <= n - 1; i++)
                                        {
                                            state.xs[i] = state.solver.x[i];
                                        }
                                        break;
                                    }
                                }
                            }
                        }

                        //
                        // Gradient evaluation
                        //
                        if (state.solver.needfg)
                        {
                            for (i = 0; i <= n - 1; i++)
                            {
                                state.tmp0[i] = state.solver.x[i] - state.xorigin[i];
                            }
                            if (state.akind == 0)
                            {
                                cqmodels.cqmadx(state.a, state.tmp0, ref state.tmp1);
                            }
                            if (state.akind == 1)
                            {
                                sparse.sparsesmv(state.sparsea, state.sparseaupper, state.tmp0, ref state.tmp1);
                            }
                            v0 = 0.0;
                            for (i_ = 0; i_ <= n - 1; i_++)
                            {
                                v0 += state.tmp0[i_] * state.tmp1[i_];
                            }
                            v1 = 0.0;
                            for (i_ = 0; i_ <= n - 1; i_++)
                            {
                                v1 += state.tmp0[i_] * state.b[i_];
                            }
                            state.solver.f = 0.5 * v0 + v1;
                            for (i_ = 0; i_ <= n - 1; i_++)
                            {
                                state.solver.g[i_] = state.tmp1[i_];
                            }
                            for (i_ = 0; i_ <= n - 1; i_++)
                            {
                                state.solver.g[i_] = state.solver.g[i_] + state.b[i_];
                            }
                        }
                    }
                    if (state.repterminationtype == 0)
                    {

                        //
                        // BLEIC optimizer was terminated by one of its inner stopping
                        // conditions. Usually it is iteration counter (if such
                        // stopping condition was specified by user).
                        //
                        minbleic.minbleicresults(state.solver, ref state.xs, state.solverrep);
                        state.repterminationtype = state.solverrep.terminationtype;
                    }
                    else
                    {

                        //
                        // BLEIC optimizer was terminated in "emergency" mode by QP
                        // solver.
                        //
                        // NOTE: such termination is "emergency" only when viewed from
                        //       BLEIC's position. QP solver sees such termination as
                        //       routine one, triggered by QP's stopping criteria.
                        //
                        minbleic.minbleicemergencytermination(state.solver);
                    }
                    return;
                }
            }




            /*************************************************************************
            QP results

            Buffered implementation of MinQPResults() which uses pre-allocated  buffer
            to store X[]. If buffer size is  too  small,  it  resizes  buffer.  It  is
            intended to be used in the inner cycles of performance critical algorithms
            where array reallocation penalty is too large to be ignored.

              -- ALGLIB --
                 Copyright 11.01.2011 by Bochkanov Sergey
            *************************************************************************/
            public static void minqpresultsbuf(minqpstate state,
                ref double[] x,
                minqpreport rep)
            {
                int i_ = 0;

                if (alglib.ap.len(x) < state.n)
                {
                    x = new double[state.n];
                }
                for (i_ = 0; i_ <= state.n - 1; i_++)
                {
                    x[i_] = state.xs[i_];
                }
                rep.inneriterationscount = state.repinneriterationscount;
                rep.outeriterationscount = state.repouteriterationscount;
                rep.nmv = state.repnmv;
                rep.ncholesky = state.repncholesky;
                rep.terminationtype = state.repterminationtype;
            }


            /*************************************************************************
            Fast version of MinQPSetLinearTerm(), which doesn't check its arguments.
            For internal use only.

              -- ALGLIB --
                 Copyright 11.01.2011 by Bochkanov Sergey
            *************************************************************************/
            public static void minqpsetlineartermfast(minqpstate state,
                double[] b)
            {
                int i_ = 0;

                for (i_ = 0; i_ <= state.n - 1; i_++)
                {
                    state.b[i_] = b[i_];
                }
            }


            /*************************************************************************
            Fast version of MinQPSetQuadraticTerm(), which doesn't check its arguments.

            It accepts additional parameter - shift S, which allows to "shift"  matrix
            A by adding s*I to A. S must be positive (although it is not checked).

            For internal use only.

              -- ALGLIB --
                 Copyright 11.01.2011 by Bochkanov Sergey
            *************************************************************************/
            public static void minqpsetquadratictermfast(minqpstate state,
                double[,] a,
                bool isupper,
                double s)
            {
                int i = 0;
                int j = 0;
                int n = 0;

                n = state.n;
                state.akind = 0;
                cqmodels.cqmseta(state.a, a, isupper, 1.0);
                if ((double)(s) > (double)(0))
                {
                    apserv.rvectorsetlengthatleast(ref state.tmp0, n);
                    for (i = 0; i <= n - 1; i++)
                    {
                        state.tmp0[i] = a[i, i] + s;
                    }
                    cqmodels.cqmrewritedensediagonal(state.a, state.tmp0);
                }

                //
                // Estimate norm of A
                // (it will be used later in the quadratic penalty function)
                //
                state.anorm = 0;
                for (i = 0; i <= n - 1; i++)
                {
                    if (isupper)
                    {
                        for (j = i; j <= n - 1; j++)
                        {
                            state.anorm = Math.Max(state.anorm, Math.Abs(a[i, j]));
                        }
                    }
                    else
                    {
                        for (j = 0; j <= i; j++)
                        {
                            state.anorm = Math.Max(state.anorm, Math.Abs(a[i, j]));
                        }
                    }
                }
                state.anorm = state.anorm * n;
            }


            /*************************************************************************
            Internal function which allows to rewrite diagonal of quadratic term.
            For internal use only.

            This function can be used only when you have dense A and already made
            MinQPSetQuadraticTerm(Fast) call.

              -- ALGLIB --
                 Copyright 16.01.2011 by Bochkanov Sergey
            *************************************************************************/
            public static void minqprewritediagonal(minqpstate state,
                double[] s)
            {
                cqmodels.cqmrewritedensediagonal(state.a, s);
            }


            /*************************************************************************
            Fast version of MinQPSetStartingPoint(), which doesn't check its arguments.
            For internal use only.

              -- ALGLIB --
                 Copyright 11.01.2011 by Bochkanov Sergey
            *************************************************************************/
            public static void minqpsetstartingpointfast(minqpstate state,
                double[] x)
            {
                int n = 0;
                int i_ = 0;

                n = state.n;
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    state.startx[i_] = x[i_];
                }
                state.havex = true;
            }


            /*************************************************************************
            Fast version of MinQPSetOrigin(), which doesn't check its arguments.
            For internal use only.

              -- ALGLIB --
                 Copyright 11.01.2011 by Bochkanov Sergey
            *************************************************************************/
            public static void minqpsetoriginfast(minqpstate state,
                double[] xorigin)
            {
                int n = 0;
                int i_ = 0;

                n = state.n;
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    state.xorigin[i_] = xorigin[i_];
                }
            }


            /*************************************************************************
            Having feasible current point XC and possibly infeasible candidate   point
            XN,  this  function  performs  longest  step  from  XC to XN which retains
            feasibility. In case XN is found to be infeasible, at least one constraint
            is activated.

            For example, if we have:
              XC=0.5
              XN=1.2
              x>=0, x<=1
            then this function will move us to X=1.0 and activate constraint "x<=1".

            INPUT PARAMETERS:
                State   -   MinQP state.
                XC      -   current point, must be feasible with respect to
                            all constraints
                XN      -   candidate point, can be infeasible with respect to some
                            constraints. Must be located in the subspace of current
                            active set, i.e. it is feasible with respect to already
                            active constraints.
                Buf     -   temporary buffer, automatically resized if needed

            OUTPUT PARAMETERS:
                State   -   this function changes following fields of State:
                            * State.ActiveSet
                            * State.ActiveC     -   active linear constraints
                XC      -   new position

            RESULT:
                >0, in case at least one inactive non-candidate constraint was activated
                =0, in case only "candidate" constraints were activated
                <0, in case no constraints were activated by the step


              -- ALGLIB --
                 Copyright 29.02.2012 by Bochkanov Sergey
            *************************************************************************/
            private static int minqpboundedstepandactivation(minqpstate state,
                double[] xn,
                ref double[] buf)
            {
                int result = 0;
                int n = 0;
                double stpmax = 0;
                int cidx = 0;
                double cval = 0;
                bool needact = new bool();
                double v = 0;
                int i_ = 0;

                n = state.n;
                apserv.rvectorsetlengthatleast(ref buf, n);
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    buf[i_] = xn[i_];
                }
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    buf[i_] = buf[i_] - state.sas.xc[i_];
                }
                sactivesets.sasexploredirection(state.sas, buf, ref stpmax, ref cidx, ref cval);
                needact = (double)(stpmax) <= (double)(1);
                v = Math.Min(stpmax, 1.0);
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    buf[i_] = v * buf[i_];
                }
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    buf[i_] = buf[i_] + state.sas.xc[i_];
                }
                result = sactivesets.sasmoveto(state.sas, buf, needact, cidx, cval);
                return result;
            }


            /*************************************************************************
            Model value: f = 0.5*x'*A*x + b'*x

            INPUT PARAMETERS:
                A       -   convex quadratic model; only main quadratic term is used,
                            other parts of the model (D/Q/linear term) are ignored.
                            This function does not modify model state.
                B       -   right part
                XC      -   evaluation point
                Tmp     -   temporary buffer, automatically resized if needed

              -- ALGLIB --
                 Copyright 20.06.2012 by Bochkanov Sergey
            *************************************************************************/
            private static double minqpmodelvalue(cqmodels.convexquadraticmodel a,
                double[] b,
                double[] xc,
                int n,
                ref double[] tmp)
            {
                double result = 0;
                double v0 = 0;
                double v1 = 0;
                int i_ = 0;

                apserv.rvectorsetlengthatleast(ref tmp, n);
                cqmodels.cqmadx(a, xc, ref tmp);
                v0 = 0.0;
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    v0 += xc[i_] * tmp[i_];
                }
                v1 = 0.0;
                for (i_ = 0; i_ <= n - 1; i_++)
                {
                    v1 += xc[i_] * b[i_];
                }
                result = 0.5 * v0 + v1;
                return result;
            }


            /*************************************************************************
            Optimum of A subject to:
            a) active boundary constraints (given by ActiveSet[] and corresponding
               elements of XC)
            b) active linear constraints (given by C, R, LagrangeC)

            INPUT PARAMETERS:
                A       -   main quadratic term of the model;
                            although structure may  store  linear  and  rank-K  terms,
                            these terms are ignored and rewritten  by  this  function.
                ANorm   -   estimate of ||A|| (2-norm is used)
                B       -   array[N], linear term of the model
                XN      -   possibly preallocated buffer
                Tmp     -   temporary buffer (automatically resized)
                Tmp1    -   temporary buffer (automatically resized)

            OUTPUT PARAMETERS:
                A       -   modified quadratic model (this function changes rank-K
                            term and linear term of the model)
                LagrangeC-  current estimate of the Lagrange coefficients
                XN      -   solution

            RESULT:
                True on success, False on failure (non-SPD model)

              -- ALGLIB --
                 Copyright 20.06.2012 by Bochkanov Sergey
            *************************************************************************/
            private static bool minqpconstrainedoptimum(minqpstate state,
                cqmodels.convexquadraticmodel a,
                double anorm,
                double[] b,
                ref double[] xn,
                ref double[] tmp,
                ref bool[] tmpb,
                ref double[] lagrangec)
            {
                bool result = new bool();
                int itidx = 0;
                int i = 0;
                double v = 0;
                double feaserrold = 0;
                double feaserrnew = 0;
                double theta = 0;
                int n = 0;
                int i_ = 0;

                n = state.n;

                //
                // Rebuild basis accroding to current active set.
                // We call SASRebuildBasis() to make sure that fields of SAS
                // store up to date values.
                //
                sactivesets.sasrebuildbasis(state.sas);

                //
                // Allocate temporaries.
                //
                apserv.rvectorsetlengthatleast(ref tmp, Math.Max(n, state.sas.basissize));
                apserv.bvectorsetlengthatleast(ref tmpb, n);
                apserv.rvectorsetlengthatleast(ref lagrangec, state.sas.basissize);

                //
                // Prepare model
                //
                for (i = 0; i <= state.sas.basissize - 1; i++)
                {
                    tmp[i] = state.sas.pbasis[i, n];
                }
                theta = 100.0 * anorm;
                for (i = 0; i <= n - 1; i++)
                {
                    if (state.sas.activeset[i] > 0)
                    {
                        tmpb[i] = true;
                    }
                    else
                    {
                        tmpb[i] = false;
                    }
                }
                cqmodels.cqmsetactiveset(a, state.sas.xc, tmpb);
                cqmodels.cqmsetq(a, state.sas.pbasis, tmp, state.sas.basissize, theta);

                //
                // Iterate until optimal values of Lagrange multipliers are found
                //
                for (i = 0; i <= state.sas.basissize - 1; i++)
                {
                    lagrangec[i] = 0;
                }
                feaserrnew = math.maxrealnumber;
                result = true;
                for (itidx = 1; itidx <= maxlagrangeits; itidx++)
                {

                    //
                    // Generate right part B using linear term and current
                    // estimate of the Lagrange multipliers.
                    //
                    for (i_ = 0; i_ <= n - 1; i_++)
                    {
                        tmp[i_] = b[i_];
                    }
                    for (i = 0; i <= state.sas.basissize - 1; i++)
                    {
                        v = lagrangec[i];
                        for (i_ = 0; i_ <= n - 1; i_++)
                        {
                            tmp[i_] = tmp[i_] - v * state.sas.pbasis[i, i_];
                        }
                    }
                    cqmodels.cqmsetb(a, tmp);

                    //
                    // Solve
                    //
                    result = cqmodels.cqmconstrainedoptimum(a, ref xn);
                    if (!result)
                    {
                        return result;
                    }

                    //
                    // Compare feasibility errors.
                    // Terminate if error decreased too slowly.
                    //
                    feaserrold = feaserrnew;
                    feaserrnew = 0;
                    for (i = 0; i <= state.sas.basissize - 1; i++)
                    {
                        v = 0.0;
                        for (i_ = 0; i_ <= n - 1; i_++)
                        {
                            v += state.sas.pbasis[i, i_] * xn[i_];
                        }
                        feaserrnew = feaserrnew + math.sqr(v - state.sas.pbasis[i, n]);
                    }
                    feaserrnew = Math.Sqrt(feaserrnew);
                    if ((double)(feaserrnew) >= (double)(0.2 * feaserrold))
                    {
                        break;
                    }

                    //
                    // Update Lagrange multipliers
                    //
                    for (i = 0; i <= state.sas.basissize - 1; i++)
                    {
                        v = 0.0;
                        for (i_ = 0; i_ <= n - 1; i_++)
                        {
                            v += state.sas.pbasis[i, i_] * xn[i_];
                        }
                        lagrangec[i] = lagrangec[i] - theta * (v - state.sas.pbasis[i, n]);
                    }
                }
                return result;
            }


        }

        /********************************************************************
   internal functions
   ********************************************************************/
        public class ap
        {
            public static int len<T>(T[] a)
            { return a.Length; }
            public static int rows<T>(T[,] a)
            { return a.GetLength(0); }
            public static int cols<T>(T[,] a)
            { return a.GetLength(1); }
            public static void swap<T>(ref T a, ref T b)
            {
                T t = a;
                a = b;
                b = t;
            }

            public static void assert(bool cond, string s)
            {
                if (!cond)
                    throw new alglibexception(s);
            }

        };

        /********************************************************************
        math functions
        ********************************************************************/
        public class math
        {
            //public static System.Random RndObject = new System.Random(System.DateTime.Now.Millisecond);
            public static System.Random rndobject = new System.Random(System.DateTime.Now.Millisecond + 1000 * System.DateTime.Now.Second + 60 * 1000 * System.DateTime.Now.Minute);

            public const double machineepsilon = 5E-16;
            public const double maxrealnumber = 1E300;
            public const double minrealnumber = 1E-300;

            public static bool isfinite(double d)
            {
                return !System.Double.IsNaN(d) && !System.Double.IsInfinity(d);
            }

            public static double randomreal()
            {
                double r = 0;
                lock (rndobject) { r = rndobject.NextDouble(); }
                return r;
            }
            public static int randominteger(int N)
            {
                int r = 0;
                lock (rndobject) { r = rndobject.Next(N); }
                return r;
            }
            public static double sqr(double X)
            {
                return X * X;
            }
            public static double abscomplex(complex z)
            {
                double w;
                double xabs;
                double yabs;
                double v;

                xabs = System.Math.Abs(z.x);
                yabs = System.Math.Abs(z.y);
                w = xabs > yabs ? xabs : yabs;
                v = xabs < yabs ? xabs : yabs;
                if (v == 0)
                    return w;
                else
                {
                    double t = v / w;
                    return w * System.Math.Sqrt(1 + t * t);
                }
            }
            public static complex conj(complex z)
            {
                return new complex(z.x, -z.y);
            }
            
        }

        public struct complex
        {
            public double x;
            public double y;

            public complex(double _x)
            {
                x = _x;
                y = 0;
            }
            public complex(double _x, double _y)
            {
                x = _x;
                y = _y;
            }

            public static implicit operator complex(double _x)
            {
                return new complex(_x);
            }
            
            public static complex operator +(complex lhs, complex rhs)
            {
                return new complex(lhs.x + rhs.x, lhs.y + rhs.y);
            }
            public static complex operator -(complex lhs, complex rhs)
            {
                return new complex(lhs.x - rhs.x, lhs.y - rhs.y);
            }
            public static complex operator *(complex lhs, complex rhs)
            {
                return new complex(lhs.x * rhs.x - lhs.y * rhs.y, lhs.x * rhs.y + lhs.y * rhs.x);
            }
            public static complex operator /(complex lhs, complex rhs)
            {
                complex result;
                double e;
                double f;
                if (System.Math.Abs(rhs.y) < System.Math.Abs(rhs.x))
                {
                    e = rhs.y / rhs.x;
                    f = rhs.x + rhs.y * e;
                    result.x = (lhs.x + lhs.y * e) / f;
                    result.y = (lhs.y - lhs.x * e) / f;
                }
                else
                {
                    e = rhs.x / rhs.y;
                    f = rhs.y + rhs.x * e;
                    result.x = (lhs.y + lhs.x * e) / f;
                    result.y = (-lhs.x + lhs.y * e) / f;
                }
                return result;
            }
            public override int GetHashCode()
            {
                return x.GetHashCode() ^ y.GetHashCode();
            }
            public override bool Equals(object obj)
            {
                if (obj is byte)
                    return Equals(new complex((byte)obj));
                if (obj is sbyte)
                    return Equals(new complex((sbyte)obj));
                if (obj is short)
                    return Equals(new complex((short)obj));
                if (obj is ushort)
                    return Equals(new complex((ushort)obj));
                if (obj is int)
                    return Equals(new complex((int)obj));
                if (obj is uint)
                    return Equals(new complex((uint)obj));
                if (obj is long)
                    return Equals(new complex((long)obj));
                if (obj is ulong)
                    return Equals(new complex((ulong)obj));
                if (obj is float)
                    return Equals(new complex((float)obj));
                if (obj is double)
                    return Equals(new complex((double)obj));
                if (obj is decimal)
                    return Equals(new complex((double)(decimal)obj));
                return base.Equals(obj);
            }
        }

        public class apserv
        {
            /*************************************************************************
            Buffers for internal functions which need buffers:
            * check for size of the buffer you want to use.
            * if buffer is too small, resize it; leave unchanged, if it is larger than
              needed.
            * use it.

            We can pass this structure to multiple functions;  after first run through
            functions buffer sizes will be finally determined,  and  on  a next run no
            allocation will be required.
            *************************************************************************/
            public class apbuffers : apobject
            {
                public int[] ia0;
                public int[] ia1;
                public int[] ia2;
                public int[] ia3;
                public double[] ra0;
                public double[] ra1;
                public double[] ra2;
                public double[] ra3;
                public apbuffers()
                {
                    init();
                }
                public override void init()
                {
                    ia0 = new int[0];
                    ia1 = new int[0];
                    ia2 = new int[0];
                    ia3 = new int[0];
                    ra0 = new double[0];
                    ra1 = new double[0];
                    ra2 = new double[0];
                    ra3 = new double[0];
                }
                public override alglib.apobject make_copy()
                {
                    apbuffers _result = new apbuffers();
                    _result.ia0 = (int[])ia0.Clone();
                    _result.ia1 = (int[])ia1.Clone();
                    _result.ia2 = (int[])ia2.Clone();
                    _result.ia3 = (int[])ia3.Clone();
                    _result.ra0 = (double[])ra0.Clone();
                    _result.ra1 = (double[])ra1.Clone();
                    _result.ra2 = (double[])ra2.Clone();
                    _result.ra3 = (double[])ra3.Clone();
                    return _result;
                }
            };


            /*************************************************************************
            Structure which is used to workaround limitations of ALGLIB parallellization
            environment.

              -- ALGLIB --
                 Copyright 12.04.2009 by Bochkanov Sergey
            *************************************************************************/
            public class sboolean : apobject
            {
                public bool val;
                public sboolean()
                {
                    init();
                }
                public override void init()
                {
                }
                public override alglib.apobject make_copy()
                {
                    sboolean _result = new sboolean();
                    _result.val = val;
                    return _result;
                }
            };


            /*************************************************************************
            Structure which is used to workaround limitations of ALGLIB parallellization
            environment.

              -- ALGLIB --
                 Copyright 12.04.2009 by Bochkanov Sergey
            *************************************************************************/
            public class sbooleanarray : apobject
            {
                public bool[] val;
                public sbooleanarray()
                {
                    init();
                }
                public override void init()
                {
                    val = new bool[0];
                }
                public override alglib.apobject make_copy()
                {
                    sbooleanarray _result = new sbooleanarray();
                    _result.val = (bool[])val.Clone();
                    return _result;
                }
            };


            /*************************************************************************
            Structure which is used to workaround limitations of ALGLIB parallellization
            environment.

              -- ALGLIB --
                 Copyright 12.04.2009 by Bochkanov Sergey
            *************************************************************************/
            public class sinteger : apobject
            {
                public int val;
                public sinteger()
                {
                    init();
                }
                public override void init()
                {
                }
                public override alglib.apobject make_copy()
                {
                    sinteger _result = new sinteger();
                    _result.val = val;
                    return _result;
                }
            };


            /*************************************************************************
            Structure which is used to workaround limitations of ALGLIB parallellization
            environment.

              -- ALGLIB --
                 Copyright 12.04.2009 by Bochkanov Sergey
            *************************************************************************/
            public class sintegerarray : apobject
            {
                public int[] val;
                public sintegerarray()
                {
                    init();
                }
                public override void init()
                {
                    val = new int[0];
                }
                public override alglib.apobject make_copy()
                {
                    sintegerarray _result = new sintegerarray();
                    _result.val = (int[])val.Clone();
                    return _result;
                }
            };


            /*************************************************************************
            Structure which is used to workaround limitations of ALGLIB parallellization
            environment.

              -- ALGLIB --
                 Copyright 12.04.2009 by Bochkanov Sergey
            *************************************************************************/
            public class sreal : apobject
            {
                public double val;
                public sreal()
                {
                    init();
                }
                public override void init()
                {
                }
                public override alglib.apobject make_copy()
                {
                    sreal _result = new sreal();
                    _result.val = val;
                    return _result;
                }
            };


            /*************************************************************************
            Structure which is used to workaround limitations of ALGLIB parallellization
            environment.

              -- ALGLIB --
                 Copyright 12.04.2009 by Bochkanov Sergey
            *************************************************************************/
            public class srealarray : apobject
            {
                public double[] val;
                public srealarray()
                {
                    init();
                }
                public override void init()
                {
                    val = new double[0];
                }
                public override alglib.apobject make_copy()
                {
                    srealarray _result = new srealarray();
                    _result.val = (double[])val.Clone();
                    return _result;
                }
            };


            /*************************************************************************
            Structure which is used to workaround limitations of ALGLIB parallellization
            environment.

              -- ALGLIB --
                 Copyright 12.04.2009 by Bochkanov Sergey
            *************************************************************************/
            public class scomplex : apobject
            {
                public complex val;
                public scomplex()
                {
                    init();
                }
                public override void init()
                {
                }
                public override alglib.apobject make_copy()
                {
                    scomplex _result = new scomplex();
                    _result.val = val;
                    return _result;
                }
            };


            /*************************************************************************
            Structure which is used to workaround limitations of ALGLIB parallellization
            environment.

              -- ALGLIB --
                 Copyright 12.04.2009 by Bochkanov Sergey
            *************************************************************************/
            public class scomplexarray : apobject
            {
                public complex[] val;
                public scomplexarray()
                {
                    init();
                }
                public override void init()
                {
                    val = new complex[0];
                }
                public override alglib.apobject make_copy()
                {
                    scomplexarray _result = new scomplexarray();
                    _result.val = (alglib.complex[])val.Clone();
                    return _result;
                }
            };




            /*************************************************************************
            This function is used to set error flags  during  unit  tests.  When  COND
            parameter is True, FLAG variable is  set  to  True.  When  COND is  False,
            FLAG is unchanged.

            The purpose of this function is to have single  point  where  failures  of
            unit tests can be detected.

            This function returns value of COND.
            *************************************************************************/
            public static bool seterrorflag(ref bool flag,
                bool cond)
            {
                bool result = new bool();

                if (cond)
                {
                    flag = true;
                }
                result = cond;
                return result;
            }


            /*************************************************************************
            If Length(X)<N, resizes X

              -- ALGLIB --
                 Copyright 20.03.2009 by Bochkanov Sergey
            *************************************************************************/
            public static void bvectorsetlengthatleast(ref bool[] x,
                int n)
            {
                if (alglib.ap.len(x) < n)
                {
                    x = new bool[n];
                }
            }


            /*************************************************************************
            If Length(X)<N, resizes X

              -- ALGLIB --
                 Copyright 20.03.2009 by Bochkanov Sergey
            *************************************************************************/
            public static void ivectorsetlengthatleast(ref int[] x,
                int n)
            {
                if (alglib.ap.len(x) < n)
                {
                    x = new int[n];
                }
            }


            /*************************************************************************
            If Length(X)<N, resizes X

              -- ALGLIB --
                 Copyright 20.03.2009 by Bochkanov Sergey
            *************************************************************************/
            public static void rvectorsetlengthatleast(ref double[] x,
                int n)
            {
                if (alglib.ap.len(x) < n)
                {
                    x = new double[n];
                }
            }


            /*************************************************************************
            If Cols(X)<N or Rows(X)<M, resizes X

              -- ALGLIB --
                 Copyright 20.03.2009 by Bochkanov Sergey
            *************************************************************************/
            public static void rmatrixsetlengthatleast(ref double[,] x,
                int m,
                int n)
            {
                if (m > 0 && n > 0)
                {
                    if (alglib.ap.rows(x) < m || alglib.ap.cols(x) < n)
                    {
                        x = new double[m, n];
                    }
                }
            }


           

            /*************************************************************************
            This function checks that length(X) is at least N and first N values  from
            X[] are finite

              -- ALGLIB --
                 Copyright 18.06.2010 by Bochkanov Sergey
            *************************************************************************/
            public static bool isfinitevector(double[] x,
                int n)
            {
                bool result = new bool();
                int i = 0;

                alglib.ap.assert(n >= 0, "APSERVIsFiniteVector: internal error (N<0)");
                if (n == 0)
                {
                    result = true;
                    return result;
                }
                if (alglib.ap.len(x) < n)
                {
                    result = false;
                    return result;
                }
                for (i = 0; i <= n - 1; i++)
                {
                    if (!math.isfinite(x[i]))
                    {
                        result = false;
                        return result;
                    }
                }
                result = true;
                return result;
            }


            /*************************************************************************
            This function calculates "safe" min(X/Y,V) for positive finite X, Y, V.
            No overflow is generated in any case.

              -- ALGLIB --
                 Copyright by Bochkanov Sergey
            *************************************************************************/
            public static double safeminposrv(double x,
                double y,
                double v)
            {
                double result = 0;
                double r = 0;

                if ((double)(y) >= (double)(1))
                {

                    //
                    // Y>=1, we can safely divide by Y
                    //
                    r = x / y;
                    result = v;
                    if ((double)(v) > (double)(r))
                    {
                        result = r;
                    }
                    else
                    {
                        result = v;
                    }
                }
                else
                {

                    //
                    // Y<1, we can safely multiply by Y
                    //
                    if ((double)(x) < (double)(v * y))
                    {
                        result = x / y;
                    }
                    else
                    {
                        result = v;
                    }
                }
                return result;
            }



            /*************************************************************************
            This function is used to increment value of integer variable
            *************************************************************************/
            public static void inc(ref int v)
            {
                v = v + 1;
            }


            

            /*************************************************************************
            'bounds' value: maps X to [B1,B2]

              -- ALGLIB --
                 Copyright 20.03.2009 by Bochkanov Sergey
            *************************************************************************/
            public static double boundval(double x,
                double b1,
                double b2)
            {
                double result = 0;

                if ((double)(x) <= (double)(b1))
                {
                    result = b1;
                    return result;
                }
                if ((double)(x) >= (double)(b2))
                {
                    result = b2;
                    return result;
                }
                result = x;
                return result;
            }

        }

        public class cqmodels
        {
            /*************************************************************************
            This structure describes convex quadratic model of the form:
                f(x) = 0.5*(Alpha*x'*A*x + Tau*x'*D*x) + 0.5*Theta*(Q*x-r)'*(Q*x-r) + b'*x
            where:
                * Alpha>=0, Tau>=0, Theta>=0, Alpha+Tau>0.
                * A is NxN matrix, Q is NxK matrix (N>=1, K>=0), b is Nx1 vector,
                  D is NxN diagonal matrix.
                * "main" quadratic term Alpha*A+Lambda*D is symmetric
                  positive definite
            Structure may contain optional equality constraints of the form x[i]=x0[i],
            in this case functions provided by this unit calculate Newton step subject
            to these equality constraints.
            *************************************************************************/
            public class convexquadraticmodel : apobject
            {
                public int n;
                public int k;
                public double alpha;
                public double tau;
                public double theta;
                public double[,] a;
                public double[,] q;
                public double[] b;
                public double[] r;
                public double[] xc;
                public double[] d;
                public bool[] activeset;
                public double[,] tq2dense;
                public double[,] tk2;
                public double[] tq2diag;
                public double[] tq1;
                public double[] tk1;
                public double tq0;
                public double tk0;
                public double[] txc;
                public double[] tb;
                public int nfree;
                public int ecakind;
                public double[,] ecadense;
                public double[,] eq;
                public double[,] eccm;
                public double[] ecadiag;
                public double[] eb;
                public double ec;
                public double[] tmp0;
                public double[] tmp1;
                public double[] tmpg;
                public double[,] tmp2;
                public bool ismaintermchanged;
                public bool issecondarytermchanged;
                public bool islineartermchanged;
                public bool isactivesetchanged;
                public convexquadraticmodel()
                {
                    init();
                }
                public override void init()
                {
                    a = new double[0, 0];
                    q = new double[0, 0];
                    b = new double[0];
                    r = new double[0];
                    xc = new double[0];
                    d = new double[0];
                    activeset = new bool[0];
                    tq2dense = new double[0, 0];
                    tk2 = new double[0, 0];
                    tq2diag = new double[0];
                    tq1 = new double[0];
                    tk1 = new double[0];
                    txc = new double[0];
                    tb = new double[0];
                    ecadense = new double[0, 0];
                    eq = new double[0, 0];
                    eccm = new double[0, 0];
                    ecadiag = new double[0];
                    eb = new double[0];
                    tmp0 = new double[0];
                    tmp1 = new double[0];
                    tmpg = new double[0];
                    tmp2 = new double[0, 0];
                }
                public override alglib.apobject make_copy()
                {
                    convexquadraticmodel _result = new convexquadraticmodel();
                    _result.n = n;
                    _result.k = k;
                    _result.alpha = alpha;
                    _result.tau = tau;
                    _result.theta = theta;
                    _result.a = (double[,])a.Clone();
                    _result.q = (double[,])q.Clone();
                    _result.b = (double[])b.Clone();
                    _result.r = (double[])r.Clone();
                    _result.xc = (double[])xc.Clone();
                    _result.d = (double[])d.Clone();
                    _result.activeset = (bool[])activeset.Clone();
                    _result.tq2dense = (double[,])tq2dense.Clone();
                    _result.tk2 = (double[,])tk2.Clone();
                    _result.tq2diag = (double[])tq2diag.Clone();
                    _result.tq1 = (double[])tq1.Clone();
                    _result.tk1 = (double[])tk1.Clone();
                    _result.tq0 = tq0;
                    _result.tk0 = tk0;
                    _result.txc = (double[])txc.Clone();
                    _result.tb = (double[])tb.Clone();
                    _result.nfree = nfree;
                    _result.ecakind = ecakind;
                    _result.ecadense = (double[,])ecadense.Clone();
                    _result.eq = (double[,])eq.Clone();
                    _result.eccm = (double[,])eccm.Clone();
                    _result.ecadiag = (double[])ecadiag.Clone();
                    _result.eb = (double[])eb.Clone();
                    _result.ec = ec;
                    _result.tmp0 = (double[])tmp0.Clone();
                    _result.tmp1 = (double[])tmp1.Clone();
                    _result.tmpg = (double[])tmpg.Clone();
                    _result.tmp2 = (double[,])tmp2.Clone();
                    _result.ismaintermchanged = ismaintermchanged;
                    _result.issecondarytermchanged = issecondarytermchanged;
                    _result.islineartermchanged = islineartermchanged;
                    _result.isactivesetchanged = isactivesetchanged;
                    return _result;
                }
            };
        }

        public class sparse
        {
            /*************************************************************************
            Sparse matrix

            You should use ALGLIB functions to work with sparse matrix.
            Never try to access its fields directly!
            *************************************************************************/
            public class sparsematrix : apobject
            {
                public double[] vals;
                public int[] idx;
                public int[] ridx;
                public int[] didx;
                public int[] uidx;
                public int matrixtype;
                public int m;
                public int n;
                public int nfree;
                public int ninitialized;
                public sparsematrix()
                {
                    init();
                }
                public override void init()
                {
                    vals = new double[0];
                    idx = new int[0];
                    ridx = new int[0];
                    didx = new int[0];
                    uidx = new int[0];
                }
                public override alglib.apobject make_copy()
                {
                    sparsematrix _result = new sparsematrix();
                    _result.vals = (double[])vals.Clone();
                    _result.idx = (int[])idx.Clone();
                    _result.ridx = (int[])ridx.Clone();
                    _result.didx = (int[])didx.Clone();
                    _result.uidx = (int[])uidx.Clone();
                    _result.matrixtype = matrixtype;
                    _result.m = m;
                    _result.n = n;
                    _result.nfree = nfree;
                    _result.ninitialized = ninitialized;
                    return _result;
                }
            };
        }

        public class sactivesets
        {
            /*************************************************************************
            This structure describes set of linear constraints (boundary  and  general
            ones) which can be active and inactive. It also has functionality to  work
            with current point and current  gradient  (determine  active  constraints,
            move current point, project gradient into  constrained  subspace,  perform
            constrained preconditioning and so on.

            This  structure  is  intended  to  be  used  by constrained optimizers for
            management of all constraint-related functionality.

            External code may access following internal fields of the structure:
                XC          -   stores current point, array[N].
                                can be accessed only in optimization mode
                ActiveSet   -   active set, array[N+NEC+NIC]:
                                * ActiveSet[I]>0    I-th constraint is in the active set
                                * ActiveSet[I]=0    I-th constraint is at the boundary, but inactive
                                * ActiveSet[I]<0    I-th constraint is far from the boundary (and inactive)
                                * elements from 0 to N-1 correspond to boundary constraints
                                * elements from N to N+NEC+NIC-1 correspond to linear constraints
                                * elements from N to N+NEC-1 are always +1
                PBasis,
                IBasis,
                SBasis      -   after call to SASRebuildBasis() these  matrices  store
                                active constraints, reorthogonalized with  respect  to
                                some inner product:
                                a) for PBasis - one  given  by  preconditioner  matrix
                                   (inverse Hessian)
                                b) for SBasis - one given by square of the scale matrix
                                c) for IBasis - traditional dot product
                            
                                array[BasisSize,N+1], where BasisSize is a  number  of
                                positive elements in  ActiveSet[N:N+NEC+NIC-1].  First
                                N columns store linear term, last column stores  right
                                part. All  three  matrices  are linearly equivalent to
                                each other, span(PBasis)=span(IBasis)=span(SBasis).
                            
                                IMPORTANT: you have to call  SASRebuildBasis()  before
                                           accessing these arrays  in  order  to  make
                                           sure that they are up to date.
                BasisSize   -   basis size (PBasis/SBasis/IBasis)
            *************************************************************************/
            public class sactiveset : apobject
            {
                public int n;
                public int algostate;
                public double[] xc;
                public bool hasxc;
                public double[] s;
                public double[] h;
                public int[] activeset;
                public bool basisisready;
                public double[,] sbasis;
                public double[,] pbasis;
                public double[,] ibasis;
                public int basissize;
                public bool constraintschanged;
                public bool[] hasbndl;
                public bool[] hasbndu;
                public double[] bndl;
                public double[] bndu;
                public double[,] cleic;
                public int nec;
                public int nic;
                public double[] mtx;
                public int[] mtas;
                public double[] cdtmp;
                public double[] corrtmp;
                public double[] unitdiagonal;
                public snnls.snnlssolver solver;
                public double[] scntmp;
                public double[] tmp0;
                public double[] tmpfeas;
                public double[,] tmpm0;
                public double[] rctmps;
                public double[] rctmpg;
                public double[] rctmprightpart;
                public double[,] rctmpdense0;
                public double[,] rctmpdense1;
                public bool[] rctmpisequality;
                public int[] rctmpconstraintidx;
                public double[] rctmplambdas;
                public double[,] tmpbasis;
                public sactiveset()
                {
                    init();
                }
                public override void init()
                {
                    xc = new double[0];
                    s = new double[0];
                    h = new double[0];
                    activeset = new int[0];
                    sbasis = new double[0, 0];
                    pbasis = new double[0, 0];
                    ibasis = new double[0, 0];
                    hasbndl = new bool[0];
                    hasbndu = new bool[0];
                    bndl = new double[0];
                    bndu = new double[0];
                    cleic = new double[0, 0];
                    mtx = new double[0];
                    mtas = new int[0];
                    cdtmp = new double[0];
                    corrtmp = new double[0];
                    unitdiagonal = new double[0];
                    solver = new snnls.snnlssolver();
                    scntmp = new double[0];
                    tmp0 = new double[0];
                    tmpfeas = new double[0];
                    tmpm0 = new double[0, 0];
                    rctmps = new double[0];
                    rctmpg = new double[0];
                    rctmprightpart = new double[0];
                    rctmpdense0 = new double[0, 0];
                    rctmpdense1 = new double[0, 0];
                    rctmpisequality = new bool[0];
                    rctmpconstraintidx = new int[0];
                    rctmplambdas = new double[0];
                    tmpbasis = new double[0, 0];
                }
                public override alglib.apobject make_copy()
                {
                    sactiveset _result = new sactiveset();
                    _result.n = n;
                    _result.algostate = algostate;
                    _result.xc = (double[])xc.Clone();
                    _result.hasxc = hasxc;
                    _result.s = (double[])s.Clone();
                    _result.h = (double[])h.Clone();
                    _result.activeset = (int[])activeset.Clone();
                    _result.basisisready = basisisready;
                    _result.sbasis = (double[,])sbasis.Clone();
                    _result.pbasis = (double[,])pbasis.Clone();
                    _result.ibasis = (double[,])ibasis.Clone();
                    _result.basissize = basissize;
                    _result.constraintschanged = constraintschanged;
                    _result.hasbndl = (bool[])hasbndl.Clone();
                    _result.hasbndu = (bool[])hasbndu.Clone();
                    _result.bndl = (double[])bndl.Clone();
                    _result.bndu = (double[])bndu.Clone();
                    _result.cleic = (double[,])cleic.Clone();
                    _result.nec = nec;
                    _result.nic = nic;
                    _result.mtx = (double[])mtx.Clone();
                    _result.mtas = (int[])mtas.Clone();
                    _result.cdtmp = (double[])cdtmp.Clone();
                    _result.corrtmp = (double[])corrtmp.Clone();
                    _result.unitdiagonal = (double[])unitdiagonal.Clone();
                    _result.solver = (snnls.snnlssolver)solver.make_copy();
                    _result.scntmp = (double[])scntmp.Clone();
                    _result.tmp0 = (double[])tmp0.Clone();
                    _result.tmpfeas = (double[])tmpfeas.Clone();
                    _result.tmpm0 = (double[,])tmpm0.Clone();
                    _result.rctmps = (double[])rctmps.Clone();
                    _result.rctmpg = (double[])rctmpg.Clone();
                    _result.rctmprightpart = (double[])rctmprightpart.Clone();
                    _result.rctmpdense0 = (double[,])rctmpdense0.Clone();
                    _result.rctmpdense1 = (double[,])rctmpdense1.Clone();
                    _result.rctmpisequality = (bool[])rctmpisequality.Clone();
                    _result.rctmpconstraintidx = (int[])rctmpconstraintidx.Clone();
                    _result.rctmplambdas = (double[])rctmplambdas.Clone();
                    _result.tmpbasis = (double[,])tmpbasis.Clone();
                    return _result;
                }
            };
        }

        public class normestimator
        {
            /*************************************************************************
            This object stores state of the iterative norm estimation algorithm.

            You should use ALGLIB functions to work with this object.
            *************************************************************************/
            public class normestimatorstate : apobject
            {
                public int n;
                public int m;
                public int nstart;
                public int nits;
                public int seedval;
                public double[] x0;
                public double[] x1;
                public double[] t;
                public double[] xbest;
                public hqrnd.hqrndstate r;
                public double[] x;
                public double[] mv;
                public double[] mtv;
                public bool needmv;
                public bool needmtv;
                public double repnorm;
                public rcommstate rstate;
                public normestimatorstate()
                {
                    init();
                }
                public override void init()
                {
                    x0 = new double[0];
                    x1 = new double[0];
                    t = new double[0];
                    xbest = new double[0];
                    r = new hqrnd.hqrndstate();
                    x = new double[0];
                    mv = new double[0];
                    mtv = new double[0];
                    rstate = new rcommstate();
                }
                public override alglib.apobject make_copy()
                {
                    normestimatorstate _result = new normestimatorstate();
                    _result.n = n;
                    _result.m = m;
                    _result.nstart = nstart;
                    _result.nits = nits;
                    _result.seedval = seedval;
                    _result.x0 = (double[])x0.Clone();
                    _result.x1 = (double[])x1.Clone();
                    _result.t = (double[])t.Clone();
                    _result.xbest = (double[])xbest.Clone();
                    _result.r = (hqrnd.hqrndstate)r.make_copy();
                    _result.x = (double[])x.Clone();
                    _result.mv = (double[])mv.Clone();
                    _result.mtv = (double[])mtv.Clone();
                    _result.needmv = needmv;
                    _result.needmtv = needmtv;
                    _result.repnorm = repnorm;
                    _result.rstate = (rcommstate)rstate.make_copy();
                    return _result;
                }
            };
        }

        public class minbleic
        {
            /*************************************************************************
            This object stores nonlinear optimizer state.
            You should use functions provided by MinBLEIC subpackage to work with this
            object
            *************************************************************************/
            public class minbleicstate : apobject
            {
                public int nmain;
                public int nslack;
                public double epsg;
                public double epsf;
                public double epsx;
                public int maxits;
                public bool xrep;
                public bool drep;
                public double stpmax;
                public double diffstep;
                public sactivesets.sactiveset sas;
                public double[] s;
                public int prectype;
                public double[] diagh;
                public double[] x;
                public double f;
                public double[] g;
                public bool needf;
                public bool needfg;
                public bool xupdated;
                public bool lsstart;
                public bool lbfgssearch;
                public bool boundedstep;
                public double teststep;
                public rcommstate rstate;
                public double[] gc;
                public double[] xn;
                public double[] gn;
                public double[] xp;
                public double[] gp;
                public double fc;
                public double fn;
                public double fp;
                public double[] d;
                public double[,] cleic;
                public int nec;
                public int nic;
                public double lastgoodstep;
                public double lastscaledgoodstep;
                public double maxscaledgrad;
                public bool[] hasbndl;
                public bool[] hasbndu;
                public double[] bndl;
                public double[] bndu;
                public int repinneriterationscount;
                public int repouteriterationscount;
                public int repnfev;
                public int repvaridx;
                public int repterminationtype;
                public double repdebugeqerr;
                public double repdebugfs;
                public double repdebugff;
                public double repdebugdx;
                public int repdebugfeasqpits;
                public int repdebugfeasgpaits;
                public double[] xstart;
                public snnls.snnlssolver solver;
                public double fbase;
                public double fm2;
                public double fm1;
                public double fp1;
                public double fp2;
                public double xm1;
                public double xp1;
                public double gm1;
                public double gp1;
                public int cidx;
                public double cval;
                public double[] tmpprec;
                public int nfev;
                public int mcstage;
                public double stp;
                public double curstpmax;
                public double activationstep;
                public double[] work;
                public linmin.linminstate lstate;
                public double trimthreshold;
                public int nonmonotoniccnt;
                public int k;
                public int q;
                public int p;
                public double[] rho;
                public double[,] yk;
                public double[,] sk;
                public double[] theta;
                public minbleicstate()
                {
                    init();
                }
                public override void init()
                {
                    sas = new sactivesets.sactiveset();
                    s = new double[0];
                    diagh = new double[0];
                    x = new double[0];
                    g = new double[0];
                    rstate = new rcommstate();
                    gc = new double[0];
                    xn = new double[0];
                    gn = new double[0];
                    xp = new double[0];
                    gp = new double[0];
                    d = new double[0];
                    cleic = new double[0, 0];
                    hasbndl = new bool[0];
                    hasbndu = new bool[0];
                    bndl = new double[0];
                    bndu = new double[0];
                    xstart = new double[0];
                    solver = new snnls.snnlssolver();
                    tmpprec = new double[0];
                    work = new double[0];
                    lstate = new linmin.linminstate();
                    rho = new double[0];
                    yk = new double[0, 0];
                    sk = new double[0, 0];
                    theta = new double[0];
                }
                public override alglib.apobject make_copy()
                {
                    minbleicstate _result = new minbleicstate();
                    _result.nmain = nmain;
                    _result.nslack = nslack;
                    _result.epsg = epsg;
                    _result.epsf = epsf;
                    _result.epsx = epsx;
                    _result.maxits = maxits;
                    _result.xrep = xrep;
                    _result.drep = drep;
                    _result.stpmax = stpmax;
                    _result.diffstep = diffstep;
                    _result.sas = (sactivesets.sactiveset)sas.make_copy();
                    _result.s = (double[])s.Clone();
                    _result.prectype = prectype;
                    _result.diagh = (double[])diagh.Clone();
                    _result.x = (double[])x.Clone();
                    _result.f = f;
                    _result.g = (double[])g.Clone();
                    _result.needf = needf;
                    _result.needfg = needfg;
                    _result.xupdated = xupdated;
                    _result.lsstart = lsstart;
                    _result.lbfgssearch = lbfgssearch;
                    _result.boundedstep = boundedstep;
                    _result.teststep = teststep;
                    _result.rstate = (rcommstate)rstate.make_copy();
                    _result.gc = (double[])gc.Clone();
                    _result.xn = (double[])xn.Clone();
                    _result.gn = (double[])gn.Clone();
                    _result.xp = (double[])xp.Clone();
                    _result.gp = (double[])gp.Clone();
                    _result.fc = fc;
                    _result.fn = fn;
                    _result.fp = fp;
                    _result.d = (double[])d.Clone();
                    _result.cleic = (double[,])cleic.Clone();
                    _result.nec = nec;
                    _result.nic = nic;
                    _result.lastgoodstep = lastgoodstep;
                    _result.lastscaledgoodstep = lastscaledgoodstep;
                    _result.maxscaledgrad = maxscaledgrad;
                    _result.hasbndl = (bool[])hasbndl.Clone();
                    _result.hasbndu = (bool[])hasbndu.Clone();
                    _result.bndl = (double[])bndl.Clone();
                    _result.bndu = (double[])bndu.Clone();
                    _result.repinneriterationscount = repinneriterationscount;
                    _result.repouteriterationscount = repouteriterationscount;
                    _result.repnfev = repnfev;
                    _result.repvaridx = repvaridx;
                    _result.repterminationtype = repterminationtype;
                    _result.repdebugeqerr = repdebugeqerr;
                    _result.repdebugfs = repdebugfs;
                    _result.repdebugff = repdebugff;
                    _result.repdebugdx = repdebugdx;
                    _result.repdebugfeasqpits = repdebugfeasqpits;
                    _result.repdebugfeasgpaits = repdebugfeasgpaits;
                    _result.xstart = (double[])xstart.Clone();
                    _result.solver = (snnls.snnlssolver)solver.make_copy();
                    _result.fbase = fbase;
                    _result.fm2 = fm2;
                    _result.fm1 = fm1;
                    _result.fp1 = fp1;
                    _result.fp2 = fp2;
                    _result.xm1 = xm1;
                    _result.xp1 = xp1;
                    _result.gm1 = gm1;
                    _result.gp1 = gp1;
                    _result.cidx = cidx;
                    _result.cval = cval;
                    _result.tmpprec = (double[])tmpprec.Clone();
                    _result.nfev = nfev;
                    _result.mcstage = mcstage;
                    _result.stp = stp;
                    _result.curstpmax = curstpmax;
                    _result.activationstep = activationstep;
                    _result.work = (double[])work.Clone();
                    _result.lstate = (linmin.linminstate)lstate.make_copy();
                    _result.trimthreshold = trimthreshold;
                    _result.nonmonotoniccnt = nonmonotoniccnt;
                    _result.k = k;
                    _result.q = q;
                    _result.p = p;
                    _result.rho = (double[])rho.Clone();
                    _result.yk = (double[,])yk.Clone();
                    _result.sk = (double[,])sk.Clone();
                    _result.theta = (double[])theta.Clone();
                    return _result;
                }
            };

            public class minbleicreport : apobject
            {
                public int iterationscount;
                public int nfev;
                public int varidx;
                public int terminationtype;
                public double debugeqerr;
                public double debugfs;
                public double debugff;
                public double debugdx;
                public int debugfeasqpits;
                public int debugfeasgpaits;
                public int inneriterationscount;
                public int outeriterationscount;
                public minbleicreport()
                {
                    init();
                }
                public override void init()
                {
                }
                public override alglib.apobject make_copy()
                {
                    minbleicreport _result = new minbleicreport();
                    _result.iterationscount = iterationscount;
                    _result.nfev = nfev;
                    _result.varidx = varidx;
                    _result.terminationtype = terminationtype;
                    _result.debugeqerr = debugeqerr;
                    _result.debugfs = debugfs;
                    _result.debugff = debugff;
                    _result.debugdx = debugdx;
                    _result.debugfeasqpits = debugfeasqpits;
                    _result.debugfeasgpaits = debugfeasgpaits;
                    _result.inneriterationscount = inneriterationscount;
                    _result.outeriterationscount = outeriterationscount;
                    return _result;
                }
            };
        }

        public class snnls
        {
            /*************************************************************************
            This structure is a SNNLS (Specialized Non-Negative Least Squares) solver.

            It solves problems of the form |A*x-b|^2 => min subject to  non-negativity
            constraints on SOME components of x, with structured A (first  NS  columns
            are just unit matrix, next ND columns store dense part).

            This solver is suited for solution of many sequential NNLS  subproblems  -
            it keeps track of previously allocated memory and reuses  it  as  much  as
            possible.
            *************************************************************************/
            public class snnlssolver : apobject
            {
                public int ns;
                public int nd;
                public int nr;
                public double[,] densea;
                public double[] b;
                public bool[] nnc;
                public int refinementits;
                public double debugflops;
                public int debugmaxnewton;
                public double[] xn;
                public double[,] tmpz;
                public double[,] tmpca;
                public double[] g;
                public double[] d;
                public double[] dx;
                public double[] diagaa;
                public double[] cb;
                public double[] cx;
                public double[] cborg;
                public int[] columnmap;
                public int[] rowmap;
                public double[] tmpcholesky;
                public double[] r;
                public snnlssolver()
                {
                    init();
                }
                public override void init()
                {
                    densea = new double[0, 0];
                    b = new double[0];
                    nnc = new bool[0];
                    xn = new double[0];
                    tmpz = new double[0, 0];
                    tmpca = new double[0, 0];
                    g = new double[0];
                    d = new double[0];
                    dx = new double[0];
                    diagaa = new double[0];
                    cb = new double[0];
                    cx = new double[0];
                    cborg = new double[0];
                    columnmap = new int[0];
                    rowmap = new int[0];
                    tmpcholesky = new double[0];
                    r = new double[0];
                }
                public override alglib.apobject make_copy()
                {
                    snnlssolver _result = new snnlssolver();
                    _result.ns = ns;
                    _result.nd = nd;
                    _result.nr = nr;
                    _result.densea = (double[,])densea.Clone();
                    _result.b = (double[])b.Clone();
                    _result.nnc = (bool[])nnc.Clone();
                    _result.refinementits = refinementits;
                    _result.debugflops = debugflops;
                    _result.debugmaxnewton = debugmaxnewton;
                    _result.xn = (double[])xn.Clone();
                    _result.tmpz = (double[,])tmpz.Clone();
                    _result.tmpca = (double[,])tmpca.Clone();
                    _result.g = (double[])g.Clone();
                    _result.d = (double[])d.Clone();
                    _result.dx = (double[])dx.Clone();
                    _result.diagaa = (double[])diagaa.Clone();
                    _result.cb = (double[])cb.Clone();
                    _result.cx = (double[])cx.Clone();
                    _result.cborg = (double[])cborg.Clone();
                    _result.columnmap = (int[])columnmap.Clone();
                    _result.rowmap = (int[])rowmap.Clone();
                    _result.tmpcholesky = (double[])tmpcholesky.Clone();
                    _result.r = (double[])r.Clone();
                    return _result;
                }
            };


            public const int iterativerefinementits = 3;

        }

        public class linmin
        {
            public class linminstate : apobject
            {
                public bool brackt;
                public bool stage1;
                public int infoc;
                public double dg;
                public double dgm;
                public double dginit;
                public double dgtest;
                public double dgx;
                public double dgxm;
                public double dgy;
                public double dgym;
                public double finit;
                public double ftest1;
                public double fm;
                public double fx;
                public double fxm;
                public double fy;
                public double fym;
                public double stx;
                public double sty;
                public double stmin;
                public double stmax;
                public double width;
                public double width1;
                public double xtrapf;
                public linminstate()
                {
                    init();
                }
                public override void init()
                {
                }
                public override alglib.apobject make_copy()
                {
                    linminstate _result = new linminstate();
                    _result.brackt = brackt;
                    _result.stage1 = stage1;
                    _result.infoc = infoc;
                    _result.dg = dg;
                    _result.dgm = dgm;
                    _result.dginit = dginit;
                    _result.dgtest = dgtest;
                    _result.dgx = dgx;
                    _result.dgxm = dgxm;
                    _result.dgy = dgy;
                    _result.dgym = dgym;
                    _result.finit = finit;
                    _result.ftest1 = ftest1;
                    _result.fm = fm;
                    _result.fx = fx;
                    _result.fxm = fxm;
                    _result.fy = fy;
                    _result.fym = fym;
                    _result.stx = stx;
                    _result.sty = sty;
                    _result.stmin = stmin;
                    _result.stmax = stmax;
                    _result.width = width;
                    _result.width1 = width1;
                    _result.xtrapf = xtrapf;
                    return _result;
                }
            };


            public class armijostate : apobject
            {
                public bool needf;
                public double[] x;
                public double f;
                public int n;
                public double[] xbase;
                public double[] s;
                public double stplen;
                public double fcur;
                public double stpmax;
                public int fmax;
                public int nfev;
                public int info;
                public rcommstate rstate;
                public armijostate()
                {
                    init();
                }
                public override void init()
                {
                    x = new double[0];
                    xbase = new double[0];
                    s = new double[0];
                    rstate = new rcommstate();
                }
                public override alglib.apobject make_copy()
                {
                    armijostate _result = new armijostate();
                    _result.needf = needf;
                    _result.x = (double[])x.Clone();
                    _result.f = f;
                    _result.n = n;
                    _result.xbase = (double[])xbase.Clone();
                    _result.s = (double[])s.Clone();
                    _result.stplen = stplen;
                    _result.fcur = fcur;
                    _result.stpmax = stpmax;
                    _result.fmax = fmax;
                    _result.nfev = nfev;
                    _result.info = info;
                    _result.rstate = (rcommstate)rstate.make_copy();
                    return _result;
                }
            };




            public const double ftol = 0.001;
            public const double xtol = 100 * math.machineepsilon;
            public const int maxfev = 20;
            public const double stpmin = 1.0E-50;
            public const double defstpmax = 1.0E+50;
            public const double armijofactor = 1.3;


           

            private static void mcstep(ref double stx,
                ref double fx,
                ref double dx,
                ref double sty,
                ref double fy,
                ref double dy,
                ref double stp,
                double fp,
                double dp,
                ref bool brackt,
                double stmin,
                double stmax,
                ref int info)
            {
                bool bound = new bool();
                double gamma = 0;
                double p = 0;
                double q = 0;
                double r = 0;
                double s = 0;
                double sgnd = 0;
                double stpc = 0;
                double stpf = 0;
                double stpq = 0;
                double theta = 0;

                info = 0;

                //
                //     CHECK THE INPUT PARAMETERS FOR ERRORS.
                //
                if (((brackt && ((double)(stp) <= (double)(Math.Min(stx, sty)) || (double)(stp) >= (double)(Math.Max(stx, sty)))) || (double)(dx * (stp - stx)) >= (double)(0)) || (double)(stmax) < (double)(stmin))
                {
                    return;
                }

                //
                //     DETERMINE IF THE DERIVATIVES HAVE OPPOSITE SIGN.
                //
                sgnd = dp * (dx / Math.Abs(dx));

                //
                //     FIRST CASE. A HIGHER FUNCTION VALUE.
                //     THE MINIMUM IS BRACKETED. IF THE CUBIC STEP IS CLOSER
                //     TO STX THAN THE QUADRATIC STEP, THE CUBIC STEP IS TAKEN,
                //     ELSE THE AVERAGE OF THE CUBIC AND QUADRATIC STEPS IS TAKEN.
                //
                if ((double)(fp) > (double)(fx))
                {
                    info = 1;
                    bound = true;
                    theta = 3 * (fx - fp) / (stp - stx) + dx + dp;
                    s = Math.Max(Math.Abs(theta), Math.Max(Math.Abs(dx), Math.Abs(dp)));
                    gamma = s * Math.Sqrt(math.sqr(theta / s) - dx / s * (dp / s));
                    if ((double)(stp) < (double)(stx))
                    {
                        gamma = -gamma;
                    }
                    p = gamma - dx + theta;
                    q = gamma - dx + gamma + dp;
                    r = p / q;
                    stpc = stx + r * (stp - stx);
                    stpq = stx + dx / ((fx - fp) / (stp - stx) + dx) / 2 * (stp - stx);
                    if ((double)(Math.Abs(stpc - stx)) < (double)(Math.Abs(stpq - stx)))
                    {
                        stpf = stpc;
                    }
                    else
                    {
                        stpf = stpc + (stpq - stpc) / 2;
                    }
                    brackt = true;
                }
                else
                {
                    if ((double)(sgnd) < (double)(0))
                    {

                        //
                        //     SECOND CASE. A LOWER FUNCTION VALUE AND DERIVATIVES OF
                        //     OPPOSITE SIGN. THE MINIMUM IS BRACKETED. IF THE CUBIC
                        //     STEP IS CLOSER TO STX THAN THE QUADRATIC (SECANT) STEP,
                        //     THE CUBIC STEP IS TAKEN, ELSE THE QUADRATIC STEP IS TAKEN.
                        //
                        info = 2;
                        bound = false;
                        theta = 3 * (fx - fp) / (stp - stx) + dx + dp;
                        s = Math.Max(Math.Abs(theta), Math.Max(Math.Abs(dx), Math.Abs(dp)));
                        gamma = s * Math.Sqrt(math.sqr(theta / s) - dx / s * (dp / s));
                        if ((double)(stp) > (double)(stx))
                        {
                            gamma = -gamma;
                        }
                        p = gamma - dp + theta;
                        q = gamma - dp + gamma + dx;
                        r = p / q;
                        stpc = stp + r * (stx - stp);
                        stpq = stp + dp / (dp - dx) * (stx - stp);
                        if ((double)(Math.Abs(stpc - stp)) > (double)(Math.Abs(stpq - stp)))
                        {
                            stpf = stpc;
                        }
                        else
                        {
                            stpf = stpq;
                        }
                        brackt = true;
                    }
                    else
                    {
                        if ((double)(Math.Abs(dp)) < (double)(Math.Abs(dx)))
                        {

                            //
                            //     THIRD CASE. A LOWER FUNCTION VALUE, DERIVATIVES OF THE
                            //     SAME SIGN, AND THE MAGNITUDE OF THE DERIVATIVE DECREASES.
                            //     THE CUBIC STEP IS ONLY USED IF THE CUBIC TENDS TO INFINITY
                            //     IN THE DIRECTION OF THE STEP OR IF THE MINIMUM OF THE CUBIC
                            //     IS BEYOND STP. OTHERWISE THE CUBIC STEP IS DEFINED TO BE
                            //     EITHER STPMIN OR STPMAX. THE QUADRATIC (SECANT) STEP IS ALSO
                            //     COMPUTED AND IF THE MINIMUM IS BRACKETED THEN THE THE STEP
                            //     CLOSEST TO STX IS TAKEN, ELSE THE STEP FARTHEST AWAY IS TAKEN.
                            //
                            info = 3;
                            bound = true;
                            theta = 3 * (fx - fp) / (stp - stx) + dx + dp;
                            s = Math.Max(Math.Abs(theta), Math.Max(Math.Abs(dx), Math.Abs(dp)));

                            //
                            //        THE CASE GAMMA = 0 ONLY ARISES IF THE CUBIC DOES NOT TEND
                            //        TO INFINITY IN THE DIRECTION OF THE STEP.
                            //
                            gamma = s * Math.Sqrt(Math.Max(0, math.sqr(theta / s) - dx / s * (dp / s)));
                            if ((double)(stp) > (double)(stx))
                            {
                                gamma = -gamma;
                            }
                            p = gamma - dp + theta;
                            q = gamma + (dx - dp) + gamma;
                            r = p / q;
                            if ((double)(r) < (double)(0) && (double)(gamma) != (double)(0))
                            {
                                stpc = stp + r * (stx - stp);
                            }
                            else
                            {
                                if ((double)(stp) > (double)(stx))
                                {
                                    stpc = stmax;
                                }
                                else
                                {
                                    stpc = stmin;
                                }
                            }
                            stpq = stp + dp / (dp - dx) * (stx - stp);
                            if (brackt)
                            {
                                if ((double)(Math.Abs(stp - stpc)) < (double)(Math.Abs(stp - stpq)))
                                {
                                    stpf = stpc;
                                }
                                else
                                {
                                    stpf = stpq;
                                }
                            }
                            else
                            {
                                if ((double)(Math.Abs(stp - stpc)) > (double)(Math.Abs(stp - stpq)))
                                {
                                    stpf = stpc;
                                }
                                else
                                {
                                    stpf = stpq;
                                }
                            }
                        }
                        else
                        {

                            //
                            //     FOURTH CASE. A LOWER FUNCTION VALUE, DERIVATIVES OF THE
                            //     SAME SIGN, AND THE MAGNITUDE OF THE DERIVATIVE DOES
                            //     NOT DECREASE. IF THE MINIMUM IS NOT BRACKETED, THE STEP
                            //     IS EITHER STPMIN OR STPMAX, ELSE THE CUBIC STEP IS TAKEN.
                            //
                            info = 4;
                            bound = false;
                            if (brackt)
                            {
                                theta = 3 * (fp - fy) / (sty - stp) + dy + dp;
                                s = Math.Max(Math.Abs(theta), Math.Max(Math.Abs(dy), Math.Abs(dp)));
                                gamma = s * Math.Sqrt(math.sqr(theta / s) - dy / s * (dp / s));
                                if ((double)(stp) > (double)(sty))
                                {
                                    gamma = -gamma;
                                }
                                p = gamma - dp + theta;
                                q = gamma - dp + gamma + dy;
                                r = p / q;
                                stpc = stp + r * (sty - stp);
                                stpf = stpc;
                            }
                            else
                            {
                                if ((double)(stp) > (double)(stx))
                                {
                                    stpf = stmax;
                                }
                                else
                                {
                                    stpf = stmin;
                                }
                            }
                        }
                    }
                }

                //
                //     UPDATE THE INTERVAL OF UNCERTAINTY. THIS UPDATE DOES NOT
                //     DEPEND ON THE NEW STEP OR THE CASE ANALYSIS ABOVE.
                //
                if ((double)(fp) > (double)(fx))
                {
                    sty = stp;
                    fy = fp;
                    dy = dp;
                }
                else
                {
                    if ((double)(sgnd) < (double)(0.0))
                    {
                        sty = stx;
                        fy = fx;
                        dy = dx;
                    }
                    stx = stp;
                    fx = fp;
                    dx = dp;
                }

                //
                //     COMPUTE THE NEW STEP AND SAFEGUARD IT.
                //
                stpf = Math.Min(stmax, stpf);
                stpf = Math.Max(stmin, stpf);
                stp = stpf;
                if (brackt && bound)
                {
                    if ((double)(sty) > (double)(stx))
                    {
                        stp = Math.Min(stx + 0.66 * (sty - stx), stp);
                    }
                    else
                    {
                        stp = Math.Max(stx + 0.66 * (sty - stx), stp);
                    }
                }
            }


        }

        public class hqrnd
        {
            /*************************************************************************
            Portable high quality random number generator state.
            Initialized with HQRNDRandomize() or HQRNDSeed().

            Fields:
                S1, S2      -   seed values
                V           -   precomputed value
                MagicV      -   'magic' value used to determine whether State structure
                                was correctly initialized.
            *************************************************************************/
            public class hqrndstate : apobject
            {
                public int s1;
                public int s2;
                public int magicv;
                public hqrndstate()
                {
                    init();
                }
                public override void init()
                {
                }
                public override alglib.apobject make_copy()
                {
                    hqrndstate _result = new hqrndstate();
                    _result.s1 = s1;
                    _result.s2 = s2;
                    _result.magicv = magicv;
                    return _result;
                }
            };




            public const int hqrndmax = 2147483561;
            public const int hqrndm1 = 2147483563;
            public const int hqrndm2 = 2147483399;
            public const int hqrndmagic = 1634357784;

            /*************************************************************************
            This function generates random integer number in [0, N)

            1. State structure must be initialized with HQRNDRandomize() or HQRNDSeed()
            2. N can be any positive number except for very large numbers:
               * close to 2^31 on 32-bit systems
               * close to 2^62 on 64-bit systems
               An exception will be generated if N is too large.

              -- ALGLIB --
                 Copyright 02.12.2009 by Bochkanov Sergey
            *************************************************************************/
            public static int hqrnduniformi(hqrndstate state,
                int n)
            {
                int result = 0;
                int maxcnt = 0;
                int mx = 0;
                int a = 0;
                int b = 0;

                alglib.ap.assert(n > 0, "HQRNDUniformI: N<=0!");
                maxcnt = hqrndmax + 1;

                //
                // Two branches: one for N<=MaxCnt, another for N>MaxCnt.
                //
                if (n > maxcnt)
                {

                    //
                    // N>=MaxCnt.
                    //
                    // We have two options here:
                    // a) N is exactly divisible by MaxCnt
                    // b) N is not divisible by MaxCnt
                    //
                    // In both cases we reduce problem on interval spanning [0,N)
                    // to several subproblems on intervals spanning [0,MaxCnt).
                    //
                    if (n % maxcnt == 0)
                    {

                        //
                        // N is exactly divisible by MaxCnt.
                        //
                        // [0,N) range is dividided into N/MaxCnt bins,
                        // each of them having length equal to MaxCnt.
                        //
                        // We generate:
                        // * random bin number B
                        // * random offset within bin A
                        // Both random numbers are generated by recursively
                        // calling HQRNDUniformI().
                        //
                        // Result is equal to A+MaxCnt*B.
                        //
                        alglib.ap.assert(n / maxcnt <= maxcnt, "HQRNDUniformI: N is too large");
                        a = hqrnduniformi(state, maxcnt);
                        b = hqrnduniformi(state, n / maxcnt);
                        result = a + maxcnt * b;
                    }
                    else
                    {

                        //
                        // N is NOT exactly divisible by MaxCnt.
                        //
                        // [0,N) range is dividided into Ceil(N/MaxCnt) bins,
                        // each of them having length equal to MaxCnt.
                        //
                        // We generate:
                        // * random bin number B in [0, Ceil(N/MaxCnt)-1]
                        // * random offset within bin A
                        // * if both of what is below is true
                        //   1) bin number B is that of the last bin
                        //   2) A >= N mod MaxCnt
                        //   then we repeat generation of A/B.
                        //   This stage is essential in order to avoid bias in the result.
                        // * otherwise, we return A*MaxCnt+N
                        //
                        alglib.ap.assert(n / maxcnt + 1 <= maxcnt, "HQRNDUniformI: N is too large");
                        result = -1;
                        do
                        {
                            a = hqrnduniformi(state, maxcnt);
                            b = hqrnduniformi(state, n / maxcnt + 1);
                            if (b == n / maxcnt && a >= n % maxcnt)
                            {
                                continue;
                            }
                            result = a + maxcnt * b;
                        }
                        while (result < 0);
                    }
                }
                else
                {

                    //
                    // N<=MaxCnt
                    //
                    // Code below is a bit complicated because we can not simply
                    // return "HQRNDIntegerBase() mod N" - it will be skewed for
                    // large N's in [0.1*HQRNDMax...HQRNDMax].
                    //
                    mx = maxcnt - maxcnt % n;
                    do
                    {
                        result = hqrndintegerbase(state);
                    }
                    while (result >= mx);
                    result = result % n;
                }
                return result;
            }


           
            /*************************************************************************
            This function returns random integer in [0,HQRNDMax]

            L'Ecuyer, Efficient and portable combined random number generators
            *************************************************************************/
            private static int hqrndintegerbase(hqrndstate state)
            {
                int result = 0;
                int k = 0;

                alglib.ap.assert(state.magicv == hqrndmagic, "HQRNDIntegerBase: State is not correctly initialized!");
                k = state.s1 / 53668;
                state.s1 = 40014 * (state.s1 - k * 53668) - k * 12211;
                if (state.s1 < 0)
                {
                    state.s1 = state.s1 + 2147483563;
                }
                k = state.s2 / 52774;
                state.s2 = 40692 * (state.s2 - k * 52774) - k * 3791;
                if (state.s2 < 0)
                {
                    state.s2 = state.s2 + 2147483399;
                }

                //
                // Result
                //
                result = state.s1 - state.s2;
                if (result < 1)
                {
                    result = result + 2147483562;
                }
                result = result - 1;
                return result;
            }


        }

        public class serializer
        {
            enum SMODE { DEFAULT, ALLOC, TO_STRING, FROM_STRING };
            private const int SER_ENTRIES_PER_ROW = 5;
            private const int SER_ENTRY_LENGTH = 11;

            private SMODE mode;
            private int entries_needed;
            private int entries_saved;
            private int bytes_asked;
            private int bytes_written;
            private int bytes_read;
            private char[] out_str;
            private char[] in_str;

            


            /************************************************************************
            This function converts six-bit value (from 0 to 63)  to  character  (only
            digits, lowercase and uppercase letters, minus and underscore are used).

            If v is negative or greater than 63, this function returns '?'.
            ************************************************************************/
            private static char[] _sixbits2char_tbl = new char[64]{ 
                '0', '1', '2', '3', '4', '5', '6', '7',
                '8', '9', 'A', 'B', 'C', 'D', 'E', 'F',
                'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N',
                'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
                'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 
                'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 
                'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 
                'u', 'v', 'w', 'x', 'y', 'z', '-', '_' };
            private static char sixbits2char(int v)
            {
                if (v < 0 || v > 63)
                    return '?';
                return _sixbits2char_tbl[v];
            }

            /************************************************************************
            This function converts character to six-bit value (from 0 to 63).

            This function is inverse of ae_sixbits2char()
            If c is not correct character, this function returns -1.
            ************************************************************************/
            private static int[] _char2sixbits_tbl = new int[128] {
            -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, 62, -1, -1,
             0,  1,  2,  3,  4,  5,  6,  7,
             8,  9, -1, -1, -1, -1, -1, -1,
            -1, 10, 11, 12, 13, 14, 15, 16,
            17, 18, 19, 20, 21, 22, 23, 24,
            25, 26, 27, 28, 29, 30, 31, 32,
            33, 34, 35, -1, -1, -1, -1, 63,
            -1, 36, 37, 38, 39, 40, 41, 42,
            43, 44, 45, 46, 47, 48, 49, 50,
            51, 52, 53, 54, 55, 56, 57, 58,
            59, 60, 61, -1, -1, -1, -1, -1 };
         

        }

        public class tsort
        {
            
            /*************************************************************************
            Internal TagSortFastI: sorts A[I1...I2] (both bounds are included),
            applies same permutations to B.

              -- ALGLIB --
                 Copyright 06.09.2010 by Bochkanov Sergey
            *************************************************************************/
            private static void tagsortfastirec(ref double[] a,
                ref int[] b,
                ref double[] bufa,
                ref int[] bufb,
                int i1,
                int i2)
            {
                int i = 0;
                int j = 0;
                int k = 0;
                int cntless = 0;
                int cnteq = 0;
                int cntgreater = 0;
                double tmpr = 0;
                int tmpi = 0;
                double v0 = 0;
                double v1 = 0;
                double v2 = 0;
                double vp = 0;


                //
                // Fast exit
                //
                if (i2 <= i1)
                {
                    return;
                }

                //
                // Non-recursive sort for small arrays
                //
                if (i2 - i1 <= 16)
                {
                    for (j = i1 + 1; j <= i2; j++)
                    {

                        //
                        // Search elements [I1..J-1] for place to insert Jth element.
                        //
                        // This code stops immediately if we can leave A[J] at J-th position
                        // (all elements have same value of A[J] larger than any of them)
                        //
                        tmpr = a[j];
                        tmpi = j;
                        for (k = j - 1; k >= i1; k--)
                        {
                            if (a[k] <= tmpr)
                            {
                                break;
                            }
                            tmpi = k;
                        }
                        k = tmpi;

                        //
                        // Insert Jth element into Kth position
                        //
                        if (k != j)
                        {
                            tmpr = a[j];
                            tmpi = b[j];
                            for (i = j - 1; i >= k; i--)
                            {
                                a[i + 1] = a[i];
                                b[i + 1] = b[i];
                            }
                            a[k] = tmpr;
                            b[k] = tmpi;
                        }
                    }
                    return;
                }

                //
                // Quicksort: choose pivot
                // Here we assume that I2-I1>=2
                //
                v0 = a[i1];
                v1 = a[i1 + (i2 - i1) / 2];
                v2 = a[i2];
                if (v0 > v1)
                {
                    tmpr = v1;
                    v1 = v0;
                    v0 = tmpr;
                }
                if (v1 > v2)
                {
                    tmpr = v2;
                    v2 = v1;
                    v1 = tmpr;
                }
                if (v0 > v1)
                {
                    tmpr = v1;
                    v1 = v0;
                    v0 = tmpr;
                }
                vp = v1;

                //
                // now pass through A/B and:
                // * move elements that are LESS than VP to the left of A/B
                // * move elements that are EQUAL to VP to the right of BufA/BufB (in the reverse order)
                // * move elements that are GREATER than VP to the left of BufA/BufB (in the normal order
                // * move elements from the tail of BufA/BufB to the middle of A/B (restoring normal order)
                // * move elements from the left of BufA/BufB to the end of A/B
                //
                cntless = 0;
                cnteq = 0;
                cntgreater = 0;
                for (i = i1; i <= i2; i++)
                {
                    v0 = a[i];
                    if (v0 < vp)
                    {

                        //
                        // LESS
                        //
                        k = i1 + cntless;
                        if (i != k)
                        {
                            a[k] = v0;
                            b[k] = b[i];
                        }
                        cntless = cntless + 1;
                        continue;
                    }
                    if (v0 == vp)
                    {

                        //
                        // EQUAL
                        //
                        k = i2 - cnteq;
                        bufa[k] = v0;
                        bufb[k] = b[i];
                        cnteq = cnteq + 1;
                        continue;
                    }

                    //
                    // GREATER
                    //
                    k = i1 + cntgreater;
                    bufa[k] = v0;
                    bufb[k] = b[i];
                    cntgreater = cntgreater + 1;
                }
                for (i = 0; i <= cnteq - 1; i++)
                {
                    j = i1 + cntless + cnteq - 1 - i;
                    k = i2 + i - (cnteq - 1);
                    a[j] = bufa[k];
                    b[j] = bufb[k];
                }
                for (i = 0; i <= cntgreater - 1; i++)
                {
                    j = i1 + cntless + cnteq + i;
                    k = i1 + i;
                    a[j] = bufa[k];
                    b[j] = bufb[k];
                }

                //
                // Sort left and right parts of the array (ignoring middle part)
                //
                tagsortfastirec(ref a, ref b, ref bufa, ref bufb, i1, i1 + cntless - 1);
                tagsortfastirec(ref a, ref b, ref bufa, ref bufb, i1 + cntless + cnteq, i2);
            }


            /*************************************************************************
            Internal TagSortFastR: sorts A[I1...I2] (both bounds are included),
            applies same permutations to B.

              -- ALGLIB --
                 Copyright 06.09.2010 by Bochkanov Sergey
            *************************************************************************/
            private static void tagsortfastrrec(ref double[] a,
                ref double[] b,
                ref double[] bufa,
                ref double[] bufb,
                int i1,
                int i2)
            {
                int i = 0;
                int j = 0;
                int k = 0;
                double tmpr = 0;
                double tmpr2 = 0;
                int tmpi = 0;
                int cntless = 0;
                int cnteq = 0;
                int cntgreater = 0;
                double v0 = 0;
                double v1 = 0;
                double v2 = 0;
                double vp = 0;


                //
                // Fast exit
                //
                if (i2 <= i1)
                {
                    return;
                }

                //
                // Non-recursive sort for small arrays
                //
                if (i2 - i1 <= 16)
                {
                    for (j = i1 + 1; j <= i2; j++)
                    {

                        //
                        // Search elements [I1..J-1] for place to insert Jth element.
                        //
                        // This code stops immediatly if we can leave A[J] at J-th position
                        // (all elements have same value of A[J] larger than any of them)
                        //
                        tmpr = a[j];
                        tmpi = j;
                        for (k = j - 1; k >= i1; k--)
                        {
                            if (a[k] <= tmpr)
                            {
                                break;
                            }
                            tmpi = k;
                        }
                        k = tmpi;

                        //
                        // Insert Jth element into Kth position
                        //
                        if (k != j)
                        {
                            tmpr = a[j];
                            tmpr2 = b[j];
                            for (i = j - 1; i >= k; i--)
                            {
                                a[i + 1] = a[i];
                                b[i + 1] = b[i];
                            }
                            a[k] = tmpr;
                            b[k] = tmpr2;
                        }
                    }
                    return;
                }

                //
                // Quicksort: choose pivot
                // Here we assume that I2-I1>=16
                //
                v0 = a[i1];
                v1 = a[i1 + (i2 - i1) / 2];
                v2 = a[i2];
                if (v0 > v1)
                {
                    tmpr = v1;
                    v1 = v0;
                    v0 = tmpr;
                }
                if (v1 > v2)
                {
                    tmpr = v2;
                    v2 = v1;
                    v1 = tmpr;
                }
                if (v0 > v1)
                {
                    tmpr = v1;
                    v1 = v0;
                    v0 = tmpr;
                }
                vp = v1;

                //
                // now pass through A/B and:
                // * move elements that are LESS than VP to the left of A/B
                // * move elements that are EQUAL to VP to the right of BufA/BufB (in the reverse order)
                // * move elements that are GREATER than VP to the left of BufA/BufB (in the normal order
                // * move elements from the tail of BufA/BufB to the middle of A/B (restoring normal order)
                // * move elements from the left of BufA/BufB to the end of A/B
                //
                cntless = 0;
                cnteq = 0;
                cntgreater = 0;
                for (i = i1; i <= i2; i++)
                {
                    v0 = a[i];
                    if (v0 < vp)
                    {

                        //
                        // LESS
                        //
                        k = i1 + cntless;
                        if (i != k)
                        {
                            a[k] = v0;
                            b[k] = b[i];
                        }
                        cntless = cntless + 1;
                        continue;
                    }
                    if (v0 == vp)
                    {

                        //
                        // EQUAL
                        //
                        k = i2 - cnteq;
                        bufa[k] = v0;
                        bufb[k] = b[i];
                        cnteq = cnteq + 1;
                        continue;
                    }

                    //
                    // GREATER
                    //
                    k = i1 + cntgreater;
                    bufa[k] = v0;
                    bufb[k] = b[i];
                    cntgreater = cntgreater + 1;
                }
                for (i = 0; i <= cnteq - 1; i++)
                {
                    j = i1 + cntless + cnteq - 1 - i;
                    k = i2 + i - (cnteq - 1);
                    a[j] = bufa[k];
                    b[j] = bufb[k];
                }
                for (i = 0; i <= cntgreater - 1; i++)
                {
                    j = i1 + cntless + cnteq + i;
                    k = i1 + i;
                    a[j] = bufa[k];
                    b[j] = bufb[k];
                }

                //
                // Sort left and right parts of the array (ignoring middle part)
                //
                tagsortfastrrec(ref a, ref b, ref bufa, ref bufb, i1, i1 + cntless - 1);
                tagsortfastrrec(ref a, ref b, ref bufa, ref bufb, i1 + cntless + cnteq, i2);
            }


            /*************************************************************************
            Internal TagSortFastI: sorts A[I1...I2] (both bounds are included),
            applies same permutations to B.

              -- ALGLIB --
                 Copyright 06.09.2010 by Bochkanov Sergey
            *************************************************************************/
            private static void tagsortfastrec(ref double[] a,
                ref double[] bufa,
                int i1,
                int i2)
            {
                int cntless = 0;
                int cnteq = 0;
                int cntgreater = 0;
                int i = 0;
                int j = 0;
                int k = 0;
                double tmpr = 0;
                int tmpi = 0;
                double v0 = 0;
                double v1 = 0;
                double v2 = 0;
                double vp = 0;


                //
                // Fast exit
                //
                if (i2 <= i1)
                {
                    return;
                }

                //
                // Non-recursive sort for small arrays
                //
                if (i2 - i1 <= 16)
                {
                    for (j = i1 + 1; j <= i2; j++)
                    {

                        //
                        // Search elements [I1..J-1] for place to insert Jth element.
                        //
                        // This code stops immediatly if we can leave A[J] at J-th position
                        // (all elements have same value of A[J] larger than any of them)
                        //
                        tmpr = a[j];
                        tmpi = j;
                        for (k = j - 1; k >= i1; k--)
                        {
                            if (a[k] <= tmpr)
                            {
                                break;
                            }
                            tmpi = k;
                        }
                        k = tmpi;

                        //
                        // Insert Jth element into Kth position
                        //
                        if (k != j)
                        {
                            tmpr = a[j];
                            for (i = j - 1; i >= k; i--)
                            {
                                a[i + 1] = a[i];
                            }
                            a[k] = tmpr;
                        }
                    }
                    return;
                }

                //
                // Quicksort: choose pivot
                // Here we assume that I2-I1>=16
                //
                v0 = a[i1];
                v1 = a[i1 + (i2 - i1) / 2];
                v2 = a[i2];
                if (v0 > v1)
                {
                    tmpr = v1;
                    v1 = v0;
                    v0 = tmpr;
                }
                if (v1 > v2)
                {
                    tmpr = v2;
                    v2 = v1;
                    v1 = tmpr;
                }
                if (v0 > v1)
                {
                    tmpr = v1;
                    v1 = v0;
                    v0 = tmpr;
                }
                vp = v1;

                //
                // now pass through A/B and:
                // * move elements that are LESS than VP to the left of A/B
                // * move elements that are EQUAL to VP to the right of BufA/BufB (in the reverse order)
                // * move elements that are GREATER than VP to the left of BufA/BufB (in the normal order
                // * move elements from the tail of BufA/BufB to the middle of A/B (restoring normal order)
                // * move elements from the left of BufA/BufB to the end of A/B
                //
                cntless = 0;
                cnteq = 0;
                cntgreater = 0;
                for (i = i1; i <= i2; i++)
                {
                    v0 = a[i];
                    if (v0 < vp)
                    {

                        //
                        // LESS
                        //
                        k = i1 + cntless;
                        if (i != k)
                        {
                            a[k] = v0;
                        }
                        cntless = cntless + 1;
                        continue;
                    }
                    if (v0 == vp)
                    {

                        //
                        // EQUAL
                        //
                        k = i2 - cnteq;
                        bufa[k] = v0;
                        cnteq = cnteq + 1;
                        continue;
                    }

                    //
                    // GREATER
                    //
                    k = i1 + cntgreater;
                    bufa[k] = v0;
                    cntgreater = cntgreater + 1;
                }
                for (i = 0; i <= cnteq - 1; i++)
                {
                    j = i1 + cntless + cnteq - 1 - i;
                    k = i2 + i - (cnteq - 1);
                    a[j] = bufa[k];
                }
                for (i = 0; i <= cntgreater - 1; i++)
                {
                    j = i1 + cntless + cnteq + i;
                    k = i1 + i;
                    a[j] = bufa[k];
                }

                //
                // Sort left and right parts of the array (ignoring middle part)
                //
                tagsortfastrec(ref a, ref bufa, i1, i1 + cntless - 1);
                tagsortfastrec(ref a, ref bufa, i1 + cntless + cnteq, i2);
            }


        }

        public class ablas
        {
            public const int rgemmparallelsize = 64;
            public const int cgemmparallelsize = 64;


            /*************************************************************************
            Splits matrix length in two parts, left part should match ABLAS block size

            INPUT PARAMETERS
                A   -   real matrix, is passed to ensure that we didn't split
                        complex matrix using real splitting subroutine.
                        matrix itself is not changed.
                N   -   length, N>0

            OUTPUT PARAMETERS
                N1  -   length
                N2  -   length

            N1+N2=N, N1>=N2, N2 may be zero

              -- ALGLIB routine --
                 15.12.2009
                 Bochkanov Sergey
            *************************************************************************/
            public static void ablassplitlength(double[,] a,
                int n,
                ref int n1,
                ref int n2)
            {
                n1 = 0;
                n2 = 0;

                if (n > ablasblocksize(a))
                {
                    ablasinternalsplitlength(n, ablasblocksize(a), ref n1, ref n2);
                }
                else
                {
                    ablasinternalsplitlength(n, ablasmicroblocksize(), ref n1, ref n2);
                }
            }


            /*************************************************************************
            Complex ABLASSplitLength

              -- ALGLIB routine --
                 15.12.2009
                 Bochkanov Sergey
            *************************************************************************/
            public static void ablascomplexsplitlength(complex[,] a,
                int n,
                ref int n1,
                ref int n2)
            {
                n1 = 0;
                n2 = 0;

                if (n > ablascomplexblocksize(a))
                {
                    ablasinternalsplitlength(n, ablascomplexblocksize(a), ref n1, ref n2);
                }
                else
                {
                    ablasinternalsplitlength(n, ablasmicroblocksize(), ref n1, ref n2);
                }
            }


            /*************************************************************************
            Returns block size - subdivision size where  cache-oblivious  soubroutines
            switch to the optimized kernel.

            INPUT PARAMETERS
                A   -   real matrix, is passed to ensure that we didn't split
                        complex matrix using real splitting subroutine.
                        matrix itself is not changed.

              -- ALGLIB routine --
                 15.12.2009
                 Bochkanov Sergey
            *************************************************************************/
            public static int ablasblocksize(double[,] a)
            {
                int result = 0;

                result = 32;
                return result;
            }


            /*************************************************************************
            Block size for complex subroutines.

              -- ALGLIB routine --
                 15.12.2009
                 Bochkanov Sergey
            *************************************************************************/
            public static int ablascomplexblocksize(complex[,] a)
            {
                int result = 0;

                result = 24;
                return result;
            }


            /*************************************************************************
            Microblock size

              -- ALGLIB routine --
                 15.12.2009
                 Bochkanov Sergey
            *************************************************************************/
            public static int ablasmicroblocksize()
            {
                int result = 0;

                result = 8;
                return result;
            }


            /*************************************************************************
            Cache-oblivous complex "copy-and-transpose"

            Input parameters:
                M   -   number of rows
                N   -   number of columns
                A   -   source matrix, MxN submatrix is copied and transposed
                IA  -   submatrix offset (row index)
                JA  -   submatrix offset (column index)
                B   -   destination matrix, must be large enough to store result
                IB  -   submatrix offset (row index)
                JB  -   submatrix offset (column index)
            *************************************************************************/
            public static void cmatrixtranspose(int m,
                int n,
                complex[,] a,
                int ia,
                int ja,
                ref complex[,] b,
                int ib,
                int jb)
            {
                int i = 0;
                int s1 = 0;
                int s2 = 0;
                int i_ = 0;
                int i1_ = 0;

                if (m <= 2 * ablascomplexblocksize(a) && n <= 2 * ablascomplexblocksize(a))
                {

                    //
                    // base case
                    //
                    for (i = 0; i <= m - 1; i++)
                    {
                        i1_ = (ja) - (ib);
                        for (i_ = ib; i_ <= ib + n - 1; i_++)
                        {
                            b[i_, jb + i] = a[ia + i, i_ + i1_];
                        }
                    }
                }
                else
                {

                    //
                    // Cache-oblivious recursion
                    //
                    if (m > n)
                    {
                        ablascomplexsplitlength(a, m, ref s1, ref s2);
                        cmatrixtranspose(s1, n, a, ia, ja, ref b, ib, jb);
                        cmatrixtranspose(s2, n, a, ia + s1, ja, ref b, ib, jb + s1);
                    }
                    else
                    {
                        ablascomplexsplitlength(a, n, ref s1, ref s2);
                        cmatrixtranspose(m, s1, a, ia, ja, ref b, ib, jb);
                        cmatrixtranspose(m, s2, a, ia, ja + s1, ref b, ib + s1, jb);
                    }
                }
            }


            /*************************************************************************
            Cache-oblivous real "copy-and-transpose"

            Input parameters:
                M   -   number of rows
                N   -   number of columns
                A   -   source matrix, MxN submatrix is copied and transposed
                IA  -   submatrix offset (row index)
                JA  -   submatrix offset (column index)
                B   -   destination matrix, must be large enough to store result
                IB  -   submatrix offset (row index)
                JB  -   submatrix offset (column index)
            *************************************************************************/
            public static void rmatrixtranspose(int m,
                int n,
                double[,] a,
                int ia,
                int ja,
                ref double[,] b,
                int ib,
                int jb)
            {
                int i = 0;
                int s1 = 0;
                int s2 = 0;
                int i_ = 0;
                int i1_ = 0;

                if (m <= 2 * ablasblocksize(a) && n <= 2 * ablasblocksize(a))
                {

                    //
                    // base case
                    //
                    for (i = 0; i <= m - 1; i++)
                    {
                        i1_ = (ja) - (ib);
                        for (i_ = ib; i_ <= ib + n - 1; i_++)
                        {
                            b[i_, jb + i] = a[ia + i, i_ + i1_];
                        }
                    }
                }
                else
                {

                    //
                    // Cache-oblivious recursion
                    //
                    if (m > n)
                    {
                        ablassplitlength(a, m, ref s1, ref s2);
                        rmatrixtranspose(s1, n, a, ia, ja, ref b, ib, jb);
                        rmatrixtranspose(s2, n, a, ia + s1, ja, ref b, ib, jb + s1);
                    }
                    else
                    {
                        ablassplitlength(a, n, ref s1, ref s2);
                        rmatrixtranspose(m, s1, a, ia, ja, ref b, ib, jb);
                        rmatrixtranspose(m, s2, a, ia, ja + s1, ref b, ib + s1, jb);
                    }
                }
            }


            /*************************************************************************
            Copy

            Input parameters:
                M   -   number of rows
                N   -   number of columns
                A   -   source matrix, MxN submatrix is copied and transposed
                IA  -   submatrix offset (row index)
                JA  -   submatrix offset (column index)
                B   -   destination matrix, must be large enough to store result
                IB  -   submatrix offset (row index)
                JB  -   submatrix offset (column index)
            *************************************************************************/
            public static void rmatrixcopy(int m,
                int n,
                double[,] a,
                int ia,
                int ja,
                ref double[,] b,
                int ib,
                int jb)
            {
                int i = 0;
                int i_ = 0;
                int i1_ = 0;

                if (m == 0 || n == 0)
                {
                    return;
                }
                for (i = 0; i <= m - 1; i++)
                {
                    i1_ = (ja) - (jb);
                    for (i_ = jb; i_ <= jb + n - 1; i_++)
                    {
                        b[ib + i, i_] = a[ia + i, i_ + i1_];
                    }
                }
            }


          


            /*************************************************************************
            Matrix-vector product: y := op(A)*x

            INPUT PARAMETERS:
                M   -   number of rows of op(A)
                N   -   number of columns of op(A)
                A   -   target matrix
                IA  -   submatrix offset (row index)
                JA  -   submatrix offset (column index)
                OpA -   operation type:
                        * OpA=0     =>  op(A) = A
                        * OpA=1     =>  op(A) = A^T
                X   -   input vector
                IX  -   subvector offset
                IY  -   subvector offset
                Y   -   preallocated matrix, must be large enough to store result

            OUTPUT PARAMETERS:
                Y   -   vector which stores result

            if M=0, then subroutine does nothing.
            if N=0, Y is filled by zeros.


              -- ALGLIB routine --

                 28.01.2010
                 Bochkanov Sergey
            *************************************************************************/
            public static void rmatrixmv(int m,
                int n,
                double[,] a,
                int ia,
                int ja,
                int opa,
                double[] x,
                int ix,
                ref double[] y,
                int iy)
            {
                int i = 0;
                double v = 0;
                int i_ = 0;
                int i1_ = 0;

                if (m == 0)
                {
                    return;
                }
                if (n == 0)
                {
                    for (i = 0; i <= m - 1; i++)
                    {
                        y[iy + i] = 0;
                    }
                    return;
                }
                if (ablasf.rmatrixmvf(m, n, a, ia, ja, opa, x, ix, ref y, iy))
                {
                    return;
                }
                if (opa == 0)
                {

                    //
                    // y = A*x
                    //
                    for (i = 0; i <= m - 1; i++)
                    {
                        i1_ = (ix) - (ja);
                        v = 0.0;
                        for (i_ = ja; i_ <= ja + n - 1; i_++)
                        {
                            v += a[ia + i, i_] * x[i_ + i1_];
                        }
                        y[iy + i] = v;
                    }
                    return;
                }
                if (opa == 1)
                {

                    //
                    // y = A^T*x
                    //
                    for (i = 0; i <= m - 1; i++)
                    {
                        y[iy + i] = 0;
                    }
                    for (i = 0; i <= n - 1; i++)
                    {
                        v = x[ix + i];
                        i1_ = (ja) - (iy);
                        for (i_ = iy; i_ <= iy + m - 1; i_++)
                        {
                            y[i_] = y[i_] + v * a[ia + i, i_ + i1_];
                        }
                    }
                    return;
                }
            }


            public static void cmatrixrighttrsm(int m,
                int n,
                complex[,] a,
                int i1,
                int j1,
                bool isupper,
                bool isunit,
                int optype,
                complex[,] x,
                int i2,
                int j2)
            {
                int s1 = 0;
                int s2 = 0;
                int bs = 0;

                bs = ablascomplexblocksize(a);
                if (m <= bs && n <= bs)
                {
                    cmatrixrighttrsm2(m, n, a, i1, j1, isupper, isunit, optype, x, i2, j2);
                    return;
                }
                if (m >= n)
                {

                    //
                    // Split X: X*A = (X1 X2)^T*A
                    //
                    ablascomplexsplitlength(a, m, ref s1, ref s2);
                    cmatrixrighttrsm(s1, n, a, i1, j1, isupper, isunit, optype, x, i2, j2);
                    cmatrixrighttrsm(s2, n, a, i1, j1, isupper, isunit, optype, x, i2 + s1, j2);
                    return;
                }
                else
                {

                    //
                    // Split A:
                    //               (A1  A12)
                    // X*op(A) = X*op(       )
                    //               (     A2)
                    //
                    // Different variants depending on
                    // IsUpper/OpType combinations
                    //
                    ablascomplexsplitlength(a, n, ref s1, ref s2);
                    if (isupper && optype == 0)
                    {

                        //
                        //                  (A1  A12)-1
                        // X*A^-1 = (X1 X2)*(       )
                        //                  (     A2)
                        //
                        cmatrixrighttrsm(m, s1, a, i1, j1, isupper, isunit, optype, x, i2, j2);
                        cmatrixgemm(m, s2, s1, -1.0, x, i2, j2, 0, a, i1, j1 + s1, 0, 1.0, x, i2, j2 + s1);
                        cmatrixrighttrsm(m, s2, a, i1 + s1, j1 + s1, isupper, isunit, optype, x, i2, j2 + s1);
                        return;
                    }
                    if (isupper && optype != 0)
                    {

                        //
                        //                  (A1'     )-1
                        // X*A^-1 = (X1 X2)*(        )
                        //                  (A12' A2')
                        //
                        cmatrixrighttrsm(m, s2, a, i1 + s1, j1 + s1, isupper, isunit, optype, x, i2, j2 + s1);
                        cmatrixgemm(m, s1, s2, -1.0, x, i2, j2 + s1, 0, a, i1, j1 + s1, optype, 1.0, x, i2, j2);
                        cmatrixrighttrsm(m, s1, a, i1, j1, isupper, isunit, optype, x, i2, j2);
                        return;
                    }
                    if (!isupper && optype == 0)
                    {

                        //
                        //                  (A1     )-1
                        // X*A^-1 = (X1 X2)*(       )
                        //                  (A21  A2)
                        //
                        cmatrixrighttrsm(m, s2, a, i1 + s1, j1 + s1, isupper, isunit, optype, x, i2, j2 + s1);
                        cmatrixgemm(m, s1, s2, -1.0, x, i2, j2 + s1, 0, a, i1 + s1, j1, 0, 1.0, x, i2, j2);
                        cmatrixrighttrsm(m, s1, a, i1, j1, isupper, isunit, optype, x, i2, j2);
                        return;
                    }
                    if (!isupper && optype != 0)
                    {

                        //
                        //                  (A1' A21')-1
                        // X*A^-1 = (X1 X2)*(        )
                        //                  (     A2')
                        //
                        cmatrixrighttrsm(m, s1, a, i1, j1, isupper, isunit, optype, x, i2, j2);
                        cmatrixgemm(m, s2, s1, -1.0, x, i2, j2, 0, a, i1 + s1, j1, optype, 1.0, x, i2, j2 + s1);
                        cmatrixrighttrsm(m, s2, a, i1 + s1, j1 + s1, isupper, isunit, optype, x, i2, j2 + s1);
                        return;
                    }
                }
            }


          

            public static void cmatrixlefttrsm(int m,
                int n,
                complex[,] a,
                int i1,
                int j1,
                bool isupper,
                bool isunit,
                int optype,
                complex[,] x,
                int i2,
                int j2)
            {
                int s1 = 0;
                int s2 = 0;
                int bs = 0;

                bs = ablascomplexblocksize(a);
                if (m <= bs && n <= bs)
                {
                    cmatrixlefttrsm2(m, n, a, i1, j1, isupper, isunit, optype, x, i2, j2);
                    return;
                }
                if (n >= m)
                {

                    //
                    // Split X: op(A)^-1*X = op(A)^-1*(X1 X2)
                    //
                    ablascomplexsplitlength(x, n, ref s1, ref s2);
                    cmatrixlefttrsm(m, s1, a, i1, j1, isupper, isunit, optype, x, i2, j2);
                    cmatrixlefttrsm(m, s2, a, i1, j1, isupper, isunit, optype, x, i2, j2 + s1);
                    return;
                }
                else
                {

                    //
                    // Split A
                    //
                    ablascomplexsplitlength(a, m, ref s1, ref s2);
                    if (isupper && optype == 0)
                    {

                        //
                        //           (A1  A12)-1  ( X1 )
                        // A^-1*X* = (       )   *(    )
                        //           (     A2)    ( X2 )
                        //
                        cmatrixlefttrsm(s2, n, a, i1 + s1, j1 + s1, isupper, isunit, optype, x, i2 + s1, j2);
                        cmatrixgemm(s1, n, s2, -1.0, a, i1, j1 + s1, 0, x, i2 + s1, j2, 0, 1.0, x, i2, j2);
                        cmatrixlefttrsm(s1, n, a, i1, j1, isupper, isunit, optype, x, i2, j2);
                        return;
                    }
                    if (isupper && optype != 0)
                    {

                        //
                        //          (A1'     )-1 ( X1 )
                        // A^-1*X = (        )  *(    )
                        //          (A12' A2')   ( X2 )
                        //
                        cmatrixlefttrsm(s1, n, a, i1, j1, isupper, isunit, optype, x, i2, j2);
                        cmatrixgemm(s2, n, s1, -1.0, a, i1, j1 + s1, optype, x, i2, j2, 0, 1.0, x, i2 + s1, j2);
                        cmatrixlefttrsm(s2, n, a, i1 + s1, j1 + s1, isupper, isunit, optype, x, i2 + s1, j2);
                        return;
                    }
                    if (!isupper && optype == 0)
                    {

                        //
                        //          (A1     )-1 ( X1 )
                        // A^-1*X = (       )  *(    )
                        //          (A21  A2)   ( X2 )
                        //
                        cmatrixlefttrsm(s1, n, a, i1, j1, isupper, isunit, optype, x, i2, j2);
                        cmatrixgemm(s2, n, s1, -1.0, a, i1 + s1, j1, 0, x, i2, j2, 0, 1.0, x, i2 + s1, j2);
                        cmatrixlefttrsm(s2, n, a, i1 + s1, j1 + s1, isupper, isunit, optype, x, i2 + s1, j2);
                        return;
                    }
                    if (!isupper && optype != 0)
                    {

                        //
                        //          (A1' A21')-1 ( X1 )
                        // A^-1*X = (        )  *(    )
                        //          (     A2')   ( X2 )
                        //
                        cmatrixlefttrsm(s2, n, a, i1 + s1, j1 + s1, isupper, isunit, optype, x, i2 + s1, j2);
                        cmatrixgemm(s1, n, s2, -1.0, a, i1 + s1, j1, optype, x, i2 + s1, j2, 0, 1.0, x, i2, j2);
                        cmatrixlefttrsm(s1, n, a, i1, j1, isupper, isunit, optype, x, i2, j2);
                        return;
                    }
                }
            }


           


            public static void rmatrixrighttrsm(int m,
                int n,
                double[,] a,
                int i1,
                int j1,
                bool isupper,
                bool isunit,
                int optype,
                double[,] x,
                int i2,
                int j2)
            {
                int s1 = 0;
                int s2 = 0;
                int bs = 0;

                bs = ablasblocksize(a);
                if (m <= bs && n <= bs)
                {
                    rmatrixrighttrsm2(m, n, a, i1, j1, isupper, isunit, optype, x, i2, j2);
                    return;
                }
                if (m >= n)
                {

                    //
                    // Split X: X*A = (X1 X2)^T*A
                    //
                    ablassplitlength(a, m, ref s1, ref s2);
                    rmatrixrighttrsm(s1, n, a, i1, j1, isupper, isunit, optype, x, i2, j2);
                    rmatrixrighttrsm(s2, n, a, i1, j1, isupper, isunit, optype, x, i2 + s1, j2);
                    return;
                }
                else
                {

                    //
                    // Split A:
                    //               (A1  A12)
                    // X*op(A) = X*op(       )
                    //               (     A2)
                    //
                    // Different variants depending on
                    // IsUpper/OpType combinations
                    //
                    ablassplitlength(a, n, ref s1, ref s2);
                    if (isupper && optype == 0)
                    {

                        //
                        //                  (A1  A12)-1
                        // X*A^-1 = (X1 X2)*(       )
                        //                  (     A2)
                        //
                        rmatrixrighttrsm(m, s1, a, i1, j1, isupper, isunit, optype, x, i2, j2);
                        rmatrixgemm(m, s2, s1, -1.0, x, i2, j2, 0, a, i1, j1 + s1, 0, 1.0, x, i2, j2 + s1);
                        rmatrixrighttrsm(m, s2, a, i1 + s1, j1 + s1, isupper, isunit, optype, x, i2, j2 + s1);
                        return;
                    }
                    if (isupper && optype != 0)
                    {

                        //
                        //                  (A1'     )-1
                        // X*A^-1 = (X1 X2)*(        )
                        //                  (A12' A2')
                        //
                        rmatrixrighttrsm(m, s2, a, i1 + s1, j1 + s1, isupper, isunit, optype, x, i2, j2 + s1);
                        rmatrixgemm(m, s1, s2, -1.0, x, i2, j2 + s1, 0, a, i1, j1 + s1, optype, 1.0, x, i2, j2);
                        rmatrixrighttrsm(m, s1, a, i1, j1, isupper, isunit, optype, x, i2, j2);
                        return;
                    }
                    if (!isupper && optype == 0)
                    {

                        //
                        //                  (A1     )-1
                        // X*A^-1 = (X1 X2)*(       )
                        //                  (A21  A2)
                        //
                        rmatrixrighttrsm(m, s2, a, i1 + s1, j1 + s1, isupper, isunit, optype, x, i2, j2 + s1);
                        rmatrixgemm(m, s1, s2, -1.0, x, i2, j2 + s1, 0, a, i1 + s1, j1, 0, 1.0, x, i2, j2);
                        rmatrixrighttrsm(m, s1, a, i1, j1, isupper, isunit, optype, x, i2, j2);
                        return;
                    }
                    if (!isupper && optype != 0)
                    {

                        //
                        //                  (A1' A21')-1
                        // X*A^-1 = (X1 X2)*(        )
                        //                  (     A2')
                        //
                        rmatrixrighttrsm(m, s1, a, i1, j1, isupper, isunit, optype, x, i2, j2);
                        rmatrixgemm(m, s2, s1, -1.0, x, i2, j2, 0, a, i1 + s1, j1, optype, 1.0, x, i2, j2 + s1);
                        rmatrixrighttrsm(m, s2, a, i1 + s1, j1 + s1, isupper, isunit, optype, x, i2, j2 + s1);
                        return;
                    }
                }
            }


          

            public static void rmatrixlefttrsm(int m,
                int n,
                double[,] a,
                int i1,
                int j1,
                bool isupper,
                bool isunit,
                int optype,
                double[,] x,
                int i2,
                int j2)
            {
                int s1 = 0;
                int s2 = 0;
                int bs = 0;

                bs = ablasblocksize(a);
                if (m <= bs && n <= bs)
                {
                    rmatrixlefttrsm2(m, n, a, i1, j1, isupper, isunit, optype, x, i2, j2);
                    return;
                }
                if (n >= m)
                {

                    //
                    // Split X: op(A)^-1*X = op(A)^-1*(X1 X2)
                    //
                    ablassplitlength(x, n, ref s1, ref s2);
                    rmatrixlefttrsm(m, s1, a, i1, j1, isupper, isunit, optype, x, i2, j2);
                    rmatrixlefttrsm(m, s2, a, i1, j1, isupper, isunit, optype, x, i2, j2 + s1);
                }
                else
                {

                    //
                    // Split A
                    //
                    ablassplitlength(a, m, ref s1, ref s2);
                    if (isupper && optype == 0)
                    {

                        //
                        //           (A1  A12)-1  ( X1 )
                        // A^-1*X* = (       )   *(    )
                        //           (     A2)    ( X2 )
                        //
                        rmatrixlefttrsm(s2, n, a, i1 + s1, j1 + s1, isupper, isunit, optype, x, i2 + s1, j2);
                        rmatrixgemm(s1, n, s2, -1.0, a, i1, j1 + s1, 0, x, i2 + s1, j2, 0, 1.0, x, i2, j2);
                        rmatrixlefttrsm(s1, n, a, i1, j1, isupper, isunit, optype, x, i2, j2);
                        return;
                    }
                    if (isupper && optype != 0)
                    {

                        //
                        //          (A1'     )-1 ( X1 )
                        // A^-1*X = (        )  *(    )
                        //          (A12' A2')   ( X2 )
                        //
                        rmatrixlefttrsm(s1, n, a, i1, j1, isupper, isunit, optype, x, i2, j2);
                        rmatrixgemm(s2, n, s1, -1.0, a, i1, j1 + s1, optype, x, i2, j2, 0, 1.0, x, i2 + s1, j2);
                        rmatrixlefttrsm(s2, n, a, i1 + s1, j1 + s1, isupper, isunit, optype, x, i2 + s1, j2);
                        return;
                    }
                    if (!isupper && optype == 0)
                    {

                        //
                        //          (A1     )-1 ( X1 )
                        // A^-1*X = (       )  *(    )
                        //          (A21  A2)   ( X2 )
                        //
                        rmatrixlefttrsm(s1, n, a, i1, j1, isupper, isunit, optype, x, i2, j2);
                        rmatrixgemm(s2, n, s1, -1.0, a, i1 + s1, j1, 0, x, i2, j2, 0, 1.0, x, i2 + s1, j2);
                        rmatrixlefttrsm(s2, n, a, i1 + s1, j1 + s1, isupper, isunit, optype, x, i2 + s1, j2);
                        return;
                    }
                    if (!isupper && optype != 0)
                    {

                        //
                        //          (A1' A21')-1 ( X1 )
                        // A^-1*X = (        )  *(    )
                        //          (     A2')   ( X2 )
                        //
                        rmatrixlefttrsm(s2, n, a, i1 + s1, j1 + s1, isupper, isunit, optype, x, i2 + s1, j2);
                        rmatrixgemm(s1, n, s2, -1.0, a, i1 + s1, j1, optype, x, i2 + s1, j2, 0, 1.0, x, i2, j2);
                        rmatrixlefttrsm(s1, n, a, i1, j1, isupper, isunit, optype, x, i2, j2);
                        return;
                    }
                }
            }


       

            public static void cmatrixsyrk(int n,
                int k,
                double alpha,
                complex[,] a,
                int ia,
                int ja,
                int optypea,
                double beta,
                complex[,] c,
                int ic,
                int jc,
                bool isupper)
            {
                int s1 = 0;
                int s2 = 0;
                int bs = 0;

                bs = ablascomplexblocksize(a);
                if (n <= bs && k <= bs)
                {
                    cmatrixsyrk2(n, k, alpha, a, ia, ja, optypea, beta, c, ic, jc, isupper);
                    return;
                }
                if (k >= n)
                {

                    //
                    // Split K
                    //
                    ablascomplexsplitlength(a, k, ref s1, ref s2);
                    if (optypea == 0)
                    {
                        cmatrixsyrk(n, s1, alpha, a, ia, ja, optypea, beta, c, ic, jc, isupper);
                        cmatrixsyrk(n, s2, alpha, a, ia, ja + s1, optypea, 1.0, c, ic, jc, isupper);
                    }
                    else
                    {
                        cmatrixsyrk(n, s1, alpha, a, ia, ja, optypea, beta, c, ic, jc, isupper);
                        cmatrixsyrk(n, s2, alpha, a, ia + s1, ja, optypea, 1.0, c, ic, jc, isupper);
                    }
                }
                else
                {

                    //
                    // Split N
                    //
                    ablascomplexsplitlength(a, n, ref s1, ref s2);
                    if (optypea == 0 && isupper)
                    {
                        cmatrixsyrk(s1, k, alpha, a, ia, ja, optypea, beta, c, ic, jc, isupper);
                        cmatrixgemm(s1, s2, k, alpha, a, ia, ja, 0, a, ia + s1, ja, 2, beta, c, ic, jc + s1);
                        cmatrixsyrk(s2, k, alpha, a, ia + s1, ja, optypea, beta, c, ic + s1, jc + s1, isupper);
                        return;
                    }
                    if (optypea == 0 && !isupper)
                    {
                        cmatrixsyrk(s1, k, alpha, a, ia, ja, optypea, beta, c, ic, jc, isupper);
                        cmatrixgemm(s2, s1, k, alpha, a, ia + s1, ja, 0, a, ia, ja, 2, beta, c, ic + s1, jc);
                        cmatrixsyrk(s2, k, alpha, a, ia + s1, ja, optypea, beta, c, ic + s1, jc + s1, isupper);
                        return;
                    }
                    if (optypea != 0 && isupper)
                    {
                        cmatrixsyrk(s1, k, alpha, a, ia, ja, optypea, beta, c, ic, jc, isupper);
                        cmatrixgemm(s1, s2, k, alpha, a, ia, ja, 2, a, ia, ja + s1, 0, beta, c, ic, jc + s1);
                        cmatrixsyrk(s2, k, alpha, a, ia, ja + s1, optypea, beta, c, ic + s1, jc + s1, isupper);
                        return;
                    }
                    if (optypea != 0 && !isupper)
                    {
                        cmatrixsyrk(s1, k, alpha, a, ia, ja, optypea, beta, c, ic, jc, isupper);
                        cmatrixgemm(s2, s1, k, alpha, a, ia, ja + s1, 2, a, ia, ja, 0, beta, c, ic + s1, jc);
                        cmatrixsyrk(s2, k, alpha, a, ia, ja + s1, optypea, beta, c, ic + s1, jc + s1, isupper);
                        return;
                    }
                }
            }




            public static void rmatrixsyrk(int n,
                int k,
                double alpha,
                double[,] a,
                int ia,
                int ja,
                int optypea,
                double beta,
                double[,] c,
                int ic,
                int jc,
                bool isupper)
            {
                int s1 = 0;
                int s2 = 0;
                int bs = 0;

                bs = ablasblocksize(a);

                //
                // Use MKL or generic basecase code
                //
                if (ablasmkl.rmatrixsyrkmkl(n, k, alpha, a, ia, ja, optypea, beta, c, ic, jc, isupper))
                {
                    return;
                }
                if (n <= bs && k <= bs)
                {
                    rmatrixsyrk2(n, k, alpha, a, ia, ja, optypea, beta, c, ic, jc, isupper);
                    return;
                }

                //
                // Recursive subdivision of the problem
                //
                if (k >= n)
                {

                    //
                    // Split K
                    //
                    ablassplitlength(a, k, ref s1, ref s2);
                    if (optypea == 0)
                    {
                        rmatrixsyrk(n, s1, alpha, a, ia, ja, optypea, beta, c, ic, jc, isupper);
                        rmatrixsyrk(n, s2, alpha, a, ia, ja + s1, optypea, 1.0, c, ic, jc, isupper);
                    }
                    else
                    {
                        rmatrixsyrk(n, s1, alpha, a, ia, ja, optypea, beta, c, ic, jc, isupper);
                        rmatrixsyrk(n, s2, alpha, a, ia + s1, ja, optypea, 1.0, c, ic, jc, isupper);
                    }
                }
                else
                {

                    //
                    // Split N
                    //
                    ablassplitlength(a, n, ref s1, ref s2);
                    if (optypea == 0 && isupper)
                    {
                        rmatrixsyrk(s1, k, alpha, a, ia, ja, optypea, beta, c, ic, jc, isupper);
                        rmatrixgemm(s1, s2, k, alpha, a, ia, ja, 0, a, ia + s1, ja, 1, beta, c, ic, jc + s1);
                        rmatrixsyrk(s2, k, alpha, a, ia + s1, ja, optypea, beta, c, ic + s1, jc + s1, isupper);
                        return;
                    }
                    if (optypea == 0 && !isupper)
                    {
                        rmatrixsyrk(s1, k, alpha, a, ia, ja, optypea, beta, c, ic, jc, isupper);
                        rmatrixgemm(s2, s1, k, alpha, a, ia + s1, ja, 0, a, ia, ja, 1, beta, c, ic + s1, jc);
                        rmatrixsyrk(s2, k, alpha, a, ia + s1, ja, optypea, beta, c, ic + s1, jc + s1, isupper);
                        return;
                    }
                    if (optypea != 0 && isupper)
                    {
                        rmatrixsyrk(s1, k, alpha, a, ia, ja, optypea, beta, c, ic, jc, isupper);
                        rmatrixgemm(s1, s2, k, alpha, a, ia, ja, 1, a, ia, ja + s1, 0, beta, c, ic, jc + s1);
                        rmatrixsyrk(s2, k, alpha, a, ia, ja + s1, optypea, beta, c, ic + s1, jc + s1, isupper);
                        return;
                    }
                    if (optypea != 0 && !isupper)
                    {
                        rmatrixsyrk(s1, k, alpha, a, ia, ja, optypea, beta, c, ic, jc, isupper);
                        rmatrixgemm(s2, s1, k, alpha, a, ia, ja + s1, 1, a, ia, ja, 0, beta, c, ic + s1, jc);
                        rmatrixsyrk(s2, k, alpha, a, ia, ja + s1, optypea, beta, c, ic + s1, jc + s1, isupper);
                        return;
                    }
                }
            }


            /*************************************************************************
            Single-threaded stub. HPC ALGLIB replaces it by multithreaded code.
            *************************************************************************/
       

            public static void cmatrixgemm(int m,
                int n,
                int k,
                complex alpha,
                complex[,] a,
                int ia,
                int ja,
                int optypea,
                complex[,] b,
                int ib,
                int jb,
                int optypeb,
                complex beta,
                complex[,] c,
                int ic,
                int jc)
            {
                int s1 = 0;
                int s2 = 0;
                int bs = 0;

                bs = ablascomplexblocksize(a);
                if ((m <= bs && n <= bs) && k <= bs)
                {
                    ablasf.cmatrixgemmk(m, n, k, alpha, a, ia, ja, optypea, b, ib, jb, optypeb, beta, c, ic, jc);
                    return;
                }

                //
                // SMP support is turned on when M or N are larger than some boundary value.
                // Magnitude of K is not taken into account because splitting on K does not
                // allow us to spawn child tasks.
                //

                //
                // Recursive algorithm: parallel splitting on M/N
                //
                if (m >= n && m >= k)
                {

                    //
                    // A*B = (A1 A2)^T*B
                    //
                    ablascomplexsplitlength(a, m, ref s1, ref s2);
                    cmatrixgemm(s1, n, k, alpha, a, ia, ja, optypea, b, ib, jb, optypeb, beta, c, ic, jc);
                    if (optypea == 0)
                    {
                        cmatrixgemm(s2, n, k, alpha, a, ia + s1, ja, optypea, b, ib, jb, optypeb, beta, c, ic + s1, jc);
                    }
                    else
                    {
                        cmatrixgemm(s2, n, k, alpha, a, ia, ja + s1, optypea, b, ib, jb, optypeb, beta, c, ic + s1, jc);
                    }
                    return;
                }
                if (n >= m && n >= k)
                {

                    //
                    // A*B = A*(B1 B2)
                    //
                    ablascomplexsplitlength(a, n, ref s1, ref s2);
                    if (optypeb == 0)
                    {
                        cmatrixgemm(m, s1, k, alpha, a, ia, ja, optypea, b, ib, jb, optypeb, beta, c, ic, jc);
                        cmatrixgemm(m, s2, k, alpha, a, ia, ja, optypea, b, ib, jb + s1, optypeb, beta, c, ic, jc + s1);
                    }
                    else
                    {
                        cmatrixgemm(m, s1, k, alpha, a, ia, ja, optypea, b, ib, jb, optypeb, beta, c, ic, jc);
                        cmatrixgemm(m, s2, k, alpha, a, ia, ja, optypea, b, ib + s1, jb, optypeb, beta, c, ic, jc + s1);
                    }
                    return;
                }

                //
                // Recursive algorithm: serial splitting on K
                //

                //
                // A*B = (A1 A2)*(B1 B2)^T
                //
                ablascomplexsplitlength(a, k, ref s1, ref s2);
                if (optypea == 0 && optypeb == 0)
                {
                    cmatrixgemm(m, n, s1, alpha, a, ia, ja, optypea, b, ib, jb, optypeb, beta, c, ic, jc);
                    cmatrixgemm(m, n, s2, alpha, a, ia, ja + s1, optypea, b, ib + s1, jb, optypeb, 1.0, c, ic, jc);
                }
                if (optypea == 0 && optypeb != 0)
                {
                    cmatrixgemm(m, n, s1, alpha, a, ia, ja, optypea, b, ib, jb, optypeb, beta, c, ic, jc);
                    cmatrixgemm(m, n, s2, alpha, a, ia, ja + s1, optypea, b, ib, jb + s1, optypeb, 1.0, c, ic, jc);
                }
                if (optypea != 0 && optypeb == 0)
                {
                    cmatrixgemm(m, n, s1, alpha, a, ia, ja, optypea, b, ib, jb, optypeb, beta, c, ic, jc);
                    cmatrixgemm(m, n, s2, alpha, a, ia + s1, ja, optypea, b, ib + s1, jb, optypeb, 1.0, c, ic, jc);
                }
                if (optypea != 0 && optypeb != 0)
                {
                    cmatrixgemm(m, n, s1, alpha, a, ia, ja, optypea, b, ib, jb, optypeb, beta, c, ic, jc);
                    cmatrixgemm(m, n, s2, alpha, a, ia + s1, ja, optypea, b, ib, jb + s1, optypeb, 1.0, c, ic, jc);
                }
                return;
            }


            public static void rmatrixgemm(int m,
                int n,
                int k,
                double alpha,
                double[,] a,
                int ia,
                int ja,
                int optypea,
                double[,] b,
                int ib,
                int jb,
                int optypeb,
                double beta,
                double[,] c,
                int ic,
                int jc)
            {
                int s1 = 0;
                int s2 = 0;
                int bs = 0;

                bs = ablasblocksize(a);

                //
                // Check input sizes for correctness
                //
                alglib.ap.assert(optypea == 0 || optypea == 1, "RMatrixGEMM: incorrect OpTypeA (must be 0 or 1)");
                alglib.ap.assert(optypeb == 0 || optypeb == 1, "RMatrixGEMM: incorrect OpTypeB (must be 0 or 1)");
                alglib.ap.assert(ic + m <= alglib.ap.rows(c), "RMatrixGEMM: incorect size of output matrix C");
                alglib.ap.assert(jc + n <= alglib.ap.cols(c), "RMatrixGEMM: incorect size of output matrix C");

                //
                // Use MKL or ALGLIB basecase code
                //
                if (ablasmkl.rmatrixgemmmkl(m, n, k, alpha, a, ia, ja, optypea, b, ib, jb, optypeb, beta, c, ic, jc))
                {
                    return;
                }
                if ((m <= bs && n <= bs) && k <= bs)
                {
                    ablasf.rmatrixgemmk(m, n, k, alpha, a, ia, ja, optypea, b, ib, jb, optypeb, beta, c, ic, jc);
                    return;
                }

                //
                // SMP support is turned on when M or N are larger than some boundary value.
                // Magnitude of K is not taken into account because splitting on K does not
                // allow us to spawn child tasks.
                //

                //
                // Recursive algorithm: split on M or N
                //
                if (m >= n && m >= k)
                {

                    //
                    // A*B = (A1 A2)^T*B
                    //
                    ablassplitlength(a, m, ref s1, ref s2);
                    if (optypea == 0)
                    {
                        rmatrixgemm(s1, n, k, alpha, a, ia, ja, optypea, b, ib, jb, optypeb, beta, c, ic, jc);
                        rmatrixgemm(s2, n, k, alpha, a, ia + s1, ja, optypea, b, ib, jb, optypeb, beta, c, ic + s1, jc);
                    }
                    else
                    {
                        rmatrixgemm(s1, n, k, alpha, a, ia, ja, optypea, b, ib, jb, optypeb, beta, c, ic, jc);
                        rmatrixgemm(s2, n, k, alpha, a, ia, ja + s1, optypea, b, ib, jb, optypeb, beta, c, ic + s1, jc);
                    }
                    return;
                }
                if (n >= m && n >= k)
                {

                    //
                    // A*B = A*(B1 B2)
                    //
                    ablassplitlength(a, n, ref s1, ref s2);
                    if (optypeb == 0)
                    {
                        rmatrixgemm(m, s1, k, alpha, a, ia, ja, optypea, b, ib, jb, optypeb, beta, c, ic, jc);
                        rmatrixgemm(m, s2, k, alpha, a, ia, ja, optypea, b, ib, jb + s1, optypeb, beta, c, ic, jc + s1);
                    }
                    else
                    {
                        rmatrixgemm(m, s1, k, alpha, a, ia, ja, optypea, b, ib, jb, optypeb, beta, c, ic, jc);
                        rmatrixgemm(m, s2, k, alpha, a, ia, ja, optypea, b, ib + s1, jb, optypeb, beta, c, ic, jc + s1);
                    }
                    return;
                }

                //
                // Recursive algorithm: split on K
                //

                //
                // A*B = (A1 A2)*(B1 B2)^T
                //
                ablassplitlength(a, k, ref s1, ref s2);
                if (optypea == 0 && optypeb == 0)
                {
                    rmatrixgemm(m, n, s1, alpha, a, ia, ja, optypea, b, ib, jb, optypeb, beta, c, ic, jc);
                    rmatrixgemm(m, n, s2, alpha, a, ia, ja + s1, optypea, b, ib + s1, jb, optypeb, 1.0, c, ic, jc);
                }
                if (optypea == 0 && optypeb != 0)
                {
                    rmatrixgemm(m, n, s1, alpha, a, ia, ja, optypea, b, ib, jb, optypeb, beta, c, ic, jc);
                    rmatrixgemm(m, n, s2, alpha, a, ia, ja + s1, optypea, b, ib, jb + s1, optypeb, 1.0, c, ic, jc);
                }
                if (optypea != 0 && optypeb == 0)
                {
                    rmatrixgemm(m, n, s1, alpha, a, ia, ja, optypea, b, ib, jb, optypeb, beta, c, ic, jc);
                    rmatrixgemm(m, n, s2, alpha, a, ia + s1, ja, optypea, b, ib + s1, jb, optypeb, 1.0, c, ic, jc);
                }
                if (optypea != 0 && optypeb != 0)
                {
                    rmatrixgemm(m, n, s1, alpha, a, ia, ja, optypea, b, ib, jb, optypeb, beta, c, ic, jc);
                    rmatrixgemm(m, n, s2, alpha, a, ia + s1, ja, optypea, b, ib, jb + s1, optypeb, 1.0, c, ic, jc);
                }
                return;
            }


         


            /*************************************************************************
            Complex ABLASSplitLength

              -- ALGLIB routine --
                 15.12.2009
                 Bochkanov Sergey
            *************************************************************************/
            private static void ablasinternalsplitlength(int n,
                int nb,
                ref int n1,
                ref int n2)
            {
                int r = 0;

                n1 = 0;
                n2 = 0;

                if (n <= nb)
                {

                    //
                    // Block size, no further splitting
                    //
                    n1 = n;
                    n2 = 0;
                }
                else
                {

                    //
                    // Greater than block size
                    //
                    if (n % nb != 0)
                    {

                        //
                        // Split remainder
                        //
                        n2 = n % nb;
                        n1 = n - n2;
                    }
                    else
                    {

                        //
                        // Split on block boundaries
                        //
                        n2 = n / 2;
                        n1 = n - n2;
                        if (n1 % nb == 0)
                        {
                            return;
                        }
                        r = nb - n1 % nb;
                        n1 = n1 + r;
                        n2 = n2 - r;
                    }
                }
            }


            /*************************************************************************
            Level 2 variant of CMatrixRightTRSM
            *************************************************************************/
            private static void cmatrixrighttrsm2(int m,
                int n,
                complex[,] a,
                int i1,
                int j1,
                bool isupper,
                bool isunit,
                int optype,
                complex[,] x,
                int i2,
                int j2)
            {
                int i = 0;
                int j = 0;
                complex vc = 0;
                complex vd = 0;
                int i_ = 0;
                int i1_ = 0;


                //
                // Special case
                //
                if (n * m == 0)
                {
                    return;
                }

                //
                // Try to call fast TRSM
                //
                if (ablasf.cmatrixrighttrsmf(m, n, a, i1, j1, isupper, isunit, optype, x, i2, j2))
                {
                    return;
                }

                //
                // General case
                //
                if (isupper)
                {

                    //
                    // Upper triangular matrix
                    //
                    if (optype == 0)
                    {

                        //
                        // X*A^(-1)
                        //
                        for (i = 0; i <= m - 1; i++)
                        {
                            for (j = 0; j <= n - 1; j++)
                            {
                                if (isunit)
                                {
                                    vd = 1;
                                }
                                else
                                {
                                    vd = a[i1 + j, j1 + j];
                                }
                                x[i2 + i, j2 + j] = x[i2 + i, j2 + j] / vd;
                                if (j < n - 1)
                                {
                                    vc = x[i2 + i, j2 + j];
                                    i1_ = (j1 + j + 1) - (j2 + j + 1);
                                    for (i_ = j2 + j + 1; i_ <= j2 + n - 1; i_++)
                                    {
                                        x[i2 + i, i_] = x[i2 + i, i_] - vc * a[i1 + j, i_ + i1_];
                                    }
                                }
                            }
                        }
                        return;
                    }
                    if (optype == 1)
                    {

                        //
                        // X*A^(-T)
                        //
                        for (i = 0; i <= m - 1; i++)
                        {
                            for (j = n - 1; j >= 0; j--)
                            {
                                vc = 0;
                                vd = 1;
                                if (j < n - 1)
                                {
                                    i1_ = (j1 + j + 1) - (j2 + j + 1);
                                    vc = 0.0;
                                    for (i_ = j2 + j + 1; i_ <= j2 + n - 1; i_++)
                                    {
                                        vc += x[i2 + i, i_] * a[i1 + j, i_ + i1_];
                                    }
                                }
                                if (!isunit)
                                {
                                    vd = a[i1 + j, j1 + j];
                                }
                                x[i2 + i, j2 + j] = (x[i2 + i, j2 + j] - vc) / vd;
                            }
                        }
                        return;
                    }
                    if (optype == 2)
                    {

                        //
                        // X*A^(-H)
                        //
                        for (i = 0; i <= m - 1; i++)
                        {
                            for (j = n - 1; j >= 0; j--)
                            {
                                vc = 0;
                                vd = 1;
                                if (j < n - 1)
                                {
                                    i1_ = (j1 + j + 1) - (j2 + j + 1);
                                    vc = 0.0;
                                    for (i_ = j2 + j + 1; i_ <= j2 + n - 1; i_++)
                                    {
                                        vc += x[i2 + i, i_] * math.conj(a[i1 + j, i_ + i1_]);
                                    }
                                }
                                if (!isunit)
                                {
                                    vd = math.conj(a[i1 + j, j1 + j]);
                                }
                                x[i2 + i, j2 + j] = (x[i2 + i, j2 + j] - vc) / vd;
                            }
                        }
                        return;
                    }
                }
                else
                {

                    //
                    // Lower triangular matrix
                    //
                    if (optype == 0)
                    {

                        //
                        // X*A^(-1)
                        //
                        for (i = 0; i <= m - 1; i++)
                        {
                            for (j = n - 1; j >= 0; j--)
                            {
                                if (isunit)
                                {
                                    vd = 1;
                                }
                                else
                                {
                                    vd = a[i1 + j, j1 + j];
                                }
                                x[i2 + i, j2 + j] = x[i2 + i, j2 + j] / vd;
                                if (j > 0)
                                {
                                    vc = x[i2 + i, j2 + j];
                                    i1_ = (j1) - (j2);
                                    for (i_ = j2; i_ <= j2 + j - 1; i_++)
                                    {
                                        x[i2 + i, i_] = x[i2 + i, i_] - vc * a[i1 + j, i_ + i1_];
                                    }
                                }
                            }
                        }
                        return;
                    }
                    if (optype == 1)
                    {

                        //
                        // X*A^(-T)
                        //
                        for (i = 0; i <= m - 1; i++)
                        {
                            for (j = 0; j <= n - 1; j++)
                            {
                                vc = 0;
                                vd = 1;
                                if (j > 0)
                                {
                                    i1_ = (j1) - (j2);
                                    vc = 0.0;
                                    for (i_ = j2; i_ <= j2 + j - 1; i_++)
                                    {
                                        vc += x[i2 + i, i_] * a[i1 + j, i_ + i1_];
                                    }
                                }
                                if (!isunit)
                                {
                                    vd = a[i1 + j, j1 + j];
                                }
                                x[i2 + i, j2 + j] = (x[i2 + i, j2 + j] - vc) / vd;
                            }
                        }
                        return;
                    }
                    if (optype == 2)
                    {

                        //
                        // X*A^(-H)
                        //
                        for (i = 0; i <= m - 1; i++)
                        {
                            for (j = 0; j <= n - 1; j++)
                            {
                                vc = 0;
                                vd = 1;
                                if (j > 0)
                                {
                                    i1_ = (j1) - (j2);
                                    vc = 0.0;
                                    for (i_ = j2; i_ <= j2 + j - 1; i_++)
                                    {
                                        vc += x[i2 + i, i_] * math.conj(a[i1 + j, i_ + i1_]);
                                    }
                                }
                                if (!isunit)
                                {
                                    vd = math.conj(a[i1 + j, j1 + j]);
                                }
                                x[i2 + i, j2 + j] = (x[i2 + i, j2 + j] - vc) / vd;
                            }
                        }
                        return;
                    }
                }
            }


            /*************************************************************************
            Level-2 subroutine
            *************************************************************************/
            private static void cmatrixlefttrsm2(int m,
                int n,
                complex[,] a,
                int i1,
                int j1,
                bool isupper,
                bool isunit,
                int optype,
                complex[,] x,
                int i2,
                int j2)
            {
                int i = 0;
                int j = 0;
                complex vc = 0;
                complex vd = 0;
                int i_ = 0;


                //
                // Special case
                //
                if (n * m == 0)
                {
                    return;
                }

                //
                // Try to call fast TRSM
                //
                if (ablasf.cmatrixlefttrsmf(m, n, a, i1, j1, isupper, isunit, optype, x, i2, j2))
                {
                    return;
                }

                //
                // General case
                //
                if (isupper)
                {

                    //
                    // Upper triangular matrix
                    //
                    if (optype == 0)
                    {

                        //
                        // A^(-1)*X
                        //
                        for (i = m - 1; i >= 0; i--)
                        {
                            for (j = i + 1; j <= m - 1; j++)
                            {
                                vc = a[i1 + i, j1 + j];
                                for (i_ = j2; i_ <= j2 + n - 1; i_++)
                                {
                                    x[i2 + i, i_] = x[i2 + i, i_] - vc * x[i2 + j, i_];
                                }
                            }
                            if (!isunit)
                            {
                                vd = 1 / a[i1 + i, j1 + i];
                                for (i_ = j2; i_ <= j2 + n - 1; i_++)
                                {
                                    x[i2 + i, i_] = vd * x[i2 + i, i_];
                                }
                            }
                        }
                        return;
                    }
                    if (optype == 1)
                    {

                        //
                        // A^(-T)*X
                        //
                        for (i = 0; i <= m - 1; i++)
                        {
                            if (isunit)
                            {
                                vd = 1;
                            }
                            else
                            {
                                vd = 1 / a[i1 + i, j1 + i];
                            }
                            for (i_ = j2; i_ <= j2 + n - 1; i_++)
                            {
                                x[i2 + i, i_] = vd * x[i2 + i, i_];
                            }
                            for (j = i + 1; j <= m - 1; j++)
                            {
                                vc = a[i1 + i, j1 + j];
                                for (i_ = j2; i_ <= j2 + n - 1; i_++)
                                {
                                    x[i2 + j, i_] = x[i2 + j, i_] - vc * x[i2 + i, i_];
                                }
                            }
                        }
                        return;
                    }
                    if (optype == 2)
                    {

                        //
                        // A^(-H)*X
                        //
                        for (i = 0; i <= m - 1; i++)
                        {
                            if (isunit)
                            {
                                vd = 1;
                            }
                            else
                            {
                                vd = 1 / math.conj(a[i1 + i, j1 + i]);
                            }
                            for (i_ = j2; i_ <= j2 + n - 1; i_++)
                            {
                                x[i2 + i, i_] = vd * x[i2 + i, i_];
                            }
                            for (j = i + 1; j <= m - 1; j++)
                            {
                                vc = math.conj(a[i1 + i, j1 + j]);
                                for (i_ = j2; i_ <= j2 + n - 1; i_++)
                                {
                                    x[i2 + j, i_] = x[i2 + j, i_] - vc * x[i2 + i, i_];
                                }
                            }
                        }
                        return;
                    }
                }
                else
                {

                    //
                    // Lower triangular matrix
                    //
                    if (optype == 0)
                    {

                        //
                        // A^(-1)*X
                        //
                        for (i = 0; i <= m - 1; i++)
                        {
                            for (j = 0; j <= i - 1; j++)
                            {
                                vc = a[i1 + i, j1 + j];
                                for (i_ = j2; i_ <= j2 + n - 1; i_++)
                                {
                                    x[i2 + i, i_] = x[i2 + i, i_] - vc * x[i2 + j, i_];
                                }
                            }
                            if (isunit)
                            {
                                vd = 1;
                            }
                            else
                            {
                                vd = 1 / a[i1 + j, j1 + j];
                            }
                            for (i_ = j2; i_ <= j2 + n - 1; i_++)
                            {
                                x[i2 + i, i_] = vd * x[i2 + i, i_];
                            }
                        }
                        return;
                    }
                    if (optype == 1)
                    {

                        //
                        // A^(-T)*X
                        //
                        for (i = m - 1; i >= 0; i--)
                        {
                            if (isunit)
                            {
                                vd = 1;
                            }
                            else
                            {
                                vd = 1 / a[i1 + i, j1 + i];
                            }
                            for (i_ = j2; i_ <= j2 + n - 1; i_++)
                            {
                                x[i2 + i, i_] = vd * x[i2 + i, i_];
                            }
                            for (j = i - 1; j >= 0; j--)
                            {
                                vc = a[i1 + i, j1 + j];
                                for (i_ = j2; i_ <= j2 + n - 1; i_++)
                                {
                                    x[i2 + j, i_] = x[i2 + j, i_] - vc * x[i2 + i, i_];
                                }
                            }
                        }
                        return;
                    }
                    if (optype == 2)
                    {

                        //
                        // A^(-H)*X
                        //
                        for (i = m - 1; i >= 0; i--)
                        {
                            if (isunit)
                            {
                                vd = 1;
                            }
                            else
                            {
                                vd = 1 / math.conj(a[i1 + i, j1 + i]);
                            }
                            for (i_ = j2; i_ <= j2 + n - 1; i_++)
                            {
                                x[i2 + i, i_] = vd * x[i2 + i, i_];
                            }
                            for (j = i - 1; j >= 0; j--)
                            {
                                vc = math.conj(a[i1 + i, j1 + j]);
                                for (i_ = j2; i_ <= j2 + n - 1; i_++)
                                {
                                    x[i2 + j, i_] = x[i2 + j, i_] - vc * x[i2 + i, i_];
                                }
                            }
                        }
                        return;
                    }
                }
            }


            /*************************************************************************
            Level 2 subroutine

              -- ALGLIB routine --
                 15.12.2009
                 Bochkanov Sergey
            *************************************************************************/
            private static void rmatrixrighttrsm2(int m,
                int n,
                double[,] a,
                int i1,
                int j1,
                bool isupper,
                bool isunit,
                int optype,
                double[,] x,
                int i2,
                int j2)
            {
                int i = 0;
                int j = 0;
                double vr = 0;
                double vd = 0;
                int i_ = 0;
                int i1_ = 0;


                //
                // Special case
                //
                if (n * m == 0)
                {
                    return;
                }

                //
                // Try to use "fast" code
                //
                if (ablasf.rmatrixrighttrsmf(m, n, a, i1, j1, isupper, isunit, optype, x, i2, j2))
                {
                    return;
                }

                //
                // General case
                //
                if (isupper)
                {

                    //
                    // Upper triangular matrix
                    //
                    if (optype == 0)
                    {

                        //
                        // X*A^(-1)
                        //
                        for (i = 0; i <= m - 1; i++)
                        {
                            for (j = 0; j <= n - 1; j++)
                            {
                                if (isunit)
                                {
                                    vd = 1;
                                }
                                else
                                {
                                    vd = a[i1 + j, j1 + j];
                                }
                                x[i2 + i, j2 + j] = x[i2 + i, j2 + j] / vd;
                                if (j < n - 1)
                                {
                                    vr = x[i2 + i, j2 + j];
                                    i1_ = (j1 + j + 1) - (j2 + j + 1);
                                    for (i_ = j2 + j + 1; i_ <= j2 + n - 1; i_++)
                                    {
                                        x[i2 + i, i_] = x[i2 + i, i_] - vr * a[i1 + j, i_ + i1_];
                                    }
                                }
                            }
                        }
                        return;
                    }
                    if (optype == 1)
                    {

                        //
                        // X*A^(-T)
                        //
                        for (i = 0; i <= m - 1; i++)
                        {
                            for (j = n - 1; j >= 0; j--)
                            {
                                vr = 0;
                                vd = 1;
                                if (j < n - 1)
                                {
                                    i1_ = (j1 + j + 1) - (j2 + j + 1);
                                    vr = 0.0;
                                    for (i_ = j2 + j + 1; i_ <= j2 + n - 1; i_++)
                                    {
                                        vr += x[i2 + i, i_] * a[i1 + j, i_ + i1_];
                                    }
                                }
                                if (!isunit)
                                {
                                    vd = a[i1 + j, j1 + j];
                                }
                                x[i2 + i, j2 + j] = (x[i2 + i, j2 + j] - vr) / vd;
                            }
                        }
                        return;
                    }
                }
                else
                {

                    //
                    // Lower triangular matrix
                    //
                    if (optype == 0)
                    {

                        //
                        // X*A^(-1)
                        //
                        for (i = 0; i <= m - 1; i++)
                        {
                            for (j = n - 1; j >= 0; j--)
                            {
                                if (isunit)
                                {
                                    vd = 1;
                                }
                                else
                                {
                                    vd = a[i1 + j, j1 + j];
                                }
                                x[i2 + i, j2 + j] = x[i2 + i, j2 + j] / vd;
                                if (j > 0)
                                {
                                    vr = x[i2 + i, j2 + j];
                                    i1_ = (j1) - (j2);
                                    for (i_ = j2; i_ <= j2 + j - 1; i_++)
                                    {
                                        x[i2 + i, i_] = x[i2 + i, i_] - vr * a[i1 + j, i_ + i1_];
                                    }
                                }
                            }
                        }
                        return;
                    }
                    if (optype == 1)
                    {

                        //
                        // X*A^(-T)
                        //
                        for (i = 0; i <= m - 1; i++)
                        {
                            for (j = 0; j <= n - 1; j++)
                            {
                                vr = 0;
                                vd = 1;
                                if (j > 0)
                                {
                                    i1_ = (j1) - (j2);
                                    vr = 0.0;
                                    for (i_ = j2; i_ <= j2 + j - 1; i_++)
                                    {
                                        vr += x[i2 + i, i_] * a[i1 + j, i_ + i1_];
                                    }
                                }
                                if (!isunit)
                                {
                                    vd = a[i1 + j, j1 + j];
                                }
                                x[i2 + i, j2 + j] = (x[i2 + i, j2 + j] - vr) / vd;
                            }
                        }
                        return;
                    }
                }
            }


            /*************************************************************************
            Level 2 subroutine
            *************************************************************************/
            private static void rmatrixlefttrsm2(int m,
                int n,
                double[,] a,
                int i1,
                int j1,
                bool isupper,
                bool isunit,
                int optype,
                double[,] x,
                int i2,
                int j2)
            {
                int i = 0;
                int j = 0;
                double vr = 0;
                double vd = 0;
                int i_ = 0;


                //
                // Special case
                //
                if (n == 0 || m == 0)
                {
                    return;
                }

                //
                // Try fast code
                //
                if (ablasf.rmatrixlefttrsmf(m, n, a, i1, j1, isupper, isunit, optype, x, i2, j2))
                {
                    return;
                }

                //
                // General case
                //
                if (isupper)
                {

                    //
                    // Upper triangular matrix
                    //
                    if (optype == 0)
                    {

                        //
                        // A^(-1)*X
                        //
                        for (i = m - 1; i >= 0; i--)
                        {
                            for (j = i + 1; j <= m - 1; j++)
                            {
                                vr = a[i1 + i, j1 + j];
                                for (i_ = j2; i_ <= j2 + n - 1; i_++)
                                {
                                    x[i2 + i, i_] = x[i2 + i, i_] - vr * x[i2 + j, i_];
                                }
                            }
                            if (!isunit)
                            {
                                vd = 1 / a[i1 + i, j1 + i];
                                for (i_ = j2; i_ <= j2 + n - 1; i_++)
                                {
                                    x[i2 + i, i_] = vd * x[i2 + i, i_];
                                }
                            }
                        }
                        return;
                    }
                    if (optype == 1)
                    {

                        //
                        // A^(-T)*X
                        //
                        for (i = 0; i <= m - 1; i++)
                        {
                            if (isunit)
                            {
                                vd = 1;
                            }
                            else
                            {
                                vd = 1 / a[i1 + i, j1 + i];
                            }
                            for (i_ = j2; i_ <= j2 + n - 1; i_++)
                            {
                                x[i2 + i, i_] = vd * x[i2 + i, i_];
                            }
                            for (j = i + 1; j <= m - 1; j++)
                            {
                                vr = a[i1 + i, j1 + j];
                                for (i_ = j2; i_ <= j2 + n - 1; i_++)
                                {
                                    x[i2 + j, i_] = x[i2 + j, i_] - vr * x[i2 + i, i_];
                                }
                            }
                        }
                        return;
                    }
                }
                else
                {

                    //
                    // Lower triangular matrix
                    //
                    if (optype == 0)
                    {

                        //
                        // A^(-1)*X
                        //
                        for (i = 0; i <= m - 1; i++)
                        {
                            for (j = 0; j <= i - 1; j++)
                            {
                                vr = a[i1 + i, j1 + j];
                                for (i_ = j2; i_ <= j2 + n - 1; i_++)
                                {
                                    x[i2 + i, i_] = x[i2 + i, i_] - vr * x[i2 + j, i_];
                                }
                            }
                            if (isunit)
                            {
                                vd = 1;
                            }
                            else
                            {
                                vd = 1 / a[i1 + j, j1 + j];
                            }
                            for (i_ = j2; i_ <= j2 + n - 1; i_++)
                            {
                                x[i2 + i, i_] = vd * x[i2 + i, i_];
                            }
                        }
                        return;
                    }
                    if (optype == 1)
                    {

                        //
                        // A^(-T)*X
                        //
                        for (i = m - 1; i >= 0; i--)
                        {
                            if (isunit)
                            {
                                vd = 1;
                            }
                            else
                            {
                                vd = 1 / a[i1 + i, j1 + i];
                            }
                            for (i_ = j2; i_ <= j2 + n - 1; i_++)
                            {
                                x[i2 + i, i_] = vd * x[i2 + i, i_];
                            }
                            for (j = i - 1; j >= 0; j--)
                            {
                                vr = a[i1 + i, j1 + j];
                                for (i_ = j2; i_ <= j2 + n - 1; i_++)
                                {
                                    x[i2 + j, i_] = x[i2 + j, i_] - vr * x[i2 + i, i_];
                                }
                            }
                        }
                        return;
                    }
                }
            }


            /*************************************************************************
            Level 2 subroutine
            *************************************************************************/
            private static void cmatrixsyrk2(int n,
                int k,
                double alpha,
                complex[,] a,
                int ia,
                int ja,
                int optypea,
                double beta,
                complex[,] c,
                int ic,
                int jc,
                bool isupper)
            {
                int i = 0;
                int j = 0;
                int j1 = 0;
                int j2 = 0;
                complex v = 0;
                int i_ = 0;
                int i1_ = 0;


                //
                // Fast exit (nothing to be done)
                //
                if (((double)(alpha) == (double)(0) || k == 0) && (double)(beta) == (double)(1))
                {
                    return;
                }

                //
                // Try to call fast SYRK
                //
                if (ablasf.cmatrixsyrkf(n, k, alpha, a, ia, ja, optypea, beta, c, ic, jc, isupper))
                {
                    return;
                }

                //
                // SYRK
                //
                if (optypea == 0)
                {

                    //
                    // C=alpha*A*A^H+beta*C
                    //
                    for (i = 0; i <= n - 1; i++)
                    {
                        if (isupper)
                        {
                            j1 = i;
                            j2 = n - 1;
                        }
                        else
                        {
                            j1 = 0;
                            j2 = i;
                        }
                        for (j = j1; j <= j2; j++)
                        {
                            if ((double)(alpha) != (double)(0) && k > 0)
                            {
                                v = 0.0;
                                for (i_ = ja; i_ <= ja + k - 1; i_++)
                                {
                                    v += a[ia + i, i_] * math.conj(a[ia + j, i_]);
                                }
                            }
                            else
                            {
                                v = 0;
                            }
                            if ((double)(beta) == (double)(0))
                            {
                                c[ic + i, jc + j] = alpha * v;
                            }
                            else
                            {
                                c[ic + i, jc + j] = beta * c[ic + i, jc + j] + alpha * v;
                            }
                        }
                    }
                    return;
                }
                else
                {

                    //
                    // C=alpha*A^H*A+beta*C
                    //
                    for (i = 0; i <= n - 1; i++)
                    {
                        if (isupper)
                        {
                            j1 = i;
                            j2 = n - 1;
                        }
                        else
                        {
                            j1 = 0;
                            j2 = i;
                        }
                        if ((double)(beta) == (double)(0))
                        {
                            for (j = j1; j <= j2; j++)
                            {
                                c[ic + i, jc + j] = 0;
                            }
                        }
                        else
                        {
                            for (i_ = jc + j1; i_ <= jc + j2; i_++)
                            {
                                c[ic + i, i_] = beta * c[ic + i, i_];
                            }
                        }
                    }
                    for (i = 0; i <= k - 1; i++)
                    {
                        for (j = 0; j <= n - 1; j++)
                        {
                            if (isupper)
                            {
                                j1 = j;
                                j2 = n - 1;
                            }
                            else
                            {
                                j1 = 0;
                                j2 = j;
                            }
                            v = alpha * math.conj(a[ia + i, ja + j]);
                            i1_ = (ja + j1) - (jc + j1);
                            for (i_ = jc + j1; i_ <= jc + j2; i_++)
                            {
                                c[ic + j, i_] = c[ic + j, i_] + v * a[ia + i, i_ + i1_];
                            }
                        }
                    }
                    return;
                }
            }


            /*************************************************************************
            Level 2 subrotuine
            *************************************************************************/
            private static void rmatrixsyrk2(int n,
                int k,
                double alpha,
                double[,] a,
                int ia,
                int ja,
                int optypea,
                double beta,
                double[,] c,
                int ic,
                int jc,
                bool isupper)
            {
                int i = 0;
                int j = 0;
                int j1 = 0;
                int j2 = 0;
                double v = 0;
                int i_ = 0;
                int i1_ = 0;


                //
                // Fast exit (nothing to be done)
                //
                if (((double)(alpha) == (double)(0) || k == 0) && (double)(beta) == (double)(1))
                {
                    return;
                }

                //
                // Try to call fast SYRK
                //
                if (ablasf.rmatrixsyrkf(n, k, alpha, a, ia, ja, optypea, beta, c, ic, jc, isupper))
                {
                    return;
                }

                //
                // SYRK
                //
                if (optypea == 0)
                {

                    //
                    // C=alpha*A*A^H+beta*C
                    //
                    for (i = 0; i <= n - 1; i++)
                    {
                        if (isupper)
                        {
                            j1 = i;
                            j2 = n - 1;
                        }
                        else
                        {
                            j1 = 0;
                            j2 = i;
                        }
                        for (j = j1; j <= j2; j++)
                        {
                            if ((double)(alpha) != (double)(0) && k > 0)
                            {
                                v = 0.0;
                                for (i_ = ja; i_ <= ja + k - 1; i_++)
                                {
                                    v += a[ia + i, i_] * a[ia + j, i_];
                                }
                            }
                            else
                            {
                                v = 0;
                            }
                            if ((double)(beta) == (double)(0))
                            {
                                c[ic + i, jc + j] = alpha * v;
                            }
                            else
                            {
                                c[ic + i, jc + j] = beta * c[ic + i, jc + j] + alpha * v;
                            }
                        }
                    }
                    return;
                }
                else
                {

                    //
                    // C=alpha*A^H*A+beta*C
                    //
                    for (i = 0; i <= n - 1; i++)
                    {
                        if (isupper)
                        {
                            j1 = i;
                            j2 = n - 1;
                        }
                        else
                        {
                            j1 = 0;
                            j2 = i;
                        }
                        if ((double)(beta) == (double)(0))
                        {
                            for (j = j1; j <= j2; j++)
                            {
                                c[ic + i, jc + j] = 0;
                            }
                        }
                        else
                        {
                            for (i_ = jc + j1; i_ <= jc + j2; i_++)
                            {
                                c[ic + i, i_] = beta * c[ic + i, i_];
                            }
                        }
                    }
                    for (i = 0; i <= k - 1; i++)
                    {
                        for (j = 0; j <= n - 1; j++)
                        {
                            if (isupper)
                            {
                                j1 = j;
                                j2 = n - 1;
                            }
                            else
                            {
                                j1 = 0;
                                j2 = j;
                            }
                            v = alpha * a[ia + i, ja + j];
                            i1_ = (ja + j1) - (jc + j1);
                            for (i_ = jc + j1; i_ <= jc + j2; i_++)
                            {
                                c[ic + j, i_] = c[ic + j, i_] + v * a[ia + i, i_ + i1_];
                            }
                        }
                    }
                    return;
                }
            }


        }

        public class ablasf
        {
            /*************************************************************************
            Fast kernel

              -- ALGLIB routine --
                 19.01.2010
                 Bochkanov Sergey
            *************************************************************************/
            public static bool cmatrixrank1f(int m,
                int n,
                ref complex[,] a,
                int ia,
                int ja,
                ref complex[] u,
                int iu,
                ref complex[] v,
                int iv)
            {
                bool result = new bool();

                result = false;
                return result;
            }


            /*************************************************************************
            Fast kernel

              -- ALGLIB routine --
                 19.01.2010
                 Bochkanov Sergey
            *************************************************************************/
            public static bool rmatrixrank1f(int m,
                int n,
                ref double[,] a,
                int ia,
                int ja,
                ref double[] u,
                int iu,
                ref double[] v,
                int iv)
            {
                bool result = new bool();

                result = false;
                return result;
            }


            /*************************************************************************
            Fast kernel

              -- ALGLIB routine --
                 19.01.2010
                 Bochkanov Sergey
            *************************************************************************/
            public static bool cmatrixmvf(int m,
                int n,
                complex[,] a,
                int ia,
                int ja,
                int opa,
                complex[] x,
                int ix,
                ref complex[] y,
                int iy)
            {
                bool result = new bool();

                result = false;
                return result;
            }


            /*************************************************************************
            Fast kernel

              -- ALGLIB routine --
                 19.01.2010
                 Bochkanov Sergey
            *************************************************************************/
            public static bool rmatrixmvf(int m,
                int n,
                double[,] a,
                int ia,
                int ja,
                int opa,
                double[] x,
                int ix,
                ref double[] y,
                int iy)
            {
                bool result = new bool();

                result = false;
                return result;
            }


            /*************************************************************************
            Fast kernel

              -- ALGLIB routine --
                 19.01.2010
                 Bochkanov Sergey
            *************************************************************************/
            public static bool cmatrixrighttrsmf(int m,
                int n,
                complex[,] a,
                int i1,
                int j1,
                bool isupper,
                bool isunit,
                int optype,
                complex[,] x,
                int i2,
                int j2)
            {
                bool result = new bool();

                result = false;
                return result;
            }


            /*************************************************************************
            Fast kernel

              -- ALGLIB routine --
                 19.01.2010
                 Bochkanov Sergey
            *************************************************************************/
            public static bool cmatrixlefttrsmf(int m,
                int n,
                complex[,] a,
                int i1,
                int j1,
                bool isupper,
                bool isunit,
                int optype,
                complex[,] x,
                int i2,
                int j2)
            {
                bool result = new bool();

                result = false;
                return result;
            }


            /*************************************************************************
            Fast kernel

              -- ALGLIB routine --
                 19.01.2010
                 Bochkanov Sergey
            *************************************************************************/
            public static bool rmatrixrighttrsmf(int m,
                int n,
                double[,] a,
                int i1,
                int j1,
                bool isupper,
                bool isunit,
                int optype,
                double[,] x,
                int i2,
                int j2)
            {
                bool result = new bool();

                result = false;
                return result;
            }


            /*************************************************************************
            Fast kernel

              -- ALGLIB routine --
                 19.01.2010
                 Bochkanov Sergey
            *************************************************************************/
            public static bool rmatrixlefttrsmf(int m,
                int n,
                double[,] a,
                int i1,
                int j1,
                bool isupper,
                bool isunit,
                int optype,
                double[,] x,
                int i2,
                int j2)
            {
                bool result = new bool();

                result = false;
                return result;
            }


            /*************************************************************************
            Fast kernel

              -- ALGLIB routine --
                 19.01.2010
                 Bochkanov Sergey
            *************************************************************************/
            public static bool cmatrixsyrkf(int n,
                int k,
                double alpha,
                complex[,] a,
                int ia,
                int ja,
                int optypea,
                double beta,
                complex[,] c,
                int ic,
                int jc,
                bool isupper)
            {
                bool result = new bool();

                result = false;
                return result;
            }


            /*************************************************************************
            Fast kernel

              -- ALGLIB routine --
                 19.01.2010
                 Bochkanov Sergey
            *************************************************************************/
            public static bool rmatrixsyrkf(int n,
                int k,
                double alpha,
                double[,] a,
                int ia,
                int ja,
                int optypea,
                double beta,
                double[,] c,
                int ic,
                int jc,
                bool isupper)
            {
                bool result = new bool();

                result = false;
                return result;
            }


            /*************************************************************************
            Fast kernel

              -- ALGLIB routine --
                 19.01.2010
                 Bochkanov Sergey
            *************************************************************************/
            public static bool rmatrixgemmf(int m,
                int n,
                int k,
                double alpha,
                double[,] a,
                int ia,
                int ja,
                int optypea,
                double[,] b,
                int ib,
                int jb,
                int optypeb,
                double beta,
                double[,] c,
                int ic,
                int jc)
            {
                bool result = new bool();

                result = false;
                return result;
            }


            /*************************************************************************
            Fast kernel

              -- ALGLIB routine --
                 19.01.2010
                 Bochkanov Sergey
            *************************************************************************/
            public static bool cmatrixgemmf(int m,
                int n,
                int k,
                complex alpha,
                complex[,] a,
                int ia,
                int ja,
                int optypea,
                complex[,] b,
                int ib,
                int jb,
                int optypeb,
                complex beta,
                complex[,] c,
                int ic,
                int jc)
            {
                bool result = new bool();

                result = false;
                return result;
            }


            /*************************************************************************
            CMatrixGEMM kernel, basecase code for CMatrixGEMM.

            This subroutine calculates C = alpha*op1(A)*op2(B) +beta*C where:
            * C is MxN general matrix
            * op1(A) is MxK matrix
            * op2(B) is KxN matrix
            * "op" may be identity transformation, transposition, conjugate transposition

            Additional info:
            * multiplication result replaces C. If Beta=0, C elements are not used in
              calculations (not multiplied by zero - just not referenced)
            * if Alpha=0, A is not used (not multiplied by zero - just not referenced)
            * if both Beta and Alpha are zero, C is filled by zeros.

            IMPORTANT:

            This function does NOT preallocate output matrix C, it MUST be preallocated
            by caller prior to calling this function. In case C does not have  enough
            space to store result, exception will be generated.

            INPUT PARAMETERS
                M       -   matrix size, M>0
                N       -   matrix size, N>0
                K       -   matrix size, K>0
                Alpha   -   coefficient
                A       -   matrix
                IA      -   submatrix offset
                JA      -   submatrix offset
                OpTypeA -   transformation type:
                            * 0 - no transformation
                            * 1 - transposition
                            * 2 - conjugate transposition
                B       -   matrix
                IB      -   submatrix offset
                JB      -   submatrix offset
                OpTypeB -   transformation type:
                            * 0 - no transformation
                            * 1 - transposition
                            * 2 - conjugate transposition
                Beta    -   coefficient
                C       -   PREALLOCATED output matrix
                IC      -   submatrix offset
                JC      -   submatrix offset

              -- ALGLIB routine --
                 27.03.2013
                 Bochkanov Sergey
            *************************************************************************/
            public static void cmatrixgemmk(int m,
                int n,
                int k,
                complex alpha,
                complex[,] a,
                int ia,
                int ja,
                int optypea,
                complex[,] b,
                int ib,
                int jb,
                int optypeb,
                complex beta,
                complex[,] c,
                int ic,
                int jc)
            {
                int i = 0;
                int j = 0;
                complex v = 0;
                complex v00 = 0;
                complex v01 = 0;
                complex v10 = 0;
                complex v11 = 0;
                double v00x = 0;
                double v00y = 0;
                double v01x = 0;
                double v01y = 0;
                double v10x = 0;
                double v10y = 0;
                double v11x = 0;
                double v11y = 0;
                double a0x = 0;
                double a0y = 0;
                double a1x = 0;
                double a1y = 0;
                double b0x = 0;
                double b0y = 0;
                double b1x = 0;
                double b1y = 0;
                int idxa0 = 0;
                int idxa1 = 0;
                int idxb0 = 0;
                int idxb1 = 0;
                int i0 = 0;
                int i1 = 0;
                int ik = 0;
                int j0 = 0;
                int j1 = 0;
                int jk = 0;
                int t = 0;
                int offsa = 0;
                int offsb = 0;
                int i_ = 0;
                int i1_ = 0;


                //
                // if matrix size is zero
                //
                if (m == 0 || n == 0)
                {
                    return;
                }

                //
                // Try optimized code
                //
                if (cmatrixgemmf(m, n, k, alpha, a, ia, ja, optypea, b, ib, jb, optypeb, beta, c, ic, jc))
                {
                    return;
                }

                //
                // if K=0, then C=Beta*C
                //
                if (k == 0)
                {
                    if (beta != 1)
                    {
                        if (beta != 0)
                        {
                            for (i = 0; i <= m - 1; i++)
                            {
                                for (j = 0; j <= n - 1; j++)
                                {
                                    c[ic + i, jc + j] = beta * c[ic + i, jc + j];
                                }
                            }
                        }
                        else
                        {
                            for (i = 0; i <= m - 1; i++)
                            {
                                for (j = 0; j <= n - 1; j++)
                                {
                                    c[ic + i, jc + j] = 0;
                                }
                            }
                        }
                    }
                    return;
                }

                //
                // This phase is not really necessary, but compiler complains
                // about "possibly uninitialized variables"
                //
                a0x = 0;
                a0y = 0;
                a1x = 0;
                a1y = 0;
                b0x = 0;
                b0y = 0;
                b1x = 0;
                b1y = 0;

                //
                // General case
                //
                i = 0;
                while (i < m)
                {
                    j = 0;
                    while (j < n)
                    {

                        //
                        // Choose between specialized 4x4 code and general code
                        //
                        if (i + 2 <= m && j + 2 <= n)
                        {

                            //
                            // Specialized 4x4 code for [I..I+3]x[J..J+3] submatrix of C.
                            //
                            // This submatrix is calculated as sum of K rank-1 products,
                            // with operands cached in local variables in order to speed
                            // up operations with arrays.
                            //
                            v00x = 0.0;
                            v00y = 0.0;
                            v01x = 0.0;
                            v01y = 0.0;
                            v10x = 0.0;
                            v10y = 0.0;
                            v11x = 0.0;
                            v11y = 0.0;
                            if (optypea == 0)
                            {
                                idxa0 = ia + i + 0;
                                idxa1 = ia + i + 1;
                                offsa = ja;
                            }
                            else
                            {
                                idxa0 = ja + i + 0;
                                idxa1 = ja + i + 1;
                                offsa = ia;
                            }
                            if (optypeb == 0)
                            {
                                idxb0 = jb + j + 0;
                                idxb1 = jb + j + 1;
                                offsb = ib;
                            }
                            else
                            {
                                idxb0 = ib + j + 0;
                                idxb1 = ib + j + 1;
                                offsb = jb;
                            }
                            for (t = 0; t <= k - 1; t++)
                            {
                                if (optypea == 0)
                                {
                                    a0x = a[idxa0, offsa].x;
                                    a0y = a[idxa0, offsa].y;
                                    a1x = a[idxa1, offsa].x;
                                    a1y = a[idxa1, offsa].y;
                                }
                                if (optypea == 1)
                                {
                                    a0x = a[offsa, idxa0].x;
                                    a0y = a[offsa, idxa0].y;
                                    a1x = a[offsa, idxa1].x;
                                    a1y = a[offsa, idxa1].y;
                                }
                                if (optypea == 2)
                                {
                                    a0x = a[offsa, idxa0].x;
                                    a0y = -a[offsa, idxa0].y;
                                    a1x = a[offsa, idxa1].x;
                                    a1y = -a[offsa, idxa1].y;
                                }
                                if (optypeb == 0)
                                {
                                    b0x = b[offsb, idxb0].x;
                                    b0y = b[offsb, idxb0].y;
                                    b1x = b[offsb, idxb1].x;
                                    b1y = b[offsb, idxb1].y;
                                }
                                if (optypeb == 1)
                                {
                                    b0x = b[idxb0, offsb].x;
                                    b0y = b[idxb0, offsb].y;
                                    b1x = b[idxb1, offsb].x;
                                    b1y = b[idxb1, offsb].y;
                                }
                                if (optypeb == 2)
                                {
                                    b0x = b[idxb0, offsb].x;
                                    b0y = -b[idxb0, offsb].y;
                                    b1x = b[idxb1, offsb].x;
                                    b1y = -b[idxb1, offsb].y;
                                }
                                v00x = v00x + a0x * b0x - a0y * b0y;
                                v00y = v00y + a0x * b0y + a0y * b0x;
                                v01x = v01x + a0x * b1x - a0y * b1y;
                                v01y = v01y + a0x * b1y + a0y * b1x;
                                v10x = v10x + a1x * b0x - a1y * b0y;
                                v10y = v10y + a1x * b0y + a1y * b0x;
                                v11x = v11x + a1x * b1x - a1y * b1y;
                                v11y = v11y + a1x * b1y + a1y * b1x;
                                offsa = offsa + 1;
                                offsb = offsb + 1;
                            }
                            v00.x = v00x;
                            v00.y = v00y;
                            v10.x = v10x;
                            v10.y = v10y;
                            v01.x = v01x;
                            v01.y = v01y;
                            v11.x = v11x;
                            v11.y = v11y;
                            if (beta == 0)
                            {
                                c[ic + i + 0, jc + j + 0] = alpha * v00;
                                c[ic + i + 0, jc + j + 1] = alpha * v01;
                                c[ic + i + 1, jc + j + 0] = alpha * v10;
                                c[ic + i + 1, jc + j + 1] = alpha * v11;
                            }
                            else
                            {
                                c[ic + i + 0, jc + j + 0] = beta * c[ic + i + 0, jc + j + 0] + alpha * v00;
                                c[ic + i + 0, jc + j + 1] = beta * c[ic + i + 0, jc + j + 1] + alpha * v01;
                                c[ic + i + 1, jc + j + 0] = beta * c[ic + i + 1, jc + j + 0] + alpha * v10;
                                c[ic + i + 1, jc + j + 1] = beta * c[ic + i + 1, jc + j + 1] + alpha * v11;
                            }
                        }
                        else
                        {

                            //
                            // Determine submatrix [I0..I1]x[J0..J1] to process
                            //
                            i0 = i;
                            i1 = Math.Min(i + 1, m - 1);
                            j0 = j;
                            j1 = Math.Min(j + 1, n - 1);

                            //
                            // Process submatrix
                            //
                            for (ik = i0; ik <= i1; ik++)
                            {
                                for (jk = j0; jk <= j1; jk++)
                                {
                                    if (k == 0 || alpha == 0)
                                    {
                                        v = 0;
                                    }
                                    else
                                    {
                                        v = 0.0;
                                        if (optypea == 0 && optypeb == 0)
                                        {
                                            i1_ = (ib) - (ja);
                                            v = 0.0;
                                            for (i_ = ja; i_ <= ja + k - 1; i_++)
                                            {
                                                v += a[ia + ik, i_] * b[i_ + i1_, jb + jk];
                                            }
                                        }
                                        if (optypea == 0 && optypeb == 1)
                                        {
                                            i1_ = (jb) - (ja);
                                            v = 0.0;
                                            for (i_ = ja; i_ <= ja + k - 1; i_++)
                                            {
                                                v += a[ia + ik, i_] * b[ib + jk, i_ + i1_];
                                            }
                                        }
                                        if (optypea == 0 && optypeb == 2)
                                        {
                                            i1_ = (jb) - (ja);
                                            v = 0.0;
                                            for (i_ = ja; i_ <= ja + k - 1; i_++)
                                            {
                                                v += a[ia + ik, i_] * math.conj(b[ib + jk, i_ + i1_]);
                                            }
                                        }
                                        if (optypea == 1 && optypeb == 0)
                                        {
                                            i1_ = (ib) - (ia);
                                            v = 0.0;
                                            for (i_ = ia; i_ <= ia + k - 1; i_++)
                                            {
                                                v += a[i_, ja + ik] * b[i_ + i1_, jb + jk];
                                            }
                                        }
                                        if (optypea == 1 && optypeb == 1)
                                        {
                                            i1_ = (jb) - (ia);
                                            v = 0.0;
                                            for (i_ = ia; i_ <= ia + k - 1; i_++)
                                            {
                                                v += a[i_, ja + ik] * b[ib + jk, i_ + i1_];
                                            }
                                        }
                                        if (optypea == 1 && optypeb == 2)
                                        {
                                            i1_ = (jb) - (ia);
                                            v = 0.0;
                                            for (i_ = ia; i_ <= ia + k - 1; i_++)
                                            {
                                                v += a[i_, ja + ik] * math.conj(b[ib + jk, i_ + i1_]);
                                            }
                                        }
                                        if (optypea == 2 && optypeb == 0)
                                        {
                                            i1_ = (ib) - (ia);
                                            v = 0.0;
                                            for (i_ = ia; i_ <= ia + k - 1; i_++)
                                            {
                                                v += math.conj(a[i_, ja + ik]) * b[i_ + i1_, jb + jk];
                                            }
                                        }
                                        if (optypea == 2 && optypeb == 1)
                                        {
                                            i1_ = (jb) - (ia);
                                            v = 0.0;
                                            for (i_ = ia; i_ <= ia + k - 1; i_++)
                                            {
                                                v += math.conj(a[i_, ja + ik]) * b[ib + jk, i_ + i1_];
                                            }
                                        }
                                        if (optypea == 2 && optypeb == 2)
                                        {
                                            i1_ = (jb) - (ia);
                                            v = 0.0;
                                            for (i_ = ia; i_ <= ia + k - 1; i_++)
                                            {
                                                v += math.conj(a[i_, ja + ik]) * math.conj(b[ib + jk, i_ + i1_]);
                                            }
                                        }
                                    }
                                    if (beta == 0)
                                    {
                                        c[ic + ik, jc + jk] = alpha * v;
                                    }
                                    else
                                    {
                                        c[ic + ik, jc + jk] = beta * c[ic + ik, jc + jk] + alpha * v;
                                    }
                                }
                            }
                        }
                        j = j + 2;
                    }
                    i = i + 2;
                }
            }


            /*************************************************************************
            RMatrixGEMM kernel, basecase code for RMatrixGEMM.

            This subroutine calculates C = alpha*op1(A)*op2(B) +beta*C where:
            * C is MxN general matrix
            * op1(A) is MxK matrix
            * op2(B) is KxN matrix
            * "op" may be identity transformation, transposition

            Additional info:
            * multiplication result replaces C. If Beta=0, C elements are not used in
              calculations (not multiplied by zero - just not referenced)
            * if Alpha=0, A is not used (not multiplied by zero - just not referenced)
            * if both Beta and Alpha are zero, C is filled by zeros.

            IMPORTANT:

            This function does NOT preallocate output matrix C, it MUST be preallocated
            by caller prior to calling this function. In case C does not have  enough
            space to store result, exception will be generated.

            INPUT PARAMETERS
                M       -   matrix size, M>0
                N       -   matrix size, N>0
                K       -   matrix size, K>0
                Alpha   -   coefficient
                A       -   matrix
                IA      -   submatrix offset
                JA      -   submatrix offset
                OpTypeA -   transformation type:
                            * 0 - no transformation
                            * 1 - transposition
                B       -   matrix
                IB      -   submatrix offset
                JB      -   submatrix offset
                OpTypeB -   transformation type:
                            * 0 - no transformation
                            * 1 - transposition
                Beta    -   coefficient
                C       -   PREALLOCATED output matrix
                IC      -   submatrix offset
                JC      -   submatrix offset

              -- ALGLIB routine --
                 27.03.2013
                 Bochkanov Sergey
            *************************************************************************/
            public static void rmatrixgemmk(int m,
                int n,
                int k,
                double alpha,
                double[,] a,
                int ia,
                int ja,
                int optypea,
                double[,] b,
                int ib,
                int jb,
                int optypeb,
                double beta,
                double[,] c,
                int ic,
                int jc)
            {
                int i = 0;
                int j = 0;


                //
                // if matrix size is zero
                //
                if (m == 0 || n == 0)
                {
                    return;
                }

                //
                // Try optimized code
                //
                if (rmatrixgemmf(m, n, k, alpha, a, ia, ja, optypea, b, ib, jb, optypeb, beta, c, ic, jc))
                {
                    return;
                }

                //
                // if K=0, then C=Beta*C
                //
                if (k == 0 || (double)(alpha) == (double)(0))
                {
                    if ((double)(beta) != (double)(1))
                    {
                        if ((double)(beta) != (double)(0))
                        {
                            for (i = 0; i <= m - 1; i++)
                            {
                                for (j = 0; j <= n - 1; j++)
                                {
                                    c[ic + i, jc + j] = beta * c[ic + i, jc + j];
                                }
                            }
                        }
                        else
                        {
                            for (i = 0; i <= m - 1; i++)
                            {
                                for (j = 0; j <= n - 1; j++)
                                {
                                    c[ic + i, jc + j] = 0;
                                }
                            }
                        }
                    }
                    return;
                }

                //
                // Call specialized code.
                //
                // NOTE: specialized code was moved to separate function because of strange
                //       issues with instructions cache on some systems; Having too long
                //       functions significantly slows down internal loop of the algorithm.
                //
                if (optypea == 0 && optypeb == 0)
                {
                    rmatrixgemmk44v00(m, n, k, alpha, a, ia, ja, b, ib, jb, beta, c, ic, jc);
                }
                if (optypea == 0 && optypeb != 0)
                {
                    rmatrixgemmk44v01(m, n, k, alpha, a, ia, ja, b, ib, jb, beta, c, ic, jc);
                }
                if (optypea != 0 && optypeb == 0)
                {
                    rmatrixgemmk44v10(m, n, k, alpha, a, ia, ja, b, ib, jb, beta, c, ic, jc);
                }
                if (optypea != 0 && optypeb != 0)
                {
                    rmatrixgemmk44v11(m, n, k, alpha, a, ia, ja, b, ib, jb, beta, c, ic, jc);
                }
            }


            /*************************************************************************
            RMatrixGEMM kernel, basecase code for RMatrixGEMM, specialized for sitation
            with OpTypeA=0 and OpTypeB=0.

            Additional info:
            * this function requires that Alpha<>0 (assertion is thrown otherwise)

            INPUT PARAMETERS
                M       -   matrix size, M>0
                N       -   matrix size, N>0
                K       -   matrix size, K>0
                Alpha   -   coefficient
                A       -   matrix
                IA      -   submatrix offset
                JA      -   submatrix offset
                B       -   matrix
                IB      -   submatrix offset
                JB      -   submatrix offset
                Beta    -   coefficient
                C       -   PREALLOCATED output matrix
                IC      -   submatrix offset
                JC      -   submatrix offset

              -- ALGLIB routine --
                 27.03.2013
                 Bochkanov Sergey
            *************************************************************************/
            public static void rmatrixgemmk44v00(int m,
                int n,
                int k,
                double alpha,
                double[,] a,
                int ia,
                int ja,
                double[,] b,
                int ib,
                int jb,
                double beta,
                double[,] c,
                int ic,
                int jc)
            {
                int i = 0;
                int j = 0;
                double v = 0;
                double v00 = 0;
                double v01 = 0;
                double v02 = 0;
                double v03 = 0;
                double v10 = 0;
                double v11 = 0;
                double v12 = 0;
                double v13 = 0;
                double v20 = 0;
                double v21 = 0;
                double v22 = 0;
                double v23 = 0;
                double v30 = 0;
                double v31 = 0;
                double v32 = 0;
                double v33 = 0;
                double a0 = 0;
                double a1 = 0;
                double a2 = 0;
                double a3 = 0;
                double b0 = 0;
                double b1 = 0;
                double b2 = 0;
                double b3 = 0;
                int idxa0 = 0;
                int idxa1 = 0;
                int idxa2 = 0;
                int idxa3 = 0;
                int idxb0 = 0;
                int idxb1 = 0;
                int idxb2 = 0;
                int idxb3 = 0;
                int i0 = 0;
                int i1 = 0;
                int ik = 0;
                int j0 = 0;
                int j1 = 0;
                int jk = 0;
                int t = 0;
                int offsa = 0;
                int offsb = 0;
                int i_ = 0;
                int i1_ = 0;

                alglib.ap.assert((double)(alpha) != (double)(0), "RMatrixGEMMK44V00: internal error (Alpha=0)");

                //
                // if matrix size is zero
                //
                if (m == 0 || n == 0)
                {
                    return;
                }

                //
                // A*B
                //
                i = 0;
                while (i < m)
                {
                    j = 0;
                    while (j < n)
                    {

                        //
                        // Choose between specialized 4x4 code and general code
                        //
                        if (i + 4 <= m && j + 4 <= n)
                        {

                            //
                            // Specialized 4x4 code for [I..I+3]x[J..J+3] submatrix of C.
                            //
                            // This submatrix is calculated as sum of K rank-1 products,
                            // with operands cached in local variables in order to speed
                            // up operations with arrays.
                            //
                            idxa0 = ia + i + 0;
                            idxa1 = ia + i + 1;
                            idxa2 = ia + i + 2;
                            idxa3 = ia + i + 3;
                            offsa = ja;
                            idxb0 = jb + j + 0;
                            idxb1 = jb + j + 1;
                            idxb2 = jb + j + 2;
                            idxb3 = jb + j + 3;
                            offsb = ib;
                            v00 = 0.0;
                            v01 = 0.0;
                            v02 = 0.0;
                            v03 = 0.0;
                            v10 = 0.0;
                            v11 = 0.0;
                            v12 = 0.0;
                            v13 = 0.0;
                            v20 = 0.0;
                            v21 = 0.0;
                            v22 = 0.0;
                            v23 = 0.0;
                            v30 = 0.0;
                            v31 = 0.0;
                            v32 = 0.0;
                            v33 = 0.0;

                            //
                            // Different variants of internal loop
                            //
                            for (t = 0; t <= k - 1; t++)
                            {
                                a0 = a[idxa0, offsa];
                                a1 = a[idxa1, offsa];
                                b0 = b[offsb, idxb0];
                                b1 = b[offsb, idxb1];
                                v00 = v00 + a0 * b0;
                                v01 = v01 + a0 * b1;
                                v10 = v10 + a1 * b0;
                                v11 = v11 + a1 * b1;
                                a2 = a[idxa2, offsa];
                                a3 = a[idxa3, offsa];
                                v20 = v20 + a2 * b0;
                                v21 = v21 + a2 * b1;
                                v30 = v30 + a3 * b0;
                                v31 = v31 + a3 * b1;
                                b2 = b[offsb, idxb2];
                                b3 = b[offsb, idxb3];
                                v22 = v22 + a2 * b2;
                                v23 = v23 + a2 * b3;
                                v32 = v32 + a3 * b2;
                                v33 = v33 + a3 * b3;
                                v02 = v02 + a0 * b2;
                                v03 = v03 + a0 * b3;
                                v12 = v12 + a1 * b2;
                                v13 = v13 + a1 * b3;
                                offsa = offsa + 1;
                                offsb = offsb + 1;
                            }
                            if ((double)(beta) == (double)(0))
                            {
                                c[ic + i + 0, jc + j + 0] = alpha * v00;
                                c[ic + i + 0, jc + j + 1] = alpha * v01;
                                c[ic + i + 0, jc + j + 2] = alpha * v02;
                                c[ic + i + 0, jc + j + 3] = alpha * v03;
                                c[ic + i + 1, jc + j + 0] = alpha * v10;
                                c[ic + i + 1, jc + j + 1] = alpha * v11;
                                c[ic + i + 1, jc + j + 2] = alpha * v12;
                                c[ic + i + 1, jc + j + 3] = alpha * v13;
                                c[ic + i + 2, jc + j + 0] = alpha * v20;
                                c[ic + i + 2, jc + j + 1] = alpha * v21;
                                c[ic + i + 2, jc + j + 2] = alpha * v22;
                                c[ic + i + 2, jc + j + 3] = alpha * v23;
                                c[ic + i + 3, jc + j + 0] = alpha * v30;
                                c[ic + i + 3, jc + j + 1] = alpha * v31;
                                c[ic + i + 3, jc + j + 2] = alpha * v32;
                                c[ic + i + 3, jc + j + 3] = alpha * v33;
                            }
                            else
                            {
                                c[ic + i + 0, jc + j + 0] = beta * c[ic + i + 0, jc + j + 0] + alpha * v00;
                                c[ic + i + 0, jc + j + 1] = beta * c[ic + i + 0, jc + j + 1] + alpha * v01;
                                c[ic + i + 0, jc + j + 2] = beta * c[ic + i + 0, jc + j + 2] + alpha * v02;
                                c[ic + i + 0, jc + j + 3] = beta * c[ic + i + 0, jc + j + 3] + alpha * v03;
                                c[ic + i + 1, jc + j + 0] = beta * c[ic + i + 1, jc + j + 0] + alpha * v10;
                                c[ic + i + 1, jc + j + 1] = beta * c[ic + i + 1, jc + j + 1] + alpha * v11;
                                c[ic + i + 1, jc + j + 2] = beta * c[ic + i + 1, jc + j + 2] + alpha * v12;
                                c[ic + i + 1, jc + j + 3] = beta * c[ic + i + 1, jc + j + 3] + alpha * v13;
                                c[ic + i + 2, jc + j + 0] = beta * c[ic + i + 2, jc + j + 0] + alpha * v20;
                                c[ic + i + 2, jc + j + 1] = beta * c[ic + i + 2, jc + j + 1] + alpha * v21;
                                c[ic + i + 2, jc + j + 2] = beta * c[ic + i + 2, jc + j + 2] + alpha * v22;
                                c[ic + i + 2, jc + j + 3] = beta * c[ic + i + 2, jc + j + 3] + alpha * v23;
                                c[ic + i + 3, jc + j + 0] = beta * c[ic + i + 3, jc + j + 0] + alpha * v30;
                                c[ic + i + 3, jc + j + 1] = beta * c[ic + i + 3, jc + j + 1] + alpha * v31;
                                c[ic + i + 3, jc + j + 2] = beta * c[ic + i + 3, jc + j + 2] + alpha * v32;
                                c[ic + i + 3, jc + j + 3] = beta * c[ic + i + 3, jc + j + 3] + alpha * v33;
                            }
                        }
                        else
                        {

                            //
                            // Determine submatrix [I0..I1]x[J0..J1] to process
                            //
                            i0 = i;
                            i1 = Math.Min(i + 3, m - 1);
                            j0 = j;
                            j1 = Math.Min(j + 3, n - 1);

                            //
                            // Process submatrix
                            //
                            for (ik = i0; ik <= i1; ik++)
                            {
                                for (jk = j0; jk <= j1; jk++)
                                {
                                    if (k == 0 || (double)(alpha) == (double)(0))
                                    {
                                        v = 0;
                                    }
                                    else
                                    {
                                        i1_ = (ib) - (ja);
                                        v = 0.0;
                                        for (i_ = ja; i_ <= ja + k - 1; i_++)
                                        {
                                            v += a[ia + ik, i_] * b[i_ + i1_, jb + jk];
                                        }
                                    }
                                    if ((double)(beta) == (double)(0))
                                    {
                                        c[ic + ik, jc + jk] = alpha * v;
                                    }
                                    else
                                    {
                                        c[ic + ik, jc + jk] = beta * c[ic + ik, jc + jk] + alpha * v;
                                    }
                                }
                            }
                        }
                        j = j + 4;
                    }
                    i = i + 4;
                }
            }


            /*************************************************************************
            RMatrixGEMM kernel, basecase code for RMatrixGEMM, specialized for sitation
            with OpTypeA=0 and OpTypeB=1.

            Additional info:
            * this function requires that Alpha<>0 (assertion is thrown otherwise)

            INPUT PARAMETERS
                M       -   matrix size, M>0
                N       -   matrix size, N>0
                K       -   matrix size, K>0
                Alpha   -   coefficient
                A       -   matrix
                IA      -   submatrix offset
                JA      -   submatrix offset
                B       -   matrix
                IB      -   submatrix offset
                JB      -   submatrix offset
                Beta    -   coefficient
                C       -   PREALLOCATED output matrix
                IC      -   submatrix offset
                JC      -   submatrix offset

              -- ALGLIB routine --
                 27.03.2013
                 Bochkanov Sergey
            *************************************************************************/
            public static void rmatrixgemmk44v01(int m,
                int n,
                int k,
                double alpha,
                double[,] a,
                int ia,
                int ja,
                double[,] b,
                int ib,
                int jb,
                double beta,
                double[,] c,
                int ic,
                int jc)
            {
                int i = 0;
                int j = 0;
                double v = 0;
                double v00 = 0;
                double v01 = 0;
                double v02 = 0;
                double v03 = 0;
                double v10 = 0;
                double v11 = 0;
                double v12 = 0;
                double v13 = 0;
                double v20 = 0;
                double v21 = 0;
                double v22 = 0;
                double v23 = 0;
                double v30 = 0;
                double v31 = 0;
                double v32 = 0;
                double v33 = 0;
                double a0 = 0;
                double a1 = 0;
                double a2 = 0;
                double a3 = 0;
                double b0 = 0;
                double b1 = 0;
                double b2 = 0;
                double b3 = 0;
                int idxa0 = 0;
                int idxa1 = 0;
                int idxa2 = 0;
                int idxa3 = 0;
                int idxb0 = 0;
                int idxb1 = 0;
                int idxb2 = 0;
                int idxb3 = 0;
                int i0 = 0;
                int i1 = 0;
                int ik = 0;
                int j0 = 0;
                int j1 = 0;
                int jk = 0;
                int t = 0;
                int offsa = 0;
                int offsb = 0;
                int i_ = 0;
                int i1_ = 0;

                alglib.ap.assert((double)(alpha) != (double)(0), "RMatrixGEMMK44V00: internal error (Alpha=0)");

                //
                // if matrix size is zero
                //
                if (m == 0 || n == 0)
                {
                    return;
                }

                //
                // A*B'
                //
                i = 0;
                while (i < m)
                {
                    j = 0;
                    while (j < n)
                    {

                        //
                        // Choose between specialized 4x4 code and general code
                        //
                        if (i + 4 <= m && j + 4 <= n)
                        {

                            //
                            // Specialized 4x4 code for [I..I+3]x[J..J+3] submatrix of C.
                            //
                            // This submatrix is calculated as sum of K rank-1 products,
                            // with operands cached in local variables in order to speed
                            // up operations with arrays.
                            //
                            idxa0 = ia + i + 0;
                            idxa1 = ia + i + 1;
                            idxa2 = ia + i + 2;
                            idxa3 = ia + i + 3;
                            offsa = ja;
                            idxb0 = ib + j + 0;
                            idxb1 = ib + j + 1;
                            idxb2 = ib + j + 2;
                            idxb3 = ib + j + 3;
                            offsb = jb;
                            v00 = 0.0;
                            v01 = 0.0;
                            v02 = 0.0;
                            v03 = 0.0;
                            v10 = 0.0;
                            v11 = 0.0;
                            v12 = 0.0;
                            v13 = 0.0;
                            v20 = 0.0;
                            v21 = 0.0;
                            v22 = 0.0;
                            v23 = 0.0;
                            v30 = 0.0;
                            v31 = 0.0;
                            v32 = 0.0;
                            v33 = 0.0;
                            for (t = 0; t <= k - 1; t++)
                            {
                                a0 = a[idxa0, offsa];
                                a1 = a[idxa1, offsa];
                                b0 = b[idxb0, offsb];
                                b1 = b[idxb1, offsb];
                                v00 = v00 + a0 * b0;
                                v01 = v01 + a0 * b1;
                                v10 = v10 + a1 * b0;
                                v11 = v11 + a1 * b1;
                                a2 = a[idxa2, offsa];
                                a3 = a[idxa3, offsa];
                                v20 = v20 + a2 * b0;
                                v21 = v21 + a2 * b1;
                                v30 = v30 + a3 * b0;
                                v31 = v31 + a3 * b1;
                                b2 = b[idxb2, offsb];
                                b3 = b[idxb3, offsb];
                                v22 = v22 + a2 * b2;
                                v23 = v23 + a2 * b3;
                                v32 = v32 + a3 * b2;
                                v33 = v33 + a3 * b3;
                                v02 = v02 + a0 * b2;
                                v03 = v03 + a0 * b3;
                                v12 = v12 + a1 * b2;
                                v13 = v13 + a1 * b3;
                                offsa = offsa + 1;
                                offsb = offsb + 1;
                            }
                            if ((double)(beta) == (double)(0))
                            {
                                c[ic + i + 0, jc + j + 0] = alpha * v00;
                                c[ic + i + 0, jc + j + 1] = alpha * v01;
                                c[ic + i + 0, jc + j + 2] = alpha * v02;
                                c[ic + i + 0, jc + j + 3] = alpha * v03;
                                c[ic + i + 1, jc + j + 0] = alpha * v10;
                                c[ic + i + 1, jc + j + 1] = alpha * v11;
                                c[ic + i + 1, jc + j + 2] = alpha * v12;
                                c[ic + i + 1, jc + j + 3] = alpha * v13;
                                c[ic + i + 2, jc + j + 0] = alpha * v20;
                                c[ic + i + 2, jc + j + 1] = alpha * v21;
                                c[ic + i + 2, jc + j + 2] = alpha * v22;
                                c[ic + i + 2, jc + j + 3] = alpha * v23;
                                c[ic + i + 3, jc + j + 0] = alpha * v30;
                                c[ic + i + 3, jc + j + 1] = alpha * v31;
                                c[ic + i + 3, jc + j + 2] = alpha * v32;
                                c[ic + i + 3, jc + j + 3] = alpha * v33;
                            }
                            else
                            {
                                c[ic + i + 0, jc + j + 0] = beta * c[ic + i + 0, jc + j + 0] + alpha * v00;
                                c[ic + i + 0, jc + j + 1] = beta * c[ic + i + 0, jc + j + 1] + alpha * v01;
                                c[ic + i + 0, jc + j + 2] = beta * c[ic + i + 0, jc + j + 2] + alpha * v02;
                                c[ic + i + 0, jc + j + 3] = beta * c[ic + i + 0, jc + j + 3] + alpha * v03;
                                c[ic + i + 1, jc + j + 0] = beta * c[ic + i + 1, jc + j + 0] + alpha * v10;
                                c[ic + i + 1, jc + j + 1] = beta * c[ic + i + 1, jc + j + 1] + alpha * v11;
                                c[ic + i + 1, jc + j + 2] = beta * c[ic + i + 1, jc + j + 2] + alpha * v12;
                                c[ic + i + 1, jc + j + 3] = beta * c[ic + i + 1, jc + j + 3] + alpha * v13;
                                c[ic + i + 2, jc + j + 0] = beta * c[ic + i + 2, jc + j + 0] + alpha * v20;
                                c[ic + i + 2, jc + j + 1] = beta * c[ic + i + 2, jc + j + 1] + alpha * v21;
                                c[ic + i + 2, jc + j + 2] = beta * c[ic + i + 2, jc + j + 2] + alpha * v22;
                                c[ic + i + 2, jc + j + 3] = beta * c[ic + i + 2, jc + j + 3] + alpha * v23;
                                c[ic + i + 3, jc + j + 0] = beta * c[ic + i + 3, jc + j + 0] + alpha * v30;
                                c[ic + i + 3, jc + j + 1] = beta * c[ic + i + 3, jc + j + 1] + alpha * v31;
                                c[ic + i + 3, jc + j + 2] = beta * c[ic + i + 3, jc + j + 2] + alpha * v32;
                                c[ic + i + 3, jc + j + 3] = beta * c[ic + i + 3, jc + j + 3] + alpha * v33;
                            }
                        }
                        else
                        {

                            //
                            // Determine submatrix [I0..I1]x[J0..J1] to process
                            //
                            i0 = i;
                            i1 = Math.Min(i + 3, m - 1);
                            j0 = j;
                            j1 = Math.Min(j + 3, n - 1);

                            //
                            // Process submatrix
                            //
                            for (ik = i0; ik <= i1; ik++)
                            {
                                for (jk = j0; jk <= j1; jk++)
                                {
                                    if (k == 0 || (double)(alpha) == (double)(0))
                                    {
                                        v = 0;
                                    }
                                    else
                                    {
                                        i1_ = (jb) - (ja);
                                        v = 0.0;
                                        for (i_ = ja; i_ <= ja + k - 1; i_++)
                                        {
                                            v += a[ia + ik, i_] * b[ib + jk, i_ + i1_];
                                        }
                                    }
                                    if ((double)(beta) == (double)(0))
                                    {
                                        c[ic + ik, jc + jk] = alpha * v;
                                    }
                                    else
                                    {
                                        c[ic + ik, jc + jk] = beta * c[ic + ik, jc + jk] + alpha * v;
                                    }
                                }
                            }
                        }
                        j = j + 4;
                    }
                    i = i + 4;
                }
            }


            /*************************************************************************
            RMatrixGEMM kernel, basecase code for RMatrixGEMM, specialized for sitation
            with OpTypeA=1 and OpTypeB=0.

            Additional info:
            * this function requires that Alpha<>0 (assertion is thrown otherwise)

            INPUT PARAMETERS
                M       -   matrix size, M>0
                N       -   matrix size, N>0
                K       -   matrix size, K>0
                Alpha   -   coefficient
                A       -   matrix
                IA      -   submatrix offset
                JA      -   submatrix offset
                B       -   matrix
                IB      -   submatrix offset
                JB      -   submatrix offset
                Beta    -   coefficient
                C       -   PREALLOCATED output matrix
                IC      -   submatrix offset
                JC      -   submatrix offset

              -- ALGLIB routine --
                 27.03.2013
                 Bochkanov Sergey
            *************************************************************************/
            public static void rmatrixgemmk44v10(int m,
                int n,
                int k,
                double alpha,
                double[,] a,
                int ia,
                int ja,
                double[,] b,
                int ib,
                int jb,
                double beta,
                double[,] c,
                int ic,
                int jc)
            {
                int i = 0;
                int j = 0;
                double v = 0;
                double v00 = 0;
                double v01 = 0;
                double v02 = 0;
                double v03 = 0;
                double v10 = 0;
                double v11 = 0;
                double v12 = 0;
                double v13 = 0;
                double v20 = 0;
                double v21 = 0;
                double v22 = 0;
                double v23 = 0;
                double v30 = 0;
                double v31 = 0;
                double v32 = 0;
                double v33 = 0;
                double a0 = 0;
                double a1 = 0;
                double a2 = 0;
                double a3 = 0;
                double b0 = 0;
                double b1 = 0;
                double b2 = 0;
                double b3 = 0;
                int idxa0 = 0;
                int idxa1 = 0;
                int idxa2 = 0;
                int idxa3 = 0;
                int idxb0 = 0;
                int idxb1 = 0;
                int idxb2 = 0;
                int idxb3 = 0;
                int i0 = 0;
                int i1 = 0;
                int ik = 0;
                int j0 = 0;
                int j1 = 0;
                int jk = 0;
                int t = 0;
                int offsa = 0;
                int offsb = 0;
                int i_ = 0;
                int i1_ = 0;

                alglib.ap.assert((double)(alpha) != (double)(0), "RMatrixGEMMK44V00: internal error (Alpha=0)");

                //
                // if matrix size is zero
                //
                if (m == 0 || n == 0)
                {
                    return;
                }

                //
                // A'*B
                //
                i = 0;
                while (i < m)
                {
                    j = 0;
                    while (j < n)
                    {

                        //
                        // Choose between specialized 4x4 code and general code
                        //
                        if (i + 4 <= m && j + 4 <= n)
                        {

                            //
                            // Specialized 4x4 code for [I..I+3]x[J..J+3] submatrix of C.
                            //
                            // This submatrix is calculated as sum of K rank-1 products,
                            // with operands cached in local variables in order to speed
                            // up operations with arrays.
                            //
                            idxa0 = ja + i + 0;
                            idxa1 = ja + i + 1;
                            idxa2 = ja + i + 2;
                            idxa3 = ja + i + 3;
                            offsa = ia;
                            idxb0 = jb + j + 0;
                            idxb1 = jb + j + 1;
                            idxb2 = jb + j + 2;
                            idxb3 = jb + j + 3;
                            offsb = ib;
                            v00 = 0.0;
                            v01 = 0.0;
                            v02 = 0.0;
                            v03 = 0.0;
                            v10 = 0.0;
                            v11 = 0.0;
                            v12 = 0.0;
                            v13 = 0.0;
                            v20 = 0.0;
                            v21 = 0.0;
                            v22 = 0.0;
                            v23 = 0.0;
                            v30 = 0.0;
                            v31 = 0.0;
                            v32 = 0.0;
                            v33 = 0.0;
                            for (t = 0; t <= k - 1; t++)
                            {
                                a0 = a[offsa, idxa0];
                                a1 = a[offsa, idxa1];
                                b0 = b[offsb, idxb0];
                                b1 = b[offsb, idxb1];
                                v00 = v00 + a0 * b0;
                                v01 = v01 + a0 * b1;
                                v10 = v10 + a1 * b0;
                                v11 = v11 + a1 * b1;
                                a2 = a[offsa, idxa2];
                                a3 = a[offsa, idxa3];
                                v20 = v20 + a2 * b0;
                                v21 = v21 + a2 * b1;
                                v30 = v30 + a3 * b0;
                                v31 = v31 + a3 * b1;
                                b2 = b[offsb, idxb2];
                                b3 = b[offsb, idxb3];
                                v22 = v22 + a2 * b2;
                                v23 = v23 + a2 * b3;
                                v32 = v32 + a3 * b2;
                                v33 = v33 + a3 * b3;
                                v02 = v02 + a0 * b2;
                                v03 = v03 + a0 * b3;
                                v12 = v12 + a1 * b2;
                                v13 = v13 + a1 * b3;
                                offsa = offsa + 1;
                                offsb = offsb + 1;
                            }
                            if ((double)(beta) == (double)(0))
                            {
                                c[ic + i + 0, jc + j + 0] = alpha * v00;
                                c[ic + i + 0, jc + j + 1] = alpha * v01;
                                c[ic + i + 0, jc + j + 2] = alpha * v02;
                                c[ic + i + 0, jc + j + 3] = alpha * v03;
                                c[ic + i + 1, jc + j + 0] = alpha * v10;
                                c[ic + i + 1, jc + j + 1] = alpha * v11;
                                c[ic + i + 1, jc + j + 2] = alpha * v12;
                                c[ic + i + 1, jc + j + 3] = alpha * v13;
                                c[ic + i + 2, jc + j + 0] = alpha * v20;
                                c[ic + i + 2, jc + j + 1] = alpha * v21;
                                c[ic + i + 2, jc + j + 2] = alpha * v22;
                                c[ic + i + 2, jc + j + 3] = alpha * v23;
                                c[ic + i + 3, jc + j + 0] = alpha * v30;
                                c[ic + i + 3, jc + j + 1] = alpha * v31;
                                c[ic + i + 3, jc + j + 2] = alpha * v32;
                                c[ic + i + 3, jc + j + 3] = alpha * v33;
                            }
                            else
                            {
                                c[ic + i + 0, jc + j + 0] = beta * c[ic + i + 0, jc + j + 0] + alpha * v00;
                                c[ic + i + 0, jc + j + 1] = beta * c[ic + i + 0, jc + j + 1] + alpha * v01;
                                c[ic + i + 0, jc + j + 2] = beta * c[ic + i + 0, jc + j + 2] + alpha * v02;
                                c[ic + i + 0, jc + j + 3] = beta * c[ic + i + 0, jc + j + 3] + alpha * v03;
                                c[ic + i + 1, jc + j + 0] = beta * c[ic + i + 1, jc + j + 0] + alpha * v10;
                                c[ic + i + 1, jc + j + 1] = beta * c[ic + i + 1, jc + j + 1] + alpha * v11;
                                c[ic + i + 1, jc + j + 2] = beta * c[ic + i + 1, jc + j + 2] + alpha * v12;
                                c[ic + i + 1, jc + j + 3] = beta * c[ic + i + 1, jc + j + 3] + alpha * v13;
                                c[ic + i + 2, jc + j + 0] = beta * c[ic + i + 2, jc + j + 0] + alpha * v20;
                                c[ic + i + 2, jc + j + 1] = beta * c[ic + i + 2, jc + j + 1] + alpha * v21;
                                c[ic + i + 2, jc + j + 2] = beta * c[ic + i + 2, jc + j + 2] + alpha * v22;
                                c[ic + i + 2, jc + j + 3] = beta * c[ic + i + 2, jc + j + 3] + alpha * v23;
                                c[ic + i + 3, jc + j + 0] = beta * c[ic + i + 3, jc + j + 0] + alpha * v30;
                                c[ic + i + 3, jc + j + 1] = beta * c[ic + i + 3, jc + j + 1] + alpha * v31;
                                c[ic + i + 3, jc + j + 2] = beta * c[ic + i + 3, jc + j + 2] + alpha * v32;
                                c[ic + i + 3, jc + j + 3] = beta * c[ic + i + 3, jc + j + 3] + alpha * v33;
                            }
                        }
                        else
                        {

                            //
                            // Determine submatrix [I0..I1]x[J0..J1] to process
                            //
                            i0 = i;
                            i1 = Math.Min(i + 3, m - 1);
                            j0 = j;
                            j1 = Math.Min(j + 3, n - 1);

                            //
                            // Process submatrix
                            //
                            for (ik = i0; ik <= i1; ik++)
                            {
                                for (jk = j0; jk <= j1; jk++)
                                {
                                    if (k == 0 || (double)(alpha) == (double)(0))
                                    {
                                        v = 0;
                                    }
                                    else
                                    {
                                        v = 0.0;
                                        i1_ = (ib) - (ia);
                                        v = 0.0;
                                        for (i_ = ia; i_ <= ia + k - 1; i_++)
                                        {
                                            v += a[i_, ja + ik] * b[i_ + i1_, jb + jk];
                                        }
                                    }
                                    if ((double)(beta) == (double)(0))
                                    {
                                        c[ic + ik, jc + jk] = alpha * v;
                                    }
                                    else
                                    {
                                        c[ic + ik, jc + jk] = beta * c[ic + ik, jc + jk] + alpha * v;
                                    }
                                }
                            }
                        }
                        j = j + 4;
                    }
                    i = i + 4;
                }
            }


            /*************************************************************************
            RMatrixGEMM kernel, basecase code for RMatrixGEMM, specialized for sitation
            with OpTypeA=1 and OpTypeB=1.

            Additional info:
            * this function requires that Alpha<>0 (assertion is thrown otherwise)

            INPUT PARAMETERS
                M       -   matrix size, M>0
                N       -   matrix size, N>0
                K       -   matrix size, K>0
                Alpha   -   coefficient
                A       -   matrix
                IA      -   submatrix offset
                JA      -   submatrix offset
                B       -   matrix
                IB      -   submatrix offset
                JB      -   submatrix offset
                Beta    -   coefficient
                C       -   PREALLOCATED output matrix
                IC      -   submatrix offset
                JC      -   submatrix offset

              -- ALGLIB routine --
                 27.03.2013
                 Bochkanov Sergey
            *************************************************************************/
            public static void rmatrixgemmk44v11(int m,
                int n,
                int k,
                double alpha,
                double[,] a,
                int ia,
                int ja,
                double[,] b,
                int ib,
                int jb,
                double beta,
                double[,] c,
                int ic,
                int jc)
            {
                int i = 0;
                int j = 0;
                double v = 0;
                double v00 = 0;
                double v01 = 0;
                double v02 = 0;
                double v03 = 0;
                double v10 = 0;
                double v11 = 0;
                double v12 = 0;
                double v13 = 0;
                double v20 = 0;
                double v21 = 0;
                double v22 = 0;
                double v23 = 0;
                double v30 = 0;
                double v31 = 0;
                double v32 = 0;
                double v33 = 0;
                double a0 = 0;
                double a1 = 0;
                double a2 = 0;
                double a3 = 0;
                double b0 = 0;
                double b1 = 0;
                double b2 = 0;
                double b3 = 0;
                int idxa0 = 0;
                int idxa1 = 0;
                int idxa2 = 0;
                int idxa3 = 0;
                int idxb0 = 0;
                int idxb1 = 0;
                int idxb2 = 0;
                int idxb3 = 0;
                int i0 = 0;
                int i1 = 0;
                int ik = 0;
                int j0 = 0;
                int j1 = 0;
                int jk = 0;
                int t = 0;
                int offsa = 0;
                int offsb = 0;
                int i_ = 0;
                int i1_ = 0;

                alglib.ap.assert((double)(alpha) != (double)(0), "RMatrixGEMMK44V00: internal error (Alpha=0)");

                //
                // if matrix size is zero
                //
                if (m == 0 || n == 0)
                {
                    return;
                }

                //
                // A'*B'
                //
                i = 0;
                while (i < m)
                {
                    j = 0;
                    while (j < n)
                    {

                        //
                        // Choose between specialized 4x4 code and general code
                        //
                        if (i + 4 <= m && j + 4 <= n)
                        {

                            //
                            // Specialized 4x4 code for [I..I+3]x[J..J+3] submatrix of C.
                            //
                            // This submatrix is calculated as sum of K rank-1 products,
                            // with operands cached in local variables in order to speed
                            // up operations with arrays.
                            //
                            idxa0 = ja + i + 0;
                            idxa1 = ja + i + 1;
                            idxa2 = ja + i + 2;
                            idxa3 = ja + i + 3;
                            offsa = ia;
                            idxb0 = ib + j + 0;
                            idxb1 = ib + j + 1;
                            idxb2 = ib + j + 2;
                            idxb3 = ib + j + 3;
                            offsb = jb;
                            v00 = 0.0;
                            v01 = 0.0;
                            v02 = 0.0;
                            v03 = 0.0;
                            v10 = 0.0;
                            v11 = 0.0;
                            v12 = 0.0;
                            v13 = 0.0;
                            v20 = 0.0;
                            v21 = 0.0;
                            v22 = 0.0;
                            v23 = 0.0;
                            v30 = 0.0;
                            v31 = 0.0;
                            v32 = 0.0;
                            v33 = 0.0;
                            for (t = 0; t <= k - 1; t++)
                            {
                                a0 = a[offsa, idxa0];
                                a1 = a[offsa, idxa1];
                                b0 = b[idxb0, offsb];
                                b1 = b[idxb1, offsb];
                                v00 = v00 + a0 * b0;
                                v01 = v01 + a0 * b1;
                                v10 = v10 + a1 * b0;
                                v11 = v11 + a1 * b1;
                                a2 = a[offsa, idxa2];
                                a3 = a[offsa, idxa3];
                                v20 = v20 + a2 * b0;
                                v21 = v21 + a2 * b1;
                                v30 = v30 + a3 * b0;
                                v31 = v31 + a3 * b1;
                                b2 = b[idxb2, offsb];
                                b3 = b[idxb3, offsb];
                                v22 = v22 + a2 * b2;
                                v23 = v23 + a2 * b3;
                                v32 = v32 + a3 * b2;
                                v33 = v33 + a3 * b3;
                                v02 = v02 + a0 * b2;
                                v03 = v03 + a0 * b3;
                                v12 = v12 + a1 * b2;
                                v13 = v13 + a1 * b3;
                                offsa = offsa + 1;
                                offsb = offsb + 1;
                            }
                            if ((double)(beta) == (double)(0))
                            {
                                c[ic + i + 0, jc + j + 0] = alpha * v00;
                                c[ic + i + 0, jc + j + 1] = alpha * v01;
                                c[ic + i + 0, jc + j + 2] = alpha * v02;
                                c[ic + i + 0, jc + j + 3] = alpha * v03;
                                c[ic + i + 1, jc + j + 0] = alpha * v10;
                                c[ic + i + 1, jc + j + 1] = alpha * v11;
                                c[ic + i + 1, jc + j + 2] = alpha * v12;
                                c[ic + i + 1, jc + j + 3] = alpha * v13;
                                c[ic + i + 2, jc + j + 0] = alpha * v20;
                                c[ic + i + 2, jc + j + 1] = alpha * v21;
                                c[ic + i + 2, jc + j + 2] = alpha * v22;
                                c[ic + i + 2, jc + j + 3] = alpha * v23;
                                c[ic + i + 3, jc + j + 0] = alpha * v30;
                                c[ic + i + 3, jc + j + 1] = alpha * v31;
                                c[ic + i + 3, jc + j + 2] = alpha * v32;
                                c[ic + i + 3, jc + j + 3] = alpha * v33;
                            }
                            else
                            {
                                c[ic + i + 0, jc + j + 0] = beta * c[ic + i + 0, jc + j + 0] + alpha * v00;
                                c[ic + i + 0, jc + j + 1] = beta * c[ic + i + 0, jc + j + 1] + alpha * v01;
                                c[ic + i + 0, jc + j + 2] = beta * c[ic + i + 0, jc + j + 2] + alpha * v02;
                                c[ic + i + 0, jc + j + 3] = beta * c[ic + i + 0, jc + j + 3] + alpha * v03;
                                c[ic + i + 1, jc + j + 0] = beta * c[ic + i + 1, jc + j + 0] + alpha * v10;
                                c[ic + i + 1, jc + j + 1] = beta * c[ic + i + 1, jc + j + 1] + alpha * v11;
                                c[ic + i + 1, jc + j + 2] = beta * c[ic + i + 1, jc + j + 2] + alpha * v12;
                                c[ic + i + 1, jc + j + 3] = beta * c[ic + i + 1, jc + j + 3] + alpha * v13;
                                c[ic + i + 2, jc + j + 0] = beta * c[ic + i + 2, jc + j + 0] + alpha * v20;
                                c[ic + i + 2, jc + j + 1] = beta * c[ic + i + 2, jc + j + 1] + alpha * v21;
                                c[ic + i + 2, jc + j + 2] = beta * c[ic + i + 2, jc + j + 2] + alpha * v22;
                                c[ic + i + 2, jc + j + 3] = beta * c[ic + i + 2, jc + j + 3] + alpha * v23;
                                c[ic + i + 3, jc + j + 0] = beta * c[ic + i + 3, jc + j + 0] + alpha * v30;
                                c[ic + i + 3, jc + j + 1] = beta * c[ic + i + 3, jc + j + 1] + alpha * v31;
                                c[ic + i + 3, jc + j + 2] = beta * c[ic + i + 3, jc + j + 2] + alpha * v32;
                                c[ic + i + 3, jc + j + 3] = beta * c[ic + i + 3, jc + j + 3] + alpha * v33;
                            }
                        }
                        else
                        {

                            //
                            // Determine submatrix [I0..I1]x[J0..J1] to process
                            //
                            i0 = i;
                            i1 = Math.Min(i + 3, m - 1);
                            j0 = j;
                            j1 = Math.Min(j + 3, n - 1);

                            //
                            // Process submatrix
                            //
                            for (ik = i0; ik <= i1; ik++)
                            {
                                for (jk = j0; jk <= j1; jk++)
                                {
                                    if (k == 0 || (double)(alpha) == (double)(0))
                                    {
                                        v = 0;
                                    }
                                    else
                                    {
                                        v = 0.0;
                                        i1_ = (jb) - (ia);
                                        v = 0.0;
                                        for (i_ = ia; i_ <= ia + k - 1; i_++)
                                        {
                                            v += a[i_, ja + ik] * b[ib + jk, i_ + i1_];
                                        }
                                    }
                                    if ((double)(beta) == (double)(0))
                                    {
                                        c[ic + ik, jc + jk] = alpha * v;
                                    }
                                    else
                                    {
                                        c[ic + ik, jc + jk] = beta * c[ic + ik, jc + jk] + alpha * v;
                                    }
                                }
                            }
                        }
                        j = j + 4;
                    }
                    i = i + 4;
                }
            }


        }

        public class ablasmkl
        {
            /*************************************************************************
            MKL-based kernel

              -- ALGLIB routine --
                 19.01.2010
                 Bochkanov Sergey
            *************************************************************************/
            public static bool rmatrixsyrkmkl(int n,
                int k,
                double alpha,
                double[,] a,
                int ia,
                int ja,
                int optypea,
                double beta,
                double[,] c,
                int ic,
                int jc,
                bool isupper)
            {
                bool result = new bool();

                result = false;
                return result;
            }


            /*************************************************************************
            MKL-based kernel

              -- ALGLIB routine --
                 19.01.2010
                 Bochkanov Sergey
            *************************************************************************/
            public static bool rmatrixgemmmkl(int m,
                int n,
                int k,
                double alpha,
                double[,] a,
                int ia,
                int ja,
                int optypea,
                double[,] b,
                int ib,
                int jb,
                int optypeb,
                double beta,
                double[,] c,
                int ic,
                int jc)
            {
                bool result = new bool();

                result = false;
                return result;
            }


        }

        public class alglibexception : System.Exception
        {
            public string msg;
            public alglibexception(string s)
            {
                msg = s;
            }

        }
    }
}
