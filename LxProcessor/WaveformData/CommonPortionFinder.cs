using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

using LxCommon;
using LxCommon.DataStructures;
using LxCommon.Models;

using LxProcessor.Common;

namespace LxProcessor.WaveformData
{
    public static class CommonPortionFinder
    {
        private static readonly int MinimumSensorNumberInLocator = Config.MinimumSensorNumberInLocator;

        public static Dictionary<string, int> FindActiveSensors(IList<ShortPortion> shortPortionList)
        {
            Dictionary<string, int> sensorDictionary = new Dictionary<string, int>();
            foreach (ShortPortion shortPortion in shortPortionList)
            {
                if (sensorDictionary.ContainsKey(shortPortion.StationId))
                {
                    sensorDictionary[shortPortion.StationId]++;
                }
                else
                {
                    sensorDictionary.Add(shortPortion.StationId, 1);
                }
            }
            return sensorDictionary;
        }


        public static void UpdateActiveSensors(ConcurrentDictionary<string, DateTime> activeSensors, IList<ShortPortion> shortPortionList, DateTime tooOldTime)
        {
            foreach (ShortPortion shortPortion in shortPortionList)
            {
                activeSensors.AddOrUpdate(shortPortion.StationId, shortPortion.StartTime.BaseTime, (key, existingVal) => shortPortion.StartTime.BaseTime);
            }

            activeSensors.RemoveWhere((kvp) =>
            {
                return kvp.Value < tooOldTime;
            });
        }


        private const long MinGapOfCommonPortionInNanoseconds = (long)(LtgConstants.MinGapOfCommonPortionInSecond * DateTimeUtcNano.NanosecondsPerSecond);
        public static List<CommonPortion> GetCommonPortionsBasedOnToa(IList<ShortPortion> shortPortionList)
        {
            List<CommonPortion> commonPortionList = null;
            if (shortPortionList.Count >= MinimumSensorNumberInLocator)
            {
                List<ShortPortion> subShortPortionList = new List<ShortPortion>();
                ShortPortion preShortPortion = null;
                commonPortionList = new List<CommonPortion>();

                foreach (ShortPortion shortPortion in shortPortionList)
                {
                    if (preShortPortion == null)
                    {
                        preShortPortion = shortPortion;
                    }

                    subShortPortionList.Add(shortPortion);

                    if (shortPortion.StartTime.Nanoseconds - preShortPortion.EndTime.Nanoseconds >
                        MinGapOfCommonPortionInNanoseconds)
                    {
                        if (subShortPortionList.Count >= MinimumSensorNumberInLocator)
                        {
                            CommonPortion commonPortion = new CommonPortion(subShortPortionList);
                            commonPortionList.Add(commonPortion);
                        }

                        subShortPortionList = new List<ShortPortion>();
                        preShortPortion = null;
                    }
                    else
                    {
                        preShortPortion = shortPortion;
                    }
                }

                if (subShortPortionList.Count >= MinimumSensorNumberInLocator)
                {
                    CommonPortion commonPortion = new CommonPortion(subShortPortionList);
                    commonPortionList.Add(commonPortion);
                }
            }

            return commonPortionList;
        }

        public static List<CommonPortion> GroupCommonPortionsBasedOnDistance(CommonPortion commonPortion, ConcurrentDictionary<string, StationData> stationData)
        {
            List<CommonPortion> refinedCommonPortions = null;

            if (commonPortion != null)
            {
                refinedCommonPortions = new List<CommonPortion>();


                // Cluster sensors
                List<List<string>> clusters = new List<List<string>>();
                Dictionary<string, List<ShortPortion>> shortPortionTable = new Dictionary<string, List<ShortPortion>>();
                foreach (ShortPortion shortPortion in commonPortion.ShortPortions)
                {
                    AddStationToCluster(shortPortion.StationId, clusters, stationData);
                    if (!shortPortionTable.ContainsKey(shortPortion.StationId))
                    {
                        List<ShortPortion> portionList = new List<ShortPortion> { shortPortion };
                        shortPortionTable.Add(shortPortion.StationId, portionList);
                    }
                    else
                    {
                        shortPortionTable[shortPortion.StationId].Add(shortPortion);
                    }
                }

                RefineStationCluster(clusters, stationData);

                // Each cluster will generate a new common portion
                foreach (List<string> stationIds in clusters)
                {
                    if (stationIds.Count >= MinimumSensorNumberInLocator)
                    {
                        List<ShortPortion> shortPortionList = new List<ShortPortion>();
                        foreach (string sensorId in stationIds)
                        {

                            foreach (ShortPortion shortPortion in shortPortionTable[sensorId])
                            {
                                shortPortionList.Add(shortPortion);
                            }

                        }
                        CommonPortion newCommonPortion = new CommonPortion(shortPortionList);
                        refinedCommonPortions.Add(newCommonPortion);
                    }
                }
            }

            return refinedCommonPortions;
        }

        public static List<CommonPortion> SplitCommonPortionsBasedOnToa(List<CommonPortion> commonPortionList)
        {
            List<CommonPortion> splitCommonPortions = null;

            if (commonPortionList != null)
            {
                splitCommonPortions = new List<CommonPortion>();

                foreach (CommonPortion commonPortion in commonPortionList)
                {
                    List<CommonPortion> split = GetCommonPortionsBasedOnToa(commonPortion.ShortPortions);

                    if (split != null)
                    {
                        splitCommonPortions.AddRange(split);
                    }
                }
            }

            return splitCommonPortions;
        }

        public static List<CommonPortion> RefineCommonPortionsBySplittingTooLongCommonPortion(List<CommonPortion> commonPortions, int maxLengthInMicrosecond)
        {
            List<CommonPortion> refinedList = null;

            if (commonPortions != null)
            {
                refinedList = new List<CommonPortion>();

                for (int i = 0; i < commonPortions.Count; i++)
                {
                    CommonPortion commonPortion = commonPortions[i];
                    List<CommonPortion> split = RefineCommonPortionBySplittingTooLongCommonPortion(commonPortion, maxLengthInMicrosecond);

                    if (split != null)
                    {
                        refinedList.AddRange(split);
                    }
                }
            }

            return refinedList;
        }

        public static List<CommonPortion> RefineCommonPortionsByLimitingNumberOfSensors(List<CommonPortion> commonPortions)
        {
            List<CommonPortion> refinedList = null;
            if (commonPortions != null)
            {
                refinedList = new List<CommonPortion>();
                foreach (CommonPortion commonPortion in commonPortions)
                {
                    List<CommonPortion> split = RefineCommonPortionByLimittingNumberOfSensors(commonPortion);
                    if (split != null)
                    {
                        refinedList.AddRange(split);
                    }
                }
            }

            return refinedList;
        }

        // Only Keep 30 sensors in each commonportion with 10 overlapping if the commonPortion is less than 1.5 ms
        // 
        private static List<CommonPortion> RefineCommonPortionByLimittingNumberOfSensors(CommonPortion commonPortion)
        {
            List<CommonPortion> split = new List<CommonPortion>();
            if (commonPortion.ShortPortions.Count < 40 ||
                (commonPortion.EndTime.Microseconds - commonPortion.StartTime.Microseconds < 1500))
            {
                split.Add(commonPortion);
            }
            else
            {
                int num = commonPortion.ShortPortions.Count / 20;
                for (int i = 0; i < num; i++)
                {
                    int start = i * 20;
                    if (start < 0)
                    {
                        start = 0;
                    }

                    int end = start + 40;
                    if (end > commonPortion.ShortPortions.Count - 20)
                    {
                        end = commonPortion.ShortPortions.Count;
                    }

                    if (end > commonPortion.ShortPortions.Count)
                    {
                        end = commonPortion.ShortPortions.Count;
                    }

                    if (end < start)
                    {
                        break;
                    }

                    CommonPortion subCommonPortion = new CommonPortion();
                    for (int j = start; j < end; j++)
                    {
                        subCommonPortion.AddShortPortion(commonPortion.ShortPortions[j]);
                    }

                    if (subCommonPortion.ShortPortions.Count > 0)
                    {
                        subCommonPortion.SortShortPortions();
                        split.Add(subCommonPortion);
                    }
                }
            }

            return split;
        }

        private static List<CommonPortion> RefineCommonPortionBySplittingTooLongCommonPortion(CommonPortion commonPortion, int maxLengthInMicrosecond)
        {
            List<CommonPortion> split = new List<CommonPortion>();
            long delta = (commonPortion.EndTime.Nanoseconds - commonPortion.StartTime.Nanoseconds) / DateTimeUtcNano.NanosecondsPerMicrosecond;

            int num = (int)(delta / (double)maxLengthInMicrosecond);

            // If the sensor number is not very big, don't split it
            if (num <= 1 || commonPortion.ShortPortions.Count <= LtgConstants.MaxSensorNumberOfValidDetections + 3)
            {
                split.Add(commonPortion);

                return split;
            }

            long beginningTime = commonPortion.StartTime.Nanoseconds;
            List<ShortPortion> newShortPortionList = null;
            long maxLengthInNanoseconds = maxLengthInMicrosecond * DateTimeUtcNano.NanosecondsPerMicrosecond;
            long additionalOverlapInNanoseconds = (maxLengthInMicrosecond + 600) * DateTimeUtcNano.NanosecondsPerMicrosecond;
            long startTime, endTime;

            //List<ShortPortion> shortPortions = new List<ShortPortion>(commonPortion.ShortPortions);

            for (int i = 0; i < num; i++)
            {
                newShortPortionList = new List<ShortPortion>();
                startTime = beginningTime + (i * maxLengthInNanoseconds);
                endTime = startTime + additionalOverlapInNanoseconds;

                foreach (ShortPortion shortPortion in commonPortion.ShortPortions)
                {
                    if (shortPortion.EndTime.Nanoseconds >= startTime && shortPortion.EndTime.Nanoseconds < endTime)
                    {
                        newShortPortionList.Add(shortPortion);
                    }
                }

                // If this is the last portion but it doesn't have enough sensors merge it with the previous one
                if (i == num - 1 && newShortPortionList.Count < MinimumSensorNumberInLocator && split.Count > 0)
                {
                    CommonPortion lastPortion = split[split.Count - 1];
                    lastPortion.AddShortPortions(newShortPortionList);
                }
                else if (newShortPortionList.Count > MinimumSensorNumberInLocator)
                {
                    CommonPortion newCommonPortion = new CommonPortion(newShortPortionList);
                    split.Add(newCommonPortion);
                }
            }

            return split;
        }

        private static void AddStationToCluster(string stationId, List<List<string>> stationClusterList, ConcurrentDictionary<string, StationData> stationData)
        {
            List<string> cluster = null;

            foreach (List<string> clust in stationClusterList)
            {
                cluster = clust;
                if (StationsInRightDistance(stationId, cluster, stationData))
                {
                    if (cluster != null) cluster.Add(stationId);
                    return;
                }
            }

            cluster = new List<string> { stationId };
            stationClusterList.Add(cluster);
        }

        private static bool StationsInRightDistance(string stationId, List<string> otherStationIds, ConcurrentDictionary<string, StationData> stationData)
        {
            bool retVal = false;
            bool tooClose = false;
            StationData station;

            if (stationData.TryGetValue(stationId, out station))
            {
                double minDistance = double.MaxValue;
                double distance;
                foreach (string id in otherStationIds)
                {

                    if (station.DistanceInMicrosecondTable.TryGetValue(id, out distance))
                    {
                        if (distance >= LtgConstants.MinDistanceOfTwoSensorsInMicrosecond)
                        {
                            if (distance < minDistance)
                                minDistance = distance;
                        }
                        else
                        {
                            tooClose = true;
                            break;
                        }
                    }
                    else
                    {
                        tooClose = true;
                        break;
                    }
                }


                if (!tooClose && minDistance < LtgConstants.MaxDistanceOfGroupSensorsInMircrosecond)
                    retVal = true;
            }

            return retVal;
        }

        // 1. Merge two clusters if they are close enough to each other
        // 2. Remove one of two sensors if they are too close to each other
        private static void RefineStationCluster(List<List<string>> clusters, ConcurrentDictionary<string, StationData> stationData)
        {
            if (clusters == null || clusters.Count == 0)
                return;

            int i = 0, j = 0;

            // Mgerge two clusters if they are close enough
            while (i < clusters.Count)
            {
                List<string> cluster1 = clusters[i];
                j = i + 1;
                while (j < clusters.Count)
                {
                    List<string> cluster2 = clusters[j];
                    if (ClustersTooClose(cluster1, cluster2, stationData))
                    {
                        clusters.RemoveAt(j);
                        cluster1.AddRange(cluster2);
                    }
                    else
                        j++;
                }
                i++;
            }

            // Remove sensor that is too close to other sensors
            foreach (List<string> cluster in clusters)
            {
                RemoveTooCloseSensorsFromCluster(cluster, stationData);
            }

            // Remove the clusters without enough sensors
            i = 0;
            while (i < clusters.Count)
            {
                List<string> cluster1 = clusters[i];
                if (cluster1.Count < MinimumSensorNumberInLocator)
                    clusters.RemoveAt(i);
                else
                    i++;
            }
        }

        private static bool ClustersTooClose(List<string> cluster1, List<string> cluster2, ConcurrentDictionary<string, StationData> stationData)
        {
            foreach (string sId1 in cluster1)
            {
                StationData s1;
                if (stationData.TryGetValue(sId1, out s1))
                {
                    double distance;
                    foreach (string sId2 in cluster2)
                    {
                        if (s1.DistanceInMicrosecondTable.TryGetValue(sId2, out distance))
                        {
                            if (distance < LtgConstants.MaxDistanceOfGroupSensorsInMircrosecond / 2.0)
                                return true;
                        }
                    }
                }
            }

            return false;

        }

        private static void RemoveTooCloseSensorsFromCluster(List<string> cluster, ConcurrentDictionary<string, StationData> stationData)
        {
            int i = 0, j = 0;
            while (i < cluster.Count - 1)
            {
                j = i + 1;
                while (j < cluster.Count)
                {
                    if (StationsTooClose(cluster[i], cluster[j], stationData))
                    {
                        cluster.RemoveAt(j);
                    }
                    else
                        j++;
                }
                i++;
            }
        }

        private static bool StationsTooClose(string stationId1, string stationId2, ConcurrentDictionary<string, StationData> stationData)
        {
            bool retVal = false;
            StationData station;
            if (stationData.TryGetValue(stationId1, out station))
            {
                double distance;
                if (station.DistanceInMicrosecondTable.TryGetValue(stationId2, out distance))
                {
                    retVal = (distance < LtgConstants.MinDistanceOfTwoSensorsInMicrosecond);
                }
            }

            return retVal;
        }
    }
}