﻿using LxCommon;
using LxCommon.Models;

namespace LxProcessor.WaveformData
{
    public class StationWaveformSample
    {
        public StationWaveformSample(string stationId, Waveform waveform, SensorVersion major, SensorVersion minor)
        {
            StationId = stationId;
            Waveform = waveform;
            MajorVersion = major;
            MinorVersion = minor;
        }

        public Waveform Waveform { get; private set; }
        public SensorVersion MajorVersion { get; private set; }
        public SensorVersion MinorVersion { get; private set; }
        public string StationId { get; private set; }
    }
}
