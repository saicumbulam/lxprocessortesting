﻿using System;
using System.Collections.Generic;

using LxCommon;
using LxCommon.DataStructures;
using LxCommon.Models;
using LxCommon.Utilities;


namespace LxProcessor.WaveformData
{
    public class StationWaveforms
    {
        private readonly FluxList<Waveform> _waveforms;
        private int _maxAbsAmp;

        public StationWaveforms()
        {
            StationId = string.Empty;
            MajorVersion = SensorVersion.ID_VERSION_UNKNOW;
            MinorVersion = SensorVersion.ID_VERSION_UNKNOW;
            _waveforms = new FluxList<Waveform>(2000);
            _maxAbsAmp = 0;
        }


        public void Initalize(string stationId, SensorVersion major, SensorVersion minor, Waveform waveform)
        {
            StationId = stationId;
            MajorVersion = major;
            MinorVersion = minor;
            AddWaveform(waveform);
            _maxAbsAmp = Math.Abs(waveform.Amplitude);
        }

        public SensorVersion MajorVersion { get; set; }
        public SensorVersion MinorVersion { get; set; }
        public string StationId { get; set; }

        public void AddWaveform(Waveform waveform)
        {
            int absAmp = Math.Abs(waveform.Amplitude);

            _waveforms.Add(waveform);

            if (absAmp > _maxAbsAmp)
            {
                _maxAbsAmp = absAmp;
            }
        }

        public void RemoveNoise()
        {
            int minAmplitude = (int)(LtgConstants.WaveformNoiseThresholdCompareToPeak * _maxAbsAmp);

            int index = 0;
            while (index < _waveforms.Count)
            {
                Waveform waveform = _waveforms[index];
                if (Math.Abs(waveform.Amplitude) < minAmplitude)
                {
                    _waveforms.RemoveAt(index);
                }
                else
                {
                    index++;
                }
            }
        }

        public int WaveformCount
        {
            get { return _waveforms.Count; }
        }

        public IEnumerable<Waveform> Waveforms
        {
            get { return _waveforms; }
        }
        
        //public List<Waveform> Waveforms 
        //{
        //    get { return _waveforms; }
        //}

        //public void SortWaveforms()
        //{
        //    _waveforms.Sort((x, y) => 
        //    {
        //        return x.TimeStamp.CompareTo(y.TimeStamp);
        //    });
        //}

        public void CleanForReuse()
        {
            StationId = string.Empty;
            MajorVersion = SensorVersion.ID_VERSION_UNKNOW;
            MinorVersion = SensorVersion.ID_VERSION_UNKNOW;
            _maxAbsAmp = 0;
            if (_waveforms != null)
            {
                _waveforms.Clear();
            }
            
        }
    }
}
