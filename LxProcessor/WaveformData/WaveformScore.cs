using System;
using System.Collections.Generic;
using System.Globalization;

using LxCommon;
using LxCommon.DataStructures;
using LxCommon.Models;
using LxCommon.Utilities;
using LxProcessor.Common;


namespace LxProcessor.WaveformData
{
    // the score of the waveform will be used to determine the confidence level of a 
    // lightning detection accuracy
    public class WaveformScore : IComparable, ICloneable
    {
        private Waveform _positivePeak;
        private Waveform _negativePeak;
        private Waveform _absPeakWaveform;
        private Waveform _firstPeakWaveform;
        private IList<Waveform> _peakList;

        // The number of samples from the raw data sent from the sensor
        private int _numberOfSamples;
        private int _score;
        private bool _qualifiedForDetection;

        private FlashType _strokeType;

        // 100*(max_sample - min_sample)/average_sample
        private const int PercentOfFluctuation = 0;


        public WaveformScore()
        {
            StationId = string.Empty;
            MajorVersion = SensorVersion.ID_VERSION_UNKNOW;
            MinorVersion  = SensorVersion.ID_VERSION_UNKNOW;
            _peakList = new FluxList<Waveform>();    
            _score = 0;
            _numberOfSamples = 0;
            _qualifiedForDetection = false;
            _strokeType = FlashType.FlashTypeUnknown;
            PeakForCalculatingPeakCurrent = default(Waveform);
        }

        public bool Initalize(string stationId, SensorVersion majorVer, SensorVersion minorVer, IList<Waveform> waveforms)
        {
            StationId = stationId;
            MajorVersion = majorVer;
            MinorVersion = minorVer;
            _qualifiedForDetection = GetScore(waveforms);

            return _qualifiedForDetection;
        }

        private bool GetScore(IList<Waveform> waveforms)
        {
            bool qualifiedForDetection = false;
            
            _score = 0;
            _numberOfSamples = waveforms.Count;
            if (waveforms.Count >= LtgConstants.MinSamplesInOneShortPortion)
            {
                SetPeakList(waveforms, _peakList);

                if (_peakList.Count > 0)
                {
                    if (!IsSaturatedForDetection())
                    {
                        _score = Math.Abs(_absPeakWaveform.Amplitude);
                        qualifiedForDetection = true;
                    }
                }
            }

            return qualifiedForDetection;
        }

        private void SetPeakList(IList<Waveform> waveforms, IList<Waveform> peakList)
        {
            if (waveforms != null && waveforms.Count >= 3)
            {
                _positivePeak = waveforms[0];
                _negativePeak = waveforms[0];
                _absPeakWaveform = waveforms[0];
                _firstPeakWaveform = waveforms[0];


                Waveform preWaveform = waveforms[0];
                Waveform curWaveform = waveforms[1];
                Waveform nextWaveform;
                int i = 2;
                while (i < waveforms.Count)
                {
                    nextWaveform = waveforms[i++];
                    if (curWaveform.Amplitude > 0 && curWaveform.Amplitude > preWaveform.Amplitude &&
                        curWaveform.Amplitude > nextWaveform.Amplitude)
                    {
                        if (_positivePeak.Amplitude < curWaveform.Amplitude)
                        {
                            _positivePeak = curWaveform;
                            if (Math.Abs(_absPeakWaveform.Amplitude) < _positivePeak.Amplitude)
                                _absPeakWaveform = _positivePeak;
                        }
                        peakList.Add(curWaveform);
                    }
                    else if (curWaveform.Amplitude < 0 && curWaveform.Amplitude < preWaveform.Amplitude &&
                             curWaveform.Amplitude < nextWaveform.Amplitude)
                    {
                        if (_negativePeak.Amplitude > curWaveform.Amplitude)
                        {
                            _negativePeak = curWaveform;
                            if (Math.Abs(_absPeakWaveform.Amplitude) < Math.Abs(_negativePeak.Amplitude))
                                _absPeakWaveform = _negativePeak;
                        }
                        peakList.Add(curWaveform);
                    }
                    preWaveform = curWaveform;
                    curWaveform = nextWaveform;
                }

                if (_positivePeak.TimeStamp < _negativePeak.TimeStamp)
                {
                    _firstPeakWaveform = _positivePeak;
                }
                else
                {
                    _firstPeakWaveform = _negativePeak;
                }   
            }
        }

        public bool HasDominantPeak()
        {
            if (PeakList == null || PeakList.Count <= 4)
                return true;

            foreach (Waveform peak in PeakList)
            {
                if (peak.Amplitude > 0 && _positivePeak != peak &&
                    Math.Abs(_positivePeak.Amplitude - peak.Amplitude) < 0.3 * _positivePeak.Amplitude)
                    return false;
                if (peak.Amplitude < 0 && _negativePeak != peak &&
                    Math.Abs(_negativePeak.Amplitude - peak.Amplitude) < 0.3 * Math.Abs(_negativePeak.Amplitude))
                    return false;
            }
            return true;
        }


        public int CompareTo(Object obj)
        {
            //Score = numberOfNoZeroSamples;
            return _score.CompareTo(((WaveformScore)obj)._score);
        }

        public bool QualifiedForDetection
        {
            get { return _qualifiedForDetection; }
            private set { _qualifiedForDetection = value; }
        }

        public string ScoreString
        {
            get
            {
                string postiveValue = _positivePeak.Amplitude.ToString(CultureInfo.InvariantCulture);
                string negativeValue = _negativePeak.Amplitude.ToString(CultureInfo.InvariantCulture);
                return string.Format("({0};{1};{2};{3})", _numberOfSamples, PercentOfFluctuation, postiveValue, negativeValue);
            }
        }

        public void CleanForReuse()
        {
            StationId = string.Empty;
            MajorVersion = SensorVersion.ID_VERSION_UNKNOW;
            MinorVersion  = SensorVersion.ID_VERSION_UNKNOW;
            _score = 0;
            _numberOfSamples = 0;
            _qualifiedForDetection = false;
            _strokeType = FlashType.FlashTypeUnknown;
            PeakForCalculatingPeakCurrent = default(Waveform);
            _absPeakWaveform = default(Waveform);
            _firstPeakWaveform = default(Waveform);
            _negativePeak = default(Waveform);
            _positivePeak = default(Waveform);

            if (_peakList != null)
            {
                _peakList.Clear();
            }
        }

        public SensorVersion MajorVersion { get; set; }

        public SensorVersion MinorVersion { get; set; }

        private bool IsSaturatedForDetection()
        {
            return (Math.Abs(_absPeakWaveform.Amplitude) > LtgConstants.SaturationValueForDetection);
        }

        public string StationId { get; set; }

        public Waveform PositivePeak
        {
            get { return _positivePeak; }
            private set { _positivePeak = value; }
        }

        public Waveform NegativePeak
        {
            get { return _negativePeak; }
            private set { _negativePeak = value; }
        }

        public Waveform AbsPeakWaveform
        {
            get { return _absPeakWaveform; }
            private set { _absPeakWaveform = value; }
        }

        public Waveform FirstPeakWaveform
        {
            get { return _firstPeakWaveform; }
            private set { _firstPeakWaveform = value; }
        }

        public IList<Waveform> PeakList
        {
            get { return _peakList; }
        }

        public Waveform PeakForCalculatingPeakCurrent { get; set; }

        public FlashType StrokeType
        {
            get { return _strokeType; }
            set { _strokeType = value; }
        }

        private int NumberOfSamples
        {
            set { _numberOfSamples = value; }
        }

        private int Score
        {
            set { _score = value; }
        }

        public object Clone()
        {
            WaveformScore clone = (WaveformScore)MemberwiseClone();
            //This will work because Waveform is a value type (struct)
            List<Waveform> peakListClone = new List<Waveform>(_peakList);
            clone._peakList = peakListClone;

            return clone;
        }
    }

}

