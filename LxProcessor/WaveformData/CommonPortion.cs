using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using LxCommon.Models;

namespace LxProcessor.WaveformData
{
    public class CommonPortion : IEquatable<CommonPortion>, ICloneable
    {
        private DateTimeUtcNano _startTime;
        private DateTimeUtcNano _endTime;
        private List<ShortPortion> _shortPortions;
        private ReadOnlyCollection<ShortPortion> _readonlyShortPortions;
        private readonly Guid _id;
        private HashSet<Guid> _mergedPortions;

        public CommonPortion()
        {
            _id = Guid.NewGuid();
            _mergedPortions = new HashSet<Guid>();
            _mergedPortions.Add(_id);
            _startTime = DateTimeUtcNano.MaxValue;
            _endTime = DateTimeUtcNano.MinValue;
            _shortPortions = new List<ShortPortion>(1000);
            _readonlyShortPortions = new ReadOnlyCollection<ShortPortion>(_shortPortions);
        }

        public CommonPortion(IList<ShortPortion> shortPortions) : this()
        {
            AddShortPortions(shortPortions);
        }


        public DateTimeUtcNano StartTime
        {
            get { return _startTime; }
            private set { _startTime = value; }
        }

        public DateTimeUtcNano EndTime
        {
            get { return _endTime; }
            private set { _endTime = value; }
        }

        public ReadOnlyCollection<ShortPortion> ShortPortions
        {
            get { return _readonlyShortPortions; }
        }

        public Guid Id
        {
            get { return _id; }
        }

        public HashSet<Guid> MergedPortions
        {
            get { return _mergedPortions; }
        }

        public void AddShortPortions(IList<ShortPortion> shortPortions)
        {
            if (shortPortions != null && shortPortions.Count > 0)
            {
                int count = shortPortions.Count;
                int i = 0;
                while (i < count)
                {
                    AddShortPortion(shortPortions[i]);
                    i++;
                }
                SortShortPortions();
            }
        }

        public void AddShortPortion(ShortPortion shortPortion)
        {
            if (shortPortion != null)
            {
                if (shortPortion.StartTime < _startTime)
                {
                    _startTime = shortPortion.StartTime;
                }

                if (shortPortion.EndTime > _endTime)
                {
                    _endTime = shortPortion.EndTime;
                }
                
                _shortPortions.Add(shortPortion);
            }
        }

        public void SortShortPortions()
        {
            _shortPortions.Sort();
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((CommonPortion) obj);
        }

        public bool Equals(CommonPortion other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            bool equals = false;
            if (_startTime.Equals(other._startTime) && _endTime.Equals(other._endTime) && _shortPortions.Count == other.ShortPortions.Count)
            {
                if (_shortPortions.Count > 0)
                {
                    int upper = _shortPortions.Count - 1;

                    equals = _shortPortions[0].Equals(other._shortPortions[0]) && _shortPortions[upper].Equals(other._shortPortions[upper]);
                }
            }

            return equals;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (_startTime.GetHashCode() * 397) ^ _endTime.GetHashCode();
            }
        }

        public static bool operator ==(CommonPortion left, CommonPortion right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(CommonPortion left, CommonPortion right)
        {
            return !Equals(left, right);
        }

        public object Clone()
        {
            CommonPortion clone = (CommonPortion)MemberwiseClone();

            if (_shortPortions != null)
            {
                clone._shortPortions = new List<ShortPortion>(_shortPortions.Count);
                clone._readonlyShortPortions = new ReadOnlyCollection<ShortPortion>(clone._shortPortions);
                clone._mergedPortions = new HashSet<Guid>(_mergedPortions);

                foreach (ShortPortion sp in _shortPortions)
                {
                    clone._shortPortions.Add((ShortPortion)sp.Clone());
                }
            }

            clone.SortShortPortions();

            return clone;
        }

        public override string ToString()
        {
            string debugInfo = "";
            debugInfo = string.Format("{0} to {1}:", _startTime.TimeString, _endTime.TimeString);
            return _readonlyShortPortions.Aggregate(debugInfo, (current, shorPortion) => current + (shorPortion.StationId + ","));
        }
    }
}

