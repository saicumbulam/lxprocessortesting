using System;
using System.Collections;
using System.Collections.Generic;

using LxCommon;
using LxCommon.DataStructures;
using LxCommon.Models;
using LxCommon.Utilities;
using LxProcessor.Common;

namespace LxProcessor.WaveformData
{
    public class ShortPortion : IComparable<ShortPortion>, IEquatable<ShortPortion>, ICloneable
    {
      
        private string _stationId;
        private IList<Waveform> _waveforms;
        private WaveformScore _score;
        
        public ShortPortion()
            : this(string.Empty, DateTimeUtcNano.MinValue)
        {

        }

        public ShortPortion(string stationId, DateTimeUtcNano startTime)
        {
            _stationId = stationId;
            StartTime = startTime;
            _waveforms = new FluxList<Waveform>();
            _score = new WaveformScore();
        }

        public void CleanForReuse()
        {
            _stationId = string.Empty;
            StartTime = DateTimeUtcNano.MaxValue;
            EndTime = DateTimeUtcNano.MinValue;

            if (_waveforms != null)
            {
                _waveforms.Clear();
            }

            _score.CleanForReuse();
        }

        private void Initalize(string stationId, DateTimeUtcNano startTime)
        {
            _stationId = stationId;
            StartTime = startTime;

        }

        private void UpdateScore(SensorVersion major, SensorVersion minor)
        {
            bool isValid = _score.Initalize(_stationId, major, minor, _waveforms);
            if (isValid)
            {
                StartTime = _score.FirstPeakWaveform.TimeStamp;
            }
        }

        // Reduce short portion if it is too long
        private const long MaxSizeOfBinsNano = LtgConstants.MaxSizeOfBinsForCrossCorrelation * DateTimeUtcNano.NanosecondsPerMicrosecond;
        private const long MaxBinSizeNanoNoDominantPeak = ((LtgConstants.MaxSizeOfBinsForCrossCorrelation * 3) / 2) * DateTimeUtcNano.NanosecondsPerMicrosecond;
        private const long MaxBinSizeNanoDominantPeak = (LtgConstants.MaxSizeOfBinsForCrossCorrelation / 2) * DateTimeUtcNano.NanosecondsPerMicrosecond;

        private bool CheckShortPortionSize()
        {
            bool valid = false;
            if (_waveforms.Count >= 2)
            {
                // Make sure the bestWaveFormList is not too long
                long portionLengthNano = _waveforms[_waveforms.Count - 1].TimeStamp.Nanoseconds -
                                         _waveforms[0].TimeStamp.Nanoseconds;

                // It is too long, cut the head or the tail
                // If there is a clear peak, then keep the peak as the center 
                if (portionLengthNano >= MaxSizeOfBinsNano)
                {
                    int i = 0;
                    long maxBinSize;
                    if (!_score.HasDominantPeak())
                    {
                        maxBinSize = MaxBinSizeNanoNoDominantPeak;
                    }
                    else
                    {
                        maxBinSize = MaxBinSizeNanoDominantPeak;
                    }

                    while (i < _waveforms.Count)
                    {
                        if (Math.Abs(_waveforms[i].TimeStamp.Nanoseconds - _score.AbsPeakWaveform.TimeStamp.Nanoseconds) > maxBinSize)
                        {
                            _waveforms.RemoveAt(i);
                        }
                        else
                        {
                            i++;
                        }
                    }

                    valid = (_waveforms.Count > 0);
                }
            }

            return valid;
        }

        public WaveformScore WaveformScore
        {
            get { return _score; }
            private set { _score = value; }
        }

        public DateTimeUtcNano StartTime { get; private set; }

        public DateTimeUtcNano EndTime { get; private set; }

        public string StationId
        {
            get { return _stationId; }
            private set { _stationId = value; }
        }

        public IList<Waveform> Waveforms
        {
            get { return _waveforms; }
            private set { _waveforms = value; }
        }

        public int CompareTo(ShortPortion other)
        {
            return WaveformScore.AbsPeakWaveform.TimeStamp.CompareTo(other.WaveformScore.AbsPeakWaveform.TimeStamp);
        }

        public override string ToString()
        {
            return StationId;
        }

       
        private const int MinimumWaveformsPerSecond = 3;
        public static List<ShortPortion> FindQualifiedShortPortions(ICollection<StationWaveforms> waveformsByStation, ConcurrentSimplePool<ShortPortion> shortPortionPool, ConcurrentSimplePool<StationWaveforms> stationWaveformPool)
        {
            List<ShortPortion> shortPortions = null;

            if (waveformsByStation.Count > 0)
            {
                shortPortions = new List<ShortPortion>();
                foreach (StationWaveforms sw in waveformsByStation)
                {
                    sw.RemoveNoise();
                    
                    if (sw.WaveformCount >= MinimumWaveformsPerSecond)
                    {
                        IEnumerable<ShortPortion> sp = FindQualifiedShortPortions(sw, LtgConstants.MinGapOfShortPortionInNanoseconds, shortPortionPool);
                        shortPortions.AddRange(sp);    
                    }
                    
                    stationWaveformPool.Return(sw);
                }
            }

            return shortPortions;
        }

        private static IEnumerable<ShortPortion> FindQualifiedShortPortions(StationWaveforms stationWaveforms, double minGapInNanoseconds, ConcurrentSimplePool<ShortPortion> shortPortionPool)
        {
            long preTime = 0;
            List<ShortPortion> shortPortionsList = new List<ShortPortion>();
            ShortPortion curShortPortion = null;
            List<Waveform> waveforms = new List<Waveform>(stationWaveforms.Waveforms);
            waveforms.Sort();
            foreach (Waveform waveform in waveforms)
            {
                if (waveform.TimeStamp.Nanoseconds - preTime > minGapInNanoseconds)
                {
                    curShortPortion = shortPortionPool.Take();
                    curShortPortion.Initalize(stationWaveforms.StationId, waveform.TimeStamp);
                    shortPortionsList.Add(curShortPortion);
                    preTime = waveform.TimeStamp.Nanoseconds;
                }

                if (curShortPortion != null)
                {
                    curShortPortion.EndTime = waveform.TimeStamp;
                    curShortPortion.Waveforms.Add(waveform);
                    preTime = waveform.TimeStamp.Nanoseconds;
                }
            }

            // Add Score
            int i = 0;
            while (i < shortPortionsList.Count)
            {
                curShortPortion = shortPortionsList[i];
                curShortPortion.UpdateScore(stationWaveforms.MajorVersion, stationWaveforms.MinorVersion);
                if (!curShortPortion.WaveformScore.QualifiedForDetection)
                {
                    shortPortionsList.RemoveAt(i);
                    shortPortionPool.Return(curShortPortion);
                }
                else
                {
                    i++;
                }
            }

            // Check size
            i = 0;
            while (i < shortPortionsList.Count)
            {
                curShortPortion = shortPortionsList[i];

                if (curShortPortion.CheckShortPortionSize())
                {
                    curShortPortion.UpdateScore(stationWaveforms.MajorVersion, stationWaveforms.MinorVersion);
                }

                if (!curShortPortion.WaveformScore.QualifiedForDetection)
                {
                    shortPortionsList.RemoveAt(i);
                    shortPortionPool.Return(curShortPortion);
                }
                else
                {
                    i++;
                }
            }
            
            return shortPortionsList;
        }

      

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ShortPortion) obj);
        }

        public bool Equals(ShortPortion other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(_stationId, other._stationId) && StartTime.Equals(other.StartTime) && EndTime.Equals(other.EndTime);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = (_stationId != null ? _stationId.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ StartTime.GetHashCode();
                hashCode = (hashCode * 397) ^ EndTime.GetHashCode();
                return hashCode;
            }
        }

        public static bool operator ==(ShortPortion left, ShortPortion right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(ShortPortion left, ShortPortion right)
        {
            return !Equals(left, right);
        }

        public object Clone()
        {

            ShortPortion clone = (ShortPortion)MemberwiseClone();

            //This will work because Waveform is a value type (struct)
            List<Waveform> waveformsClone = new List<Waveform>(Waveforms);
            WaveformScore scoreClone = (WaveformScore)WaveformScore.Clone();

            clone.Waveforms = waveformsClone;
            clone.WaveformScore = scoreClone;
            
            return clone;
        }

}

    // Used to sort the sensors based on the distance to flash
    public class ShortPortionDistance
    {
        public double DistanceToFlash { get; set; }

        public ShortPortion ShortPortion { get; set; }
    }

    public class ShortPortionComparerByDistance : IComparer<ShortPortionDistance>
    {
        public int Compare(ShortPortionDistance x, ShortPortionDistance y)
        {
            return x.DistanceToFlash.CompareTo(y.DistanceToFlash);
        }
    }
}
