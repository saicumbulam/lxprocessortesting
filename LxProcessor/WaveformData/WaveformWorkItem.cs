﻿using System.Collections.Generic;
using LxCommon.Models;

namespace LxProcessor.WaveformData
{
    public class WaveformWorkItem
    {
        public WaveformWorkItem() { }

        public ICollection<StationWaveforms> StationWaveforms { get; set; }
        public DateTimeUtcNano StartTime { get; set; }

        public void CleanForReuse()
        {
            StationWaveforms = null;
            StartTime = DateTimeUtcNano.MinValue;
        }

    }
}
