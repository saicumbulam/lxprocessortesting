﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;

using Aws.Core.Utilities;
using Aws.Core.Utilities.Config;
using LxCommon;
using LxCommon.DataStructures;
using LxCommon.Models;
using LxCommon.Utilities;
using LxProcessor.Common;
using LxProcessor.Locator;
using LxProcessor.RawData;

namespace LxProcessor.WaveformData
{
    public class WaveformProcessor : ParallelTaskProcessor<WaveformWorkItem>
    {
        private bool _isPlayback;
        private RawDataQueueProcessor _rawDataQueueProcessor;
        private OffsetProcessor _offsetProcessor;
        private ConcurrentDictionary<string, StationData> _stationData;
        private ConcurrentDictionary<string, DateTime> _activeSensors;

        private ConcurrentDictionary<int, ConcurrentDictionary<string, StationWaveforms>> _waveformsByTime;
        private SyncronizedDateTime _oldestWaveform;
        private SyncronizedDateTime _newestWaveform;
        private SyncronizedDateTime _lastWaveformProcessed;

        private int _maxDelaySeconds;
        private int _minDelaySeconds;


        private static readonly DateTime ReferenceTime;

        private const long TimeslotWaveformOverlappingTimeInNanoseconds = LtgConstants.TimeslotWaveformOverlappingTimeInMicrosecond * DateTimeUtcNano.NanosecondsPerMicrosecond;

        private readonly ConcurrentSimplePool<StationWaveforms> _stationWaveformPool;
        private readonly ConcurrentSimplePool<WaveformWorkItem> _workItemPool;
        private readonly ConcurrentSimplePool<ShortPortion> _shortPortionPool;
        private readonly ConcurrentSimplePool<StringBuilder> _sbPool;

        private readonly int _totalProcessors;
        private readonly int _processorNum;

        static WaveformProcessor()
        {
            ReferenceTime = DateTime.UtcNow;
        }

        public WaveformProcessor(bool isPlayback, OffsetProcessor offsetProcessor, ConcurrentDictionary<string, StationData> stationData, int minDelaySeconds, int maxDelaySeconds,
            int stationWaveformPoolSize, int waveformWorkItemPoolSize, int shortPortionPoolSize, ManualResetEventSlim stopEvent, int maxWaveformTasks, ConcurrentDictionary<string, DateTime> activeSensors,
            int totalProcessors, int processorNum)
            : base(stopEvent, maxWaveformTasks)
        {
            _isPlayback = isPlayback;

            _offsetProcessor = offsetProcessor;
            _stationData = stationData;
            _activeSensors = activeSensors;

            _minDelaySeconds = minDelaySeconds;
            _maxDelaySeconds = maxDelaySeconds;

            _waveformsByTime = new ConcurrentDictionary<int, ConcurrentDictionary<string, StationWaveforms>>();
            _oldestWaveform = new SyncronizedDateTime(DateTime.MaxValue);
            _newestWaveform = new SyncronizedDateTime(DateTime.MinValue);
            _lastWaveformProcessed = new SyncronizedDateTime(DateTime.MinValue);

            _stationWaveformPool = new ConcurrentSimplePool<StationWaveforms>(stationWaveformPoolSize,
                () => { return new StationWaveforms(); },
                sw => sw.CleanForReuse(),
                "_stationWaveformPool");

            _workItemPool = new ConcurrentSimplePool<WaveformWorkItem>(waveformWorkItemPoolSize,
                () => { return new WaveformWorkItem(); },
                wwi => wwi.CleanForReuse(),
                "_workItemPool");

            _shortPortionPool = new ConcurrentSimplePool<ShortPortion>(shortPortionPoolSize,
                () => { return new ShortPortion(); },
                sp => sp.CleanForReuse(),
                "_shortPortionPool");


            _totalProcessors = totalProcessors;
            _processorNum = processorNum;
        }

        public RawDataQueueProcessor RawDataQueueProcessor
        {
            set { _rawDataQueueProcessor = value; }
        }

        public DateTime LastWaveformProcessed
        {
            get { return _lastWaveformProcessed.DateTimeValue; }
        }

        protected override bool TryGetWorkItem(out WaveformWorkItem workItem)
        {
            bool hasItems = false;
            workItem = null;
            DateTime start, end;
            DateTime lastWaveformConverted = LtgTimeUtils.GetUtcDateTimeFrom1970Seconds(_rawDataQueueProcessor.LastWaveformConvertedTime);
            if (FindValidStartAndEndTimes(_isPlayback, _oldestWaveform, _lastWaveformProcessed, _minDelaySeconds, _maxDelaySeconds,
                lastWaveformConverted, out start, out end))
            {
                if (_totalProcessors <= 1 || LtgTimeUtils.GetSecondsSince1970FromDateTime(start) % _totalProcessors == _processorNum)
                {
                    DateTimeUtcNano startNano = new DateTimeUtcNano(start, 0);
                    int key = CalculateKey(ReferenceTime, startNano);
                    ConcurrentDictionary<string, StationWaveforms> sws;
                    if (_waveformsByTime.TryRemove(key, out sws))
                    {
                        hasItems = true;

                        workItem = _workItemPool.Take();
                        workItem.StartTime = startNano;
                        workItem.StationWaveforms = sws.Values;
                    }
                }
            }

            return hasItems;
        }

        protected override void DoWork(WaveformWorkItem workItem)
        {
            //ICollection<StationWaveforms> waveformsByStations = workItem.Item1;
            //DateTimeUtcNano startNano = workItem.Item2;
            Stopwatch sw = new Stopwatch();
            sw.Start();

            List<ShortPortion> shortPortions = ShortPortion.FindQualifiedShortPortions(workItem.StationWaveforms, _shortPortionPool, _stationWaveformPool);

            if (shortPortions != null && shortPortions.Count > 0)
            {
                // Split shortPortions to groups based on TOA gap
                shortPortions.Sort();

                // Active sensors from the second
                // Dictionary<string, int> activeSensors = CommonPortionFinder.FindActiveSensors(shortPortions);
                DateTime toolOldTime = shortPortions[0].StartTime.BaseTime.AddHours(-5);
                CommonPortionFinder.UpdateActiveSensors(_activeSensors, shortPortions, toolOldTime);
                //find the common portion based on Toa
                List<CommonPortion> commonPortions = CommonPortionFinder.GetCommonPortionsBasedOnToa(shortPortions);

                if (commonPortions != null && commonPortions.Count > 0)
                {
                    commonPortions = CommonPortionFinder.RefineCommonPortionsBySplittingTooLongCommonPortion(commonPortions, LtgConstants.MaxCommonPortionLengthInMicrosecond);
                    foreach (CommonPortion commonPortion in commonPortions)
                    {
                        List<CommonPortion> subCommonPortions = CommonPortionFinder.GroupCommonPortionsBasedOnDistance(commonPortion, _stationData);

                        // Split commonPortions based on TOA gap again
                        subCommonPortions = CommonPortionFinder.SplitCommonPortionsBasedOnToa(subCommonPortions);

                        subCommonPortions = CommonPortionFinder.RefineCommonPortionsByLimitingNumberOfSensors(subCommonPortions);

                        if (subCommonPortions != null)
                        {
                            foreach (CommonPortion subCommonPortion in subCommonPortions)
                            {
                                Offsets offsets = OffsetFinder.GetOffsetsFromCommonPortion(subCommonPortion, _stationData, false, true);
                                if (offsets != null && offsets.OffsetList != null && offsets.OffsetList.Count > 0)
                                {
                                    offsets.ActiveSensors = _activeSensors;
                                    _offsetProcessor.Add(offsets);
                                }
                            }
                        }
                    }
                }

                foreach (ShortPortion shortPortion in shortPortions)
                {
                    _shortPortionPool.Return(shortPortion);
                }
            }

            //_lastWaveformProcessed.ReplaceIfLater(startNano.BaseTime);
            int key = CalculateKey(ReferenceTime, workItem.StartTime);
            IList<ConcurrentDictionary<string, StationWaveforms>> removed = _waveformsByTime.GetRemoveWhere((kvp) =>
            {
                return kvp.Key <= key;
            });

            if (removed.Count > 0)
            {
                foreach (ConcurrentDictionary<string, StationWaveforms> item in removed)
                {
                    foreach (StationWaveforms sws in item.Values)
                    {
                        _stationWaveformPool.Return(sws);
                    }
                }
            }

            DateTime end = workItem.StartTime.BaseTime.AddSeconds(LtgConstants.TimeSlotSizeOfEachThreadInSecond);
            _oldestWaveform.Replace(end);
            sw.Stop();
            EventManager.LogInfo(999, string.Format("{0} - {1} waveform processing took {2} milliseconds", workItem.StartTime.BaseTime, end, sw.ElapsedMilliseconds));
            _workItemPool.Return(workItem);
        }


        private bool FindValidStartAndEndTimes(bool isPlayback, SyncronizedDateTime oldestWaveform, SyncronizedDateTime lastWaveformProcessed, int minDelaySeconds,
            int maxDelaySeconds, DateTime lastWaveformConvertedTime, out DateTime start, out DateTime end)
        {
            bool isValid = false;

            if (isPlayback)
            {
                isValid = GetStartEndTimeForPlayback(oldestWaveform, lastWaveformProcessed, minDelaySeconds, lastWaveformConvertedTime, out start, out end);
            }
            else
            {
                isValid = GetStartEndTime(oldestWaveform, lastWaveformProcessed, lastWaveformConvertedTime, minDelaySeconds, maxDelaySeconds, out start, out end);
            }


            return isValid;
        }

        private bool GetStartEndTime(SyncronizedDateTime oldestWaveform, SyncronizedDateTime lastWaveformProcessed, DateTime lastWaveformConvertedTime,
            int minDelaySeconds, int maxDelaySeconds, out DateTime start, out DateTime end)
        {
            bool retVal = false;
            start = DateTime.MaxValue;
            end = DateTime.MinValue;

            DateTime lastProcessed = lastWaveformProcessed.DateTimeValue;

            if (oldestWaveform.DateTimeValue != DateTime.MaxValue)
            {
                DateTime oldestWaveFormTime;
                if (lastProcessed == DateTime.MinValue)
                    oldestWaveFormTime = oldestWaveform.DateTimeValue;
                else
                    oldestWaveFormTime = lastProcessed;

                if (oldestWaveFormTime != DateTime.MinValue)
                {
                    if ((lastWaveformConvertedTime - lastProcessed).TotalSeconds > minDelaySeconds || (DateTime.UtcNow - lastProcessed).TotalSeconds > maxDelaySeconds)
                    {
                        start = oldestWaveFormTime;
                        end = start.AddSeconds(LtgConstants.TimeSlotSizeOfEachThreadInSecond);

                        if ((DateTime.UtcNow - lastProcessed).TotalSeconds <= maxDelaySeconds
                            && (end > lastWaveformConvertedTime || (DateTime.UtcNow - end).TotalSeconds < minDelaySeconds))
                        {
                            EventManager.LogDebug(string.Format("Bad end time: endTime: {0}, lastWaveFormCovertedTime: {1}", end, lastWaveformConvertedTime));
                        }
                        else
                        {
                            lastWaveformProcessed.ReplaceIfLater(end);
                            retVal = true;
                        }
                    }
                }
            }

            return retVal;
        }

        private bool GetStartEndTimeForPlayback(SyncronizedDateTime oldestWaveform, SyncronizedDateTime lastWaveformProcessed,
            int minDelaySeconds, DateTime lastWaveformConvertedTime, out DateTime start, out DateTime end)
        {
            bool retVal = false;
            start = DateTime.MaxValue;
            end = DateTime.MinValue;

            DateTime oldestWaveformTime = lastWaveformProcessed.DateTimeValue == DateTime.MinValue ? oldestWaveform.DateTimeValue : lastWaveformProcessed.DateTimeValue;

            if (oldestWaveformTime != DateTime.MaxValue)
            {
                start = oldestWaveformTime;
                // Skip the time when the date is not continuous in the raw data
                if ((lastWaveformConvertedTime - start).TotalDays >= 1)
                {
                    start = lastWaveformConvertedTime.AddHours(-1);
                }
                end = start.AddSeconds(LtgConstants.TimeSlotSizeOfEachThreadInSecond);

                if (end.AddSeconds(minDelaySeconds) < lastWaveformConvertedTime)
                {
                    lastWaveformProcessed.ReplaceIfLater(end);
                    retVal = true;
                }
            }

            return retVal;
        }

        public void AddWaveforms(WaveformOfSecondSample waveformSample)
        {
            DateTime waveformTime = LtgTimeUtils.GetUtcDateTimeFrom1970Seconds(waveformSample.Time);

            if (waveformTime > _lastWaveformProcessed.DateTimeValue)
            {
                foreach (Waveform waveform in waveformSample.WaveformOfSecondSampleList)
                {
                    int mainKey = CalculateKey(ReferenceTime, waveform.TimeStamp);
                    AddWaveform(mainKey, waveform, waveformSample, _waveformsByTime, _stationWaveformPool);

                    long low = waveform.TimeStamp.Nanoseconds - TimeslotWaveformOverlappingTimeInNanoseconds;
                    DateTimeUtcNano lowTime = new DateTimeUtcNano(low);
                    int lowKey = CalculateKey(ReferenceTime, lowTime);
                    if (lowKey != mainKey)
                    {
                        AddWaveform(lowKey, waveform, waveformSample, _waveformsByTime, _stationWaveformPool);
                    }

                    long high = waveform.TimeStamp.Nanoseconds + TimeslotWaveformOverlappingTimeInNanoseconds;
                    DateTimeUtcNano highTime = new DateTimeUtcNano(high);
                    int highKey = CalculateKey(ReferenceTime, highTime);
                    if (highKey != mainKey)
                    {
                        AddWaveform(highKey, waveform, waveformSample, _waveformsByTime, _stationWaveformPool);
                    }
                }

                _oldestWaveform.ReplaceIfEarlier(waveformTime);
                _newestWaveform.ReplaceIfLater(waveformTime);
            }
        }

        private void AddWaveform(int key, Waveform waveform, WaveformOfSecondSample stationWaveformSample, ConcurrentDictionary<int, ConcurrentDictionary<string, StationWaveforms>> waveformsByTime,
            ConcurrentSimplePool<StationWaveforms> stationWaveformPool)
        {
            ConcurrentDictionary<string, StationWaveforms> secondOfWaveforms;
            StationWaveforms stationWaveforms;

            if (waveformsByTime.TryGetValue(key, out secondOfWaveforms))
            {
                if (secondOfWaveforms.TryGetValue(stationWaveformSample.StationId, out stationWaveforms))
                {
                    stationWaveforms.AddWaveform(waveform);
                }
                else
                {
                    stationWaveforms = stationWaveformPool.Take();
                    stationWaveforms.Initalize(stationWaveformSample.StationId, stationWaveformSample.MajorVersion, stationWaveformSample.MinorVersion, waveform);

                    //collision try to add it again
                    if (!secondOfWaveforms.TryAdd(stationWaveformSample.StationId, stationWaveforms))
                    {
                        stationWaveformPool.Return(stationWaveforms);
                        AddWaveform(key, waveform, stationWaveformSample, waveformsByTime, stationWaveformPool);
                    }
                }
            }
            else
            {
                stationWaveforms = stationWaveformPool.Take();
                stationWaveforms.Initalize(stationWaveformSample.StationId, stationWaveformSample.MajorVersion, stationWaveformSample.MinorVersion, waveform);

                secondOfWaveforms = new ConcurrentDictionary<string, StationWaveforms>();
                secondOfWaveforms.TryAdd(stationWaveformSample.StationId, stationWaveforms);

                //collision try to add it again
                if (!waveformsByTime.TryAdd(key, secondOfWaveforms))
                {
                    stationWaveformPool.Return(stationWaveforms);
                    AddWaveform(key, waveform, stationWaveformSample, waveformsByTime, stationWaveformPool);
                }
            }
        }

        public int GetWaveformCount()
        {
            DateTime oldest, newest;
            return GetWaveformCount(out oldest, out newest);
        }

        public int GetWaveformCount(out DateTime oldest, out DateTime newest)
        {
            oldest = ReferenceTime;
            newest = ReferenceTime;

            int old = int.MaxValue, neww = int.MinValue;
            int count = 0;
            foreach (KeyValuePair<int, ConcurrentDictionary<string, StationWaveforms>> kvp in _waveformsByTime)
            {
                if (kvp.Key < old)
                {
                    old = kvp.Key;
                }
                if (kvp.Key > neww)
                {
                    neww = kvp.Key;
                }

                count = count + kvp.Value.Count;
            }

            if (old != int.MaxValue)
            {
                oldest = ConvertKeyToDateTime(old, ReferenceTime);
            }

            if (neww != int.MinValue)
            {
                newest = ConvertKeyToDateTime(neww, ReferenceTime);
            }

            return count;
        }

        public int GetStationWithWaveformCount()
        {
            HashSet<string> uniqueStations = new HashSet<string>();

            foreach (KeyValuePair<int, ConcurrentDictionary<string, StationWaveforms>> kvp in _waveformsByTime)
            {
                foreach (KeyValuePair<string, StationWaveforms> sws in kvp.Value)
                {
                    uniqueStations.Add(sws.Key);
                }
            }

            return uniqueStations.Count;
        }

        private DateTime ConvertKeyToDateTime(int key, DateTime referenceTime)
        {
            return referenceTime.AddSeconds(key);
        }

        private int CalculateKey(DateTime referenceTime, DateTimeUtcNano sampleTime)
        {
            return (int)(sampleTime.BaseTime - referenceTime).TotalSeconds;
        }
    }
}
