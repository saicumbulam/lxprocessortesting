﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Aws.Core.Utilities;

using LxCommon;
using LxCommon.DataStructures;
using LxCommon.Models;
using LxCommon.Utilities;

using LxProcessor.Common;
using LxProcessor.Locator;
using LxProcessor.MainController;
using LxProcessor.WaveformData;

namespace LxProcessor.PostLocator
{
    public class PostLocatorProcessor : ParallelTaskProcessor<List<LTGFlashPortion>>
    {
        private ConcurrentDictionary<string, StationData> _stationData;
        private SortedList<TimeRangeNano, PortionBucket> _portionBuckets;
        private ReaderWriterLockSlim _portionBucketsLock;

        private SyncronizedDateTime _lastPortionAdded;
        private SyncronizedDateTime _lastPortionProcessed;
        private SyncronizedDateTime _lastPortionProcessedTimeStamp;

        private FlashDataLogger _flashDataLogger;
        private DataController _dataController;
        private MedianSampler<int> _outputFlashAge;
        private ParallelOptions _parallelOptions;
        private FlashClassifier _flashClassifier;

        private SensorDensity _sensorDensity;

        private const int TimeRangeInSeconds = 1;
        private static readonly int PostLocatorTimeDelayMilliseconds = Config.PostLocatorTimeDelayMilliseconds;
        private static readonly double MinimumFlashGroupDistanceInKm = Config.MinimumFlashGroupDistanceInKm;
        private static readonly int PostLocatorMaxAccumulatedPortions = Config.PostLocatorMaxAccumulatedPortions;

        public PostLocatorProcessor(ConcurrentDictionary<string, StationData> stationData, FlashDataLogger flashDataLogger, DataController dataController,
            MedianSampler<int> outputFlashAge, int degreeOfParallelism, int sensorDensityGridSizeInDegrees, int sensorDensitySecondsToKeep, ManualResetEventSlim stopEvent, int maxActiveWorkItems)
            : base(stopEvent, maxActiveWorkItems)
        {
            _stationData = stationData;

            _flashDataLogger = flashDataLogger;
            _dataController = dataController;
            _outputFlashAge = outputFlashAge;
            _portionBuckets = new SortedList<TimeRangeNano, PortionBucket>();
            _portionBucketsLock = new ReaderWriterLockSlim();
            _lastPortionAdded = new SyncronizedDateTime(DateTime.MinValue);
            _lastPortionProcessed = new SyncronizedDateTime(DateTime.MinValue);
            _lastPortionProcessedTimeStamp = new SyncronizedDateTime(DateTime.UtcNow);
            _parallelOptions = new ParallelOptions { MaxDegreeOfParallelism = degreeOfParallelism };
            _flashClassifier = new FlashClassifier();
            _sensorDensity = new SensorDensity(sensorDensityGridSizeInDegrees, sensorDensitySecondsToKeep);

        }

        protected override bool TryGetWorkItem(out List<LTGFlashPortion> flashPortions)
        {
            bool hasWorkItems = false;
            flashPortions = null;

            TimeSpan diffRealTime = DateTime.UtcNow - _lastPortionProcessedTimeStamp.DateTimeValue;
            _portionBucketsLock.EnterReadLock();
            try
            {
                if (_portionBuckets.Count > 0)
                {
                    TimeRangeNano key = default(TimeRangeNano);
                    TimeSpan diffProcessed = _portionBuckets.Keys[0].End.BaseTime - _lastPortionProcessed.DateTimeValue;
                    //if the time thresholds have passed grab the bucket;
                    if (diffProcessed.TotalMilliseconds < 0 || diffProcessed.TotalMilliseconds >= PostLocatorTimeDelayMilliseconds
                        || diffRealTime.TotalMilliseconds > PostLocatorTimeDelayMilliseconds)
                    {
                        key = _portionBuckets.Keys[0];
                    }
                    else
                    {
                        int i = 0;
                        while (i < _portionBuckets.Count)
                        {
                            if (_portionBuckets.Values[i].Portions.Count > PostLocatorMaxAccumulatedPortions)
                            {
                                key = _portionBuckets.Keys[i];
                                break;
                            }
                            i++;
                        }
                    }
                    _portionBucketsLock.ExitReadLock();

                    // found a bucket to process
                    if (key != default(TimeRangeNano))
                    {
                        PortionBucket bucket;
                        _portionBucketsLock.EnterWriteLock();
                        if (_portionBuckets.TryGetValue(key, out bucket))
                        {
                            bucket.MarkAsRemoved();
                            _portionBuckets.Remove(key);
                        }
                        _portionBucketsLock.ExitWriteLock();

                        if (bucket != null)
                        {
                            flashPortions = bucket.Portions;
                            hasWorkItems = true;
                        }
                    }
                }
            }
            finally
            {
                if (_portionBucketsLock.IsReadLockHeld)
                {
                    _portionBucketsLock.ExitReadLock();
                }
                if (_portionBucketsLock.IsWriteLockHeld)
                {
                    _portionBucketsLock.ExitWriteLock();
                }
            }

            return hasWorkItems;
        }

        protected override void DoWork(List<LTGFlashPortion> preFlashPortions)
        {
            List<LTGFlashPortion> mergedPortions = MergePortions(preFlashPortions);

            Parallel.ForEach(mergedPortions, _parallelOptions, (portion) =>
            {
                double sensorDensity = _sensorDensity.GetDensity(portion.Latitude, portion.Longitude, portion.TimeStamp, portion.ActiveSensorTable as ConcurrentDictionary<string, DateTime>, _stationData);

                CommonPortion commonPortion = FlashLocator.GetPostCommonPortionFromPreFlashPortion(portion, _stationData, sensorDensity);
                if (commonPortion != null)
                {
                    Offsets offsets = OffsetFinder.GetOffsetsFromCommonPortion(commonPortion, _stationData, true, false);
                    if (offsets != null && offsets.OffsetList.Count > 0)
                    {
                        LTGFlashPortion postPortion = FlashLocator.PostFindFlashPortionFromOffsets(offsets, _stationData, portion, PortionCount, sensorDensity);

                        if (postPortion != null)
                        {
                            FlashLocator.CalculateFlashPortionEllipse(postPortion, _stationData);
                            if (postPortion.ErrorEllipse.MajorAxis < LtgConstants.MaxAllowedErrorEllipseMajorAxis)
                            {
                                postPortion.Version = Config.AssemblyVersion;
                                CreateAndSendfFlash(postPortion, _stationData, _flashDataLogger, _dataController,
                                    _outputFlashAge, _flashClassifier);
                                _lastPortionProcessed.ReplaceIfLater(postPortion.TimeStamp.BaseTime);
                                _lastPortionProcessedTimeStamp.ReplaceIfLater(DateTime.UtcNow);
                            }
                        }
                    }
                }
            });
        }
        // postlocator process
        private List<LTGFlashPortion> MergePortions(List<LTGFlashPortion> preFlashPortions)
        {
            List<LTGFlashPortion> merged = null;
            if (preFlashPortions != null && preFlashPortions.Count > 0)
            {
                if (preFlashPortions.Count > 1)
                {
                    merged = new List<LTGFlashPortion>();
                    while (preFlashPortions.Count > 0)
                    {
                        LTGFlashPortion portion = preFlashPortions[0];

                        bool wasMerged = false;
                        long lowerValue = portion.TimeStamp.Microseconds - ((long)(LtgConstants.MinGapOfFlashPortionInMicroseconds / 3));
                        long upperValue = portion.TimeStamp.Microseconds + ((long)(LtgConstants.MinGapOfFlashPortionInMicroseconds / 3));
                        foreach (LTGFlashPortion mergedPortion in merged)
                        {
                            if (mergedPortion.TimeStamp.Microseconds >= lowerValue && mergedPortion.TimeStamp.Microseconds <= upperValue)
                            {
                                if (portion.LocationCompareTo(mergedPortion) < MinimumFlashGroupDistanceInKm * 3)
                                {
                                    CommonPortion mergedCommon = (CommonPortion)mergedPortion.CommonPortionRef;
                                    CommonPortion common = (CommonPortion)portion.CommonPortionRef;
                                    //need to check these are not the same common portion before merging them
                                    if (!mergedCommon.MergedPortions.Contains(common.Id))
                                    {
                                        mergedCommon.AddShortPortions(common.ShortPortions);
                                        wasMerged = true;
                                        mergedCommon.MergedPortions.Add(common.Id);
                                        break;
                                    }
                                    else
                                    {
                                        int i = 0;
                                    }
                                }
                            }
                        }

                        if (!wasMerged)
                        {
                            merged.Add(portion);
                        }

                        preFlashPortions.RemoveAt(0);

                    }
                }
                else
                {
                    merged = preFlashPortions;
                }
            }

            return merged;
        }

        private void CreateAndSendfFlash(LTGFlashPortion portion, ConcurrentDictionary<string, StationData> stationData, FlashDataLogger flashDataLogger, DataController dataController,
            MedianSampler<int> outputFlashAge, FlashClassifier flashClassifier)
        {
            //FlashLocator.CalculateFlashPortionEllipse(portion, stationData);
            //sort the sensors based on the distance and remove the extra range
            UpdateClosestSensorsForClassification(portion, LtgConstants.MaxNumberOfNearestSensorsForClassification + 3);
            //update the amplitude of the flash portions
            AmplitudeDecider.UpdateAmplitude(portion, stationData);
            flashClassifier.ClassifyFlashPortion(portion);

            LTGFlash flash = new LTGFlash();
            flash.AddFlashPortion(portion);
            flash.SetFlashData();

            int flashAge = (int)(DateTime.UtcNow - portion.TimeStamp.BaseTime).TotalSeconds;
            outputFlashAge.AddSample(flashAge);

            if (_flashDataLogger != null)
            {
                flashDataLogger.WritePortion(portion, null, portion.TimeStamp.BaseTime);
            }

            dataController.UpdateFlashData(new List<LTGFlash> { flash });
        }


        private static void UpdateClosestSensorsForClassification(LTGFlashPortion flashPortion, int numberOfSensors)
        {
            try
            {
                if (flashPortion != null)
                {
                    List<Offset> offsetList = flashPortion.OffsetList;

                    if (offsetList != null && offsetList.Count != 0)
                    {
                        flashPortion.ClosestSensorOffsetForClassification = new List<Offset>();

                        foreach (Offset offset in offsetList)
                        {
                            flashPortion.ClosestSensorOffsetForClassification.Add(offset);
                        }

                        // Sort it by distance 
                        flashPortion.ClosestSensorOffsetForClassification.Sort(new OffsetCompareDistance());

                        if (flashPortion.ClosestSensorOffsetForClassification.Count > numberOfSensors)
                        {
                            flashPortion.ClosestSensorOffsetForClassification.RemoveRange(numberOfSensors,
                                flashPortion.ClosestSensorOffsetForClassification.Count - numberOfSensors);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventId.Grouper.FlashGrouper.ClosestSensors, "failure in UpdateClosestSensorsForClassification", ex);
            }
        }

        public void AddPortion(LTGFlashPortion portion)
        {
            bool wasAdded = false;
            try
            {
                _portionBucketsLock.EnterReadLock();
                foreach (TimeRangeNano key in _portionBuckets.Keys)
                {
                    if (key.Contains(portion.TimeStamp))
                    {
                        wasAdded = _portionBuckets[key].TryAddPortion(portion);
                        if (wasAdded) break;
                    }
                }
            }
            finally
            {
                _portionBucketsLock.ExitReadLock();
            }

            if (!wasAdded)
            {
                PortionBucket bucket = PortionBucket.CreateBucket(portion);
                _portionBucketsLock.EnterWriteLock();
                try
                {
                    if (!_portionBuckets.ContainsKey(bucket.TimeRange))
                    {
                        _portionBuckets.Add(bucket.TimeRange, bucket);
                        wasAdded = true;
                    }
                    else
                    {
                        _portionBucketsLock.ExitWriteLock();
                        AddPortion(portion);
                    }
                }
                finally
                {
                    if (_portionBucketsLock.IsWriteLockHeld)
                    {
                        _portionBucketsLock.ExitWriteLock();
                    }
                }
            }

            if (wasAdded)
            {
                _lastPortionAdded.ReplaceIfLater(portion.TimeStamp.BaseTime);
            }
        }

        public int PortionCount
        {
            get
            {
                int count = 0;
                int i = 0;
                while (i < _portionBuckets.Values.Count)
                {
                    try
                    {
                        if (_portionBuckets.Values[i].Portions != null)
                        {
                            count += _portionBuckets.Values[i].Portions.Count;
                        }
                    }
                    catch { }

                    i++;
                }
                return count;
            }
        }

        public int BucketCount
        {
            get
            {
                return _portionBuckets.Count;
            }
        }

        public DateTime LastPortionAdded
        {
            get { return _lastPortionAdded.DateTimeValue; }
        }

        public DateTime LastPortionProcessed
        {
            get { return _lastPortionProcessed.DateTimeValue; }
        }

        private class PortionBucket
        {
            private List<LTGFlashPortion> _portions;
            private object _bucketLock;
            private bool _hasBeenRemoved;

            private PortionBucket(TimeRangeNano timeRange)
            {
                TimeRange = timeRange;
                _portions = new List<LTGFlashPortion>();
                _bucketLock = new object();
                _hasBeenRemoved = false;
            }

            public bool TryAddPortion(LTGFlashPortion portion)
            {
                bool wasAdded = false;
                lock (_bucketLock)
                {
                    if (!_hasBeenRemoved)
                    {
                        _portions.Add(portion);
                        wasAdded = true;
                    }
                }
                return wasAdded;
            }

            public void MarkAsRemoved()
            {
                lock (_bucketLock)
                {
                    _hasBeenRemoved = true;
                }
            }

            public TimeRangeNano TimeRange { get; private set; }
            public List<LTGFlashPortion> Portions
            {
                get { return _portions; }
            }

            public static PortionBucket CreateBucket(LTGFlashPortion portion)
            {
                TimeRangeNano key = GetTimeRange(portion.TimeStamp.BaseTime);
                PortionBucket bucket = new PortionBucket(key);
                bucket.TryAddPortion(portion);
                return bucket;
            }

            private static TimeRangeNano GetTimeRange(DateTime dateTime)
            {
                DateTime s = new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute, dateTime.Second);
                DateTimeUtcNano start = new DateTimeUtcNano(s, 0);
                DateTime e = s.AddSeconds(TimeRangeInSeconds);
                DateTimeUtcNano end = new DateTimeUtcNano(e, 0);
                return new TimeRangeNano(start, end);

            }
        }
    }
}
