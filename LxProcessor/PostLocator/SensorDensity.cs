﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

using LxCommon;
using LxCommon.Models;
using LxProcessor.Common;

namespace LxProcessor.PostLocator
{
    public class SensorDensity
    {
        private readonly int _gridSizeInDegrees;
        private readonly int _gridX;
        private readonly int _gridY;
        private readonly int _secondsToKeep;
        private readonly DateTime _referenceTime;


        private readonly ConcurrentDictionary<int, double[,]> _densitiesBySecond;

        public SensorDensity(int gridSizeInDegrees, int secondsToKeep)
        {
            _gridSizeInDegrees = gridSizeInDegrees;
            _gridX = 360 / gridSizeInDegrees;
            _gridY = 180 / gridSizeInDegrees;
            _secondsToKeep = secondsToKeep;
            _densitiesBySecond = new ConcurrentDictionary<int, double[,]>();
            _referenceTime = DateTime.UtcNow;

        }

        public double GetDensity(double latitude, double longitude, DateTimeUtcNano portionTime, ConcurrentDictionary<string, DateTime> activeSensors, ConcurrentDictionary<string, StationData> stationData)
        {
            double density = 0.0;
            int key = CalculateKey(_referenceTime, portionTime);

            double[,] densities;
            if (!_densitiesBySecond.TryGetValue(key, out densities))
            {
                densities = CreateDensityForSecond(activeSensors, stationData, _gridX, _gridY, _gridSizeInDegrees);
                _densitiesBySecond.TryAdd(key, densities);
            }

            int x, y;
            if (GetGridNum(latitude, longitude, _gridX, _gridY, _gridSizeInDegrees, out x, out y))
            {
                density = densities[x, y];
            }

            _densitiesBySecond.RemoveWhere((kvp) =>
            {
                return kvp.Key <= (key - _secondsToKeep);
            });

            return density;
        }

        private double[,] CreateDensityForSecond(ConcurrentDictionary<string, DateTime> activeSensors, ConcurrentDictionary<string, StationData> stationData, int gridX, int gridY, int gridSizeInDegrees)
        {
            double[,] densities = new double[gridX, gridY];
            foreach (string sensorId in activeSensors.Keys)
            {
                StationData station;
                if (stationData.TryGetValue(sensorId, out station))
                {
                    int x, y;
                    if (GetGridNum(station.Latitude, station.Longitude, gridX, gridY, gridSizeInDegrees, out x, out y))
                    {
                        densities[x, y]++;
                    }

                }
            }

            Normalize(densities, gridX, gridY, gridSizeInDegrees);

            return densities;
        }

        private bool GetGridNum(double latitude, double longitude, int gridX, int gridY, int gridSizeInDegrees, out int x, out int y)
        {
            x = (int)(longitude + 180) / gridSizeInDegrees;
            y = (int)(latitude + 90) / gridSizeInDegrees;
            return (x >= 0 && y >= 0 && x < gridX && y < gridY);
        }

        private void Normalize(double[,] densities, int gridX, int gridY, int gridSizeInDegrees)
        {
            for (int i = 0; i < gridX; i++)
            {
                for (int j = 0; j < gridY; j++)
                {
                    if (densities[i, j] > 0)
                    {
                        double gridArea = GetGridArea(j, gridSizeInDegrees);
                        densities[i, j] /= gridArea;
                    }
                }
            }
        }

        private int CalculateKey(DateTime referenceTime, DateTimeUtcNano sampleTime)
        {
            return (int)(sampleTime.BaseTime - referenceTime).TotalSeconds;
        }

        private double GetGridArea(int y, int gridSizeInDegrees)
        {
            double lat = y * gridSizeInDegrees - 90;
            //double lon = x * gridSizeInDegrees - 180;
            double height = 111 * gridSizeInDegrees; // 111km/degree
            double width = Math.Cos(lat * Math.PI / 180) * LtgConstants.EarthRadiusEquatorialInMeters * Math.PI * gridSizeInDegrees / 180000;
            return (height * width / 10000);
        }

    }
}
