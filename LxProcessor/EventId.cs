﻿namespace LxProcessor
{
    public static class EventId
    {
       
        public static class Config
        {
            public static class AppConfig
            {
                public const ushort ParsingForwardServer = 700;
                public static ushort ParsingRegion = 701;
                public static ushort UsingRegion = 702;
            }
        }

        public static class Utilities
        {
            public static class Export
            {
                public const ushort CreatingTextWriter = 800;
                public const ushort ExportingFlashList = 801;
                public const ushort ExportingFlashPortions = 802;
                public const ushort DataHourPath = 803;
                public const ushort DataDayPath = 804;
                public const ushort RawData = 805;

            }

            public static class Matrix
            {
                public const ushort LinearEquation = 805;
            }
        }

        public static class Grouper
        {
            public static class FlashGrouper
            {
                public const ushort ClosestSensors = 900;
                public const ushort Export = 901;
            }
        }

        public static class Locator
        {
            public static class AmplitudeDecider
            {
                public const ushort UpdateAmplitude = 1000;
            }

            public static class FlashClassifier
            {
                public const ushort ClassifyFlash = 1100;
            }

            public static class FlashLocator
            {
                public const ushort GetFlashTime = 1200;
                public const ushort CheckAndSortStations = 1201;
                public const ushort GetObsLocation = 1202;
                public const ushort ProcPreprocessTimeOut = 1203;
            }
        }

        public static class MainController
        {
            public static class DataController
            {
                public const ushort UpdatingFlashes = 1300;
                public const ushort UpdatingStation = 1301;
            }

            public static class ProcessController
            {
                public const ushort CreatingMonitorPage = 1400;
            }

            public static class MonitoringHandler
            {
                public const ushort LoadMonitorFile = 1450;
                public static ushort StartOpsHttp = 1451;
                public static ushort BuildMonitorHtml = 1452;
                public static ushort StopOpsHttp = 1453;
            }
        }

        public static class OffsetData
        {
            public static class OffsetFinder
            {
                public const ushort BinTooLarge = 1500;
                public const ushort CrossCorrelation = 1501;
                public const ushort CrossCorrelationHf = 1502;
                public const ushort CrossCorrelationLf = 1503;

            }
        }

        public static class SampleData
        {
            public static class WaveformOfSecondSample
            {
                public const ushort Export = 1600;
            }
        }

        public static class PostLocator
        {
            public const ushort PostProcessTimeout = 1700;
        }


        public static class WaveformData
        {
            public static class WaveformProcessor
            {
                public const ushort TimeStamp = 1800;
            }
        }
    }
}
