﻿using System;
using System.Globalization;
using System.IO;
using System.Text;
using LxCommon.Utilities;
using LxProcessor.Locator;
using Newtonsoft.Json.Linq;

using Aws.Core.Data.Common.Operations;
using Aws.Core.Utilities;

using LxCommon;
using LxCommon.TcpCommunication;

using LxProcessor.Common;
using LxProcessor.RawData;
using LxProcessor.PostLocator;
using LxProcessor.WaveformData;

namespace LxProcessor.MainController
{
    public class MonitoringHandler
    {
        private string _monitorHmtl;
        private string _systemType;
        private StationDataTable _stationDataTable;
        private PostLocatorProcessor _postLocatorProcessor;
        private WaveformProcessor _waveformProcessor;
        private RawDataQueueProcessor _rawDataQueueProcessor;
        private Listener _receiverListener;
        private OffsetProcessor _offsetProcessor;
        private MedianSampler<int> _outputFlashAge;

        public MonitoringHandler(string systemType, MedianSampler<int> outputFlashAge)
        {
            _systemType = systemType;
            _outputFlashAge = outputFlashAge;

            try
            {
                _monitorHmtl = File.ReadAllText(AppUtils.AppLocation + @"\Monitor.html", Encoding.UTF8);
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventId.MainController.MonitoringHandler.LoadMonitorFile, "Unable to load monitor html file", ex);
                _monitorHmtl = "Unavailable";
            }
        }

        public void Start()
        {
            try
            {
                OpsCommandHttpEndpointSvc.Start((cmd) => MonitorStatusCheck(cmd));
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventId.MainController.MonitoringHandler.StartOpsHttp, "Unable to start ecv http endpoint", ex);
            }
        }

        private string MonitorStatusCheck(string cmd)
        {
            if (cmd.ToUpper().Equals("PRTG"))
            {
                return GeneratePrtgStatus();
            }
            else
            {
                return GenerateHtmlStatus(_systemType, _monitorHmtl) ;
            }
        }

        private string GeneratePrtgStatus()
        {
            DateTime utcNow = DateTime.UtcNow;

            JObject j = new JObject();

            double receiverPacketRate = 0, receiverConnectionRate = 0;
            int activeConnections = 0;
            if (_receiverListener != null)
            {
                receiverPacketRate = Math.Round(_receiverListener.GetIncomingRatePerSecond(utcNow), 2, MidpointRounding.AwayFromZero);
                receiverConnectionRate = Math.Round(_receiverListener.GetConnectionRatePerSecond(utcNow), 2, MidpointRounding.AwayFromZero);
                activeConnections = _receiverListener.ActiveConnections;
            }
            j.Add("receiverPacketRate", receiverPacketRate);
            j.Add("receiverConnectionRate", receiverConnectionRate);
            j.Add("activeConnections", activeConnections);

            int stations = 0;
            if (_stationDataTable != null)
            {
                stations = _stationDataTable.Data.Count;
            }
            j.Add("databaseStations", stations);

            int stationsWithWaveforms = 0;
            if (_waveformProcessor != null)
            {
                stationsWithWaveforms = _waveformProcessor.GetStationWithWaveformCount();
            }
            j.Add("stationsWithWaveforms", stationsWithWaveforms);

            int lastWaveformConvertedAge = 0;
            if (_rawDataQueueProcessor != null)
            {
                lastWaveformConvertedAge = (int)(utcNow - TimeSvc.GetTime(_rawDataQueueProcessor.LastWaveformConvertedTime)).TotalSeconds;
            }
            j.Add("lastWaveformConvertedAgeSeconds", lastWaveformConvertedAge);

            int lastWaveformProcessedAge = 0;
            if (_waveformProcessor != null)
            {
                lastWaveformProcessedAge = (int)(utcNow - _waveformProcessor.LastWaveformProcessed).TotalSeconds;
            }
            j.Add("lastWaveformProcessedAgeSeconds", lastWaveformProcessedAge);

            int minFlashAge = 0, maxFlashAge = 0, medianFlashAge = 0;
            if (_outputFlashAge != null)
            {
                Tuple<int, int, int> mmm = _outputFlashAge.GetMinMaxMedian();
                minFlashAge = mmm.Item1;
                maxFlashAge = mmm.Item2;
                medianFlashAge = mmm.Item3;
            }
            j.Add("minOutputFlashAgeSeconds", minFlashAge);
            j.Add("maxOutputFlashAgeSeconds", maxFlashAge);
            j.Add("medianOutputFlashAgeSeconds", medianFlashAge);

            return j.ToString();
        }

        private const string Success = "<span style=\"color: green;\">SUCCESS</span>";
        private const string Failure = "<span style=\"color: red;\">FAIL</span>";
        private const string NA = "-";
        
        
        private string GenerateHtmlStatus(string systemType, string monitorHmtl)
        {
            string html = null;
            string success = Success;
            string failure = Failure;
            bool isTln = (systemType.ToUpper().Equals("TLN"));

            if (!isTln)
            {
                success = NA;
                failure = NA;
            }

            CultureInfo enUs = new CultureInfo("en-US");
            DateTime utcNow = DateTime.UtcNow;
            
            int newestWaveform;

            int statsWithWaveforms = 0;
            int waveformCount = 0;
            DateTime oldest = utcNow, newest = utcNow;
            if (_waveformProcessor != null)
            {
                statsWithWaveforms = _waveformProcessor.GetStationWithWaveformCount();
                waveformCount = _waveformProcessor.GetWaveformCount(out oldest, out newest);
            }

            int minFlashAge = 0, maxFlashAge = 0, medianFlashAge = 0;
            if (_outputFlashAge != null)
            {
                Tuple<int, int, int> mmm = _outputFlashAge.GetMinMaxMedian();
                minFlashAge = mmm.Item1;
                maxFlashAge = mmm.Item2;
                medianFlashAge = mmm.Item3;
            }

            Tuple<double, double, double> offsetMmm = _offsetProcessor.DurationMinMaxMedian;

            int grouperCount = 0;
            DateTime grouperLastFlashTime = DateTime.MinValue;
            DateTime grouperLastGroupTime = DateTime.MinValue;
            int grouperActive = 0;
            int grouperMaxActive = 0;
            
            try
            {
                html = string.Format(monitorHmtl,
                    AppUtils.GetVersion(),
                    utcNow.ToString(enUs),

                    (_receiverListener != null && _receiverListener.GetIncomingRatePerSecond(utcNow) >= Config.MonitorIncomingTcpRateMinimum) ? Success : Failure,
                    (_receiverListener != null) ? _receiverListener.GetIncomingRatePerSecond(utcNow).ToString("###0.00") : "n/a",

                    (_receiverListener != null && _receiverListener.ActiveConnections >= Config.MonitorIncomingActiveConnectionsMinimum) ? success : failure, //4
                    (_receiverListener != null) ? _receiverListener.ActiveConnections : 0,

                    (_receiverListener != null && _receiverListener.GetConnectionRatePerSecond(utcNow) <= Config.MonitorIncomingConnectionRateMaximum) ? success : failure,
                    (_receiverListener != null) ? _receiverListener.GetConnectionRatePerSecond(utcNow).ToString("###0.00") : "n/a",

                    (_stationDataTable.Data.Count >= Config.MonitorDbStationsMinimum) ? success : failure,
                    _stationDataTable.Data.Count,
                    
                    (statsWithWaveforms >= Config.MonitorStationsWithWaveformsMinimum) ? success : failure,
                    statsWithWaveforms,
                    
                    (Math.Abs((utcNow - TimeSvc.GetTime(_rawDataQueueProcessor.LastWaveformConvertedTime)).TotalSeconds) < Config.MonitorConvertedAgeThresholdSeconds) ? success : failure,
                    TimeSvc.GetTime(_rawDataQueueProcessor.LastWaveformConvertedTime).ToString(enUs),

                    (Math.Abs((utcNow - _waveformProcessor.LastWaveformProcessed).TotalSeconds) < Config.MonitorProcessedAgeThresholdSeconds) ? success : failure,
                    _waveformProcessor.LastWaveformProcessed.ToString(enUs),

                    _rawDataQueueProcessor.Count, //14
                    _rawDataQueueProcessor.LastPacketTime.ToString(enUs),
                    _rawDataQueueProcessor.ActiveCommands,
                    _rawDataQueueProcessor.MaxActiveWorkItems,
                    
                    _waveformProcessor.GetWaveformCount(),
                    oldest.ToString(enUs), //19
                    newest.ToString(enUs),
                    _waveformProcessor.ActiveCommands,
                    _waveformProcessor.MaxActiveWorkItems,

                    _offsetProcessor.Count,
                    _offsetProcessor.LastPortionTimeStamp.ToString(enUs), //19
                    offsetMmm.Item1,
                    offsetMmm.Item2,
                    offsetMmm.Item3,
                    _offsetProcessor.ActiveCommands,
                    _offsetProcessor.MaxActiveWorkItems,

                    _postLocatorProcessor.BucketCount,
                    _postLocatorProcessor.PortionCount,
                    _postLocatorProcessor.LastPortionAdded.ToString(enUs), 
                    _postLocatorProcessor.LastPortionProcessed.ToString(enUs),
                    _postLocatorProcessor.ActiveCommands,
                    _postLocatorProcessor.MaxActiveWorkItems,
                    
                    grouperCount,
                    grouperLastFlashTime.ToString(enUs),
                    grouperLastGroupTime.ToString(enUs), //29
                    grouperActive,
                    grouperMaxActive,
                    
                    minFlashAge,
                    maxFlashAge,
                    medianFlashAge); //31
            }
            catch (Exception ex)
            {   
                EventManager.LogWarning(EventId.MainController.ProcessController.CreatingMonitorPage, "Failure generating Monitor page.", ex);
                html = OpsCommandHttpEndpointSvc.FailedResponse + " - Failure generating Monitor page: " + ex.Message;
            }

            return html;
        }

        public void Stop()
        {
            try
            {
                OpsCommandHttpEndpointSvc.Stop();
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventId.MainController.MonitoringHandler.StopOpsHttp, "Unable to stop ecv http endpoint", ex);
            }

        }

        public StationDataTable StationDataTable 
        {
            set { _stationDataTable = value; }
        }

        public PostLocatorProcessor PostLocatorProcessor
        {
            set { _postLocatorProcessor = value; }
        }

        public WaveformProcessor WaveformProcessor
        {
            set { _waveformProcessor = value; }
        }

        public RawDataQueueProcessor RawDataQueueProcessor
        {
            set { _rawDataQueueProcessor = value; }
        }

        public OffsetProcessor OffsetProcessor
        {
            set { _offsetProcessor = value; }
        }

        public Listener ReceiverListener
        {
            set { _receiverListener = value; }
        }
    }
}
