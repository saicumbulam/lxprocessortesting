using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Threading;

using Aws.Core.Utilities;

using LxCommon;
using LxCommon.Monitoring;
using LxCommon.TcpCommunication;
using LxCommon.Utilities;

using LxProcessor.Common;
using LxProcessor.Locator;
using LxProcessor.Playback;
using LxProcessor.PostLocator;
using LxProcessor.RawData;
using LxProcessor.Utilities;
using LxProcessor.WaveformData;

namespace LxProcessor.MainController
{
    public class ProcessController
    {
        private DataController _dataController;

        private Listener _receiverListener;

        private readonly ManualResetEventSlim _stopEvent;

        private Timer _stationUpdateTimer;
        private Timer _statusTimer;
        private Timer _perfWriterTimer;

        private StationDataTable _stationDataTable;
        private RawDataQueueProcessor _rawDataQueueProcessor;

        private WaveformProcessor _waveformProcessor;
        private PostLocatorProcessor _postLocatorProcessor;

        private OffsetProcessor _offsetProcessor;

        private PerfMon _perfMon;
        private PerfCsvWriter _perfCsvWriter;

        private MonitoringHandler _monitoringHandler;
        private MedianSampler<int> _outputFlashAge;
        private GcHelper _gcHelper;

        public ProcessController()
        {
            _stopEvent = new ManualResetEventSlim(false);
            _outputFlashAge = new MedianSampler<int>(Config.FlashAgeMedianSamplerCapacity);
            _monitoringHandler = new MonitoringHandler(Config.SystemType, _outputFlashAge);
            _monitoringHandler.Start();
            _gcHelper = new GcHelper(Config.MaxLargeObjectHeapToCompactBytes, Config.LargeObjectHeapCheckIntervalMinutes, Config.MaxLargeObjectHeapCompactIntervalMinutes);
        }

        public void Start()
        {
            if (Config.PerfMonEnabled)
            {
                _perfMon = new PerfMon();
                _perfMon.Init();
            }

            _dataController = new DataController(this, Config.IsPlayback, Config.TotalProcessors, Config.ProcessorNum);

            _stationDataTable = new StationDataTable();
            _stationDataTable.InitalLoad();
            _monitoringHandler.StationDataTable = _stationDataTable;
            _dataController.StationDataTable = _stationDataTable;

            InitializePipeline();

            InitalizeTimers();

            InitializeCommunication();
        }

        public void Stop()
        {
            _stopEvent.Set();

            CleanupTimers();

            CleanupCommunication();

            _monitoringHandler.Stop();

            EventManager.LogInfo(TaskMonitor.TidServiceStopped, "Lightning Processor Service Stopped successfully.");
        }

        private void InitializePipeline()
        {
            FlashDataLogger fdl = null;
            if (Config.ExportFlash)
            {
                fdl = new FlashDataLogger(Config.ExportFlashFolder, Config.ExportFlashFileName, true);
            }

            _postLocatorProcessor = new PostLocatorProcessor(_stationDataTable.Data, fdl, _dataController, _outputFlashAge, Config.PostLocatorDegreeOfParallelism, Config.SensorDensityGridSizeInDegrees,
                Config.SensorDensitySecondsToKeep, _stopEvent, Config.MaxPostLocatorTasks);
            _monitoringHandler.PostLocatorProcessor = _postLocatorProcessor;

            _offsetProcessor = new OffsetProcessor(_postLocatorProcessor, _stationDataTable.Data, Config.OffsetProcessorSampleSize, _stopEvent, Config.OffsetProcessorMaxTasks);
            _monitoringHandler.OffsetProcessor = _offsetProcessor;

            _waveformProcessor = new WaveformProcessor(Config.IsPlayback, _offsetProcessor, _stationDataTable.Data, Config.ProcessWaveformTimeDelayInSeconds,
                Config.MaximumWaveformProcessingDelaySeconds, Config.StationWaveformPoolSize, Config.WaveformWorkItemPoolSize, Config.ShortPortionPoolSize,
                _stopEvent, Config.MaxWaveformTasks, _dataController.ActiveSensors, Config.TotalProcessors, Config.ProcessorNum);
            _monitoringHandler.WaveformProcessor = _waveformProcessor;

            _rawDataQueueProcessor = new RawDataQueueProcessor(_stationDataTable.Data, _waveformProcessor, _perfMon, Config.RawDataWaveformOfSecondSamplePoolSize, Config.RawDataPulseDataPoolSize,
                _stopEvent, Config.MaxRawDataQueueTasks);
            _waveformProcessor.RawDataQueueProcessor = _rawDataQueueProcessor;
            _monitoringHandler.RawDataQueueProcessor = _rawDataQueueProcessor;

            _waveformProcessor.Start();
            _postLocatorProcessor.Start();
        }

        private void InitalizeTimers()
        {
            _stationUpdateTimer = new Timer(_stationDataTable.Update, null, TimeSpan.FromMinutes(Config.StationDatabaseUpdateIntervalMinutes),
                TimeSpan.FromMinutes(Config.StationDatabaseUpdateIntervalMinutes));

            _statusTimer = new Timer(OnCheckStatus, null, new TimeSpan(0), TimeSpan.FromSeconds(10));

            _gcHelper.Start();

            if (Config.PerfCsvWriterEnabled)
            {
                _perfCsvWriter = InitPerfWriter();
                _perfWriterTimer = new Timer(OnPerfWrite, null, new TimeSpan(0), TimeSpan.FromMinutes(Config.PerfCsvWriterIntervalMinutes));
            }
        }

        private void CleanupTimers()
        {
            if (_stationUpdateTimer != null)
            {
                _stationUpdateTimer.Dispose();
            }

            if (_statusTimer != null)
            {
                _statusTimer.Dispose();
            }

            _gcHelper.Stop();

            if (_perfWriterTimer != null)
            {
                _perfWriterTimer.Dispose();
            }

        }

        private void InitializeCommunication()
        {

            if (Config.IsPlaybackFiles)
            {
                PlaybackFiles pf = new PlaybackFiles(_dataController, _rawDataQueueProcessor, _waveformProcessor, _offsetProcessor, _postLocatorProcessor, _stopEvent, this);
                //ThreadPool.QueueUserWorkItem(pf.Start);
                Thread thread = new Thread(pf.Start);
                thread.Start();
            }
            else
            {
                //Listener receiverCleaner = _receiverListener = new Listener("Receiver listener", IPAddress.Any, Config.TcpListenPort, Config.ReceiverListenerMaxPendingConnections,
                //    DecodeRawPacket, Config.ReceiverListenerRateSampleSize, Config.ReceiverListenerConnectionRateSampleSize, Config.ListenerIdleCheckIntervalSeconds,
                //    Config.MonitorIncomingActiveConnectionsMinimum);

                //receiverCleaner.Open();

                //Stopwatch sw = new Stopwatch();
                //sw.Start();
                //while (sw.ElapsedMilliseconds < 10000)
                //{
                //    Thread.Sleep(500);
                //}
                //sw.Stop();
                //receiverCleaner.Close();

                _receiverListener = new Listener("Receiver listener", IPAddress.Any, Config.TcpListenPort, Config.ReceiverListenerMaxPendingConnections,
                    _dataController.DecodeRawPacket, Config.ReceiverListenerRateSampleSize, Config.ReceiverListenerConnectionRateSampleSize, Config.ListenerIdleCheckIntervalSeconds,
                    Config.MonitorIncomingActiveConnectionsMinimum);

                _receiverListener.Open();

                _monitoringHandler.ReceiverListener = _receiverListener;
            }
        }

        public DecodeResult DecodeRawPacket(List<byte> packetBuffer, string remoteIp)
        {
            PacketHeaderInfo phi;
            DecodeResult result = TcpPacketUtils.TryTcpDecode(packetBuffer, remoteIp, out phi);
            return result;
        }

        private void CleanupCommunication()
        {
            if (_receiverListener != null)
            {
                _receiverListener.Close();
            }

            if (_dataController != null)
            {
                _dataController.CloseConnections();
            }
        }

        private PerfCsvWriter InitPerfWriter()
        {
            List<PerformanceItem> items = new List<PerformanceItem>
            {
                new PerformanceItem
                {
                    Counter = new PerformanceCounter("Processor", "% Processor Time", "_Total"),
                    Header = "Total % Processor Time",
                    Order = 1,
                    FormatString = "##0.0"
                },
                new PerformanceItem
                {
                    Counter = new PerformanceCounter("Process", "% Processor Time", Process.GetCurrentProcess().ProcessName),
                    Header = "Process % Processor Time",
                    Order = 2,
                    FormatString = "##0.0",
                    Converter = x => x/Environment.ProcessorCount
                },
                new PerformanceItem
                {
                    Counter = new PerformanceCounter("Memory", "Available Bytes", string.Empty),
                    Header = "Memory Available Bytes",
                    Order = 3,
                    FormatString = "#############"
                },
                new PerformanceItem
                {
                    Counter = new PerformanceCounter("Process", "Private Bytes", Process.GetCurrentProcess().ProcessName),
                    Header = "Process Private Bytes",
                    Order = 4,
                    FormatString = "#############"
                },
                new PerformanceItem
                {
                    Header = "Stations with Waveforms",
                    Order = 5,
                    FormatString = "############0"
                },
                new PerformanceItem
                {
                    Header = "Stage 1 Last Packet",
                    Order = 6,
                    FormatString = "u"
                },
                new PerformanceItem
                {
                    Header = "Stage 2 Last Waveform Processed",
                    Order = 6,
                    FormatString = "u"
                },
                new PerformanceItem
                {
                    Header = "Stage 4 Last Flash Generated",
                    Order = 6,
                    FormatString = "u"
                }
            };

            Dictionary<string, PerformanceItem> dict = new Dictionary<string, PerformanceItem>();
            foreach (PerformanceItem item in items)
            {
                dict.Add(item.Header, item);
            }

            return new PerfCsvWriter(dict, Config.PerfCsvWriterOutputDirectory);
        }

        private void OnPerfWrite(object state)
        {
            if (_perfCsvWriter != null)
            {
                Dictionary<string, Tuple<DateTime, double>> dict = new Dictionary<string, Tuple<DateTime, double>>
                {
                    { "Stations with Waveforms", new Tuple<DateTime, double>(DateTime.MinValue, 0) },
                    { "Stage 1 Last Packet", new Tuple<DateTime, double>(_rawDataQueueProcessor.LastPacketTime, 0) },
                    { "Stage 2 Last Waveform Processed", new Tuple<DateTime, double>(_waveformProcessor.LastWaveformProcessed, 0) },
                };

                _perfCsvWriter.RecordData(dict);
            }
        }

        private void OnCheckStatus(object sender)
        {
            //double rate = WriteState();
            double rate = (_receiverListener != null) ? _receiverListener.GetIncomingRatePerSecond(DateTime.UtcNow) : 0;

            if (_receiverListener == null)
                return;

            if (_receiverListener != null)
            {
                if (rate <= 0)
                {
                    EventManager.LogError(TaskMonitor.TidTcpSenderFail, "Critical Error: No raw data from Receiver, check network and Receiver");
                }

                EventManager.LogInfo(string.Format("Incoming TCP connections (Receiver): {0}", _receiverListener.Connections));
            }
        }
        // called from the normal datacontroller.cs file for the playback version
        public RawDataQueueProcessor RawDataQueueProcessor
        {
            get { return _rawDataQueueProcessor; }
        }
    }
}
