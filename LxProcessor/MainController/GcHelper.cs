﻿using System;
using System.Diagnostics;
using System.Runtime;
using System.Threading;

using Aws.Core.Utilities;

namespace LxProcessor.MainController
{
    public class GcHelper
    {
        private Timer _timer;
        private readonly long _maxLohSizeBytes;
        private readonly int _checkIntervalMinutes;
        private readonly PerformanceCounter _lohPerfCounter;
        private DateTime _lastSet;
        private readonly int _maxCompactInterval;

        public GcHelper(long maxLohSizeBytes, int checkIntervalMinutes, int maxCompactInterval)
        {
            _maxLohSizeBytes = maxLohSizeBytes;
            _checkIntervalMinutes = checkIntervalMinutes;
            _maxCompactInterval = maxCompactInterval;
            _lohPerfCounter = new PerformanceCounter(".NET CLR Memory", "Large Object Heap size", Process.GetCurrentProcess().ProcessName);
            _lastSet = DateTime.MinValue;
        }

        public void Start()
        {
            _timer = new Timer(OnLohCheck, null, TimeSpan.FromSeconds(30), TimeSpan.FromMinutes(_checkIntervalMinutes));
        }

        private void OnLohCheck(object state)
        {
            _lohPerfCounter.NextValue();

            Thread.Sleep(1000);// 1 second wait

            long lohSize = (long)_lohPerfCounter.NextValue();

            EventManager.LogInfo(string.Format("Large Object Heap size bytes {0}", lohSize.ToString("N")));

            TimeSpan ts = DateTime.UtcNow - _lastSet;

            if (lohSize > _maxLohSizeBytes && ts.TotalMinutes > _maxCompactInterval)
            {
                EventManager.LogInfo(string.Format("Large Object Heap size bytes {0:n0} greater than {1:n0}, last set {2:n0} setting GCLargeObjectHeapCompactionMode.CompactOnce", 
                    lohSize, _maxLohSizeBytes, ts.TotalMinutes));
                GCSettings.LargeObjectHeapCompactionMode = GCLargeObjectHeapCompactionMode.CompactOnce;
                _lastSet = DateTime.UtcNow;
            }
        }

        public void Stop()
        {
            if (_timer != null)
            {
                _timer.Dispose();
            }
        }


    }
}
