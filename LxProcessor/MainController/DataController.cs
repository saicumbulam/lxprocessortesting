using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

using Aws.Core.Utilities;

using LxCommon;
using LxCommon.Monitoring;
using LxCommon.TcpCommunication;

using LxProcessor.Common;
using LxProcessor.Utilities;

namespace LxProcessor.MainController
{
    public class DataController
    {
        private StationDataTable _stationDataTable;
        private ConcurrentDictionary<string, StationData> _stationData;
        private ConcurrentDictionary<string, DateTime> _activeSensors = new ConcurrentDictionary<string, DateTime>();  // Record the active sensor last waveform update time


        private Sender _flashDataSender;
        private Sender _rawDataSender;

        private readonly ProcessController _processController;
        private readonly int _totalProcessors;
        private readonly int _processorNum;
        private readonly bool _isPlayback;

        public DataController(ProcessController processController, bool isPlayback, int totalProcessors, int processorNum)
        {
            _processController = processController;

            if (Config.LtgManagerAddresses != null && Config.LtgManagerAddresses.Count > 0)
            {
                _flashDataSender = new Sender("Flash data sender", Config.LtgManagerAddresses);
            }

            if (Config.RawDataAddresses != null && Config.RawDataAddresses.Count > 0)
            {
                _rawDataSender = new Sender("Raw data sender", Config.RawDataAddresses);
            }

            _isPlayback = isPlayback;
            _totalProcessors = totalProcessors;
            _processorNum = processorNum;
        }

        public DecodeResult DecodeRawPacket(List<byte> packetBuffer, string remoteIp)
        {
            PacketHeaderInfo phi;
            DecodeResult result = TcpPacketUtils.TryTcpDecode(packetBuffer, remoteIp, out phi);

            if (result == DecodeResult.FoundPacket)
            {
                if (phi.MsgType != PacketType.MsgTypeKeepAlive)
                {
                    ProcessIncomingPacket(phi);
                }
            }

            return result;
        }

        // this function is called from the playback.cs file after successful read of the packets.  
        internal void ProcessIncomingPacket(PacketHeaderInfo data)
        {
            // Forward to other Receiver/Processor
            // check to see if there are multiple processors or recievers associated 
            // with the incoming packet
            if (_rawDataSender != null)
            {
                _rawDataSender.AddPacket(data);
            }
            // In a normal scenario when we run the msgtype from the packet is cheked with the packettype msg 
            if (data.MsgType == PacketType.MsgTypeLtg)
            {
                //if its a goodpacket, then the boolean true is returned or false is returned and data is not 
                //processed.
                /**Can be used to find importantly why data is not written **/ 
                if (GoodPacketForDetection(data, _isPlayback, _totalProcessors, _processorNum))
                {
                    //sent to the processcontroller for starting to process the data and for calculations.
                    _processController.RawDataQueueProcessor.Add(data);
                }
            }
            else if (data.MsgType == PacketType.MsgTypeGps)
            {
                UpdateStationGpsData(data);
            }

            if (Config.ExportAllRawData)
            {
                Export.RawData(data);
            }
        }

        private static readonly Rect _excludeRegion = Config.ExcludeSensorRegion;
        private static readonly Rect _includeRegion = Config.IncludeSensorRegion;

        private bool GoodPacketForDetection(PacketHeaderInfo data, bool isPlayback, int totalProcessors, int processorNum)
        {
            bool retVal = false;
            // check to see whether the data is in the valid range and not associated with any include and the exclude regiosn of the code
            StationData station;
            if (_stationData.TryGetValue(data.SensorId, out station))
            {
                if (_excludeRegion == null || !_excludeRegion.IsInside(station.Longitude, station.Latitude))
                {
                    if (_includeRegion == null || _includeRegion.IsInside(station.Longitude, station.Latitude))
                    {
                        if (station.QcFlag == 100 && station.IsGpsValid)
                        {
                            if (!isPlayback)
                                retVal = LtgTimeUtils.PacketGoodForProcessing(data.TimeStamp);
                            else
                                retVal = true; // if everything is normal , true is returned from here.

                            if (totalProcessors > 1)  //I think this is the multiproceesor codes and when the number of processor is less than
                                //the required one, then false is returned.
                            {
                                if (data.PacketTimeInSeconds % totalProcessors != processorNum)
                                    retVal = false;
                            }
                        }
                    }
                }
            }

            return retVal;
        }

        public void UpdateFlashData(List<LTGFlash> flashList)
        {
            if (Config.LtgManagerAddresses != null)
            {
                foreach (LTGFlash flashData in flashList)
                {
                    PacketHeaderInfo header = new PacketHeaderInfo();
                    byte[] packet = RawPacketUtil.EncodeLtgFlashToManager(flashData);
                    if (packet != null)
                    {
                        if (RawPacketUtil.GetPacketHeaderInfo(packet, header))
                        {
                            _flashDataSender.AddPacket(header);
                        }
                    }
                }
            }
        }

        private const double Epsilon = 0.001;
        private static readonly bool EnableGpsWriteDb = Config.EnableGpsWriteDb;

        private void UpdateStationGpsData(PacketHeaderInfo gpsDataHeader)
        {
            if (gpsDataHeader.MsgType == PacketType.MsgTypeGps)
            {
                try
                {
                    GpsData gpsData = gpsDataHeader.OtherObject as GpsData;

                    if (gpsData != null && !string.IsNullOrWhiteSpace(gpsData.SensorId))
                    {
                        bool isGpsValid = false;

                        if (gpsData.VisibleSatellites > 0 && gpsData.TrackedSatellites > 0)
                        {
                            if (!(Math.Abs(gpsData.Latitude - 0.0) < Epsilon && Math.Abs(gpsData.Longtitude - 0.0) < Epsilon && Math.Abs(gpsData.Height - 0.0) < Epsilon))
                                isGpsValid = true;
                        }

                        StationData sd = new StationData(gpsData.SensorId, gpsData.Latitude, gpsData.Longtitude, gpsData.Height, 100);
                        sd.SetVersion(gpsData.SensorVersion.Trim());
                        sd.IsGpsValid = isGpsValid;

                        _stationData.AddOrUpdate(sd.StationID,
                            (key) =>
                            {
                                EventManager.LogInfo("New sensor added from GPS packet: " + gpsData.SensorId);
                                return sd;
                            },
                            (key, existingValue) =>
                            {
                                //if we have db data use the existing qc flag, network type and calibration data
                                sd.QcFlag = existingValue.QcFlag;
                                sd.NetworkType = existingValue.NetworkType;
                                sd.CalibrationData = existingValue.CalibrationData;
                                return sd;
                            });

                        _stationDataTable.UpdateStationDistance(sd.StationID);

                        if (EnableGpsWriteDb)
                        {
                            InsertGpsPacket(gpsDataHeader, string.Empty);
                        }

                    }


                }
                catch (Exception ex)
                {
                    EventManager.LogError(EventId.MainController.DataController.UpdatingStation, "Failure updating station data", ex);
                }
            }
        }

        private void InsertGpsPacket(PacketHeaderInfo phi, string version)
        {
            GpsData gps = phi.OtherObject as GpsData;
            if (gps != null)
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Lightning"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("LtgUpdateGPSData_pr", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@SensorID", gps.SensorId);
                        cmd.Parameters.AddWithValue("@Version", gps.SensorVersion);
                        cmd.Parameters.AddWithValue("@DataVersion", version);
                        cmd.Parameters.AddWithValue("@Latitude", gps.Latitude);
                        cmd.Parameters.AddWithValue("@Longitude", gps.Longtitude);
                        cmd.Parameters.AddWithValue("@Height", gps.Height);
                        cmd.Parameters.AddWithValue("@RawData", phi.RawData);
                        cmd.Parameters.AddWithValue("@IPAddress", phi.RemoteIP);
                        cmd.Parameters.AddWithValue("@MACAddress", phi.RemoteMAC);

                        try
                        {
                            conn.Open();
                            cmd.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            EventManager.LogError(TaskMonitor.TidUpdateGpsDbFail, "Error inserting gps data", ex);
                        }
                        finally
                        {
                            conn.Close();
                        }
                    }
                }
            }
        }

        public void CloseConnections()
        {
            if (_rawDataSender != null)
            {
                _rawDataSender.Close();
            }

            if (_flashDataSender != null)
            {
                _flashDataSender.Close();
            }
        }

        public StationDataTable StationDataTable
        {
            get { return _stationDataTable; }
            set
            {
                _stationDataTable = value;
                _stationData = value.Data;
            }
        }

        public ConcurrentDictionary<string, DateTime> ActiveSensors
        {
            get { return _activeSensors; }
            set { _activeSensors = value; }
        }
    }
}
