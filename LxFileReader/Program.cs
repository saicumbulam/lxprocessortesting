﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

using Newtonsoft.Json;

using LxCommon.Models;

namespace LxFileReader
{
    public class Program
    {
        private const string FolderName = @"E:\En\LxProcessor\Export\Pre";
        private static void Main(string[] args)
        {
            string folderName;
            if (args != null && args.Length > 0 && !string.IsNullOrWhiteSpace(args[0]))
            {
				folderName = args[0];
            }
            else
            {
				folderName = FolderName;
            }

            Tuple<List<Flash>, int> flashes = null;
            Task.Run(async () =>
            {
                flashes = await ReadFolder(folderName);
            }).Wait();

            if (flashes != null)
            {
                Console.WriteLine("Total flashes {0}, total portions {1}", flashes.Item1.Count, flashes.Item2);
            }

            Console.ReadLine();
        }

        private static async Task<Tuple<List<Flash>, int>> ReadFolder(string folderName)
        {
            Tuple<List<Flash>, int> value = null;
            List<Flash> flashes = null;
            
            int portionCount = 0;
            try
            {
				DirectoryInfo folder = new DirectoryInfo(folderName);
				if (folder.Exists)
				{
					FileInfo[] files = folder.GetFiles("flash*.json", SearchOption.TopDirectoryOnly);
					if (files != null && files.Length > 0)
					{
						flashes = new List<Flash>();
						foreach (FileInfo file in files)
						{
							using (StreamReader reader = new StreamReader(file.FullName))
							{
								Console.WriteLine("Reading file: " + folderName);
								string line;
								while ((line = await reader.ReadLineAsync()) != null)
								{
									Flash f = JsonConvert.DeserializeObject<Flash>(line);
									flashes.Add(f);
									portionCount += f.Portions.Count;

									Console.WriteLine("Flash {0}, portions {1}", f.TimeStamp, f.Portions.Count);
								}
							}
						}

						value = new Tuple<List<Flash>, int>(flashes, portionCount);
					}
					else
					{
						Console.WriteLine("No files found: " + folderName);
					}
				}
                else
                {
					Console.WriteLine("folder does not exist: " + folderName);
				}
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return value;
        }
    }
}
