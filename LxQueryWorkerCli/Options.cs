﻿using System;
using System.Collections.Generic;
using CommandLine;
using LxPortalLib.Models;

namespace LxQueryWorkerCli
{
    class Options
    {
        [Option("querytype", Default = LxQueryType.Circle, HelpText = "Valid values are Circle, BoundingBox, or QuadKey")]
        public LxQueryType QueryType { get; set; }

        [Option("stroketype", Default = StrokeType.None, HelpText = "Valid values are CloudToGround, IntraCloud, None (don't filter)")]
        public StrokeType StrokeType { get; set; }

        [Option("lxtype", Default = LxType.Both, HelpText = "Valid values are Portion, Flash, Both (don't filter)")]
        public LxType LxType { get; set; }


        [Option("startdate", HelpText = "Query start date. Day only format: YYYY-MM-DD; Day with time format: YYYY-MM-DDTHH:MM:SS", Required = true)]
        public DateTime Start { get; set; }

        [Option("enddate", HelpText = "Query end date, inclusive. Day only format: YYYY-MM-DD; Day with time format: YYYY-MM-DDTHH:MM:SS", Required = true)]
        public DateTime End { get; set; }


        [Option("radius", SetName = "Circle", Default = 10, HelpText = "Size of radius")]
        public int? Radius { get; set; }

        [Option("radiustype", SetName = "Circle", Default = RadiusType.Miles, HelpText = "Valid values are Miles or Meters")]
        public RadiusType RadiusType { get; set; }

        [Option("latitude", SetName = "Circle", HelpText = "If QueryType is Circle, this must be included")]
        public double? Latitude { get; set; }

        [Option("longitude", SetName = "Circle", HelpText = "If QueryType is Circle, this must be included")]
        public double? Longitude { get; set; }



        [Option("latitudemax", SetName = "BoundingBox", HelpText = "If QueryType is BoundingBox, this must be included")]
        public double? LatitudeMax { get; set; }

        [Option("latitudemin", SetName = "BoundingBox", HelpText = "If QueryType is BoundingBox, this must be included")]
        public double? LatitudeMin { get; set; }

        [Option("longitudemax", SetName = "BoundingBox", HelpText = "If QueryType is BoundingBox, this must be included")]
        public double? LongitudeMax { get; set; }

        [Option("longitudemin", SetName = "BoundingBox", HelpText = "If QueryType is BoundingBox, this must be included")]
        public double? LongitudeMin { get; set; }


        [Option("outputtype", Default = FileOutputType.Json, HelpText = "Valid values are JSON, CSV, KMZ")]
        public FileOutputType OutputType { get; set; }

        [Option("emailaddress", HelpText = "If SendNotifications is set to true, a valid email address must be specified")]
        public string RecipientEmailAddress { get; set; }

        [Option("sendnotifications", Default = false, HelpText = "Sends notifications to provided email address. Valid values are true or false")]
        public bool? SendNotifications { get; set; }

        [Option("notifyprogress", Default = false, HelpText = "Sends progress notifications to provided email address. Valid values are true or false")]
        public bool? SendProgressNotifications { get; set; }


        [Option("quadkeys", SetName = "QuadKey", Separator = ':', HelpText = "If QueryType is QuadKey, this must be included. Separate multiple quadKeys with a colon (':')")]
        public IEnumerable<string> QuadKey { get; set; }

        [Option("outputversion", Default = LxOutputVersion.V3, HelpText = "Specifices which LX API version to use for output")]
        public LxOutputVersion OutputVersion { get; set; }

    }
}
