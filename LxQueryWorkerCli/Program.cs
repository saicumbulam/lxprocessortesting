﻿using System;
using System.Globalization;
using System.IO;
using CommandLine;
using LxPortalLib.Models;
using LxQueryWorker.Common;
using LxQueryWorker.Process;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace LxQueryWorkerCli
{
    class Program
    {
        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += OnUnhandledException;

            //var options = new Options();
            //var cliParser = new Parser(with =>
            //{
            //    with.CaseSensitive = false;
            //    with.EnableDashDash = true;
            //    //with.IgnoreUnknownArguments = true;
            //    with.ParsingCulture = CultureInfo.InvariantCulture;
            //    with.HelpWriter = Parser.Default.Settings.HelpWriter;
            //    with.CaseInsensitiveEnumValues = true;
            //});

            //var options = cliParser.ParseArguments<Options>(args).MapResult(opts => opts, errors => null);

            //if (options != null)
            //{
            //var lxQueryJobData = new LxQueryJobData
            //{
            //    JobId = Guid.NewGuid().ToString(),

            //    Start = options.Start,
            //    End = options.End,

            //    QueryType = options.QueryType,

            //    Radius = options.Radius,
            //    RadiusType = options.RadiusType,
            //    Latitude = options.Latitude,
            //    Longitude = options.Longitude,

            //    LatitudeMin = options.LatitudeMin,
            //    LatitudeMax = options.LatitudeMax,
            //    LongitudeMin = options.LongitudeMin,
            //    LongitudeMax = options.LongitudeMax,

            //    StrokeType = options.StrokeType,
            //    LxType = options.LxType,

            //    OutputType = options.OutputType,

            //    RecipientEmailAddress = options.RecipientEmailAddress,
            //    SendNotifications = options.SendNotifications,
            //    SendProgressNotifications = options.SendProgressNotifications,

            //    QuadKey = options.QuadKey.ToList(),

            //    OutputVersion = options.OutputVersion
            //};

            //var quadKeys = GetQuadKeys(lxQueryJobData);
            var bBox = new Aws.Core.Utilities.Spatial.BoundingBox((float)28
                                           , (float)-81.5
                                           , (float)30
                                           , (float)-80);
            var key = LxCommon.Utilities.TileSystem.BoundingBoxToQuadKeys(bBox.LatitudeMax, bBox.LongitudeMax, bBox.LatitudeMin, bBox.LongitudeMin);

            string x = "{\"JobId\":\"8b7fc17e-9008-4774-b208-40cfc2b80c06\",\"SubmittedDateTime\":\"2016-10-31T14:56:15.0000165Z\",\"QueryType\":0,\"StrokeType\":2,\"LxType\":0,\"Start\":\"2016-07-04T10:00:00Z\",\"End\":\"2016-07-05T18:00:00Z\",\"Radius\":null,\"RadiusType\":1,\"Latitude\":null,\"Longitude\":null,\"LatitudeMax\":37.0,\"LatitudeMin\":34.0,\"LongitudeMax\":-94.0,\"LongitudeMin\":-99.0,\"OutputType\":2,\"RecipientEmailAddress\":\"viyer@earthnetworks.com\",\"SendNotifications\":true,\"SendProgressNotifications\":true,\"QuadKey\":null,\"OutputVersion\":1,\"OutputCompressed\":true}";
            var lxQueryJobData = JsonConvert.DeserializeObject<LxQueryJobData>(x);

            try
            {

                var lxQueryProcessor = new QueryProcessor(Config.S3LxDataBucket, Config.S3LxQueryOutputBucket);
                //var lxQueryJobData = new LxQueryJobData
                //{
                //    JobId = Guid.NewGuid().ToString(),
                //    //Start = new DateTime(2015, 11, 4, 0, 0, 0, DateTimeKind.Utc),
                //    //End = new DateTime(2015, 11, 6, 0, 0, 0, DateTimeKind.Utc),
                //    Start = new DateTime(2016, 3, 1, 0, 0, 0, DateTimeKind.Utc),
                //    End = new DateTime(2016, 3, 3, 0, 0, 0, DateTimeKind.Utc),


                //    QueryType = LxQueryType.Circle,
                //    Radius = 500,
                //    RadiusType = RadiusType.Miles,
                //    Latitude = 33.4869,
                //    Longitude = -112.0743,




                //    //Min Lat:-27.03
                //    //Max Lat: -22.72
                //    //Min Long: -55.91
                //    //Max Long: -51.14
                //    //QueryType = LxQueryType.BoundingBox,
                //    LatitudeMin = -27.03,
                //    LatitudeMax = -22.72,
                //    LongitudeMin = -55.91,
                //    LongitudeMax = -51.14,


                //    //StrokeType = StrokeType.None, // This means both
                //    //LxType = LxType.Flash,
                //    StrokeType = StrokeType.None,
                //    LxType = LxType.Portion,

                //    SendNotifications = false,
                //    SendProgressNotifications = false,

                //    OutputType = FileOutputType.Csv
                //};

                Console.WriteLine(Config.StoreResultsInS3
                    ? $"Storing output to data S3 path {Config.S3LxQueryOutputBucket}/{lxQueryJobData.JobId}"
                    : $"Storing output data to {Config.JobFolder}{Path.DirectorySeparatorChar}{lxQueryJobData.JobId}{Path.DirectorySeparatorChar}");

                var result = false;
                var ctx = new CancellationTokenSource();
                var ct = ctx.Token;
                var t = Task.Run(async () =>
                {
                    result = await lxQueryProcessor.Process(lxQueryJobData, ct);
                }, ct);
                t.ConfigureAwait(false).GetAwaiter().GetResult();

                Console.WriteLine(Config.StoreResultsInS3
                    ? $"Check S3 path {Config.S3LxQueryOutputBucket}/{lxQueryJobData.JobId} for output data"
                    : $"Check {Config.JobFolder}{Path.DirectorySeparatorChar}{lxQueryJobData.JobId}{Path.DirectorySeparatorChar} for output data");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Failed to process request due to exception: {ex}");
            }
            //}
        }
        private static void OnUnhandledException(object sender, UnhandledExceptionEventArgs args)
        {
            var ex = (Exception)args.ExceptionObject;
            Console.WriteLine($"Caught global unhandled exception: {ex}");
            System.Environment.Exit(ex.HResult);
        }
    }
}
