using System;
using System.Collections.Concurrent;
using System.Configuration;
using System.Threading.Tasks;

using LxCommon;

using LxDataManager.Services;

namespace LxDataManager.Models
{
    public class SensorDetectionStatistics
    {
        private readonly ConcurrentDictionary<string, Datum> _sensorStats;
        private readonly PrimaryDatabaseGateway _databaseGateway;
        private readonly ParallelOptions _parallelOptions;
        public SensorDetectionStatistics(int saveMaxDegreeOfParallelism)
        {
            _sensorStats = new ConcurrentDictionary<string, Datum>();
            _databaseGateway = new PrimaryDatabaseGateway(ConfigurationManager.ConnectionStrings["SensorStatistics"].ConnectionString);
            _parallelOptions = new ParallelOptions { MaxDegreeOfParallelism = saveMaxDegreeOfParallelism };
        }

        public void Update(LTGFlash flash)
        {
            // Find the Offset
            foreach (LTGFlashPortion flashPortion in flash.FlashPortionList)
            {
                foreach (Offset offset in flashPortion.OffsetList)
                {
                    string sensorId = offset.StationId;
                    Datum stats = _sensorStats.GetOrAdd(sensorId, new Datum { SensorId = sensorId });
                    stats.Add(flashPortion);
                }
            }
        }

        public void Save(object state)
        {
            DateTime saveTime = DateTime.UtcNow;
            Parallel.ForEach(_sensorStats, _parallelOptions, (kvp) => 
                {
                    _databaseGateway.InsertSensorStatistic(kvp.Value, saveTime);
                });
        }

        public class Datum
        {
            private FlashHour[] _flashesByHour;
            private static DateTime _startTime = DateTime.MinValue;
            private const int SampleSize = 24;
            
            private class FlashHour
            {
                private object _lock;
                public FlashHour()
                {
                    // this is so we ignore these bukcets when GetFlashesInLast24Hours is called
                    HourId = 0 - SampleSize - 1;
                    Count = 0;
                    _lock = new object();
                }
                public int HourId { get; set; }
                public int Count { get; set; }

                public Object SyncLock 
                {
                    get { return _lock; }
                }
            }

            public Datum()
            {
                _flashesByHour = new FlashHour[SampleSize];
                for (int i = 0; i < SampleSize; i++)
                {
                    _flashesByHour[i] = new FlashHour();
                }
            }

            public void Add(LTGFlashPortion flash)
            {
                DateTime flashTime = LtgTimeUtils.GetUtcDateTimeFromString(flash.FlashTime);
                if (_startTime == DateTime.MinValue)
                {
                    _startTime = flashTime;
                }

                LastFlashTime = flash.FlashTime;
                LastFlashLatitude = flash.Latitude;
                LastFlashLongitude = flash.Longitude;
                LastFlashAmplitude = flash.Amplitude;

                TimeSpan ts = flashTime - _startTime;

                int index = (int)ts.TotalHours % SampleSize;
                int hourId = (int)ts.TotalHours;

                FlashHour fh = _flashesByHour[index];

                lock (fh.SyncLock)
                {
                    if (fh.HourId == hourId)
                    {
                        fh.Count = fh.Count + 1;
                    }
                    else
                    {
                        fh.HourId = hourId;
                        fh.Count = 1;
                    }
                }
            }

            public int GetFlashesInLast24Hours(DateTime saveTime)
            {
                TimeSpan ts = saveTime - _startTime;
                int saveHourId = (int)ts.TotalHours;

                int flashes = 0;

                foreach (FlashHour flashHour in _flashesByHour)
                {
                    lock (flashHour.SyncLock)
                    {
                        int diff = saveHourId - flashHour.HourId;
                        if (diff >= 0 && diff <= 24)
                        {
                            flashes = flashes + flashHour.Count;
                        }
                    }
                }

                return flashes;
            }

            public string SensorId { get; set; }
            public string LastFlashTime { get; set; }
            public double LastFlashLatitude { get; set; }
            public double LastFlashLongitude { get; set; }
            public double LastFlashAmplitude { get; set; }
        }
    }
}
