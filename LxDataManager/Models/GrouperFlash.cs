﻿using System;
using System.Threading;
using Aws.Core.Utilities;
using LxCommon;

namespace LxDataManager.Models
{
    public class GrouperFlash : IDisposable
    {
        private LTGFlash _flash;
        private long _start;
        private long _end;
        private long _groupTimeNanoseconds;
        private double _groupTlnDistanceInKm;
        private double _groupWwllnDistanceInKm;
        private DateTime _sendTime;
        private ReaderWriterLockSlim _sync;
        

        public GrouperFlash(LTGFlashPortion portion, double groupTlnDistanceInKm, double groupWwllnDistanceInKm, 
            long minimumFlashGroupTimeNanoseconds, int grouperTimeDelayInMillisecond, DateTime createTime)
        {
            _groupTimeNanoseconds = minimumFlashGroupTimeNanoseconds;
            _groupTlnDistanceInKm = groupTlnDistanceInKm;
            _groupWwllnDistanceInKm = groupWwllnDistanceInKm;
            _sync = new ReaderWriterLockSlim();

            _flash = new LTGFlash();
            _start = portion.TimeStamp.Nanoseconds;
            _end = _start;
            _sendTime = createTime.AddMilliseconds(grouperTimeDelayInMillisecond);
            _flash.AddFlashPortion(portion);
        }

        public bool IsCanidate(LTGFlashPortion portion, out double shortestDistanceKm)
        {
            bool isCanidate = false;
            long time = portion.TimeStamp.Nanoseconds;
            shortestDistanceKm = double.MaxValue;
            
            _sync.EnterReadLock();
            try
            {
                long start = _start - _groupTimeNanoseconds;
                long end = _end + _groupTimeNanoseconds;
                if (time >= start && time <= end)
                {
                    int index = 0;
                    while (index < _flash.FlashPortionList.Count)
                    {
                        LTGFlashPortion existingPortion = _flash.FlashPortionList[index] as LTGFlashPortion;
                        if (existingPortion != null)
                        {
                            double distance = StationData.DistanceInKM(existingPortion.Latitude, existingPortion.Longitude, 0, portion.Latitude, portion.Longitude, 0);
                            
                            if (existingPortion.IsWwlln || portion.IsWwlln)
                            {
                                if (distance <= _groupWwllnDistanceInKm)
                                {
                                    isCanidate = true;

                                    if (distance < shortestDistanceKm)
                                    {
                                        shortestDistanceKm = distance;
                                    }
                                }
                            }
                            else
                            {
                                if (distance <= _groupTlnDistanceInKm)
                                {
                                    isCanidate = true;

                                    if (distance < shortestDistanceKm)
                                    {
                                        shortestDistanceKm = distance;
                                    }
                                }
                            }
                        }

                        index++;
                    }
                }

                
            }
            catch (Exception ex)
            {
                EventManager.LogError(0, "Error in TryAddPortion", ex);
            }
            finally
            {
                _sync.ExitReadLock();   
            }

            return isCanidate;
        }

        public void AddPortion(LTGFlashPortion portion)
        {
            _sync.EnterWriteLock();
            try
            {
                if (portion.TimeStamp.Nanoseconds < _start)
                {
                    _start = portion.TimeStamp.Nanoseconds;
                }
                else if (portion.TimeStamp.Nanoseconds > _end)
                {
                    _end = portion.TimeStamp.Nanoseconds;
                }

                _flash.AddFlashPortion(portion);
            }
            catch (Exception ex)
            {
                EventManager.LogError(0, "Error in AddPortion", ex);
            }
            finally
            {
                _sync.ExitWriteLock();
            }

        }

        public bool IsTimeToSend(DateTime checkTime)
        {
            bool isTime = false;
            
            _sync.EnterReadLock();
            try
            {    
                isTime = (checkTime >= _sendTime);
            }
            finally
            {
                _sync.ExitReadLock();
            }

            return isTime;
        }

        public void Dispose()
        {
            ((IDisposable)_sync).Dispose();
        }

        public LTGFlash Flash
        {
            get { return _flash; }
        }

        ~GrouperFlash()
        {
            if (_sync != null) _sync.Dispose();
        }
    }
}
