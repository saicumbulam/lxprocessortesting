﻿using LxCommon;

namespace LxDataManager.Models
{
    public class GeoBoundingArea
    {
        public string Name { get; set; }
        public bool DoBuddyCheck { get; set;}
        public int BuddyCheckTimeSeconds { get; set; }
        public int BuddyCheckDistanceKm { get; set; }
        public int BuddyCheckMinNumNeighbor { get; set; }
        public FlashConfidenceData MinConfindence { get; set; }
        public Rect Bounds { get; set; }
        public FlashQuadTree Flashes { get; set; }
        public double? MaxNodeSizeInDegrees { get; set; }
        public double? MaxNodeSizeInKm { get; set; }
        public int StrictNearSensorConfidence { get; set; }
        public int StrictOffsetCount { get; set; }
        public int StrictBiggestAngle { get; set; }
        public int StrictSensorDistribution { get; set; }
    }
}
