using System;
using System.Collections.Generic;
using LxCommon;
using LxCommon.DataStructures;
using LxCommon.Models;

namespace LxDataManager.Models
{
    public class FlashQuadTree
    {
        private FlashQuadTree[] _nodes;
        private readonly Rect _bounds;
        private readonly double _maxNodeSize;
        private readonly bool _isMaxKm;

        private ConcurrentSortedSet<LTGFlash> _flashSet;
        private static readonly LtgFlashByTimeStamp FlashByTimeStamp;
        private static readonly LTGFlash MinFlash;


        static FlashQuadTree()
        {
            FlashByTimeStamp = new LtgFlashByTimeStamp();
            MinFlash = new LTGFlash() { TimeStamp = DateTimeUtcNano.MinValue };
        }

        public FlashQuadTree(Rect bounds, double maxNodeSize, bool isMaxKm)
        {
            _bounds = bounds;
            _maxNodeSize = maxNodeSize;
            _isMaxKm = isMaxKm;
            _flashSet = null;
            PopulateTree();
        }

        private void PopulateTree()
        {
            if (_isMaxKm)
            {
                if (_bounds.MaxSizeInKM() < _maxNodeSize)
                {
                    _flashSet = new ConcurrentSortedSet<LTGFlash>(FlashByTimeStamp);
                    return;
                }
            }
            else
            {
                if (_bounds.MaxSizeInDegree() < _maxNodeSize)
                {
                    _flashSet = new ConcurrentSortedSet<LTGFlash>(FlashByTimeStamp);
                    return;
                }

            }
            
            double midLontgitude = (_bounds.left + _bounds.right) / 2.0;
            double midLatitude = (_bounds.bottom + _bounds.top) / 2.0;

            _nodes = new FlashQuadTree[4];
            _nodes[0] = new FlashQuadTree(new Rect(midLontgitude, _bounds.right, midLatitude, _bounds.top), _maxNodeSize, _isMaxKm);
            _nodes[1] = new FlashQuadTree(new Rect(_bounds.left, midLontgitude, midLatitude, _bounds.top), _maxNodeSize, _isMaxKm);
            _nodes[2] = new FlashQuadTree(new Rect(_bounds.left, midLontgitude, _bounds.bottom, midLatitude), _maxNodeSize, _isMaxKm);
            _nodes[3] = new FlashQuadTree(new Rect(midLontgitude, _bounds.right, _bounds.bottom, midLatitude), _maxNodeSize, _isMaxKm);

        }

        public bool Contains(double latitude, double longitude)
        {
            return (longitude > _bounds.left && longitude <= _bounds.right && latitude > _bounds.bottom && latitude <= _bounds.top);
        }

      
        public void AddFlash(LTGFlash flash)
        {
            FlashQuadTree fqt = FindLeaf(flash.Latitude, flash.Longitude);
            if (fqt != null)
            {
                fqt._flashSet.TryAdd(flash);
            }
        }

        public FlashQuadTree FindLeaf(double latitude, double longitude)
        {
            if (Contains(latitude, longitude) && _nodes == null)
                return this;

            foreach (FlashQuadTree node in _nodes)
            {
                if (node != null && node.Contains(latitude, longitude))
                {
                    return node.FindLeaf(latitude, longitude);
                }
            }

            return null;
        }


        public void PruneOldFlashes(DateTimeUtcNano flashTime, long ageInNanoseconds)
        {
            if (_nodes != null)
            {
                foreach (FlashQuadTree node in _nodes)
                {
                    node.PruneOldFlashes(flashTime, ageInNanoseconds);
                }
            }
            else
            {
                RemoveOldFlashesInternal(flashTime, ageInNanoseconds);
            }
        }

        public LTGFlash[] GetRecentFlashes(DateTimeUtcNano flashTime, int ageInSeconds)
        {
            long ageInNanoseconds = ageInSeconds * DateTimeUtcNano.NanosecondsPerSecond;
            
            LTGFlash lower = new LTGFlash() { TimeStamp = new DateTimeUtcNano(flashTime.Nanoseconds - ageInNanoseconds) };
            LTGFlash upper = new LTGFlash() { TimeStamp = new DateTimeUtcNano(flashTime.Nanoseconds + ageInNanoseconds) };
            
            SortedSet<LTGFlash> between = _flashSet.GetBetween(lower, upper);

            LTGFlash[] flashes = new LTGFlash[between.Count];
            int i = 0;
            foreach (LTGFlash flash in between)
            {
                flashes[i] = flash;
                i++;
            }

            return flashes;
        }

        private const int AdditionalBufferMultiplier = 3;
        private void RemoveOldFlashesInternal(DateTimeUtcNano flashTime, long ageInNanoseconds)
        {
            long u = flashTime.Nanoseconds - (ageInNanoseconds * AdditionalBufferMultiplier);
            DateTimeUtcNano ut = new DateTimeUtcNano(u);
            LTGFlash upper = new LTGFlash() { TimeStamp = ut };

            _flashSet.RemoveBetween(MinFlash, upper);
        }

        private class LtgFlashByTimeStamp : IComparer<LTGFlash>
        {
            public int Compare(LTGFlash x, LTGFlash y)
            {
                return x.TimeStamp.CompareTo(y.TimeStamp);
            }
        }
    }
}
