﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using LxCommon;
using LxCommon.Models;
using LxDataManager.Common;
using LxDataManager.Process;
using Newtonsoft.Json;

namespace LxDataManager.Playback
{
    public class FlashPlayer
    {
        private ManualResetEventSlim _stopEvent;
        private IncomingFlashProcessor _incomingFlashProcessor;

        public FlashPlayer(IncomingFlashProcessor incomingFlashProcessor, ManualResetEventSlim stopEvent)
        {
            _stopEvent = stopEvent;
            _incomingFlashProcessor = incomingFlashProcessor;
        }

        public void Start()
        {
            Console.WriteLine("Waiting 10 seconds to begin playback");
            Thread.Sleep(TimeSpan.FromSeconds(10));

            FileInfo[] files = FindFiles(Config.PlaybackFlashDirectory);

            if (files != null)
            {
                foreach (FileInfo file in files)
                {
                    List<LTGFlash> fl = ReadFile(file.FullName);
                    if (fl != null && fl.Count > 0)
                    {
                        foreach (LTGFlash flash in fl)
                        {
                            PacketHeaderInfo phi = RawPacketUtil.EncodeFlashToPacket(flash);
                            if (phi != null)
                                _incomingFlashProcessor.Add(phi);

                            if (_stopEvent.Wait(0))
                                break;
                        }
                    }

                    if (_stopEvent.Wait(0))
                        break;
                }
            }
            else
                Console.WriteLine("No playback files found");
        }

        private static FileInfo[] FindFiles(string dirName)
        {
            FileInfo[] files = null;
            DirectoryInfo di = new DirectoryInfo(dirName);
            if (di.Exists)
            {
                files = di.GetFiles("*.*", SearchOption.AllDirectories);
                Array.Sort(files, CompareFileInfoByName);
            }

            return files;
        }

        private static int CompareFileInfoByName(FileInfo x, FileInfo y)
        {
            return x.Name.CompareTo(y.Name);
        }

        private static List<LTGFlash> ReadFile(string fileName)
        {
            List<LTGFlash> flashList = null;
            Console.WriteLine("Starting playback of {0}", fileName);
            try
            {
                using (StreamReader sr = File.OpenText(fileName))
                {
                    string line;
                    flashList = new List<LTGFlash>();
                    Portion portion = null;
                    LTGFlash lFlash = null;
                    LTGFlashPortion lPortion = null;
                    while ((line = sr.ReadLine()) != null)
                    {
                        try
                        {
                            portion = JsonConvert.DeserializeObject<Portion>(line);
                            lFlash = new LTGFlash();
                            if (portion != null)
                            {
                                Offset[] oa = {};
                                if (portion.StationOffsets != null)
                                {
                                    oa = new Offset[portion.StationOffsets.Count];
                                    int i = 0;
                                    foreach (KeyValuePair<string, double> stationOffset in portion.StationOffsets)
                                    {
                                        Offset o = new Offset(stationOffset.Key, (int)stationOffset.Value);
                                        oa[i] = o;
                                        i++;
                                    }
                                }

                                PortionExtendedData ed = new PortionExtendedData
                                {
                                    ErrorEllipse = portion.ErrorEllipse,
                                    Version = portion.Version,
                                    NumberStations = (portion.StationOffsets != null) ? portion.StationOffsets.Count : 0,
                                    StationOffsets = portion.StationOffsets

                                };
                                lPortion = new LTGFlashPortion(portion.TimeStamp, portion.Latitude, portion.Longitude, portion.Height, (float)portion.Amplitude, ed.ToJsonString(), portion.Type, portion.Confidence);
                                lPortion.OffsetList = new List<Offset>(oa);
                                lFlash.AddFlashPortion(lPortion);
                                lFlash.SetFlashData();
                                flashList.Add(lFlash);
                            }
                        }
                        catch(Exception ex)
                        {
                            Console.WriteLine("Unable to deserialize flash: Exception: {0}", ex.Message);
                        }
                    }
                }
                Console.WriteLine("Finshed playback of {0}", fileName);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Unable to read file: {0} Exception: {1}", fileName, ex.Message);
            }

            return flashList;
        }

        private static LTGFlash ParseFlashLine(string line)
        {
            LTGFlash flash = null;
            int index = line.IndexOf(' ');
            if (index > 0)
            {
                flash = new LTGFlash();
                flash.FlashTime = line.Substring(0, index - 1);
                index = line.IndexOf('(', index);
                if (index > 0)
                {
                    index++;
                    int endIndex = line.IndexOf(',', index);
                    if (endIndex > 0)
                    {
                        double d;
                        if (double.TryParse(line.Substring(index, (endIndex - index)), out d))
                        {
                            flash.Latitude = d;
                            index = endIndex + 1;
                            endIndex = line.IndexOf(',', index);
                            if (endIndex > 0)
                            {
                                if (double.TryParse(line.Substring(index, (endIndex - index)), out d))
                                {
                                    flash.Longitude = d;
                                    index = endIndex + 1;
                                    endIndex = line.IndexOf(')', index);
                                    if (endIndex > 0)
                                    {
                                        if (double.TryParse(line.Substring(index, (endIndex - index)), out d))
                                        {
                                            flash.Height = d;
                                            index = line.IndexOf("Type:", endIndex);
                                            if (index > 0)
                                            {
                                                index = index + 5;
                                                endIndex = line.IndexOf(',', index);
                                                if (endIndex > 0)
                                                {
                                                    if (line.Substring(index, (endIndex - index)) == "FlashTypeIC")
                                                        flash.FlashType = FlashType.FlashTypeIC;
                                                    else if (line.Substring(index, (endIndex - index)) == "FlashTypeCG")
                                                        flash.FlashType = FlashType.FlashTypeCG;

                                                    index = line.IndexOf("Amp:", endIndex);
                                                    if (index > 0)
                                                    {
                                                        index = index + 4;
                                                        endIndex = line.IndexOf(',', index);
                                                        if (endIndex > 0)
                                                        {
                                                            float f;
                                                            if (float.TryParse(line.Substring(index, (endIndex - index)), out f))
                                                            {
                                                                flash.Amplitude = f;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return flash;
        }

        private static LTGFlashPortion ParsePortionLine(string line)
        {
            LTGFlashPortion portion = null;
            int index = line.IndexOf("\tPortion:");
            if (index >= 0)
            {
                index = index + 9;
                int endIndex = line.IndexOf(' ', index);
                if (endIndex > 0)
                {
                    portion = new LTGFlashPortion();
                    portion.FlashTime = line.Substring(index, (endIndex - index));
                    index = line.IndexOf('(', index);
                    if (index > 0)
                    {
                        index++;
                        endIndex = line.IndexOf(',', index);
                        if (endIndex > 0)
                        {
                            double d;
                            if (double.TryParse(line.Substring(index, (endIndex - index)), out d))
                            {
                                portion.Latitude = d;
                                index = endIndex + 1;
                                endIndex = line.IndexOf(',', index);
                                if (endIndex > 0)
                                {
                                    if (double.TryParse(line.Substring(index, (endIndex - index)), out d))
                                    {
                                        portion.Longitude = d;
                                        index = endIndex + 1;
                                        endIndex = line.IndexOf(')', index);
                                        if (endIndex > 0)
                                        {
                                            if (double.TryParse(line.Substring(index, (endIndex - index)), out d))
                                            {
                                                portion.Height = d;
                                                index = line.IndexOf("Type:", endIndex);
                                                if (index > 0)
                                                {
                                                    index = index + 5;
                                                    endIndex = line.IndexOf(',', index);
                                                    if (endIndex > 0)
                                                    {
                                                        if (line.Substring(index, (endIndex - index)) == "FlashTypeIC")
                                                            portion.FlashType = FlashType.FlashTypeIC;
                                                        else if (line.Substring(index, (endIndex - index)) == "FlashTypeCG")
                                                            portion.FlashType = FlashType.FlashTypeCG;

                                                        index = line.IndexOf("Amp:", endIndex);
                                                        if (index > 0)
                                                        {
                                                            index = index + 4;
                                                            endIndex = line.IndexOf(',', index);
                                                            if (endIndex > 0)
                                                            {
                                                                float f;
                                                                if (float.TryParse(line.Substring(index, (endIndex - index)), out f))
                                                                {
                                                                    portion.Amplitude = f;
                                                                    index = line.IndexOf("Conf:", endIndex);
                                                                    if (index > 0)
                                                                    {
                                                                        index = index + 5;
                                                                        endIndex = line.IndexOf(",", index);
                                                                        if (endIndex > 0)
                                                                        {
                                                                            int i;
                                                                            if (int.TryParse(line.Substring(index, (endIndex - index)), out i))
                                                                            {
                                                                                portion.Confidence = i;
                                                                                index = line.IndexOf("Desc:Sensor: offset@err(num;dev;+peak;-peak)=", endIndex);
                                                                                if (index > 0)
                                                                                {
                                                                                    index = index + 46;
                                                                                    string offsets = line.Substring(index);
                                                                                    portion.OffsetList = ParseOffsets(offsets);
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }                                                     
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return portion;
        }
        //TLLHS:0.00@1.60(34;0;3362;-2054) MCDN2:16.00@1.17(23;0;192;-156) MCDON:50.00@0.77(29;0;875;-717) HPVLL:77.00@0.78(28;0;793;-655) CRSHR:118.00@0.91(31;0;1782;-1418) STNTL:158.00@-0.48(48;0;832;-712) RSWLC:211.00@0.95(30;0;1050;-941) MTNBR:282.00@-0.68(27;0;711;-649) PNSCM:342.00@-1.31(27;0;791;-673) WSTPS:383.00@-1.20(22;0;415;-445) TSCLB:390.00@-0.62(26;0;1222;-1050) 
        private static List<Offset> ParseOffsets(string offsetStr)
        {
            List<Offset> oal = new List<Offset>();
            string[] offsets = offsetStr.Split(' ');
            if (offsets.Length > 0)
            {
                foreach (string offset in offsets)
                {
                    Offset o = new Offset();
                    int index = offset.IndexOf(":");
                    if (index > 0)
                    {
                        o.StationId = offset.Substring(0, index);
                        index++;
                        if (index < offset.Length)
                        {
                            int endIndex = offset.IndexOf('@', index);
                            if (endIndex > index)
                            {
                                double d;

                                if (double.TryParse(offset.Substring(index, endIndex - index), out d))
                                {
                                    o.Value = d;
                                    index = endIndex + 1;
                                    endIndex = offset.IndexOf("(", endIndex);
                                    if (endIndex > 0)
                                    {
                                        if (double.TryParse(offset.Substring(index, endIndex - index), out d))
                                        {
                                            o.ResidualError = d;
                                            oal.Add(o);
                                        }
                                    }
                                    
                                }
                            }
                        }
                    }



                }
            }

            return oal;
        }
    }
}
