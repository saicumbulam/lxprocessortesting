using System;
using System.Threading;

using Aws.Core.Utilities;

using LxCommon;
using LxCommon.Monitoring;
using LxCommon.TcpCommunication;

using LxDataManager.Common;
using LxDataManager.Models;
using LxDataManager.Playback;
using LxDataManager.Process;

namespace LxDataManager
{
    public class Controller
    {
        private ManualResetEventSlim _stopEventS;
        
        private Timer _statisticsTimer;
        private Timer _stationUpdateTimer;
        private Timer _keepAliveTimer;
        
        private StationDataTable _stationDataTable;

        private FlashValidatorProcessor _flashValidatorProcessor;
        private PortionGroupingProcessor _portionGroupingProcessor;
        private IncomingFlashProcessor _incomingFlashProcessor;

        private ArchivingProcessor _archivingProcessor;
        private SensorDetectionStatistics _sensorDetectionStatistics = null;
        private RejectedFlashLogger _rejectedFlashLogger = null;
        private Listener _processorListener;
        private Responder _datafeedResponder;
        private Sender _managerSender;
        private bool _isTln;
        private MonitoringHandler _monitoringHandler;
        private PortionDeduper _portionDeduper;


        public Controller()
        {
            _stopEventS = new ManualResetEventSlim(false);
            _isTln = (Config.SystemType.ToUpperInvariant() == "TLN");
            SqlServerTypes.Utilities.LoadNativeAssemblies(AppDomain.CurrentDomain.BaseDirectory);
        }

        public void Start()
        {
            InitalizePipeline();

            InitalizeTimers();

            InitalizeCommunication();

            _monitoringHandler = new MonitoringHandler(_isTln, _processorListener, _datafeedResponder, _managerSender, _flashValidatorProcessor, 
                _incomingFlashProcessor, _portionGroupingProcessor, _archivingProcessor);
            _monitoringHandler.Start();
        }

        public void ShutDown()
        {
            EventManager.LogInfo("Beginning shut down sequence.");

            // this will signal the pipeline to shutdown
            if (_stopEventS != null)
                _stopEventS.Set();
            
            _portionGroupingProcessor.Stop();

            CleanupTimers();

            CleanupCommunication();

            //_monitoringHandler.Stop();

            EventManager.LogInfo("Finished shut down sequence");
        }

        private void InitalizePipeline()
        {
            _stationDataTable = new StationDataTable();

            if (Config.IsSensorStatisticsEnabled)
            {
                _sensorDetectionStatistics = new SensorDetectionStatistics(Config.SensorStatisiticsSaveMaxDegreeOfParallelism);
            }

            if (Config.EnableRejectedFlashLogger)
            {
                RejectedFlashLogger.Create(Config.RejectedFlashLoggerDirectory, Config.RejectedFlashLoggerBaseFileName, Config.RejectedFlashLoggerMinutesPerFile, _stopEventS);
            }

            _portionDeduper = new PortionDeduper(Config.ValidationDuplicateDistanceKm, Config.ValidationDuplicateTimeSeconds, Config.RecentFlashesNumberOfSecondsToKeep);

            _archivingProcessor = new ArchivingProcessor(Config.IsPrimaryDbEnabled, Config.IsEtlDbEnabled, _sensorDetectionStatistics, Config.MedianSampleSize, Config.MaxArchivingQueueSize, 
                _stopEventS, Config.MaxArchivingQueueTasks);

            FlashValidatorSettings settings = new FlashValidatorSettings
            {
                StrongAmplitude = Config.ValidationStrongAmplitude,
                CloseStationDistanceKm = Config.ValidationCloseStationDistanceKm,
                RegionCleanupIntervalMinutes = Config.RegionCleanupIntervalMinutes,
                MedianSampleSize = Config.MedianSampleSize,
                MinimumPortionsForValidFlash = Config.MinimumPortionsForValidFlash,
                WeakAmplitude = Config.ValidationWeakAmplitude
            };

            _flashValidatorProcessor = new FlashValidatorProcessor(this, _archivingProcessor, Config.TlnAreas, Config.WwllnGlobalArea, settings, _stopEventS, Config.MaxFlashValidatorProcessorTasks);

            PortionGroupingSettings grouperSettings = new PortionGroupingSettings
            {
                GrouperTimeDelayInMillisecond = Config.GrouperTimeDelayInMillisecond,
                MinimumGroupTimeSeconds = Config.MinimumFlashGroupTimeInSecond,
                MinimumTlnGroupDistanceKm = Config.MinimumTlnFlashGroupDistanceInKm,
                MinimumWwllnGroupDistanceKm = Config.MinimumWwllnFlashGroupDistanceInKm,
                SendCheckIntervalMilliseconds = Config.GrouperSendCheckIntervalMilliseconds
            };
            
            _portionGroupingProcessor = new PortionGroupingProcessor(_flashValidatorProcessor, _stationDataTable.Data, _portionDeduper, grouperSettings, 
                _stopEventS, Config.MaxFlashPortionGrouperQueueTasks);

            _incomingFlashProcessor = new IncomingFlashProcessor(_portionGroupingProcessor, Config.TlnAreas, Config.WwllnExcludingRects, _stationDataTable.Data, 
                Config.DefaultConfidence, Config.MedianSampleSize,_stopEventS, Config.MaxIncomingFlashProcessorTasks);
        }
        
        private void InitalizeCommunication()
        {
            if (Config.IsPlayback)
            {
                FlashPlayer fp = new FlashPlayer(_incomingFlashProcessor, _stopEventS);
                Thread thread = new Thread(fp.Start);
                thread.Start();
            }
            else
            {
                _processorListener = new Listener("Processor Listener", System.Net.IPAddress.Any, Config.TcpListenPort, Config.ProcessorMaxPendingConnections,
                    _incomingFlashProcessor.TcpFlashDecode, Config.ProcessorReceiveSampleSize, Config.ProcessorConnectionSampleSize, Config.ListenerIdleCheckIntervalSeconds, 
                    Config.MonitorProcessorActiveConnectionsMinimum);
                _processorListener.Open();
            }

            _datafeedResponder = new Responder("Datafeed Responder", System.Net.IPAddress.Any, Config.TcpFlashServerListenPort, Config.DatafeedMaxPendingConnections, Config.DatafeedConnectionSampleSize);
            _datafeedResponder.Open();

            if (Config.OtherManagerTcpAddresses != null && Config.OtherManagerTcpAddresses.Count > 0)
            {
                _managerSender = new Sender("Manager Sender", Config.OtherManagerTcpAddresses);
            }
        }

        private void CleanupCommunication()
        {
            if (_processorListener != null)
                _processorListener.Close();

            if (_datafeedResponder != null)
                _datafeedResponder.Close();

            if (_managerSender != null)
                _managerSender.Close();
        }

        private void InitalizeTimers()
        {
            _stationUpdateTimer = new Timer(_stationDataTable.Load, null, new TimeSpan(0), TimeSpan.FromMinutes(Config.StationDatabaseUpdateIntervalMinutes));

            _keepAliveTimer = new Timer(SendKeepAlivePacket, null, TimeSpan.FromSeconds(Config.TcpKeepAliveIntervalSeconds), TimeSpan.FromSeconds(Config.TcpKeepAliveIntervalSeconds));

            if (_sensorDetectionStatistics != null)
            {
                _statisticsTimer = new Timer(_sensorDetectionStatistics.Save, null, TimeSpan.FromMinutes(30), TimeSpan.FromMinutes(60));
            }
        }

        private void CleanupTimers()
        {
            if (_stationUpdateTimer != null)
                _stationUpdateTimer.Dispose();

            if (_keepAliveTimer != null)
                _keepAliveTimer.Dispose();

            if (_statisticsTimer != null)
                _statisticsTimer.Dispose();
        }
        
        public void SendFlash(LTGFlash flash)
        {
            if (flash != null && flash.FlashPortionList != null && flash.FlashPortionList.Count > 0)
            {
                PacketHeaderInfo phi = null;

                try
                {
                    phi = RawPacketUtil.EncodeFlashToPacket(flash);
                }
                catch (Exception ex)
                {
                    EventManager.LogError(TaskMonitor.SendWorker, "Error encoding flash", ex);
                }

                if (phi != null)
                {
                    byte[] packet = TcpPacketUtils.TcpEncodeWithInt32Length(phi);

                    if (_datafeedResponder != null)
                    {
                        _datafeedResponder.AddPacket(packet);
                    }

                    if (_managerSender != null)
                    {
                        _managerSender.AddPacket(packet);
                    }
                }
            }
        }

        private void SendKeepAlivePacket(object state)
        {
            PacketHeaderInfo phi = RawPacketUtil.GetAKeepAlivePacket();
            if (phi != null)
            {
                byte[] packet = TcpPacketUtils.TcpEncodeWithInt32Length(phi);

                if (_datafeedResponder != null)
                {
                    _datafeedResponder.AddPacket(packet);
                }
            }
        }
    }
}
