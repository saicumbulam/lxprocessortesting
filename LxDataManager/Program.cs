﻿using System;
using System.ServiceProcess;
using Aws.Core.Utilities;
using LxCommon.Monitoring;

namespace LxDataManager
{
    public class Program
    {
        private static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
            { 
                new WinService() 
            };
            ServiceBase.Run(ServicesToRun);
        }

        private static Controller _controller = null;

        public static void Start()
        {
            EventManager.Init();

            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += OnUnhandledException;

            _controller = new Controller();
            _controller.Start();
        }

        public static void Stop()
        {
            if (_controller != null) _controller.ShutDown();
            EventManager.ShutDown();
        }

        private static void OnUnhandledException(object sender, UnhandledExceptionEventArgs args)
        {
            Exception ex = (Exception)args.ExceptionObject;
            EventManager.LogError(TaskMonitor.TidServiceStoppedFail, "Global unhandled exception", ex);
            Stop();
        }
    }
}
