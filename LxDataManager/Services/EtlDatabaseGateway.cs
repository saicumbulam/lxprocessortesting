﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using LxCommon;
using LxCommon.Monitoring;
using LxCommon.Utilities;
using LxDataManager.Common;
using Microsoft.SqlServer.Types;
using Aws.Core.Utilities;

namespace LxDataManager.Services
{
    public class EtlDatabaseGateway 
    {
        private const ushort EventManagerOffset = TaskMonitor.ManagerEtlDatabaseGateway;

        private static readonly bool EnableDbFailureLogging = Config.EnableDbFailureLogging;

        private readonly string _connectionString;
        private readonly FlashDataLogger _failedFlashLogger;
        private readonly FlashDataLogger _failedPortionLogger;

        public EtlDatabaseGateway(string connectionString)
        {
            _connectionString = connectionString;
            bool isTlnSystem = Config.SystemType.ToUpper().Equals("TLN");

            if (EnableDbFailureLogging)
            {
                _failedFlashLogger = new FlashDataLogger(Config.EtlFailedFlashFolder, Config.EtlFailedFlashFileName, isTlnSystem);
                _failedPortionLogger = new FlashDataLogger(Config.EtlFailedPortionFolder, Config.EtlFailedPortionFileName, isTlnSystem);
            }
        }

        public void InsertFlash(LTGFlash flash)
        {
            Guid flashId = Guid.NewGuid();
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("InsertStageFlash_pr", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    CreateCommonParams(flashId, flash, cmd);
                    cmd.Parameters.AddWithValue("@StrokeSolution", flash.Description);
                    try
                    {
                        conn.Open();
                        cmd.ExecuteNonQuery();
                        conn.Close();
                        InsertFlashPortions(flashId, flash.FlashPortionList);
                    }
                    catch (Exception ex)
                    {
                        EventManager.LogError(TaskMonitor.TidSaveLxFlashProcessFail, "Error inserting flash", ex);
                        if (EnableDbFailureLogging)
                            _failedFlashLogger.WriteFlash(flash, LtgTimeUtils.GetUtcDateTimeFromString(flash.FlashTime));
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }
        }

        private void InsertFlashPortions(Guid flashId, ArrayList portionList)
        {
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                foreach (LTGFlashPortion portion in portionList)
                {
                    using (SqlCommand cmd = new SqlCommand("InsertStageFlashPortion_pr", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        CreateCommonParams(flashId, portion, cmd);
                        cmd.Parameters.AddWithValue("@StrokeSolution", portion.Description);
                        try
                        {
                            conn.Open();
                            cmd.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            EventManager.LogError(TaskMonitor.TidSaveLxFlashPortionToDBFail, "Error inserting flash portion", ex);
                            if (EnableDbFailureLogging)
                                _failedPortionLogger.WritePortion(portion, flashId, LtgTimeUtils.GetUtcDateTimeFromString(portion.FlashTime));
                        }
                        finally
                        {
                            conn.Close();
                        }
                    }
                }
            }
        }

        private static void CreateCommonParams(Guid flashId, FlashData fd, SqlCommand cmd)
        {

            cmd.Parameters.AddWithValue("@FlashGUID", flashId);
            cmd.Parameters.AddWithValue("@LightningTime", LtgTimeUtils.GetUtcDateTimeFromString(fd.FlashTime));
            cmd.Parameters.AddWithValue("@LightningTimeString", fd.FlashTime);
            cmd.Parameters.AddWithValue("@Latitude", fd.Latitude);
            cmd.Parameters.AddWithValue("@Longitude", fd.Longitude);
            cmd.Parameters.AddWithValue("@Height", Convert.ToDecimal(fd.Height.ToString("0.0")));
            cmd.Parameters.AddWithValue("@StrokeType", FlashUtils.ConvertWwllnTypeToTlnType(fd.FlashType));
            cmd.Parameters.AddWithValue("@Amplitude", Convert.ToDecimal(fd.Amplitude.ToString("0.0")));
            cmd.Parameters.AddWithValue("@Confidence", fd.Confidence);

            cmd.Parameters.Add(new SqlParameter("@GeoPoint", CreatePoint(fd.Latitude, fd.Longitude)) { UdtTypeName = "Geography" });
        }

        public const int Wgs84SpatialRefId = 4326;
        private static SqlGeography CreatePoint(double lat, double lon)
        {
            SqlGeography geo = null;
            try
            {
                SqlGeographyBuilder b = new SqlGeographyBuilder();
                b.SetSrid(Wgs84SpatialRefId);
                b.BeginGeography(OpenGisGeographyType.Point);
                b.BeginFigure(lat, lon);
                b.EndFigure();
                b.EndGeography();
                geo = b.ConstructedGeography;
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventManagerOffset, "Error creating SqlGeography type", ex);
            }
            return geo;
        }

        public void AddFlashToFailureLog(LTGFlash flash)
        {
            if (EnableDbFailureLogging)
                _failedFlashLogger.WriteFlash(flash, LtgTimeUtils.GetUtcDateTimeFromString(flash.FlashTime));
        }
    }
}
