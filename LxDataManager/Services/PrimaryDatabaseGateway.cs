﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using Aws.Core.Utilities;
using LxCommon;
using LxCommon.Monitoring;
using LxCommon.Utilities;
using LxDataManager.Common;
using LxDataManager.Models;

namespace LxDataManager.Services
{
    public class PrimaryDatabaseGateway
    {
        private static readonly bool EnableDbFailureLogging = Config.EnableDbFailureLogging;

        private readonly string _connectionString;
        private FlashDataLogger _failedFlashLogger;
        private FlashDataLogger _failedPortionLogger;

        public PrimaryDatabaseGateway(string connectionString)
        {
            _connectionString = connectionString;

            bool isTlnSystem = Config.SystemType.ToUpper().Equals("TLN");

            if (EnableDbFailureLogging)
            {
                _failedFlashLogger = new FlashDataLogger(Config.PrimaryFailedFlashFolder, Config.PrimaryFailedFlashFileName, isTlnSystem);
                _failedPortionLogger = new FlashDataLogger(Config.PrimaryFailedPortionFolder, Config.PrimaryFailedPortionFileName, isTlnSystem);
            }
        }

        public void InsertFlash(LTGFlash flash)
        {
            Guid? flashId = null;
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("LtgInsertFlash_pr", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    CreateCommonParams(flash, cmd);
                    cmd.Parameters.AddWithValue("@Solution", flash.Description);
                    try
                    {
                        conn.Open();
                        flashId = (Guid)cmd.ExecuteScalar();
                    }
                    catch (Exception ex)
                    {
                        EventManager.LogError(TaskMonitor.TidSaveLxFlashProcessFail, "Error inserting flash", ex);
                        if (EnableDbFailureLogging)
                            _failedFlashLogger.WriteFlash(flash, LtgTimeUtils.GetUtcDateTimeFromString(flash.FlashTime));
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }

            if (flashId.HasValue)
                InsertFlashPortions(flashId.Value, flash.FlashPortionList);
        }

        private void InsertFlashPortions(Guid flashId, ArrayList portionList)
        {
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                foreach (LTGFlashPortion portion in portionList)
                {
                    using (SqlCommand cmd = new SqlCommand("LtgInsertFlashPortion_pr", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@FlashGUID", flashId);
                        cmd.Parameters.AddWithValue("@Offsets", RawPacketUtil.FormatOffsetAbsPeakAmplitude(portion.OffsetList));
                        cmd.Parameters.AddWithValue("@Solution", portion.Description);

                        CreateCommonParams(portion, cmd);
                        try
                        {
                            conn.Open();
                            cmd.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            EventManager.LogError(TaskMonitor.TidSaveLxFlashPortionToDBFail, "Error inserting flash portion", ex);
                            if (EnableDbFailureLogging)
                                _failedPortionLogger.WritePortion(portion, flashId, LtgTimeUtils.GetUtcDateTimeFromString(portion.FlashTime));
                        }
                        finally
                        {
                            conn.Close();
                        }
                    }
                }
            }
        }


        private static void CreateCommonParams(FlashData fd, SqlCommand cmd)
        {
            cmd.Parameters.AddWithValue("@Time", LtgTimeUtils.GetUtcDateTimeFromString(fd.FlashTime));
            cmd.Parameters.AddWithValue("@TimeStr", fd.FlashTime);
            cmd.Parameters.AddWithValue("@Latitude", fd.Latitude);
            cmd.Parameters.AddWithValue("@Longitude", fd.Longitude);
            cmd.Parameters.AddWithValue("@Height", Convert.ToDecimal(fd.Height.ToString("0.0")));
            cmd.Parameters.AddWithValue("@Type", FlashUtils.ConvertWwllnTypeToTlnType(fd.FlashType));
            cmd.Parameters.AddWithValue("@Amp", Convert.ToDecimal(fd.Amplitude.ToString("0.0")));
            cmd.Parameters.AddWithValue("@Confidence", fd.Confidence);
        }

        public void InsertSensorStatistic(SensorDetectionStatistics.Datum stat, DateTime saveTime)
        {
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("LtgUpdateSensorDetectionStatistics_pr", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@SensorID", stat.SensorId);
                    cmd.Parameters.AddWithValue("@LastFlashTime", stat.LastFlashTime);
                    cmd.Parameters.AddWithValue("@LastFlashLatitude", stat.LastFlashLatitude);
                    cmd.Parameters.AddWithValue("@LastFlashLongitude", stat.LastFlashLongitude);
                    cmd.Parameters.AddWithValue("@LastFlashAmp", stat.LastFlashAmplitude);
                    cmd.Parameters.AddWithValue("@TotalFlashInLast24Hours", stat.GetFlashesInLast24Hours(saveTime));
                    try
                    {
                        conn.Open();
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        EventManager.LogError(TaskMonitor.TidSaveLxFlashStatFail, "Error inserting sensor detection statistics", ex);
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }
        }

        public void AddFlashToFailureLog(LTGFlash flash)
        {
            if (EnableDbFailureLogging)
                _failedFlashLogger.WriteFlash(flash, LtgTimeUtils.GetUtcDateTimeFromString(flash.FlashTime));
        }
    }
}
