﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;

using LxCommon;
using LxCommon.DataStructures;
using LxCommon.TcpCommunication;
using LxCommon.Utilities;
using LxDataManager.Common;
using LxDataManager.Models;

namespace LxDataManager.Process
{
    public class IncomingFlashProcessor : ParallelQueueProcessor<PacketHeaderInfo>
    {
        private readonly Rect[] _wwllnExclusions;
        private readonly PortionGroupingProcessor _portionGroupingProcessor;
        private readonly GeoBoundingArea[] _tlnAreas;
        private readonly ConcurrentDictionary<string, StationData> _stationData;
        private readonly FlashConfidenceData _defaultConfidence;
        private MedianSampler<double> _wwllnSamples;
        private MedianSampler<double> _tlnSamples;

        public IncomingFlashProcessor(PortionGroupingProcessor portionGroupingProcessor, GeoBoundingArea[] tlnAreas, Rect[] wwllnExclusions, ConcurrentDictionary<string, StationData> stationData, 
            FlashConfidenceData defaultConfidence, int medianSampleSize, ManualResetEventSlim stopEvent, int maxActiveWorkItems) : base(stopEvent, maxActiveWorkItems)
        {
            _portionGroupingProcessor = portionGroupingProcessor;
            _tlnAreas = tlnAreas;
            _wwllnExclusions = wwllnExclusions;
            _stationData = stationData;
            _defaultConfidence = defaultConfidence;
            
            //wwlln produce roughly a third of the amount of data as tln
            _wwllnSamples = new MedianSampler<double>(medianSampleSize/3);
            _tlnSamples = new MedianSampler<double>(medianSampleSize);
        }

        public DecodeResult TcpFlashDecode(List<byte> packetBuffer, string remoteIp)
        {
            PacketHeaderInfo phi;
            DecodeResult result = TcpPacketUtils.TryTcpDecode(packetBuffer, remoteIp, out phi);

            if (result == DecodeResult.FoundPacket)
            {
                Add(phi);
            }

            return result;
        }

        protected override void DoWork(PacketHeaderInfo phi)
        {
            if (phi != null && phi.MsgType != PacketType.MsgTypeKeepAlive)
            {
                LTGFlash flash = RawPacketUtil.DecodeFlashPacket(phi.RawData);
                DateTime utcNow = DateTime.UtcNow;
                if (flash != null)
                {
                    bool isWwlln = (flash.FlashType == FlashType.FlashTypeGlobalCG || flash.FlashType == FlashType.FlashTypeGlobalIC);

                    if (isWwlln)
                    {
                        flash = FlashUtils.ConvertWwllnFlashToTlnFlash(flash);
                        _wwllnSamples.AddSample((utcNow - flash.TimeStamp.BaseTime).TotalSeconds);
                    }
                    else
                    {
                        _tlnSamples.AddSample((utcNow - flash.TimeStamp.BaseTime).TotalSeconds);
                    }

                    if (flash.FlashPortionList != null)
                    {
                        foreach (LTGFlashPortion portion in flash.FlashPortionList)
                        {
                            bool isValid = false;
                            if (portion.IsWwlln)
                            {
                                isValid = !InWwllnExclusionZone(portion, _wwllnExclusions);
                            }
                            else
                            {
                                isValid = IsValidTlnPortion(portion, _tlnAreas, _stationData, _defaultConfidence);
                            }

                            if (isValid)
                            {
                                _portionGroupingProcessor.Add(portion);
                            }
                            else
                            {
                                RejectedFlashLogger.Write(portion);
                            }
                        }
                    }
                }
            }
        }
        
        private bool InWwllnExclusionZone(LTGFlashPortion portion, Rect[] wwllnExclusions)
        {
            bool inExclusion = false;
            if (wwllnExclusions != null)
            {
                foreach (Rect wwllnExclusion in wwllnExclusions)
                {
                    if (wwllnExclusion.IsInside(portion.Longitude, portion.Latitude))
                    {
                        inExclusion = true;
                        portion.RejectedReason = "in Wwlln exclusion zone";
                        break;
                    }
                }
            }
            return inExclusion;
        }

        private bool IsValidTlnPortion(LTGFlashPortion portion, GeoBoundingArea[] tlnAreas, ConcurrentDictionary<string, StationData> stationData, FlashConfidenceData defaultConfidence)
        {
            bool isValid = false;


            GeoBoundingArea gba = FindArea(portion, tlnAreas);
            if (gba != null)
            {
                FlashUtils.SetFlashPortionConfidenceData(portion, stationData);

                FlashConfidenceData conf = gba.MinConfindence ?? defaultConfidence;

                isValid = portion.CheckConfidence(conf);
                
                if (isValid)
                {
                    CalculateClosestDistanceToFlash(portion, stationData);
                }
                else
                {
                    portion.RejectedReason = "failed confidence check";
                }
            }
            else
            {
                portion.RejectedReason = "no GeoBoundingArea found";
            }
            
            return isValid;
        }

        private GeoBoundingArea FindArea(LTGFlashPortion portion, GeoBoundingArea[] tlnAreas)
        {
            GeoBoundingArea gba = null;
            
            if (tlnAreas != null)
            {
                foreach (GeoBoundingArea area in tlnAreas)
                {
                    if (area.Flashes.Contains(portion.Latitude, portion.Longitude))
                    {
                        gba = area;
                        break;
                    }
                }
            }

            return gba;
        }
        
        private void CalculateClosestDistanceToFlash(LTGFlashPortion portion, ConcurrentDictionary<string, StationData> stationData)
        {
            if (portion.OffsetList != null && portion.OffsetList.Count > 0)
            {
                StationData station;
                if (stationData.TryGetValue(portion.OffsetList[0].StationId, out station))
                {
                    portion.OffsetList[0].DistanceToFlash = station.DistanceFromLatLongHeightInKM(portion.Latitude, portion.Longitude, 0);
                }
            }
        }

        public Tuple<double, double, double> TlnMinMaxMedian
        {
            get { return _tlnSamples.GetMinMaxMedian(); }
        }

        public Tuple<double, double, double> WwllnMinMaxMedian
        {
            get { return _wwllnSamples.GetMinMaxMedian(); }
        }
    }
}
