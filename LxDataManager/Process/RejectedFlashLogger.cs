﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Aws.Core.Utilities;

using LxCommon;
using LxCommon.Models;
using LxCommon.Monitoring;
using LxCommon.Utilities;

namespace LxDataManager.Process
{
    public class RejectedFlashLogger
    {
        private const ushort EventManagerOffset = TaskMonitor.ManagerRejectedFlashLogger;

        private string _baseFileName;
        private ConcurrentQueue<LTGFlashPortion> _portionsToWrite;
        private int _writerLock;
        private DirectoryInfo _directory = null;
        private int _minutesPerFile;
        private ManualResetEventSlim _stopEvent;
        

        public RejectedFlashLogger(string folder, string baseFileName, int minutesPerFile, ManualResetEventSlim stopEvent)
        {

            _baseFileName = baseFileName;
            _minutesPerFile = minutesPerFile;
            _stopEvent = stopEvent;

            _portionsToWrite = new ConcurrentQueue<LTGFlashPortion>();

            _directory = FileHelper.CreateDirectory(folder);
        }

        private void AddPortion(LTGFlashPortion portion)
        {
            _portionsToWrite.Enqueue(portion);
            // This controls the thread that manages the queue, make sure we only launch one thread 
            if (0 == Interlocked.CompareExchange(ref _writerLock, 1, 0))
            {
                Task.Factory.StartNew(WritesPortions);
            }
        }

        private void AddFlash(LTGFlash flash, string reason)
        {
            foreach (LTGFlashPortion portion in flash.FlashPortionList)
            {
                portion.RejectedReason = reason;
                Write(portion);
            }
        }

        private async Task WritesPortions()
        {
            LTGFlashPortion portion;
            string currentFileName = null;
            FileStream fileStream = null;
            UTF8Encoding utf8 = new UTF8Encoding();
            
            Portion p = null;
            try
            {
                while (_portionsToWrite.TryDequeue(out portion) && !_stopEvent.Wait(0))
                {
                    if (portion != null)
                    {
                        string fileName = GetFullFileName(portion.TimeStamp.BaseTime);
                        if (fileName != null)
                        {
                            if (fileStream == null || currentFileName != fileName)
                            {
                                if (fileStream != null)
                                {
                                    FinalizeFileStream(fileStream);
                                }

                                fileStream = GetFileStream(fileName);
                                currentFileName = fileName;
                            }

                            p = new Portion(portion, null, true);
                            byte[] b = utf8.GetBytes(JsonConvert.SerializeObject(p) + Environment.NewLine);
                            await fileStream.WriteAsync(b, 0, b.Length);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventManagerOffset + 2, "An error occured while writing the file in RejectedFlashLogger", ex);
            }
            finally
            {
                if (fileStream != null)
                {
                    FinalizeFileStream(fileStream);
                }
                Interlocked.Decrement(ref _writerLock);
            }
        }

        private void FinalizeFileStream(FileStream fs)
        {
            try
            {
                fs.Close();
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventManagerOffset + 2, "An error occured while finalizing the FileStream in RejectedFlashLogger", ex);
            }

        }

        private FileStream GetFileStream(string fileName)
        {
            FileStream fs;

            if (File.Exists(fileName))
            {
                fs = new FileStream(fileName, FileMode.Open, FileAccess.ReadWrite);
                fs.Seek(0, SeekOrigin.End);

            }
            else
            {
                fs = new FileStream(fileName, FileMode.CreateNew, FileAccess.ReadWrite);
            }

            return fs;
        }

        private string GetFullFileName(DateTime utcTime)
        {
            string fileName = null;
            string d = string.Format("\\{0:yyyy-MM-dd}\\", utcTime);
            string dirPath = _directory.FullName + d;

            DirectoryInfo di = FileHelper.CreateDirectory(dirPath);
            if (di != null)
            {
                int min = (utcTime.Minute / _minutesPerFile) * _minutesPerFile;
                string f = string.Format("{0:D4}-{1:D2}-{2:D2}T{3:D2}-{4:D2}", utcTime.Year, utcTime.Month, utcTime.Day, utcTime.Hour, min);
                fileName = string.Format(_baseFileName, f);
                fileName = Path.Combine(di.FullName, fileName);
            }

            return fileName;
        }


        private static RejectedFlashLogger _logger;

        static RejectedFlashLogger()
        {
            _logger = null;
        }

        public static void Create(string folder, string baseFileName, int minutesPerFile, ManualResetEventSlim stopEvent)
        {
            _logger = new RejectedFlashLogger(folder, baseFileName, minutesPerFile, stopEvent);
        }

        public static void Write(LTGFlashPortion portion)
        {
            if (_logger != null)
            {
                _logger.AddPortion(portion);
            }   
        }

        public static void WriteFlash(LTGFlash flash, string reason)
        {
            if (_logger != null)
            {
                _logger.AddFlash(flash, reason);
            }
        }

        public static void WriteFlash(LTGFlash flash)
        {
            if (_logger != null)
            {
                foreach (LTGFlashPortion portion in flash.FlashPortionList)
                {
                    _logger.AddPortion(portion);
                }
            }
        }

    }
}
