﻿using System;
using System.Globalization;
using System.IO;
using System.Text;

using Newtonsoft.Json.Linq;

using Aws.Core.Data.Common.Operations;
using Aws.Core.Utilities;

using LxCommon.Monitoring;
using LxCommon.TcpCommunication;

using LxDataManager.Common;

namespace LxDataManager.Process
{
    public class MonitoringHandler
    {
        private bool _isTln;
        private readonly string _monitorHmtl;
        private Listener _processorListener;
        private Responder _datafeedResponder;
        private Sender _managerSender;
        private FlashValidatorProcessor _flashValidatorProcessor;
        private IncomingFlashProcessor _incomingFlashProcessor;
        private PortionGroupingProcessor _portionGroupingProcessor;
        private ArchivingProcessor _archivingProcessor;

        public MonitoringHandler(bool isTln, Listener processorListener, Responder datafeedResponder, Sender managerSender, FlashValidatorProcessor flashValidatorProcessor, 
            IncomingFlashProcessor incomingFlashProcessor, PortionGroupingProcessor flashPortionGrouperProcessor, ArchivingProcessor archivingProcessor)
        {
            _isTln = isTln;
            _processorListener = processorListener;
            _datafeedResponder = datafeedResponder;
            _managerSender = managerSender;
            _flashValidatorProcessor = flashValidatorProcessor;
            _incomingFlashProcessor = incomingFlashProcessor;
            _portionGroupingProcessor = flashPortionGrouperProcessor;
            _archivingProcessor = archivingProcessor;

            try
            {
                _monitorHmtl = File.ReadAllText(AppUtils.AppLocation + @"\Monitor.html", Encoding.UTF8);
            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.TidMonitorPageUnavailable, "Unable to load monitor html file", ex);
                _monitorHmtl = "Unavailable";
            }
        }

        public void Start()
        {
            try
            {
                OpsCommandHttpEndpointSvc.Start((cmd) => MonitorStatusCheck(cmd));
            }
            catch (Exception ex)
            {
                EventManager.LogError(0, "Unable to start ecv http endpoint", ex);
            }
        }

        private string MonitorStatusCheck(string cmd)
        {
            if (cmd.ToUpper().Equals("PRTG"))
            {
                return GeneratePrtgStatus();
            }
            else
            {
                return GenerateHtmlStatus(_isTln, _monitorHmtl);
            }

        }

        private const string Success = "<span style=\"color: green;\">SUCCESS</span>";
        private const string Failure = "<span style=\"color: red;\">FAIL</span>";
        private const string NA = "-";

        private string GenerateHtmlStatus(bool isTln, string monitorHmtl)
        {
            string html = string.Empty;

            string success = Success;
            string failure = Failure;
            if (!isTln)
            {
                success = NA;
                failure = NA;
            }

            CultureInfo enUs = new CultureInfo("en-US");
            DateTime utcNow = DateTime.UtcNow;

            double processorPacketRate = 0.0;
            int processorConnections = 0;
            double processorConnectionRate = 0.0;
            if (_processorListener != null)
            {
                processorPacketRate = _processorListener.GetIncomingRatePerSecond(utcNow);
                processorConnections = _processorListener.Connections;
                processorConnectionRate = _processorListener.GetConnectionRatePerSecond(utcNow);
            }

            int datafeedConnections = 0;
            double datafeedConnectionRate = 0.0;
            if (_datafeedResponder != null)
            {
                datafeedConnections = _datafeedResponder.Connections;
                datafeedConnectionRate = _datafeedResponder.GetConnectionRatePerSecond(utcNow);
            }

            int managerConnections = 0;
            int managerActiveConnections = 0;
            if (_managerSender != null)
            {
                managerConnections = _managerSender.Connections;
                managerActiveConnections = _managerSender.ActiveConnections;
            }

            double minTlnAge = 0, maxTlnAge = 0, medianTlnAge = 0;
            double minWwllnAge = 0, maxWwllnAge = 0, medianWwllnAge = 0;
            int incomingCount = 0, incomingActiveTasks = 0, incomingMaxTasks = 0;
            if (_incomingFlashProcessor != null)
            {
                Tuple<double, double, double> tln = _incomingFlashProcessor.TlnMinMaxMedian;
                minTlnAge = tln.Item1;
                maxTlnAge = tln.Item2;
                medianTlnAge = tln.Item3;

                Tuple<double, double, double> wwlln = _incomingFlashProcessor.WwllnMinMaxMedian;
                minWwllnAge = wwlln.Item1;
                maxWwllnAge = wwlln.Item2;
                medianWwllnAge = wwlln.Item3;

                incomingCount = _incomingFlashProcessor.Count;
                incomingActiveTasks = _incomingFlashProcessor.ActiveCommands;
                incomingMaxTasks = _incomingFlashProcessor.MaxActiveWorkItems;
            }

            int goupingCount = 0, groupedCount = 0, ageLastGroupedFlash = 0, activeGroupingTasks = 0, maxGroupingTasks = 0;
            if (_portionGroupingProcessor != null)
            {
                goupingCount = _portionGroupingProcessor.Count;
                groupedCount = _portionGroupingProcessor.GroupedCount;
                ageLastGroupedFlash = (int)(utcNow - _portionGroupingProcessor.LastFlashTime).TotalSeconds;
                activeGroupingTasks = _portionGroupingProcessor.ActiveCommands;
                maxGroupingTasks = _portionGroupingProcessor.MaxActiveWorkItems;
            }

            double flashAgeMin = 0, flashAgeMax = 0, flashAgeMedian = 0;
            int validtorCount = 0, validtorActiveTasks = 0, validtorMaxTasks = 0;
            if (_flashValidatorProcessor != null)
            {
                Tuple<double, double, double> flashAges = _flashValidatorProcessor.FlashAgeMinMaxMedian;
                flashAgeMin = flashAges.Item1;
                flashAgeMax = flashAges.Item2;
                flashAgeMedian = flashAges.Item3;

                validtorCount = _flashValidatorProcessor.Count;
                validtorActiveTasks = _flashValidatorProcessor.ActiveCommands;
                validtorMaxTasks = _flashValidatorProcessor.MaxActiveWorkItems;

            }

            int archiveCount = 0, archivingTasks = 0, archivingMaxTasks = 0;
            if (_archivingProcessor != null)
            {
                archiveCount = _archivingProcessor.Count;
                archivingTasks = _archivingProcessor.ActiveCommands;
                archivingMaxTasks = _archivingProcessor.MaxActiveWorkItems;
            }

            try
            {
                html = string.Format(monitorHmtl,
                        AppUtils.GetVersion(), //0
                        utcNow.ToString(enUs),

                        (processorConnections >= Config.MonitorProcessorActiveConnectionsMinimum) ? success : failure,
                        processorConnections.ToString("n0"),

                        (processorPacketRate >= Config.MonitorProcessorPacketRateMinimum) ? success : failure,
                        processorPacketRate.ToString("##,0.0"),

                        (processorConnectionRate <= Config.MonitorProcessorConnectionRateMaximum) ? success : failure,
                        processorConnectionRate.ToString("##,0.0"),

                        (datafeedConnections >= Config.MonitorDatafeedConnectionsMinimum) ? success : failure,
                        datafeedConnections.ToString("n0"),

                        (datafeedConnectionRate <= Config.MonitorDatafeedConnectionRateMaximum) ? success : failure,
                        datafeedConnectionRate.ToString("##,0.0"),

                        (managerConnections == managerActiveConnections) ? success : failure,
                        managerActiveConnections.ToString("n0"),

                        minTlnAge.ToString("##,0.00"), maxTlnAge.ToString("##,0.00"), medianTlnAge.ToString("##,0.00"),
                        minWwllnAge.ToString("##,0.00"), maxWwllnAge.ToString("##,0.00"), medianWwllnAge.ToString("##,0.00"),
                        flashAgeMin.ToString("##,0.00"), flashAgeMax.ToString("##,0.00"), flashAgeMedian.ToString("##,0.00"),

                        incomingCount,
                        incomingActiveTasks,
                        incomingMaxTasks,
                        
                        goupingCount,
                        groupedCount,
                        ageLastGroupedFlash,
                        activeGroupingTasks,
                        maxGroupingTasks,

                        validtorCount,
                        validtorActiveTasks,
                        validtorMaxTasks,

                        archiveCount,
                        archivingTasks,
                        archivingMaxTasks);
            }
            catch (Exception ex)
            {
                EventManager.LogError(1, "Unable to create status", ex);
            }
            

            return html;
        }

        private string GeneratePrtgStatus()
        {
            DateTime utcNow = DateTime.UtcNow;
            JObject j = new JObject();

            double processorPacketRate = 0.0;
            int processorConnections = 0;
            double processorConnectionRate = 0.0;
            if (_processorListener != null)
            {
                processorPacketRate = _processorListener.GetIncomingRatePerSecond(utcNow);
                processorPacketRate = Math.Round(processorPacketRate, 2, MidpointRounding.AwayFromZero);
                processorConnections = _processorListener.Connections;
                processorConnectionRate = _processorListener.GetConnectionRatePerSecond(utcNow);
                processorConnectionRate = Math.Round(processorConnectionRate, 2, MidpointRounding.AwayFromZero);
                
                
            }
            j.Add("processorPacketRate", processorPacketRate);
            j.Add("processorConnections", processorConnections);
            j.Add("processorConnectionRate", processorConnectionRate);

            int validationQueueLength = 0;
            if (_incomingFlashProcessor != null)
            {
                validationQueueLength = _incomingFlashProcessor.Count;
            }
            j.Add("validationQueueLength", validationQueueLength);
            

            int datafeedConnections = 0;
            double datafeedConnectionRate = 0.0;
            if (_datafeedResponder != null)
            {
                datafeedConnections = _datafeedResponder.Connections;
                datafeedConnectionRate = _datafeedResponder.GetConnectionRatePerSecond(utcNow);
                datafeedConnectionRate = Math.Round(datafeedConnectionRate, 2, MidpointRounding.AwayFromZero);
            }
            j.Add("datafeedConnections", datafeedConnections);
            j.Add("datafeedConnectionRate", datafeedConnectionRate);
            

            int managerActiveConnections = 0;
            if (_managerSender != null)
            {
                managerActiveConnections = _managerSender.ActiveConnections;
            }
            j.Add("managerActiveConnections", managerActiveConnections);

            int groupingIncomingCount = 0, groupingGroupedCount = 0;
            if (_portionGroupingProcessor != null)
            {
                groupingIncomingCount = _portionGroupingProcessor.Count;
                groupingGroupedCount = _portionGroupingProcessor.GroupedCount;
            }
            j.Add("groupingIncomingCount", groupingIncomingCount);
            j.Add("groupingGroupedCount", groupingGroupedCount);

            int archiveQueueLength = 0;
            double minTlnAge = 0, maxTlnAge = 0, medianTlnAge = 0;
            double minWwllnAge = 0, maxWwllnAge = 0, medianWwllnAge = 0;
            if (_incomingFlashProcessor != null)
            {
                Tuple<double, double, double> tln = _incomingFlashProcessor.TlnMinMaxMedian;
                minTlnAge = tln.Item1;
                maxTlnAge = tln.Item2;
                medianTlnAge = tln.Item3;

                Tuple<double, double, double> wwlln = _incomingFlashProcessor.WwllnMinMaxMedian;
                minWwllnAge = wwlln.Item1;
                maxWwllnAge = wwlln.Item2;
                medianWwllnAge = wwlln.Item3;
            }

            j.Add("minTlnAgeSeconds", Math.Round(minTlnAge, 2, MidpointRounding.AwayFromZero));
            j.Add("maxTlnAgeSeconds", Math.Round(maxTlnAge, 2, MidpointRounding.AwayFromZero));
            j.Add("medianTlnAgeSeconds", Math.Round(medianTlnAge, 2, MidpointRounding.AwayFromZero));
            j.Add("minWwllnAgeSeconds", Math.Round(minWwllnAge, 2, MidpointRounding.AwayFromZero));
            j.Add("maxWwllnAgeSeconds", Math.Round(maxWwllnAge, 2, MidpointRounding.AwayFromZero));
            j.Add("medianWwllnAgeSeconds", Math.Round(medianWwllnAge, 2, MidpointRounding.AwayFromZero));
            j.Add("archiveQueueLength", archiveQueueLength);

            return j.ToString();
        }

        public void Stop()
        {
            try
            {
                OpsCommandHttpEndpointSvc.Stop();
            }
            catch (Exception ex)
            {
                EventManager.LogError(1, "Unable to stop ecv http endpoint", ex);
            }

        }
    }
}
