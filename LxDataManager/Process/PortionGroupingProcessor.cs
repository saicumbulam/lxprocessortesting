﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

using Aws.Core.Utilities;

using LxCommon;
using LxCommon.DataStructures;
using LxCommon.Models;

using LxDataManager.Models;

namespace LxDataManager.Process
{
    public class PortionGroupingSettings
    {
        public double MinimumTlnGroupDistanceKm { get; set; }
        public double MinimumGroupTimeSeconds { get; set; }
        public double MinimumWwllnGroupDistanceKm { get; set; }
        public int GrouperTimeDelayInMillisecond { get; set; }
        public int SendCheckIntervalMilliseconds { get; set; }
    }

    public class PortionGroupingProcessor : ParallelQueueProcessor<LTGFlashPortion>
    {
        private FlashValidatorProcessor _validator;
        private List<GrouperFlash> _grouperFlashes;
        private ReaderWriterLockSlim _grouperFlashesLock;
        
        private long _minimumGroupTimeNanoSeconds;
        
        private Timer _sendTimer;
        private ConcurrentDictionary<string, StationData> _stationData;

        private DateTime _lastFlashTime;
        private readonly PortionGroupingSettings _settings;
        private readonly PortionDeduper _portionDeduper;

        public PortionGroupingProcessor(FlashValidatorProcessor validator, ConcurrentDictionary<string, StationData> stationData, PortionDeduper portionDeduper, PortionGroupingSettings settings,
            ManualResetEventSlim stopEvent, int maxWorkItems) : base(stopEvent, maxWorkItems)
        {
            _validator = validator;
            _stationData = stationData;
            _settings = settings;

            _portionDeduper = portionDeduper;

            _minimumGroupTimeNanoSeconds = (long)(_settings.MinimumGroupTimeSeconds * DateTimeUtcNano.NanosecondsPerSecond);

            _sendTimer = new Timer(SendFlashes, null, TimeSpan.FromMilliseconds(_settings.SendCheckIntervalMilliseconds), TimeSpan.FromMilliseconds(-1));

            _grouperFlashes = new List<GrouperFlash>();
            _grouperFlashesLock = new ReaderWriterLockSlim();
            
            _lastFlashTime = DateTime.UtcNow;
        }

        protected override void DoWork(LTGFlashPortion portion)
        {
            bool foundCanidate = false;
            int index = 0;
            int indexOfBest = -1;

            _grouperFlashesLock.EnterReadLock();
            try
            {
                double shortestDistance = double.MaxValue;
                while (index < _grouperFlashes.Count)
                {
                    double distance;
                    if (_grouperFlashes[index].IsCanidate(portion, out distance))
                    {
                        if (distance < shortestDistance)
                        {
                            shortestDistance = distance;
                            indexOfBest = index;
                        }
                        foundCanidate = true;
                    }
                    index++;
                }
                
                if (foundCanidate)
                {
                    _grouperFlashes[indexOfBest].AddPortion(portion);
                }
            }
            catch (Exception ex)
            {
                EventManager.LogWarning(0, "Failure trying to group a portion.", ex);
            }
            finally
            {
                _grouperFlashesLock.ExitReadLock();
            }
                
            
            if (!foundCanidate)
            {
                GrouperFlash gp = new GrouperFlash(portion, _settings.MinimumTlnGroupDistanceKm, _settings.MinimumWwllnGroupDistanceKm, _minimumGroupTimeNanoSeconds, 
                    _settings.GrouperTimeDelayInMillisecond, DateTime.UtcNow);
                _grouperFlashesLock.EnterWriteLock();
                try
                {
                    _grouperFlashes.Add(gp);
                }
                finally
                {
                    _grouperFlashesLock.ExitWriteLock();
                }
            }
        }

        private void SendFlashes(object state)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            int index = 0;
            List<LTGFlash> flashesToSend = new List<LTGFlash>();
            _grouperFlashesLock.EnterUpgradeableReadLock();
            try
            {
                while (index < _grouperFlashes.Count && !_stopEvent.Wait(0))
                {
                    try
                    {
                        if (_grouperFlashes[index].IsTimeToSend(DateTime.UtcNow))
                        {
                            _grouperFlashesLock.EnterWriteLock();
                            try
                            {
                                flashesToSend.Add(_grouperFlashes[index].Flash);
                                _grouperFlashes.RemoveAt(index);
                            }
                            finally
                            {
                                _grouperFlashesLock.ExitWriteLock();
                            }
                        }
                        else
                        {
                            index++;
                        }
                    }
                    catch (Exception ex)
                    {
                        EventManager.LogError(0, "Error in SendFlashes", ex);
                    }
                }
            }
            finally
            {
                _grouperFlashesLock.ExitUpgradeableReadLock();
            }

            if (flashesToSend.Count > 0)
            {
                foreach (LTGFlash flash in flashesToSend)
                {
                    FinalUpdateFlash(flash);
                    _validator.Add(flash);
                }
            }

            sw.Stop();
            if (!_stopEvent.Wait(0))
            {
                if (sw.ElapsedMilliseconds >= _settings.SendCheckIntervalMilliseconds)
                {
                    SendFlashes(null);
                }
                else
                {
                    _sendTimer.Change(TimeSpan.FromMilliseconds(_settings.SendCheckIntervalMilliseconds - sw.ElapsedMilliseconds), TimeSpan.FromMilliseconds(-1));
                }
                
            }
        }
        
        private LTGFlash FinalUpdateFlash(LTGFlash flash)
        {
            try
            {

                flash.FlashPortionList.Sort();
                RemoveFlashPortionsTooCloseInTime(flash.FlashPortionList);
                flash = _portionDeduper.RemoveDuplicates(flash);
                if (flash != null)
                {
                    flash.FinalizeFlash(_stationData, false);
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(0, "Error finalizing flash", ex);
            }

            return flash;
        }
        
        private void RemoveFlashPortionsTooCloseInTime(ArrayList flashPortionList)
        {
            int i = 0;
            // Remove portions that are within 1 ms 
            while (i <= flashPortionList.Count - 2)
            {
                LTGFlashPortion curPortion = (LTGFlashPortion)flashPortionList[i];
                LTGFlashPortion nextPortion = (LTGFlashPortion)flashPortionList[i + 1];

                if (curPortion != null && nextPortion != null)
                {
                    double timeDifference = (double)Math.Abs(curPortion.TimeStamp.Nanoseconds - nextPortion.TimeStamp.Nanoseconds) / DateTimeUtcNano.NanosecondsPerSecond;
                    
                    if (timeDifference < LtgConstants.MinGapOfFlashPortionInSecond)
                    {
                        int indexToRemove = PickBetterFlash(curPortion, nextPortion, i);
                        LTGFlashPortion removed, kept;
                        if (i == indexToRemove)
                        {
                            removed = curPortion;
                            kept = nextPortion;
                        }
                        else
                        {
                            removed = nextPortion;
                            kept = curPortion;
                        }

                        removed.RejectedReason = $"portion to close in time to portion already in flash, kept {kept.FlashTime}: ({kept.Latitude}, {kept.Longitude})";
                        RejectedFlashLogger.Write(removed);
                        flashPortionList.RemoveAt(indexToRemove);
                        continue;
                    }
                }
                i++;
            }
        }

        private int PickBetterFlash(LTGFlashPortion curPortion, LTGFlashPortion nextPortion, int index)
        {
            int removeIndex;

            //favor tln data over wwlln data
            if ((curPortion.FlashType == FlashType.FlashTypeCG || curPortion.FlashType == FlashType.FlashTypeIC) &&
                (nextPortion.FlashType == FlashType.FlashTypeGlobalCG || nextPortion.FlashType == FlashType.FlashTypeGlobalIC))
            {
                removeIndex = index + 1;
            }
            else if ((curPortion.FlashType == FlashType.FlashTypeGlobalCG || curPortion.FlashType == FlashType.FlashTypeGlobalIC) &&
                (nextPortion.FlashType == FlashType.FlashTypeCG || nextPortion.FlashType == FlashType.FlashTypeIC))
            {
                removeIndex = index;
            }
            else if (curPortion.OffsetList != null && nextPortion.OffsetList != null && curPortion.OffsetList.Count > 0 && nextPortion.OffsetList.Count > 0 
                && curPortion.OffsetList[0].DistanceToFlash > nextPortion.OffsetList[0].DistanceToFlash)
            {
                removeIndex = index;
            }
            else
            {
                removeIndex = index + 1;
            }

            return removeIndex;
        }

        public void Stop()
        {
            if (_sendTimer != null)
            {
                _sendTimer.Dispose();
            }
        }

        public DateTime LastFlashTime
        {
            get { return _lastFlashTime; }
        }

        public int GroupedCount
        {
            get { return _grouperFlashes.Count; }
        }

       
    }
}
