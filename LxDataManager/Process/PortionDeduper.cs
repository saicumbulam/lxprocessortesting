﻿using System.Collections.Generic;

using LxCommon;
using LxCommon.Models;

namespace LxDataManager.Process
{
    public class PortionDeduper
    {
        private readonly SortedSet<LTGFlashPortion> _recentPortions;
        private readonly double _duplicateDistanceKm;
        private readonly long _duplicateTimeNanoseconds;
        private readonly long _duplicateBufferNanoseconds;
        private readonly object _lock;
        readonly LTGFlashPortion _oldestPortion;

        public PortionDeduper(double duplicateDistanceKm, double duplicateTimeSeconds, int duplicateBufferSeconds)
        {
            _recentPortions = new SortedSet<LTGFlashPortion>();

            _duplicateDistanceKm = duplicateDistanceKm;
            _duplicateTimeNanoseconds = (long)(duplicateTimeSeconds * DateTimeUtcNano.NanosecondsPerSecond);
            _duplicateBufferNanoseconds = duplicateBufferSeconds * DateTimeUtcNano.NanosecondsPerSecond;
            
            DateTimeUtcNano o = DateTimeUtcNano.MinValue;
            _oldestPortion = new LTGFlashPortion() { TimeStamp = o };

            _lock = new object();
        }

        public LTGFlash RemoveDuplicates(LTGFlash flash)
        {
            if (flash != null && flash.FlashPortionList != null && flash.FlashPortionList.Count > 0)
            {
                int count = 0;
                LTGFlashPortion portion = null;
                lock (_lock)
                {
                    while (count < flash.FlashPortionList.Count)
                    {
                        bool didRemove = false;
                        portion = flash.FlashPortionList[count] as LTGFlashPortion;
                        if (portion != null)
                        {
                            DateTimeUtcNano l = new DateTimeUtcNano(portion.TimeStamp.Nanoseconds - _duplicateTimeNanoseconds);
                            LTGFlashPortion lower = new LTGFlashPortion() { TimeStamp = l };
                            DateTimeUtcNano u = new DateTimeUtcNano(portion.TimeStamp.Nanoseconds + _duplicateTimeNanoseconds);
                            LTGFlashPortion upper = new LTGFlashPortion() { TimeStamp = u };

                            SortedSet<LTGFlashPortion> canidates = _recentPortions.GetViewBetween(lower, upper);
                            if (canidates.Count > 0)
                            {
                                foreach (LTGFlashPortion canidate in canidates)
                                {
                                    double dist = StationData.DistanceInKM(canidate.Latitude, canidate.Longitude, 0, portion.Latitude, portion.Longitude, 0);
                                    if (dist < _duplicateDistanceKm)
                                    {
                                        flash.FlashPortionList.RemoveAt(count);
                                        didRemove = true;
                                        long diff = portion.TimeStamp.Nanoseconds - canidate.TimeStamp.Nanoseconds;

                                        portion.RejectedReason = $"duplicate found: ({canidate.TimeStamp}, {canidate.Latitude}, {canidate.Longitude}) time: {diff}ns distance: {dist}km";
                                        RejectedFlashLogger.Write(portion);
                                        break;
                                    }
                                }
                            }
                        }
                        else
                        {
                            flash.FlashPortionList.RemoveAt(count);
                        }

                        if (!didRemove && portion != null)
                        {
                            _recentPortions.Add(portion);
                            count++;
                        }
                    }

                    if (portion != null)
                    {
                        CleanupPortions(_recentPortions, _oldestPortion, portion, _duplicateBufferNanoseconds);
                    }
                }

                if (flash.FlashPortionList == null || flash.FlashPortionList.Count < 1)
                {
                    flash = null;
                }
            }

            return flash;
        }

        private void CleanupPortions(SortedSet<LTGFlashPortion> recentPortions, LTGFlashPortion oldestPortion, LTGFlashPortion portion, long duplicateBufferNanoseconds)
        {
            DateTimeUtcNano u = new DateTimeUtcNano(portion.TimeStamp.Nanoseconds - duplicateBufferNanoseconds);
            LTGFlashPortion upper = new LTGFlashPortion() { TimeStamp = u };
            SortedSet<LTGFlashPortion> trash = recentPortions.GetViewBetween(oldestPortion, upper);
            //since this is a view it will clear these out of the underlying set as well
            trash.Clear();
        }
    }
}
