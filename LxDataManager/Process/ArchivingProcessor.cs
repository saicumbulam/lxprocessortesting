﻿using System;
using System.Configuration;
using System.Threading;

using Aws.Core.Utilities;

using LxCommon;
using LxCommon.DataStructures;
using LxCommon.Monitoring;
using LxCommon.Utilities;

using LxDataManager.Common;
using LxDataManager.Models;
using LxDataManager.Services;

namespace LxDataManager.Process
{
    public class ArchivingProcessor : ParallelQueueProcessor<LTGFlash>
    {
        private PrimaryDatabaseGateway _primary;
        private EtlDatabaseGateway _etl;

        private bool _isPrimaryDbEnabled;
        private bool _isEtlDbEnabled;
        private SensorDetectionStatistics _sensorDetectionStatistics;
        private FlashDataLogger _fdl = null;
        private int _maxQueueSize;
        private MedianSampler<double> _samples;

        private const ushort EventManagerOffset = TaskMonitor.ManagerArchiveProcessor;

        public ArchivingProcessor(bool isPrimaryDbEnabled, bool isEtlDbEnabled, SensorDetectionStatistics sensorDetectionStatistics, int medianSampleSize, int maxQueueSize, ManualResetEventSlim stopEvent, int maxActiveWorkItems)
            : base(stopEvent, maxActiveWorkItems)
        {
            _isPrimaryDbEnabled = isPrimaryDbEnabled;
            if (_isPrimaryDbEnabled)
            {
                _primary = new PrimaryDatabaseGateway(ConfigurationManager.ConnectionStrings["Primary"].ConnectionString);
            }

            _isEtlDbEnabled = isEtlDbEnabled;
            if (_isEtlDbEnabled)
            {
                _etl = new EtlDatabaseGateway(ConfigurationManager.ConnectionStrings["Etl"].ConnectionString);
            }

            _sensorDetectionStatistics = sensorDetectionStatistics;

            _maxQueueSize = maxQueueSize;

            bool isTlnSystem = Config.SystemType.ToUpper().Equals("TLN");

            _samples = new MedianSampler<double>(medianSampleSize);

            if (Config.ExportFlash)
            {
                _fdl = new FlashDataLogger(Config.ExportFileDirectory, "Manager Flashes {0}.json", isTlnSystem, 10);
            }
            
        }

        /// <summary>
        /// Adds a flash to the archiving queue and also checks the size of the queue and dumps excess flashes. Use this method instead of Add
        /// </summary>
        /// <param name="flash">Flash to be added the archive queue</param>
        public override void Add(LTGFlash flash)
        {
            if (flash != null && flash.FlashPortionList != null && flash.FlashPortionList.Count > 0)
            {
                double age = (DateTime.UtcNow - flash.TimeStamp.BaseTime).TotalSeconds;

                _samples.AddSample(age);

                if (_queue.Count > _maxQueueSize)
                {
                    EventManager.LogWarning(EventManagerOffset, "Archiving queue greater than max size, dumping flashes from queue");
                    while (_queue.Count > _maxQueueSize)
                    {
                        LTGFlash f;
                        if (_queue.TryDequeue(out f))
                        {
                            if (_isPrimaryDbEnabled)
                            {
                                _primary.AddFlashToFailureLog(f);
                            }

                            if (_isEtlDbEnabled)
                            {
                                _etl.AddFlashToFailureLog(f);
                            }
                        }
                    }
                }

                base.Add(flash);
            }
        }

        protected override void DoWork(LTGFlash flash)
        {
            if (_isPrimaryDbEnabled)
            {
                _primary.InsertFlash(flash);
            }

            if (_isEtlDbEnabled)
            {
                _etl.InsertFlash(flash);
            }

            if (_sensorDetectionStatistics != null)
            {
                _sensorDetectionStatistics.Update(flash);
            }

            if (_fdl != null)
            {
                DateTime ft = LtgTimeUtils.GetUtcDateTimeFromString(flash.FlashTime);
                _fdl.WriteFlash(flash, ft);
            }
        }

        public Tuple<double, double, double> FlashMinMaxMedian
        {
            get { return _samples.GetMinMaxMedian(); }
        }
    }
}
