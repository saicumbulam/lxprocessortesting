﻿using System;
using System.Threading;

using Aws.Core.Utilities;

using LxCommon;
using LxCommon.DataStructures;
using LxCommon.Models;
using LxCommon.Utilities;
using LxDataManager.Models;

namespace LxDataManager.Process
{
    public class FlashValidatorSettings
    {
        public int RegionCleanupIntervalMinutes { get; set; }
        public short StrongAmplitude { get; set; }
        public short WeakAmplitude { get; set; }
        public int CloseStationDistanceKm { get; set; }
        public int MinimumPortionsForValidFlash { get; set; }
        public int MedianSampleSize { get; set; }
    }
    

    public class FlashValidatorProcessor : ParallelQueueProcessor<LTGFlash>, IDisposable
    {
        private GeoBoundingArea[] _tlnAreas;
        private GeoBoundingArea _wwllnGlobalArea;

        private Timer _regionCleaner;
        private DateTime _lastFlashTime;

        private FlashValidatorSettings _settings;
        private Controller _controller;
        private ArchivingProcessor _archiver;

        private MedianSampler<double> _flashAges;

        public FlashValidatorProcessor(Controller controller, ArchivingProcessor archiver, GeoBoundingArea[] tlnAreas, GeoBoundingArea wwllnGlobalArea, FlashValidatorSettings settings, ManualResetEventSlim stopEvent, int maxActiveWorkItems)
            : base(stopEvent, maxActiveWorkItems)
        {
            _controller = controller;

            _archiver = archiver;


            _tlnAreas = tlnAreas;
            _wwllnGlobalArea = wwllnGlobalArea;

            _settings = settings;

            _flashAges = new MedianSampler<double>(_settings.MedianSampleSize);

            _regionCleaner = new Timer(CleanupRegion, null, TimeSpan.FromMinutes(_settings.RegionCleanupIntervalMinutes), TimeSpan.FromMinutes(_settings.RegionCleanupIntervalMinutes));

            //need to set this a little ahead of the min value so the cleanup methods will work
            _lastFlashTime = DateTimeUtcNano.MinValue.BaseTime.AddDays(1);
        }

        protected override void DoWork(LTGFlash flash)
        {
            if (flash != null && flash.FlashPortionList != null && flash.FlashPortionList.Count > 0)
            {
                bool isValid = false;
                GeoBoundingArea area = FindArea(flash, _tlnAreas, _wwllnGlobalArea);
                if (area != null)
                {
                    isValid = (flash.FlashPortionList.Count >= _settings.MinimumPortionsForValidFlash);
                    if (!isValid)
                    {
                        isValid = ContainsTlnAndWwlln(flash);
                        if (!isValid)
                        {
                            isValid = ConntainsStrongCloseOffset(flash, _settings.StrongAmplitude, _settings.CloseStationDistanceKm);
                            if (!isValid)
                            {
                                isValid = DoBuddyCheck(flash, area);
                                if (!isValid)
                                {
                                    isValid = MeetsStrictConfidenceCheck(flash, area, _settings.WeakAmplitude);
                                }
                            }
                        }
                    }
                    
                    area.Flashes.AddFlash(flash);
                }
                
                if (isValid)
                {
                    SendOrAchiveFlash(flash, _controller, _archiver);
                }
                else
                {
                    RejectedFlashLogger.WriteFlash(flash);
                }
            }
        }

       

        private bool ContainsTlnAndWwlln(LTGFlash flash)
        {
            bool contains = false;
            int tlnCount = 0;
            int wwllnCount = 0;
            foreach (LTGFlashPortion portion in flash.FlashPortionList)
            {
                if (portion.IsWwlln)
                {
                    wwllnCount++;
                    if (tlnCount > 0)
                    {
                        contains = true;
                        break;
                    }
                }
                else
                {
                    tlnCount++;
                    if (wwllnCount > 0)
                    {
                        contains = true;
                        break;
                    }
                }
            }
            return contains;
        }

        private bool ConntainsStrongCloseOffset(LTGFlash flash, short strongAmplitude, int closeStationDistanceKm)
        {
            bool isStrongClose = false;

            foreach (LTGFlashPortion portion in flash.FlashPortionList)
            {
                if (portion.FlashType == FlashType.FlashTypeCG && portion.OffsetList != null && portion.OffsetList.Count > 0)
                {
                    if (Math.Abs(portion.OffsetList[0].AbsPeakAmplitude) >= strongAmplitude && portion.OffsetList[0].DistanceToFlash > 0
                        && portion.OffsetList[0].DistanceToFlash <= closeStationDistanceKm)
                    {
                        isStrongClose = true;
                        break;
                    }
                }
            }
            
            return isStrongClose;
        }

        private GeoBoundingArea FindArea(LTGFlash flash, GeoBoundingArea[] tlnAreas, GeoBoundingArea wwllnGlobalArea)
        {
            GeoBoundingArea gba = wwllnGlobalArea;
            
            if (tlnAreas != null)
            {
                foreach (GeoBoundingArea area in tlnAreas)
                {
                    if (area.Flashes.Contains(flash.Latitude, flash.Longitude))
                    {
                        gba = area;
                        break;
                    }
                }
            }
            
            return gba;
        }

        private bool DoBuddyCheck(LTGFlash flash, GeoBoundingArea area)
        {
            bool isValid = false;
            FlashQuadTree fqt = area.Flashes.FindLeaf(flash.Latitude, flash.Longitude);
            if (fqt != null)
            {
                LTGFlash[] neighborFlashes = fqt.GetRecentFlashes(flash.TimeStamp, area.BuddyCheckTimeSeconds);
                int neighborCount = 0;
                foreach (LTGFlash neighborFlash in neighborFlashes)
                {
                    if (StationData.DistanceInKM(neighborFlash.Latitude, neighborFlash.Longitude, 0,
                            flash.Latitude, flash.Longitude, 0) < area.BuddyCheckDistanceKm)
                    {
                        neighborCount++;
                        if (neighborCount >= area.BuddyCheckMinNumNeighbor)
                        {
                            isValid = true;
                            break;
                        }
                    }
                }
            }
            return isValid;
        }

        private bool MeetsStrictConfidenceCheck(LTGFlash flash, GeoBoundingArea area, short weakAmplitude)
        {
            bool isValid = false;
            
            foreach (LTGFlashPortion portion in flash.FlashPortionList)
            {
                if (Math.Abs(portion.Amplitude) > 0.0001 && Math.Abs(portion.Amplitude) < weakAmplitude)
                {
                    portion.RejectedReason = $"failed strict confidence, weak portion {portion.Amplitude} amps";
                }
                else
                {
                    if (portion.ConfidenceData == null || portion.OffsetList == null)
                    {
                        portion.RejectedReason =
                            $"failed strict confidence, no confidence data: {portion.ConfidenceData == null} no offsets: {portion.OffsetList == null}";
                    }
                    else
                    {
                        if (portion.ConfidenceData.BiggestAngle <= area.StrictBiggestAngle)
                        {
                            portion.RejectedReason =
                                $"failed strict confidence, angle too small {portion.ConfidenceData.BiggestAngle} <= {area.StrictBiggestAngle}";
                        }
                        else
                        {
                            if (portion.ConfidenceData.SensorDistribution <= area.StrictSensorDistribution)
                            {
                                portion.RejectedReason =
                                    $"failed strict confidence, sensor distribution too small {portion.ConfidenceData.SensorDistribution} <= {area.StrictSensorDistribution}";
                            }
                            else
                            {
                                if (portion.OffsetList.Count < area.StrictOffsetCount)
                                {
                                    portion.RejectedReason =
                                        $"failed strict confidence, not enough sensors {portion.OffsetList.Count} < {area.StrictOffsetCount}";
                                }
                                else
                                {
                                    if (portion.ConfidenceData.NearSensorConfidence < area.StrictNearSensorConfidence)
                                    {
                                        portion.RejectedReason =
                                            $"failed strict confidence, near sensor confidence too low {portion.ConfidenceData.NearSensorConfidence} < {area.StrictNearSensorConfidence}";
                                    }
                                    else
                                    {
                                        isValid = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return isValid;
        }
        
        private void SendOrAchiveFlash(LTGFlash flash, Controller controller, ArchivingProcessor archiver)
        {
            try
            {
                controller.SendFlash(flash);
                _flashAges.AddSample((DateTime.UtcNow - flash.TimeStamp.BaseTime).TotalSeconds);
                archiver.Add(flash);
                _lastFlashTime = flash.TimeStamp.BaseTime;
            }
            catch (Exception ex)
            {
                EventManager.LogError(0, "Error in SendOrAchiveFlash", ex);
            }
        }
        
        private void CleanupRegion(object state)
        {
            DateTimeUtcNano lastFlash = new DateTimeUtcNano(_lastFlashTime, 0);
            
            if (_tlnAreas != null)
            {
                foreach (GeoBoundingArea geoBoundingArea in _tlnAreas)
                {
                    if (geoBoundingArea.Flashes != null)
                    {
                        long checkTimeNanoseconds = geoBoundingArea.BuddyCheckTimeSeconds * DateTimeUtcNano.NanosecondsPerSecond;
                        geoBoundingArea.Flashes.PruneOldFlashes(lastFlash, checkTimeNanoseconds);
                    }
                }
            }

            if (_wwllnGlobalArea.Flashes != null)
            {
                long checkTimeNanoseconds = _wwllnGlobalArea.BuddyCheckTimeSeconds * DateTimeUtcNano.NanosecondsPerSecond;
                _wwllnGlobalArea.Flashes.PruneOldFlashes(lastFlash, checkTimeNanoseconds);
            }
        }
        
        public void Dispose()
        {
            if (_regionCleaner != null)
                _regionCleaner.Dispose();
        }
        public Tuple<double, double, double> FlashAgeMinMaxMedian
        {
            get { return _flashAges.GetMinMaxMedian(); }
        }

    }
}
