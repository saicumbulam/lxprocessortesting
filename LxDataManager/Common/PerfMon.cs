﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Aws.Core.Utilities;

namespace LxDataManager.Common
{
    /// <summary>
    /// This class is to handle the perf monitor for the data manager instances. Please note that if the counter is
    /// added/edited/removed from the category, the category will need to get removed and added back in for it 
    /// to work properly.
    /// </summary>
    public class PerfMon
    {
        private string _instanceName;
        Dictionary<string, PerformanceCounter> _counters;

        private const string CategoryName = "EN Lightning Data Manager";

        public const string StreamPacketRate = "Incoming Packet Rate";
        public const string NumberIncomingConnections = "Number of Incoming Connections";
      
 
        public PerfMon()
        {
            _instanceName = string.Format("LtgManager"); 
        }

        public void Init()
        {
            CreateCategory();
            CreateCounters(); 
        }

        public void RemovePerfMon()
        {
            DeleteCategory();
        }

        private void CreateCounters()
        {
            _counters = new Dictionary<string, PerformanceCounter>
                {
                    {StreamPacketRate, new PerformanceCounter(CategoryName, StreamPacketRate, _instanceName, false)}
                };
            foreach (string key in _counters.Keys)
            {
                UpdateNumberOfItemsCounter(key, 0); 
            }
        }

        private void CreateCategory()
        {
            if (!PerformanceCounterCategory.Exists(CategoryName))
            {
                try
                {
                    CounterCreationDataCollection ccds = new CounterCreationDataCollection
                    {
                        new CounterCreationData(StreamPacketRate, "Estimated packet per minute of incoming streaming data", PerformanceCounterType.NumberOfItems32),
                        new CounterCreationData(NumberIncomingConnections, "Number of incoming tcp connections", PerformanceCounterType.NumberOfItems32)
                    };
                    PerformanceCounterCategory.Create(CategoryName, "Lightning Data Manager counters", PerformanceCounterCategoryType.MultiInstance, ccds);
                }
                catch (Exception ex)
                {
                    EventManager.LogInfo("Failing to CreateCategory for PerfMon \nReason: " + ex.Message); 
                }
              
            }
        }


        private void DeleteCategory()
        {
            try
            {
                PerformanceCounterCategory.Delete(CategoryName);
                EventManager.LogInfo("Successfully deleted CategoryName for PerfMon");
            }
            catch(Exception ex)
            {
                EventManager.LogInfo("Failed to DELETE " + CategoryName + " in PerfMon \nReason: " + ex.Message); 
            }

        }

        public void UpdateNumberOfItemsCounter(string counterName, int value)
        {
            if (_counters != null && _counters.Count > 0)
            {
                PerformanceCounter pc;
                if (_counters.TryGetValue(counterName, out pc))
                {
                    pc.RawValue = value;
                }
            }
        }
    }
}
