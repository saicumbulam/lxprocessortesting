﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using LxCommon;

namespace LxDataManager.Common
{
    public static class FlashUtils
    {
        public static LTGFlash ConvertWwllnFlashToTlnFlash(LTGFlash wwllnFlash)
        {
            if (wwllnFlash == null)
                throw new ArgumentNullException("wwllnFlash");

            CorrectWwllnAmplitude(wwllnFlash);
            PatchWwllnTimeString(wwllnFlash);

            if (wwllnFlash.FlashPortionList != null && wwllnFlash.FlashPortionList.Count > 0)
            {
                foreach (LTGFlashPortion portion in wwllnFlash.FlashPortionList)
                {
                    CorrectWwllnAmplitude(portion);
                    PatchWwllnTimeString(portion);
                    portion.IsWwlln = true;
                }
            }

            return wwllnFlash;
        }

        private static void CorrectWwllnAmplitude(FlashData fd)
        {
            if (fd.FlashType == FlashType.FlashTypeGlobalCG)
            {   
                // Sanity check on CG peak current, all positive peak > MinPeakCurrentForPositiveCg else change it to negative CG   
                if (fd.Amplitude > 0 && fd.Amplitude < LtgConstants.MinPeakCurrentForPositiveCg)
                {
                    fd.Amplitude = -1 * fd.Amplitude;
                }
                
            }
        }

        public static FlashType ConvertWwllnTypeToTlnType(FlashType type)
        {
            FlashType ft = type;
            switch (ft)
            {
                case FlashType.FlashTypeGlobalCG:
                    ft = FlashType.FlashTypeCG;
                    break;
                case FlashType.FlashTypeGlobalIC:
                    ft = FlashType.FlashTypeIC;
                    break;
            }
            return ft;
        }

        private const int TimeStringLength = 29;

        private static void PatchWwllnTimeString(FlashData fd)
        {
            fd.FlashTime = fd.FlashTime.Trim();
            if (fd.FlashTime.Length > TimeStringLength)
            {
                fd.FlashTime = fd.FlashTime.Substring(0, TimeStringLength);
            }
            else if (fd.FlashTime.Length < TimeStringLength)
            {
                fd.FlashTime = fd.FlashTime.PadRight(TimeStringLength, '0');
            }
        }

        public static void SetFlashPortionConfidenceData(LTGFlashPortion portion, ConcurrentDictionary<string, StationData> stationDataTable)
        {
            portion.ConfidenceData.NearSensorConfidence = (short)portion.Confidence;
            portion.ConfidenceData.BiggestAngle = (short)(GetMaxAngleOfSensorsFromTheFlash(portion, stationDataTable) * 180 / Math.PI);
            portion.ConfidenceData.SensorDistribution = GetTotalBinsOfSensorsFromFlash(portion, stationDataTable);
        }

        private static double GetMaxAngleOfSensorsFromTheFlash(LTGFlashPortion flashPortion, ConcurrentDictionary<string, StationData> stationDataTable)
        {
            double startAngle = double.MaxValue;
            double endAngle = double.MinValue;
            List<double> angles = new List<double>();
            List<int> quadrants = new List<int>();

            foreach (Offset offset in flashPortion.OffsetList)
            {
                if (stationDataTable.ContainsKey(offset.StationId))
                {
                    StationData stationData = stationDataTable[offset.StationId];

                    double angle = stationData.AngleFromALocation(flashPortion.Latitude, flashPortion.Longitude,
                                                                     flashPortion.Height);
                    int quadrantNum = GetQuadrant(angle);

                    if (!quadrants.Contains(quadrantNum))
                        quadrants.Add(quadrantNum);

                    angles.Add(angle);
                }
            }

            double maxAngle = double.MinValue;
            if (quadrants.Count >= 3)
            {
                maxAngle = 1.5 * Math.PI;
            }
            else if (quadrants.Count == 2)
            {
                int delta = Math.Abs(quadrants[1] - quadrants[0]);
                if (delta == 2)
                { // two quadrants apart
                    maxAngle = Math.PI;
                }
                else if (delta == 3)
                {   // quadrant 1 and 4
                    for (int i = 0; i < angles.Count; i++)
                    {
                        angles[i] = angles[i] - Math.PI * 1.5;
                        if (angles[i] < 0)
                        {
                            angles[i] = angles[i] + 2.0 * Math.PI;
                        }
                    }
                }
            }

            if (maxAngle == double.MinValue)
            {
                foreach (double angle in angles)
                {
                    if (angle < startAngle)
                        startAngle = angle;
                    if (angle > endAngle)
                        endAngle = angle;
                }
                maxAngle = endAngle - startAngle;
            }

            return maxAngle;
        }

        private static int GetQuadrant(double angle)
        {
            int quadrant = 0;
            if (angle >= 0 && angle < Math.PI * 0.5)
            {
                quadrant = 1;
            }
            else if (angle >= Math.PI / 2 && angle < Math.PI)
            {
                quadrant = 2;
            }
            else if (angle >= Math.PI && angle < Math.PI * 1.5)
            {
                quadrant = 3;
            }
            else if (angle >= Math.PI * 1.5 && angle <= Math.PI * 2.0)
            {
                quadrant = 4;
            }

            return quadrant;
        }

        // Divide the 360 degree into 36 bins, each of which has 10 degrees
        // Check total how many bins that all the sensors occupy
        private static short GetTotalBinsOfSensorsFromFlash(LTGFlashPortion flashPortion, ConcurrentDictionary<string, StationData> stationDataTable)
        {
            short totalBin = 0;
            int[] bins = new int[36];

            foreach (Offset offset in flashPortion.OffsetList)
            {
                if (stationDataTable.ContainsKey(offset.StationId))
                {
                    StationData stationData = stationDataTable[offset.StationId];

                    double theAngle = stationData.AngleFromALocation(flashPortion.Latitude, flashPortion.Longitude,
                                                                     flashPortion.Height);
                    bins[GetAngleBinNumber(theAngle)]++;
                }
            }

            for (int i = 0; i < 36; i++)
            {
                if (bins[i] != 0)
                    totalBin++;
            }

            return totalBin;
        }

        private static int GetAngleBinNumber(double angle)
        {
            return (int)(angle * 18 / Math.PI);
        }
    }
}
