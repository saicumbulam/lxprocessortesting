using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net;
using Aws.Core.Utilities;
using Aws.Core.Utilities.Config;
using LxCommon;
using LxCommon.Monitoring;
using LxCommon.TcpCommunication;
using LxDataManager.Models;

namespace LxDataManager.Common
{
    public class Config : AppConfig
    {
        private static readonly GeoBoundingArea[] _tlnAreas;
        private static readonly GeoBoundingArea _wwllnGlobalArea;
        private static readonly ConcurrentDictionary<IPEndPoint, ISendWorker> _otherManagers;
        private static readonly Rect[] _wwllnExcludingRegions;
        private static readonly FlashConfidenceData _defaultConfidence;
        
        static Config()
        {
            _tlnAreas = LoadGeoBoundingAreas();
            _wwllnGlobalArea = LoadWwllnGlobalArea();
            _otherManagers = LoadManagers();
            _wwllnExcludingRegions = LoadWwllnExcludingRegions();
            _defaultConfidence = LoadDefaultConfidence();
        }

        private static FlashConfidenceData LoadDefaultConfidence()
        {
            return new FlashConfidenceData
            {
                NearSensorConfidence = Get<short>("DefaultMinNearestSensorRatio", 80),
                BiggestAngle = Get<short>("DefaultMinSensorAngle", 35),
                SensorDistribution = Get<short>("DefaultMinSensorDistributionBins", 2)
            };
        }

        private static GeoBoundingArea LoadWwllnGlobalArea()
        {
            GeoBoundingArea gba = new GeoBoundingArea();
            double left = Get("WwllnGlobalLeft", -180);
            double right = Get("WwllnGlobalRight", 180);
            double top = Get("WwllnGlobalTop", 90);
            double bottom = Get("WwllnGlobalBottom", -90);
            gba.Bounds = new Rect(left, right, bottom, top);
            gba.Name = "WWLLN Global";

            gba.DoBuddyCheck = Get("WwllnGlobalDoBuddyCheck", true);
            gba.BuddyCheckMinNumNeighbor = Get("WwllnGlobalBuddyCheckMinNumNeighbor", 3);
            gba.BuddyCheckTimeSeconds = Get("WwllnGlobalBuddyCheckTimeInSecond", 300);
            gba.BuddyCheckDistanceKm = Get("WwllnGlobalBuddyCheckDistanceInKM", 20);
            gba.MaxNodeSizeInKm = Get("WwllnGlobalMaxNodeSizeInKm", 800);
            gba.Flashes = new FlashQuadTree(gba.Bounds, gba.MaxNodeSizeInKm.Value, true);
            return gba;
        }

        private static Rect[] LoadWwllnExcludingRegions()
        {
            Rect[] rects = null;
            int num = Get("WWLLNExcludingRegionNum", 0);
            if (num > 0)
            {
                rects = new Rect[num];

                for (int i = 1; i <= num; i++)
                {
                    List<double> points = GetListConfigSettings<double>(string.Format("WWLLNExcludingRegion{0}", i), null, ',');
                    if (points != null && points.Count >= 4)
                    {
                        Rect rect = new Rect(points[0], points[1], points[2], points[3]);
                        rects[i - 1] = rect;
                    }
                    else
                    {
                        EventManager.LogError(TaskMonitor.TidServiceConfigLoadFail, string.Format("Unable to parse config for WWLLNExcludingRegion{0}", i));
                    }
                }
            }

            return rects;
        }

        private static ConcurrentDictionary<IPEndPoint, ISendWorker> LoadManagers()
        {
            ConcurrentDictionary<IPEndPoint, ISendWorker> clients = null;
            int num = Get("NumberOfManagers", 0);
            if (num > 0)
            {
                clients = new ConcurrentDictionary<IPEndPoint, ISendWorker>();
                for (int i = 1; i <= num; i++)
                {
                    int port = Get(string.Format("ManagerPort{0}", i), -1);
                    IPAddress address;
                    if (port != -1 && IPAddress.TryParse(Get(string.Format("ManagerIp{0}", i), string.Empty), out address))
                    {
                        IPEndPoint ipep = new IPEndPoint(address, port);
                        SendWorker sw = new SendWorker(ipep);
                        clients.TryAdd(ipep, sw);
                    }
                    else
                    {
                        EventManager.LogError(TaskMonitor.TidServiceConfigLoadFail, string.Format("Unable to parse config for Manager{0}", i));
                    }
                }
            }
            return clients;
        }

        private static GeoBoundingArea[] LoadGeoBoundingAreas()
        {
            GeoBoundingArea[] areas = null;
            int num = Get("NumberOfAreas", 0);
            if (num > 0)
            {
                areas = new GeoBoundingArea[num];
                for (int i = 1; i <= num; i++)
                {
                    GeoBoundingArea gba = new GeoBoundingArea();
                    double left = Get(string.Format("Area{0}Left", i), 0);
                    double right = Get(string.Format("Area{0}Right", i), 0);
                    double top = Get(string.Format("Area{0}Top", i), 0);
                    double bottom = Get(string.Format("Area{0}Bottom", i), 0);
                    gba.Bounds = new Rect(left, right, bottom, top);
                    gba.Name = Get(string.Format("Area{0}Name", i), string.Empty);
                    
                    FlashConfidenceData fcd = new FlashConfidenceData
                    {
                        NearSensorConfidence = Get<short>(string.Format("Area{0}MinNearestSensorRatio", i), 50),
                        BiggestAngle = Get<short>(string.Format("Area{0}MinSensorAngle", i), 0),
                        SensorDistribution = Get<short>(string.Format("Area{0}MinSensorDistributionBins", i), 0)
                    };
                    gba.MinConfindence = fcd;

                    gba.DoBuddyCheck = Get(string.Format("Area{0}DoBuddyCheck", i), false);
                    gba.BuddyCheckTimeSeconds = Get(string.Format("Area{0}BuddyCheckTimeInSecond", i), 600);
                    gba.BuddyCheckDistanceKm = Get(string.Format("Area{0}BuddyCheckDistanceInKM", i), 20);
                    gba.MaxNodeSizeInDegrees = GetNullableDouble(string.Format("Area{0}MaxNodeSizeInDegrees", i), null);
                    gba.MaxNodeSizeInKm = GetNullableDouble(string.Format("Area{0}MaxNodeSizeInKm", i), null);
                    gba.BuddyCheckMinNumNeighbor = Get(string.Format("Area{0}BuddyCheckMinNumNeighbor", i), 3);
                    gba.StrictNearSensorConfidence = Get(string.Format("Area{0}StrictNearestSensorRatio", i), 80);
                    gba.StrictOffsetCount = Get(string.Format("Area{0}StrictOffsetCount", i), 10);
                    gba.StrictBiggestAngle = Get(string.Format("Area{0}StrictBiggestAngle", i), 180);
                    gba.StrictSensorDistribution = Get(string.Format("Area{0}StrictSensorDistribution", i), 3);
                    areas[i - 1] = gba;

                    if (gba.MaxNodeSizeInDegrees.HasValue)
                    {
                        gba.Flashes = new FlashQuadTree(gba.Bounds, gba.MaxNodeSizeInDegrees.Value, false);
                    }
                    else if (gba.MaxNodeSizeInKm.HasValue)
                    {
                        gba.Flashes = new FlashQuadTree(gba.Bounds, gba.MaxNodeSizeInKm.Value, true);
                    }
                    else
                    {
                        throw new Exception(string.Format("Geo bounding area {0} mis-configured, must have MaxNodeSizeInDegrees or MaxNodeSizeInKm", gba.Name));
                    }

                }
            }
            return areas;
        }

        public static GeoBoundingArea[] TlnAreas
        {
            get { return _tlnAreas; }
        }

        public static int TcpListenPort
        {
            get { return Get<int>("TcpListenPort", 1030); }
        }

        public static ConcurrentDictionary<IPEndPoint, ISendWorker> OtherManagerTcpAddresses
        {
            get { return _otherManagers; }
            
        }

        public static int TcpFlashServerListenPort
        {
            get { return Get<int>("TcpFlashServerListenPort", 95); }
        }
        
        public static Rect[] WwllnExcludingRects
        {
            get { return _wwllnExcludingRegions; }
        }

        public static int StationDatabaseUpdateIntervalMinutes
        {
            get { return Get<int>("StationDatabaseUpdateIntervalMinutes", 600); }
        }

     

        public static double MinimumFlashGroupTimeInSecond
        {
            get { return Get<double>("MinimumFlashGroupTimeInSecond", 0.7); }
        }

        public static double MinimumTlnFlashGroupDistanceInKm
        {
            get { return Get("MinimumFlashGroupDistanceInKm", 10); }
        }

        public static double MinimumWwllnFlashGroupDistanceInKm
        {
            get { return Get("MinimumFlashGroupDistanceInKm", 25); }
        }

        public static int GrouperTimeDelayInMillisecond
        {
            get { return Get<int>("GrouperTimeDelayInMillisecond", 2500); }
        }

        public static int GrouperSendCheckIntervalMilliseconds 
        {
            get { return Get<int>("GrouperSendCheckIntervalMilliseconds", 250); }
        }

        

        public static GeoBoundingArea WwllnGlobalArea
        {
            get { return _wwllnGlobalArea; }
        }

        public static FlashConfidenceData DefaultConfidence
        {
            get { return _defaultConfidence; }
        }

        public static short ValidationWeakAmplitude
        {
            get { return Get<short>("ValidationWeakAmplitude", 2000); }
        }

    

        public static int MaxArchivingQueueTasks 
        {
            get { return Get("MaxArchivingQueueTasks", 5); }
        }

        public static int MaxFlashPortionGrouperQueueTasks
        {
            get { return Get("MaxFlashPortionGrouperQueueTasks", 5); }
        }

        public static int MaxFlashValidatorProcessorTasks
        {
            get { return Get("MaxFlashValidatorProcessorTasks", 2); }
        }

        public static int MaxIncomingFlashProcessorTasks
        {
            get { return Get<int>("MaxIncomingFlashProcessorTasks", 5); }
        }

        public static string PlaybackFlashDirectory 
        {
            get { return Get("PlaybackFlashDirectory", @"E:\Lightning Archive\Flashes\2013-04-01"); }
        }

        public static bool IsPlayback 
        {
            get { return Get("IsPlayback", false); }
        }

        public static bool IsSensorStatisticsEnabled
        {
            get { return Get("IsSensorStatisticsEnabled", true); }
        }

        public static bool IsPrimaryDbEnabled
        {
            get { return Get("IsPrimaryDbEnabled", true); }
        }

        public static bool IsEtlDbEnabled 
        {
            get { return Get("IsEtlDbEnabled", false); }
        }

        public static int SensorStatisiticsSaveMaxDegreeOfParallelism
        {
            get { return Get("SensorStatisiticsSaveMaxDegreeOfParallelism", 5); }
        }

        public static int RegionCleanupIntervalMinutes 
        {
            get { return Get("RegionCleanupIntervalMinutes", 10); }
        }

        public static int ProcessorMaxPendingConnections 
        {
            get { return Get("ProcessorMaxPendingConnections", 50); }
        }

        public static int ProcessorReceiveSampleSize 
        {
            get { return Get("ProcessorReceiveSampleSize", 5); }
        }

        public static int ProcessorConnectionSampleSize 
        {
            get { return Get("ProcessorConnectionSampleSize", 30); }
        }

        public static bool ExportFlash 
        { 
            get { return Get("ExportFlash", false); }
        }

        public static int DatafeedMaxPendingConnections
        {
            get { return Get("DatafeedMaxPendingConnections", 50); }
        }

        public static int DatafeedConnectionSampleSize
        {
            get { return Get("DatafeedConnectionSampleSize", 30); }
        }

        public static int RecentFlashesNumberOfSecondsToKeep
        {
            get { return Get("RecentFlashesNumberOfSecondsToKeep", 120); }
        }

        public static int MonitorProcessorPacketRateMinimum 
        {
            get { return Get("MonitorProcessorPacketRateMinimum", 10); }
        }

        public static int MonitorProcessorConnectionRateMaximum
        {
            get { return Get("MonitorProcessorConnectionRateMaximum", 5); }
        }

        public static int MonitorProcessorActiveConnectionsMinimum 
        {
            get { return Get("MonitorProcessorActiveConnectionsMinimum", 1); }
        }

        public static int MonitorDatafeedConnectionsMinimum
        {
            get { return Get("MonitorDatafeedConnectionsMinimum", 1); }
        }

        public static int MonitorDatafeedConnectionRateMaximum 
        {
            get { return Get("MonitorDatafeedConnectionRateMaximum", 3); }
        }

        public static string PrimaryFailedFlashFolder 
        {
            get { return Get("PrimaryFailedFlashFolder", @"E:\File Logger\Db Failures\"); }
        }

        public static string PrimaryFailedFlashFileName
        {
            get { return Get("PrimaryFailedFlashFileName", "Primary Failed Flashes {0}.json"); }
        }

        public static string PrimaryFailedPortionFolder
        {
            get { return Get("PrimaryFailedPortionFolder", @"E:\File Logger\Db Failures\"); }
        }

        public static string PrimaryFailedPortionFileName
        {
            get { return Get("PrimaryFailedPortionFileName", "Primary Failed Portions {0}.json"); }
        }

        public static string EtlFailedFlashFolder
        {
            get { return Get("EtlFailedFlashFolder", @"E:\File Logger\Db Failures\"); }
        }

        public static string EtlFailedFlashFileName
        {
            get { return Get("EtlFailedFlashFileName", "Etl Failed Flashes {0}.json"); }
        }

        public static string EtlFailedPortionFolder
        {
            get { return Get("EtlFailedPortionFolder", @"E:\File Logger\Db Failures\"); }
        }

        public static string EtlFailedPortionFileName
        {
            get { return Get("EtlFailedPortionFileName", "Etl Failed Portions {0}.json"); }
        }
        
        public static bool EnableDbFailureLogging
        {
            get { return Get("EnableDbFailureLogging", true); }
        }

        public static string ExportFileDirectory
        {
            get { return Get("ExportFileDirectory", string.Empty); }
        }

        public static int TcpKeepAliveIntervalSeconds
        {
            get { return Get("TcpKeepAliveIntervalSeconds", 15); }
        }

        public static int MaxArchivingQueueSize
        {
            get { return Get("MaxArchivingQueueSize", 100000); }
        }

        public static string SystemType
        {
            get { return Get("SystemType", "TLN"); }
        }

        public static bool EnableRejectedFlashLogger
        {
            get { return Get("EnableRejectedFlashLogger", false); }
        }

        public static string RejectedFlashLoggerDirectory
        {
            get { return Get("RejectedFlashLoggerDirectory", @"E:\EN\Archive\RejectedFlashes"); }
        }

        public static string RejectedFlashLoggerBaseFileName
        {
            get { return Get("RejectedFlashLoggerBaseFileName", @"RejectedFlash-{0}.txt"); }
        }

        public static int RejectedFlashLoggerMinutesPerFile
        {
            get { return Get("RejectedFlashLoggerMinutesPerFile", 1); }
        }

        public static int ListenerIdleCheckIntervalSeconds
        {
            get { return Get("ListenerIdleCheckIntervalSeconds", 30); }
        }

        public static int MedianSampleSize 
        {
            get { return Get("MedianSampleSize", 250); }
        }

        public static int GrouperSendDegreeOfParallelism 
        {
            get { return Get("GrouperSendDegreeOfParallelism", 5); }
        }

        public static short ValidationStrongAmplitude
        {
            get { return Get<short>("ValidationStrongAmplitude", 2000); }
        }

        public static int ValidationCloseStationDistanceKm
        {
            get { return Get("ValidationCloseStationDistanceKm", 500); }
        }

        public static int MinimumPortionsForValidFlash
        {
            get { return Get("MinimumPortionsForValidFlash", 3); }
        }

        public static double ValidationDuplicateTimeSeconds
        {
            get { return Get("ValidationDuplicateTimeSeconds", 0.0004); }
        }

        public static double ValidationDuplicateDistanceKm
        {
            get { return Get("ValidationDuplicateDistanceKm", 100); }
        }
        
        public static double? GetNullableDouble(string keyName, double? defaultValue)
        {
            double? retVal = defaultValue;
            string strVal = Get<string>(keyName, null);
            if (strVal != null)
            {
                try
                {
                    retVal = (double)Convert.ChangeType(strVal, typeof(double));
                }
                catch { }
            }

            return retVal;
        }
        
        
    }
}
