﻿using System.Collections;
using System.Data;

using Aws.Core.Data;

namespace LxDatafeed.Data.Partner
{
    internal class InsertPartnerEventLogDbCmdText : DbCommandText
    {
        private string _partnerId;
        private string _remoteIpAddress;
        private string _eventText;
        private string _lastModifiedBy;

        public InsertPartnerEventLogDbCmdText(string partnerId, string remoteIpAddress, string eventText, string lastModifiedBy)
        {
            _partnerId = partnerId;
            _remoteIpAddress = remoteIpAddress;
            _eventText = eventText;
            _lastModifiedBy = lastModifiedBy;

            InitSP();
        }

        public override string BuildCmdText()
        {
            return "lxdf.PartnerEventLogInsert";
        }

        public override ArrayList BuildParameters()
        {   // If any of these are NULL they will not be sent as params which is fine.
            AddParam("@partnerId", _partnerId, SqlDbType.VarChar, 50);
            AddParam("@remoteIpAddress", _remoteIpAddress, SqlDbType.VarChar, 39);
            AddParam("@eventText", _eventText, SqlDbType.VarChar, 2000);
            AddParam("@lastModifiedBy", _lastModifiedBy, SqlDbType.VarChar, 100);

            return _paramList;
        }
    }
}
