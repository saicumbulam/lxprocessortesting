﻿using System.Data.SqlClient;

using Aws.Core.Data;

using LxDatafeed.Data.ResponsePackage;

namespace LxDatafeed.Data.Partner
{
    internal class GetPartnerDataElementDbCmdText : DbCommandText
    {
        public GetPartnerDataElementDbCmdText()
        {
            InitSP();
        }

        public override string BuildCmdText()
        {
            return "lxdf.PartnerDataElementGet";
        }

        public override object ParseRow(SqlDataReader dr)
        {
            return new Data()
                       {
                           PartnerId = this.GetDataString(dr, "PartnerId").ToLower(),  // Always lower the PartnerId
                           ResponseDataElement = (ResponseDataElement)this.GetDataShort(dr, "ResponseDataElementId")
                       };
        }

        #region Inner Class: Data
        public class Data
        {
            public string PartnerId { get; set; }
            public ResponseDataElement ResponseDataElement { get; set; }
        }
        #endregion
    }
}
