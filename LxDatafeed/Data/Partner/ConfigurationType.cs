﻿namespace LxDatafeed.Data.Partner
{
    internal enum ConfigurationType : short
    {
        CanPartnerPassInBoundingBoxOnConnection = 1,
        CanPartnerPassInLxClassificationOnConnection = 2,
        CanPartnerpassInDataElementsonConnection = 3
    }
}
