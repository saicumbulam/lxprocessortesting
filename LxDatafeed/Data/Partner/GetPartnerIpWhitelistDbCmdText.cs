﻿using System.Data.SqlClient;

using Aws.Core.Data;

namespace LxDatafeed.Data.Partner
{
    internal class GetPartnerIpWhitelistDbCmdText : DbCommandText
    {
        public GetPartnerIpWhitelistDbCmdText()
        {
            InitSP();
        }

        public override string BuildCmdText()
        {
            return "lxdf.PartnerIpWhitelistGet";
        }

        public override object ParseRow(SqlDataReader dr)
        {
            return new Data()
                       {
                           PartnerId = this.GetDataString(dr, "PartnerId").ToLower(),  // Always lower the PartnerId
                           IpAddress = this.GetDataString(dr, "IpAddress")
                       };
        }

        #region Inner Class: Data
        public class Data
        {
            public string PartnerId { get; set; }
            public string IpAddress { get; set; }
        } 
        #endregion
    }
}
