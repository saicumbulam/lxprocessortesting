﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

using Aws.Core.Data;
using Aws.Core.Utilities;
using Aws.Core.Utilities.Threading;

using LxCommon.Monitoring;

using LxDatafeed.Data.ResponsePackage;
using LxDatafeed.Utilities;
using System.Threading.Tasks;
using En.Data.Api.Audit.Client;
using En.Data.Api.Audit.Client.Models;
using En.Ods.Lib.Common.Interface;
using En.Ods.Lib.Common.Utility.ServiceClient;

namespace LxDatafeed.Data.Partner
{
    internal static class PartnerSvc
    {
        private static Dictionary<string, PartnerData> _partnerList;
        private static readonly LoadDataProcessor LoadProcessor;
        private static readonly ReaderWriterLockSlim LockPartnerList;
        private static DateTime _lastRefreshTime = DateTime.MinValue;
        private static readonly bool EnableAudit;
        private static readonly string AuditBaseUrl;
        private static readonly string AuditTableName;
        


        #region GetLastDbRefreshTime
        public static DateTime LastDbRefreshTime
        {
            get { return _lastRefreshTime; }
        }
        #endregion

        static PartnerSvc()
        {
            LockPartnerList = new ReaderWriterLockSlim();
            LoadProcessor = new LoadDataProcessor();

            LoadProcessor.InitialLoad();
            LoadProcessor.StartWorkerThreadSync();

            EnableAudit = AppConfig.EnableAudit;
            AuditBaseUrl = AppConfig.AuditBaseUrl;
            AuditTableName = AppConfig.AuditTableName;
            if (EnableAudit && string.IsNullOrWhiteSpace(AuditBaseUrl))
            {
                throw new ArgumentException("Missing config value AuditBaseUrl");
            }

            if (EnableAudit && string.IsNullOrWhiteSpace(AuditTableName))
            {
                throw new ArgumentException("Missing config value AuditTableName");
            }

        }

        #region Init
        /// <summary>
        /// TODO: Refactor this wait out
        /// </summary>
        /// <returns></returns>
        public static bool Init()
        {
            bool isSuccessful = false;

            LockPartnerList.EnterReadLock();
            try
            {
                isSuccessful = (_partnerList != null && _partnerList.Count > 0);
            }
            finally
            {
                LockPartnerList.ExitReadLock();
            }

            return isSuccessful;
        } 
        #endregion

        #region GetData
        public static PartnerData GetData(string partnerId)
        {
            PartnerData partnerData = null;

            if (!string.IsNullOrEmpty(partnerId))
            {
                LockPartnerList.EnterReadLock();
                try
                {
                    _partnerList.TryGetValue(partnerId, out partnerData);
                }
                finally
                {
                    LockPartnerList.ExitReadLock();
                }
            }

            return partnerData;
        } 
        #endregion

        #region LogEvent
        public static void LogEvent(string partnerId, string remoteIpAddress, string eventText, string lastModifiedBy)
        {
            ThreadPoolHelper.QueueUserWorkItem(
                () =>
                    {
                        try
                        {
                            DbCommand.ExecuteNonQuery(AppConfig.LightningDbConnStr,
                                              new InsertPartnerEventLogDbCmdText(partnerId, remoteIpAddress, eventText + " Server: " + Environment.MachineName, lastModifiedBy));
                        }
                        catch (Exception ex)
                        {
                            EventManager.LogError(TaskMonitor.DfLogEventFail, "Lightning Data Feed failed to write Partner Event Log entry in DB. \r\nReason: " + ex.Message);
                        }

                        if (EnableAudit)
                        {
                            try
                            {
                                if (string.IsNullOrWhiteSpace(partnerId))
                                {
                                    partnerId = remoteIpAddress;
                                }
                                if (!string.IsNullOrWhiteSpace(partnerId))
                                {
                                    SubmitAuditV1 record = CreateAuditMessage(AuditTableName, partnerId.ToLower(), remoteIpAddress, eventText, lastModifiedBy);
                                    AuditClient client = new AuditClient(AuditBaseUrl);
                                    OdsServiceResponse<string> osr = client.SendAuditRequest(record);
                                    if (osr.Code < 200 || osr.Code > 299)
                                    {
                                        EventManager.LogError(TaskMonitor.DfLogEventFail, string.Format("Audit record http call failed status: {0}, requestId: {1}", osr.Code, osr.RequestId));
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                EventManager.LogError(TaskMonitor.DfLogEventFail, "Error submitting audit record", ex);
                            }    
                        }
                    });
        }

        private static SubmitAuditV1 CreateAuditMessage(string tableName, string partnerId, string remoteIpAddress, string eventText, string lastModifiedBy)
        {
            return new SubmitAuditV1
            {
                AuditName = tableName,
                AuditRangeKey = partnerId,
                AuditData = new Dictionary<string, string>
                {
                    {"remoteIpAddress", remoteIpAddress},
                    {"eventText", eventText},
                    {"lastModifiedBy", lastModifiedBy}
                }
            };
        }
        #endregion

        #region RefreshList
        private static void RefreshList(Dictionary<string, PartnerData> newPartnerList)
        {
            LockPartnerList.EnterWriteLock();
            try
            {
                _partnerList = newPartnerList;
            }
            finally
            {
                LockPartnerList.ExitWriteLock();
            }
        } 
        #endregion

        #region Inner Class: LoadDataProcessor
        private class LoadDataProcessor : WorkerThread
        {
            public LoadDataProcessor()
            {   // TODO: move config
                this.WaitTimeSecondsBetweenRun = (int)TimeSpan.FromMinutes(AppConfig.PartnerProfileRefreshMinutes).TotalSeconds;
                this.WaitFullWaitTimeBeforeFirstRun = true;
            }

            public void InitialLoad()
            {
                Run();
            }

            protected override WorkerThread.RunReturnStatus Run()
            {
                EventManager.LogInfo("Lightning Data Feed starting to refresh partner profile data...");
                Dictionary<string, PartnerData> list = RefreshFromDb();
                RefreshList(list);
                
                // Let's read the cfg again in case it has changed
                // TODO: For some reason this appears to be reading a cached version of the config data
                this.WaitTimeSecondsBetweenRun = (int)TimeSpan.FromMinutes(AppConfig.PartnerProfileRefreshMinutes).TotalSeconds;
                EventManager.LogInfo("Lightning Data Feed finished refreshing partner profile data.[partnerCount=" + list.Count + "; PartnerProfileRefreshMinutes=" +
                                     AppConfig.PartnerProfileRefreshMinutes + "]");

                return WorkerThread.RunReturnStatus.RunAgain;
            }

            private Dictionary<string, PartnerData> RefreshFromDb()
            {
                Dictionary<string, PartnerData> partnerList = LoadPartnerList();
                LoadIpWhitelist(partnerList);
                LoadDataElement(partnerList);
                LoadConfiguration(partnerList);
                
                //get the lastRefreshTime for Partner Table 
                _lastRefreshTime = DateTime.UtcNow; 

                return partnerList;

            }

            private Dictionary<string, PartnerData> LoadPartnerList()
            {
                Dictionary<string, PartnerData> partnerList = new Dictionary<string, PartnerData>();

                ArrayList list = DbCommand.ExecuteList(AppConfig.LightningDbConnStr, new GetPartnerListDbCmdText());

                if (list != null)
                {
                    foreach (PartnerData partnerData in list)
                    {
                        partnerList.Add(partnerData.PartnerId, partnerData);
                    }
                }

                return partnerList;
            }

            #region LoadIpWhitelist
            private void LoadIpWhitelist(Dictionary<string, PartnerData> partnerList)
            {
                if (partnerList != null)
                {
                    ArrayList list = DbCommand.ExecuteList(AppConfig.LightningDbConnStr, new GetPartnerIpWhitelistDbCmdText());

                    if (list != null)
                    {
                        PartnerData partnerData = null;
                        foreach (GetPartnerIpWhitelistDbCmdText.Data data in list)
                        {
                            if (partnerList.TryGetValue(data.PartnerId, out partnerData))
                            {
                                if (partnerData.IpWhitelist == null)
                                {
                                    partnerData.IpWhitelist = new HashSet<string>();
                                }

                                partnerData.IpWhitelist.Add(data.IpAddress);
                            }
                        }
                    }
                }
            } 
            #endregion

            #region LoadDataElement
            private void LoadDataElement(Dictionary<string, PartnerData> partnerList)
            {
                if (partnerList != null)
                {
                    DbCommand.ExecuteList(AppConfig.LightningDbConnStr, new GetPartnerDataElementDbCmdText());

                    ArrayList list = DbCommand.ExecuteList(AppConfig.LightningDbConnStr, new GetPartnerDataElementDbCmdText());

                    if (list != null)
                    {
                        PartnerData partnerData = null;
                        foreach (GetPartnerDataElementDbCmdText.Data data in list)
                        {
                            if (partnerList.TryGetValue(data.PartnerId, out partnerData))
                            {
                                if (partnerData.ResponseDataElements == null)
                                {
                                    partnerData.ResponseDataElements = new HashSet<ResponseDataElement>();
                                }

                                partnerData.ResponseDataElements.Add(data.ResponseDataElement);
                            }
                        }
                    }
                }
            } 
            #endregion

            #region LoadConfiguration
            private void LoadConfiguration(Dictionary<string, PartnerData> partnerList)
            {
                if (partnerList != null)
                {
                    ArrayList list = DbCommand.ExecuteList(AppConfig.LightningDbConnStr, new GetPartnerConfigurationDbCmdText());

                    if (list != null)
                    {
                        PartnerData partnerData = null;
                        foreach (GetPartnerConfigurationDbCmdText.Data data in list)
                        {
                            if (partnerList.TryGetValue(data.PartnerId, out partnerData))
                            {
                                if (partnerData.Configuration == null)
                                {
                                    partnerData.Configuration = new Dictionary<ConfigurationType, string>();
                                }

                                partnerData.Configuration.Add(data.ConfigurationType, data.ConfigurationValue);
                            }
                        }
                    }
                }
            }
            #endregion
        } 
        #endregion


    }
}
