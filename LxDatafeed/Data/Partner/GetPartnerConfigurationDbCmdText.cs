﻿using System.Data.SqlClient;

using Aws.Core.Data;

namespace LxDatafeed.Data.Partner
{
    internal class GetPartnerConfigurationDbCmdText : DbCommandText
    {
        public GetPartnerConfigurationDbCmdText()
        {
            InitSP();
        }

        public override string BuildCmdText()
        {
            return "lxdf.PartnerConfigurationGet";
            
        }

        public override object ParseRow(SqlDataReader dr)
        {
            return new Data()
                       {
                           PartnerId = this.GetDataString(dr, "PartnerId").ToLower(),  // Always lower the PartnerId
                           ConfigurationType = (ConfigurationType) this.GetDataShort(dr, "ConfigurationId"),
                           ConfigurationValue = this.GetDataString(dr, "ConfigurationValue", null)
                       };
        }

        #region Inner Class: Data
        public class Data
        {
            public string PartnerId { get; set; }
            public ConfigurationType ConfigurationType { get; set; }
            public string ConfigurationValue { get; set; }
        }
        #endregion
    }
}
