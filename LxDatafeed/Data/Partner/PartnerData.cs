﻿using System;
using System.Collections.Generic;

using LxCommon;

using LxDatafeed.Data.ResponsePackage;

namespace LxDatafeed.Data.Partner
{
    internal class PartnerData : ICloneable
    {
        public string PartnerId { get; set; }
        public string PartnerName { get; set; }
        public ResponseFormatType ResponseFormatType { get; set; }
        public ResponsePackageType ResponsePackageType { get; set; }
        public LxClassificationType LxClassificationType { get; set; }
        public BoundingBox BoundingArea { get; set; }
        public short ResponsePackageVersion { get; set; }

        public HashSet<string> IpWhitelist { get; set; }
        public HashSet<ResponseDataElement> ResponseDataElements { get; set; }
        public Dictionary<ConfigurationType, string> Configuration { get; set; }
        public FlashNetworkType NetworkType { get; set; }
        public bool Metadata { get; set; }
        public bool ShowSource { get; set; }

        public object Clone()
        {
            var clone = new PartnerData
                            {
                                PartnerId = PartnerId,
                                PartnerName = PartnerName,
                                ResponseFormatType = ResponseFormatType,
                                ResponsePackageType = ResponsePackageType,
                                LxClassificationType = LxClassificationType,
                                NetworkType = NetworkType,
                                Metadata = Metadata,
                                BoundingArea = BoundingArea
                            };           

            if (IpWhitelist != null)
            {
                clone.IpWhitelist = new HashSet<string>();
                foreach (string ipAddress in IpWhitelist)
                {
                    clone.IpWhitelist.Add(ipAddress);
                }
            }

            if (ResponseDataElements != null)
            {
                clone.ResponseDataElements = new HashSet<ResponseDataElement>();
                foreach (ResponseDataElement responseDataElement in ResponseDataElements)
                {
                    clone.ResponseDataElements.Add(responseDataElement);
                }
            }

            if (Configuration != null)
            {
                clone.Configuration = new Dictionary<ConfigurationType, string>();
                foreach (KeyValuePair<ConfigurationType, string> kvp in Configuration)
                {
                    clone.Configuration.Add(kvp.Key, kvp.Value);
                }
            }

            return clone;
        }

        public bool HasDataElement(ResponseDataElement responseDataElement)
        {
            return (ResponseDataElements != null && ResponseDataElements.Contains(responseDataElement));
        }
        
        public bool IsValidRemoteConnection(string remoteIpAddress)
        {
            bool isValid = false;

            if (IpWhitelist == null || IpWhitelist.Count == 0)
            {   // No whitelist: this means any client can connect
                isValid = true;
            }
            else if (IpWhitelist.Contains(remoteIpAddress))
            {   // Whitelist contains the remote ip address: this means we have a valid client
                isValid = true;
            }

            return isValid;
        }
        
        public bool IsLightningInBoundingArea(LTGFlash flash)
        {
            bool isLightningInBoundingArea = false;

            if (BoundingArea == null)
            {   // No bounding area so the entire world is valid
                isLightningInBoundingArea = true;
            }
            else
            {   // A bounding area exists so check if lightning is inside boundary
                // The rule here is that if any pulse is inside the boundary then all pulses
                // are sent (including the parent flash).

                foreach (FlashData flashData in flash.FlashPortionList)
                {
                    if (BoundingArea.Contains(flashData.Longitude, flashData.Latitude))
                    {
                        isLightningInBoundingArea = true;
                        break;
                    }
                }
            }

            return isLightningInBoundingArea;
        }
        
        public bool IsRequestedLightningType(FlashData flashData)
        {
            bool isRequestedLightningType = false;

            if (LxClassificationTypeHelper.IsMatch(LxClassificationType, flashData.FlashType))
            {
                if ((flashData is LTGFlashPortion && ResponsePackageType != ResponsePackageType.Flash) ||
                    (flashData is LTGFlash && ResponsePackageType != ResponsePackageType.Pulse))
                {
                    isRequestedLightningType = true;
                }
            }

            return isRequestedLightningType;
        }
        
    }
}


