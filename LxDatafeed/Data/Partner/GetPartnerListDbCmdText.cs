﻿using System;
using System.Data.SqlClient;

using Microsoft.SqlServer.Types;

using LxDatafeed.Data.ResponsePackage;

using Aws.Core.Data;

namespace LxDatafeed.Data.Partner
{
    internal class GetPartnerListDbCmdText : DbCommandText
    {
        public GetPartnerListDbCmdText()
        {
            InitSP();
        }

        public override string BuildCmdText()
        {
            return "lxdf.PartnerGet";
        }

        public override object ParseRow(SqlDataReader dr)
        {
            PartnerData partnerData = new PartnerData()
            {
                PartnerId = this.GetDataString(dr, "PartnerId").ToLower(),   // Always lower the PartnerId
                PartnerName = this.GetDataString(dr, "PartnerName"),
                ResponseFormatType = (ResponseFormatType) this.GetDataByte(dr, "ResponseFormatTypeId"),
                ResponsePackageType = (ResponsePackageType) this.GetDataByte(dr, "ResponsePackageTypeId"),
                NetworkType = (FlashNetworkType)this.GetDataInt(dr, "NetworkTypeId"),
                Metadata = this.GetDataBool(dr, "Metadata")
            };

            if (this.GetDataBool(dr, "IsInCloudEnabled"))
            {
                partnerData.LxClassificationType |= LxClassificationType.InCloud;
            }

            if (this.GetDataBool(dr, "IsCloudToGroundEnabled"))
            {
                partnerData.LxClassificationType |= LxClassificationType.CloudToGround;
            }

            if (dr["BoundingArea"] != DBNull.Value)
            {
                SqlGeography geo = dr["BoundingArea"] as SqlGeography;
                bool isInclusion = GetDataBool(dr, "IsInclusionBoundingArea", true);
                partnerData.BoundingArea = BoundingBox.Convert(geo, isInclusion);
            }

           return partnerData;
        }      
    }
}
