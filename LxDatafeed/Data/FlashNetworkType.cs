﻿namespace LxDatafeed.Data
{
    public enum FlashNetworkType : int
    {
        NotDefined = -1,
        EnTlnNetwork = 1,
        EnGlnNetwork = 2
    }
}
