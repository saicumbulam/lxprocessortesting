﻿using System;

using LxCommon;

namespace LxDatafeed.Data
{
    [Flags]
    internal enum LxClassificationType
    {
        NotDefined = 0x00,
        InCloud = 0x01,
        CloudToGround = 0x02,
    }

    /// <summary>
    /// TODO: This should be refactored... this was added as a bridge between old/new code
    /// </summary>
    internal static class LxClassificationTypeHelper
    {
        public static bool IsMatch(LxClassificationType lxClassificationType, FlashType flashType)
        {
            bool isMatch = false;

            switch (flashType)
            {
                case FlashType.FlashTypeCG:
                case FlashType.FlashTypeGlobalCG:
                    isMatch = ((lxClassificationType & LxClassificationType.CloudToGround) == LxClassificationType.CloudToGround);
                    break;
                case FlashType.FlashTypeIC:
                case FlashType.FlashTypeGlobalIC:
                case FlashType.FlashTypeICNarrowBipolar:
                    isMatch = ((lxClassificationType & LxClassificationType.InCloud) == LxClassificationType.InCloud);
                    break;
            }

            return isMatch;
        }
    }
}
