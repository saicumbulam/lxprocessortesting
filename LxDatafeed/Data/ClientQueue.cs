﻿using System.Threading;
using System.Collections.Generic;
using Aws.Core.Utilities;
using Aws.Core.Utilities.Threading;

using LxCommon;

using LxDatafeed.Net;
using LxDatafeed.Process;
using LxDatafeed.Utilities;

namespace LxDatafeed.Data
{
    internal class ClientQueue
    {
        private static readonly List<ClientConnWorker> _clientList = new List<ClientConnWorker>();
        private static readonly ReaderWriterLockSlim LockClientList = new ReaderWriterLockSlim();


        public static int Count
        {
            get
            {
                int count = 0;
                try
                {
                    LockClientList.EnterReadLock();
                    count = _clientList.Count;
                    
                }
                finally
                {
                    LockClientList.ExitReadLock();
                }
                return count;
            }
        }

        public static void AddTcpClient(SocketState data)
        {
            if (data != null && !string.IsNullOrEmpty(data.RemoteIP))
            {
                bool isClientAlreadyConnected = false;
                bool allowMultipleConnections = AppConfig.AllowMultipleConnections;

                try
                {
                    LockClientList.EnterUpgradeableReadLock();

                    if (!allowMultipleConnections)
                    {
                        for (int i = 0; i < _clientList.Count; i++)
                        {   // Let's loop through looking for existing connection for this partner/ip

                            if (_clientList[i].IsSamePartnerId(data.PartnerSessionData))
                            {  // found existing connection so close the old connection and replace old connection with new

                                string existing = _clientList[i].SocketState.RemoteAddress;
                                _clientList[i].SocketState.CloseTcpClient();
                                ReplaceSocketState(data, i);
                                isClientAlreadyConnected = true;
                                EventManager.LogWarning(string.Format("Client {0} already connected. Replacing connection from {1} with {2}",
                                    data.PartnerSessionData.PartnerId,
                                    existing,
                                    data.RemoteAddress));
                                break;
                            }
                        }
                    }

                    if (!isClientAlreadyConnected)
                    {   // Didn't find existing connection so create new client worker thread and add to list
                        CreateClientWorker(data);
                    }
                }
                finally
                {
                    LockClientList.ExitUpgradeableReadLock();
                }
            }
        }

        private static void ReplaceSocketState(SocketState data, int i)
        {
            try
            {
                LockClientList.EnterWriteLock();
                _clientList[i].SocketState = data;
            }
            finally
            {
                LockClientList.ExitWriteLock();
            }
        } 
        
        private static void CreateClientWorker(SocketState data)
        {
            var clientConnWorker = new ClientConnWorker { SocketState = data };
            WorkerThread.StartWorkerThreadAsync(clientConnWorker);

            try
            {
                LockClientList.EnterWriteLock();
                _clientList.Add(clientConnWorker);
            }
            finally
            {
                LockClientList.ExitWriteLock();
            }
        } 
        
        public static void RemoveTcpClient(string socketId)
        {
            ClientConnWorker clientConnWorker = null;

            try
            {
                LockClientList.EnterUpgradeableReadLock();

                for (int i = 0; i < _clientList.Count; i++)
                {
                    if (_clientList[i].SocketState.SocketId == socketId)
                    {
                        try
                        {
                            LockClientList.EnterWriteLock();
                            clientConnWorker = _clientList[i];
                            _clientList.RemoveAt(i);
                        }
                        finally
                        {
                            LockClientList.ExitWriteLock();
                        }
                        break;
                    }
                }
            }
            finally
            {
                LockClientList.ExitUpgradeableReadLock();
            }

            if (clientConnWorker != null)
            {
                clientConnWorker.Stop();
            }
        } 
        
        public static void SendTcpPacket(LTGFlash flash)
        {
            try
            {
                //change the type to tln 
                bool isWwlln = (flash.FlashType == FlashType.FlashTypeGlobalCG || flash.FlashType == FlashType.FlashTypeGlobalIC);
                
                
                LockClientList.EnterReadLock();
                foreach (ClientConnWorker t in _clientList)
                {
                    //gln gets wwlln + tln, tln gets only tln
                    if (t.SocketState.PartnerSessionData.NetworkType == FlashNetworkType.EnGlnNetwork)
                    {
                        t.AddData(flash);
                    }   
                    else if (t.SocketState.PartnerSessionData.NetworkType == FlashNetworkType.EnTlnNetwork && !isWwlln)
                    {
                        t.AddData(flash);
                    }
                }
            }
            finally
            {
                LockClientList.ExitReadLock();
            }
        }
    }  
}
