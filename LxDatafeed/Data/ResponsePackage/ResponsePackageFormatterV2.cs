﻿using System;
using System.Collections.Generic;
using System.Text;

using Aws.Core.Utilities;

using LxCommon;

using LxDatafeed.Data.Partner;

namespace LxDatafeed.Data.ResponsePackage
{
    internal partial class ResponsePackageFormatter
    {
        /// <remarks>
        /// This class is public so that GetNumberOfSensors() can be accessed externally. Note that this logic
        /// should be part of the class containing the pulse data but I am currently not able to change that class.
        /// We should do this when we start to refactor the shared libs. 
        /// </remarks>
        public class Version2
        {
            private class AsciiFormatTemplates
            {
                public const char Delimiter = ',';
                public const string Time = "yyyyMMddTHHmmss.fff";
                public const string Latitude = "{0:00.0000000}";
                public const string Longitude = "{0:000.0000000}";
                public const string Amplitude = "{0:000000000}";
                public const string LocationError = "000";
                public const string IcHeight = "00000";
                public const string NumberOfSensors = "000";
                public const string Multiplicity = "000";
            }

            public static Dictionary<ResponsePackageType, Dictionary<ResponseFormatType, IFormatter>> CreateFormatters()
            {
                return new Dictionary<ResponsePackageType, Dictionary<ResponseFormatType, IFormatter>>
                           {   // There should be one entry per response package type
                               {ResponsePackageType.Flash, Flash.CreateFormatters()},
                               {ResponsePackageType.Pulse, Pulse.CreateFormatters()},
                               {ResponsePackageType.FlashAndPulseCombo, FlashAndPulseCombo.CreateFormatters()}
                           };
            }

            private static byte CreateChecksum(short packetLen, byte[] packageBytes)
            {
                byte bytesum = 0;
                for (int ct = 0; ct < (packetLen - 1); ++ct)
                {
                    bytesum += packageBytes[ct];
                }
                return (byte)(256 - bytesum);
            }

            private static void CreateAsciiSignedValue(double coordinate, StringBuilder asciiText, string formatTemplate)
            {
                asciiText.Append(AsciiFormatTemplates.Delimiter);
                if (coordinate >= 0)
                {
                    asciiText.Append('+');
                }
                else
                {
                    asciiText.Append('-');
                }
                asciiText.Append(string.Format(formatTemplate, Math.Abs(coordinate)));
            }

            private static bool IsAllowedDataElement(PartnerData partnerData, ResponseDataElement responseDataElement)
            {
                return (partnerData.ResponseDataElements != null && partnerData.ResponseDataElements.Contains(responseDataElement));
            }

            private static byte GetLocationError(FlashData flashData)
            {
                return (flashData.LocationError > byte.MaxValue ? byte.MaxValue : (byte)flashData.LocationError);
            }

            private static ushort GetIcHeight(FlashData flashData)
            {
                return (flashData.Height > ushort.MaxValue ? ushort.MaxValue : (ushort)flashData.Height);
            }

            public static byte GetNumberOfSensors(LTGFlash flash, int? pulseIndex)
            {
                byte numberOfSensors = 0;

                if (pulseIndex.HasValue)
                {
                    LTGFlashPortion pulse = (LTGFlashPortion)flash.FlashPortionList[pulseIndex.Value];
                    numberOfSensors = (pulse.OffsetList.Count > byte.MaxValue ? byte.MaxValue : (byte)pulse.OffsetList.Count);
                }
                else
                {
                    HashSet<string> uniqueSetOfSensorIds = new HashSet<string>();
                    foreach (LTGFlashPortion pulse in flash.FlashPortionList)
                    {
                        if (pulse.OffsetList != null)
                        {
                            foreach (Offset sensor in pulse.OffsetList)
                            {
                                uniqueSetOfSensorIds.Add(sensor.StationId);
                            }
                        }
                    }
                    numberOfSensors = (uniqueSetOfSensorIds.Count > byte.MaxValue ? byte.MaxValue : (byte)uniqueSetOfSensorIds.Count);
                }

                return numberOfSensors;
            }

            private static byte GetMultiplicity(LTGFlash flash)
            {
                return (flash.FlashPortionList.Count > byte.MaxValue ? byte.MaxValue : (byte)flash.FlashPortionList.Count);
            }

            private class Flash
            {
                public static Dictionary<ResponseFormatType, IFormatter> CreateFormatters()
                {
                    return new Dictionary<ResponseFormatType, IFormatter>
                           {
                               {ResponseFormatType.Ascii, new Ascii()},
                               {ResponseFormatType.Binary, new Binary()}
                           };
                }

                public class Ascii : IFormatter
                {
                    public short PacketLen
                    {   // This is an approximate value... only used for reading bytes in test client
                        get { return 4000; }
                    }

                    /// <summary>
                    /// Since the packet format between Flash and Pulse are the same, we will
                    /// use this method for both.
                    /// </summary>
                    public byte[] Format(LTGFlash flash, int? pulseIndex, PartnerData partnerData)
                    {
                        // This method will be called with pulse since the format is the same so look for pulseIndex
                        // and use the pulse object when there is an index value.
                        FlashData flashData = (pulseIndex.HasValue ? (FlashData)flash.FlashPortionList[pulseIndex.Value] : flash);

                        StringBuilder asciiText = new StringBuilder();
                        asciiText.Append((byte)GetResponseFlashType(flashData.FlashType, partnerData.ShowSource));

                        // Let's make sure the output format matches the specification by using the AsciiFormatTemplates.Time
                        DateTime flashTime = DateTime.Parse(flashData.FlashTime);
                        asciiText.Append(AsciiFormatTemplates.Delimiter).Append(flashTime.ToString(AsciiFormatTemplates.Time));

                        CreateAsciiSignedValue(flashData.Latitude, asciiText, AsciiFormatTemplates.Latitude);
                        CreateAsciiSignedValue(flashData.Longitude, asciiText, AsciiFormatTemplates.Longitude);
                        CreateAsciiSignedValue(flashData.Amplitude, asciiText, AsciiFormatTemplates.Amplitude);

                        // Add LocationError (if allowed)
                        asciiText.Append(AsciiFormatTemplates.Delimiter);
                        asciiText.Append(IsAllowedDataElement(partnerData, ResponseDataElement.LocationError)
                                             ? GetLocationError(flashData).ToString(AsciiFormatTemplates.LocationError)
                                             : AsciiFormatTemplates.LocationError);

                        // Add IcHeight (if allowed)
                        asciiText.Append(AsciiFormatTemplates.Delimiter);
                        asciiText.Append(IsAllowedDataElement(partnerData, ResponseDataElement.IcHeight)
                                             ? GetIcHeight(flashData).ToString(AsciiFormatTemplates.IcHeight)
                                             : AsciiFormatTemplates.IcHeight);

                        // Add NumberOfSensors (if allowed)
                        asciiText.Append(AsciiFormatTemplates.Delimiter);
                        asciiText.Append(IsAllowedDataElement(partnerData, ResponseDataElement.NumberOfSensors)
                                             ? GetNumberOfSensors(flash, pulseIndex).ToString(AsciiFormatTemplates.NumberOfSensors)
                                             : AsciiFormatTemplates.NumberOfSensors);
                        // Add Multiplicity (if allowed)
                        asciiText.Append(AsciiFormatTemplates.Delimiter);
                        asciiText.Append(IsAllowedDataElement(partnerData, ResponseDataElement.Multiplicity)
                                             ? GetMultiplicity(flash).ToString(AsciiFormatTemplates.Multiplicity)
                                             : AsciiFormatTemplates.Multiplicity);
                        asciiText.Append("\r\n");  // Let's be explicit here instead of using Environment

                        return Encoding.ASCII.GetBytes(asciiText.ToString());
                    }

                    public string ToString(byte[] packageBytes, PartnerData partnerData)
                    {
                        string[] packageList = Encoding.ASCII.GetString(packageBytes).Trim().Split(',');

                        StringBuilder asciiText = new StringBuilder();

                        asciiText.Append("[class=").Append(packageList[0]);
                        asciiText.Append("; time=").Append(packageList[1]);
                        asciiText.Append("; lat=").Append(packageList[2]);
                        asciiText.Append("; lon=").Append(packageList[3]);
                        asciiText.Append("; amp=").Append(packageList[4]);
                        asciiText.Append("; err=").Append(packageList[5]);
                        asciiText.Append("; ht=").Append(packageList[6]);
                        asciiText.Append("; sensors=").Append(packageList[7]);
                        asciiText.Append("; mult=").Append(packageList[8]);

                        return asciiText.Append("]").ToString();
                    }

                    public byte[] KeepAlive(PartnerData partnerData)
                    {
                        return Format(CreateKeepAliveFlash(), null, partnerData);
                    }
                }

                public class Binary : IFormatter
                {
                    public short PacketLen
                    {
                        get { return 26; }
                    }

                    public class PositionOffset
                    {
                        public const byte NumberOfBytes = 0;
                        public const byte LxClassificationType = 1;
                        public const byte Time = 2;
                        public const byte Millisecond = 6;
                        public const byte Latitude = 8;
                        public const byte Longitude = 12;
                        public const byte Amplitude = 16;
                        public const byte LocationError = 20;   // Publicly this is a reserved field; used by IAF
                        public const byte IcHeight = 21;
                        public const byte NumberOfSensors = 23;
                        public const byte Multiplicity = 24;
                        public const byte Checksum = 25;
                    }


                    /// <summary>
                    /// Since the packet format between Flash and Pulse are the same, we will
                    /// use this method for both.
                    /// </summary>
                    public byte[] Format(LTGFlash flash, int? pulseIndex, PartnerData partnerData)
                    {
                        // This method will be called with pulse since the format is the same so look for pulseIndex
                        // and use the pulse object when there is an index value.
                        FlashData flashData = (pulseIndex.HasValue ? (FlashData)flash.FlashPortionList[pulseIndex.Value] : flash);

                        byte[] packageBytes = new byte[PacketLen];
                        packageBytes[PositionOffset.NumberOfBytes] = Convert.ToByte(PacketLen);
                        packageBytes[PositionOffset.LxClassificationType] = (byte)GetResponseFlashType(flashData.FlashType, partnerData.ShowSource);

                        TimeSpan ts = TimeSvc.GetUnixTimeSpan(DateTime.Parse(flashData.FlashTime));

                        // Add Time & Milli's: when adding time we must remove the milli's (via Math.Floor()) so that 
                        // the seconds aren't rounded up... we already capture the milli's in another 2 bytes anyway so this is safe.
                        IntegerConverter.WriteUIntToBufferAtOffset(Convert.ToUInt32(Math.Floor(ts.TotalSeconds)), packageBytes, PositionOffset.Time, true);
                        IntegerConverter.WriteUShortToBufferAtOffset(Convert.ToUInt16(ts.Milliseconds), packageBytes, PositionOffset.Millisecond, true);

                        // Add Lat/Lon
                        int latitude = (int)(flashData.Latitude * Math.Pow(10, 7));
                        IntegerConverter.WriteIntToBufferAtOffset(latitude, packageBytes, PositionOffset.Latitude, true);

                        int longitude = (int)(flashData.Longitude * Math.Pow(10, 7));
                        IntegerConverter.WriteIntToBufferAtOffset(longitude, packageBytes, PositionOffset.Longitude, true);

                        // Add Amplitude
                        IntegerConverter.WriteIntToBufferAtOffset(Convert.ToInt32(flashData.Amplitude), packageBytes, PositionOffset.Amplitude, true);

                        // Add LocationError (if allowed)
                        if (IsAllowedDataElement(partnerData, ResponseDataElement.LocationError))
                        {
                            packageBytes[PositionOffset.LocationError] = GetLocationError(flashData);
                        }

                        // Add IcHeight (if allowed)
                        if (IsAllowedDataElement(partnerData, ResponseDataElement.IcHeight))
                        {
                            IntegerConverter.WriteUShortToBufferAtOffset(GetIcHeight(flashData), packageBytes, PositionOffset.IcHeight, true);
                        }

                        // Add NumberOfSensors (if allowed)
                        if (IsAllowedDataElement(partnerData, ResponseDataElement.NumberOfSensors))
                        {
                            packageBytes[PositionOffset.NumberOfSensors] = GetNumberOfSensors(flash, pulseIndex);
                        }

                        // Add Multiplicity (if allowed)
                        if (IsAllowedDataElement(partnerData, ResponseDataElement.Multiplicity))
                        {
                            packageBytes[PositionOffset.Multiplicity] = GetMultiplicity(flash);
                        }

                        packageBytes[PositionOffset.Checksum] = CreateChecksum(PacketLen, packageBytes);

                        return packageBytes;
                    }

                    public string ToString(byte[] packageBytes, PartnerData partnerData)
                    {
                        StringBuilder asciiText = new StringBuilder();

                        asciiText.Append("[bytes=").Append(packageBytes[PositionOffset.NumberOfBytes]);
                        asciiText.Append("; class=").Append((byte)packageBytes[PositionOffset.LxClassificationType]);

                        // Read Time and Milliseconds
                        uint? time = IntegerConverter.ReadUIntFromBufferAtOffset(packageBytes, PositionOffset.Time, true);
                        ushort? millisec = IntegerConverter.ReadUShortFromBufferAtOffset(packageBytes, PositionOffset.Millisecond, true);
                        if (time.HasValue && millisec.HasValue)
                        {
                            asciiText.Append("; time=").Append(TimeSvc.GetTime((int)time.Value).AddMilliseconds(millisec.Value).ToString(AsciiFormatTemplates.Time));
                        }

                        // Read Lat
                        int? lat = IntegerConverter.ReadIntFromBufferAtOffset(packageBytes, PositionOffset.Latitude, true);
                        if (lat.HasValue)
                        {
                            asciiText.Append("; lat=").Append(lat / Math.Pow(10, 7));
                        }

                        // Read Lon
                        int? lon = IntegerConverter.ReadIntFromBufferAtOffset(packageBytes, PositionOffset.Longitude, true);
                        if (lon.HasValue)
                        {
                            asciiText.Append("; lon=").Append(lon / Math.Pow(10, 7));
                        }

                        asciiText.Append("; amp=").Append(IntegerConverter.ReadIntFromBufferAtOffset(packageBytes, PositionOffset.Amplitude, true));
                        asciiText.Append("; err=").Append(packageBytes[PositionOffset.LocationError]);
                        asciiText.Append("; ht=").Append(IntegerConverter.ReadUShortFromBufferAtOffset(packageBytes, PositionOffset.IcHeight, true));
                        asciiText.Append("; sensors=").Append(packageBytes[PositionOffset.NumberOfSensors]);
                        asciiText.Append("; mult=").Append(packageBytes[PositionOffset.Multiplicity]);
                        asciiText.Append("; chksum=").Append(packageBytes[PositionOffset.Checksum]);

                        return asciiText.Append("]").ToString();
                    }

                    public byte[] KeepAlive(PartnerData partnerData)
                    {
                        return Format(CreateKeepAliveFlash(), null, partnerData);
                    }
                }
            }

            private class Pulse
            {
                public static Dictionary<ResponseFormatType, IFormatter> CreateFormatters()
                {
                    return new Dictionary<ResponseFormatType, IFormatter>
                           {
                               {ResponseFormatType.Ascii, new Ascii()},
                               {ResponseFormatType.Binary, new Binary()}
                           };
                }

                public class Ascii : IFormatter
                {
                    public short PacketLen
                    {
                        get { return (new Flash.Ascii()).PacketLen; }
                    }

                    public byte[] Format(LTGFlash flash, int? pulseIndex, PartnerData partnerData)
                    {   // Package format is the same for Flash and Pulse
                        return (new Flash.Ascii()).Format(flash, pulseIndex, partnerData);
                    }

                    public string ToString(byte[] packageBytes, PartnerData partnerData)
                    {
                        return (new Flash.Ascii()).ToString(packageBytes, partnerData);
                    }

                    public byte[] KeepAlive(PartnerData partnerData)
                    {
                        return (new Flash.Ascii()).KeepAlive(partnerData);
                    }
                }

                public class Binary : IFormatter
                {
                    public short PacketLen
                    {
                        get { return (new Flash.Binary()).PacketLen; }
                    }

                    public byte[] Format(LTGFlash flash, int? pulseIndex, PartnerData partnerData)
                    {   // Package format is the same for Flash and Pulse
                        return (new Flash.Binary()).Format(flash, pulseIndex, partnerData);
                    }

                    public string ToString(byte[] packageBytes, PartnerData partnerData)
                    {
                        return (new Flash.Binary()).ToString(packageBytes, partnerData);
                    }

                    public byte[] KeepAlive(PartnerData partnerData)
                    {
                        return (new Flash.Binary()).KeepAlive(partnerData);
                    }
                }
            }

            private class FlashAndPulseCombo
            {
                public static Dictionary<ResponseFormatType, IFormatter> CreateFormatters()
                {
                    return new Dictionary<ResponseFormatType, IFormatter>
                           {
                               {ResponseFormatType.Binary, new Binary()}
                           };
                }

                private class Binary : IFormatter
                {
                    public short PacketLen
                    {   // This is an approximate value... used for reading bytes in test client
                        get { return 1000; }
                    }

                    private const byte ComboHeaderPacketLen = 2;

                    private class PositionOffset
                    {
                        public const byte NumberOfBytes = 0;
                        public const byte FlashPacket = 2;
                        public const byte PulsePacket = 28;
                    }

                    public byte[] Format(LTGFlash flash, int? pulseIndex, PartnerData partnerData)
                    {
                        Flash.Binary fb = new Flash.Binary();
                        Pulse.Binary pb = new Pulse.Binary();

                        int packetSize =
                            ComboHeaderPacketLen +                            // Combo packet header length
                            1 +                                               // Checksum length in footer
                            fb.PacketLen +                                    // Flash packet length
                            (flash.FlashPortionList.Count * pb.PacketLen);    // Set of Pulse packet lengths
                        byte[] packageBytes = new byte[packetSize];

                        // Add Combo header
                        IntegerConverter.WriteUShortToBufferAtOffset((ushort)packetSize, packageBytes, PositionOffset.NumberOfBytes, true);

                        // Add Flash
                        byte[] flashBytes = fb.Format(flash, pulseIndex, partnerData);
                        flashBytes.CopyTo(packageBytes, PositionOffset.FlashPacket);

                        // Add each Pulse
                        for (int i = 0; i < flash.FlashPortionList.Count; i++)
                        {
                            byte[] pulseBytes = pb.Format(flash, i, partnerData);
                            pulseBytes.CopyTo(packageBytes, PositionOffset.PulsePacket + (pulseBytes.Length * i));
                        }

                        // Add Checksum
                        packageBytes[packageBytes.Length - 1] = CreateChecksum((short)packageBytes.Length, packageBytes);

                        return packageBytes;
                    }

                    public string ToString(byte[] packageBytes, PartnerData partnerData)
                    {
                        const string CheckSum = "^^CHECKSUM^^";
                        StringBuilder asciiText = new StringBuilder();

                        // Read number bytes
                        asciiText.Append("[bytes=").Append(IntegerConverter.ReadUShortFromBufferAtOffset(packageBytes, PositionOffset.NumberOfBytes, true));
                        asciiText.Append("; chksum=").Append(CheckSum).Append("]");

                        // Read Flash packet - Copy flash packets out of the packageBytes into it's own byte array
                        Flash.Binary fb = new Flash.Binary();
                        byte[] flashBytes = new byte[fb.PacketLen];
                        Array.Copy(packageBytes, PositionOffset.FlashPacket, flashBytes, 0, fb.PacketLen);
                        asciiText.Append(Environment.NewLine).Append(fb.ToString(flashBytes, partnerData));

                        int numMulti = flashBytes[Flash.Binary.PositionOffset.Multiplicity];
                        if (numMulti == 0)
                        {   // Check for pulses via packet size since Multiplicity will be zero when the
                            // partner doesn't have access to that data element.
                            numMulti = (packageBytes.Length - PositionOffset.PulsePacket + 1) / PositionOffset.PulsePacket;
                        }

                        // Read Pulse packet(s)
                        Pulse.Binary pb = new Pulse.Binary();
                        int nextPulseStartingPosition = 0;
                        int numPulses = ((int)IntegerConverter.ReadUShortFromBufferAtOffset(packageBytes,
                            PositionOffset.NumberOfBytes, true) - 3) / 26 - 1;
                        for (int i = 0; i < numPulses; i++)
                        {
                            byte[] pulseBytes = new byte[pb.PacketLen];
                            nextPulseStartingPosition = PositionOffset.PulsePacket + (i * pulseBytes.Length);

                            Array.Copy(packageBytes, nextPulseStartingPosition, pulseBytes, 0, pulseBytes.Length);
                            asciiText.Append(Environment.NewLine).Append(pb.ToString(pulseBytes, partnerData));
                        }

                        // Replace token with checksum
                        return asciiText.ToString().Replace(CheckSum, packageBytes[nextPulseStartingPosition + pb.PacketLen].ToString());
                    }

                    public byte[] KeepAlive(PartnerData partnerData)
                    {
                        return Format(CreateKeepAliveFlash(), null, partnerData);
                    }
                }
            }
        } 
    }
}
