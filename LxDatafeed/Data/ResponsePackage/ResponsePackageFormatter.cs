﻿using System;
using System.Collections;
using System.Collections.Generic;

using LxCommon;

using LxDatafeed.Data.Partner;

namespace LxDatafeed.Data.ResponsePackage
{
    internal partial class ResponsePackageFormatter
    {
        // Key: version; Value: dictionary of formatter objects by format type and package type
        private static readonly Dictionary<short, Dictionary<ResponsePackageType, Dictionary<ResponseFormatType, IFormatter>>> Formatters;

        static ResponsePackageFormatter()
        {   
            Formatters = new Dictionary<short, Dictionary<ResponsePackageType, Dictionary<ResponseFormatType, IFormatter>>>
                              {   // There should be one entry per response package version
                                  {1, Version1.CreateFormatters()}, 
                                  {2, Version2.CreateFormatters()},
                                  {3, Version3.CreateFormatters()}
                              };
        } 
        
        public static IFormatter GetFormatter(PartnerData partnerData)
        {
            return Formatters[partnerData.ResponsePackageVersion][partnerData.ResponsePackageType][partnerData.ResponseFormatType];
        }

        public static byte[] Format(LTGFlash flash, int? pulseIndex, PartnerData partnerData)
        {
            return GetFormatter(partnerData).Format(flash, pulseIndex, partnerData);
        }

        public static string ToString(byte[] packageBytes, PartnerData partnerData)
        {
            return GetFormatter(partnerData).ToString(packageBytes, partnerData);
        }

        public static byte[] KeepAlive(PartnerData partnerData)
        {
            return GetFormatter(partnerData).KeepAlive(partnerData);            
        }

        public interface IFormatter
        {
            byte[] Format(LTGFlash flash, int? pulseIndex, PartnerData partnerData);
            string ToString(byte[] packageBytes, PartnerData partnerData);
            byte[] KeepAlive(PartnerData partnerData);

            short PacketLen { get; }
        } 
        
        private static LTGFlash CreateKeepAliveFlash()
        {
            const int KeepAlive = 9;

            // Create dummy sensor
            List<Offset> offsetList = new List<Offset>()
            {
                new Offset("KeepAlive", 0)
            };

            // Create dummy pulse
            LTGFlashPortion pulse = new LTGFlashPortion()
            {
                FlashTime = LtgTimeUtils.GetDateTimeString(DateTime.UtcNow),
                FlashType = (FlashType)KeepAlive,
                OffsetList = offsetList
            };

            // Create dummy pulse list
            ArrayList flashPortionList = new ArrayList{ pulse };
            // Create dummy flash
            LTGFlash flash = new LTGFlash()
            {
                FlashTime = LtgTimeUtils.GetDateTimeString(DateTime.UtcNow),
                FlashType = (FlashType)KeepAlive,
                FlashPortionList = flashPortionList
            };
            
            //kludge: say it is a wwlln flash to avoid validation checks
            flash.FinalizeFlash(null, true);

            //need to reset the type since finalize will set it to an ic flash
            flash.FlashType = (FlashType)KeepAlive;

            return flash;
        }

        private static FlashType GetResponseFlashType(FlashType current, bool showSource)
        {
            FlashType ft = current;
            if (!showSource)
            {
                switch (ft)
                {
                    case FlashType.FlashTypeGlobalIC:
                        ft = FlashType.FlashTypeIC;
                        break;
                    case FlashType.FlashTypeGlobalCG:
                        ft = FlashType.FlashTypeCG;
                        break;
                }
            }

            return ft;
        }

        private static LTGFlash CreateKeepAlivePulse()
        {
            return CreateKeepAliveFlash();
        } 
        
        public class IntegerConverter
        {
            public static void WriteUIntToBufferAtOffset(uint data, byte[] packageBytes, int offset, bool isMsbFirst)
            {
                ConvertBytes(BitConverter.GetBytes(data), packageBytes, offset, isMsbFirst);
            }

            public static void WriteUShortToBufferAtOffset(ushort data, byte[] packageBytes, int offset, bool isMsbFirst)
            {
                ConvertBytes(BitConverter.GetBytes(data), packageBytes, offset, isMsbFirst);
            }

            public static void WriteIntToBufferAtOffset(int data, byte[] packageBytes, int offset, bool isMsbFirst)
            {
                ConvertBytes(BitConverter.GetBytes(data), packageBytes, offset, isMsbFirst);
            }

            internal static void WriteLongToBufferAtOffset(long data, byte[] packageBytes, byte offset, bool isMsbFirst)
            {
                ConvertBytes(BitConverter.GetBytes(data), packageBytes, offset, isMsbFirst);
            }

            private static void ConvertBytes(byte[] sourceBytes, byte[] destinationBytes, int offset, bool isMsbFirst)
            {
                if (isMsbFirst)
                {
                    Array.Reverse(sourceBytes);
                }
                sourceBytes.CopyTo(destinationBytes, offset);
            } 
        
            public static uint? ReadUIntFromBufferAtOffset(byte[] packageBytes, int offset, bool isMsbFirst)
            {
                byte[] bytes = ReadBytes(packageBytes, offset, isMsbFirst, 4);
                return (bytes != null ? BitConverter.ToUInt32(bytes, 0) : (uint?)null);
            }

            public static ushort? ReadUShortFromBufferAtOffset(byte[] packageBytes, int offset, bool isMsbFirst)
            {
                byte[] bytes = ReadBytes(packageBytes, offset, isMsbFirst, 2);
                return (bytes != null ? BitConverter.ToUInt16(bytes, 0) : (ushort?)null);
            }

            public static int? ReadIntFromBufferAtOffset(byte[] packageBytes, int offset, bool isMsbFirst)
            {
                byte[] bytes = ReadBytes(packageBytes, offset, isMsbFirst, 4);
                return (bytes != null ? BitConverter.ToInt32(bytes, 0) : (int?)null);
            }

            public static long? ReadLongFromBufferAtOffset(byte[] packageBytes, int offset, bool isMsbFirst)
            {
                byte[] bytes = ReadBytes(packageBytes, offset, isMsbFirst, 8);
                return (bytes != null ? BitConverter.ToInt64(bytes, 0) : (long?)null);
            }

            private static byte[] ReadBytes(byte[] packageBytes, int offset, bool isMsbFirst, int dataTypeSize)
            {
                byte[] relevantBytes = null;

                if (packageBytes.Length > (offset + dataTypeSize - 1))
                {
                    relevantBytes = new byte[dataTypeSize];
                    Array.Copy(packageBytes, offset, relevantBytes, 0, dataTypeSize);
                    if (isMsbFirst)
                    {
                        Array.Reverse(relevantBytes);
                    }
                }

                return relevantBytes;
            }
        }
    }
}
