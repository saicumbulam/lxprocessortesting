﻿using System;
using System.Collections.Generic;
using System.Text;

using Aws.Core.Utilities;

using LxCommon;

using LxDatafeed.Data.Partner;

namespace LxDatafeed.Data.ResponsePackage
{
    internal partial class ResponsePackageFormatter
    {
        private class Version1
        {
            private const char AsciiDelimiter = ',';
            private const string TimeFormatTemplate = "yyyyMMddTHHmmss.fff";
            private const string LatitudeFormatTemplate = "{0:00.0000000}";
            private const string LongitudeFormatTemplate = "{0:000.0000000}";
            private const string AmplitudeFormatTemplate = "{0:000000000}";

            public static Dictionary<ResponsePackageType, Dictionary<ResponseFormatType, IFormatter>> CreateFormatters()
            {
                return new Dictionary<ResponsePackageType, Dictionary<ResponseFormatType, IFormatter>>
                           {   // There should be one entry per response package type
                               {ResponsePackageType.Flash, Flash.CreateFormatters()},
                               {ResponsePackageType.Pulse, Pulse.CreateFormatters()},
                               {ResponsePackageType.FlashAndPulseCombo, FlashAndPulseCombo.CreateFormatters()}
                           };
            }

            private static byte CreateChecksum(short packetLen, byte[] packageBytes)
            {
                byte byteSum = 0;
                for (int i = 0; i < (packetLen - 1); i++)
                {
                    byteSum += packageBytes[i];
                }
                return (byte)(256 - byteSum);
            }

            private static void CreateAsciiSignedValue(double coordinate, StringBuilder asciiText, string formatTemplate)
            {
                asciiText.Append(AsciiDelimiter);
                if (coordinate >= 0)
                {
                    asciiText.Append('+');
                }
                else
                {
                    asciiText.Append('-');
                }
                asciiText.Append(string.Format(formatTemplate, Math.Abs(coordinate)));
            }

            public class Flash
            {
                public static Dictionary<ResponseFormatType, IFormatter> CreateFormatters()
                {
                    return new Dictionary<ResponseFormatType, IFormatter>
                               {
                               {ResponseFormatType.Ascii, new Ascii()},
                               {ResponseFormatType.Binary, new Binary()}
                           };
                }

                public class Ascii : IFormatter
                {
                    public short PacketLen
                    {   // This is an approximate value... only used for reading bytes in test client
                        get { return 65; }
                    }

                    /// <summary>
                    /// Since the packet format between Flash and Pulse are the same, we will
                    /// use this method for both.
                    /// </summary>
                    public byte[] Format(LTGFlash flash, int? pulseIndex, PartnerData partnerData)
                    {
                        // We need to determine if we are build packet for flash or pulse. It will be pulse
                        // when pulseIndex has a value. 
                        FlashData flashData = (pulseIndex.HasValue ? (FlashData)flash.FlashPortionList[pulseIndex.Value] : flash);

                        StringBuilder asciiText = new StringBuilder();
                        asciiText.Append((byte)GetResponseFlashType(flashData.FlashType, partnerData.ShowSource));

                        // Let's make sure the output format matches the specification
                        DateTime flashTime = DateTime.Parse(flashData.FlashTime);
                        asciiText.Append(AsciiDelimiter).Append(flashTime.ToString(TimeFormatTemplate));

                        CreateAsciiSignedValue(flashData.Latitude, asciiText, LatitudeFormatTemplate);
                        CreateAsciiSignedValue(flashData.Longitude, asciiText, LongitudeFormatTemplate);
                        CreateAsciiSignedValue(flashData.Amplitude, asciiText, AmplitudeFormatTemplate);
                        asciiText.Append("\r\n");

                        return Encoding.ASCII.GetBytes(asciiText.ToString());
                    }

                    public string ToString(byte[] packageBytes, PartnerData partnerData)
                    {
                        return Encoding.ASCII.GetString(packageBytes);
                    }

                    public byte[] KeepAlive(PartnerData partnerData)
                    {
                        return Format(CreateKeepAliveFlash(), null, partnerData);
                    }
                }

                public class Binary : IFormatter
                {
                    public short PacketLen
                    {
                        get { return 24; }
                    }

                    private class PositionOffset
                    {
                        public const byte NumberOfBytes = 0;
                        public const byte LxClassificationType = 1;
                        public const byte Time = 2;
                        public const byte Millisecond = 6;
                        public const byte Latitude = 8;
                        public const byte Longitude = 12;
                        public const byte Amplitude = 16;
                        // 20-22 are reserved
                        public const byte Checksum = 23;
                    }

                    /// <summary>
                    /// Since the packet format between Flash and Pulse are the same, we will
                    /// use this method for both.
                    /// </summary>
                    public byte[] Format(LTGFlash flash, int? pulseIndex, PartnerData partnerData)
                    {
                        // We need to determine if we are build packet for flash or pulse. It will be pulse
                        // when pulseIndex has a value. 
                        FlashData flashData = (pulseIndex.HasValue ? (FlashData)flash.FlashPortionList[pulseIndex.Value] : flash);

                        byte[] packageBytes = new byte[PacketLen];
                        packageBytes[PositionOffset.NumberOfBytes] = Convert.ToByte(PacketLen);
                        packageBytes[PositionOffset.LxClassificationType] = (byte)GetResponseFlashType(flashData.FlashType, partnerData.ShowSource);

                        TimeSpan ts = TimeSvc.GetUnixTimeSpan(DateTime.Parse(flashData.FlashTime));

                        // Add Time & Milli's: when adding time we must remove the milli's (via Math.Floor()) so that 
                        // the seconds aren't rounded up... we already capture the milli's in another 2 bytes anyway so this is safe.
                        IntegerConverter.WriteUIntToBufferAtOffset(Convert.ToUInt32(Math.Floor(ts.TotalSeconds)), packageBytes, PositionOffset.Time, true);
                        IntegerConverter.WriteUShortToBufferAtOffset(Convert.ToUInt16(ts.Milliseconds), packageBytes, PositionOffset.Millisecond, true);

                        int latitude = (int)(flashData.Latitude * Math.Pow(10, 7));
                        IntegerConverter.WriteIntToBufferAtOffset(latitude, packageBytes, PositionOffset.Latitude, true);

                        int longitude = (int)(flashData.Longitude * Math.Pow(10, 7));
                        IntegerConverter.WriteIntToBufferAtOffset(longitude, packageBytes, PositionOffset.Longitude, true);

                        IntegerConverter.WriteIntToBufferAtOffset(Convert.ToInt32(flashData.Amplitude), packageBytes, PositionOffset.Amplitude, true);

                        packageBytes[PositionOffset.Checksum] = CreateChecksum(PacketLen, packageBytes);

                        return packageBytes;
                    }

                    public string ToString(byte[] packageBytes, PartnerData partnerData)
                    {
                        StringBuilder asciiText = new StringBuilder();

                        asciiText.Append(packageBytes[PositionOffset.NumberOfBytes]);
                        asciiText.Append(AsciiDelimiter).Append(packageBytes[PositionOffset.LxClassificationType]);

                        // Read Time and Milliseconds
                        asciiText.Append(AsciiDelimiter);
                        uint? time = IntegerConverter.ReadUIntFromBufferAtOffset(packageBytes, PositionOffset.Time, true);
                        ushort? millisec = IntegerConverter.ReadUShortFromBufferAtOffset(packageBytes, PositionOffset.Millisecond, true);
                        if (time.HasValue && millisec.HasValue)
                        {
                            asciiText.Append(TimeSvc.GetTime((int)time.Value).AddMilliseconds(millisec.Value).ToString(TimeFormatTemplate));
                        }

                        // Read Lat
                        asciiText.Append(AsciiDelimiter);
                        int? lat = IntegerConverter.ReadIntFromBufferAtOffset(packageBytes, PositionOffset.Latitude, true);
                        if (lat.HasValue)
                        {
                            asciiText.Append(lat / Math.Pow(10, 7));
                        }

                        // Read Lon
                        asciiText.Append(AsciiDelimiter);
                        int? lon = IntegerConverter.ReadIntFromBufferAtOffset(packageBytes, PositionOffset.Longitude, true);
                        if (lon.HasValue)
                        {
                            asciiText.Append(lon / Math.Pow(10, 7));
                        }

                        asciiText.Append(AsciiDelimiter).Append(IntegerConverter.ReadIntFromBufferAtOffset(packageBytes, PositionOffset.Amplitude, true));
                        asciiText.Append(AsciiDelimiter).Append(packageBytes[PositionOffset.Checksum]);

                        return asciiText.ToString();
                    }

                    public byte[] KeepAlive(PartnerData partnerData)
                    {
                        return Format(CreateKeepAliveFlash(), null, partnerData);
                    }
                }
            }

            public class Pulse
            {
                public static Dictionary<ResponseFormatType, IFormatter> CreateFormatters()
                {
                    return new Dictionary<ResponseFormatType, IFormatter>
                               {
                               {ResponseFormatType.Ascii, new Ascii()},
                               {ResponseFormatType.Binary, new Binary()}
                           };
                }

                public class Ascii : IFormatter
                {
                    public short PacketLen
                    {
                        get { return (new Flash.Ascii()).PacketLen; }
                    }

                    public byte[] Format(LTGFlash flash, int? pulseIndex, PartnerData partnerData)
                    {   // Package format is the same for Flash and Pulse
                        return (new Flash.Ascii()).Format(flash, pulseIndex, partnerData);
                    }

                    public string ToString(byte[] packageBytes, PartnerData partnerData)
                    {
                        return (new Flash.Ascii()).ToString(packageBytes, partnerData);
                    }

                    public byte[] KeepAlive(PartnerData partnerData)
                    {
                        return (new Flash.Ascii()).KeepAlive(partnerData);
                    }
                }

                public class Binary : IFormatter
                {
                    public short PacketLen
                    {
                        get { return (new Flash.Binary()).PacketLen; }
                    }

                    public byte[] Format(LTGFlash flash, int? pulseIndex, PartnerData partnerData)
                    {   // Package format is the same for Flash and Pulse
                        return (new Flash.Binary()).Format(flash, pulseIndex, partnerData);
                    }

                    public string ToString(byte[] packageBytes, PartnerData partnerData)
                    {
                        return (new Flash.Binary()).ToString(packageBytes, partnerData);
                    }

                    public byte[] KeepAlive(PartnerData partnerData)
                    {
                        return (new Flash.Binary()).KeepAlive(partnerData);
                    }
                }
            }

            private class FlashAndPulseCombo
            {
                public static Dictionary<ResponseFormatType, IFormatter> CreateFormatters()
                {
                    return new Dictionary<ResponseFormatType, IFormatter>
                               {
                               {ResponseFormatType.Binary, new Binary()}
                           };
                }

                private class Binary : IFormatter
                {
                    public short PacketLen
                    {   // This is an approximate value... used for reading bytes in test client
                        get { return 1000; }
                    }

                    private const byte ComboHeaderPacketLen = 7;

                    private class PositionOffset
                    {
                        public const byte NumberOfBytes = 0;
                        public const byte ComboRequestType = 2;
                        public const byte NumberOfPulses = 3;
                        // 4-6 are reserved
                        public const byte FlashPacket = 7;
                        public const byte PulsePacket = 31;
                    }

                    public byte[] Format(LTGFlash flash, int? pulseIndex, PartnerData partnerData)
                    {
                        Flash.Binary fb = new Flash.Binary();
                        Pulse.Binary pb = new Pulse.Binary();

                        int packetSize =
                            ComboHeaderPacketLen +                                      // Combo packet header length
                            1 +                                                         // Checksum length in footer
                            fb.PacketLen +                                    // Flash packet length
                            (flash.FlashPortionList.Count * pb.PacketLen);    // Set of Pulse packet lengths
                        byte[] packageBytes = new byte[packetSize];

                        // Add Combo header
                        IntegerConverter.WriteUShortToBufferAtOffset((ushort)packetSize, packageBytes, PositionOffset.NumberOfBytes, true);
                        packageBytes[PositionOffset.ComboRequestType] = 115;
                        packageBytes[PositionOffset.NumberOfPulses] = (byte)flash.FlashPortionList.Count;

                        // Add Flash
                        byte[] flashBytes = fb.Format(flash, pulseIndex, partnerData);
                        flashBytes.CopyTo(packageBytes, PositionOffset.FlashPacket);

                        // Add each Pulse
                        for (int i = 0; i < flash.FlashPortionList.Count; i++)
                        {
                            byte[] pulseBytes = pb.Format(flash, i, partnerData);
                            pulseBytes.CopyTo(packageBytes, PositionOffset.PulsePacket + (pulseBytes.Length * i));
                        }

                        // Add Checksum
                        packageBytes[packageBytes.Length - 1] = CreateChecksum((short)packageBytes.Length, packageBytes);

                        return packageBytes;
                    }

                    public string ToString(byte[] packageBytes, PartnerData partnerData)
                    {
                        const string CheckSum = "^^CHECKSUM^^";
                        StringBuilder asciiText = new StringBuilder();

                        // Read number bytes
                        asciiText.Append("[bytes=").Append(IntegerConverter.ReadUShortFromBufferAtOffset(packageBytes, PositionOffset.NumberOfBytes, true));
                        asciiText.Append("; rt=").Append(packageBytes[PositionOffset.ComboRequestType]);

                        int numMulti = packageBytes[PositionOffset.NumberOfBytes];
                        asciiText.Append("; multi=").Append(numMulti);
                        asciiText.Append("; chksum=").Append(CheckSum).Append("]");

                        // Read Flash packet - Copy flash packets out of the packageBytes into it's own byte array
                        Flash.Binary fb = new Flash.Binary();
                        byte[] flashBytes = new byte[fb.PacketLen];
                        Array.Copy(packageBytes, PositionOffset.FlashPacket, flashBytes, 0, fb.PacketLen);
                        asciiText.Append(Environment.NewLine).Append(fb.ToString(flashBytes, partnerData));

                        // Read Pulse packet(s)
                        Pulse.Binary pb = new Pulse.Binary();
                        int nextPulseStartingPosition = 0;
                        for (int i = 0; i < numMulti; i++)
                        {
                            byte[] pulseBytes = new byte[pb.PacketLen];
                            nextPulseStartingPosition = PositionOffset.PulsePacket + (i * pulseBytes.Length);

                            Array.Copy(packageBytes, nextPulseStartingPosition, pulseBytes, 0, pulseBytes.Length);
                            asciiText.Append(Environment.NewLine).Append(pb.ToString(pulseBytes, partnerData));
                        }

                        // Replace token with checksum
                        return asciiText.ToString().Replace(CheckSum, packageBytes[nextPulseStartingPosition + pb.PacketLen].ToString());
                    }

                    public byte[] KeepAlive(PartnerData partnerData)
                    {
                        return Format(CreateKeepAliveFlash(), null, partnerData);
                    }
                }
            }
        } 
    }
}
