﻿namespace LxDatafeed.Data.ResponsePackage
{
    /// <remarks>
    /// Mapped to db table lxdf.ResponsePackageType
    /// </remarks>
    internal enum ResponsePackageType : byte
    {
        NotDefined = 0,
        Flash=1,
        Pulse=2,
        FlashAndPulseCombo=3
    }
}
