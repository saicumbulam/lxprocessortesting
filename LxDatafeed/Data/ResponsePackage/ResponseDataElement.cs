﻿namespace LxDatafeed.Data.ResponsePackage
{
    /// <remarks>
    /// Mapped to db table lxdf.ResponseDataElement
    /// </remarks>
    internal enum ResponseDataElement : short
    {
        NotDefined = 0,
        IcHeight=1,
        NumberOfSensors=2,
        Multiplicity=3,
        LocationError=4
    }
}
