﻿namespace LxDatafeed.Data.ResponsePackage
{
    /// <remarks>
    /// Mapped to db table lxdf.ResponseFormatType
    /// </remarks>
    internal enum ResponseFormatType : byte
    {
        NotDefined = 0,
        Ascii=1,
        Binary=2
    }
}
