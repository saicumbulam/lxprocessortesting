﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

using Newtonsoft.Json;

using Aws.Core.Utilities;

using LxCommon;
using LxCommon.Models;

using LxDatafeed.Data.Partner;

namespace LxDatafeed.Data.ResponsePackage
{
    internal partial class ResponsePackageFormatter
    {
        public class Version3
        {
            private const int BinaryFlashPacketLength = 56;
            private const int BinaryPulsePacketLength = 32;

            public static Dictionary<ResponsePackageType, Dictionary<ResponseFormatType, IFormatter>> CreateFormatters()
            {
                return new Dictionary<ResponsePackageType, Dictionary<ResponseFormatType, IFormatter>>
                           {   // There should be one entry per response package type
                               {ResponsePackageType.Flash, Flash.CreateFormatters()},
                               {ResponsePackageType.Pulse, Pulse.CreateFormatters()},
                               {ResponsePackageType.FlashAndPulseCombo, FlashAndPulseCombo.CreateFormatters()}
                           };
            }

            private static byte CreateChecksum(short packetLen, byte[] packageBytes)
            {
                byte bytesum = 0;
                for (int ct = 0; ct < (packetLen - 1); ++ct)
                {
                    bytesum += packageBytes[ct];
                }
                return (byte)(256 - bytesum);
            }

            //private static bool IsAllowedDataElement(PartnerData partnerData, ResponseDataElement responseDataElement)
            //{
            //    return (partnerData.ResponseDataElements != null && partnerData.ResponseDataElements.Contains(responseDataElement));
            //}

            private static byte GetByteFromInt(int value)
            {
                byte b;
                if (value < Byte.MinValue)
                {
                    b = Byte.MinValue;
                }
                else if (value > Byte.MaxValue)
                {
                    b = Byte.MaxValue;
                }
                else
                {
                    b = (byte)value;
                }
                return b;
            }

            private static ushort GetUInt16FromDouble(double value)
            {
                ushort us;
                if (value < UInt16.MinValue)
                {
                    us = UInt16.MinValue;
                }
                else if (value > UInt16.MaxValue)
                {
                    us = UInt16.MaxValue;
                }
                else
                {
                    us = (ushort)value;
                }

                return us;
            }

            private static uint GetUInt32FromDecinmal(decimal value)
            {
                uint ui;

                if (value < UInt32.MinValue)
                {
                    ui = UInt32.MinValue;
                }
                else if (value > UInt32.MaxValue)
                {
                    ui = UInt32.MaxValue;
                }
                else
                {
                    ui = (UInt32)value;
                }

                return ui;
            }

            public class PositionOffset
            {
                //common
                public const byte NumberOfBytes = 0;
                public const byte LxClassificationType = 1;
                public const byte Time = 2;
                public const byte Nanosecond = 6;
                public const byte Latitude = 10;
                public const byte Longitude = 14;
                public const byte Amplitude = 18;
                public const byte IcHeight = 22;
                public const byte NumberOfSensors = 24;
                //flash
                public const byte IcMultiplicity = 25;
                public const byte CgMultiplicity = 26;
                public const byte StartTime = 27;
                public const byte StartTimeNanosecond = 31;
                public const byte Duration = 35;
                public const byte UpperLeftLatitude = 39;
                public const byte UpperLeftLongitude = 43;
                public const byte LowerRightLatitude = 47;
                public const byte LowerRightLongitude = 51;
                public const byte FlashChecksum = 55;
                //portion
                public const byte ErrorEllipseMajorAxis = 25;
                public const byte ErrorEllipseMinorAxis = 27;
                public const byte ErrorEllipseBearing = 29;
                public const byte PortionChecksum = 31;
            }

            public static byte[] CreateCommonBinaryFormat(byte[] packageBytes, LTGFlash flash, int? pulseIndex, PartnerData partnerData)
            {
                // This method will be called with pulse since the format is the same so look for pulseIndex
                // and use the pulse object when there is an index value.
                FlashData flashData = (pulseIndex.HasValue ? (FlashData)flash.FlashPortionList[pulseIndex.Value] : flash);


                packageBytes[PositionOffset.NumberOfBytes] = Convert.ToByte(packageBytes.Length);
                packageBytes[PositionOffset.LxClassificationType] = (byte)GetResponseFlashType(flashData.FlashType, partnerData.ShowSource);

                TimeSpan ts = TimeSvc.GetUnixTimeSpan(flashData.TimeStamp.BaseTime);
                IntegerConverter.WriteUIntToBufferAtOffset(Convert.ToUInt32(Math.Floor(ts.TotalSeconds)), packageBytes, PositionOffset.Time, true);
                IntegerConverter.WriteUIntToBufferAtOffset(Convert.ToUInt32(flashData.TimeStamp.NanosecondsFromBaseTime), packageBytes, PositionOffset.Nanosecond, true);

                // Add Lat/Lon
                int latitude = (int)(flashData.Latitude * Math.Pow(10, 7));
                IntegerConverter.WriteIntToBufferAtOffset(latitude, packageBytes, PositionOffset.Latitude, true);

                int longitude = (int)(flashData.Longitude * Math.Pow(10, 7));
                IntegerConverter.WriteIntToBufferAtOffset(longitude, packageBytes, PositionOffset.Longitude, true);

                // Add Amplitude
                IntegerConverter.WriteIntToBufferAtOffset(Convert.ToInt32(flashData.Amplitude), packageBytes, PositionOffset.Amplitude, true);

                IntegerConverter.WriteUShortToBufferAtOffset(GetUInt16FromDouble(flashData.Height), packageBytes, PositionOffset.IcHeight, true);

                return packageBytes;
            }

            private static void CreateCommonJson(JsonTextWriter writer, FlashData flash, PartnerData partnerData)
            {
                writer.WritePropertyName("time");
                writer.WriteValue(flash.TimeStamp.TimeString + "Z");

                writer.WritePropertyName("type");
                writer.WriteValue(GetResponseFlashType(flash.FlashType, partnerData.ShowSource));

                writer.WritePropertyName("latitude");
                writer.WriteValue(flash.Latitude);

                writer.WritePropertyName("longitude");
                writer.WriteValue(flash.Longitude);

                writer.WritePropertyName("peakCurrent");
                writer.WriteValue(flash.Amplitude);

                writer.WritePropertyName("icHeight");
                writer.WriteValue(flash.Height);
            }

            private static void CommonToString(byte[] packageBytes, StringBuilder asciiText)
            {
                asciiText.Append("[bytes=").Append(packageBytes[PositionOffset.NumberOfBytes]);
                asciiText.Append("; class=").Append((byte)packageBytes[PositionOffset.LxClassificationType]);

                // Read Time and Milliseconds
                uint? time = IntegerConverter.ReadUIntFromBufferAtOffset(packageBytes, PositionOffset.Time, true);
                long? nano = IntegerConverter.ReadUIntFromBufferAtOffset(packageBytes, PositionOffset.Nanosecond, true);
                if (time.HasValue && nano.HasValue)
                {
                    DateTimeUtcNano nanoTime;
                    if ((int)time.Value == 0)
                    {
                        nanoTime = new DateTimeUtcNano(0);
                    }
                    else
                    {
                        nanoTime = new DateTimeUtcNano(TimeSvc.GetTime((int)time.Value), (int)nano.Value);
                    }
                    asciiText.Append("; time=").Append(nanoTime);
                }

                // Read Lat
                int? lat = IntegerConverter.ReadIntFromBufferAtOffset(packageBytes, PositionOffset.Latitude, true);
                if (lat.HasValue)
                {
                    asciiText.Append("; lat=").Append(lat / Math.Pow(10, 7));
                }

                // Read Lon
                int? lon = IntegerConverter.ReadIntFromBufferAtOffset(packageBytes, PositionOffset.Longitude, true);
                if (lon.HasValue)
                {
                    asciiText.Append("; lon=").Append(lon / Math.Pow(10, 7));
                }

                asciiText.Append("; amp=").Append(IntegerConverter.ReadIntFromBufferAtOffset(packageBytes, PositionOffset.Amplitude, true));
                asciiText.Append("; ht=").Append(IntegerConverter.ReadUShortFromBufferAtOffset(packageBytes, PositionOffset.IcHeight, true));
                asciiText.Append("; sensors=").Append(packageBytes[PositionOffset.NumberOfSensors]);
            }

            private class Flash
            {
                public static Dictionary<ResponseFormatType, IFormatter> CreateFormatters()
                {
                    return new Dictionary<ResponseFormatType, IFormatter>
                           {
                               {ResponseFormatType.Ascii, new Ascii()},
                               {ResponseFormatType.Binary, new Binary()}
                           };
                }

                public class Ascii : IFormatter
                {
                    public short PacketLen
                    {   // This is an approximate value... only used for reading bytes in test client
                        get { return 4000; }
                    }

                    public byte[] Format(LTGFlash flash, int? pulseIndex, PartnerData partnerData)
                    {
                        string payload = String.Empty;

                        using (StringWriter sw = new StringWriter())
                        {
                            using (JsonTextWriter writer = new JsonTextWriter(sw))
                            {
                                CreateFlashJson(writer, flash, partnerData);
                            }
                            payload = sw.ToString();
                        }

                        int size = Encoding.UTF8.GetByteCount(payload) + 4;
                        byte[] packageBytes = new byte[size];
                        IntegerConverter.WriteIntToBufferAtOffset(size, packageBytes, 0, true);
                        Encoding.UTF8.GetBytes(payload, 0, payload.Length, packageBytes, 4);

                        return packageBytes;
                    }

                    internal static void CreateFlashJson(JsonTextWriter writer, LTGFlash flash, PartnerData partnerData)
                    {
                        writer.WriteStartObject();

                        CreateCommonJson(writer, flash, partnerData);

                        writer.WritePropertyName("numSensors");
                        writer.WriteValue(flash.NumberSensors);

                        writer.WritePropertyName("icMultiplicity");
                        writer.WriteValue(flash.IcMultiplicity);

                        writer.WritePropertyName("cgMultiplicity");
                        writer.WriteValue(flash.CgMultiplicity);

                        if (partnerData.Metadata)
                        {
                            writer.WritePropertyName("startTime");
                            writer.WriteValue(flash.StartTime + "Z");

                            writer.WritePropertyName("duration");
                            long duration = (long)(flash.DurationSeconds * DateTimeUtcNano.NanosecondsPerSecond);
                            writer.WriteValue(duration);

                            writer.WritePropertyName("ulLatitude");
                            writer.WriteValue(flash.MaxLatitude);

                            writer.WritePropertyName("ulLongitude");
                            writer.WriteValue(flash.MinLongitude);

                            writer.WritePropertyName("lrLatitude");
                            writer.WriteValue(flash.MinLatitude);

                            writer.WritePropertyName("lrLongitude");
                            writer.WriteValue(flash.MaxLongitude);
                        }

                        writer.WriteEndObject();
                    }

                    public string ToString(byte[] packageBytes, PartnerData partnerData)
                    {
                        return Encoding.UTF8.GetString(packageBytes, 4, packageBytes.Length - 4);
                    }

                    public byte[] KeepAlive(PartnerData partnerData)
                    {
                        return Format(CreateKeepAliveFlash(), null, partnerData);
                    }
                }

                public class Binary : IFormatter
                {
                    public short PacketLen
                    {
                        get { return BinaryFlashPacketLength; }
                    }

                    public byte[] Format(LTGFlash flash, int? pulseIndex, PartnerData partnerData)
                    {
                        byte[] packageBytes = new byte[PacketLen];
                        packageBytes = CreateCommonBinaryFormat(packageBytes, flash, pulseIndex, partnerData);

                        packageBytes[PositionOffset.NumberOfSensors] = GetByteFromInt(flash.NumberSensors);

                        packageBytes[PositionOffset.IcMultiplicity] = GetByteFromInt(flash.IcMultiplicity);
                        packageBytes[PositionOffset.CgMultiplicity] = GetByteFromInt(flash.CgMultiplicity);

                        if (partnerData.Metadata)
                        {
                            DateTimeUtcNano start = new DateTimeUtcNano(flash.StartTime);
                            TimeSpan tss = TimeSvc.GetUnixTimeSpan(start.BaseTime);
                            IntegerConverter.WriteUIntToBufferAtOffset(Convert.ToUInt32(Math.Floor(tss.TotalSeconds)), packageBytes, PositionOffset.StartTime, true);
                            IntegerConverter.WriteUIntToBufferAtOffset(Convert.ToUInt32(start.NanosecondsFromBaseTime), packageBytes, PositionOffset.StartTimeNanosecond, true);

                            uint duration = GetUInt32FromDecinmal(Math.Floor(flash.DurationSeconds * DateTimeUtcNano.NanosecondsPerSecond));
                            IntegerConverter.WriteUIntToBufferAtOffset(duration, packageBytes, PositionOffset.Duration, true);

                            int ullat = (int)(flash.MaxLatitude * Math.Pow(10, 7));
                            IntegerConverter.WriteIntToBufferAtOffset(ullat, packageBytes, PositionOffset.UpperLeftLatitude, true);

                            int ullon = (int)(flash.MinLongitude * Math.Pow(10, 7));
                            IntegerConverter.WriteIntToBufferAtOffset(ullon, packageBytes, PositionOffset.UpperLeftLongitude, true);

                            int lrlat = (int)(flash.MinLatitude * Math.Pow(10, 7));
                            IntegerConverter.WriteIntToBufferAtOffset(lrlat, packageBytes, PositionOffset.LowerRightLatitude, true);

                            int lrlon = (int)(flash.MaxLongitude * Math.Pow(10, 7));
                            IntegerConverter.WriteIntToBufferAtOffset(lrlon, packageBytes, PositionOffset.LowerRightLongitude, true);
                        }

                        packageBytes[PositionOffset.FlashChecksum] = CreateChecksum(PacketLen, packageBytes);

                        return packageBytes;
                    }

                    public string ToString(byte[] packageBytes, PartnerData partnerData)
                    {
                        StringBuilder asciiText = new StringBuilder();

                        CommonToString(packageBytes, asciiText);

                        asciiText.Append("; icm=").Append(packageBytes[PositionOffset.IcMultiplicity]);
                        asciiText.Append("; cgm=").Append(packageBytes[PositionOffset.CgMultiplicity]);

                        uint? time = IntegerConverter.ReadUIntFromBufferAtOffset(packageBytes, PositionOffset.StartTime, true);
                        uint? nano = IntegerConverter.ReadUIntFromBufferAtOffset(packageBytes, PositionOffset.StartTimeNanosecond, true);
                        if (time.HasValue && nano.HasValue)
                        {
                            DateTimeUtcNano nanoTime;
                            if ((int)time.Value == 0)
                            {
                                nanoTime = new DateTimeUtcNano(0);
                            }
                            else
                            {
                                nanoTime = new DateTimeUtcNano(TimeSvc.GetTime((int)time.Value), (int)nano.Value);
                            }
                            asciiText.Append("; start=").Append(nanoTime);
                        }

                        asciiText.Append("; dur=").Append(IntegerConverter.ReadUIntFromBufferAtOffset(packageBytes, PositionOffset.Duration, true));

                        int? ullat = IntegerConverter.ReadIntFromBufferAtOffset(packageBytes, PositionOffset.UpperLeftLatitude, true);
                        if (ullat.HasValue)
                        {
                            asciiText.Append("; ullat=").Append(ullat.Value / Math.Pow(10, 7));
                        }

                        int? ullon = IntegerConverter.ReadIntFromBufferAtOffset(packageBytes, PositionOffset.UpperLeftLongitude, true);
                        if (ullon.HasValue)
                        {
                            asciiText.Append("; ullon=").Append(ullon.Value / Math.Pow(10, 7));
                        }

                        int? lrlat = IntegerConverter.ReadIntFromBufferAtOffset(packageBytes, PositionOffset.LowerRightLatitude, true);
                        if (lrlat.HasValue)
                        {
                            asciiText.Append("; lrlat=").Append(lrlat.Value / Math.Pow(10, 7));
                        }

                        int? lrlon = IntegerConverter.ReadIntFromBufferAtOffset(packageBytes, PositionOffset.LowerRightLongitude, true);
                        if (lrlon.HasValue)
                        {
                            asciiText.Append("; lrlon=").Append(lrlon.Value / Math.Pow(10, 7));
                        }


                        asciiText.Append("; chksum=").Append(packageBytes[PositionOffset.FlashChecksum]);

                        return asciiText.Append("]").ToString();
                    }

                    public byte[] KeepAlive(PartnerData partnerData)
                    {
                        return Format(CreateKeepAliveFlash(), null, partnerData);
                    }
                }
            }

            private class Pulse
            {
                public static Dictionary<ResponseFormatType, IFormatter> CreateFormatters()
                {
                    return new Dictionary<ResponseFormatType, IFormatter>
                           {
                               {ResponseFormatType.Ascii, new Ascii()},
                               {ResponseFormatType.Binary, new Binary()}
                           };
                }

                public class Ascii : IFormatter
                {
                    public short PacketLen
                    {
                        get { return (new Flash.Ascii()).PacketLen; }
                    }

                    public byte[] Format(LTGFlash flash, int? pulseIndex, PartnerData partnerData)
                    {
                        string payload = String.Empty;
                        if (pulseIndex.HasValue)
                        {
                            LTGFlashPortion portion = flash.FlashPortionList[pulseIndex.Value] as LTGFlashPortion;
                            if (portion != null)
                            {
                                using (StringWriter sw = new StringWriter())
                                {
                                    using (JsonTextWriter writer = new JsonTextWriter(sw))
                                    {
                                        CreatePortionJson(writer, portion, partnerData);
                                    }
                                    payload = sw.ToString();
                                }
                            }
                        }

                        int size = Encoding.UTF8.GetByteCount(payload) + 4;
                        byte[] packageBytes = new byte[size];
                        IntegerConverter.WriteIntToBufferAtOffset(size, packageBytes, 0, true);
                        Encoding.UTF8.GetBytes(payload, 0, payload.Length, packageBytes, 4);

                        return packageBytes;

                    }

                    private static void CreatePortionJson(JsonTextWriter writer, LTGFlashPortion portion, PartnerData partnerData)
                    {
                        writer.WriteStartObject();
                        
                        CreateCommonJson(writer, portion, partnerData);

                        writer.WritePropertyName("numSensors");
                        writer.WriteValue(portion.NumberSensors);

                        if (partnerData.Metadata)
                        {
                            if (portion.ErrorEllipse != null)
                            {
                                writer.WritePropertyName("eeMajor");
                                double meters = portion.ErrorEllipse.MajorAxis * 1000D;
                                writer.WriteValue(meters);

                                writer.WritePropertyName("eeMinor");
                                meters = portion.ErrorEllipse.MinorAxis * 1000D;
                                writer.WriteValue(meters);

                                writer.WritePropertyName("eeBearing");
                                writer.WriteValue(portion.ErrorEllipse.MajorAxisBearing);
                            }
                            else
                            {
                                writer.WritePropertyName("eeMajor");
                                writer.WriteValue(0);

                                writer.WritePropertyName("eeMinor");
                                writer.WriteValue(0);

                                writer.WritePropertyName("eeBearing");
                                writer.WriteValue(0);
                            }
                        }

                        writer.WriteEndObject();
                    }

                    public string ToString(byte[] packageBytes, PartnerData partnerData)
                    {
                        return (new Flash.Ascii()).ToString(packageBytes, partnerData);
                    }

                    public byte[] KeepAlive(PartnerData partnerData)
                    {
                        return Format(CreateKeepAlivePulse(), 0, partnerData);
                    }
                }

                public class Binary : IFormatter
                {
                    public short PacketLen
                    {
                        get { return BinaryPulsePacketLength; }
                    }

                    public byte[] Format(LTGFlash flash, int? pulseIndex, PartnerData partnerData)
                    {
                        byte[] packageBytes = null;
                        if (pulseIndex.HasValue)
                        {
                            packageBytes = new byte[PacketLen];
                            packageBytes = CreateCommonBinaryFormat(packageBytes, flash, pulseIndex, partnerData);

                            LTGFlashPortion portion = flash.FlashPortionList[pulseIndex.Value] as LTGFlashPortion;
                            if (portion != null)
                            {
                                packageBytes[PositionOffset.NumberOfSensors] = GetByteFromInt(portion.NumberSensors);

                                if (partnerData.Metadata)
                                {
                                    if (portion.ErrorEllipse != null)
                                    {
                                        double meters = portion.ErrorEllipse.MajorAxis * 1000D;
                                        IntegerConverter.WriteUShortToBufferAtOffset(GetUInt16FromDouble(meters), packageBytes, PositionOffset.ErrorEllipseMajorAxis, true);
                                        meters = portion.ErrorEllipse.MinorAxis * 1000D;
                                        IntegerConverter.WriteUShortToBufferAtOffset(GetUInt16FromDouble(meters), packageBytes, PositionOffset.ErrorEllipseMinorAxis, true);
                                        IntegerConverter.WriteUShortToBufferAtOffset(GetUInt16FromDouble(portion.ErrorEllipse.MajorAxisBearing), packageBytes, PositionOffset.ErrorEllipseBearing, true);
                                    }
                                    else
                                    {
                                        IntegerConverter.WriteUShortToBufferAtOffset(0, packageBytes, PositionOffset.ErrorEllipseMajorAxis, true);
                                        IntegerConverter.WriteUShortToBufferAtOffset(0, packageBytes, PositionOffset.ErrorEllipseMinorAxis, true);
                                        IntegerConverter.WriteUShortToBufferAtOffset(0, packageBytes, PositionOffset.ErrorEllipseBearing, true);
                                    }
                                }
                            }

                            packageBytes[PositionOffset.PortionChecksum] = CreateChecksum(PacketLen, packageBytes);
                        }

                        return packageBytes;
                    }

                    public string ToString(byte[] packageBytes, PartnerData partnerData)
                    {
                        StringBuilder asciiText = new StringBuilder();

                        CommonToString(packageBytes, asciiText);

                        ushort? maj = IntegerConverter.ReadUShortFromBufferAtOffset(packageBytes, PositionOffset.ErrorEllipseMajorAxis, true);
                        asciiText.Append("; eeMaj=").Append((maj.HasValue) ? maj.Value.ToString() : "--");

                        ushort? min = IntegerConverter.ReadUShortFromBufferAtOffset(packageBytes, PositionOffset.ErrorEllipseMinorAxis, true);
                        asciiText.Append("; eeMin=").Append((min.HasValue) ? min.Value.ToString() : "--");

                        ushort? bear = IntegerConverter.ReadUShortFromBufferAtOffset(packageBytes, PositionOffset.ErrorEllipseBearing, true);
                        asciiText.Append("; eeBear=").Append((bear.HasValue) ? bear.Value.ToString() : "--");

                        asciiText.Append("; chksum=").Append(packageBytes[PositionOffset.PortionChecksum]);

                        return asciiText.Append("]").ToString();
                    }

                    public byte[] KeepAlive(PartnerData partnerData)
                    {
                        return Format(CreateKeepAlivePulse(), 0, partnerData);
                    }
                }
            }

            private class FlashAndPulseCombo
            {
                public static Dictionary<ResponseFormatType, IFormatter> CreateFormatters()
                {
                    return new Dictionary<ResponseFormatType, IFormatter>
                           {
                               {ResponseFormatType.Binary, new Binary()}
                           };
                }

                private class Binary : IFormatter
                {
                    public short PacketLen
                    {   // This is an approximate value... used for reading bytes in test client
                        get { return 1000; }
                    }

                    private const byte ComboHeaderPacketLen = 2;

                    private class PositionOffset
                    {
                        public const byte NumberOfBytes = 0;
                        public const byte FlashPacket = 2;
                        public const byte PulsePacket = FlashPacket + BinaryFlashPacketLength;
                    }

                    public byte[] Format(LTGFlash flash, int? pulseIndex, PartnerData partnerData)
                    {
                        Flash.Binary fb = new Flash.Binary();
                        Pulse.Binary pb = new Pulse.Binary();

                        int packetSize =
                            ComboHeaderPacketLen +                            // Combo packet header length
                            1 +                                               // Checksum length in footer
                            fb.PacketLen +                                    // Flash packet length
                            (flash.FlashPortionList.Count * pb.PacketLen);    // Set of Pulse packet lengths
                        byte[] packageBytes = new byte[packetSize];

                        // Add Combo header
                        IntegerConverter.WriteUShortToBufferAtOffset((ushort)packetSize, packageBytes, PositionOffset.NumberOfBytes, true);

                        // Add Flash
                        byte[] flashBytes = fb.Format(flash, pulseIndex, partnerData);
                        flashBytes.CopyTo(packageBytes, PositionOffset.FlashPacket);

                        // Add each Pulse
                        for (int i = 0; i < flash.FlashPortionList.Count; i++)
                        {
                            byte[] pulseBytes = pb.Format(flash, i, partnerData);
                            pulseBytes.CopyTo(packageBytes, PositionOffset.PulsePacket + (pulseBytes.Length * i));
                        }

                        // Add Checksum
                        packageBytes[packageBytes.Length - 1] = CreateChecksum((short)packageBytes.Length, packageBytes);

                        return packageBytes;
                    }

                    public string ToString(byte[] packageBytes, PartnerData partnerData)
                    {
                        string retValue = String.Empty;

                        const string CheckSum = "^^CHECKSUM^^";
                        StringBuilder asciiText = new StringBuilder();

                        ushort? numBytes = IntegerConverter.ReadUShortFromBufferAtOffset(packageBytes, PositionOffset.NumberOfBytes, true);
                        if (numBytes.HasValue)
                        {
                            // Read number bytes
                            asciiText.Append("[bytes=").Append(numBytes.Value);
                            asciiText.Append("; chksum=").Append(CheckSum).Append("]");

                            // Read Flash packet - Copy flash packets out of the packageBytes into it's own byte array
                            Flash.Binary fb = new Flash.Binary();
                            byte[] flashBytes = new byte[fb.PacketLen];
                            Array.Copy(packageBytes, PositionOffset.FlashPacket, flashBytes, 0, fb.PacketLen);
                            asciiText.Append(Environment.NewLine).Append(fb.ToString(flashBytes, partnerData));

                            Pulse.Binary pb = new Pulse.Binary();
                            int nextPulseStartingPosition = 0;
                            int numPulses = (numBytes.Value - ComboHeaderPacketLen - fb.PacketLen) / BinaryPulsePacketLength;

                            for (int i = 0; i < numPulses; i++)
                            {
                                byte[] pulseBytes = new byte[pb.PacketLen];
                                nextPulseStartingPosition = PositionOffset.PulsePacket + (i * pulseBytes.Length);

                                Array.Copy(packageBytes, nextPulseStartingPosition, pulseBytes, 0, pulseBytes.Length);
                                asciiText.Append(Environment.NewLine).Append(pb.ToString(pulseBytes, partnerData));
                            }

                            retValue = asciiText.ToString().Replace(CheckSum, packageBytes[nextPulseStartingPosition + pb.PacketLen].ToString());

                        }

                        // Replace token with checksum
                        return retValue;
                    }

                    public byte[] KeepAlive(PartnerData partnerData)
                    {
                        return Format(CreateKeepAliveFlash(), null, partnerData);
                    }
                }
            }
        }
    }
}
