﻿using Microsoft.SqlServer.Types;

namespace LxDatafeed.Data
{
    public class BoundingBox
    {
        public static BoundingBox Convert(SqlGeography sqlGeo, bool isInclusion)
        {
            BoundingBox bb = null;
            double north = 0, south = 0, east = 0, west = 0;

            for (int i = 1; i <= sqlGeo.STNumPoints(); i++)
            {
                SqlGeography point = sqlGeo.STPointN(i);

                //set the intial values on the first time through
                if (i == 1)
                {
                    north = point.Lat.Value;
                    south = point.Lat.Value;
                    east = point.Long.Value;
                    west = point.Long.Value;
                }
                else
                { 
                    if (point.Lat.Value > north)
                    {
                        north = point.Lat.Value;
                    }

                    if (point.Lat.Value < south)
                    {
                        south = point.Lat.Value;
                    }

                    if (point.Long.Value > east)
                    {
                        east = point.Long.Value;
                    }

                    if (point.Long.Value < west)
                    {
                        west = point.Long.Value;
                    }
                }
            }

            bb = new BoundingBox(west, east, south, north, isInclusion);

            return bb;
        }

        private double _north;
        private double _south;
        private double _west;
        private double _east;
        private bool _isInclusion;
        
        public BoundingBox()
        {
            _north = 0; _south = 0; _west = 0; _east = 0;
            _isInclusion = true;
        }

        public BoundingBox(double west, double east, double south, double north, bool isInclusion)
        {
            _west = west; _east = east; _north = north; _south = south;
            _isInclusion = isInclusion;
        }

        public bool Contains(double longitude, double latitude)
        {
            bool inside =  longitude >= _west && longitude <= _east && latitude >= _south && latitude <= _north;
            if (_isInclusion)
            {
                return inside;
            }
            else
            {
                return !inside;
            }
        }

        public double North
        {
            get { return _north; }
        }

        public double South
        {
            get { return _south; }
        }

        public double West
        {
            get { return _west; }
        }

        public double East
        {
            get { return _east; }
        }
    }
}
