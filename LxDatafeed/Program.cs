﻿using System;
using System.ServiceProcess;
using System.Timers;

using Aws.Core.Utilities;
using Aws.Core.Utilities.Threading;

using LxCommon.Monitoring;

using LxDatafeed.Manager;

namespace LxDatafeed
{
    public class Program
    {
        private static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
            { 
                new WinService() 
            };
            ServiceBase.Run(ServicesToRun);
        }


        private static Timer _startUpTimer;
        private static ProcessManager _processManager = null;

        public static void Start()
        {
            EventManager.Init();

            AppDomain.CurrentDomain.UnhandledException += CurrentDomainUnhandledException;
            
            EventManager.LogInfo("LightningDataFeedConsole");

            SqlServerTypes.Utilities.LoadNativeAssemblies(AppDomain.CurrentDomain.BaseDirectory);

            _startUpTimer = new Timer(1000);
            _startUpTimer.Elapsed += OnStartupTimerEvent;
            _startUpTimer.Start();
        }


        public static void Stop()
        {
            try
            {
                if (null != _processManager)
                {
                    _processManager.Stop();

                    while (null != _processManager && !_processManager.HasShutdown)
                    {
                        System.Threading.Thread.Sleep(100);
                    }
                }
                EventManager.ShutDown();
            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.TidServiceStoppedFail, "Failure occurred in OnStop() method of LightningDataFeedConsole. " + ex.Message);
            }

        }
        private static void OnStartupTimerEvent(object sender, ElapsedEventArgs e)
        {
            _startUpTimer.Stop();
            _startUpTimer = null;
            _processManager = new ProcessManager();
            WorkerThread.StartWorkerThreadAsync(_processManager);
        }


        #region Log unhandled exceptions
        /// <summary>
        /// This is the callback for any unhandled exception that may occur.  MDE will crash after this method is
        /// called... there is no way around that from this point... this just gives us the chance to log the problem.
        /// </summary>
        private static void CurrentDomainUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Exception ex = null;
            if (e != null && e.ExceptionObject != null && e.ExceptionObject is Exception) ex = (Exception)e.ExceptionObject;
            LogUnhandledException(ex);
        }

        /// <summary>
        /// This method will log the unhandled exception. It's not in the above method so that Program.cs can call this for
        /// logging an error if necessary.
        /// </summary>
        public static void LogUnhandledException(Exception ex)
        {

            // Let's try to log this using EventLog and AppLogger
            EventManager.LogError(TaskMonitor.DfInternalError, "LightningDataFeed is crashing!!! Unhandled exception has occurred." + ex.Message);

            try
            {
                if (true)
                {   // errMsg will be null if we were unable to log using EventLog and AppLogger so let's build it manually.
                    //errMsg = string.Format("{0} Error: {1}{2}{3}Source:{4}{5}{6}Stack Trace:{7}{8}",
                    // errorPrefix, Environment.NewLine, ex.Message, Environment.NewLine + Environment.NewLine, Environment.NewLine, ex.Source, Environment.NewLine, Environment.NewLine, ex.StackTrace);
                    //File.AppendAllText(AppConfig.LogFolder + "CrashDump - " + DateTime.Now.ToString("MM.dd.yyyy HH.mm.ss") + ".txt", errMsg);
                }
            }
            catch (Exception ex2)
            {
                throw new Exception("Failure trying to log unhandled exception in LightningDataFeed. Error in this handler: " + ex2.Message);
            }
        }
        #endregion
    }
}
