using System;
using System.Collections.Concurrent;

using Aws.Core.Utilities;
using Aws.Core.Utilities.Threading;

using LxCommon;
using LxCommon.Monitoring;
using LxDatafeed.Data.Partner;
using LxDatafeed.Data.ResponsePackage;
using LxDatafeed.Net;
using LxDatafeed.Utilities;

namespace LxDatafeed.Process
{
    internal class ClientConnWorker : WorkerThread
    {
        private const int MaxQueueSize = 5000;
        private const int NumMinutesBetweenLoggingMaxQueueSize = 60;

        private SocketState _socketState = null;
        DateTime _utcLastQueueMaxSizeReached;
        private int _numTimesMaxQueueReached;
        private readonly ConcurrentQueue<LTGFlash> _flashQueue;

        DateTime _utcLastQueueSize500Reached;
        private int _numTimesQueue500Reached;
        
        public ClientConnWorker()
        {
            _flashQueue = new ConcurrentQueue<LTGFlash>();
            _utcLastQueueMaxSizeReached = DateTime.MinValue;
            WaitTimeSecondsBetweenRun = 1;
        }
        
        protected override RunReturnStatus Run()
        {
            ProcessData();
            SendKeepAlive();
            return RunReturnStatus.RunAgain;
        }

        protected override void ShutDown()
        {
            base.ShutDown();
            try
            {
                if (_socketState != null)
                {
                    _socketState.CloseConnection("Lightning Data Feed Normal Shutdown.", true);
                }
            }
            catch (Exception ex)
            {
                EventManager.LogInfo("Lightning Data Feed encountered an error in stopping TcpListener." + ex.Message);
            }
        }

       

        public void AddData(LTGFlash flash)
        {
            bool isQueueMaxSizeReached = false;
            
            while (_flashQueue.Count > MaxQueueSize)
            {
                isQueueMaxSizeReached = true;
                LTGFlash trash;
                _flashQueue.TryDequeue(out trash);
            }
            
            _flashQueue.Enqueue(flash);

            if (_flashQueue.Count > 500)
            {
                _numTimesQueue500Reached++;
                if ((DateTime.UtcNow - _utcLastQueueSize500Reached).TotalMinutes > 5)
                {
                    EventManager.LogWarning(TaskMonitor.DfQueMaxSizeReached, "Queue max size has exceeded 500 " + _numTimesQueue500Reached + " time(s) since " +
                                            _utcLastQueueSize500Reached.ToString() + " (UTC) [partnerId=" + _socketState.PartnerId + "; remoteId=" +
                                            _socketState.RemoteIP + "]");

                    _numTimesQueue500Reached = 0;
                    _utcLastQueueSize500Reached = DateTime.UtcNow;
                }
            }    

            if (isQueueMaxSizeReached)
            {
                _numTimesMaxQueueReached++;
                if ((DateTime.UtcNow - _utcLastQueueMaxSizeReached).TotalMinutes > NumMinutesBetweenLoggingMaxQueueSize)
                {
                    EventManager.LogWarning(TaskMonitor.DfQueMaxSizeReached, "Queue max size was reached " + _numTimesMaxQueueReached + " time(s) since " +
                                            _utcLastQueueMaxSizeReached.ToString() + " (UTC) [partnerId=" + _socketState.PartnerId + "; remoteId=" +
                                            _socketState.RemoteIP + "]");

                    _numTimesMaxQueueReached = 0;
                    _utcLastQueueMaxSizeReached = DateTime.UtcNow;
                }
            }     
        }
        
        
        private void ProcessData()
        {
            LTGFlash flash;
            bool stop = false;
            while (!stop && _flashQueue.TryDequeue(out flash))
            {
                stop = AttemptToSendData(flash);
            }
        }

        private bool AttemptToSendData(LTGFlash flash)
        {
            bool stop = false;
            if (_socketState != null && _socketState.IsConnected && flash != null)
            {
                bool isLightningInBoundingArea = _socketState.PartnerSessionData.IsLightningInBoundingArea(flash);
                if (_socketState.PartnerSessionData.ResponsePackageType == ResponsePackageType.Pulse)
                {
                    for (int i = 0; i < flash.FlashPortionList.Count; i++)
                    {
                        SendPulse(flash, i, isLightningInBoundingArea);
                    }
                }
                else
                {   // Flash and Flash/Pulse Combo
                    SendFlash(flash, isLightningInBoundingArea);
                }
            }
            else
            {
                if (_socketState == null)
                {
                    EventManager.LogError(TaskMonitor.DfSendResponsePackFail, "Lightning data not sent to client because socket is null.");
                    stop = true;
                }
                else if (_socketState.IsConnected == false)
                {
                    EventManager.LogError(TaskMonitor.DfSendResponsePackFail, "Lightning data not sent to client because socket state is not connected.");
                    _socketState.CloseConnection("Closing connection, socket state is not connected", true);
                    stop = true;
                }
                else if (flash == null)
                {
                    EventManager.LogError(TaskMonitor.DfSendResponsePackFail, "Lightning data not sent to client because flash is null.");
                }
                else
                {
                    EventManager.LogError(TaskMonitor.DfSendResponsePackFail, "Lightning data not sent to client because unknown error.");
                }
            }
            return stop;
        }

        private void SendFlash(LTGFlash flash, bool isLightningInBoundingArea)
        {
            if (isLightningInBoundingArea && _socketState.PartnerSessionData.IsRequestedLightningType(flash))
            {
                byte[] responsePackage = ResponsePackageFormatter.Format(flash, null, _socketState.PartnerSessionData);
                _socketState.SendResponsePackage(responsePackage);
            }
            else
            {
                LogNotSent(flash, null, isLightningInBoundingArea, false);
            }
        }

        private void SendPulse(LTGFlash flash, int pulseIndex, bool isLightningInBoundingArea)
        {
            LTGFlashPortion pulse = flash.FlashPortionList[pulseIndex] as LTGFlashPortion;
            if (isLightningInBoundingArea && _socketState.PartnerSessionData.IsRequestedLightningType(pulse))
            {
                byte[] responsePackage = ResponsePackageFormatter.Format(flash, pulseIndex, _socketState.PartnerSessionData);
                _socketState.SendResponsePackage(responsePackage);
            }
            else
            {
                LogNotSent(flash, pulseIndex, isLightningInBoundingArea, false);
            }
        }

        private void LogNotSent(LTGFlash flash, int? pulseIndex, bool isLightningInBoundingArea, bool isRequestedLightningType)
        {
            byte[] responsePackage = ResponsePackageFormatter.Format(flash, pulseIndex, _socketState.PartnerSessionData);

            string reason = "ba=" + isLightningInBoundingArea + ", class=" + isRequestedLightningType;
            DebugLogTool.LogResponsePackageNotSentToClient(_socketState.RemoteIP, responsePackage, _socketState.PartnerSessionData, reason);
        } 
        
        private void SendKeepAlive()
        {
            if ((DateTime.UtcNow - _socketState.LastPacketTimeUtc).TotalSeconds > AppConfig.KeepAliveSeconds)
            {
                _socketState.SendResponsePackage(ResponsePackageFormatter.KeepAlive(_socketState.PartnerSessionData));
            }
        }
        
        public bool IsSamePartnerId(PartnerData partnerSessionData)
        {
            return partnerSessionData != null && _socketState.PartnerSessionData.PartnerId == partnerSessionData.PartnerId;
        }

        public SocketState SocketState
        {
            get { return _socketState; }
            set { _socketState = value; }
        }
    }
}
