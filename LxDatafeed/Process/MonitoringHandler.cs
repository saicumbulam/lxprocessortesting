﻿using System;
using System.Globalization;
using System.IO;
using System.Text;
using LxDatafeed.Utilities;
using Newtonsoft.Json.Linq;

using Aws.Core.Data.Common.Operations;
using Aws.Core.Utilities;

using LxCommon.TcpCommunication;
using LxCommon.Monitoring;

using LxDatafeed.Data;
using LxDatafeed.Manager;

namespace LxDatafeed.Process
{
    public class MonitoringHandler
    {
        private ConnServerManager _connServerManager;
        private Requester _primaryRequester;
        private Requester _secondaryRequester;
        private readonly string _systemType;
        private readonly string _monitorHmtl;
        Func<DateTime> _lastPartnerRefreshFunc;
        private IncomingPacketProcessor _incomingPacketProcessor;
        private DateTime _startTime;

        public MonitoringHandler(string systemType)
        {
            _systemType = systemType;
            _incomingPacketProcessor = null;
            _startTime = DateTime.UtcNow;

            try
            {
                _monitorHmtl = File.ReadAllText(AppUtils.AppLocation + @"\Monitor.html", Encoding.UTF8);
            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.TidMonitorPageUnavailable, "Unable to load monitor html file", ex);
                _monitorHmtl = "Unavailable";
            }
        }

        public void Start()
        {
            try
            {
                OpsCommandHttpEndpointSvc.Start((cmd) => MonitorStatusCheck(cmd));
            }
            catch (Exception ex)
            {
                EventManager.LogError(0, "Unable to start ecv http endpoint", ex);
            }
        }

        private string MonitorStatusCheck(string cmd)
        {
            if (cmd.ToUpper().Equals("PRTG"))
            {
                return GeneratePrtgStatus();
            }
            else
            {
                return GenerateHtmlStatus(_systemType, _monitorHmtl);
            }
            
        }

        private const string Success = "<span style=\"color: green;\">SUCCESS</span>";
        private const string Failure = "<span style=\"color: red;\">FAIL</span>";
        private const string NA = "-";
        private string GenerateHtmlStatus(string systemType, string monitorHmtl)
        {
            string html = null;
            string success = Success;
            string failure = Failure;
            bool isTln = (systemType.ToUpper().Equals("TLN"));

            if (!isTln)
            {
                success = NA;
                failure = NA;
            }

            CultureInfo enUs = new CultureInfo("en-US");
            DateTime utcNow = DateTime.UtcNow;

            double primaryConnectionRate = 0;
            double primaryPacketRate = 0;
            double secondaryConnectionRate = 0;
            double secondaryPacketRate = 0;
     
            if (_primaryRequester != null)
            {
                primaryConnectionRate = Math.Round(_primaryRequester.GetConnectionRatePerSecond(utcNow), 2, MidpointRounding.AwayFromZero);
                primaryPacketRate = Math.Round(_primaryRequester.GetIncomingRatePerSecond(utcNow), 2, MidpointRounding.AwayFromZero);
            }

            if (_secondaryRequester != null)
            {
                secondaryConnectionRate = Math.Round(_secondaryRequester.GetConnectionRatePerSecond(utcNow), 2, MidpointRounding.AwayFromZero);
                secondaryPacketRate = Math.Round(_secondaryRequester.GetIncomingRatePerSecond(utcNow), 2, MidpointRounding.AwayFromZero);
            }

            int clientConnections = 0;//, minFlashAge = 0, medianFlashAge = 0, maxFlashAge = 0;
            Tuple<int, int, int> mmmFlashAgeSent = new Tuple<int, int, int>(int.MaxValue, int.MaxValue, int.MaxValue);
            double connectionAttemptRate = 0.0;
            if (_connServerManager != null)
            {
                clientConnections = ClientQueue.Count;
                connectionAttemptRate = Math.Round(_connServerManager.ConnectionAttemptRate, 2, MidpointRounding.AwayFromZero);
                mmmFlashAgeSent = _connServerManager.FlashAgeMinMaxMedian;
            }

            DateTime last = DateTime.MinValue;
            if (_lastPartnerRefreshFunc != null)
            {
                last = _lastPartnerRefreshFunc();
            }

            TimeSpan ts = utcNow - last;
            double ageLastRefreshMinutes = Math.Round(ts.TotalMinutes, 2, MidpointRounding.AwayFromZero);

            DateTime lastFlashTime = _startTime;
            string activeDataManager = string.Empty;
            if (_incomingPacketProcessor != null)
            {
                lastFlashTime = _incomingPacketProcessor.LastFlashTime;
                activeDataManager = _incomingPacketProcessor.ActiveDataManager;
            }

            int ageLastFlashProcessedSeconds = (int)(utcNow - lastFlashTime).TotalSeconds;
            
            try
            {
                html = string.Format(monitorHmtl,
                      AppUtils.GetVersion(), //0
                      utcNow.ToString(enUs),

                      (primaryPacketRate >= AppConfig.MonitorPrimaryPacketRateMinimum) ? success : failure,
                      primaryPacketRate,

                      (primaryConnectionRate <= AppConfig.MonitorPrimaryConnectionRateMaximum) ? success : failure,
                      primaryConnectionRate,

                      (secondaryPacketRate >= AppConfig.MonitorSecondaryPacketRateMinimum) ? success : failure,
                      secondaryPacketRate,

                      (secondaryConnectionRate <= AppConfig.MonitorSecondaryConnectionRateMaximum) ? success : failure,
                      secondaryConnectionRate,

                      (ageLastRefreshMinutes <= AppConfig.MonitorPartnerRefreshAgeMinutesMaximum) ? success : failure,
                      ageLastRefreshMinutes,

                      (clientConnections >= AppConfig.MonitorClientConnectionsMinimum) ? success : failure,
                      clientConnections,

                      (connectionAttemptRate <= AppConfig.MonitorClientConnectionRateMaximum) ? success : failure, 
                      connectionAttemptRate,
                      
                      "--",
                      ageLastFlashProcessedSeconds,

                      "--",
                      activeDataManager,

                      mmmFlashAgeSent.Item1,

                      mmmFlashAgeSent.Item2,

                      mmmFlashAgeSent.Item3
                      );
            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.TidMonitorPageUnavailable, "Unable to build monitor html file", ex);
            }


            return html;
        }

        private string GeneratePrtgStatus()
        {
            DateTime utcNow = DateTime.UtcNow;

            JObject j = new JObject();
                    
            if (_primaryRequester != null)
            {
                j.Add("primaryConnectionRate", Math.Round(_primaryRequester.GetConnectionRatePerSecond(utcNow), 2));
                j.Add("primaryPacketRate", Math.Round(_primaryRequester.GetIncomingRatePerSecond(utcNow), 2));
            }

            if (_secondaryRequester != null)
            {
                j.Add("secondaryConnectionRate", Math.Round(_secondaryRequester.GetConnectionRatePerSecond(utcNow), 2));
                j.Add("secondaryPacketRate", Math.Round(_secondaryRequester.GetIncomingRatePerSecond(utcNow), 2));
            }

            if (_connServerManager != null)
            {
                j.Add("clientConnections", ClientQueue.Count);
                j.Add("clientConnectionAttemptRate", _connServerManager.ConnectionAttemptRate);
                Tuple<int, int, int> mmmFlashAgeSent = _connServerManager.FlashAgeMinMaxMedian;
                j.Add("minFlashAgeSendSeconds", mmmFlashAgeSent.Item1);
                j.Add("maxFlashAgeSendSeconds", mmmFlashAgeSent.Item2);
                j.Add("medianFlashAgeSendSeconds", mmmFlashAgeSent.Item3);
            }

            DateTime last = DateTime.MinValue;
            if (_lastPartnerRefreshFunc != null)
            {
                last = _lastPartnerRefreshFunc();
            }

            TimeSpan ts = utcNow - last;
            double ageLastRefreshMinutes = Math.Round(ts.TotalMinutes, 2, MidpointRounding.AwayFromZero);
            j.Add("partnerAgeLastRefreshMinutes", ageLastRefreshMinutes);

            DateTime lastFlashProcessedUtc = _startTime;
            if (_incomingPacketProcessor != null)
            {
                lastFlashProcessedUtc = _incomingPacketProcessor.LastFlashProcessedUtc;
            }

            j.Add("lastFlashProcessedUtc", lastFlashProcessedUtc);


            DateTime lastFlashTime = _startTime;
            string activeDataManager = string.Empty;
            if (_incomingPacketProcessor != null)
            {
                lastFlashTime = _incomingPacketProcessor.LastFlashTime;
                activeDataManager = _incomingPacketProcessor.ActiveDataManager;
            }

            int ageLastFlashProcessedSeconds = (int)(utcNow - lastFlashTime).TotalSeconds;

            j.Add("ageLastFlashProcessedSeconds", ageLastFlashProcessedSeconds);

            j.Add("activeDataManager", activeDataManager);


            return j.ToString();
        }

        public void Stop()
        {
            try
            {
                OpsCommandHttpEndpointSvc.Stop();
            }
            catch (Exception ex)
            {
                EventManager.LogError(1, "Unable to stop ecv http endpoint", ex);
            }

        }

        internal ConnServerManager ConnServerManager 
        {
            set { _connServerManager = value; }
        }

        public Requester PrimaryRequester
        {
            set { _primaryRequester = value; }
        }

        public Requester SecondaryRequester
        {
            set { _secondaryRequester = value; }
        }

        public Func<DateTime> LastPartnerRefreshFunc
        {
            set { _lastPartnerRefreshFunc = value; }
        }

        public IncomingPacketProcessor IncomingPacketProcessor 
        {
            set { _incomingPacketProcessor = value; }
        }
    }
}
