﻿using System;
using System.Collections.Generic;
using System.Threading;

using Aws.Core.Utilities;

using LxCommon;
using LxCommon.DataStructures;
using LxCommon.Monitoring;
using LxCommon.TcpCommunication;

using LxDatafeed.Data;
using LxDatafeed.Manager;

namespace LxDatafeed.Process
{
    public class IncomingPacketProcessor : ParallelQueueProcessor<Tuple<PacketHeaderInfo, string>>
    {
        private Requester _primaryRequester;
        private Requester _secondaryRequester;
        private readonly int _maxAgeForFailoverSeconds;
        private string _activeRequester = null;
        private readonly object _lock;
        private DateTime _lastPrimaryPacket;
        private DateTime _lastSecondaryPacket;
        private readonly FlashLogger _flashLogger;
        private DateTime _lastFlashProcessedUtc;
        private DateTime _lastFlashTime;
        private readonly ConnServerManager _connServerManager;
        
        private const ushort EventManagerOffset = TaskMonitor.DfIncomingPacketProcessor;

        public IncomingPacketProcessor(ConnServerManager connServerManager, int maxAgeForFailoverSeconds, FlashLogger flashLogger, ManualResetEventSlim stopEvent, int maxActiveWorkItems)
            : base(stopEvent, maxActiveWorkItems)
        {
            _primaryRequester = null;
            _secondaryRequester = null;
            _connServerManager = connServerManager;
            _maxAgeForFailoverSeconds = maxAgeForFailoverSeconds;
            _flashLogger = flashLogger;
            _lastPrimaryPacket = DateTime.UtcNow;
            _lastSecondaryPacket = DateTime.UtcNow;
            _lastFlashProcessedUtc = DateTime.UtcNow;
            _lastFlashTime = DateTime.UtcNow;
            _lock = new object();
        }

        public DecodeResult TcpFlashDecode(List<byte> packetBuffer, string remoteIp)
        {
            PacketHeaderInfo phi;
            DecodeResult result = TcpPacketUtils.TryTcpDecodeWithInt32Length(packetBuffer, remoteIp, out phi);

            if (result == DecodeResult.FoundPacket && phi.MsgType == PacketType.MsgTypeFlash)
            {
                Tuple<PacketHeaderInfo, string> t = new Tuple<PacketHeaderInfo, string>(phi, remoteIp);
                Add(t);
            }

            return result;
        }

        public override void Add(Tuple<PacketHeaderInfo, string> workItem)
        {
            DateTime utcNow = DateTime.UtcNow;

            lock (_lock)
            {
                //once we get the first packet default to the primary
                if (_activeRequester == null)
                {
                    if (_primaryRequester != null)
                    {
                        _activeRequester = _primaryRequester.Address;
                    }
                    else if (_secondaryRequester != null)
                    {
                        _activeRequester = _secondaryRequester.Address;
                    }
                }

                //update the time checks
                if (_primaryRequester != null && workItem.Item2.Equals(_primaryRequester.Address))
                {
                    _lastPrimaryPacket = utcNow;
                }
                else if (_secondaryRequester != null && workItem.Item2.Equals(_secondaryRequester.Address))
                {
                    _lastSecondaryPacket = utcNow;
                }

                TimeSpan primaryAge = utcNow - _lastPrimaryPacket;
                TimeSpan secondaryAge = utcNow - _lastSecondaryPacket;

                //check too see if we need to switch to a new requester
                if (_primaryRequester != null && _activeRequester == _primaryRequester.Address && primaryAge.TotalSeconds > _maxAgeForFailoverSeconds 
                    && _secondaryRequester != null && secondaryAge.TotalSeconds <= _maxAgeForFailoverSeconds)
                {
                    _activeRequester = _secondaryRequester.Address;
                    EventManager.LogWarning(EventManagerOffset, string.Format("Switching to secondary requester, primary requester has not received a packet in {0} secs", 
                        primaryAge.TotalSeconds));
                }
                else if (_secondaryRequester != null && _activeRequester == _secondaryRequester.Address && _primaryRequester != null 
                    && (primaryAge.TotalSeconds <= _maxAgeForFailoverSeconds || primaryAge < secondaryAge))
                {
                    _activeRequester = _primaryRequester.Address;
                    EventManager.LogWarning(EventManagerOffset + 1, string.Format("Switching to primary requester, last primary requester packet age: {0} secs, last secondary requester packet age: {1} secs", 
                        primaryAge.TotalSeconds, secondaryAge.TotalSeconds));
                }

                //only add packets from the active
                if (workItem.Item2.Equals(_activeRequester))
                {
                    base.Add(workItem);
                }
            }
        }

        protected override void DoWork(Tuple<PacketHeaderInfo, string> workItem)
        {
            PacketHeaderInfo phi = workItem.Item1;

            if (phi != null)
            {
                if (phi.MsgType != PacketType.MsgTypeKeepAlive)
                {
                    if (phi.RawData != null)
                    {
                        LTGFlash flashData = RawPacketUtil.DecodeFlashPacket(phi.RawData);

                        if (flashData != null)
                        {
                            Validate(flashData);

                            _connServerManager.AddOutboundFlash(flashData);

                            _lastFlashProcessedUtc = DateTime.UtcNow;
                            _lastFlashTime = LtgTimeUtils.GetUtcDateTimeFromString(flashData.FlashTime);

                            if (_flashLogger != null)
                            {
                                _flashLogger.WriteFlash(flashData, DateTime.UtcNow);
                            }
                        }
                    }
                }
            }
        }

        private void Validate(LTGFlash flash)
        {
            ValidateHeight(flash);

            foreach (FlashData pulse in flash.FlashPortionList)
            {
                ValidateHeight(pulse);
            }
        }

        private void ValidateHeight(FlashData flashData)
        {
            if (flashData.Height > ushort.MaxValue)
            {
                flashData.Height = ushort.MaxValue;
            }
        } 

        public Requester PrimaryRequester { set { _primaryRequester = value; } }

        public Requester SecondaryRequester { set { _secondaryRequester = value; } }

        public DateTime LastFlashProcessedUtc { get { return _lastFlashProcessedUtc; } }

        public DateTime LastFlashTime { get { return _lastFlashTime; } }

        public string ActiveDataManager { get { return _activeRequester; } }
    }
}
