﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Aws.Core.Utilities;

using LxCommon;
using LxCommon.Models;
using LxCommon.Monitoring;
using LxCommon.Utilities;

namespace LxDatafeed.Process
{
    public class FlashLogger
    {
        private const ushort EventManagerOffset = TaskMonitor.FileLogger;

        private string _baseFileName;
        private ConcurrentQueue<LogEntry> _linesToWrite;
        private int _writerLock;
        private int _minutesPerFile;
        private readonly DirectoryInfo _inUseDirectory = null;
        private readonly List<DirectoryInfo> _pickupDirectories = null;
        private Timer _movingTimer;
        private bool _isTlnSystem;


        public FlashLogger(string isUseDir, ReadOnlyCollection<string> pickupDirs, string baseFileName, int movingTimerIntervalSeconds, int minAgeSecondsMoveToPickup, int minutesPerFile, bool isTlnSystem)
        {
            if (minutesPerFile < 1 || minutesPerFile > 60)
                throw new ArgumentOutOfRangeException("minutesPerFile", "minutesPerFile must be >= 1 && <= 60");


            _inUseDirectory = FileHelper.CreateDirectory(isUseDir);
            if (_inUseDirectory == null)
            {
                throw new ArgumentException("invalid in use directory", "isUseDir");
            }

            if (pickupDirs == null)
            {
                throw new ArgumentNullException("pickupDirs");
            }

            _pickupDirectories = new List<DirectoryInfo>();
            foreach (string pickupDir in pickupDirs)
            {
                DirectoryInfo dir = FileHelper.CreateDirectory(pickupDir);
                if (dir == null)
                {
                    throw new ArgumentException("invalid pickup directory", "pickupDirs");
                }
                else
                {
                    _pickupDirectories.Add(dir);
                }
            }

            _baseFileName = baseFileName;
            _linesToWrite = new ConcurrentQueue<LogEntry>();
            _minutesPerFile = minutesPerFile;
            _isTlnSystem = isTlnSystem;


            _movingTimer = new Timer((state) => MoveInUseFiles(_inUseDirectory, _pickupDirectories, _baseFileName, _minutesPerFile, movingTimerIntervalSeconds, minAgeSecondsMoveToPickup),
                _inUseDirectory, TimeSpan.FromSeconds(movingTimerIntervalSeconds), TimeSpan.FromMilliseconds(-1));

            _writerLock = 0;
        }

      

        public void WriteFlash(LTGFlash ltgFlash, DateTime dt)
        {
            try
            {
                Flash flash = new Flash(ltgFlash, _isTlnSystem, dt);
                string f = JsonConvert.SerializeObject(flash);
                string fullFileName = GetFullFileName(dt, _minutesPerFile, _baseFileName, _inUseDirectory.FullName);
                Write(new LogEntry(f, fullFileName));
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventManagerOffset, "Unable to write flash", ex);
            }
        }

        private void Write(LogEntry logEntry)
        {
            _linesToWrite.Enqueue(logEntry);
            // This controls the thread that manages the queue, make sure we only launch one thread 
            if (0 == Interlocked.CompareExchange(ref _writerLock, 1, 0))
            {
                Task.Factory.StartNew(WriteLines);
            }
        }

        private void WriteLines()
        {
            LogEntry logEntry;
            string currentFileName = null;
            TextWriter textWriter = null;
            try
            {
                while (_linesToWrite.TryDequeue(out logEntry))
                {
                    if (textWriter == null || currentFileName != logEntry.FileName)
                    {
                        if (textWriter != null)
                        {
                            textWriter.Close();
                        }

                        textWriter = new StreamWriter(logEntry.FileName, true);
                        currentFileName = logEntry.FileName;
                    }

                    textWriter.WriteLine(logEntry.Line);
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventManagerOffset + 2, "An error occured while writing the file in FlashDataLogger", ex);
            }
            finally
            {
                if (textWriter != null)
                {
                    textWriter.Close();
                }

                Interlocked.Decrement(ref _writerLock);
            }
        }

        private void MoveInUseFiles(DirectoryInfo inUseDirectory, List<DirectoryInfo> pickupDirectories, string baseFileName, int minutesPerFile, int movingTimerIntervalSeconds, int minAgeSecondsMoveToPickup)
        {
            string pattern = baseFileName.Replace("{0}", "*");
            FileInfo[] fi = inUseDirectory.GetFiles(pattern);
            DateTime utcNow = DateTime.UtcNow;
            foreach (FileInfo fileInfo in fi)
            {
                DateTime fileTime = FileHelper.GetFileTimeFromName(fileInfo.Name, pattern);
                if (fileTime != DateTime.MinValue)
                {
                    if ((utcNow - fileTime).TotalSeconds > (minutesPerFile * 60 + minAgeSecondsMoveToPickup))
                    {
                        for (int i = 0; i < pickupDirectories.Count; i++)
                        {
                            if (i < (pickupDirectories.Count - 1))
                            {
                                CopyFileToPickupDirectory(fileInfo, pickupDirectories[i]);
                            }
                            else
                            {
                                MoveFileToPickupDirectory(fileInfo, pickupDirectories[i]);
                            }
                        }
                    }
                }
            }

            _movingTimer.Change(TimeSpan.FromSeconds(movingTimerIntervalSeconds), TimeSpan.FromMilliseconds(-1));
        }

        private void CopyFileToPickupDirectory(FileInfo fileInfo, DirectoryInfo directoryInfo)
        {
            try
            {
                string dest = Path.Combine(directoryInfo.FullName, fileInfo.Name);
                fileInfo.CopyTo(dest);
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventManagerOffset + 3, string.Format("An error occured while copying file from {0} to {1}", fileInfo.FullName, directoryInfo.FullName), ex);
            }
        }

        private void MoveFileToPickupDirectory(FileInfo fileInfo, DirectoryInfo directoryInfo)
        {
            try
            {
                string dest = Path.Combine(directoryInfo.FullName, fileInfo.Name);
                if (File.Exists(dest)) File.Delete(dest);

                fileInfo.MoveTo(dest);
            }
            catch(Exception ex)
            {
                EventManager.LogError(EventManagerOffset + 3, string.Format("An error occured while moving file from {0} to {1}", fileInfo.FullName, directoryInfo.FullName), ex);
            }
        }

        private static string GetFullFileName(DateTime utcTime, int minutesPerFile, string baseFileName, string dirFullName)
        {
            int min = (utcTime.Minute / minutesPerFile) * minutesPerFile;
            string f = string.Format("{0:D4}-{1:D2}-{2:D2}T{3:D2}-{4:D2}", utcTime.Year, utcTime.Month, utcTime.Day, utcTime.Hour, min);
            string fileName = string.Format(baseFileName, f);
            fileName = Path.Combine(dirFullName, fileName);
            return fileName;
        }

        private class LogEntry
        {
            public LogEntry(string line, string fileName)
            {
                Line = line;
                FileName = fileName;
            }

            public string Line { get; private set; }
            public string FileName { get; private set; }
        }
    }
}
