using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;

using Aws.Core.Utilities;
using Aws.Core.Utilities.Threading;

using LxCommon.Monitoring;

using LxDatafeed.Data;
using LxDatafeed.Utilities;

namespace LxDatafeed.Net
{
    internal class TcpSocketListerner : WorkerThread
    {
        private readonly int _port;      
        private TcpListener _tcpListener = null;
        private readonly bool _isSsl;
        private readonly X509Certificate _serverCetrificate;
        private double _connectionAttempts = 0;
        
        public TcpSocketListerner(int port, bool isSsl)
        {
            _isSsl = isSsl  ;
            if (AppConfig.DataFeedSslCertPath != "" && isSsl)
            {
                _serverCetrificate = new X509Certificate(AppConfig.DataFeedSslCertPath, AppConfig.DataFeedSslCertPw);
            }
            else
            {
                _serverCetrificate = null;
            }
            _port = port;
            WaitTimeSecondsBetweenRun = 50;
        }
        
        protected override RunReturnStatus Run()
        {            
            Listening();
            return RunReturnStatus.RunAgain;
        }

        protected override void ShutDown()
        {
            base.ShutDown();
            try
            {
                if (_tcpListener != null)
                {
                    _tcpListener.Stop();
                }
            }
            catch(Exception ex)
            {
                EventManager.LogInfo("Lightning Data Feed encountered an error in stopping TcpListener." + ex.Message);
            }
        }

        private void Listening()
        {
            try
            {
                if (_tcpListener == null || _tcpListener.Server == null )
                {

                    var localEp = new IPEndPoint(IPAddress.Any, _port);
                    _tcpListener = new TcpListener(localEp);
                    _tcpListener.Start();
                    _tcpListener.BeginAcceptTcpClient(OnAcceptConnection, null);

                    //_tcpListener.BeginAcceptSocket(new AsyncCallback(this.OnAcceptConnection), null);
                    EventManager.LogInfo("Lightning Data Feed started TCP Listener for client connections.");
                }
            }
            catch (Exception ex)
            {
                try
                {
                    if (_tcpListener != null)
                    {
                        var socket = _tcpListener.Server;
                        if (socket != null)
                        {
                            socket.Close();
                        }
                        _tcpListener.Stop();
                    }
                }
                catch
                {}

                _tcpListener = null;
                EventManager.LogError(TaskMonitor.DfFailedTcpListnerStartUp, "Lightning Data Feed failed to start TCP Listener." + ex.Message);
            }
         }

        private void OnAcceptConnection(IAsyncResult ar)
        {
            if (_tcpListener == null || _isSignaledForShutdown)
            {
                 return;
            }

            try
            {
                _connectionAttempts++;
                //clientSocket = _tcpListener.EndAcceptSocket(ar);
                var tcpClient = _tcpListener.EndAcceptTcpClient(ar);
                
                if (tcpClient.Client != null)
                {
                    SetTcpKeepAlive(tcpClient.Client);

                    var state = new SocketState(tcpClient, 256, _isSsl, _serverCetrificate);
                    state.GenericStream.BeginRead(state.Buffer, 0, state.Buffer.Length, OnReceiveFromClient, state);
                    EventManager.LogInfo("Received TCP client connection.[remoteIp=" + state.RemoteIP + "]");
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.DfFailedAsyncTcpConn, "Lightning Data Feed failed accepting async TCP client connection. \r\nReason: " + ex.Message);
            }
            finally
            {
              // begin another async accept connection thre ad
                if (_tcpListener != null)
                {
                    try
                    {
                        _tcpListener.BeginAcceptSocket(OnAcceptConnection, null);
                    }
                    catch (Exception exx)
                    {
                        EventManager.LogError(TaskMonitor.DfFailedAccepSocketStartUp, "Lighting Data Feed failed in AcceptSocket startup for TCP client connections. \r\nReason: " + exx.Message);
                        try
                        {
                            _tcpListener.Server.Close();
                            _tcpListener.Stop();
                        }
                        catch(Exception)
                        {
                        }
                        _tcpListener = null;
                    }
                }
            }
        }

        //private async void OnBeginReceiveAsync(IAsyncResult ar)
        //{
        //    var state = (SocketState)ar.AsyncState;
        //    var handler = await state.GenericStream.ReadAsync();
        //}

        private void SetTcpKeepAlive(Socket socket)
        {
            if (!AppConfig.TcpKeepAliveEnabled) return;

            try
            {
                var uintSize = Marshal.SizeOf((uint)0);
                var optionValues = new byte[uintSize * 3];

                var tcpKeepAliveStartDelayMs = AppConfig.TcpKeepAliveStartDelayMilliseconds;
                var tcpKeepAliveStartIntervalMs = AppConfig.TcpKeepAliveIntervalMilliseconds;
                
                // enable KeepAlive
                BitConverter.GetBytes((uint)1).CopyTo(optionValues, 0);

                // keepalivetime: specifies the timeout, in milliseconds, with no activity until the first keep-alive packet is sent
                BitConverter.GetBytes((uint)tcpKeepAliveStartDelayMs).CopyTo(optionValues, uintSize);

                // keepaliveinterval: specifies the interval, in milliseconds, between when successive keep-alive packets are sent if no acknowledgement is received
                BitConverter.GetBytes((uint)tcpKeepAliveStartIntervalMs).CopyTo(optionValues, uintSize * 2);

                socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, true);
                socket.IOControl(IOControlCode.KeepAliveValues, optionValues, null);
            }
            catch (Exception ex)
            {
                EventManager.LogWarning(TaskMonitor.DfFailedAcceptSocketSetKeepAlives, "Lightning Data Feed failed to set TCP keep-alive on async TCP client connection. Reason: " + ex.Message, ex);
            }
        }

        private void OnReceiveFromClient(IAsyncResult ar)
        {
            if (ar == null || _isSignaledForShutdown)
                return;

            try
            {
                SocketState state = (SocketState)ar.AsyncState;
                state.GenericStream.EndRead(ar);
                bool isValid = ConnectionStringParser.Parse(state);

                if (isValid)
                {
                    ClientQueue.AddTcpClient(state);
                }
                else
                {
                    state.GenericStream.Close();
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.DfFailedReceiveAsyncMsg, "Lightning Data Feed failed in receiving async msg from TCP client connection. \r\nReason: " + ex.Message);
            }
        }
        
        public void ResetConnectionAttempts()
        {
            _connectionAttempts = 0;
        }

        public double ConnectionAttempts
        {
            get { return _connectionAttempts; }
        }
    }
}