﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;

using Microsoft.SqlServer.Types;

using Aws.Core.Utilities;

using LxCommon.Monitoring;

using LxDatafeed.Data;
using LxDatafeed.Data.Partner;
using LxDatafeed.Data.ResponsePackage;

namespace LxDatafeed.Net
{
    internal class ConnectionStringParser
    {
        public static bool Parse(SocketState socketState)
        {
            bool isValid = false;
            Data connStrData = null;

            if (ReadRequest(socketState))
            {   // Let's check for legacy connection string first
                connStrData = LegacyVersion.GetConnectionStringData(socketState);

                if (connStrData == null)
                {   // Try current connection string
                    connStrData = Data.FromJson(socketState.ReceivedMsg);
                }
            }

            string eventLogMsg = null;
            isValid = IsValidConnection(socketState, connStrData, out eventLogMsg);
            if (isValid)
            {
                EventManager.LogInfo(eventLogMsg);
                PartnerSvc.LogEvent(socketState.PartnerId, socketState.RemoteIP, eventLogMsg, "Lightning Data Feed: ConnectionStringParser.Parse()");
            }
            else
            {
                socketState.CloseConnection(eventLogMsg, true);
            }
                
            return isValid;
        }

        #region ReadRequest
        private static bool ReadRequest(SocketState socketState)
        {
            string request = string.Empty;

            if (socketState.Buffer != null)
            {
                request = (new ASCIIEncoding()).GetString(socketState.Buffer);
                request = request.Trim().Replace("\0", string.Empty);
            }
            socketState.ReceivedMsg = request;

            return (!string.IsNullOrEmpty(request));
        } 
        #endregion

        #region IsValidConnection
        private static bool IsValidConnection(SocketState socketState, Data connStrData, out string eventLogMsg)
        {
            bool isValid = false;
            eventLogMsg = null;

            if (connStrData != null)
            {
                try
                {
                    PartnerData partnerData = PartnerSvc.GetData(connStrData.PartnerId);
                    if (partnerData != null)
                    {
                        //for https connection, the actual client ip is contained in connStrData
                        string clientIP = "";
                        if (socketState.RemoteIP == "127.0.0.1" && !string.IsNullOrEmpty(connStrData.ClientIP))
                            clientIP = connStrData.ClientIP;
                        else
                            clientIP = socketState.RemoteIP;
                        isValid = partnerData.IsValidRemoteConnection(clientIP);

                        if (isValid)
                        {
                            SetupConnectionSettings(socketState, connStrData, partnerData.Clone() as PartnerData);

                            eventLogMsg =
                                "Successful connection: TCP client connection has been validated and accepted.[remoteIp=" +
                                socketState.RemoteIP +
                                "; connectStr=" + socketState.ReceivedMsg + "]";
                        }
                        else
                        {
                            eventLogMsg =
                                "Unsuccessful connection: The remote ip address is not in whitelist for this partner.[remoteIp=" +
                                socketState.RemoteIP + "; connStr=" + socketState.ReceivedMsg + "]";
                        }
                    }
                    else
                    {
                        eventLogMsg = "Unsuccessful connection: Invalid partner attempting to connect.[remoteIp=" +
                                      socketState.RemoteIP +
                                      "; connStr=" + socketState.ReceivedMsg + "]";
                    }

                }
                catch (Exception exception)
                {
                    EventManager.LogWarning(TaskMonitor.DfInternalConnFailed,
                        "Lightning Data Feed failed at checking valid connection for connection string " +
                        socketState.ReceivedMsg, exception);
                    isValid = false;
                }
            }
            else
            {
                eventLogMsg = "Lightning Data Feed encountered a problem: Invalid connection string.[remoteIp=" + socketState.RemoteIP + "; connStr=" + socketState.ReceivedMsg + "]";
            }

            return isValid;
        } 
        #endregion

        #region SetupConnectionSettings
        private static void SetupConnectionSettings(SocketState socketState, Data connStrData, PartnerData clonedPartnerData)
        {
            clonedPartnerData.ResponsePackageVersion = connStrData.Version;

            // The client IS allowed to override this server setting
            if (connStrData.ResponseFormatType != ResponseFormatType.NotDefined)
            {
                clonedPartnerData.ResponseFormatType = connStrData.ResponseFormatType;
            }

            // The client IS allowed to override this server setting
            if (connStrData.ResponsePackageType != ResponsePackageType.NotDefined)
            {
                clonedPartnerData.ResponsePackageType = connStrData.ResponsePackageType;
            }

            if (clonedPartnerData.Configuration != null)
            {
                // The client IS NOT allowed to override this server setting unless this partner has been allowed
                if (connStrData.LxClassificationType != LxClassificationType.NotDefined &&
                    clonedPartnerData.Configuration.ContainsKey(ConfigurationType.CanPartnerPassInLxClassificationOnConnection))
                {
                    clonedPartnerData.LxClassificationType = connStrData.LxClassificationType;
                }

                // The client IS NOT allowed to override this server setting unless this partner has been allowed
                if (!string.IsNullOrEmpty(connStrData.BoundingArea) &&
                    clonedPartnerData.Configuration.ContainsKey(ConfigurationType.CanPartnerPassInBoundingBoxOnConnection))
                {
                    SqlGeography geo = SqlGeography.Parse(connStrData.BoundingArea);
                    clonedPartnerData.BoundingArea = BoundingBox.Convert(geo, true);
                }
            }

            if (clonedPartnerData.ResponseDataElements == null)
            {
                clonedPartnerData.ResponseDataElements = new HashSet<ResponseDataElement>();
            }

            if (connStrData.LocationError)
            {
                clonedPartnerData.ResponseDataElements.Add(ResponseDataElement.LocationError);
            }

            //Added for The client IS NOT allowed to override this server setting unless this partner has been allowed
            if (clonedPartnerData.Configuration != null && clonedPartnerData.Configuration.ContainsKey(ConfigurationType.CanPartnerpassInDataElementsonConnection))
            {
                if (connStrData.IcHeight)
                {
                    clonedPartnerData.ResponseDataElements.Add(ResponseDataElement.IcHeight);
                }

                if (connStrData.NumberOfSensors)
                {
                    clonedPartnerData.ResponseDataElements.Add(ResponseDataElement.NumberOfSensors);
                }

                if (connStrData.Multiplicity)
                {
                    clonedPartnerData.ResponseDataElements.Add(ResponseDataElement.Multiplicity);
                }
                // Metadata
                clonedPartnerData.Metadata = connStrData.Metadata;
            }
            clonedPartnerData.ShowSource = connStrData.ShowSource;

            socketState.PartnerSessionData = clonedPartnerData;
            socketState.PartnerSessionData.ResponsePackageVersion = connStrData.Version;
        } 
        #endregion

        #region Data
        [DataContract]
        public class Data
        {
            private string _partnerId;

            [DataMember(Name = "p", IsRequired = true, Order = 1)]
            public string PartnerId
            {
                get { return _partnerId; }
                set { _partnerId = (value != null ? value.ToLower() : null); }
            }

            [DataMember(Name = "v", IsRequired = true, Order = 2)]
            public short Version { get; set; }

            [DataMember(Name = "f", EmitDefaultValue = false, Order = 3)]
            public ResponseFormatType ResponseFormatType { get; set; }

            [DataMember(Name = "t", EmitDefaultValue = false, Order = 4)]
            public ResponsePackageType ResponsePackageType { get; set; }

            [DataMember(Name = "ba", EmitDefaultValue = false, Order = 5)]
            public string BoundingArea { get; set; }

            [DataMember(Name = "class", EmitDefaultValue = false, Order = 6)]
            public LxClassificationType LxClassificationType { get; set; }

            [DataMember(Name = "err", EmitDefaultValue = false, Order = 7)]
            public bool LocationError { get; set; }

            [DataMember(Name = "ht", EmitDefaultValue = false, Order = 8)]
            public bool IcHeight { get; set; }

            [DataMember(Name = "sensor", EmitDefaultValue = false, Order = 9)]
            public bool NumberOfSensors { get; set; }

            [DataMember(Name = "mult", EmitDefaultValue = false, Order = 10)]
            public bool Multiplicity { get; set; }

            [DataMember(Name = "ip", EmitDefaultValue = false, Order = 11)]
            public string ClientIP { get; set; }

            [DataMember(Name = "meta", EmitDefaultValue = false, Order = 12)]
            public bool Metadata { get; set; }

            [DataMember(Name = "showSource", EmitDefaultValue = false, Order = 12)]
            public bool ShowSource { get; set; }

            public string ToJson()
            {
                string json = null;

                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof (Data));
                using (MemoryStream ms = new MemoryStream())
                {
                    serializer.WriteObject(ms, this);
                    json = Encoding.ASCII.GetString(ms.ToArray());
                }

                return json;
            }

            public static Data FromJson(string json)
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(Data));

                Data data = null;
                using (MemoryStream ms = new MemoryStream(Encoding.ASCII.GetBytes(json)))
                {
                    data = serializer.ReadObject(ms) as Data;
                }

                return data;
            }
        }
        #endregion

        #region Inner Class: LegacyVersion
        private static class LegacyVersion
        {
            private const string PacketStart = "awslgt";
            private const string PacketEnd = "end";

            public static Data GetConnectionStringData(SocketState socketState)
            {
                Data connStrData = null;
                string requestLowered = socketState.ReceivedMsg.ToLower();

                if (requestLowered.StartsWith(PacketStart) && requestLowered.EndsWith(PacketEnd))
                {
                    string partnerId = requestLowered.Substring(PacketStart.Length, requestLowered.Length - PacketEnd.Length - PacketStart.Length);

                    connStrData = new Data()
                                      {
                                          PartnerId = partnerId,
                                          Version = 1
                                      };
                }

                return connStrData;
            }
        } 
        #endregion
    }
}
