using System;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;

using Aws.Core.Utilities;

using LxCommon.Monitoring;

using LxDatafeed.Data;
using LxDatafeed.Data.Partner;
using LxDatafeed.Utilities;

namespace LxDatafeed.Net
{
	internal class SocketState
	{
		private int _bufferSize;
		private string _receivedMsg = null;
		private DateTime _connectedTime ;   
	 
		bool _isConnected;
		private string _socketId;
		private string _remoteIp;
        private readonly string _remoteAddress;

        //private Socket _socket;    
        private byte[] _buffer;

		public string InvalidPartnerId { get; set; }
		public PartnerData PartnerSessionData { get; set; }
		public DateTime LastPacketTimeUtc { get; set; }
		public SocketError SocketErr;
		public Stream GenericStream;
        private TcpClient _tcpClient;

        #region Constructor
        public SocketState(TcpClient tcpClient, int bufferSize, bool isSsl, X509Certificate sslCert)
		{
            _tcpClient = tcpClient;
			_bufferSize = bufferSize;
			_buffer = new byte[_bufferSize];
			if (isSsl)
			{
				var sslStream = new SslStream(tcpClient.GetStream(), true, ValidateCert, null);
				sslStream.AuthenticateAsServer(sslCert, false, SslProtocols.Tls12, false);
				GenericStream = sslStream;
			}
			else
			{
				GenericStream = tcpClient.GetStream();
			}
			_remoteIp = ((IPEndPoint)tcpClient.Client.RemoteEndPoint).Address.ToString();

		    _remoteAddress = _remoteIp + ":" + ((IPEndPoint)tcpClient.Client.RemoteEndPoint).Port.ToString();

            LastPacketTimeUtc = DateTime.UtcNow;
			_connectedTime = DateTime.Now;
			_socketId = Guid.NewGuid().ToString().ToLower();
			_isConnected = true;
		}
		#endregion

		private static bool ValidateCert(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
		{
			return true;
		}

		public bool SendResponsePackage(byte[] responsePackage)
		{
			bool success = false;
			try
			{
				GenericStream.Write(responsePackage, 0, responsePackage.Length);
				GenericStream.Flush();

				LastPacketTimeUtc = DateTime.UtcNow;
				DebugLogTool.LogResponsePackageSentToClient(_remoteIp, responsePackage, PartnerSessionData);
				success = true;
			}
			catch (IOException ioEx)
			{
				EventManager.LogWarning(TaskMonitor.DfSendResponsePackFail, "I/O error in socket sending. [partnerId=" + PartnerSessionData.PartnerId + "; remoteIp=" + _remoteIp + "]", ioEx);
				CloseConnection("Failure trying to send to TCP client.[partnerId=" + PartnerSessionData.PartnerId + "; remoteIp=" + _remoteIp + "]", true);
			}
			catch (Exception ex)
			{
				EventManager.LogWarning(TaskMonitor.DfSendResponsePackFail, "Error in socket sending. [partnerId=" + PartnerSessionData.PartnerId + "; remoteIp=" + _remoteIp + "]", ex);
				CloseConnection("Failure trying to send to TCP client.[partnerId=" + PartnerSessionData .PartnerId + "; remoteIp=" + _remoteIp + "]", true);
			}

			return success;
		}

		private void SetDisconnectedFlag(string msg)
		{
			_isConnected = false;
			EventManager.LogInfo(msg);
			PartnerSvc.LogEvent(PartnerId, RemoteIP, msg, "Lightning Data Feed: SocketState.SetDisconnectedFlag()");
		}

		public void CloseConnection(string msg, bool close)
		{
			if (close)
			{
                CloseTcpClient();
                
                ClientQueue.RemoveTcpClient(_socketId);
				DebugLogTool.RemoveConnection(_remoteIp, PartnerSessionData);
			}

			SetDisconnectedFlag(msg);
		}

        public void CloseTcpClient()
        {
            if (GenericStream != null)
            {
                GenericStream.Close();
                GenericStream.Dispose();
            }
            
            if (_tcpClient != null)
            {
                _tcpClient.Close();
            }
            
        }

		#region Properties
		public byte[] Buffer
		{
			get { return _buffer; }
			set { _buffer = value; }
		}
		public string RemoteIP
		{
			get { return _remoteIp;}
		}
		public string ReceivedMsg
		{
			get { return _receivedMsg; }
			set { _receivedMsg = value; }
		}

		//public Socket Socket
		//{
		//	get { return _socket; }
		//	set { _socket = value; }
		//}

	   public bool IsConnected
	   {
		 get { return _isConnected;}
		 set { _isConnected = value; }
	   }

	   public string SocketId
	   {
		   get { return _socketId; }
		   set { _socketId = value; }
	   }
		public string PartnerId
		{
			get { return (PartnerSessionData != null ? PartnerSessionData.PartnerId : InvalidPartnerId);}
		}

	   public DateTime ConnectedTime
	   {
		   get { return _connectedTime; }
		   set { _connectedTime = value; }

	   }

	    public string RemoteAddress
	    {
	        get { return _remoteAddress; }
	    }

	    #endregion
	}
}
