﻿using System;
using System.Data.SqlClient;

using Aws.Core.Utilities;

using LxCommon.Monitoring;

namespace LxDatafeed.Utilities
{
    class DbTools
    {
        static public DateTime GetDataDateTime(SqlDataReader dr, string columnName)
        {
            try
            {
                return (DBNull.Value != dr[columnName] ? (DateTime)dr[columnName] : DateTime.MinValue);
            }
            catch (Exception ex)
            {
                EventManager.LogWarning(TaskMonitor.DfGetDataDatetime, "Lightning Data Feed failed reading DATETIME column '" + columnName + "' from the resultset. \r\nReason: " + ex.Message);               
                return DateTime.MinValue;
            }

        }


        static public double GetDataDouble(SqlDataReader dr, string columnName, double defaultValue)
        {
            try
            {
                return (DBNull.Value != dr[columnName] ? (double)dr[columnName] : defaultValue);
            }
            catch (Exception ex)
            {
                EventManager.LogWarning(TaskMonitor.DfGetDataDouble, "Lightning Data Feed failed reading double column '" + columnName + "' from the resultset. \r\nReason: " + ex.Message);              
         
                return defaultValue;
            }
        }

        static public float GetDataFloat(SqlDataReader dr, string columnName, float defaultValue)
        {
            try
            {
                return (DBNull.Value != dr[columnName] ? (float)dr[columnName] : defaultValue);
            }
            catch (Exception ex)
            {
                EventManager.LogWarning(TaskMonitor.DfGetDataFloat, "Lightning Data Feed failed reading float column '" + columnName + "' from the resultset. \r\nReason: " + ex.Message);              
                return defaultValue;
            }
        }


        static public int GetDataInt(SqlDataReader dr, string columnName)
        {
            try
            {
                return (DBNull.Value != dr[columnName] ? (int)dr[columnName] : -1);
            }
            catch (Exception ex)
            {
                EventManager.LogWarning(TaskMonitor.DfGetDataInt, "Lightning Data Feed failed reading INT column '" + columnName + "' from the resultset. \r\nReason: " + ex.Message);       
                return -1;
            }
        }

        static public string GetDataString(SqlDataReader dr, string columnName)
        {
            try
            {
                return (DBNull.Value != dr[columnName] ? (string)dr[columnName] : null);
            }
            catch (Exception ex)
            {
                EventManager.LogWarning(TaskMonitor.DfGetDataString, "Lightning Data Feed failed reading STRING column '" + columnName + "' from the resultset." + ex.Message);                    
                return null;
            }
        }
    }

   
}

