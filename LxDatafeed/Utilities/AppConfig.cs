using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.IO;
using System.Net;
using System.Reflection;
using Aws.Core.Utilities;
using LxCommon.Monitoring;

namespace LxDatafeed.Utilities
{
    public class AppConfig : Aws.Core.Utilities.Config.AppConfig
    {
        protected const string RootConfigPrefix = "AppConfig.";

        public static readonly string AppLocation;
        public static readonly string ExeName;
        
        private const string LtgFlashServerPortConfigKey = "AppConfig.LTGFlashServerPort";
        private const string DataFeedTcpPortPortConfigKey = "AppConfig.DataFeedTcpPort";
        private const string DataFeedTcpSslPortPortConfigKey = "AppConfig.DataFeedTcpSslPort";

        private static readonly ReadOnlyCollection<string> _fileArchiveDirectories;

        static AppConfig()
        {
            // We are going to get the exe folder
            string appLocation = AppUtils.AppLocation;
            
            // Set readonly var's
            AppLocation = AddTrailingSlashToFolder(appLocation);
            Assembly assembly = Assembly.GetExecutingAssembly();
            ExeName = assembly.GetName().Name;

            _fileArchiveDirectories = LoadArchiveDirectories();

       }

    
       
        public static void CreateDirectory(string folder)
        {
            if (!Directory.Exists(folder)) Directory.CreateDirectory(folder);
        }

        /// <summary>
        /// Add the trailing "\" if not there
        /// </summary>
        private static string AddTrailingSlashToFolder(string folder)
        {
            if (null != folder && !folder.Trim().EndsWith(@"\")) folder += @"\";
            return folder;
        }

        public static string LightningDbConnStr
        {
            get { return ConfigurationManager.ConnectionStrings["Lightning"].ConnectionString; }
        }

        public static int PartnerProfileRefreshMinutes
        {
            get { return Get("Db.PartnerProfileRefreshMinutes", 10); }
        }

        public static int KeepAliveSeconds
        {
            get { return Get("Client.KeepAliveSeconds", 15); }
        }

        public static IPAddress PrimaryDataManagerIp
        {
            get { return GetKeyAsIpAddress("PrimaryDataManagerIp"); }
        }

        public static IPAddress SecondaryDataManagerIp
        {
            get { return GetKeyAsIpAddress("SecondaryDataManagerIp"); }
        }

        private static IPAddress GetKeyAsIpAddress(string key)
        {
            string ip = Get<string>(key, null);
            IPAddress ipa = null;
            if (!string.IsNullOrWhiteSpace(ip))
            {
                if (!IPAddress.TryParse(ip, out ipa))
                {
                    EventManager.LogError(TaskMonitor.TidServiceConfigLoadFail, string.Format("Unable to parse key {0} as an IPAddress", key));
                }
            }
            return ipa;
        }

        public static bool EnableFileArchive
        {
            get { return Get("EnableFileArchive", false); }
        }

        public static int FileArchiveDirectoryCount
        {
            get { return Get("FileArchiveDirectoryCount", 0); }
        }

        private static ReadOnlyCollection<string> LoadArchiveDirectories()
        {
            ReadOnlyCollection<string> roDirs = null;
            int count = FileArchiveDirectoryCount;
            if (FileArchiveDirectoryCount > 0)
            {
                List<string> dirs = new List<string>();
                for(int i = 0; i < count; i++)
                {
                    string dir = Get<string>(string.Format("FileArchiveDirectory_{0}", i), null);
                    if (!string.IsNullOrWhiteSpace(dir))
                    {
                        dirs.Add(dir);
                    }
                }

                roDirs = new ReadOnlyCollection<string>(dirs);
            }
            return roDirs;
        }        

        public static ReadOnlyCollection<string> FileArchiveDirectories
        {
            get { return _fileArchiveDirectories; }
        }

        public static string FileArchiveInUseDirectory
        {
            get { return Get("FileArchiveInUseDirectory", "E:\\En\\Archive\\LxDatafeed\\EnGlnNetwork\\InUse"); }
        }

        public static int FileArchiveMovingTimerIntervalSeconds
        {
            get { return Get("FileArchiveMovingTimerIntervalSeconds", 5); }
        }

        public static int FileArchiveMinimumAgeSecondsMoveToPickup
        {
            get { return Get("FileArchiveMinimumAgeSecondsMoveToPickup", 10); }
        }

        public static int FileArchiveMinutessPerFile
        {
            get { return Get("FileArchiveMinutessPerFile", 1); }
        }

        public static int LtgFlashServerPort
        {
            get { return Get(LtgFlashServerPortConfigKey, 95); }
        }

        public static int DataFeedTcpPortPort
        {
            get { return Get(DataFeedTcpPortPortConfigKey, 0); }
        }

        public static int DataFeedTcpSslPortPort
        {
            get { return Get(DataFeedTcpSslPortPortConfigKey, 0); }
        }

        public static string DataFeedSslCertPath
        {
            get { return Get("AppConfig.DataFeedSslCertPath", ""); }
        }

        public static string DataFeedSslCertPw
        {
            get { return Get("AppConfig.DataFeedSslCertPw", ""); }
        }

        public static bool IsLogDetail
        {
            get { return (Get("AppConfig.IsLogDetail", 0) == 1); }
        }

        public static bool AllowMultipleConnections
        {
            get { return (Get("AppConfig.AllowMultipleConnections", 0) == 1); }
        }

        public static string DetailLogPath
        {
            get { return Get("AppConfig.DetailLogPath", ""); }
        }

        public static string SystemType
        {
            get { return Get("SystemType", "TLN"); }
        }
        
        public static int MaxIncomingFlashProcessorTasks 
        {
            get { return Get("MaxIncomingFlashProcessorTasks", 10); }
        }

        public static int RequesterConnectionRateSampleSize 
        {
            get { return Get("RequesterConnectionRateSampleSize", 5); }
        }

        public static int RequesterReceiveRateSampleSize
        {
            get { return Get("RequesterReceiveRateSampleSize", 5); }
        }

        public static int RequesterMaxAgeForFailoverSeconds 
        {
            get { return Get("RequesterMaxAgeForFailoverSeconds ", 30); }
        }

        public static string ArchiveFileName
        {
            get { return Get("ArchiveFileName", "flash-{0}.txt"); }
        }

        public static int MonitorPrimaryPacketRateMinimum
        {
            get { return Get("MonitorPrimaryPacketRateMinimum", 5); }
        }

        public static int MonitorPrimaryConnectionRateMaximum
        {
            get { return Get("MonitorPrimaryConnectionRateMaximum", 1); }
        }

        public static int MonitorSecondaryPacketRateMinimum
        {
            get { return Get("MonitorSecondaryPacketRateMinimum", 5); }
        }

        public static int MonitorSecondaryConnectionRateMaximum
        {
            get { return Get("MonitorSecondaryConnectionRateMaximum", 1); }
        }

        public static int MonitorClientConnectionsMinimum
        {
            get { return Get("MonitorClientConnectionsMinimum", 1); }
        }

        public static int MonitorClientConnectionRateMaximum
        {
            get { return Get("MonitorClientConnectionRateMaximum", 10); }
        }

        public static int MonitorPartnerRefreshAgeMinutesMaximum
        {
            get { return Get("Db.PartnerProfileRefreshMinutes", 10) + 1; }
        }

        public static bool EnableAudit
        {
            get { return Get("EnableAudit", true); }
        }


        public static string AuditBaseUrl
        {
            get { return Get<string>("AuditBaseUrl", null); }
        }

        public static string AuditTableName
        {
            get { return Get<string>("AuditTableName", "LxDatafeedPartner"); }
        }

        public static int FlashAgeSampleSize
        {
            get { return Get("FlashAgeSampleSize", 2000); }
        }

        public static bool TcpKeepAliveEnabled
        {
            get { return Get("TcpKeepAlive.Enabled", false); }
        }

        public static int TcpKeepAliveStartDelayMilliseconds
        {
            get { return Get("TcpKeepAlive.StartDelayMilliseconds", 5000); }
        }

        public static int TcpKeepAliveIntervalMilliseconds
        {
            get { return Get("TcpKeepAlive.IntervalMilliseconds", 1000); }
        }
    }
}
