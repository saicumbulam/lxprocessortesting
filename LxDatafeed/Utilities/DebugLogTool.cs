﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

using Aws.Core.Utilities;
using Aws.Core.Utilities.Threading;

using LxCommon;
using LxCommon.Monitoring;

using LxDatafeed.Data;
using LxDatafeed.Data.Partner;
using LxDatafeed.Data.ResponsePackage;

namespace LxDatafeed.Utilities
{
    internal class DebugLogTool
    {
        private const string LxDataManagerConnectionId = "LXDM";
        private static readonly Dictionary<string, Logger> LoggerListByConnectionId = new Dictionary<string, Logger>();
        private static readonly ReaderWriterLockSlim LockLogger = new ReaderWriterLockSlim();
        private static PartnerData _partnerDataForLxDataMgr;

        public static void Init()
        {
            try
            {
                if (AppConfig.IsLogDetail)
                {
                    AppConfig.CreateDirectory(AppConfig.DetailLogPath);
                }

                // Create dummy partner for LXDM to use with ResponsePackageFormatter
                // This will define what data gets log from the LXDM
                _partnerDataForLxDataMgr = new PartnerData()
                                               {
                                                   ResponseFormatType = ResponseFormatType.Binary,
                                                   ResponsePackageType = ResponsePackageType.FlashAndPulseCombo,
                                                   LxClassificationType = LxClassificationType.CloudToGround | LxClassificationType.InCloud,
                                                   ResponsePackageVersion = 2,
                                                   ResponseDataElements = new HashSet<ResponseDataElement>()
                                                                              {
                                                                                  ResponseDataElement.IcHeight,
                                                                                  ResponseDataElement.LocationError,
                                                                                  ResponseDataElement.Multiplicity,
                                                                                  ResponseDataElement.NumberOfSensors
                                                                              }
                                               };
            }
            catch(Exception ex)
            {
                EventManager.LogWarning(TaskMonitor.DfDebugLogToolFailedInit, "Lightning Data Feed failed initializing DebugLogTool. \r\nReason: " + ex.Message);
            }
        }

        #region RemoveConnection
        public static void RemoveConnection(string remoteIp, PartnerData partnerData)
        {
            string connectionId = GetConnectionId(remoteIp, partnerData);

            try
            {
                LockLogger.EnterUpgradeableReadLock();

                if (LoggerListByConnectionId.ContainsKey(connectionId))
                {
                    try
                    {
                        LockLogger.EnterWriteLock();
                        LoggerListByConnectionId.Remove(connectionId);
                    }
                    finally
                    {
                        LockLogger.ExitWriteLock();
                    }
                }
            }
            catch
            { }
            finally
            {
                LockLogger.ExitUpgradeableReadLock();
            }
        } 
        #endregion

        #region ShutDown
        public static void ShutDown()
        {
            try
            {
                if (LoggerListByConnectionId != null)
                {
                    foreach (Logger logger in LoggerListByConnectionId.Values)
                    {
                        logger.ShutDown();
                    }
                }
            }
            catch (Exception) { }
        } 
        #endregion

        #region LogLightningFromDataManager
        public static void LogLightningFromDataManager(LTGFlash flash)
        {
            if (AppConfig.IsLogDetail)
            {
                ThreadPoolHelper.QueueUserWorkItem(
                    () =>
                        {
                            try
                            {
                                byte[] responsePackage = ResponsePackageFormatter.Format(flash, null, _partnerDataForLxDataMgr);
                                string responsePackageStr = ResponsePackageFormatter.ToString(responsePackage, _partnerDataForLxDataMgr);
                                LogResponsePackage(null, responsePackageStr, null, null, null);
                            }
                            catch
                            {}
                        });
            }
        } 
        #endregion

        #region LogResponsePackageSentToClient
        public static void LogResponsePackageSentToClient(string remoteIp, byte[] responsePackage, PartnerData partnerData)
        {
            if (AppConfig.IsLogDetail)
            {
                ThreadPoolHelper.QueueUserWorkItem(
                    () =>
                        {
                            try
                            {
                                string responsePackageStr = ResponsePackageFormatter.ToString(responsePackage, partnerData);
                                LogResponsePackage(remoteIp, responsePackageStr, partnerData, true, null);
                            }
                            catch
                            {}
                        });
            }
        } 
        #endregion

        #region LogResponsePackageNotSentToClient
        public static void LogResponsePackageNotSentToClient(string remoteIp, byte[] responsePackage, PartnerData partnerData, string reasonNotSent)
        {
            if (AppConfig.IsLogDetail)
            {
                ThreadPoolHelper.QueueUserWorkItem(
                    () =>
                        {
                            try
                            {
                                string responsePackageStr = ResponsePackageFormatter.ToString(responsePackage, partnerData);
                                LogResponsePackage(remoteIp, responsePackageStr, partnerData, false, reasonNotSent);
                                EventManager.LogError(TaskMonitor.DfSendResponsePackFail, "Lightning data not sent to client because [" + reasonNotSent + "] " + responsePackageStr);
                            }
                            catch
                            {}
                        });
            }
        } 
        #endregion

        #region LogResponsePackage
        private static void LogResponsePackage(string remoteIp, string responsePackageStr, PartnerData partnerData, bool? wasSent, string reason)
        {
            try
            {
                string prefix = string.Empty;
                if (wasSent.HasValue)
                {
                    prefix = "[sent=" + wasSent;
                    if (!string.IsNullOrEmpty(reason))
                    {
                        prefix += ", " + reason;
                    }
                    prefix += "]";
                }

                Logger logger = GetLogger(remoteIp, partnerData);
                lock (logger)
                {
                    logger.WriteLine(prefix + responsePackageStr, Logger.MessageType.None, null, true);
                }
            }
            catch
            { }
        } 
        #endregion

        #region GetLogger
        private static Logger GetLogger(string remoteIp, PartnerData partnerData)
        {
            Logger logger = null;

            try
            {
                string id = GetConnectionId(remoteIp, partnerData);

                LockLogger.EnterUpgradeableReadLock();
                if (!LoggerListByConnectionId.ContainsKey(id))
                {   // Doesn't exist so create new logger
                    string fileName = AppConfig.DetailLogPath + id + " - " + DateTime.Now.ToString(AppLogger.DefaultFileNameTimeStampPattern);
                    logger = new Logger(fileName + AppLogger.DefaultExtension);
                    LogHelpInfo(logger, partnerData);

                    try
                    {
                        LockLogger.EnterWriteLock();
                        LoggerListByConnectionId.Add(id, logger);
                    }
                    finally
                    {
                        LockLogger.ExitWriteLock();
                    }
                }
                else
                {   // Logger exists so grab reference
                    logger = LoggerListByConnectionId[id];
                }
            }
            catch
            { }
            finally
            {
                LockLogger.ExitUpgradeableReadLock();
            }

            return logger;
        }

        #region GetConnectionId
        private static string GetConnectionId(string remoteIp, PartnerData partnerData)
        {
            string connectionId;
            if (partnerData == null)
            {
                connectionId = LxDataManagerConnectionId;
            }
            else
            {
                connectionId = partnerData.PartnerId + "-" + remoteIp;
            }
            return connectionId;
        } 
        #endregion

        #region LogHelpInfo
        private static void LogHelpInfo(Logger logger, PartnerData partnerData)
        {
            if (partnerData != null)
            {
                StringBuilder help = new StringBuilder();

                GenerateLogHelp(typeof(ResponseFormatType), "f", help);
                GenerateLogHelp(typeof(ResponsePackageType), "t", help);
                GenerateLogHelp(typeof(LxClassificationType), "class", help);

                logger.WriteLine(help.ToString(), Logger.MessageType.None, null, true);
                logger.WriteLine(GenerateConnectionHelp(partnerData), Logger.MessageType.None, null, true);
            }
        }

        #region GenerateLogHelp
        private static void GenerateLogHelp(Type type, string prefix, StringBuilder help)
        {
            help.Append("[").Append(prefix).Append(": ");

            Array list = Enum.GetValues(type);
            for (int i = 0; i < list.Length; i++)
            {
                object enumText = list.GetValue(i);
                int enumNumeric = Convert.ToInt32(Enum.Parse(type, enumText.ToString()));

                if (enumNumeric != 0)
                {
                    help.Append(enumText + "=" + enumNumeric);
                    if (i < (list.Length - 1))
                    {
                        help.Append(",");
                    }
                }
            }

            help.Append("]");
        }
        #endregion

        #region GenerateConnectionHelp
        private static string GenerateConnectionHelp(PartnerData partnerData)
        {
            StringBuilder connectionHelp = new StringBuilder();

            connectionHelp.Append("[session: v=").Append(partnerData.ResponsePackageVersion);
            connectionHelp.Append("; f=").Append(partnerData.ResponseFormatType);
            connectionHelp.Append("; t=").Append(partnerData.ResponsePackageType);
            connectionHelp.Append("; class=").Append(partnerData.LxClassificationType);
            connectionHelp.Append("; err=").Append(partnerData.HasDataElement(ResponseDataElement.LocationError));
            connectionHelp.Append("; ht=").Append(partnerData.HasDataElement(ResponseDataElement.IcHeight));
            connectionHelp.Append("; sensor=").Append(partnerData.HasDataElement(ResponseDataElement.NumberOfSensors));
            connectionHelp.Append("; multi=").Append(partnerData.HasDataElement(ResponseDataElement.Multiplicity));
            connectionHelp.Append("; ba=").Append(partnerData.BoundingArea != null ? partnerData.BoundingArea.ToString() : string.Empty);
            connectionHelp.Append("]");

            return connectionHelp.ToString();
        }
        #endregion 
        #endregion
        #endregion
    }
}
