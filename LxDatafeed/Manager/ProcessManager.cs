using System;
using System.Net;
using System.Threading;

using Aws.Core.Utilities;
using Aws.Core.Utilities.Threading;

using LxCommon.Monitoring;
using LxCommon.TcpCommunication;

using LxDatafeed.Data.Partner;
using LxDatafeed.Process;
using LxDatafeed.Utilities;

namespace LxDatafeed.Manager
{
    public class ProcessManager : WorkerThread
    {
        private readonly ManualResetEvent _eventStopThread;
        private readonly ManualResetEventSlim _stopEvent;
        private ConnServerManager _connServerManager;

        private Requester _dataManagerPrimaryRequester;
        private Requester _dataManagerSecondaryRequester;
        private IncomingPacketProcessor _incomingPacketProcessor;

        private TaskMonitor _taskMonitor;
        private MonitoringHandler _monitoringHandler;

        public ProcessManager()
        {
            _eventStopThread = new ManualResetEvent(false);
            _stopEvent = new ManualResetEventSlim(false);
            //init task monitor
            InitTaskMonitor();
        }

        protected override RunReturnStatus Run()
        {
            _eventStopThread.Reset(); 

            DebugLogTool.Init();


            if (_monitoringHandler == null)
            {
                _monitoringHandler = new MonitoringHandler(AppConfig.SystemType);
                _monitoringHandler.Start();
            }

            try
            {
                if (!PartnerSvc.Init())
                {
                    EventManager.LogError(TaskMonitor.DfLoadPartnerIdFailed,
                                          "Lightning Data Feed failed loading PartnerId info at starting.");
                    return RunReturnStatus.RunAgain;
                }

                _monitoringHandler.LastPartnerRefreshFunc = () => { return PartnerSvc.LastDbRefreshTime; };
            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.DfLoadPartnerIdFailed,
                                      "Lightning Data Feed failed loading PartnerId info at starting.\r\nReason: "+ ex.Message);
                return RunReturnStatus.RunAgain;
            }
 
            StartConnServerManager();
            if (_connServerManager != null)
            {
                InitalizeCommunication();
                if (_incomingPacketProcessor != null)
                {
                    _monitoringHandler.IncomingPacketProcessor = _incomingPacketProcessor;
                    EventManager.LogInfo(TaskMonitor.TidServiceStarted, "Lightning Data Feed service has started.");
                    return RunReturnStatus.WaitForShutdown;
                }
            }

            //if we need to run again need to close comm to prevent opening multiple connections
            CleanupCommunication();

            return RunReturnStatus.RunAgain;
        }

        protected override void ShutDown()
        {
            EventManager.LogInfo("Lightning Data Feed Service shut down sequence begins.");

            base.ShutDown();
            try
            {
                _eventStopThread.Set();
                _stopEvent.Set();

                CleanupCommunication();

                if (_monitoringHandler != null)
                    {
                    _monitoringHandler.Stop();
                }

            }
            catch (Exception ex)
            {
                EventManager.LogWarning(TaskMonitor.TidServiceStoppedFail, "Lightning Data Feed encountered an error during the shutdown process. Warning...\r\nReason: " + ex.Message);
            }

            EventManager.LogInfo(TaskMonitor.TidServiceStopped, "Lightning Data Feed has stopped.");
        }

        private void InitTaskMonitor()
        {
            _taskMonitor = new TaskMonitor();
            _taskMonitor.AddTask(TaskMonitor.DfConnServerManStartup, 120);
            _taskMonitor.AddTask(TaskMonitor.DfConnectFailtoLxDm, 120);
            _taskMonitor.AddTask(TaskMonitor.DfDataControllerFailedStartup, 120);            
            _taskMonitor.AddTask(TaskMonitor.DfGetIntConfigFail, 120);
            _taskMonitor.AddTask(TaskMonitor.DfInternalConnFailed, 120);
            _taskMonitor.AddTask(TaskMonitor.DfInternalError, 120);
            _taskMonitor.AddTask(TaskMonitor.DfLoadPartnerIdFailed, 120);
            _taskMonitor.AddTask(TaskMonitor.DfLogEventFail, 120);
            _taskMonitor.AddTask(TaskMonitor.DfSendDatatoTcp, 120);
            _taskMonitor.AddTask(TaskMonitor.DfSendResponsePackFail, 120);
            _taskMonitor.AddTask(TaskMonitor.DfTcpConnectFail, 120);
            _taskMonitor.AddTask(TaskMonitor.DfFailedTcpListnerStartUp, 120);
            _taskMonitor.AddTask(TaskMonitor.DfFailedAsyncTcpConn, 120);
            _taskMonitor.AddTask(TaskMonitor.DfQueMaxSizeReached, 120);
            _taskMonitor.AddTask(TaskMonitor.DfGetDataDatetime, 120);
            _taskMonitor.AddTask(TaskMonitor.DfGetDataDouble, 120);
            _taskMonitor.AddTask(TaskMonitor.DfGetDataFloat, 120);
            _taskMonitor.AddTask(TaskMonitor.DfGetDataInt, 120);
            _taskMonitor.AddTask(TaskMonitor.DfGetDataString, 120);
            _taskMonitor.AddTask(TaskMonitor.DfDebugLogToolFailedInit, 120);
            _taskMonitor.AddTask(TaskMonitor.DfLxDataFeedCrashing, 120);
            _taskMonitor.AddTask(TaskMonitor.DfFailedNetworkTypeConn, 120);
        }

        private void InitalizeCommunication()
        {
            
            bool isTlnSystem = AppConfig.SystemType.ToUpper().Equals("TLN");

            try
            {
                FlashLogger fl = null;
                if (AppConfig.EnableFileArchive)
                {
                    fl = new FlashLogger(AppConfig.FileArchiveInUseDirectory, AppConfig.FileArchiveDirectories, AppConfig.ArchiveFileName, AppConfig.FileArchiveMovingTimerIntervalSeconds,
                        AppConfig.FileArchiveMinimumAgeSecondsMoveToPickup, AppConfig.FileArchiveMinutessPerFile, isTlnSystem);
                }

                _incomingPacketProcessor = new IncomingPacketProcessor(_connServerManager, AppConfig.RequesterMaxAgeForFailoverSeconds, fl, _stopEvent, AppConfig.MaxIncomingFlashProcessorTasks);

                IPAddress primary = AppConfig.PrimaryDataManagerIp;
                if (primary != null)
                {
                    IPEndPoint endPoint = new IPEndPoint(primary, AppConfig.LtgFlashServerPort);
                    _dataManagerPrimaryRequester = new Requester("DataManager Primary", endPoint, _incomingPacketProcessor.TcpFlashDecode, 
                        AppConfig.RequesterReceiveRateSampleSize, AppConfig.RequesterConnectionRateSampleSize, _stopEvent);
                    _dataManagerPrimaryRequester.Open();

                    _incomingPacketProcessor.PrimaryRequester = _dataManagerPrimaryRequester;
                }

                IPAddress secondary = AppConfig.SecondaryDataManagerIp;
                if (secondary != null)
                {
                    IPEndPoint endPoint = new IPEndPoint(secondary, AppConfig.LtgFlashServerPort);
                    _dataManagerSecondaryRequester = new Requester("DataManager Secondary", endPoint, _incomingPacketProcessor.TcpFlashDecode,
                        AppConfig.RequesterReceiveRateSampleSize, AppConfig.RequesterConnectionRateSampleSize, _stopEvent);
                    _dataManagerSecondaryRequester.Open();

                    _incomingPacketProcessor.SecondaryRequester = _dataManagerSecondaryRequester;
                }            
            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.DfDataControllerFailedStartup, "failure during initalization of data manager connections", ex);
            }
            

            _monitoringHandler.PrimaryRequester = _dataManagerPrimaryRequester;
            _monitoringHandler.SecondaryRequester = _dataManagerSecondaryRequester;
        }

        private void CleanupCommunication()
        {
            if (_connServerManager != null)
            {
                _connServerManager.Stop();
            }

            if (_dataManagerPrimaryRequester != null)
            {
                _dataManagerPrimaryRequester.Close();
            }

            if (_dataManagerSecondaryRequester != null)
            {
                _dataManagerSecondaryRequester.Close();
            }
        }

        private void StartConnServerManager()
        {
            try
            {
                if (_connServerManager == null)
                {
                    _connServerManager = new ConnServerManager(AppConfig.FlashAgeSampleSize);
                    _connServerManager.StartWorkerThreadSync();
                    _monitoringHandler.ConnServerManager = _connServerManager;
                }
            }
            catch (Exception ex)
            {
                _connServerManager = null;
                EventManager.LogError(TaskMonitor.DfConnServerManStartup, "Lightning Data Feed failed during startup of ConnServerManager. \r\nReason: " + ex.Message);

            }
        }
    }
}
 