using System;
using System.Collections.Concurrent;

using Aws.Core.Utilities;
using Aws.Core.Utilities.Threading;

using LxCommon;
using LxCommon.Monitoring;
using LxCommon.Utilities;

using LxDatafeed.Data;
using LxDatafeed.Net;
using LxDatafeed.Utilities;

namespace LxDatafeed.Manager
{
    public class ConnServerManager : WorkerThread
    {
        private TcpSocketListerner _tcpSocketListerner;
        private bool _checkTcpAval = true;
        private TcpSocketListerner _tcpSslSocketListerner;
        private bool _checkTcpSslAval = true;
        
        private DateTime _lastReset = DateTime.UtcNow;
        private readonly MedianSampler<int> _flashAgeSampler;
        private readonly ConcurrentQueue<LTGFlash> _outboundFlashes;

        public ConnServerManager(int flashAgeSampleSize)
        {
            _flashAgeSampler = new MedianSampler<int>(flashAgeSampleSize);
            _outboundFlashes = new ConcurrentQueue<LTGFlash>();
        }

        public void AddOutboundFlash(LTGFlash flash)
        {
            _outboundFlashes.Enqueue(flash);
        }

        #region WorkerThread Interface
        protected override RunReturnStatus Run()
        {
            try
            {
                if (_checkTcpAval)
                {
                    int port = AppConfig.DataFeedTcpPortPort;
                    _checkTcpAval = (port > 0);
                    if (_tcpSocketListerner == null && port > 0)
                    {
                        _tcpSocketListerner = new TcpSocketListerner(AppConfig.DataFeedTcpPortPort, false);
                        _tcpSocketListerner.StartWorkerThreadSync();
                    }
                }
                if (_checkTcpSslAval)
                {
                    int port = AppConfig.DataFeedTcpSslPortPort;
                    _checkTcpSslAval = (port > 0);
                    if (_tcpSslSocketListerner == null && port > 0)
                    {
                        _tcpSslSocketListerner = new TcpSocketListerner(AppConfig.DataFeedTcpSslPortPort, true);
                        _tcpSslSocketListerner.StartWorkerThreadSync();
                    }
                }

            }
            catch (Exception ex)
            {
                EventManager.LogError(TaskMonitor.DfTcpConnectFail, "Lightning Data Feed ran into an error connecting to TCP. \r\nReason: " + ex.Message);
                
            }

            try
            {
                SendDataToTcpClients();
            }
            catch (Exception exx)
            {
                EventManager.LogError(TaskMonitor.DfSendDatatoTcp, "Lightning Data Feed ran into an error while sending Data to TCP clients. \r\nReason: " + exx.Message);
            }     

            return RunReturnStatus.RunAgain;
        }

        protected override void ShutDown()
        {
            base.ShutDown();
            if (_tcpSocketListerner != null)
            {
                _tcpSocketListerner.Stop();
                _tcpSocketListerner = null;
            }
        }
        #endregion

        #region Private Methods
        private void SendDataToTcpClients()
        {
            LTGFlash flash;

            while (_outboundFlashes.TryDequeue(out flash))
            {
                ClientQueue.SendTcpPacket(flash);
                DateTime ft = LtgTimeUtils.GetUtcDateTimeFromString(flash.FlashTime);
                int age = (int)(DateTime.UtcNow - ft).TotalSeconds;
                _flashAgeSampler.AddSample(age);
            }
        }

        #endregion

        public double ConnectionAttemptRate
        {
            get
            {
                TimeSpan ts = DateTime.UtcNow - _lastReset;
                double minutes = ts.TotalMinutes;

                minutes = minutes > 0 ? minutes : 1;
                double attempts = 0;

                if (_tcpSocketListerner != null)
                {
                    attempts = attempts + _tcpSocketListerner.ConnectionAttempts;
                }

                if (_tcpSslSocketListerner != null)
                {
                    attempts = attempts + _tcpSslSocketListerner.ConnectionAttempts;
                }

                double rate = attempts / minutes;

                //reset the count every 10 mins 
                if (ts.TotalMinutes > 10)
                {
                    _lastReset = DateTime.UtcNow;
                    if (_tcpSocketListerner != null)
                    {
                        _tcpSocketListerner.ResetConnectionAttempts();
                    }

                    if (_tcpSslSocketListerner != null)
                    {
                        _tcpSslSocketListerner.ResetConnectionAttempts();
                    }
                }
                return rate;
            }
        }

        public Tuple<int, int, int> FlashAgeMinMaxMedian
        {
            get { return _flashAgeSampler.GetMinMaxMedian(); }
        }
    }
}
