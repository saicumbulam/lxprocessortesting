﻿using System;

namespace LxDetectionArchiverConsole
{
    public class Program
    {
        private static void Main(string[] args)
        {
            LxDetectionArchiver.Program.Start();

            Console.WriteLine("Press \'q\' to quit the program.");
            while (Console.Read() != 'q') { }

            LxDetectionArchiver.Program.Stop();
        }
    }
}
