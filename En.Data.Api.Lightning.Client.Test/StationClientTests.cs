﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using En.Ods.Lib.Common.Utility.ServiceClient;

using En.Data.Api.Lightning.Client.Models;
using En.Data.Api.Lightning.Client.Utilities;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace En.Data.Api.Lightning.Client.Test
{
    [TestClass]
    public class StationClientTests
    {
        private const string ServiceBaseUrl = "http://localhost:12539";
        [TestMethod]
        public async Task GetAllAsyncTest()
        {
            StationClient client = new StationClient(ServiceBaseUrl);
            OdsServiceResponse<IEnumerable<Station>> r = await client.GetAllAsync();
            Assert.IsTrue(r.IsSuccessStatusCode());
        }

        [TestMethod]
        public async Task GetAsyncTest()
        {
            StationClient client = new StationClient(ServiceBaseUrl);
            string id = "ABLWC";
            OdsServiceResponse<Station> r = await client.GetAsync(id);

            Assert.IsTrue(r.IsSuccessStatusCode());
            Assert.AreEqual(id, r.Result.Id);
        }

        [TestMethod]
        public async Task AddToNetworkAsyncTest()
        {
            StationClient client = new StationClient(ServiceBaseUrl);

            OdsServiceResponse<object> r = await client.AddToNetworkAsync("ABLWC", "AFRI");

            Assert.IsTrue(r.IsSuccessStatusCode());
        }

        [TestMethod]
        public async Task RemoveFromNetworkAsyncTest()
        {
            StationClient client = new StationClient(ServiceBaseUrl);

            OdsServiceResponse<object> r = await client.RemoveFromNetworkAsync("ABLWC", "AFRI");

            Assert.IsTrue(r.IsSuccessStatusCode());
        }

        [TestMethod]
        public async Task GetAllInNetworkAsyncTest()
        {
            StationClient client = new StationClient(ServiceBaseUrl);

            OdsServiceResponse<IEnumerable<Station>> r = await client.GetAllInNetworkAsync("AFRI");

            Assert.IsTrue(r.IsSuccessStatusCode());

            Network n = new Network { Id = "AFRI", Name = "AFRI", Description = "Africa" };

            foreach (Station station in r.Result)
            {
                Assert.IsTrue(station.Networks.Contains(n));
            }
            
        }

        //[TestMethod]
        //public async Task AddAsyncTest()
        //{
        //    Station s = GetTestStation();

        //    StationClient client = new StationClient(ServiceBaseUrl);
        //    OdsServiceResponse<Station> r = await client.AddAsync(s);
        //    Assert.IsTrue(r.IsSuccessStatusCode());

        //    Assert.AreEqual(s.Id, r.Result.Id);
        //    Assert.AreEqual(s.Name, r.Result.Name);
        //}

        //[TestMethod]
        //public async Task UpdateAsyncTest()
        //{
        //    Station s = new Station { Id = "TEST", Name = "Test Station" };

        //    StationClient client = new StationClient(ServiceBaseUrl);
        //    OdsServiceResponse<object> r = await client.UpdateAsync(s.Id, s);
        //    Assert.IsTrue(r.IsSuccessStatusCode());
        //    Assert.AreEqual(r.GetResponseStatusCode(), HttpStatusCode.NoContent);
        //}

        //[TestMethod]
        //public async Task DeleteAsyncTest()
        //{
        //    string id = "TEST";

        //    StationClient client = new StationClient(ServiceBaseUrl);
        //    OdsServiceResponse<object> r = await client.DeleteAsync(id);
        //    Assert.IsTrue(r.IsSuccessStatusCode());
        //    Assert.AreEqual(r.GetResponseStatusCode(), HttpStatusCode.NoContent);
        //}

        private Station GetTestStation()
        {
            return new Station
            {
                Id = "IAF001",
                Name = "Station Name #1",
                SerialNumber = "5375",
                InstallDate = new DateTime(2012, 1, 1),
                IndoorFirmware = "1.1.6.19",
                Ip = "204.156.11.104",
                FirstCallHome = new DateTime(2012, 1, 1),
                LastCallHome = new DateTime(2012, 7, 31),
                LastCalibration = new DateTime(2012, 7, 30),
                StreetAddress = "Dattaram Lad Marg",
                City = "Mumbai",
                Region = null,
                Province = "Maharashtra",
                Country = "India",
                PostalCode = "400012",
                PocName = "Joe Smith",
                PocPhone = "1-555-555-1434",
                PocEmail = "jsmith@gmail.com",
                SendLogRateMinutes = 60,
                SendGpsRateMinutes = 60,
                SendAntennaGainRateMinutes = 15,
                AntennaMode = 9,
                AntennaAttenuation = 40,
                PrimaryDeliveryIp = "8.8.8.1",
                SecondaryDeliveryIp = "8.8.8.2",
                OutdoorFirmware = "3.0.1.50",
                Status = 0
            };
        }
    }
}
