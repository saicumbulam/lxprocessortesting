﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using En.Data.Api.Lightning.Client.Models;
using En.Data.Api.Lightning.Client.Utilities;
using En.Ods.Lib.Common.Utility.ServiceClient;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace En.Data.Api.Lightning.Client.Test
{
    [TestClass()]
    public class PartnerClientTests
    {
        private const string ServiceBaseUrl = "http://localhost:12539";

        [TestMethod()]
        public async Task GetEventsAsyncTest()
        {
            PartnerClient client = new PartnerClient(ServiceBaseUrl);
            OdsServiceResponse<IEnumerable<PartnerEvent>> osr = await client.GetEventsAsync("endev", new DateTime(2015, 4, 13), new DateTime(2015, 4, 14));
            Assert.IsTrue(osr.IsSuccessStatusCode());

        }

      
    }
}
