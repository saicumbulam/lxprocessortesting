﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using En.Ods.Lib.Common.Utility.ServiceClient;

using En.Data.Api.Lightning.Client.Models;
using En.Data.Api.Lightning.Client.Utilities;


namespace En.Data.Api.Lightning.Client.Test
{
    [TestClass]
    public class NetworkClientTests
    {
        private const string ServiceBaseUrl = "http://localhost.fiddler:12539";
        [TestMethod]
        public async Task GetAllAsyncTest()
        {
            NetworkClient client = new NetworkClient(ServiceBaseUrl);
            OdsServiceResponse<IEnumerable<Network>> r = await client.GetAllAsync();
            Assert.IsTrue(r.IsSuccessStatusCode());
        }

        [TestMethod]
        public async Task AddAsyncTest()
        {
            Network n = new Network {Id = "TEST", Name = "Test Network", Description = "A unit test network"};

            NetworkClient client = new NetworkClient(ServiceBaseUrl);
            OdsServiceResponse<Network> r = await client.AddAsync(n);
            Assert.IsTrue(r.IsSuccessStatusCode());

            Assert.AreEqual(n.Id, r.Result.Id);
            Assert.AreEqual(n.Name, r.Result.Name);
            Assert.AreEqual(n.Description, r.Result.Description);
            
        }
        
        [TestMethod]
        public async Task UpdateAsyncTest()
        {
            Network n = new Network { Id = "TEST", Name = "Test Network Updated", Description = "A unit test network updated" };

            NetworkClient client = new NetworkClient(ServiceBaseUrl);
            OdsServiceResponse<Network> r = await client.UpdateAsync(n.Id, n);
            Assert.IsTrue(r.IsSuccessStatusCode());
            Assert.AreEqual(r.GetResponseStatusCode(), HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task DeleteAsyncTest()
        {
            string id = "TEST";

            NetworkClient client = new NetworkClient(ServiceBaseUrl);
            OdsServiceResponse<object> r = await client.DeleteAsync(id);
            Assert.IsTrue(r.IsSuccessStatusCode());
            Assert.AreEqual(r.GetResponseStatusCode(), HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task DeleteAsyncTest2()
        {
            string id = "TEST&T";

            NetworkClient client = new NetworkClient(ServiceBaseUrl);
            OdsServiceResponse<object> r = await client.DeleteAsync(id);
            Assert.IsTrue(r.IsSuccessStatusCode());
            Assert.AreEqual(r.GetResponseStatusCode(), HttpStatusCode.NoContent);
        }

    }
}
