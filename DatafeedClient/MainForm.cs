﻿using System;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using Aws.Core.Utilities;
using System.Configuration;
using LxDatafeed.Data;
using LxDatafeed.Data.ResponsePackage;
using LxDatafeed.Net;

namespace EarthNetworks.Lx.DataFeed.ClientApp
{
    public partial class MainForm : Form
    {
        private Client _client;
        private static string _dfClientId;
        private static string _connStr;
        private StringBuilder _fifo;
        private int _displaySize;


        public MainForm()
        {
            InitializeComponent();
            Init();
        }

        private void Init()
        {
            AppLogger.IsLoggingThreadName = true;
            AppLogger.IsTypeInternalCoreErrorEnabled = true;
            AppLogger.IsTypeInternalCoreWarningEnabled = true;
            AppLogger.Init("LgtDataFeed Test Client", Environment.CurrentDirectory + @"\logs\", "Log");
            this.Text += " - (v" + AppUtils.GetVersion(Assembly.GetExecutingAssembly().GetName().Name) + ")";

            btConnect.Enabled = true;
            btDisconnect.Enabled = false;
            textBoxIpAddress.Text = AppConfig.IpAddress;
            textBoxPort.Text = AppConfig.Port;
            textBoxPartnerId.Text = AppConfig.PartnerId;
            checkBoxDataLogFile.Checked = false;
            checkBoxDataLogDb.Checked = false;
            checkBoxMetadata.Enabled = false;
            comboBoxVersion.SelectedItem = "3";
 
            _client = new Client(this);

            _dfClientId = ConfigurationManager.AppSettings["DFClientID"];
            _connStr = ConfigurationManager.AppSettings["DBConnStr"];
            timerForm.Interval = 100;
            //timerForm.Start();

            _displaySize = 50000;
            _fifo = new StringBuilder(_displaySize * 2);

            AppLogger.Log("The LxDataTestClient started.");
        }

        private void Reset()
        {
            textBoxNumPacketsReceived.Text = string.Empty;
            textBoxResults.Text = string.Empty;
            _fifo.Clear();
        }  
        
        private void PerformConnection()
        {
            try
            {
                string ipAddress = textBoxIpAddress.Text;
                int port = Convert.ToInt16(textBoxPort.Text);
                
                if (_client != null)
                {
                    _client.DoConnect(ipAddress, port, textBoxConnStr.Text, ConvertUiToConnStrData(),
                                         radioTransportTcpSsl.Checked, _dfClientId, _connStr);
                }
              }
            catch (Exception ex)
            {
                AppLogger.LogError("Error:", ex);
            }
        }

        private delegate void AppendTextCallback(string s);

        internal void WritePacketToUi(string s)
        {
            if (textBoxResults.InvokeRequired)
            {
                AppendTextCallback d = WritePacketToUi;
                Invoke(d, new object[] { s });
            }
            else
            {
                s = s.Replace("[", string.Empty).Replace("]", string.Empty) + Environment.NewLine;
                textBoxResults.AppendText(s);
                int len = textBoxResults.Text.Length;
                if (len > _displaySize)
                {
                    textBoxResults.Text = textBoxResults.Text.Remove(0, len - _displaySize);
                }

                textBoxResults.SelectionStart = textBoxResults.TextLength; // make sure view is to the latest
                textBoxResults.ScrollToCaret();

            }
        }

        private string AppendToFifo(string s, StringBuilder fifo, int displaySize)
        {
            
            if (fifo.Length + s.Length > fifo.Capacity)
            {
                // FACT: we will overflow the fifo
                // therefore, keep only data in the fifo that remains on the display
                fifo.Remove(0, fifo.Length + s.Length - displaySize);
            }
            fifo.Append(s);
            if (fifo.Length <= displaySize)
            {
                // FACT: the entire fifo content fits on the display
                // therefore, send it all
                return fifo.ToString();
            }
            // FACT: the fifo content exceed the display size
            // therefore, extract just the tail
            return fifo.ToString(fifo.Length - displaySize, displaySize);
        }


        private void TimerFormTick(object sender, EventArgs e)
        {  
            timerForm.Stop();

            try
            {
                string msg = null;
                do
                {
                    msg = MsgQueue.GetNextMsg();

                    if (!string.IsNullOrEmpty(msg))
                    {
                        textBoxResults.AppendText(msg.Replace("[", string.Empty).Replace("]", string.Empty) + Environment.NewLine);
                        //string temp = _textBoxResults.Text + msg.Replace("[", string.Empty).Replace("]", string.Empty) + Environment.NewLine;
                        //if (_textBoxResults.MaxLength > temp.Length)
                        //    _textBoxResults.Text += msg.Replace("[", string.Empty).Replace("]", string.Empty) + Environment.NewLine;
                        //else
                        //{
                        //    _textBoxResults.Text = msg.Replace("[", string.Empty).Replace("]", string.Empty) + Environment.NewLine;
                        //}
                        string numPacketsReceived = textBoxNumPacketsReceived.Text;
                        if (string.IsNullOrEmpty(numPacketsReceived))
                        {
                            textBoxNumPacketsReceived.Text = "1";
                        }
                        else
                        {
                            textBoxNumPacketsReceived.Text = (Convert.ToInt32(textBoxNumPacketsReceived.Text) + 1).ToString();
                        }
                    }
                    //isDone = true;
                    //FlashObj lgtData = MsgQueue.GeLgtDataRaw();
                    //if (lgtData != null && lgtData.GetStrokeType() != StrokTypes.NA)
                    //{
                    //    //LogLgtData(lgtData);
                    //    isDone = false;
                    //    //if (lgtData.GetStrokeType() != StrokTypes.KeepAliveDataType)
                    //    //{
                    //    //    MsgQueue.AddLgtDataDB(lgtData);
                    //    //}
                    //}
                } while (!string.IsNullOrEmpty(msg));
            }
            catch (Exception ex)
            {
                AppLogger.LogError("Error In timeTick", ex);
            }
            finally
            {
                timerForm.Start();
            }
        }

        private ConnectionStringParser.Data ConvertUiToConnStrData()
        {
            ConnectionStringParser.Data data = new ConnectionStringParser.Data()
            {
                PartnerId = textBoxPartnerId.Text,
                Version = Version
            };

            if (!string.IsNullOrEmpty(textBoxBoundingArea.Text))
            {
                data.BoundingArea = textBoxBoundingArea.Text;
            }

            // ResponsePackageType
            if (radioButtonPackageTypePulse.Checked)
            {
                data.ResponsePackageType = ResponsePackageType.Pulse;
            }
            else if (radioButtonPackageTypeFlash.Checked)
            {
                data.ResponsePackageType = ResponsePackageType.Flash;
            }
            else
            {
                data.ResponsePackageType = ResponsePackageType.FlashAndPulseCombo;
            }

            // ResponseFormatType
            if (data.ResponsePackageType == ResponsePackageType.FlashAndPulseCombo)
            {   // if combo then force binary
                data.ResponseFormatType = ResponseFormatType.Binary;
                radioButtonFormatBinary.Checked = true;
                radioButtonFormatAscii.Checked = false;
            }
            else if (radioButtonFormatAscii.Checked)
            {
                data.ResponseFormatType = ResponseFormatType.Ascii;
            }
            else
            {
                data.ResponseFormatType = ResponseFormatType.Binary;
            }

            // LxClassificationType
            if (checkBoxLxClassCloudToGround.Checked)
            {
                data.LxClassificationType |= LxClassificationType.CloudToGround;
            }

            if (checkBoxLxClassInCloud.Checked)
            {
                data.LxClassificationType |= LxClassificationType.InCloud;
            }

            // Data Elements
            if (checkBoxMetadata.Checked)
            {
                data.Metadata = true;
            }

            return data;
        }

        private short Version
        {
            get
            {
                short version = 0;
                if (!short.TryParse(comboBoxVersion.Text, out version))
                {
                    version = 1;
                }

                return version;
            }
        }

        private void buttonGenerateConnStr_Click_1(object sender, EventArgs e)
        {
            ConnectionStringParser.Data data = ConvertUiToConnStrData();

            if (data.Version >= 2)
            {
                textBoxConnStr.Text = data.ToJson();
            }
            else
            {
                textBoxConnStr.Text = "AWSLGT" + data.PartnerId + "END";
            }
        }

        private void BtConnectClick(object sender, EventArgs e)
        {
            Reset();
            btConnect.Enabled = false;
            btDisconnect.Enabled = true;
            _client.ConnectionTime = DateTime.UtcNow;
            _client.SaveToFile = checkBoxDataLogFile.Checked;
            _client.SaveToDb = checkBoxDataLogDb.Checked;
            _client.IsDetailed = radioButtonDisplayDetailed.Checked;
            PerformConnection();
        }

        private void BtDisconnectClick(object sender, EventArgs e)
        {
            if (_client != null)
            {
                _client.DoDisconnect();
            }
            btConnect.Enabled = true;
            btDisconnect.Enabled = false;
        }

        private void RadioButtonPackageTypeComboCheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonPackageTypeCombo.Checked)
            {
                radioButtonFormatBinary.Checked = true;
                radioButtonFormatAscii.Enabled = false;
            }
            else
            {
                radioButtonFormatAscii.Enabled = true;
            }
        }

        private void comboBoxVersion_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox comboBox = (ComboBox)sender;
            if ((string)comboBox.SelectedItem == "3")
            {
                checkBoxMetadata.Enabled = true;
            }
            else
            {
                checkBoxMetadata.Enabled = false;
                checkBoxMetadata.Checked = false;
            }

            if ((string) comboBox.SelectedItem == "1")
            {
                labelNote.Visible = true;
            }
            else
            {
                labelNote.Visible = false;
            }
        }


        
     
    }
}
