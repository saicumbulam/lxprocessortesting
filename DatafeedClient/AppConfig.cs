﻿using System;

namespace EarthNetworks.Lx.DataFeed.ClientApp
{
    internal class AppConfig : Aws.Core.Utilities.Config.AppConfig
    {
        public static string IpAddress
        {
            get { return Get<string>("IpAddress"); }
        }

        public static string Port
        {
            get { return Get<string>("Port"); }
        }

        public static string PartnerId
        {
            get { return Get<string>("PartnerId"); }
        }

        public static string DataLogDirectory
        {
            get { return Get<string>("DataLogDirectory"); }
        }
    }
}
