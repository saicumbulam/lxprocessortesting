using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Threading;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using Aws.Core.Utilities;
using Aws.Core.Utilities.Threading;

namespace EarthNetworks.Lx.DataFeed.ClientApp
{
    internal class DbSaver : WorkerThread
    {
        #region Class Level Variables
      //  private string _sConn = ConfigurationManager.AppSettings["DBConnStr"];
        private SqlConnection _conn = null;

        #endregion

        #region Constructor
        public DbSaver()
        {
        }
        #endregion


        #region WorkerThread Interface
        protected override RunReturnStatus Run()
        {  
            //if (!DoJob())
            ////if (!SaveData())
            //    WaitTimeSecondsBetweenRun = 15;
            //else
            //    WaitTimeSecondsBetweenRun = 2;
            return RunReturnStatus.RunAgain;
        }

        protected override void ShutDown()
        {
            base.ShutDown();
            if (_conn != null && _conn.State != ConnectionState.Closed)
            {
                _conn.Close();
            }
        }

        #endregion

        #region Private Methods
        /*
        private bool DoJob()
        {
            try
            {
                int n = MsgQueue.GetLgtDataDbNum();
                for (int i = 0; i < n; i++)
                {
                    FlashObj flashObj = MsgQueue.GeLgtDataDB();
                    if (flashObj == null)
                        return true;

                    if (flashObj.GetFlashObjType() == FlashObjType.Portion)
                    {
                        SavePortionData(flashObj, 0);
                    }
                    else
                    {
                        SaveCombFlash(flashObj);
                    }

                    
                    //if (flashObj.GetFlashObjType() == FlashObjType.Combined)
                    //{
                    //    SaveCombFlash(flashObj);
                    //}
                    //else
                    //{
                    //    SavePortionData(flashObj, 0);

                    //    //SaveData();
                    //}
                    
                }
            }
            catch (SqlException se)
            {
                AppLogger.LogError("error at save db", se);
                _conn.Close();
                return false;
            }
            catch (Exception ex)
            {
                AppLogger.LogError("error at save db2", ex);
                return false;
            }
            return true;
   
        }

        
        private void SaveCombFlash(FlashObj flashObj)
        {
            if (flashObj == null || flashObj.GetFlashObjType() == FlashObjType.NA || flashObj.GetFlashObjType()==FlashObjType.Portion)
            {
                return;
            }

            //here need to save flash object first
            int flashId =SaveFlash(flashObj);
            if (flashId == 0)
            {
                return;
                //int nnn = 0;
            }

            List<FlashObj> portionsList = flashObj.GetPortionsList();
            if (portionsList == null || portionsList.Count == 0)
                return;

            for (int i = 0; i < portionsList.Count; i++)
            {
                FlashObj portionObj = portionsList[i];
                SavePortionData(portionObj, flashId); 
            }

        }

        private Int32 SaveFlash(FlashObj flashObj)
        {
            if (flashObj == null)
                return 0;

            try
            {
                if (_conn == null)
                {
                    _conn = new SqlConnection(_sConn);
                }
                if (_conn.State != ConnectionState.Open)
                {
                    _conn.Open();
                }

                SqlCommand cmd = null;
                cmd = _conn.CreateCommand();
                cmd.CommandText = "dbo.LgtInsertFlashForDataFeed_pr";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@p_Time", SqlDbType.DateTime));
                cmd.Parameters.Add(new SqlParameter("@p_Latitude", SqlDbType.Decimal));
                cmd.Parameters.Add(new SqlParameter("@p_Longitude", SqlDbType.Decimal));
                cmd.Parameters.Add(new SqlParameter("@p_Type", SqlDbType.Int));
                cmd.Parameters.Add(new SqlParameter("@p_Amp", SqlDbType.Decimal));

                cmd.Parameters["@p_Time"].Value = flashObj.GetLgtTime();
                cmd.Parameters["@p_Latitude"].Value = flashObj.GetLatitude();
                cmd.Parameters["@p_Longitude"].Value = flashObj.GetLongitude();
                cmd.Parameters["@p_Type"].Value = (int)flashObj.GetStrokeType();
                cmd.Parameters["@p_Amp"].Value = flashObj.GetAmp();

                SqlParameter flashId =  cmd.Parameters.Add(new SqlParameter("@p_flashId", SqlDbType.BigInt));
                flashId.Direction = ParameterDirection.Output;
                 SqlDataReader sqlreader =  cmd.ExecuteReader();
                //cmd.
                 sqlreader.Close();
                 Int32 id = Convert.ToInt32(flashId.Value);

                //int id = Convert.ToInt16(flashId.Value);
                return id;



            }
            catch (SqlException se)
            {
                AppLogger.LogError("error at save db", se);
                _conn.Close();
                return 0;
            }
            catch (Exception ex)
            {
                AppLogger.LogError("error at save db2", ex);
                return 0;
            }
        }

        private bool SavePortionData(FlashObj flashObj, Int32 flashId)
        {
            try
            {
                if (flashObj == null)
                   return true;

                if (_conn == null)
                {
                        _conn = new SqlConnection(_sConn);
                }
                if (_conn.State != ConnectionState.Open)
                {
                     _conn.Open();
                }

                SqlCommand cmd = null;
                cmd = _conn.CreateCommand();
                cmd.CommandText = "LgtInsertPortionsForDataFeed_pr";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@p_Time", SqlDbType.DateTime));
                cmd.Parameters.Add(new SqlParameter("@p_Latitude", SqlDbType.Decimal));
                cmd.Parameters.Add(new SqlParameter("@p_Longitude", SqlDbType.Decimal));
                cmd.Parameters.Add(new SqlParameter("@p_Type", SqlDbType.Int));
                cmd.Parameters.Add(new SqlParameter("@p_Amp", SqlDbType.Decimal));
                cmd.Parameters.Add(new SqlParameter("@p_flashId", SqlDbType.BigInt));

                cmd.Parameters["@p_Time"].Value = flashObj.GetLgtTime();
                cmd.Parameters["@p_Latitude"].Value = flashObj.GetLatitude();
                cmd.Parameters["@p_Longitude"].Value = flashObj.GetLongitude();
                cmd.Parameters["@p_Type"].Value = flashObj.GetStrokeType();
                cmd.Parameters["@p_Amp"].Value = flashObj.GetAmp();
                cmd.Parameters["@p_flashId"].Value = flashId;
                cmd.ExecuteNonQuery();
          

            }
            catch (SqlException se)
            {
                AppLogger.LogError("error at save db", se);
                _conn.Close();
                return false;
            }
            catch (Exception ex)
            {
                AppLogger.LogError("error at save db2", ex);
                return false;
            }
            return true;
        }

        private bool SaveData() //old functions
        {
            try
            {
                int n = MsgQueue.GetLgtDataDbNum();
                for (int i = 0; i < n; i++)
                {
                    FlashObj flashObj = MsgQueue.GeLgtDataDB();
                    if (flashObj == null)
                        return true;

                    if (_conn == null)
                    {
                        _conn = new SqlConnection(_sConn);
                    }
                    if (_conn.State != ConnectionState.Open)
                    {
                        _conn.Open();
                    }

                    SqlCommand cmd = null;
                    cmd = _conn.CreateCommand();
                    cmd.CommandText = "LgtInsertFlashPortionForDataFeed_pr";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@p_Time", SqlDbType.DateTime));
                    cmd.Parameters.Add(new SqlParameter("@p_Latitude", SqlDbType.Decimal));
                    cmd.Parameters.Add(new SqlParameter("@p_Longitude", SqlDbType.Decimal));
                    cmd.Parameters.Add(new SqlParameter("@p_Type", SqlDbType.Int));
                    cmd.Parameters.Add(new SqlParameter("@p_Amp", SqlDbType.Decimal));

                    cmd.Parameters["@p_Time"].Value = flashObj.GetLgtTime();
                    cmd.Parameters["@p_Latitude"].Value = flashObj.GetLatitude();
                    cmd.Parameters["@p_Longitude"].Value = flashObj.GetLongitude();
                    cmd.Parameters["@p_Type"].Value = flashObj.GetStrokeType();
                    cmd.Parameters["@p_Amp"].Value = flashObj.GetAmp();
                    cmd.ExecuteNonQuery();
                }

            }
            catch (SqlException se)
            {
                AppLogger.LogError("error at save db", se);
                _conn.Close();
                return false;
            }
            catch (Exception ex)
            {
                AppLogger.LogError("error at save db2", ex);
                return false;
            }
            return true;
        }
         * */
        #endregion


        #region Properties

        #endregion
    }
}
