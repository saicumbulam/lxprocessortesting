﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Aws.Core.Utilities;

namespace EarthNetworks.Lx.DataFeed.ClientApp
{
    public enum FlashObjType
    {
        NA = -1,
        Portion = 0,
        Flash = 1,
        Combined = 2
    }

    public enum PacketFormatTys
    {
        //107-107=0(CG Binary portion) 1(CC Binary portion) 2(CG ascii portion) 3(CC ascii portion)
        //4(CG Binary Flash) 5(CC Binary Flash) 6(CG ascii Flash) 7(CC ascii Flash) 
        //8(combination)
        NA = -1,
        BinaryPortion = 0,
        AsciiPortion  = 1,
        BinaryFlash   = 2,
        AsciiFlash    = 3,
        CombFlash     = 4
     }

    public enum StrokTypes
    {
        NA = -1,
        CloudToGround = 0,  //CG
        CloudToCloud = 1,   //CC  
        KeepAliveDataType = 9 //keep alive type
    }

    class Utility
    {
        #region Constants
        public const int PacketBinaryLen = 24;
        public const int PacketAsciiLen = 59;
        public const int DataTypeOffset = 1;
        public const int TimeOffset = 2;
        public const int MillSecOffset = 6;
        public const int LatitudeOffset = 8;
        public const int LongitudeOffset = 12;
        public const int AmplitudeOffset = 16;

        public const int KeepAliveDataType = 9;
        public const int InvalidType = -1;

        public const int FlasComReqestType = 115;
        public const int FlashComLenStartInx = 0;
        public const int FlashComReTypeStartInx = 2;
        public const int FlashComStroksNumStartInx = 3;
        public const int FlashComReservedStInx = 4;
        public const int FlashComFlashPackStInx = 7;
        public const int FlashComExtrBys = 8; //2(totalLen) + 1(115 reqType) + 1(numStroks) + 3(reserved) +1(checksum, last one)
  

        //private static bool _isAscii = false;
        //private static bool _isFlash = false;
        //private static bool _isCombFlash = false;       
     
        
        public static DateTime EpochTime = new DateTime(1970, 1, 1,0,0,0);
        #endregion


 
        public static bool Int32To4BytesLSBFirstOrNot(bool lsbFirst,Int32 num, byte[] bytesResult, int offset)//LSB first...MSB
        {
            if (num >= 0)
            {
                return UInt32To4BytesLSBFirstOrNot(lsbFirst,(UInt32)num, bytesResult, offset);
            }
            else
            {
                UInt32  numConverted =(UInt32) (UInt32.MaxValue+num+1);
                return UInt32To4BytesLSBFirstOrNot(lsbFirst,(UInt32)numConverted, bytesResult, offset);
            }        
        }

        public static bool UInt32To4BytesLSBFirstOrNot(bool lsbFirst, UInt32 num, byte[] bytesResult, int offset)//LSB first...MSB
        {
            UInt32 p2 = Convert.ToUInt32(Math.Pow(256, 2));
            UInt32 p3 = Convert.ToUInt32(Math.Pow(256, 3));
            UInt16 nTemp0 = 0;
            UInt16 nTemp1, nTemp2, nTemp3;
            try
            {
                nTemp0 = (UInt16)(num / p3);
                nTemp1 = (UInt16)((num - nTemp0 * p3) / p2);
                nTemp2 = (UInt16)((num - nTemp0 * p3 - nTemp1 * p2) / 256);
                nTemp3 = (UInt16)(num - nTemp0 * p3 - nTemp1 * p2 - nTemp2 * 256);
                if (lsbFirst)
                {
                    bytesResult[offset + 3] = Convert.ToByte(nTemp0);
                    bytesResult[offset + 2] = Convert.ToByte(nTemp1);
                    bytesResult[offset + 1] = Convert.ToByte(nTemp2);
                    bytesResult[offset + 0] = Convert.ToByte(nTemp3);
                }
                else
                {
                    bytesResult[offset + 0] = Convert.ToByte(nTemp0);
                    bytesResult[offset + 1] = Convert.ToByte(nTemp1);
                    bytesResult[offset + 2] = Convert.ToByte(nTemp2);
                    bytesResult[offset + 3] = Convert.ToByte(nTemp3);   

                }
            }
            catch (Exception e)
            {
                EventManager.LogError("ErroInUInt32ToBytes", e);
                return false;
            }
            return true;
        }

        public static bool UInt16To2BytesLSBFirstOrNot(bool lsbFirst,UInt16 num, byte[] bytesResult, int offset)//LSB first...MSB
        {
            try
            {
                UInt16 nTemp0 = (UInt16)(num / 256);
                UInt16 nTemp1 = (UInt16)(num - nTemp0 * 256);
                if (lsbFirst)
                {
                    bytesResult[offset + 1] = Convert.ToByte(nTemp0);
                    bytesResult[offset + 0] = Convert.ToByte(nTemp1);
                }
                else
                {
                    bytesResult[offset + 0] = Convert.ToByte(nTemp0);
                    bytesResult[offset + 1] = Convert.ToByte(nTemp1);
                }
            }
            catch (Exception e)
            {
                EventManager.LogError("ErroInUInt16To2Bytes", e);
                return false;
            }
            return true;
        }

        public static bool Int16To2BytesLSBFirstOrNot(bool lsbFirst, Int16 num, byte[] bytesResult, int offset)//LSB first...MSB
        {
            if (num >= 0)
            {
                return UInt16To2BytesLSBFirstOrNot(lsbFirst,(UInt16)num, bytesResult, offset);
            }
            else
            {
                UInt16 numConverted = (UInt16)(UInt16.MaxValue +num + 1);
                return UInt16To2BytesLSBFirstOrNot(lsbFirst,(UInt16)numConverted, bytesResult, offset);
            } 
          }

        public static UInt16 TowBytesToUInt16LSBFirstOrNot(bool lsbFirst,byte[] bytes, int offset) //LSB first
        {
            if (lsbFirst)
            {
                return (UInt16)(bytes[offset] + bytes[offset + 1] * 256);
            }
            else
            {
                return (UInt16)(bytes[offset+1] + bytes[offset] * 256);     

            }
        }

        public static Int16 TowBytesToInt16LSBFirstOrNot(bool lsbFirst,byte[] bytes, int offset)
        {
            UInt16 u = TowBytesToUInt16LSBFirstOrNot(lsbFirst,bytes, offset);
            if (lsbFirst)
            {
                if (bytes[offset + 1] >= 128)
                {
                    unchecked
                    {
                        return (Int16)((UInt16.MaxValue - TowBytesToUInt16LSBFirstOrNot(lsbFirst, bytes, offset) + 1) * (-1));
                    }
                }
                else
                {
                    return (Int16)TowBytesToUInt16LSBFirstOrNot(lsbFirst, bytes, offset);
                }
            }
            else
            {
                if (bytes[offset + 0] >= 128)
                {
                    unchecked
                    {
                        return (Int16)((UInt16.MaxValue - TowBytesToUInt16LSBFirstOrNot(lsbFirst, bytes, offset) + 1) * (-1));
                    }
                }
                else
                {
                    return (Int16)TowBytesToUInt16LSBFirstOrNot(lsbFirst, bytes, offset);
                }
            }
        }

        public static UInt32 FourBytesToUInt32LSBFirstOrNot(bool lsbFirst,byte[] bytes, int offset) //LSB first
        {
            if (lsbFirst)
            {
                return (UInt32)(bytes[offset] + bytes[offset + 1] * 256 + bytes[offset + 2] * 256 * 256 + bytes[offset + 3] * 256 * 256*256);
            }
            else
            {
                return (UInt32)(bytes[offset+3] + bytes[offset + 2] * 256 + bytes[offset + 1] * 256 * 256 + bytes[offset + 0] * 256 * 256*256);
          

            }
        }

        public static Int32 FourBytesToInt32LSBFirstOrNot(bool lsbFirst,byte[] bytes, int offset)
        {
            if (lsbFirst)
            {
                if (bytes[offset + 3] >= 128)
                {
                    unchecked
                    {
                        return (Int32)((UInt32.MaxValue - FourBytesToUInt32LSBFirstOrNot(lsbFirst, bytes, offset) + 1) * (-1));
                    }
                }
                else
                {
                    return (Int32)FourBytesToUInt32LSBFirstOrNot(lsbFirst, bytes, offset);
                }
            }
            else
            {
                if (bytes[offset] >= 128)
                {
                    unchecked
                    {
                        return (Int32)((UInt32.MaxValue - FourBytesToUInt32LSBFirstOrNot(lsbFirst, bytes, offset) + 1) * (-1));
                    }
                }
                else
                {
                    return (Int32)FourBytesToUInt32LSBFirstOrNot(lsbFirst, bytes, offset);
                }


            }
        }

        //public static void setIsAscii(bool isAscII)
        //{
        //    _isAscii = isAscII;            
        //}

        //public static void setIsFlash(bool isFlash)
        //{
        //    _isFlash = isFlash;
        //}

        //public static bool IsAscii()
        //{
        //    return _isAscii;
        //}

        //public static bool IsFlash()
        //{
        //    return _isFlash;
        //}

        //public static void setIsCombFlash(bool isCombFlash)
        //{
        //    _isCombFlash = isCombFlash;
        //}

        //public static bool IsComFlash()
        //{
        //    return _isCombFlash;
        //} 
                

    }
}
