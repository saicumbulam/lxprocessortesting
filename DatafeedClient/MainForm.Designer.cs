﻿namespace EarthNetworks.Lx.DataFeed.ClientApp
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.checkBoxMetadata = new System.Windows.Forms.CheckBox();
            this.labelNote = new System.Windows.Forms.Label();
            this.textBoxBoundingArea = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxNumPacketsReceived = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonGenerateConnStr = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.textBoxConnStr = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.radioButtonPackageTypeCombo = new System.Windows.Forms.RadioButton();
            this.radioButtonPackageTypeFlash = new System.Windows.Forms.RadioButton();
            this.radioButtonPackageTypePulse = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.checkBoxLxClassCloudToGround = new System.Windows.Forms.CheckBox();
            this.checkBoxLxClassInCloud = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButtonFormatBinary = new System.Windows.Forms.RadioButton();
            this.radioButtonFormatAscii = new System.Windows.Forms.RadioButton();
            this.textBoxPartnerId = new System.Windows.Forms.TextBox();
            this.textBoxIpAddress = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxPort = new System.Windows.Forms.TextBox();
            this.btDisconnect = new System.Windows.Forms.Button();
            this.btConnect = new System.Windows.Forms.Button();
            this.timerForm = new System.Windows.Forms.Timer(this.components);
            this.textBoxResults = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.radioTransportTcpSsl = new System.Windows.Forms.RadioButton();
            this.radioTransportTCP = new System.Windows.Forms.RadioButton();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.checkBoxDataLogDb = new System.Windows.Forms.CheckBox();
            this.checkBoxDataLogFile = new System.Windows.Forms.CheckBox();
            this.comboBoxVersion = new System.Windows.Forms.ComboBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.radioButtonDisplayDetailed = new System.Windows.Forms.RadioButton();
            this.radioButtonDisplaySummary = new System.Windows.Forms.RadioButton();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.checkBoxMetadata);
            this.groupBox4.Location = new System.Drawing.Point(525, 73);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(141, 100);
            this.groupBox4.TabIndex = 66;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Data Elements";
            // 
            // checkBoxMetadata
            // 
            this.checkBoxMetadata.AutoSize = true;
            this.checkBoxMetadata.Location = new System.Drawing.Point(14, 27);
            this.checkBoxMetadata.Name = "checkBoxMetadata";
            this.checkBoxMetadata.Size = new System.Drawing.Size(71, 17);
            this.checkBoxMetadata.TabIndex = 0;
            this.checkBoxMetadata.Text = "Metadata";
            this.checkBoxMetadata.UseVisualStyleBackColor = true;
            // 
            // labelNote
            // 
            this.labelNote.AutoSize = true;
            this.labelNote.ForeColor = System.Drawing.Color.Red;
            this.labelNote.Location = new System.Drawing.Point(101, 211);
            this.labelNote.Name = "labelNote";
            this.labelNote.Size = new System.Drawing.Size(399, 13);
            this.labelNote.TabIndex = 65;
            this.labelNote.Text = "Make sure the UI settings above match the settings for this partner in the databa" +
    "se.";
            this.labelNote.Visible = false;
            // 
            // textBoxBoundingArea
            // 
            this.textBoxBoundingArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxBoundingArea.Location = new System.Drawing.Point(91, 181);
            this.textBoxBoundingArea.Name = "textBoxBoundingArea";
            this.textBoxBoundingArea.Size = new System.Drawing.Size(699, 20);
            this.textBoxBoundingArea.TabIndex = 64;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 184);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 13);
            this.label7.TabIndex = 63;
            this.label7.Text = "Bounding Area:";
            // 
            // textBoxNumPacketsReceived
            // 
            this.textBoxNumPacketsReceived.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxNumPacketsReceived.Location = new System.Drawing.Point(690, 283);
            this.textBoxNumPacketsReceived.Name = "textBoxNumPacketsReceived";
            this.textBoxNumPacketsReceived.ReadOnly = true;
            this.textBoxNumPacketsReceived.Size = new System.Drawing.Size(100, 20);
            this.textBoxNumPacketsReceived.TabIndex = 62;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(580, 286);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(108, 13);
            this.label4.TabIndex = 61;
            this.label4.Text = "# Packets Received:";
            // 
            // buttonGenerateConnStr
            // 
            this.buttonGenerateConnStr.Location = new System.Drawing.Point(473, 11);
            this.buttonGenerateConnStr.Name = "buttonGenerateConnStr";
            this.buttonGenerateConnStr.Size = new System.Drawing.Size(169, 23);
            this.buttonGenerateConnStr.TabIndex = 59;
            this.buttonGenerateConnStr.Text = "Generate Connection String";
            this.buttonGenerateConnStr.UseVisualStyleBackColor = true;
            this.buttonGenerateConnStr.Click += new System.EventHandler(this.buttonGenerateConnStr_Click_1);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(9, 211);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(94, 13);
            this.label10.TabIndex = 58;
            this.label10.Text = "Connection String:";
            // 
            // textBoxConnStr
            // 
            this.textBoxConnStr.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxConnStr.Location = new System.Drawing.Point(12, 228);
            this.textBoxConnStr.Multiline = true;
            this.textBoxConnStr.Name = "textBoxConnStr";
            this.textBoxConnStr.Size = new System.Drawing.Size(778, 25);
            this.textBoxConnStr.TabIndex = 57;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.radioButtonPackageTypeCombo);
            this.groupBox3.Controls.Add(this.radioButtonPackageTypeFlash);
            this.groupBox3.Controls.Add(this.radioButtonPackageTypePulse);
            this.groupBox3.Location = new System.Drawing.Point(336, 73);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(183, 100);
            this.groupBox3.TabIndex = 56;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Response Package Type";
            // 
            // radioButtonPackageTypeCombo
            // 
            this.radioButtonPackageTypeCombo.AutoSize = true;
            this.radioButtonPackageTypeCombo.Location = new System.Drawing.Point(19, 73);
            this.radioButtonPackageTypeCombo.Name = "radioButtonPackageTypeCombo";
            this.radioButtonPackageTypeCombo.Size = new System.Drawing.Size(142, 17);
            this.radioButtonPackageTypeCombo.TabIndex = 2;
            this.radioButtonPackageTypeCombo.Text = "Flash/Pulse Combination";
            this.radioButtonPackageTypeCombo.UseVisualStyleBackColor = true;
            this.radioButtonPackageTypeCombo.CheckedChanged += new System.EventHandler(this.RadioButtonPackageTypeComboCheckedChanged);
            // 
            // radioButtonPackageTypeFlash
            // 
            this.radioButtonPackageTypeFlash.AutoSize = true;
            this.radioButtonPackageTypeFlash.Checked = true;
            this.radioButtonPackageTypeFlash.Location = new System.Drawing.Point(19, 28);
            this.radioButtonPackageTypeFlash.Name = "radioButtonPackageTypeFlash";
            this.radioButtonPackageTypeFlash.Size = new System.Drawing.Size(50, 17);
            this.radioButtonPackageTypeFlash.TabIndex = 1;
            this.radioButtonPackageTypeFlash.TabStop = true;
            this.radioButtonPackageTypeFlash.Text = "Flash";
            this.radioButtonPackageTypeFlash.UseVisualStyleBackColor = true;
            // 
            // radioButtonPackageTypePulse
            // 
            this.radioButtonPackageTypePulse.AutoSize = true;
            this.radioButtonPackageTypePulse.Location = new System.Drawing.Point(19, 51);
            this.radioButtonPackageTypePulse.Name = "radioButtonPackageTypePulse";
            this.radioButtonPackageTypePulse.Size = new System.Drawing.Size(51, 17);
            this.radioButtonPackageTypePulse.TabIndex = 0;
            this.radioButtonPackageTypePulse.Text = "Pulse";
            this.radioButtonPackageTypePulse.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.checkBoxLxClassCloudToGround);
            this.groupBox2.Controls.Add(this.checkBoxLxClassInCloud);
            this.groupBox2.Location = new System.Drawing.Point(12, 73);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(143, 87);
            this.groupBox2.TabIndex = 55;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Lightning Classification";
            // 
            // checkBoxLxClassCloudToGround
            // 
            this.checkBoxLxClassCloudToGround.AutoSize = true;
            this.checkBoxLxClassCloudToGround.Checked = true;
            this.checkBoxLxClassCloudToGround.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxLxClassCloudToGround.Location = new System.Drawing.Point(23, 29);
            this.checkBoxLxClassCloudToGround.Name = "checkBoxLxClassCloudToGround";
            this.checkBoxLxClassCloudToGround.Size = new System.Drawing.Size(101, 17);
            this.checkBoxLxClassCloudToGround.TabIndex = 1;
            this.checkBoxLxClassCloudToGround.Text = "Cloud to ground";
            this.checkBoxLxClassCloudToGround.UseVisualStyleBackColor = true;
            // 
            // checkBoxLxClassInCloud
            // 
            this.checkBoxLxClassInCloud.AutoSize = true;
            this.checkBoxLxClassInCloud.Location = new System.Drawing.Point(23, 52);
            this.checkBoxLxClassInCloud.Name = "checkBoxLxClassInCloud";
            this.checkBoxLxClassInCloud.Size = new System.Drawing.Size(64, 17);
            this.checkBoxLxClassInCloud.TabIndex = 0;
            this.checkBoxLxClassInCloud.Text = "In-cloud";
            this.checkBoxLxClassInCloud.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButtonFormatBinary);
            this.groupBox1.Controls.Add(this.radioButtonFormatAscii);
            this.groupBox1.Location = new System.Drawing.Point(161, 73);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(169, 100);
            this.groupBox1.TabIndex = 54;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Response Package Format";
            // 
            // radioButtonFormatBinary
            // 
            this.radioButtonFormatBinary.AutoSize = true;
            this.radioButtonFormatBinary.Location = new System.Drawing.Point(17, 51);
            this.radioButtonFormatBinary.Name = "radioButtonFormatBinary";
            this.radioButtonFormatBinary.Size = new System.Drawing.Size(54, 17);
            this.radioButtonFormatBinary.TabIndex = 1;
            this.radioButtonFormatBinary.Text = "Binary";
            this.radioButtonFormatBinary.UseVisualStyleBackColor = true;
            // 
            // radioButtonFormatAscii
            // 
            this.radioButtonFormatAscii.AutoSize = true;
            this.radioButtonFormatAscii.Checked = true;
            this.radioButtonFormatAscii.Location = new System.Drawing.Point(17, 27);
            this.radioButtonFormatAscii.Name = "radioButtonFormatAscii";
            this.radioButtonFormatAscii.Size = new System.Drawing.Size(47, 17);
            this.radioButtonFormatAscii.TabIndex = 0;
            this.radioButtonFormatAscii.TabStop = true;
            this.radioButtonFormatAscii.Text = "Ascii";
            this.radioButtonFormatAscii.UseVisualStyleBackColor = true;
            // 
            // textBoxPartnerId
            // 
            this.textBoxPartnerId.Location = new System.Drawing.Point(96, 35);
            this.textBoxPartnerId.Name = "textBoxPartnerId";
            this.textBoxPartnerId.Size = new System.Drawing.Size(244, 20);
            this.textBoxPartnerId.TabIndex = 53;
            // 
            // textBoxIpAddress
            // 
            this.textBoxIpAddress.Location = new System.Drawing.Point(96, 13);
            this.textBoxIpAddress.Name = "textBoxIpAddress";
            this.textBoxIpAddress.Size = new System.Drawing.Size(245, 20);
            this.textBoxIpAddress.TabIndex = 52;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(359, 39);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(45, 13);
            this.label9.TabIndex = 51;
            this.label9.Text = "Version:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(33, 35);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 13);
            this.label6.TabIndex = 49;
            this.label6.Text = "Partner Id:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(375, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 44;
            this.label2.Text = "Port:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 43;
            this.label1.Text = "IpAddress:";
            // 
            // textBoxPort
            // 
            this.textBoxPort.Location = new System.Drawing.Point(404, 13);
            this.textBoxPort.Name = "textBoxPort";
            this.textBoxPort.Size = new System.Drawing.Size(53, 20);
            this.textBoxPort.TabIndex = 45;
            // 
            // btDisconnect
            // 
            this.btDisconnect.Location = new System.Drawing.Point(561, 35);
            this.btDisconnect.Name = "btDisconnect";
            this.btDisconnect.Size = new System.Drawing.Size(81, 23);
            this.btDisconnect.TabIndex = 46;
            this.btDisconnect.Text = "Disconnect";
            this.btDisconnect.UseVisualStyleBackColor = true;
            this.btDisconnect.Click += new System.EventHandler(this.BtDisconnectClick);
            // 
            // btConnect
            // 
            this.btConnect.Location = new System.Drawing.Point(473, 35);
            this.btConnect.Name = "btConnect";
            this.btConnect.Size = new System.Drawing.Size(82, 23);
            this.btConnect.TabIndex = 42;
            this.btConnect.Text = "Connect";
            this.btConnect.UseVisualStyleBackColor = true;
            this.btConnect.Click += new System.EventHandler(this.BtConnectClick);
            // 
            // timerForm
            // 
            this.timerForm.Tick += new System.EventHandler(this.TimerFormTick);
            // 
            // textBoxResults
            // 
            this.textBoxResults.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxResults.Location = new System.Drawing.Point(13, 306);
            this.textBoxResults.Multiline = true;
            this.textBoxResults.Name = "textBoxResults";
            this.textBoxResults.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxResults.Size = new System.Drawing.Size(777, 517);
            this.textBoxResults.TabIndex = 67;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.radioTransportTcpSsl);
            this.groupBox5.Controls.Add(this.radioTransportTCP);
            this.groupBox5.Location = new System.Drawing.Point(672, 73);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(106, 87);
            this.groupBox5.TabIndex = 55;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Transport Mode";
            // 
            // radioTransportTcpSsl
            // 
            this.radioTransportTcpSsl.AutoSize = true;
            this.radioTransportTcpSsl.Location = new System.Drawing.Point(17, 51);
            this.radioTransportTcpSsl.Name = "radioTransportTcpSsl";
            this.radioTransportTcpSsl.Size = new System.Drawing.Size(71, 17);
            this.radioTransportTcpSsl.TabIndex = 1;
            this.radioTransportTcpSsl.TabStop = true;
            this.radioTransportTcpSsl.Text = "TCP/SSL";
            this.radioTransportTcpSsl.UseVisualStyleBackColor = true;
            // 
            // radioTransportTCP
            // 
            this.radioTransportTCP.AutoSize = true;
            this.radioTransportTCP.Checked = true;
            this.radioTransportTCP.Location = new System.Drawing.Point(17, 28);
            this.radioTransportTCP.Name = "radioTransportTCP";
            this.radioTransportTCP.Size = new System.Drawing.Size(46, 17);
            this.radioTransportTCP.TabIndex = 0;
            this.radioTransportTCP.TabStop = true;
            this.radioTransportTCP.Text = "TCP";
            this.radioTransportTCP.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.checkBoxDataLogDb);
            this.groupBox6.Controls.Add(this.checkBoxDataLogFile);
            this.groupBox6.Location = new System.Drawing.Point(648, 11);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(130, 47);
            this.groupBox6.TabIndex = 68;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Data Logging";
            // 
            // checkBoxDataLogDb
            // 
            this.checkBoxDataLogDb.AutoSize = true;
            this.checkBoxDataLogDb.Location = new System.Drawing.Point(65, 23);
            this.checkBoxDataLogDb.Name = "checkBoxDataLogDb";
            this.checkBoxDataLogDb.Size = new System.Drawing.Size(41, 17);
            this.checkBoxDataLogDb.TabIndex = 1;
            this.checkBoxDataLogDb.Text = "DB";
            this.checkBoxDataLogDb.UseVisualStyleBackColor = true;
            // 
            // checkBoxDataLogFile
            // 
            this.checkBoxDataLogFile.AutoSize = true;
            this.checkBoxDataLogFile.Location = new System.Drawing.Point(17, 23);
            this.checkBoxDataLogFile.Name = "checkBoxDataLogFile";
            this.checkBoxDataLogFile.Size = new System.Drawing.Size(42, 17);
            this.checkBoxDataLogFile.TabIndex = 0;
            this.checkBoxDataLogFile.Text = "File";
            this.checkBoxDataLogFile.UseVisualStyleBackColor = true;
            // 
            // comboBoxVersion
            // 
            this.comboBoxVersion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxVersion.FormattingEnabled = true;
            this.comboBoxVersion.Items.AddRange(new object[] {
            "1",
            "2",
            "3"});
            this.comboBoxVersion.Location = new System.Drawing.Point(404, 36);
            this.comboBoxVersion.Name = "comboBoxVersion";
            this.comboBoxVersion.Size = new System.Drawing.Size(53, 21);
            this.comboBoxVersion.TabIndex = 69;
            this.comboBoxVersion.SelectedIndexChanged += new System.EventHandler(this.comboBoxVersion_SelectedIndexChanged);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.radioButtonDisplayDetailed);
            this.groupBox7.Controls.Add(this.radioButtonDisplaySummary);
            this.groupBox7.Location = new System.Drawing.Point(13, 259);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(212, 44);
            this.groupBox7.TabIndex = 70;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Display";
            // 
            // radioButtonDisplayDetailed
            // 
            this.radioButtonDisplayDetailed.AutoSize = true;
            this.radioButtonDisplayDetailed.Location = new System.Drawing.Point(91, 17);
            this.radioButtonDisplayDetailed.Name = "radioButtonDisplayDetailed";
            this.radioButtonDisplayDetailed.Size = new System.Drawing.Size(64, 17);
            this.radioButtonDisplayDetailed.TabIndex = 1;
            this.radioButtonDisplayDetailed.Text = "Detailed";
            this.radioButtonDisplayDetailed.UseVisualStyleBackColor = true;
            // 
            // radioButtonDisplaySummary
            // 
            this.radioButtonDisplaySummary.AutoSize = true;
            this.radioButtonDisplaySummary.Checked = true;
            this.radioButtonDisplaySummary.Location = new System.Drawing.Point(6, 17);
            this.radioButtonDisplaySummary.Name = "radioButtonDisplaySummary";
            this.radioButtonDisplaySummary.Size = new System.Drawing.Size(68, 17);
            this.radioButtonDisplaySummary.TabIndex = 0;
            this.radioButtonDisplaySummary.TabStop = true;
            this.radioButtonDisplaySummary.Text = "Summary";
            this.radioButtonDisplaySummary.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(802, 835);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.comboBoxVersion);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.textBoxResults);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.labelNote);
            this.Controls.Add(this.textBoxBoundingArea);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.textBoxNumPacketsReceived);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.buttonGenerateConnStr);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.textBoxConnStr);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.textBoxPartnerId);
            this.Controls.Add(this.textBoxIpAddress);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxPort);
            this.Controls.Add(this.btDisconnect);
            this.Controls.Add(this.btConnect);
            this.Name = "MainForm";
            this.Text = "LX DF Client";
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckBox checkBoxMetadata;
        private System.Windows.Forms.Label labelNote;
        private System.Windows.Forms.TextBox textBoxBoundingArea;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxNumPacketsReceived;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonGenerateConnStr;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxConnStr;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton radioButtonPackageTypeCombo;
        private System.Windows.Forms.RadioButton radioButtonPackageTypeFlash;
        private System.Windows.Forms.RadioButton radioButtonPackageTypePulse;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox checkBoxLxClassCloudToGround;
        private System.Windows.Forms.CheckBox checkBoxLxClassInCloud;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButtonFormatBinary;
        private System.Windows.Forms.RadioButton radioButtonFormatAscii;
        private System.Windows.Forms.TextBox textBoxPartnerId;
        private System.Windows.Forms.TextBox textBoxIpAddress;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxPort;
        private System.Windows.Forms.Button btDisconnect;
        private System.Windows.Forms.Button btConnect;
        private System.Windows.Forms.Timer timerForm;
        private System.Windows.Forms.TextBox textBoxResults;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton radioTransportTcpSsl;
        private System.Windows.Forms.RadioButton radioTransportTCP;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.CheckBox checkBoxDataLogFile;
        private System.Windows.Forms.CheckBox checkBoxDataLogDb;
        private System.Windows.Forms.ComboBox comboBoxVersion;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.RadioButton radioButtonDisplayDetailed;
        private System.Windows.Forms.RadioButton radioButtonDisplaySummary;
    }
}

