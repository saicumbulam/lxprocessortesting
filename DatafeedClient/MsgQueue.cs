﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EarthNetworks.Lx.DataFeed.ClientApp
{
    internal class MsgQueue
    {
        private static Queue<string> _list = new Queue<string>();
        private static readonly object LockList = new object();

        public static string GetNextMsg()
        {
            string msg = null;

            lock (LockList)
            {
                if (_list.Count > 0)
                {
                    msg = _list.Dequeue();
                }
            }

            return msg;
        }

        public static void AddMsg(string msg)
        {
            lock (LockList)
            {
                _list.Enqueue(msg);
            }    
        }

        /*
        private static Queue<FlashObj> _queueLgtDataRaw = new Queue<FlashObj>();
        private static Queue<FlashObj> _queueLgtDataDB = new Queue<FlashObj>();      
        private static object _lockLgtDataRaw = new object();
        private static object _lockLgtDataDB = new object();

        public static FlashObj GeLgtDataRaw()
        {
            lock (_lockLgtDataRaw)
            {
                if (_queueLgtDataRaw.Count > 0)
                {
                    return (FlashObj)_queueLgtDataRaw.Dequeue();
                }
            }

            return null;
        }

        public static int GetLgtMsgQNum()
        {
            lock (_lockLgtDataRaw)
            {
               
                return _queueLgtDataRaw.Count;
            }
        }
    
        public static void AddLgtDataRaw(FlashObj data)
        {
            lock (_lockLgtDataRaw)
            {
                _queueLgtDataRaw.Enqueue(data);
            }
        }

        public static FlashObj GeLgtDataDB()
        {
            lock (_lockLgtDataDB)
            {
                if (_queueLgtDataDB.Count > 0)
                {
                    return (FlashObj)_queueLgtDataDB.Dequeue();
                }
            }

            return null;
        }

        public static int GetLgtDataDbNum()
        {
            lock (_lockLgtDataDB)
            {
              return _queueLgtDataDB.Count;
            }
        }
        public static void AddLgtDataDB(FlashObj data)
        {
            lock (_lockLgtDataDB)
            {
                _queueLgtDataDB.Enqueue((FlashObj)data);
            }
        }
        */
    }
}
