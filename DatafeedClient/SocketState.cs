using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace EarthNetworks.Lx.DataFeed.ClientApp
{
    internal class SocketState
    {
        private string _id;
        private int _bufferSize;
        private int _bytesRead;
        private byte[] _buffer;
        private byte[] _cacheBuffer;
        private Socket _socket;
        private StringBuilder _receivedMsg;
        public int _cacheLen = 0;
        public EndPoint RemoteEndPoint = null;
        public SocketError SocketErr;
        public SocketState(Socket socket, int bufferSize)
            : this(socket, bufferSize, null)
        {}

        public SocketState()
            : this(null, 0, null)
        { }

        public SocketState(Socket socket, int bufferSize, string id)
        {
            _socket = socket;
            _bufferSize = bufferSize;
            _id = id;
            _buffer = new byte[_bufferSize];
            _cacheBuffer = new byte[_bufferSize];
            _bytesRead = 0;
            _receivedMsg = new StringBuilder();
            _cacheLen = 0;

            if (null != _socket && SocketType.Stream == _socket.SocketType)
                RemoteEndPoint = _socket.RemoteEndPoint;
            else
                RemoteEndPoint = new IPEndPoint(IPAddress.Any, 0);
        }

        #region Properties
        public byte[] Buffer
        {
            get { return _buffer; }
            set { _buffer = value; }
        }
        public int CacheLen
        {
            //_cacheLen
            get { return _cacheLen; }
            set { _cacheLen = value; }      

        }
        public byte[] CacheBuffer
        {
            get { return _cacheBuffer; }
            set { _cacheBuffer = value; }
        }
        public int BufferSize
        {
	        get { return _bufferSize;}
	        set { _bufferSize = value;}
        }
        public int BytesRead
        {
            get { return _bytesRead; }
            set { _bytesRead = value; }
        }
        public string Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public string RemoteIPAddress
        {
            get { return ((IPEndPoint)RemoteEndPoint).Address.ToString(); }
        }
        public StringBuilder ReceivedMsg
        {
            get { return _receivedMsg; }
            set { _receivedMsg = value; }
        }
        public Socket Socket
        {
            get { return _socket; }
            set { _socket = value; }
        }
    
        #endregion
    }
}
