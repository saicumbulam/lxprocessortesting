﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using Aws.Core.Utilities;

using LxDatafeed.Data.Partner;
using LxDatafeed.Data.ResponsePackage;
using LxDatafeed.Net;
using Newtonsoft.Json.Linq;

namespace EarthNetworks.Lx.DataFeed.ClientApp
{
    internal class Client
    {
        private int _packetLen;

        private TcpClient _tcpClient;
        private Stream _genericStream;
        private SocketState _socketState;
        private IPEndPoint _endPoint;
        private IPAddress _ipAddress;
        private int _port;
        private string _connectionString;
        private ConnectionStringParser.Data _connectionStringData;
        private PartnerData _partnerData;
        private bool _isSsl;
        private byte[] _unUsedData;
        private string _dfClientId;
        private string _dbConnStr;

        private DateTime _connectionTime;
        private bool _saveToFile;
        private bool _saveToDb;
        private bool _isDetailed;

        public void DoDisconnect()
        {
            if (_tcpClient != null)
            {
                CleanupSocket(_tcpClient.Client);
                _socketState = null;
                _tcpClient = null;
            }
            //if (!_isDetailed)
            //{
                _mainForm.WritePacketToUi("Disconnected");
            //}
            AppLogger.Log("Disconnected.");
        }

        public void DoConnect(string ip, int port, string connectionString, ConnectionStringParser.Data connectionStringData, bool isSsl, string dfClientId, string dbConnStr)
        {
            if (_tcpClient != null)
            {
                CleanupSocket(_tcpClient.Client);
                _socketState = null;
                _tcpClient = null;
            }

            _unUsedData = null;
            
            IPHostEntry host = Dns.GetHostEntry(ip);
            _ipAddress = null;
            _endPoint = null;

            foreach (IPAddress ipAddress in host.AddressList)
            {
                if (ipAddress.AddressFamily == AddressFamily.InterNetwork)
                {
                    _ipAddress = ipAddress;
                    break;
                }
            }
            
            if (_ipAddress != null)
            {
                _endPoint = new IPEndPoint(_ipAddress, port);
            }

            _port = port;
            _connectionString = connectionString;
            _connectionStringData = connectionStringData;
            _isSsl = isSsl;
            _partnerData = new PartnerData
            {
                PartnerId = connectionStringData.PartnerId,
                ResponseFormatType = connectionStringData.ResponseFormatType,
                LxClassificationType = connectionStringData.LxClassificationType,
                ResponsePackageType = connectionStringData.ResponsePackageType,
                ResponsePackageVersion = connectionStringData.Version,
                Metadata = connectionStringData.Metadata
            };
            _dfClientId = dfClientId;
            _dbConnStr = dbConnStr;
            Connecting();
        }

        private void Connecting()
        {
            try
            {
                ResponsePackageFormatter.IFormatter formatter = ResponsePackageFormatter.GetFormatter(_partnerData);
                _packetLen = formatter.PacketLen;

                byte[] buf = Encoding.ASCII.GetBytes(_connectionString);

                if (_tcpClient != null && _tcpClient.Client != null)
                {
                    try
                    {
                        _genericStream.Close();
                        _tcpClient.Client.Close();
                        _tcpClient.Close();
                    }
                    catch (Exception inex)
                    {
                        AppLogger.LogError("Error in close the client socket.", inex);
                    }
                    finally
                    {
                        _tcpClient = null;
                    }
                }

                AppLogger.Log("Connecting to LX Datafeed.[server=" + _endPoint + ":" + _port + "; connectionString=" + _connectionString + "]");
                _tcpClient = new TcpClient();
                if (_isSsl)
                {
                    _tcpClient = new TcpClient();
                    while (!_tcpClient.Connected)
                    {
                        try
                        {
                            if (!_isDetailed)
                            {
                                _mainForm.WritePacketToUi(DateTime.UtcNow.ToString("O") + " Connecting...");
                            }
                            _tcpClient.Connect(_endPoint);
                        }
                        catch (Exception ex)
                        {
                            // retry in 5 sec
                            Thread.Sleep(5000);
                        }
                    }

                    var sslStream = new SslStream(_tcpClient.GetStream(), false, ValidateCert, null);
                    sslStream.AuthenticateAsClient(_ipAddress.ToString(), null, SslProtocols.Tls12, false);
                    _genericStream = sslStream;
                    _genericStream.Write(buf, 0, buf.Length);
                    _genericStream.Flush();
                }
                else
                {
                    _tcpClient = new TcpClient();
                    while (!_tcpClient.Connected)
                    {
                        try
                        {
                            if (!_isDetailed)
                            {
                                _mainForm.WritePacketToUi(DateTime.UtcNow.ToString("O") + " Connecting...");
                            }
                            _tcpClient.Connect(_endPoint);
                        }
                        catch (Exception ex)
                        {
                            // retry in 5 sec
                            Thread.Sleep(5000);
                        }
                    }
                    
                    _genericStream = _tcpClient.GetStream();
                    _genericStream.Write(buf, 0, buf.Length);
                    _genericStream.Flush();
                }
                if (!_isDetailed)
                {
                    _mainForm.WritePacketToUi(DateTime.UtcNow.ToString("O") + " Connected");
                }
                _socketState = new SocketState(_tcpClient.Client, _packetLen);
                if (_connectionStringData.ResponseFormatType == ResponseFormatType.Ascii)
                {
                    _genericStream.BeginRead(_socketState.Buffer, 0, _socketState.Buffer.Length,
                                                    RespCallback, _socketState);
                }
                else
                {
                    _genericStream.BeginRead(_socketState.Buffer, 0, _socketState.Buffer.Length,
                                                    RespCallback, _socketState);
                }
            }
            catch (Exception e)
            {
                AppLogger.LogError("Error connecting.", e);
                _tcpClient = null;
            }
        }

        private static bool ValidateCert(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        private void CleanupSocket(Socket socket)
        {
            try
            {
                _genericStream.Close();
                if (null != socket && socket.Connected)
                {
                    socket.Shutdown(SocketShutdown.Both);
                    socket.Close();
                    AppLogger.Log("socket closed.");
                }
            }
            catch (Exception ex)
            {
                EventManager.LogWarning("Failure cleaning up socket.", ex);
            }

        }

        private void RespCallback(IAsyncResult result)
        {
            try
            {
                SocketState request = (SocketState)result.AsyncState;
                int count = _genericStream.EndRead(result);

                byte[] data = new byte[count + (_unUsedData == null ? 0 : _unUsedData.Length)];
                if (_unUsedData != null)
                {
                    Array.Copy(_unUsedData, 0, data, 0, _unUsedData.Length);
                    Array.Copy(request.Buffer, 0, data, _unUsedData.Length, count);
                    _unUsedData = null;
                }
                else
                {
                    Array.Copy(request.Buffer, 0, data, 0, count);
                }
                SetText(data);

                if (_tcpClient != null)
                {
                    try
                    {
                        _genericStream.BeginRead(_socketState.Buffer, 0, _socketState.Buffer.Length,
                            RespCallback, _socketState);
                    }
                    catch (IOException ioexx)
                    {
                        AppLogger.LogError("Failure in BeginRead for RespCallback.", ioexx);
                        if (!_isDetailed)
                        {
                            _mainForm.WritePacketToUi(DateTime.UtcNow.ToString("O") + " Reconnecting...");
                        }
                        try
                        {
                            CleanupSocket(_tcpClient.Client);
                            DoConnect(_ipAddress.ToString(), _port, _connectionString, _connectionStringData, _isSsl, _dfClientId, _dbConnStr);
                        }
                        catch (Exception ex)
                        {
                            _tcpClient = null;
                        }
                    }
                    catch (Exception exx)
                    {
                        AppLogger.LogError("Failure in BeginRead for RespCallback.", exx);
                        _tcpClient = null;
                    }
                }
            }
            catch (IOException ioException)
            {
                AppLogger.LogError("Failure in BeginRead for RespCallback.", ioException);
                try
                {
                    if (_tcpClient != null)
                    {
                        _mainForm.WritePacketToUi("Reconnecting...");
                        CleanupSocket(_tcpClient.Client);
                        DoConnect(_ipAddress.ToString(), _port, _connectionString, _connectionStringData, _isSsl, _dfClientId, _dbConnStr);
                    }
                }
                catch (Exception ex)
                {
                    _tcpClient = null;
                }
            }
            catch (Exception exception)
            {
                try
                {
                    CleanupSocket(_tcpClient.Client);

                }
                catch (Exception)
                {
                    _tcpClient = null;
                }

                if (_tcpClient != null)
                {
                    AppLogger.LogError("Failure in BeginRead for RespCallback.", exception);
                    try
                    {
                        _genericStream.BeginRead(_socketState.Buffer, 0, _socketState.Buffer.Length,
                                                        RespCallback, _socketState);
                    }
                    catch (Exception exx)
                    {
                        _tcpClient = null;
                        AppLogger.LogError("Failure in BeginRead for RespCallback.", exx);
                    }
                }
            }
        }

        private const int WmoHeaderSize = 32;
        private DateTime fileDateTime = DateTime.UtcNow;
        private string fileName = string.Empty;
        private FileStream fs = null;
        private MainForm _mainForm;

        public Client(MainForm mainForm)
        {
            _mainForm = mainForm;
        }

        private void SetText(byte[] data)
        {
            int bufferStartPosForNextRead = 0;
            if (data.Length > 0)
            {
                if (_connectionStringData.ResponseFormatType == ResponseFormatType.Ascii)
                {
                    if (_connectionStringData.Version == 3)
                    {
                        List<string> body = DecodeTextV3(data);
                        foreach (var line in body)
                        {
                            string latencyText = GetLatency(line, ResponseFormatType.Ascii, _connectionStringData.Version);
                            if (_isDetailed)
                            {
                                _mainForm.WritePacketToUi(line + latencyText);
                            }
                            //if (AppConfig.IsWritingPacketsToLogFile)
                            if (_saveToFile)
                            {
                                WriteToFile(line);
                            }
                            WriteToDb(DateTime.UtcNow, line, "ASCII");
                        }
                    }
                    else
                    {
                        _unUsedData = data;
                        byte[] msg = null;
                        do
                        {
                            msg = DecodeTextV2(_unUsedData);
                            if (msg != null)
                            {
                                string tempMsg = ResponsePackageFormatter.ToString(msg, _partnerData);
                                tempMsg += GetLatency(tempMsg, ResponseFormatType.Ascii, _connectionStringData.Version);
                                if (_isDetailed)
                                {
                                    _mainForm.WritePacketToUi(tempMsg);
                                }
                                if (_saveToFile)
                                {
                                    WriteToFile(tempMsg);
                                }
                                WriteToDb(DateTime.UtcNow, tempMsg, "ASCII");
                            }
                        } while (msg != null && _unUsedData != null);
                    }
                }
                if (_connectionStringData.ResponseFormatType == ResponseFormatType.Binary)
                {
                    int packageStartPos = 0;
                    int startingPosExtraBytes = (_partnerData.ResponsePackageType == ResponsePackageType.FlashAndPulseCombo ? 1 : 0);
                    do
                    {
                        ushort? packageByteSize = null;
                        if (_partnerData.ResponsePackageType == ResponsePackageType.FlashAndPulseCombo)
                        {
                            packageByteSize = ResponsePackageFormatter.IntegerConverter.ReadUShortFromBufferAtOffset(data, packageStartPos, true);
                        }
                        else
                        {
                            packageByteSize = data[packageStartPos];
                        }

                        if (packageByteSize.HasValue)
                        {
                            if ((packageStartPos + packageByteSize.Value) <= data.Length)
                            {
                                byte[] temp = new byte[packageByteSize.Value];
                                Array.Copy(data, packageStartPos, temp, 0, packageByteSize.Value);

                                CreateWmoFile(temp);
                                
                                string tempMsg = ResponsePackageFormatter.ToString(temp, _partnerData);

                                tempMsg += GetLatency(tempMsg, ResponseFormatType.Binary, _connectionStringData.Version);
                                if (_isDetailed)
                                {
                                    _mainForm.WritePacketToUi(tempMsg);
                                }
                                //MsgQueue.AddMsg(tempMsg);
                                if (_saveToFile)
                                {
                                    WriteToFile(tempMsg);
                                }
                                WriteToDb(DateTime.UtcNow, tempMsg, "Binary");
                                packageStartPos += packageByteSize.Value;
                                if ((packageStartPos + startingPosExtraBytes) == data.Length)
                                {
                                    MoveBytesToBeginningOfBuffer(data, data.Length, packageStartPos);
                                    break;
                                }
                            }
                            else
                            {   // We have already started reading the next package so setup the buffer to read the rest of the msg
                                MoveBytesToBeginningOfBuffer(data, data.Length, packageStartPos);
                                break;
                            }
                        }
                    } while ((packageStartPos + startingPosExtraBytes) < data.Length);
                }
            }
        }

        private string GetLatency(string body, ResponseFormatType format, short version)
        {
            string result = string.Empty;

            if (version == 3)
            {
                if (format == ResponseFormatType.Ascii)
                {
                    JObject jo = JObject.Parse(body);
                    string str = (string)jo.SelectToken("time");

                    DateTime dt = Convert.ToDateTime(str).ToUniversalTime();
                    TimeSpan ts = DateTime.UtcNow - dt;

                    result = ts.TotalSeconds.ToString();
                }
                else if (format == ResponseFormatType.Binary)
                {
                    Regex regex = new Regex(@"time=(\S+);");
                    Match match = regex.Match(body);
                    string str = match.Groups[1].Value;

                    DateTime dt = Convert.ToDateTime(str); // UTC
                    TimeSpan ts = DateTime.UtcNow - dt;

                    result = ts.TotalSeconds.ToString();
                }
                result = " Latency = " + result;
            }

            return result;
        }

        private List<string> DecodeTextV3(byte[] data)
        {
            List<string> body = new List<string>();
            int offset = 0;
            bool okay;
            do
            {
                okay = false;
                int? size = ResponsePackageFormatter.IntegerConverter.ReadIntFromBufferAtOffset(data, offset, true);
                if (size.HasValue)
                {
                    if (data.Length >= offset + size.Value)
                    {
                        string text = Encoding.UTF8.GetString(data, offset + 4, size.Value - 4);
                        body.Add(text);
                        offset += size.Value;
                        okay = true;
                    }
                }
            } while (okay);

            // unused
            _unUsedData = new byte[data.Length - offset];
            Array.Copy(data, offset, _unUsedData, 0, data.Length - offset);

            return body;
        }

        private byte[] DecodeTextV2(byte[] data)
        {
            int bufferStartPosForNextRead = 0;
            byte[] msg = null;
            for (int i = 0; i < data.Length; i++)
            {   // Loop through looking for the carriage return/line feed characters that indicate the end of the package
                if (data[i] == 13 && data[i + 1] == 10)
                {   // Copy this package to a new buffer to isolate it
                    int origMsgLen = (i + 1) + 1;
                    msg = new byte[origMsgLen];
                    Array.Copy(data, 0, msg, 0, origMsgLen);

                    // Now check the remaining bytes for data
                    for (int j = origMsgLen; j < data.Length; j++)
                    {
                        if (data[j] != 0)
                        {   // We found some data so this is the starting point of the next package
                            bufferStartPosForNextRead = j;
                            break;
                        }
                    }

                    if (bufferStartPosForNextRead > 0)
                    {   // We have already started reading the next package so setup the buffer to read the rest of the msg
                        MoveBytesToBeginningOfBuffer(data, data.Length, bufferStartPosForNextRead);
                    }
                    else
                    {
                        _unUsedData = null;
                    }
                    break;
                }
            }

            return msg;
        }
        
        private void CreateWmoFile(byte[] temp)
        {
            if (ConfigurationManager.AppSettings["CreateWMOFile"] == "true")
            {
                #region WMO

                //creating 1 minute WMO header file
                if (string.IsNullOrEmpty(fileName))
                {
                    //first time
                    fileDateTime = DateTime.UtcNow;
                    fileName = string.Format(ConfigurationManager.AppSettings["WMOFileLocation"],
                        fileDateTime.ToString("yyyyMMddHHmm"), "body");
                    if (!Directory.Exists(Path.GetDirectoryName(fileName)))
                    {
                        Directory.CreateDirectory(Path.GetDirectoryName(fileName));
                    }
                    fs = new FileStream(fileName, FileMode.Create);
                }
                else if (DateTime.UtcNow.Subtract(fileDateTime).TotalSeconds > 60)
                {
                    //create the  WMO files
                    fs.Close();
                    byte[] minData = File.ReadAllBytes(fileName);
                    byte[] minDataHeader = new byte[minData.Length + 40];
                    string headerStr = String.Format("****{0}**** SFPA42 KWBC {1}   ",
                        (minData.Length + 21).ToString().PadLeft(10, '0'),
                        fileDateTime.ToString("ddHHmm"));
                    byte[] headerBytes = Encoding.ASCII.GetBytes(headerStr);
                    headerBytes[18] = 10;
                    headerBytes[37] = 13;
                    headerBytes[38] = 13;
                    headerBytes[39] = 10;

                    Array.Copy(headerBytes, minDataHeader, 40);
                    Array.Copy(minData, 0, minDataHeader, 40, minData.Length);
                    string wmoFileName =
                        string.Format(ConfigurationManager.AppSettings["WMOFileLocation"],
                            fileDateTime.ToString("yyyyMMddHHmm"), "unencrypted");
                    File.WriteAllBytes(wmoFileName, minDataHeader);

                    StringBuilder sb = new StringBuilder();
                    foreach (byte value in minDataHeader)
                    {
                        sb.AppendFormat("{0:x2} ", value);
                    }
                    File.WriteAllText(wmoFileName + ".txt", sb.ToString());
                    //encryption
                    byte[] encrypted;
                    using (AesManaged aManaged = new AesManaged())
                    {
                        aManaged.BlockSize = 128;
                        aManaged.KeySize = 128;
                        aManaged.Key = UTF8Encoding.UTF8.GetBytes(
                            File.ReadAllText(ConfigurationManager.AppSettings["EncryptionKey"]));
                        aManaged.IV = new byte[]
                        {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
                        using (var memoryStream = new MemoryStream())
                        {
                            using (var cryptoStream = new CryptoStream(
                                memoryStream, aManaged.CreateEncryptor(), CryptoStreamMode.Write))
                            {
                                cryptoStream.Write(minData, 0, minData.Length);
                                cryptoStream.FlushFinalBlock();
                                encrypted = memoryStream.ToArray();
                            }
                        }
                    }
                    minDataHeader = new byte[encrypted.Length + 40];
                    Array.Copy(headerBytes, minDataHeader, 40);
                    Array.Copy(encrypted, 0, minDataHeader, 40, encrypted.Length);
                    wmoFileName =
                        string.Format(ConfigurationManager.AppSettings["WMOFileLocation"],
                            fileDateTime.ToString("yyyyMMddHHmm"), "encrypted");
                    File.WriteAllBytes(wmoFileName, minDataHeader);

                    sb = new StringBuilder();
                    foreach (byte value in minDataHeader)
                    {
                        sb.AppendFormat("{0:x2} ", value);
                    }
                    File.WriteAllText(wmoFileName + ".txt", sb.ToString());

                    //create the new body file
                    fileDateTime = DateTime.UtcNow;
                    fileName = string.Format(ConfigurationManager.AppSettings["WMOFileLocation"],
                        fileDateTime.ToString("yyyyMMddHHmm"), "body");
                    fs = new FileStream(fileName, FileMode.Create);
                }

                //append data
                fs.Write(temp, 0, temp.Length);

                #endregion
            }
        }

        private void WriteToDb (DateTime receiveTime, string data, string responseType)
        {
            if (!_saveToDb || string.IsNullOrEmpty(_dfClientId))
                return;

            if (responseType == "ASCII")
            {
                string[] dataSeparators;
                if (_connectionStringData.Version == 3)
                {
                    dataSeparators = new string[] { "{\"time\":\"", "\",\"type\":", ",\"latitude\":", ",\"longitude\":", ",\"peakCurrent\":", ",\"icHeight\":", ",\"numSensors\":", ",\"icMultiplicity\":", ",\"cgMultiplicity\":", "}" };
                }
                else // version 2
                {
                    dataSeparators = new string[] { "[bytes=", "[class=", "; class=", "; time=", "; lat=", "; lon=", "; amp=", "; err=", "; ht=", "; sensors=", "; mult=", "; chksum=", "]" };
                }

                string[] responseData = data.Split(dataSeparators, StringSplitOptions.None);
                string receiveTimeStr = receiveTime.ToString("yyyy-MM-ddTHH:mm:ss.FFFFFF");
                const string insertSql =
                    "insert into lightning.dbo.dfclientdatalog(df_client_id, receive_time, data, Class, Lightning_Time, Latitude, Longitude, Amplitude, Error, Height, Sensors, Multiplicity) values ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}');";
                SqlConnection conn = null;
                SqlCommand cmd = null; 
                try
                {
                    if (conn == null)
                    {
                        conn = new SqlConnection(_dbConnStr);
                    }
                    if (conn.State != ConnectionState.Open)
                    {
                        conn.Open();
                    }

                    cmd = conn.CreateCommand();
                    if (_connectionStringData.Version == 3)
                    {
                        cmd.CommandText = string.Format(insertSql, _dfClientId, receiveTimeStr, data, responseData[2], responseData[1].Substring(0, 10) + " " + responseData[1].Substring(11, 12), responseData[3], responseData[4], (int)Convert.ToDouble(responseData[5]), 0, (int)Convert.ToDouble(responseData[6]), responseData[7], Convert.ToInt32(responseData[8]) + Convert.ToInt32(responseData[9]));
                    }
                    else
                    {
                        cmd.CommandText = string.Format(insertSql, _dfClientId, receiveTimeStr, data, responseData[1], responseData[2].Substring(0, 8) + " " + responseData[2].Substring(9, 2) + ":" + responseData[2].Substring(11, 2) + ":" + responseData[2].Substring(13, 6), responseData[3], responseData[4], responseData[5], responseData[6], responseData[7], responseData[8], responseData[9]);
                    }
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    EventManager.LogInfo(ex.ToString());
                }
                finally
                {
                    cmd.Dispose();
                    conn.Close();
                }
            }
            if (responseType == "Binary")
            {
                string[] dataSeparators = new string[] { "[bytes=", "; class=", "; time=", "; lat=", "; lon=", "; amp=", "; err=", "; ht=", "; sensors=", "; mult=", "; chksum=", "]" };
                string[] responseData = data.Split(dataSeparators, StringSplitOptions.None); 
            string receiveTimeStr = receiveTime.ToString("yyyy-MM-ddTHH:mm:ss.FFFFFF");
            const string insertSql =
                    "insert into lightning.dbo.dfclientdatalog(df_client_id, receive_time, data, Bytes, Class, Lightning_Time, Latitude, Longitude, Amplitude, Error, Height, Sensors, Multiplicity) values ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}', '{12}');";

            SqlConnection conn = null;
            SqlCommand cmd = null; 
            try
            {
                if (conn == null)
                {
                    conn = new SqlConnection(_dbConnStr);
                }
                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }

                cmd = conn.CreateCommand();
                    cmd.CommandText = string.Format(insertSql, _dfClientId, receiveTimeStr, data, responseData[1], responseData[2], responseData[3].Substring(0,8) + " " + responseData[3].Substring(9,2) + ":" + responseData[3].Substring(11,2) + ":" + responseData[3].Substring(13,6), responseData[4], responseData[5], responseData[6], responseData[7], responseData[8], responseData[9], responseData[10]);
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                EventManager.LogInfo(ex.ToString());
            }
            finally
            {
                cmd.Dispose();
                conn.Close();
            }

        }

        }

        private void WriteToFile(string data)
        {
            string timeStr = _connectionTime.ToString("yyyy-MM-dd_HH-mm-ss");

            if (!Directory.Exists(AppConfig.DataLogDirectory))
            {
                Directory.CreateDirectory(AppConfig.DataLogDirectory);
            }

            string outputPath = Path.Combine(AppConfig.DataLogDirectory, string.Format("data-{0}.txt", timeStr));
            File.AppendAllText(outputPath, data + Environment.NewLine);
        }

        private void MoveBytesToBeginningOfBuffer(byte[] data, int bytesRead, int packageStartPos)
        {
            _unUsedData = new byte[bytesRead - packageStartPos];
            Array.Copy(data, packageStartPos, _unUsedData, 0, _unUsedData.Length);
        }

        public DateTime ConnectionTime
        {
            get { return _connectionTime; }
            set { _connectionTime = value; }
        }

        public bool SaveToFile
        {
            get { return _saveToFile; }
            set { _saveToFile = value; }
        }

        public bool SaveToDb
        {
            get { return _saveToDb; }
            set { _saveToDb = value; }
        }

        public bool IsDetailed
        {
            get { return _isDetailed; }
            set { _isDetailed = value; }
        }
    } 
}
