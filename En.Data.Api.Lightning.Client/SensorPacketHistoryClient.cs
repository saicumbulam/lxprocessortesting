﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

using Newtonsoft.Json;

using En.Ods.Lib.Common.Interface;
using En.Ods.Lib.Common.Utility.ServiceClient;

using En.Data.Api.Lightning.Client.Models;

namespace En.Data.Api.Lightning.Client
{
    public class SensorPacketHistoryClient : ServiceClientBase
    {
        public SensorPacketHistoryClient(string serviceBaseUrl, IAppConfig config = null, IOdsLogManager logManager = null, JsonSerializer serializer = null)
            : base(serviceBaseUrl, config, logManager, serializer)
        {
        }

        public async Task<OdsServiceResponse<List<PacketRecord>>> GetSensorPacketHistoryAsync(string id, DateTime start, DateTime end)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentException("id cannot be null or empty");
            }
            if (start == default(DateTime))
            {
                throw new ArgumentException("start");
            }
            if (end == default(DateTime))
            {
                throw new ArgumentNullException("end");
            }
            start = NormalizeDateTimeToUtc(start);
            end = NormalizeDateTimeToUtc(end);
            string url = string.Format("{0}/api/v2/sensorpackethistory?id={1}&start={2}&end={3}",
                ServiceBaseUrl,
                WebUtility.UrlEncode(id),
                WebUtility.UrlEncode(start.ToString("s")),
                WebUtility.UrlEncode(end.ToString("s"))
                );
            return new OdsServiceResponse<List<PacketRecord>>(await GetResponseAsync<List<PacketRecord>>(url).ConfigureAwait(false));
        }

        public async Task<OdsServiceResponse<List<PacketRecord>>> GetSensorPacketHistoryAsync(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentException("id cannot be null or empty");
            }
            string url = string.Format("{0}/api/v2/sensorpackethistory?id={1}",
                ServiceBaseUrl,
                WebUtility.UrlEncode(id)
                );
            return new OdsServiceResponse<List<PacketRecord>>(await GetResponseAsync<List<PacketRecord>>(url).ConfigureAwait(false));
        }

        public static DateTime NormalizeDateTimeToUtc(DateTime value)
        {
            if (value.Kind != DateTimeKind.Utc)
                value = DateTime.SpecifyKind(value, DateTimeKind.Utc);

            return value;
        }
    }
}
