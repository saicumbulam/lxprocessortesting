﻿using System;
using System.Net;
using System.Threading.Tasks;

using Newtonsoft.Json;

using En.Ods.Lib.Common.Interface;
using En.Ods.Lib.Common.Utility.ServiceClient;

using En.Data.Api.Lightning.Client.Models;

namespace En.Data.Api.Lightning.Client
{
    public class CurrentSensorStatusClient : ServiceClientBase
    {
        public CurrentSensorStatusClient(string serviceBaseUrl, IAppConfig config = null, IOdsLogManager logManager = null, JsonSerializer serializer = null)
            : base(serviceBaseUrl, config, logManager, serializer)
        {
        }

        public async Task<OdsServiceResponse<CurrentSensorStatus>> GetCurrentSensorStatusAsync(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentException("id cannot be null or empty");
            }
            string url = string.Format("{0}/api/v2/currentsensorstatus?id={1}", 
                ServiceBaseUrl,
                WebUtility.UrlEncode(id)
                );
            return new OdsServiceResponse<CurrentSensorStatus>(await GetResponseAsync<CurrentSensorStatus>(url).ConfigureAwait(false));
        }
    }
}
