﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

using Newtonsoft.Json;

using En.Ods.Lib.Common.Interface;
using En.Ods.Lib.Common.Utility.ServiceClient;

using En.Data.Api.Lightning.Client.Models;

namespace En.Data.Api.Lightning.Client
{
    public class NetworkClient : ServiceClientBase
    {
        public NetworkClient(string serviceBaseUrl, IAppConfig config = null, IOdsLogManager logManager = null, JsonSerializer serializer = null) 
            : base(serviceBaseUrl, config, logManager, serializer)
        {
        }

        public async Task<OdsServiceResponse<IEnumerable<Network>>> GetAllAsync()
        {
            string url = string.Format("{0}/api/network", ServiceBaseUrl);
            return new OdsServiceResponse<IEnumerable<Network>>(await GetResponseAsync<IEnumerable<Network>>(url).ConfigureAwait(false));
        }

        public async Task<OdsServiceResponse<Network>> AddAsync(Network network)
        {
            if (network == null)
            {
                throw new ArgumentNullException("network");
            }
            if (string.IsNullOrWhiteSpace(network.Id))
            {
                throw new ArgumentException("network.Id cannot be null or empty");
            }
            if (string.IsNullOrWhiteSpace(network.Name))
            {
                throw new ArgumentException("network.Name cannot be null or empty");
            }

            string url = string.Format("{0}/api/network", ServiceBaseUrl);

            HttpContent httpContent = new StringContent(JsonConvert.SerializeObject(network));
            httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            HttpResponseMessage message = await HttpClient.PostAsync(url, httpContent);
            return new OdsServiceResponse<Network>(await ProcessResponseMessage<Network>(message).ConfigureAwait(false));
        }

        public async Task<OdsServiceResponse<Network>> UpdateAsync(string id, Network network)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentException("id cannot be null or empty");
            }
            if (network == null)
            {
                throw new ArgumentNullException("network");
            }
            if (string.IsNullOrWhiteSpace(network.Id))
            {
                throw new ArgumentException("network.Id cannot be null or empty");
            }
            if (string.IsNullOrWhiteSpace(network.Name))
            {
                throw new ArgumentException("network.Name cannot be null or empty");
            }

            string url = string.Format("{0}/api/network/{1}", ServiceBaseUrl, WebUtility.UrlEncode(id));

            HttpContent httpContent = new StringContent(JsonConvert.SerializeObject(network));
            httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            HttpResponseMessage message = await HttpClient.PutAsync(url, httpContent);
            return new OdsServiceResponse<Network>(await ProcessResponseMessage<Network>(message).ConfigureAwait(false));
        }

        public async Task<OdsServiceResponse<object>> DeleteAsync(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentException("id cannot be null or empty");
            }
            
            string url = string.Format("{0}/api/network/{1}", ServiceBaseUrl, WebUtility.UrlEncode(id));
            HttpResponseMessage message = await HttpClient.DeleteAsync(url);
            return new OdsServiceResponse<object>(await ProcessResponseMessage<object>(message).ConfigureAwait(false));
        }

    }
}
