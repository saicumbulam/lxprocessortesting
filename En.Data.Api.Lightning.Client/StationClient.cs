﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

using Newtonsoft.Json;

using En.Ods.Lib.Common.Interface;
using En.Ods.Lib.Common.Utility.ServiceClient;

using En.Data.Api.Lightning.Client.Models;

namespace En.Data.Api.Lightning.Client
{
    public class StationClient : ServiceClientBase
    {
        public StationClient(string serviceBaseUrl, IAppConfig config = null, IOdsLogManager logManager = null, JsonSerializer serializer = null)
            : base(serviceBaseUrl, config, logManager, serializer)
        {
        }

        public async Task<OdsServiceResponse<IEnumerable<Station>>> GetAllAsync()
        {
            string url = string.Format("{0}/api/v2/stations", ServiceBaseUrl);
            return new OdsServiceResponse<IEnumerable<Station>>(await GetResponseAsync<IEnumerable<Station>>(url).ConfigureAwait(false));
        }

        public async Task<OdsServiceResponse<Station>> GetAsync(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentException("id cannot be null or empty");
            }

            string url = string.Format("{0}/api/v2/stations/{1}", ServiceBaseUrl, WebUtility.UrlEncode(id));
            return new OdsServiceResponse<Station>(await GetResponseAsync<Station>(url).ConfigureAwait(false));
        }

        public async Task<OdsServiceResponse<object>> AddToNetworkAsync(string stationId, string networkId)
        {
            if (string.IsNullOrWhiteSpace(stationId))
            {
                throw new ArgumentException("stationId cannot be null or empty");
            }
            if (string.IsNullOrWhiteSpace(networkId))
            {
                throw new ArgumentException("networkId cannot be null or empty");
            }

            string url = string.Format("{0}/api/station/AddToNetwork/{1}?networkId={2}", ServiceBaseUrl, WebUtility.UrlEncode(stationId), WebUtility.UrlEncode(networkId));
            return new OdsServiceResponse<object>(await GetResponseAsync<object>(url).ConfigureAwait(false));
        }

        public async Task<OdsServiceResponse<object>> RemoveFromNetworkAsync(string stationId, string networkId)
        {
            if (string.IsNullOrWhiteSpace(stationId))
            {
                throw new ArgumentException("stationId cannot be null or empty");
            }
            if (string.IsNullOrWhiteSpace(networkId))
            {
                throw new ArgumentException("networkId cannot be null or empty");
            }

            string url = string.Format("{0}/api/station/RemoveFromNetwork/{1}?networkId={2}", ServiceBaseUrl, WebUtility.UrlEncode(stationId), WebUtility.UrlEncode(networkId));
            return new OdsServiceResponse<object>(await GetResponseAsync<object>(url).ConfigureAwait(false));
        }

        public async Task<OdsServiceResponse<IEnumerable<Station>>> GetAllInNetworkAsync(string networkId)
        {
            if (string.IsNullOrWhiteSpace(networkId))
            {
                throw new ArgumentException("networkId cannot be null or empty");
            }

            string url = string.Format("{0}/api/station/GetAllInNetwork/{1}", ServiceBaseUrl, WebUtility.UrlEncode(networkId));
            return new OdsServiceResponse<IEnumerable<Station>>(await GetResponseAsync<IEnumerable<Station>>(url).ConfigureAwait(false));
        }

        public async Task<OdsServiceResponse<Station>> AddAsync(Station station)
        {
            if (station == null)
            {
                throw new ArgumentNullException("station");
            }

            string url = string.Format("{0}/api/v2/stations", ServiceBaseUrl);

            HttpContent httpContent = new StringContent(JsonConvert.SerializeObject(station));
            httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            HttpResponseMessage message = await HttpClient.PostAsync(url, httpContent);
            return new OdsServiceResponse<Station>(await ProcessResponseMessage<Station>(message).ConfigureAwait(false));
        }

        public async Task<OdsServiceResponse<object>> UpdateAsync(string id, Station station)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentException("id cannot be null or empty");
            }
            if (station == null)
            {
                throw new ArgumentNullException("station");
            }

            string url = string.Format("{0}/api/v2/stations/{1}", ServiceBaseUrl, id);

            HttpContent httpContent = new StringContent(JsonConvert.SerializeObject(station));
            httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            HttpResponseMessage message = await HttpClient.PutAsync(url, httpContent);
            return new OdsServiceResponse<object>(await ProcessResponseMessage<object>(message).ConfigureAwait(false));
        }

        public async Task<OdsServiceResponse<object>> DeleteAsync(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentException("id cannot be null or empty");
            }

            string url = string.Format("{0}/api/v2/stations/{1}", ServiceBaseUrl, id);
            HttpResponseMessage message = await HttpClient.DeleteAsync(url);
            return new OdsServiceResponse<object>(await ProcessResponseMessage<object>(message).ConfigureAwait(false));
        }

        

    }
}
