﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

using Newtonsoft.Json;

using En.Ods.Lib.Common.Interface;
using En.Ods.Lib.Common.Utility.ServiceClient;

using En.Data.Api.Lightning.Client.Models;

namespace En.Data.Api.Lightning.Client
{
    public class StormClient : ServiceClientBase
    {
        public StormClient(string serviceBaseUrl, IAppConfig config = null, IOdsLogManager logManager = null, JsonSerializer serializer = null)
            : base(serviceBaseUrl, config, logManager, serializer)
        {
        }

        public async Task<OdsServiceResponse<List<StormRecord>>> GetStormAsync(DateTime start, DateTime end, decimal ullat, decimal ullon, decimal lrlat, decimal lrlon)
        {
            if (start == default(DateTime))
            {
                throw new ArgumentException("start");
            }
            if (end == default(DateTime))
            {
                throw new ArgumentNullException("end");
            }
            start = NormalizeDateTimeToUtc(start);
            end = NormalizeDateTimeToUtc(end);
            string url = string.Format("{0}/api/v2/storm?start={1}&end={2}&ullat={3}&ullon={4}&lrlat={5}&lrlon={6}",
                ServiceBaseUrl,
                WebUtility.UrlEncode(start.ToString("s")),
                WebUtility.UrlEncode(end.ToString("s")),
                ullat,
                ullon,
                lrlat,
                lrlon
                );
            return new OdsServiceResponse<List<StormRecord>>(await GetResponseAsync<List<StormRecord>>(url).ConfigureAwait(false));
        }

        public static DateTime NormalizeDateTimeToUtc(DateTime value)
        {
            if (value.Kind != DateTimeKind.Utc)
                value = DateTime.SpecifyKind(value, DateTimeKind.Utc);

            return value;
        }
    }
}
