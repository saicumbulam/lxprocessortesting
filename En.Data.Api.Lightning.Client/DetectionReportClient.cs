﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Threading.Tasks;

using Newtonsoft.Json;

using En.Data.Api.Lightning.Client.Models;
using En.Ods.Lib.Common.Interface;
using En.Ods.Lib.Common.Utility.ServiceClient;
using En.Ods.Lib.Common.Utility.Weather;

namespace En.Data.Api.Lightning.Client
{
    public class DetectionReportClient : ServiceClientBase
    {
        public DetectionReportClient(string serviceBaseUrl, IAppConfig config = null, IOdsLogManager logManager = null, JsonSerializer serializer = null)
            : base(serviceBaseUrl, config, logManager, serializer)
        {
        }

        public async Task<OdsServiceResponse<IEnumerable<FlashRecord>>> GetCircleDetectionsInMiles(DateTime start, DateTime end, string type, decimal lat, decimal lon, int radiusMiles, LayerType layer, string lxVersion)
        {
            int radiusMeters = (int) (WeatherFunctions.ConvertMilesToKilometers(radiusMiles) * 1000);
            return await GetCircleDetections(start, end, type, lat, lon, radiusMeters, layer, lxVersion);
        }

        public async Task<OdsServiceResponse<IEnumerable<FlashRecord>>> GetCircleDetections(DateTime start, DateTime end, string type, decimal lat, decimal lon, int radius, LayerType layer, string lxVersion)
        {
            if (start == default(DateTime))
            {
                throw new ArgumentException("start");
            }
            if (end == default(DateTime))
            {
                throw new ArgumentNullException("end");
            }
            start = NormalizeDateTimeToUtc(start);
            end = NormalizeDateTimeToUtc(end);
            string strokeType = string.Empty;
            string parameters = null;

            string baseurl = string.Format((layer == LayerType.Flash ? "{0}/api/v2/flash" : "{0}/api/v2/flashportion"), ServiceBaseUrl);

            if (type != "both")
            {
                strokeType = (type == "ic") ? StrokeType.IntraCloud.ToString() : StrokeType.CloudToGround.ToString();

                parameters = string.Format("?start={0}&end={1}&lat={2}&lon={3}&type={4}&radius={5}",
                       WebUtility.UrlEncode(start.ToString("s")),
                       WebUtility.UrlEncode(end.ToString("s")),
                       lat,
                       lon,
                       strokeType,
                       radius);
            }
            else
            {
                parameters = string.Format("?start={0}&end={1}&lat={2}&lon={3}&type={4}&radius={5}",
                       WebUtility.UrlEncode(start.ToString("s")),
                       WebUtility.UrlEncode(end.ToString("s")),
                       lat,
                       lon,
                       StrokeType.None,
                       radius);
            }

            string cgurl = baseurl + parameters;

            return new OdsServiceResponse<IEnumerable<FlashRecord>>(await GetResponseAsync<IEnumerable<FlashRecord>>(cgurl).ConfigureAwait(false));
        }

        public async Task<OdsServiceResponse<IEnumerable<FlashRecord>>> GetBoxDetections(DateTime start, DateTime end, string type, decimal ullat, decimal ullon, decimal lrlat, decimal lrlon, LayerType layer, string lxVersion)
        {
            if (start == default(DateTime))
            {
                throw new ArgumentException("start");
            }
            if (end == default(DateTime))
            {
                throw new ArgumentNullException("end");
            }
            start = NormalizeDateTimeToUtc(start);
            end = NormalizeDateTimeToUtc(end);
            string strokeType = string.Empty;
            string parameters = null;

            string baseurl = string.Format((layer == LayerType.Flash ? "{0}/api/v2/flash" : "{0}/api/v2/flashportion"), ServiceBaseUrl);

            if (type != "both")
            {
                strokeType = (type == "ic") ? StrokeType.IntraCloud.ToString() : StrokeType.CloudToGround.ToString();

                parameters = string.Format("?start={0}&end={1}&ullat={2}&ullon={3}&lrlat={4}&lrlon={5}&type={6}",
                       WebUtility.UrlEncode(start.ToString("s")),
                       WebUtility.UrlEncode(end.ToString("s")),
                       ullat,
                       ullon,
                       lrlat,
                       lrlon,
                       strokeType);
            }
            else
            {
                parameters = string.Format("?start={0}&end={1}&ullat={2}&ullon={3}&lrlat={4}&lrlon={5}&type={6}",
                       WebUtility.UrlEncode(start.ToString("s")),
                       WebUtility.UrlEncode(end.ToString("s")),
                       ullat,
                       ullon,
                       lrlat,
                       lrlon,
                       StrokeType.None);
            }

            string cgurl = baseurl + parameters;

            return new OdsServiceResponse<IEnumerable<FlashRecord>>(await GetResponseAsync<IEnumerable<FlashRecord>>(cgurl).ConfigureAwait(false));
        }

        public static DateTime NormalizeDateTimeToUtc(DateTime value)
        {
            if (value.Kind != DateTimeKind.Utc)
                value = DateTime.SpecifyKind(value, DateTimeKind.Utc);

            return value;
        }
    }
}

