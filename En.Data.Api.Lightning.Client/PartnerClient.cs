﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

using Newtonsoft.Json;

using En.Ods.Lib.Common.Interface;
using En.Ods.Lib.Common.Utility.ServiceClient;

using En.Data.Api.Lightning.Client.Models;

namespace En.Data.Api.Lightning.Client
{
    public class PartnerClient : ServiceClientBase
    {
        public PartnerClient(string serviceBaseUrl, IAppConfig config = null, IOdsLogManager logManager = null, JsonSerializer serializer = null)
            : base(serviceBaseUrl, config, logManager, serializer)
        {
        }

        public async Task<OdsServiceResponse<IEnumerable<PartnerEvent>>> GetEventsAsync(string id, DateTime startUtc, DateTime endUtc)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentException("id cannot be null or empty");
            }
            if (startUtc == default(DateTime))
            {
                throw new ArgumentException("startUtc");
            }
            if (endUtc == default(DateTime))
            {
                throw new ArgumentNullException("endUtc");
            }
            startUtc = NormalizeDateTimeToUtc(startUtc);
            endUtc = NormalizeDateTimeToUtc(endUtc);
            string url = string.Format("{0}/api/PartnerEvents/{1}?start={2}&end={3}", 
                ServiceBaseUrl,
                 WebUtility.UrlEncode(id),
                 WebUtility.UrlEncode(startUtc.ToString("s")),
                 WebUtility.UrlEncode(endUtc.ToString("s"))
                );
            return new OdsServiceResponse<IEnumerable<PartnerEvent>>(await GetResponseAsync<IEnumerable<PartnerEvent>>(url).ConfigureAwait(false));
        }


        public static DateTime NormalizeDateTimeToUtc(DateTime value)
        {
            if (value.Kind != DateTimeKind.Utc)
                value = DateTime.SpecifyKind(value, DateTimeKind.Utc);

            return value;
        }

    }
}
