﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace En.Data.Api.Lightning.Client.Models
{
    public enum ResponseDataElement { ICHeight = 1, NumberOfSensor = 2, Multiplicity = 3 }

    public class PartnerRecord
    {
        public string PartnerId { get; set; }
        public string PartnerName { get; set; }
        public Boolean IsActive { get; set; } /*bit*/
        public Byte ResponseFormatTypeId { get; set; } /*tinyInt*/
        public Byte ResponsePackageTypeId { get; set; } /*tinyInt*/
        public Boolean IsCloudToGroundEnabled { get; set; } /*bit*/
        public Boolean IsInCloudEnabled { get; set; } /*bit*/
        public string BoundingArea { get; set; } /*Db accepts SqlGeo*/
        public DateTime LastModifiedDateUtc { get; set; }
        public string LastModifiedBy { get; set; }
        public int NetworkTypeId { get; set; }
        public List<string> IpAddress { get; set; }
        public List<int> FeedType { get; set; }
        public Boolean Metadata { get; set; }
    }

}
