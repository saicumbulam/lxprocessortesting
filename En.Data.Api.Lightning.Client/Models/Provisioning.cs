﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace En.Data.Api.Lightning.Client.Models
{
    public class Provisioning
    {
        public enum PackageName
        {
            TlnCg = 1,
            GlnCg = 2,
            TlnCgIc = 3,
            GlnCgIc = 4
        }

        public enum FormatType
        {
            AsciiFlash = 1,
            AsciiPulse = 2,
            BinaryFlash = 3,
            BinaryPulse = 4,
            BinaryCombo = 5
        }

        public enum FeedType
        {
            LxDetectionFeed = 1,
            LxDTAFeed = 2
        }
    }

    public class ProvisioningPartner
    {
        public string PartnerId { get; set; }
        public string PartnerName { get; set; }
        public Boolean IsActive { get; set; } /*bit*/
        public Provisioning.PackageName PackageName { get; set; }
        public Provisioning.FormatType FormatType { get; set; }
        public List<double> BoundingArea { get; set; } /*N S E W*/
        public List<string> IpAddress { get; set; }
        public List<int> FeedTypeList { get; set; }
        public Boolean Metadata { get; set; }

        public ProvisioningPartner()
        {
            
        }

        public ProvisioningPartner(PartnerRecord partnerRecord)
        {
            PartnerId = partnerRecord.PartnerId;
            PartnerName = partnerRecord.PartnerName;
            IsActive = partnerRecord.IsActive;

            if (partnerRecord.NetworkTypeId == 1)
            {
                if (partnerRecord.IsInCloudEnabled)
                {
                    PackageName = Provisioning.PackageName.TlnCgIc;
                }
                else
                {
                    PackageName = Provisioning.PackageName.TlnCg;
                }
            }
            else
            {
                if (partnerRecord.IsInCloudEnabled)
                {
                    PackageName = Provisioning.PackageName.GlnCgIc;
                }
                else
                {
                    PackageName = Provisioning.PackageName.GlnCg;
                }
            }

            if (partnerRecord.ResponseFormatTypeId == 1)
            {
                if (partnerRecord.ResponsePackageTypeId == 1)
                {
                    FormatType = Provisioning.FormatType.AsciiFlash;
                }
                else
                {
                    FormatType = Provisioning.FormatType.AsciiPulse;
                }
            }
            else
            {
                if (partnerRecord.ResponsePackageTypeId == 1)
                {
                    FormatType = Provisioning.FormatType.BinaryFlash;
                }
                else if (partnerRecord.ResponsePackageTypeId == 2)
                {
                    FormatType = Provisioning.FormatType.BinaryPulse;
                }
                else
                {
                    FormatType = Provisioning.FormatType.BinaryCombo;
                }
            }

            BoundingArea = ParseBoundingArea(partnerRecord.BoundingArea);
            IpAddress = partnerRecord.IpAddress;
            FeedTypeList = partnerRecord.FeedType;
            Metadata = partnerRecord.Metadata;
        }

        private List<double> ParseBoundingArea(string boundingArea)
        {
            List<double> result = null;

            if (!string.IsNullOrEmpty(boundingArea))
            {
                //string[] array = boundingArea.Split(','); // POLYGON ((NW, SW, SE, NE, NW))

                ///*N W S E*/
                //string[] sw = array[1].Trim().Split();
                //string[] ne = array[3].Trim().Split();

                /*N W S E*/
                //result = new List<double> { Convert.ToDouble(ne[1]), Convert.ToDouble(sw[0]), Convert.ToDouble(sw[1]), Convert.ToDouble(ne[0]) };


                string pattern = @"POLYGON \(\((\S+) (\S+), (\S+) (\S+), (\S+)";
                Regex rgx = new Regex(pattern);
                Match match = rgx.Match(boundingArea);

                double n = Convert.ToDouble(match.Groups[2].Value); // N 
                double w = Convert.ToDouble(match.Groups[1].Value); // W
                double s = Convert.ToDouble(match.Groups[4].Value); // S
                double e = Convert.ToDouble(match.Groups[5].Value); // E

                result = new List<double> { n, w, s, e };
            }

            return result;
        }
    }
}
