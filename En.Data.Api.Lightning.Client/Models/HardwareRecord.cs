﻿using System;

namespace En.Data.Api.Lightning.Client.Models
{
    public class HardwareRecord
    {
        public DateTime? LastCallHomeUtc { get; set; }
        public string EnnaVersion { get; set; }
        public string EndspVersion { get; set; }
        public decimal? BackUpBatteryVol { get; set; }
        public decimal? MemBackUpVoltage { get; set; }
    }
}
