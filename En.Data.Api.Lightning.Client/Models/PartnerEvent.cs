﻿using System;

namespace En.Data.Api.Lightning.Client.Models
{
    public class PartnerEvent
    {
        public string Text { get; set; }

        public string RemoteIpAddress { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime TimeUtc { get; set; }
    }
}
