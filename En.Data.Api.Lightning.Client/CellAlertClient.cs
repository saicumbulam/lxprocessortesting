﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

using Newtonsoft.Json;

using En.Ods.Lib.Common.Interface;
using En.Ods.Lib.Common.Utility.ServiceClient;

using En.Data.Api.Lightning.Client.Models;

namespace En.Data.Api.Lightning.Client
{
    public class CellAlertClient : ServiceClientBase
    {
        public CellAlertClient(string serviceBaseUrl, IAppConfig config = null, IOdsLogManager logManager = null, JsonSerializer serializer = null)
            : base(serviceBaseUrl, config, logManager, serializer)
        {
        }

        public async Task<OdsServiceResponse<IEnumerable<CellAlertRecord>>> GetCellAlertAsync(DateTime startUtc, DateTime endUtc, decimal ullat, decimal ullon, decimal lrlat, decimal lrlon)
        {
            if (startUtc == default(DateTime))
            {
                throw new ArgumentException("startUtc");
            }
            if (endUtc == default(DateTime))
            {
                throw new ArgumentNullException("endUtc");
            }
            startUtc = NormalizeDateTimeToUtc(startUtc);
            endUtc = NormalizeDateTimeToUtc(endUtc);
            string url = string.Format("{0}/api/v2/cellalert?start={1}&end={2}&ullat={3}&ullon={4}&lrlat={5}&lrlon={6}", 
                ServiceBaseUrl,
                 WebUtility.UrlEncode(startUtc.ToString("s")),
                 WebUtility.UrlEncode(endUtc.ToString("s")),
                 ullat,
                 ullon,
                 lrlat,
                 lrlon
                );
            return new OdsServiceResponse<IEnumerable<CellAlertRecord>>(await GetResponseAsync<IEnumerable<CellAlertRecord>>(url).ConfigureAwait(false));
        }


        public static DateTime NormalizeDateTimeToUtc(DateTime value)
        {
            if (value.Kind != DateTimeKind.Utc)
                value = DateTime.SpecifyKind(value, DateTimeKind.Utc);

            return value;
        }

    }
}
