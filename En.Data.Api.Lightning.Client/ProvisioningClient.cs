﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

using Newtonsoft.Json;

using En.Ods.Lib.Common.Interface;
using En.Ods.Lib.Common.Utility.ServiceClient;

using En.Data.Api.Lightning.Client.Models;

namespace En.Data.Api.Lightning.Client
{
    public class ProvisioningClient : ServiceClientBase
    {
        public ProvisioningClient(string serviceBaseUrl, IAppConfig config = null, IOdsLogManager logManager = null, JsonSerializer serializer = null) 
            : base(serviceBaseUrl, config, logManager, serializer)
        {
        }

        public async Task<OdsServiceResponse<object>> DeleteAsync(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentException("id cannot be null or empty");
            }

            string url = string.Format("{0}/api/provisioning/Partner?id={1}", ServiceBaseUrl, WebUtility.UrlEncode(id));
            
            HttpResponseMessage message = await HttpClient.DeleteAsync(url);
            return new OdsServiceResponse<object>(await ProcessResponseMessage<object>(message).ConfigureAwait(false));
        }

        public async Task<OdsServiceResponse<ProvisioningPartner>> PostAsync(ProvisioningPartner provisioningPartner)
        {
            PartnerRecord partner = new PartnerRecord
            {
                PartnerName = provisioningPartner.PartnerName,
                IsActive = provisioningPartner.IsActive,
                BoundingArea = FormatBoundingArea(provisioningPartner.BoundingArea),
                Metadata = provisioningPartner.Metadata,
                FeedType = provisioningPartner.FeedTypeList
            };

            SetPartner(partner, provisioningPartner.PackageName, provisioningPartner.FormatType);

            string url = string.Format("{0}/api/provisioning/Partner", ServiceBaseUrl);

            HttpContent httpContent = new StringContent(JsonConvert.SerializeObject(partner));
            httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            HttpResponseMessage message = await HttpClient.PostAsync(url, httpContent);
            return new OdsServiceResponse<ProvisioningPartner>(await ProcessResponseMessage<ProvisioningPartner>(message).ConfigureAwait(false));
        }

        public async Task<OdsServiceResponse<ProvisioningPartner>> PutAsync(ProvisioningPartner provisioningPartner)
        {
            PartnerRecord partner = new PartnerRecord
            {
                PartnerId = provisioningPartner.PartnerId,
                PartnerName = provisioningPartner.PartnerName,
                IsActive = provisioningPartner.IsActive,
                BoundingArea = FormatBoundingArea(provisioningPartner.BoundingArea),
                Metadata = provisioningPartner.Metadata,
                IpAddress = provisioningPartner.IpAddress
                //FeedType = provisioningPartner.FeedTypeList
            };

            SetPartner(partner, provisioningPartner.PackageName, provisioningPartner.FormatType);

            string url = string.Format("{0}/api/provisioning/Partner", ServiceBaseUrl);

            HttpContent httpContent = new StringContent(JsonConvert.SerializeObject(partner));
            httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            HttpResponseMessage message = await HttpClient.PutAsync(url, httpContent);
            return new OdsServiceResponse<ProvisioningPartner>(await ProcessResponseMessage<ProvisioningPartner>(message).ConfigureAwait(false));
        }

        private static void SetPartner(PartnerRecord partner, Provisioning.PackageName packageName, Provisioning.FormatType formatType)
        {
            if (packageName == Provisioning.PackageName.TlnCg)
            {
                partner.NetworkTypeId = 1;
                partner.IsCloudToGroundEnabled = true;
                partner.IsInCloudEnabled = false;
            }
            else if (packageName == Provisioning.PackageName.GlnCg)
            {
                partner.NetworkTypeId = 2;
                partner.IsCloudToGroundEnabled = true;
                partner.IsInCloudEnabled = false;
            }
            if (packageName == Provisioning.PackageName.TlnCgIc)
            {
                partner.NetworkTypeId = 1;
                partner.IsCloudToGroundEnabled = true;
                partner.IsInCloudEnabled = true;
            }
            else if (packageName == Provisioning.PackageName.GlnCgIc) //GlnCgIc
            {
                partner.NetworkTypeId = 2;
                partner.IsCloudToGroundEnabled = true;
                partner.IsInCloudEnabled = true;
            }

            if (formatType == Provisioning.FormatType.AsciiFlash)
            {
                partner.ResponseFormatTypeId = 1;
                partner.ResponsePackageTypeId = 1;
            }
            else if (formatType == Provisioning.FormatType.AsciiPulse)
            {
                partner.ResponseFormatTypeId = 1;
                partner.ResponsePackageTypeId = 2;
            }
            else if (formatType == Provisioning.FormatType.BinaryFlash)
            {
                partner.ResponseFormatTypeId = 2;
                partner.ResponsePackageTypeId = 1;
            }
            else if (formatType == Provisioning.FormatType.BinaryPulse)
            {
                partner.ResponseFormatTypeId = 2;
                partner.ResponsePackageTypeId = 2;
            }
            else // BinaryCombo
            {
                partner.ResponseFormatTypeId = 2;
                partner.ResponsePackageTypeId = 3;
            }
        }

        public async Task<OdsServiceResponse<List<ProvisioningPartner>>> GetAllAsync(Provisioning.FeedType feedType)
        {
            string url = string.Format("{0}/api/provisioning/Partner?feedType={1}", ServiceBaseUrl, feedType);

            return new OdsServiceResponse<List<ProvisioningPartner>>(await GetResponseAsync<List<ProvisioningPartner>>(url).ConfigureAwait(false));
        }

        public async Task<OdsServiceResponse<ProvisioningPartner>> Get(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentException("id cannot be null or empty");
            }
            string url = string.Format("{0}/api/provisioning/Partner?partnerId={1}", ServiceBaseUrl, id);

            return new OdsServiceResponse<ProvisioningPartner>(await GetResponseAsync<ProvisioningPartner>(url).ConfigureAwait(false));
        }

        public async Task<OdsServiceResponse<List<EventLogRecord>>> GetEventLogAsync(string id, int epochStart = -1, int epochEnd = -1)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentException("id cannot be null or empty");
            }
            string url;
            if (epochStart == -1 || epochEnd == -1)
            {
                url = string.Format("{0}/api/provisioning/EventLog?partnerId={1}", ServiceBaseUrl, id);
            }
            else
            {
                url = string.Format("{0}/api/provisioning/EventLog?partnerId={1}&epochStart={2}&epochEnd={3}", ServiceBaseUrl, id, epochStart, epochEnd);
            }

            return new OdsServiceResponse<List<EventLogRecord>>(await GetResponseAsync<List<EventLogRecord>>(url).ConfigureAwait(false));
        }

        private static string FormatBoundingArea(List<double> boundingArea)
        {
            string result = null;

            if (boundingArea.Count == 4)
            {
                double n = boundingArea[0];
                double w = boundingArea[1];
                double s = boundingArea[2];
                double e = boundingArea[3];

                result = string.Format("{0},{1};{2},{3}", n, w, s, e); // "n,w;s,e"
            }

            return result;
        }


    }
}
