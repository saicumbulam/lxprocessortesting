﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

using Newtonsoft.Json;

using En.Ods.Lib.Common.Interface;
using En.Ods.Lib.Common.Utility.ServiceClient;

using En.Data.Api.Lightning.Client.Models;

namespace En.Data.Api.Lightning.Client
{
    public class NwsAlertClient : ServiceClientBase
    {
        public NwsAlertClient(string serviceBaseUrl, IAppConfig config = null, IOdsLogManager logManager = null, JsonSerializer serializer = null)
            : base(serviceBaseUrl, config, logManager, serializer)
        {
        }

        public async Task<OdsServiceResponse<List<NwsAlert>>> GetNwsAlertAsync(DateTime start, DateTime end)
        {
            if (start == default(DateTime))
            {
                throw new ArgumentException("start");
            }
            if (end == default(DateTime))
            {
                throw new ArgumentNullException("end");
            }
            start = NormalizeDateTimeToUtc(start);
            end = NormalizeDateTimeToUtc(end);
            string url = string.Format("{0}/api/v2/nwsalert?start={1}&end={2}", 
                ServiceBaseUrl,
                WebUtility.UrlEncode(start.ToString("s")),
                WebUtility.UrlEncode(end.ToString("s"))
                );
            return new OdsServiceResponse<List<NwsAlert>>(await GetResponseAsync<List<NwsAlert>>(url).ConfigureAwait(false));
        }

        public static DateTime NormalizeDateTimeToUtc(DateTime value)
        {
            if (value.Kind != DateTimeKind.Utc)
                value = DateTime.SpecifyKind(value, DateTimeKind.Utc);

            return value;
        }
    }
}
