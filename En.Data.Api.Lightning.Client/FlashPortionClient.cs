﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

using Newtonsoft.Json;

using En.Ods.Lib.Common.Interface;
using En.Ods.Lib.Common.Utility.ServiceClient;

using En.Data.Api.Lightning.Client.Models;

namespace En.Data.Api.Lightning.Client
{
    public class FlashPortionClient : ServiceClientBase
    {
        public FlashPortionClient(string serviceBaseUrl, IAppConfig config = null, IOdsLogManager logManager = null, JsonSerializer serializer = null)
            : base(serviceBaseUrl, config, logManager, serializer)
        {
        }

        public async Task<OdsServiceResponse<List<FlashRecord>>> GetFlashPortionAsync(DateTime start, DateTime end, StrokeType type, decimal lat, decimal lon, int radius)
        {
            if (start == default(DateTime))
            {
                throw new ArgumentException("start");
            }
            if (end == default(DateTime))
            {
                throw new ArgumentNullException("end");
            }
            start = NormalizeDateTimeToUtc(start);
            end = NormalizeDateTimeToUtc(end);
            string url = string.Format("{0}/api/v2/flashportion?start={1}&end={2}&type={3}&lat={4}&lon={5}&radius={6}", 
                ServiceBaseUrl,
                WebUtility.UrlEncode(start.ToString("s")),
                WebUtility.UrlEncode(end.ToString("s")),
                type,
                lat,
                lon,
                radius
                );
            return new OdsServiceResponse<List<FlashRecord>>(await GetResponseAsync<List<FlashRecord>>(url).ConfigureAwait(false));
        }

        public async Task<OdsServiceResponse<List<FlashRecord>>> GetFlashPortionAsync(DateTime start, DateTime end, StrokeType type, decimal ullat, decimal ullon, decimal lrlat, decimal lrlon)
        {
            if (start == default(DateTime))
            {
                throw new ArgumentException("start");
            }
            if (end == default(DateTime))
            {
                throw new ArgumentNullException("end");
            }
            start = NormalizeDateTimeToUtc(start);
            end = NormalizeDateTimeToUtc(end);
            string url = string.Format("{0}/api/v2/flashportion?start={1}&end={2}&type={3}&ullat={4}&ullon={5}&lrlat={6}&lrlon={7}",
                ServiceBaseUrl,
                WebUtility.UrlEncode(start.ToString("s")),
                WebUtility.UrlEncode(end.ToString("s")),
                type,
                ullat,
                ullon,
                lrlat,
                lrlon
                );
            return new OdsServiceResponse<List<FlashRecord>>(await GetResponseAsync<List<FlashRecord>>(url).ConfigureAwait(false));
        }

        public static DateTime NormalizeDateTimeToUtc(DateTime value)
        {
            if (value.Kind != DateTimeKind.Utc)
                value = DateTime.SpecifyKind(value, DateTimeKind.Utc);

            return value;
        }
    }
}
