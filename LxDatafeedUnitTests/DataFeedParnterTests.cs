﻿//using System.Collections;
//using System.Net.Sockets;
//using System.Security.Cryptography.X509Certificates;
//using Aws.Core.Data;
//using LxDatafeed;
//using LxDatafeed.Data;
//using LxDatafeed.Data.Partner;
//using LxDatafeed.Data.ResponsePackage;
//using LxDatafeed.Net;
//using Microsoft.VisualStudio.TestTools.UnitTesting;

//namespace LxDatafeedUnitTests
//{
//    [TestClass]
//    public class DataFeedParnterTests
//    {
//        [TestMethod]
//        [DeploymentItem("LxDatafeed.dll")]
//        public void PartnerSvcInit()
//        {
//            Assert.IsTrue(PartnerSvc.Init());
//        }

//        [TestMethod]
//        public void PartnerGetDataTest()
//        {
//            string partnerId = "endev2";
//            var bsuccess =  PartnerSvc.Init();
//            var partnerData = PartnerSvc.GetData(partnerId);
//            Assert.IsNotNull(partnerData);
//        }

//         [TestMethod]
//        public void LoadPartnerListTest()
//         {
//             bool result = false;
//            // var bsuccess = PartnerSvc.Init();

//             //PrivateObject privateType = new PrivateObject(typeof(PartnerSvc)); 
//             //var partnerList = privateType.Invoke("LoadPartnerList");

//             ArrayList list = DbCommand.ExecuteList(AppConfig.LightningDbConnStr, new GetPartnerListDbCmdText());
//             if(list != null)
//             {
//                if(list.Count > 0)
//                {
//                    result = true;
//                }
//             }
//             Assert.IsTrue(result);
//         }

//         [TestMethod]
//        public void LoadIpWhitelistTest()
//        {
//            bool result = false;
//            ArrayList list = DbCommand.ExecuteList(AppConfig.LightningDbConnStr, new GetPartnerIpWhitelistDbCmdText());
//            if (list != null)
//            {
//                if (list.Count > 0)
//                {
//                    result = true;
//                }
//            }
//            Assert.IsTrue(result);
//        }

//        [TestMethod]
//        public void LoadDataElementTest()
//          {
//              bool result = false;
//              ArrayList list = DbCommand.ExecuteList(AppConfig.LightningDbConnStr, new GetPartnerDataElementDbCmdText());
//              if (list != null)
//              {
//                  if (list.Count > 0)
//                  {
//                      result = true;
//                  }
                  
//              }
//              Assert.IsTrue(result);
//          }

//        [TestMethod]
//        public void LoadConfiguration()
//        {
//            bool result = false;
//            ArrayList list = DbCommand.ExecuteList(AppConfig.LightningDbConnStr, new GetPartnerConfigurationDbCmdText());
//            if (list != null)
//            {
//                if (list.Count > 0)
//                {
//                    result = true;
//                }

//            }
//            Assert.IsTrue(result);
//        }

//         [TestMethod]
//        public void PartnerNetworkTypeGLNTest()
//        {
//            string partnerId = "endev2";
//             bool result = false;
 
//            var partnerData = PartnerSvc.GetData(partnerId);
//            if (partnerData != null && partnerData.NetworkType == FlashNetworkType.EnGlnNetwork)
//            {
//                result = true;
//            }
//            Assert.IsTrue(result);
//        }

//         [TestMethod]
//         public void PartnerNetworkTypeTLNTest()
//         {
//             string partnerId = "endev2";
//             bool result = false;

//             var partnerData = PartnerSvc.GetData(partnerId);
//             if (partnerData != null && partnerData.NetworkType == FlashNetworkType.EnTlnNetwork)
//             {
//                 result = true;
//             }
//             Assert.IsTrue(result);
//         }

//        [TestMethod]
//        public void PartnerConfigurationForDataElementAllowedTest()
//        {
//             string partnerId = "endev2";
//             bool result = false;

//              var partnerData = PartnerSvc.GetData(partnerId);
//              if (partnerData != null && partnerData.Configuration != null && partnerData.Configuration.ContainsKey(ConfigurationType.CanPartnerpassInDataElementsonConnection))
//              {
//                  result = true;
//              }

//              Assert.IsTrue(result);
//        }

//        [TestMethod]
//        public void PartnerConfigurationForBoundingBoxAllowedTest()
//        {
//            string partnerId = "endev2";
//            bool result = false;

//            var partnerData = PartnerSvc.GetData(partnerId);
//            if (partnerData != null && partnerData.Configuration != null && partnerData.Configuration.ContainsKey(ConfigurationType.CanPartnerPassInBoundingBoxOnConnection))
//            {
//                result = true;
//            }

//            Assert.IsTrue(result);
//        }

//        [TestMethod]
//        public void PartnerConfigurationForLxClassificationAllowedTest()
//        {
//            string partnerId = "endev2";
//            bool result = false;

//            var partnerData = PartnerSvc.GetData(partnerId);
//            if (partnerData != null && partnerData.Configuration != null && partnerData.Configuration.ContainsKey(ConfigurationType.CanPartnerPassInLxClassificationOnConnection))
//            {
//                result = true;
//            }

//            Assert.IsTrue(result);
//        }


//        [TestMethod]
//        public void PartnerProvisioningforICHeightTest()
//        {
//            string partnerId = "endev2";
//            bool result = true;

//            var partnerdataDB = PartnerSvc.GetData(partnerId);

//            var connStrData =  ConnectionStringData();

//            //Calling SetupConnectionSettings method to test
//            X509Certificate sslcert = null;
//            TcpClient tcpclient = new TcpClient("127.0.0.1", 2324);
//            SocketState socketStateData = new SocketState(tcpclient, 256, false, sslcert);

//            PrivateType privateType = new PrivateType(typeof(ConnectionStringParser));
//            privateType.InvokeStatic("SetupConnectionSettings", socketStateData, connStrData, partnerdataDB);

//            if (socketStateData.PartnerSessionData != null)
//            {
//                if (socketStateData.PartnerSessionData.ResponseDataElements.Contains(ResponseDataElement.IcHeight))
//                {
//                    result = true;
//                }
//            }

//            Assert.IsTrue(result);

//        }

//        [TestMethod]
//        public void PartnerProvisioningforMultiplicityTest()
//        {
//            string partnerId = "endev2";
//            bool result = true;

//            var partnerdataDB = PartnerSvc.GetData(partnerId);

//            var connStrData = ConnectionStringData();

//            //Calling SetupConnectionSettings method to test
//            X509Certificate sslcert = null;
//            TcpClient tcpclient = new TcpClient("127.0.0.1", 2324);
//            SocketState socketStateData = new SocketState(tcpclient, 256, false, sslcert);

//            PrivateType privateType = new PrivateType(typeof(ConnectionStringParser));
//            privateType.InvokeStatic("SetupConnectionSettings", socketStateData, connStrData, partnerdataDB);

//            if (socketStateData.PartnerSessionData != null)
//            {
//                if (socketStateData.PartnerSessionData.ResponseDataElements.Contains(ResponseDataElement.Multiplicity))
//                {
//                    result = true;
//                }
//            }

//            Assert.IsTrue(result);

//        }


//        [TestMethod]
//        public void PartnerProvisioningforSensorsTest()
//        {
//            string partnerId = "endev2";
//            bool result = true;

//            var partnerdataDB = PartnerSvc.GetData(partnerId);

//            var connStrData = ConnectionStringData();

//            //Calling SetupConnectionSettings method to test
//            X509Certificate sslcert = null;
//            TcpClient tcpclient = new TcpClient("127.0.0.1", 2324);
//            SocketState socketStateData = new SocketState(tcpclient, 256, false, sslcert);

//            PrivateType privateType = new PrivateType(typeof(ConnectionStringParser));
//            privateType.InvokeStatic("SetupConnectionSettings", socketStateData, connStrData, partnerdataDB);

//            if (socketStateData.PartnerSessionData != null)
//            {
//                if (socketStateData.PartnerSessionData.ResponseDataElements.Contains(ResponseDataElement.NumberOfSensors))
//                {
//                    result = true;
//                }
//            }

//            Assert.IsTrue(result);

//        }

//        [TestMethod]
//        public void  HasDataElementTest()
//        {
//           /* PartnerData partnerdataDB = new PartnerData();
//            partnerdataDB.PartnerId = "endev";
//            partnerdataDB.NetworkType = FlashNetworkType.EnTlnNetwork;
//            partnerdataDB.LxClassificationType = LxClassificationType.InCloud;
//            */
//            var partnerdataDb = PartnerSvc.GetData("endev2");
//            Assert.IsTrue(partnerdataDb.HasDataElement(ResponseDataElement.IcHeight));
//        }



//        private ConnectionStringParser.Data ConnectionStringData()
//        {
//            ConnectionStringParser.Data conStringdata = new ConnectionStringParser.Data();

//            conStringdata.PartnerId = "endev2";
//            conStringdata.LxClassificationType = LxClassificationType.CloudToGround;

//            conStringdata.Multiplicity = true;
//            conStringdata.NumberOfSensors = true;
//            conStringdata.IcHeight = true;

//            conStringdata.ResponseFormatType = ResponseFormatType.Ascii;
//            conStringdata.ResponsePackageType = ResponsePackageType.Flash;

//            return conStringdata;
//        }

       
//    }
//}
