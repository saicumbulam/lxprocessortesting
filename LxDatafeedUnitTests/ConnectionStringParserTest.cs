﻿//using System.Collections.Generic;
//using System.Net.Sockets;
//using System.Security.Cryptography.X509Certificates;
//using System.Text;
//using LxDatafeed.Data;
//using LxDatafeed.Data.Partner;
//using LxDatafeed.Data.ResponsePackage;
//using LxDatafeed.Net;
//using Microsoft.VisualStudio.TestTools.UnitTesting;

//namespace LxDatafeedUnitTests
//{
    
    
//    /// <summary>
//    ///This is a test class for ConnectionStringParserTest and is intended
//    ///to contain all ConnectionStringParserTest Unit Tests
//    ///</summary>
//    [TestClass()]
//    public class ConnectionStringParserTest
//    {


//        private TestContext testContextInstance;

//        /// <summary>
//        ///Gets or sets the test context which provides
//        ///information about and functionality for the current test run.
//        ///</summary>
//        public TestContext TestContext
//        {
//            get
//            {
//                return testContextInstance;
//            }
//            set
//            {
//                testContextInstance = value;
//            }
//        }

//        #region Additional test attributes
//        // 
//        //You can use the following additional attributes as you write your tests:
//        //
//        //Use ClassInitialize to run code before running the first test in the class
//        //[ClassInitialize()]
//        //public static void MyClassInitialize(TestContext testContext)
//        //{
//        //}
//        //
//        //Use ClassCleanup to run code after all tests in a class have run
//        //[ClassCleanup()]
//        //public static void MyClassCleanup()
//        //{
//        //}
//        //
//        //Use TestInitialize to run code before running each test
//        //[TestInitialize()]
//        //public void MyTestInitialize()
//        //{
//        //}
//        //
//        //Use TestCleanup to run code after each test has run
//        //[TestCleanup()]
//        //public void MyTestCleanup()
//        //{
//        //}
//        //
//        #endregion


//        /// <summary>
//        ///A test for SetupConnectionSettings
//        ///</summary>
//        [TestMethod()]
//        [DeploymentItem("LxDatafeed.dll")]
//        public void SetupConnectionSettingsTest()
//        {
//            bool result = false;
//            bool inputResponseElement_Ic = false;

//            //SocketState_Accessor socketState = null; // TODO: Initialize to an appropriate value
//            //ConnectionStringParser_Accessor.Data connStrData = null; // TODO: Initialize to an appropriate value
//            //PartnerData_Accessor clonedPartnerData = null; // TODO: Initialize to an appropriate value
//            //ConnectionStringParser_Accessor.SetupConnectionSettings(socketState, connStrData, clonedPartnerData);


//            //Build a connection string object
//            ConnectionStringParser.Data conStringdata = new ConnectionStringParser.Data();
//            conStringdata.PartnerId = "endev";
//            conStringdata.LxClassificationType = LxClassificationType.CloudToGround;
//            conStringdata.Multiplicity = true;
//            conStringdata.NumberOfSensors = true;
//            conStringdata.IcHeight = true;
//            conStringdata.ResponseFormatType = ResponseFormatType.Ascii;
//            conStringdata.ResponsePackageType = ResponsePackageType.Flash;

//            //Build a database partner object
//            PartnerData partnerdataDB = new PartnerData();
//            partnerdataDB.PartnerId = "endev";
//            partnerdataDB.NetworkType = FlashNetworkType.EnTlnNetwork;
//            partnerdataDB.LxClassificationType = LxClassificationType.InCloud;

//            if (partnerdataDB.ResponseDataElements != null)
//            {
//                partnerdataDB.ResponseDataElements = new HashSet<ResponseDataElement>();
//                foreach (ResponseDataElement responseDataElement in partnerdataDB.ResponseDataElements)
//                {
//                    partnerdataDB.ResponseDataElements.Add(responseDataElement);
//                }
//            }

//            partnerdataDB.ResponseDataElements  = new HashSet<ResponseDataElement>();
//            partnerdataDB.ResponseDataElements.Add(ResponseDataElement.IcHeight);
//            partnerdataDB.ResponseDataElements.Add(ResponseDataElement.Multiplicity);
//            partnerdataDB.ResponseDataElements.Add(ResponseDataElement.NumberOfSensors);
//            partnerdataDB.ResponseFormatType = ResponseFormatType.Ascii;
//            partnerdataDB.ResponsePackageType = ResponsePackageType.Flash;
//            partnerdataDB.ResponsePackageVersion = 2;

//            partnerdataDB.Configuration = new Dictionary<ConfigurationType, string>();
//            partnerdataDB.Configuration.Add(ConfigurationType.CanPartnerPassInBoundingBoxOnConnection, "1");
//            partnerdataDB.Configuration.Add(ConfigurationType.CanPartnerPassInLxClassificationOnConnection, "2");
//            partnerdataDB.Configuration.Add(ConfigurationType.CanPartnerpassInDataElementsonConnection, "3");

//            //Calling SetupConnectionSettings method to test
//            X509Certificate sslcert = null;
//            TcpClient tcpclient = new TcpClient("127.0.0.1", 2324);
//            SocketState socketStateData = new SocketState(tcpclient, 256, false, sslcert);

//            PrivateType privateType = new PrivateType(typeof(ConnectionStringParser));
//            privateType.InvokeStatic("SetupConnectionSettings", socketStateData, conStringdata, partnerdataDB);

//            if (socketStateData.PartnerSessionData != null)
//            {
//                if (socketStateData.PartnerSessionData.ResponseDataElements.Contains(ResponseDataElement.IcHeight))
//                {
//                    result = true;
//                }
//            }

//            if (partnerdataDB.Configuration.ContainsValue("3"))
//            {
//                inputResponseElement_Ic = conStringdata.IcHeight;
            
//            }
//            else
//            {
//                if (partnerdataDB.ResponseDataElements.Contains(ResponseDataElement.IcHeight))
//                {
//                    inputResponseElement_Ic = true;
//                }
//            }

//            Assert.AreEqual(result, inputResponseElement_Ic);

//          //  Assert.Inconclusive("A method that does not return a value cannot be verified.");
//        }

//        /// <summary>
//        ///A test for Parse
//        ///</summary>
//        [TestMethod()]
//        public void ParseTest()
//        {
//            SocketState socketState = null; // TODO: Initialize to an appropriate value
//            bool expected = true; // TODO: Initialize to an appropriate value
//            bool actual;
//            string connStringJson = string.Empty;


//            ConnectionStringParser.Data data = ConnectionStringData();
//            if (data.Version >= 2)
//            {
//                connStringJson = data.ToJson();
//            }
//            byte[] buf = Encoding.ASCII.GetBytes((connStringJson));

//            X509Certificate sslcert = null;
//            TcpClient tcpclient = new TcpClient("127.0.0.1", 2324);
//            socketState = new SocketState(tcpclient, 256, false, sslcert);
//            socketState.Buffer = buf;

//            actual = ConnectionStringParser.Parse(socketState);
//            Assert.AreEqual(expected, actual);
//        }

//        private ConnectionStringParser.Data ConnectionStringData()
//        {
//            ConnectionStringParser.Data conStringdata = new ConnectionStringParser.Data();

//            conStringdata.PartnerId = "endev";
//            conStringdata.LxClassificationType = LxClassificationType.CloudToGround;

//            conStringdata.Multiplicity = true;
//            conStringdata.NumberOfSensors = true;
//            conStringdata.IcHeight = true;

//            conStringdata.ResponseFormatType = ResponseFormatType.Ascii;
//            conStringdata.ResponsePackageType = ResponsePackageType.Flash;
//            conStringdata.Version = 2;

//            return conStringdata;
//        }

//        /// <summary>
//        ///A test for IsValidConnection
//        ///</summary>
//        [TestMethod()]
//        [DeploymentItem("LxDatafeed.dll")]
//        public void IsValidConnectionTest()
//        {
//           // SocketState_Accessor socketState = null; // TODO: Initialize to an appropriate value
//            //ConnectionStringParser_Accessor.Data connStrData = null; // TODO: Initialize to an appropriate value

//            SocketState socketState = null;
//            string eventLogMsg = string.Empty; // TODO: Initialize to an appropriate value
//            string eventLogMsgExpected = string.Empty; // TODO: Initialize to an appropriate value
//            bool expected = true; // TODO: Initialize to an appropriate value
//            bool actual;
//            string connStringJson = string.Empty;

//            ConnectionStringParser.Data data = ConnectionStringData();
//            if (data.Version >= 2)
//            {
//                connStringJson = data.ToJson();
//            }
//            byte[] buf = Encoding.ASCII.GetBytes((connStringJson));

//            X509Certificate sslcert = null;
//            TcpClient tcpclient = new TcpClient("127.0.0.1", 2324);
//            socketState = new SocketState(tcpclient, 256, false, sslcert);
//            socketState.Buffer = buf;


//            PrivateType privateType = new PrivateType(typeof(ConnectionStringParser));
//            actual = (bool) privateType.InvokeStatic("IsValidConnection", socketState, data, eventLogMsg);

//            //actual = ConnectionStringParser_Accessor.IsValidConnection(socketState, connStrData, out eventLogMsg);
//            Assert.AreEqual(eventLogMsgExpected, eventLogMsg);
//            Assert.AreEqual(expected, actual);
            
//        }
//    }
//}
