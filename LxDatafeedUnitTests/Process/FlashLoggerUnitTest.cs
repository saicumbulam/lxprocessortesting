﻿using System;
using LxDatafeed.Process;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LxDatafeedUnitTests.Process
{
    [TestClass]
    public class FlashLoggerUnitTest
    {
        [TestMethod]
        public void GetFullFileNameTest()
        {
            DateTime dt;
            DateTime.TryParse("05/01/2009 14:57:32.8", out dt);

            int spf = 30;
            string bfn = "flash--{0}.txt";
            string fdn = @"C:\Whatever\";

            PrivateType pt = new PrivateType(typeof(FlashLogger));
            string actual = (string)pt.InvokeStatic("GetFullFileName", dt, spf, bfn, fdn);
            string expected = @"C:\Whatever\flash--2009-05-01T14-57-30.txt";
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetFullFileNameTest2()
        {
            DateTime dt;
            DateTime.TryParse("05/01/2009 14:57:29.8", out dt);

            int spf = 30;
            string bfn = "flash--{0}.txt";
            string fdn = @"C:\Whatever\";

            PrivateType pt = new PrivateType(typeof(FlashLogger));
            string actual = (string)pt.InvokeStatic("GetFullFileName", dt, spf, bfn, fdn);
            string expected = @"C:\Whatever\flash--2009-05-01T14-57-00.txt";
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetFullFileNameTest3()
        {
            DateTime dt;
            DateTime.TryParse("05/01/2009 14:57:29.8", out dt);

            int spf = 60;
            string bfn = "flash--{0}.txt";
            string fdn = @"C:\Whatever\";

            PrivateType pt = new PrivateType(typeof(FlashLogger));
            string actual = (string)pt.InvokeStatic("GetFullFileName", dt, spf, bfn, fdn);
            string expected = @"C:\Whatever\flash--2009-05-01T14-57-00.txt";
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetFullFileNameTest4()
        {
            DateTime dt;
            DateTime.TryParse("05/01/2009 14:57:29.8", out dt);

            int spf = 1;
            string bfn = "flash--{0}.txt";
            string fdn = @"C:\Whatever\";

            PrivateType pt = new PrivateType(typeof(FlashLogger));
            string actual = (string)pt.InvokeStatic("GetFullFileName", dt, spf, bfn, fdn);
            string expected = @"C:\Whatever\flash--2009-05-01T14-57-29.txt";
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetFullFileNameTest5()
        {
            DateTime dt;
            DateTime.TryParse("05/01/2009 14:57:29.8", out dt);

            int spf = 5;
            string bfn = "flash--{0}.txt";
            string fdn = @"C:\Whatever\";

            PrivateType pt = new PrivateType(typeof(FlashLogger));
            string actual = (string)pt.InvokeStatic("GetFullFileName", dt, spf, bfn, fdn);
            string expected = @"C:\Whatever\flash--2009-05-01T14-57-25.txt";
            Assert.AreEqual(expected, actual);
        }
    }
}
