﻿using System;
using System.ServiceProcess;
using System.Threading.Tasks;

using Aws.Core.Utilities;

using LxDatafeedFileDelivery.Common;
using LxDatafeedFileDelivery.Process;

namespace LxDatafeedFileDelivery
{
    public class Program
    {
        private static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
            { 
                new WinService() 
            };
            ServiceBase.Run(ServicesToRun);
        }

        private static Program _program = null;

        public static void Start()
        {
            EventManager.Init();

            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += OnUnhandledException;

            _program = new Program();

            Task.Factory.StartNew(() => _program.Begin());
        }


        public static void Stop()
        {
            if (_program != null)
                _program.End();

            EventManager.ShutDown();
        }

        private static void OnUnhandledException(object sender, UnhandledExceptionEventArgs args)
        {
            Exception ex = (Exception)args.ExceptionObject;
            EventManager.LogError(EventId.Program, "Global unhandled exception", ex);
            Stop();
        }

        private MonitoringHandler _monitoringHandler;
        private FileReader _reader;
        private FileCleaner _cleaner;
        private S3Archiver _s3Archiver;

        private Program()
        {
            _cleaner = new FileCleaner(Config.FileCleanupIntervalHours, Config.FileCleanupKeepHours);
            _reader = new FileReader(Config.FileReaderPickupFolder, Config.FileReaderModels);
            _monitoringHandler = new MonitoringHandler(_reader);
            _s3Archiver = new S3Archiver(Config.S3StagingFolder, Config.LxFileMinutes);
        }

        private void Begin()
        {
            _monitoringHandler.Start();
            _reader.Start();
            _cleaner.Start();
            _s3Archiver.Start();
        }

        private void End()
        {
            _monitoringHandler.Stop();
            _reader.Stop();
            _cleaner.Stop();
            _s3Archiver.Stop();
        }
    }
}
