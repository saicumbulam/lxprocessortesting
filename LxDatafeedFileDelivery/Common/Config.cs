﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Aws.Core.Utilities;
using Aws.Core.Utilities.Config;
using LxCommon.Utilities;
using LxDatafeedFileDelivery.Model;

namespace LxDatafeedFileDelivery.Common
{
    public class Config : AppConfig
    {
        private static readonly ReadOnlyCollection<FileReaderModel> _fileReaders;

        static Config()
        {
            _fileReaders = LoadFileReaders();
        }

        public static ReadOnlyCollection<FileReaderModel> FileReaderModels
        {
            get { return _fileReaders; }
        }

        private static ReadOnlyCollection<FileReaderModel> LoadFileReaders()
        {
            ReadOnlyCollection<FileReaderModel> fileReaders = null;

            try
            {
                int num = Get("NumberOfFileReaders", 0);

                if (num > 0)
                {
                    List<FileReaderModel> listFileReaders = new List<FileReaderModel>();

                    for (int i = 1; i <= num; i++)
                    {
                        FileReaderModel readerModel = new FileReaderModel();
                        readerModel.Name = Get<string>(string.Format("FileReaderName_{0}", i));
                        string outputFolder = Get<string>(string.Format("FileReaderOutputFolder_{0}", i));
                        readerModel.OutputFolder = null;
                        if (outputFolder != null)
                        {
                            readerModel.OutputFolder = FileHelper.CreateDirectory(outputFolder);
                        }
                        string localFolder = Get<string>(string.Format("FileReaderLocalFolder_{0}", i));
                        readerModel.LocalFolder = null;
                        if (localFolder != null)
                        {
                            readerModel.LocalFolder = FileHelper.CreateDirectory(localFolder);
                        }

                        readerModel.OutputCsv = Get<bool>(string.Format("FileReaderOutputCsv_{0}", i));
                        readerModel.EncryptBody = Get<bool>(string.Format("FileReaderEncryptBody_{0}", i));
                        readerModel.HasSftpHeader = Get<bool>(string.Format("FileReaderHasSftpHeader_{0}", i));


                        readerModel.EncryptionKey = Get<string>(string.Format("FileReaderEncryptionKey_{0}", i));
                        readerModel.EncryptionIV = Get<string>(string.Format("FileReaderEncryptionIV_{0}", i));

                        
                        readerModel.NldnFormat = Get(string.Format("FileReaderNldnFormat_{0}", i), false);
                        readerModel.WmoHeaderId = Get(string.Format("FileReaderWmoHeaderId_{0}", i), string.Empty);
                        

                        string boundingBoxStr = Get<string>(string.Format("FileReaderBoundingBox_{0}", i));
                        if (!string.IsNullOrEmpty(boundingBoxStr))
                        {
                            string[] arr = boundingBoxStr.Split(',');
                            BoundingBox boundingBox = new BoundingBox(Convert.ToDouble(arr[0]), Convert.ToDouble(arr[1]), Convert.ToDouble(arr[2]), Convert.ToDouble(arr[3]));
                            readerModel.BoundingBox = boundingBox;
                        }
                        else
                        {
                            readerModel.BoundingBox = null;
                        }

                        string exclusionBoundingBoxStr = Get<string>(string.Format("FileReaderExclusionBoundingBox_{0}", i));
                        if (exclusionBoundingBoxStr != null)
                        {
                            string[] arr = exclusionBoundingBoxStr.Split(',');
                            BoundingBox exclusionBoundingBox = new BoundingBox(Convert.ToDouble(arr[0]), Convert.ToDouble(arr[1]), Convert.ToDouble(arr[2]), Convert.ToDouble(arr[3]));
                            readerModel.ExclusionBoundingBox = exclusionBoundingBox;
                        }
                        else
                        {
                            readerModel.ExclusionBoundingBox = null;
                        }

                        string uploadTypeStr = Get<string>(string.Format("FileReaderUploadType_{0}", i));
                        if (uploadTypeStr != null)
                        {
                            if (uploadTypeStr.ToLower() == "ftp")
                            {
                                readerModel.UploadType = UploadType.Ftp;
                            }
                            else if (uploadTypeStr.ToLower() == "sftp")
                            {
                                readerModel.UploadType = UploadType.SFtp;
                            }
                            else if (uploadTypeStr.ToLower() == "ftps")
                            {
                                readerModel.UploadType = UploadType.FTPS;
                            }
                            else if (uploadTypeStr.ToLower() == "s3")
                            {
                                readerModel.UploadType = UploadType.S3;
                            }
                            else
                            {
                                readerModel.UploadType = UploadType.Local;
                            }
                        }
                        else
                        {
                            readerModel.UploadType = UploadType.Local;
                        }

                        readerModel.UploadServer = Get<string>(string.Format("FileReaderUploadServer_{0}", i));
                        readerModel.UploadPath = Get<string>(string.Format("FileReaderUploadPath_{0}", i));
                        readerModel.UploadUserName = Get<string>(string.Format("FileReaderUploadUserName_{0}", i));
                        readerModel.UploadPassword = Get<string>(string.Format("FileReaderUploadPassword_{0}", i));

                        readerModel.RetryCount = Get<int>(string.Format("FileReaderRetryCount_{0}", i), 5);
                        readerModel.RetryInitialIntervalSeconds = Get<int>(string.Format("FileReaderRetryInitialIntervalSeconds_{0}", i), 1);
                        readerModel.RetryIncrementSeconds = Get<int>(string.Format("FileReaderRetryIncrementSeconds_{0}", i), 2);

                        readerModel.FlashSolutionColumn = Get<bool>(string.Format("FileReaderFlashSolutionColumn_{0}", i), true);

                        listFileReaders.Add(readerModel);
                    }

                    if (listFileReaders.Count > 0)
                    {
                        fileReaders = new ReadOnlyCollection<FileReaderModel>(listFileReaders);
                    }
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogError("Error loading file reader configs", ex);
            }
            return fileReaders;
        }

        public static string FileReaderPickupFolder
        {
            get { return Get("FileReaderPickupFolder", string.Empty); }
        }

        public static string FileReaderFailedFolder
        {
            get { return Get("FileReaderFailedFolder", string.Empty); }
        }

        public static bool MasterServer
        {
            get { return Get("MasterServer", false); }
        }

        public static string MasterServerEcvUrl
        {
            get { return Get("MasterServerEcvUrl", string.Empty); }
        }

        public static int MasterServerEcvTimeoutSeconds
        {
            get { return Get("MasterServerEcvTimeoutSeconds", 10); }
        }

        public static int SlaveServerDelaySeconds
        {
            get { return Get("SlaveServerDelaySeconds", 30); }
        }

        public static int FileCleanupIntervalHours
        {
            get { return Get("FileCleanupIntervalHours", 4); }
        }

        public static int FileCleanupKeepHours
        {
            get { return Get("FileCleanupKeepHours", 12); }
        }

        public static string S3Bucket
        {
            get { return Get("S3Bucket", string.Empty); }
        }

        public static string S3PathPrefix
        {
            get { return Get("S3PathPrefix", string.Empty); }
        }

        public static string S3StagingFolder
        {
            get { return Get("S3StagingFolder", string.Empty); }
        }

        public static string SnsTopicName
        {
            get { return Get("SnsTopicName", string.Empty); }
        }

        public static string DynamoMonitoringTable
        {
            get { return Get("DynamoMonitoringTable", string.Empty); }
        }

        public static int LxFileMinutes
        {
            get { return Get("LxFileMinutes", 5); }
        }

        public static bool DynamoMonitoring
        {
            get { return Get("DynamoMonitoring", true); }
        }
    }
}
