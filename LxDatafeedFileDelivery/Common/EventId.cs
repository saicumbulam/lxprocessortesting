﻿
namespace LxDatafeedFileDelivery.Common
{
    public static class EventId
    {
        public const ushort Program = 100;
        
        public static class MonitoringHandler
        {
            public static ushort StartOpsHttp = 1850;
            public static ushort StopOpsHttp = 1851;
            public static ushort GetFileCount = 1852;
            public static ushort LoadMonitorFile = 1853;
            public static ushort BuildMonitorHtml = 1854;
            public static ushort GetDatafeedStatus = 1855;
        }
    }
}

