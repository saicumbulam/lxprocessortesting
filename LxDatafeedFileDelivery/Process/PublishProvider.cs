﻿using System;
using System.Threading.Tasks;
using Aws.Core.Utilities;
using En.Ods.Lib.Common.AwsPublisher;
using En.Ods.Lib.Common.Ecv;
using En.Ods.Lib.Common.Interface.Publisher;
using En.Ods.Lib.Common.Serialization;
using LxDatafeedFileDelivery.Common;

namespace LxDatafeedFileDelivery.Process
{
    public class PublishProvider
    {
        private static IPublisherTopic _topic;
        public static string ErrorMsg;

        public static void Initialize()
        {
            try
            {
                IOdsPublisher publisher = new AwsPublisherFactory().GetPublisher(new JsonOdsValue());
                _topic = publisher.CreateTopic(Config.SnsTopicName);
            }
            catch (Exception ex)
            {
                string errorMsg = string.Format("Failed to get topic {0}.", Config.SnsTopicName);
                ErrorMsg = errorMsg + ex;
                EventManager.LogError(errorMsg, ex);
            }

        }

        internal static async Task<bool> TrySendNewFileMessage(string newFileMessage, string guid)
        {
            bool returnStatus = false;

            try
            {
                var fileEvent = new LightningDataFileEvent
                {
                    Subject = "LightningDataFile.Updated",
                    DownloadUrl = newFileMessage,
                    ModifiedDateTimeUtc = DateTime.UtcNow,
                    UniqueId = guid
                };
                var message = JsonOdsValue.GetValue(fileEvent);
                var publishMessage = await _topic.PublishAsync(message).ConfigureAwait(false);
                ErrorMsg = null;
                returnStatus = true;
            }
            catch (Exception ex)
            {
                string errorMsg = "Failed to write message to topic: " + Config.SnsTopicName;
                ErrorMsg = errorMsg + ex;
                EventManager.LogError(errorMsg, ex);
            }
            return returnStatus;
        }

        public static MonitorStatus GetStatus()
        {
            return new MonitorStatus
            {
                MonitorMessage = ErrorMsg ?? string.Format("Publishing to {0}", _topic.TopicId),
                MonitorName = "StationMetaDataReportPublisher",
                MonitorStatusType = ErrorMsg != null ? EMonitorStatusType.Failed : EMonitorStatusType.Success
            };
        }
    }

    public class LightningDataFileEvent
    {
        public string Subject { get; set; }
        public DateTime ModifiedDateTimeUtc { get; set; }
        public string DownloadUrl { get; set; }
        public string UniqueId { get; set; }
    }
}
