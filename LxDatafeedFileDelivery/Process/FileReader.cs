﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Aws.Core.Utilities;
using CsvHelper;
using LxCommon;
using LxCommon.DataFeed;
using LxCommon.Models;
using LxDatafeedFileDelivery.Common;
using LxDatafeedFileDelivery.Model;
using Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Renci.SshNet;

namespace LxDatafeedFileDelivery.Process
{
    public class FileReader
    {
        private Timer _importTimer;

        private List<FileReaderModel> _fileReaderModels = null;

        private FileInfo _lastFileProcessed;
        private Queue<string> _lastFilesCreated = new Queue<string>();
        private double _lastFileProcessTime;
        private readonly int _queueSize = Config.FileReaderModels.Count * 2; // The queue will hold two minute-files per model

        private bool _masterError = false;
        private bool _minuteProcessed = false;

        private int _failedConsecutiveMinutes;
        private bool _processResult;

        private HttpClient _httpClient;

        private readonly FileWatcher _fileWatcher;
        private CancellationTokenSource _cancellationTokenSource;
        private readonly ConcurrentDictionary<string, FileInfo> _fileTracker;

        private readonly ReaderWriterLockSlim _fileTrackerLock;

        private const int MaxLockTries = 5;
        private const int MaxLockWaitMillseconds = 2;

        internal class LxCsvHeader {
            public string Lightning_Time_String { get; set; }
            public string Latitude { get; set; }
            public string Longitude { get; set; }
            public string Height { get; set; }
            public string Flash_Type { get; set; }
            public string Amplitude { get; set; }
            public string Flash_Solution { get; set; }
            public string Confidence { get; set; }   
        }

        internal class LxCsvHeaderLite
        {
            public string Lightning_Time_String { get; set; }
            public string Latitude { get; set; }
            public string Longitude { get; set; }
            public string Height { get; set; }
            public string Flash_Type { get; set; }
            public string Amplitude { get; set; }
            public string Confidence { get; set; }
        }

        public FileReader(string pickupFolder, ReadOnlyCollection<FileReaderModel> fileReaderModels)
        {
            _fileReaderModels = new List<FileReaderModel>(fileReaderModels);
            _httpClient = new HttpClient() {Timeout = new TimeSpan(0, 0, Config.MasterServerEcvTimeoutSeconds)};

            _fileWatcher = new FileWatcher(pickupFolder, "*.txt");

            _fileTracker = new ConcurrentDictionary<string, FileInfo>();

            _fileTrackerLock = new ReaderWriterLockSlim();
        }

        public void Start()
        {
            if (_fileWatcher != null)
            {
                _fileWatcher.FileReady += OnFileReady;
                _fileWatcher.Start();
            }

            if (_cancellationTokenSource?.Token != null)
            {
                _cancellationTokenSource.Cancel();
                _cancellationTokenSource.Dispose();
            }

            _cancellationTokenSource = new CancellationTokenSource();
        }

        public void Stop()
        {
            if (_importTimer != null)
            {
                _importTimer.Dispose();
            }

            if (_fileWatcher != null)
            {
                _fileWatcher.Stop();
                _fileWatcher.FileReady -= OnFileReady;
            }

            _cancellationTokenSource.Cancel();
        }

        private void OnFileReady(object sender, FileReadyEventArgs e)
        {

            _cancellationTokenSource?.Token.ThrowIfCancellationRequested();

            if (e?.File == null || !e.File.Exists) return;

            EventManager.LogDebug($"FileReader.OnFileReady() - Event raised. File: {e.FullName}");

            var readLockHeld = false;
            var writeLockHeld = false;

            try
            {
                var invalidFile = false;
                var doesNotContainKey = false;

                var processFile = false;
                var tries = 0;

                while (!(readLockHeld = _fileTrackerLock.TryEnterReadLock(MaxLockWaitMillseconds)) && tries < MaxLockTries)
                    tries++;

                if (readLockHeld)
                {
                    doesNotContainKey = _fileTracker.IsEmpty || !_fileTracker.ContainsKey(e.FullName);

                    _fileTrackerLock.ExitReadLock();
                    readLockHeld = false;
                }
                else
                {
                    EventManager.LogWarning($"FileReader.OnFileReady() - Failed to obtain read lock. Moving file to failed file folder. File: {e.FullName}");
                    MoveToFailedFolder(e.File);
                }

                if (doesNotContainKey)
                {
                    tries = 0;

                    while (!(writeLockHeld = _fileTrackerLock.TryEnterWriteLock(MaxLockWaitMillseconds)) && tries < MaxLockTries)
                        tries++;

                    if (writeLockHeld)
                    {
                        _fileTracker.AddOrUpdate(e.FullName, e.File, (key, existingValue) => e.File);
                        processFile = true;

                        _fileTrackerLock.ExitWriteLock();
                        writeLockHeld = false;
                    }
                    else
                    {
                        EventManager.LogWarning($"FileReader.OnFileReady() - Failed to obtain write lock. Moving file to failed file folder. File: {e.FullName}");
                    }
                }

                if (processFile)
                {
                    _cancellationTokenSource?.Token.ThrowIfCancellationRequested();
                    EventManager.LogDebug($"FileReader.OnFileReady() - Processing file. File: {e.FullName}");

                    Process(e.File);

                    _cancellationTokenSource?.Token.ThrowIfCancellationRequested();

                    tries = 0;
                    while (!(writeLockHeld = _fileTrackerLock.TryEnterWriteLock(MaxLockWaitMillseconds)) && tries < MaxLockTries)
                        tries++;

                    if (writeLockHeld)
                    {
                        FileInfo junkFile;
                        _fileTracker.TryRemove(e.FullName, out junkFile);

                        _fileTrackerLock.ExitWriteLock();
                        writeLockHeld = false;
                    }
                    else
                    {
                        EventManager.LogWarning($"FileReader.OnFileReady() - Failed to obtain second write lock. File: {e.FullName}");
                    }
                }
                else
                {
                    EventManager.LogDebug($"FileReader.OnFileReady() - Skipping file as it's already in progress. File: {e.FullName}");
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError($"FileReader.OnFileReady() - Caught exception while processing file. Moving file to failed folder. File: {e.FullName}, Error: {ex.Message}", ex);

                if (e?.File != null && e.File.Exists)
                    MoveToFailedFolder(e.File);
            }
            finally
            {
                if (readLockHeld) _fileTrackerLock.ExitReadLock();
                if (writeLockHeld) _fileTrackerLock.ExitWriteLock();
            }
        }

        private void MoveToFailedFolder(FileInfo failedFile)
        {
            if (failedFile != null && failedFile.Exists)
            {
                try
                {
                    var destinationFile = Path.Combine(Config.FileReaderFailedFolder, failedFile.Name);
                    failedFile.MoveTo(destinationFile);
                    EventManager.LogInfo($"FileReader.MoveToFailedFolder() - Moved file to failed flash folder. File: {failedFile.FullName}, Destination: {destinationFile}");
                }
                catch (Exception ex)
                {
                    EventManager.LogError($"FileReader.MoveToFailedFolder() - Caught exception while moving file to failed file folder. File: {failedFile.FullName}, Failed File Folder: {Config.FileReaderFailedFolder}, Error: {ex.Message}", ex);
                }
            }
        }

        private const short PacketLen = 26;
        private void Process(FileInfo fileInfo)
        {
            _processResult = true;

            string timeString = "      "; // 6 bytes in WMO header
            string fullPath = fileInfo.FullName;

            // Check last write timestamp, if older than 1 min, process
            try
            {
                // Measure time to process one file
                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();

                // Parse timestamp from file name
                Match match = Regex.Match(fileInfo.Name, @"\d{4}-\d{2}-(\d{2})T(\d{2})-(\d{2})");
                if (match.Success)
                {
                    string day = match.Groups[1].Value;
                    string hour = match.Groups[2].Value;
                    string minute = match.Groups[3].Value;
                    timeString = day + hour + minute;
                }

                foreach (var fileReaderModel in _fileReaderModels)
                {
                    // Create bin body
                    byte[] body;
                    byte[] output = {};
                    string outputFileName = null;
                    string localPath;

                    if (fileReaderModel.OutputCsv) // CSV
                    {
                        outputFileName = Path.GetFileNameWithoutExtension(fileInfo.Name) + "-.csv";
                        localPath = Path.Combine(fileReaderModel.LocalFolder.FullName, outputFileName);
                        if (File.Exists(fullPath))
                        {
                            // Create CSV and export to local folder (always)
                            output = CreateCsvFile(fileInfo, fileReaderModel.BoundingBox, localPath, fileReaderModel.FlashSolutionColumn);
                        }
                        else
                        {
                            // Create empty file in local folder
                            File.Create(localPath).Dispose();
                            output = new byte[0];
                        }
                    }
                    else if (fileReaderModel.NldnFormat)
                    {
                        try
                        {
                            outputFileName = fileReaderModel.Name + "_" + Path.GetFileNameWithoutExtension(fileInfo.Name) + ".bin";
                            output = NldnFormat.CreateOutput(fileInfo, fileReaderModel, timeString);
                            localPath = Path.Combine(fileReaderModel.LocalFolder.FullName, outputFileName);
                            File.WriteAllBytes(localPath, output);
                        }
                        catch(Exception ex)
                        {
                            if (outputFileName == null) outputFileName = string.Empty;
                            string name = fileReaderModel.Name;
                            if (name == null) name = string.Empty;
                            AppLogger.LogError(string.Format("Error creating output for {0} file {1} ", name, outputFileName), ex);
                        }
                    }
                    else // Binary
                    {
                        if (File.Exists(fullPath))
                        {
                            body = CreateBinBody(fileInfo, fileReaderModel.BoundingBox);
                        }
                        else
                        {
                            body = new byte[0];
                        }

                        byte[] headerBytes;
                        if (fileReaderModel.HasSftpHeader)
                        {
                            // With sFTP header
                            long length;
                            if (fileReaderModel.EncryptBody)
                            {
                                // Round up to mulitple of 128 bits = 16 bytes (block size)
                                length = (body.Length / 16 + 1) * 16;
                            }
                            else
                            {
                                length = body.Length;
                            }

                            string headerStr = string.Format("****{0}**** SFPA42 KWBC {1}   ",
                                (length + 21).ToString().PadLeft(10, '0'),
                                timeString); // 21 = WMO headers. sFTP header is not included
                            headerBytes = Encoding.ASCII.GetBytes(headerStr);
                            headerBytes[18] = 10;
                            headerBytes[37] = 13;
                            headerBytes[38] = 13;
                            headerBytes[39] = 10;
                        }
                        else
                        {
                            // Without sFTP header
                            string headerStr = string.Format("SFPA42 KWBC {0}   ",
                                timeString);
                            headerBytes = Encoding.ASCII.GetBytes(headerStr);
                            headerBytes[18] = 13;
                            headerBytes[19] = 13;
                            headerBytes[20] = 10;
                        }

                        if (fileReaderModel.EncryptBody)
                        {
                            byte[] encrypted = EncryptBody(body, fileReaderModel.EncryptionKey, fileReaderModel.EncryptionIV);
                            output = new byte[encrypted.Length + headerBytes.Length];
                            Array.Copy(headerBytes, output, headerBytes.Length);
                            Array.Copy(encrypted, 0, output, headerBytes.Length, encrypted.Length);
                        }
                        else
                        {
                            // Not encrypted
                            output = new byte[body.Length + headerBytes.Length];
                            Array.Copy(headerBytes, output, headerBytes.Length);
                            Array.Copy(body, 0, output, headerBytes.Length, body.Length);
                        }

                        // Export to local folder (always)
                        outputFileName = Path.GetFileNameWithoutExtension(fileInfo.Name) + "-.bin";
                        localPath = Path.Combine(fileReaderModel.LocalFolder.FullName, outputFileName);
                        File.WriteAllBytes(localPath, output);
                    }

                    // Export to output folder (local/ftp/sftp)
                    if (Config.MasterServer)
                    {
                        // Master
                        bool res = WriteToOutput(fileReaderModel, outputFileName, output);
                        _processResult = _processResult && res;
                        if (_lastFilesCreated.Count == _queueSize)
                        {
                            _lastFilesCreated.Dequeue(); // remove oldest one
                        }
                        _lastFilesCreated.Enqueue(outputFileName);
                    }
                    else
                    {
                        // Slave
                        // Check master ECV first
                        if (CheckMasterEcv(outputFileName))
                        {
                            // Already created by master, do nothing
                            if (_masterError)
                            {
                                _masterError = false;
                                AppLogger.LogWarning("Master output resumed", null);
                            }
                        }
                        else
                        {
                            // Export
                            bool res = WriteToOutput(fileReaderModel, outputFileName, output);
                            _processResult = _processResult && res;
                            if (_lastFilesCreated.Count == _queueSize)
                            {
                                _lastFilesCreated.Dequeue(); // remove oldest one
                            }
                            _lastFilesCreated.Enqueue(outputFileName);
                            _masterError = true;

                            // Log
                            AppLogger.LogWarning(string.Format("Master output not working; Slave output to {0}: {1}", fileReaderModel.UploadType, outputFileName), null);
                        }
                    }
                }

                _lastFileProcessed = fileInfo;
                _lastFileProcessTime = Math.Round(stopWatch.Elapsed.TotalSeconds, 7);
                stopWatch.Stop();
                AppLogger.LogMessage(string.Format("Total time elapsed to process file {0} (size: {1}) = {2}", fileInfo.Name, File.Exists(fullPath) ? fileInfo.Length : 0, _lastFileProcessTime));

                //Delete source
                fileInfo.Delete();
                if (!_processResult)
                {
                    _failedConsecutiveMinutes++;
                }
                else
                {
                    _failedConsecutiveMinutes = 0;
                }
                _minuteProcessed = true;
            }
            catch (Exception ex)
            {
                AppLogger.LogError("Error processing file " + fileInfo.Name, ex);

                //Move to failed folder
                if (!Directory.Exists(Config.FileReaderFailedFolder))
                {
                    Directory.CreateDirectory(Config.FileReaderFailedFolder);
                }
                if (File.Exists(fullPath))
                {
                    fileInfo.CopyTo(Path.Combine(Config.FileReaderFailedFolder, fileInfo.Name), true);
                    fileInfo.Delete();
                }
            }
        }

        private bool WriteToOutput(FileReaderModel fileReaderModel, string outputFileName, byte[] output)
        {
            var retryStrategy = new Incremental(fileReaderModel.RetryCount, TimeSpan.FromSeconds(fileReaderModel.RetryInitialIntervalSeconds), 
                TimeSpan.FromSeconds(fileReaderModel.RetryIncrementSeconds));
            var retryPolicy = new RetryPolicy<AllExceptionsAsTransient>(retryStrategy);

            retryPolicy.Retrying += (sender, e) =>
            {
                var frm = fileReaderModel;
                var ofn = outputFileName;
                var tgt = frm.UploadType == UploadType.Local ? frm.OutputFolder.FullName : frm.UploadServer;
                AppLogger.LogWarning($"Retry - File:{ofn}, Count:{e.CurrentRetryCount}, Delay:{e.Delay}, Method:{frm.UploadType}, Destination:{tgt}", e.LastException);
            };


            try
            {
                if (fileReaderModel.UploadType == UploadType.Local)
                {
                    string outputPath = Path.Combine(fileReaderModel.OutputFolder.FullName, outputFileName);
                    
                    retryPolicy.ExecuteAction(() =>
                        {
                            File.WriteAllBytes(outputPath, output);
                        });
                }
                else if (fileReaderModel.UploadType == UploadType.Ftp)
                {

                    retryPolicy.ExecuteAction(() =>
                    {
                        FtpWebRequest request = (FtpWebRequest)WebRequest.Create(new Uri(string.Format(@"ftp://{0}/{1}/{2}", fileReaderModel.UploadServer, fileReaderModel.UploadPath, outputFileName)));
                        request.Method = WebRequestMethods.Ftp.UploadFile;
                        request.Credentials = new NetworkCredential(fileReaderModel.UploadUserName, fileReaderModel.UploadPassword);

                        using (Stream requestStream = request.GetRequestStream())
                        {
                            requestStream.Write(output, 0, output.Length);
                            requestStream.Close();
                        }
                    });
                }
                else if (fileReaderModel.UploadType == UploadType.SFtp)
                {
                    retryPolicy.ExecuteAction(() =>
                    {
                        string privateKeyFileName = fileReaderModel.UploadPassword; // use FileReaderUploadPassword config to store private key file path
                        PrivateKeyFile pkf = new PrivateKeyFile(privateKeyFileName);
                        using (var sftpClient = new SftpClient(fileReaderModel.UploadServer, fileReaderModel.UploadUserName, pkf))
                        {
                            sftpClient.Connect();
                            sftpClient.WriteAllBytes(Path.Combine(fileReaderModel.UploadPath, outputFileName).Replace('\\', '/'), output);
                            sftpClient.Disconnect();
                        }
                    });
                }
                else if (fileReaderModel.UploadType == UploadType.FTPS)
                {
                    retryPolicy.ExecuteAction(() =>
                    {
                        FtpWebRequest request = (FtpWebRequest)WebRequest.Create(new Uri(string.Format(@"ftp://{0}/{1}/{2}", fileReaderModel.UploadServer, fileReaderModel.UploadPath, outputFileName)));
                        request.EnableSsl = true;
                        request.Method = WebRequestMethods.Ftp.UploadFile;
                        request.Credentials = new NetworkCredential(fileReaderModel.UploadUserName, fileReaderModel.UploadPassword);

                        using (Stream requestStream = request.GetRequestStream())
                        {
                            requestStream.Write(output, 0, output.Length);
                            requestStream.Close();
                        }
                    });
                    
                }
                else if (fileReaderModel.UploadType == UploadType.S3)
                {
                    // Write to output folder. Then watcher will process when there are enough
                    string outputPath = Path.Combine(fileReaderModel.OutputFolder.FullName, outputFileName);

                    retryPolicy.ExecuteAction(() =>
                        {
                            File.WriteAllBytes(outputPath, output);
                        });
                }
                return true;
            }
            catch (Exception ex)
            {
                var tgt = fileReaderModel.UploadType == UploadType.Local ? fileReaderModel.OutputFolder?.FullName : fileReaderModel.UploadServer;
                AppLogger.LogError($"Error creating out file {outputFileName} for upload type {fileReaderModel.UploadType} with destination of {tgt}", ex);
                return false;
            }

        }
        
        private class AllExceptionsAsTransient : ITransientErrorDetectionStrategy
        {
            public bool IsTransient(Exception ex)
            {
                return true;
            }
        }

        private bool CheckMasterEcv(string outputFileName)
        {
            // return false if master ECV does not have outputFileName or time out
            try
            {
                string url = Config.MasterServerEcvUrl;
                string json = _httpClient.GetStringAsync(url).Result;
                JObject jo = JObject.Parse(json);
                JToken token = jo.SelectToken("lastFilesCreated", false);
                List<string> list = JsonConvert.DeserializeObject<List<string>>(token.ToString());

                return list.Contains(outputFileName);
            }
            catch (Exception ex)
            {
                if (ex.InnerException.GetType() == typeof(TaskCanceledException))
                {
                    // Timeout
                    AppLogger.LogError("CheckMasterEcv times out: " + outputFileName, ex);
                    return false;
                }
                else
                {
                    AppLogger.LogError("CheckMasterEcv failed: " + outputFileName, ex);
                    return false;
                }
            }
        }

        private byte[] CreateBinBody(FileInfo fileInfo, BoundingBox boundingBox)
        {
            string[] flashJson = File.ReadAllLines(fileInfo.FullName);

            List<byte> bodyByteList = new List<byte>();

            List<byte> comboPacketByteList;
            foreach (string line in flashJson)
            {
                comboPacketByteList = new List<byte>();
                // Deserialize
                Flash flash;
                try
                {
                    flash = JsonConvert.DeserializeObject<Flash>(line);
                }
                catch (Exception ex)
                {
                    AppLogger.LogWarning(string.Format("Failed to deserialize \"{0}\" in {1}", line, fileInfo.FullName), ex);
                    continue;
                }

                if (boundingBox != null && !boundingBox.Contains(flash.Longitude, flash.Latitude))
                {
                    continue;
                }

                // Create combo packet

                // Flash
                byte[] packet = new byte[PacketLen];
                packet[Client.PositionOffset.NumberOfBytes] = Convert.ToByte(PacketLen);

                // Change flash type 40 to 0
                if (flash.Type == FlashType.FlashTypeGlobalCG)
                {
                    flash.Type = FlashType.FlashTypeCG;
                }
                packet[Client.PositionOffset.LxClassificationType] = (byte)flash.Type;

                TimeSpan ts = TimeSvc.GetUnixTimeSpan(DateTime.Parse(flash.TimeStamp));

                // Add Time & Milli's: when adding time we must remove the milli's (via Math.Floor()) so that 
                // the seconds aren't rounded up... we already capture the milli's in another 2 bytes anyway so this is safe.
                IntegerConverter.WriteUIntToBufferAtOffset(Convert.ToUInt32(Math.Floor(ts.TotalSeconds)), packet, Client.PositionOffset.Time, true);
                IntegerConverter.WriteUShortToBufferAtOffset(Convert.ToUInt16(ts.Milliseconds), packet, Client.PositionOffset.Millisecond, true);

                // Add Lat/Lon
                int latitude = (int)(flash.Latitude * Math.Pow(10, 7));
                IntegerConverter.WriteIntToBufferAtOffset(latitude, packet, Client.PositionOffset.Latitude, true);

                int longitude = (int)(flash.Longitude * Math.Pow(10, 7));
                IntegerConverter.WriteIntToBufferAtOffset(longitude, packet, Client.PositionOffset.Longitude, true);

                // Add Amplitude
                IntegerConverter.WriteIntToBufferAtOffset(Convert.ToInt32(flash.Amplitude), packet, Client.PositionOffset.Amplitude, true);

                // Add IcHeight
                IntegerConverter.WriteUShortToBufferAtOffset(GetIcHeight(flash), packet, Client.PositionOffset.IcHeight, true);

                // Add NumberOfSensors
                if (flash.NumberSensors == 0)
                {
                    // Count unique sensors from portions
                    HashSet<string> uniqueSetOfSensorIds = new HashSet<string>();
                    foreach (Portion portion in flash.Portions)
                    {
                        if (portion.StationOffsets != null)
                        {
                            foreach (string sensor in portion.StationOffsets.Keys)
                            {
                                uniqueSetOfSensorIds.Add(sensor);
                            }
                        }
                    }
                    packet[Client.PositionOffset.NumberOfSensors] = (byte)uniqueSetOfSensorIds.Count;
                }
                else
                {
                    packet[Client.PositionOffset.NumberOfSensors] = (byte)flash.NumberSensors;
                }

                // Add Multiplicity
                packet[Client.PositionOffset.Multiplicity] = GetMultiplicity(flash);

                packet[Client.PositionOffset.Checksum] = CreateChecksum(PacketLen, packet);

                // Append to ComboPacket
                comboPacketByteList.AddRange(packet);

                // Portions (Pulses)
                foreach (Portion portion in flash.Portions)
                {
                    packet = new byte[PacketLen];
                    packet[Client.PositionOffset.NumberOfBytes] = Convert.ToByte(PacketLen);

                    // Change portion type 40 to 0
                    if (portion.Type == FlashType.FlashTypeGlobalCG)
                    {
                        portion.Type = FlashType.FlashTypeCG;
                    }
                    packet[Client.PositionOffset.LxClassificationType] = (byte)portion.Type;

                    ts = TimeSvc.GetUnixTimeSpan(DateTime.Parse(portion.TimeStamp));

                    // Add Time & Milli's: when adding time we must remove the milli's (via Math.Floor()) so that 
                    // the seconds aren't rounded up... we already capture the milli's in another 2 bytes anyway so this is safe.
                    IntegerConverter.WriteUIntToBufferAtOffset(Convert.ToUInt32(Math.Floor(ts.TotalSeconds)), packet, Client.PositionOffset.Time, true);
                    IntegerConverter.WriteUShortToBufferAtOffset(Convert.ToUInt16(ts.Milliseconds), packet, Client.PositionOffset.Millisecond, true);

                    // Add Lat/Lon
                    latitude = (int)(portion.Latitude * Math.Pow(10, 7));
                    IntegerConverter.WriteIntToBufferAtOffset(latitude, packet, Client.PositionOffset.Latitude, true);

                    longitude = (int)(portion.Longitude * Math.Pow(10, 7));
                    IntegerConverter.WriteIntToBufferAtOffset(longitude, packet, Client.PositionOffset.Longitude, true);

                    // Add Amplitude
                    IntegerConverter.WriteIntToBufferAtOffset(Convert.ToInt32(portion.Amplitude), packet, Client.PositionOffset.Amplitude, true);

                    // Add IcHeight
                    IntegerConverter.WriteUShortToBufferAtOffset(GetIcHeight(portion), packet, Client.PositionOffset.IcHeight, true);

                    // Add NumberOfSensors
                    packet[Client.PositionOffset.NumberOfSensors] = (byte)portion.StationOffsets.Count;

                    // Add Multiplicity
                    packet[Client.PositionOffset.Multiplicity] = GetMultiplicity(flash);

                    packet[Client.PositionOffset.Checksum] = CreateChecksum(PacketLen, packet);

                    // Append to Combo Packet
                    comboPacketByteList.AddRange(packet);
                }

                byte[] comboPacketBytes = new byte[comboPacketByteList.Count + 3];

                // Combo packet size (byte 0 - 1)
                IntegerConverter.WriteUShortToBufferAtOffset((ushort)(comboPacketBytes.Length), comboPacketBytes, 0, true);

                Array.Copy(comboPacketByteList.ToArray(), 0, comboPacketBytes, 2, comboPacketByteList.Count);

                // Combo Packet checksum
                comboPacketBytes[comboPacketBytes.Length-1] = CreateChecksum((short)(comboPacketBytes.Length), comboPacketBytes.ToArray());

                // One combo packet completed
                bodyByteList.AddRange(comboPacketBytes);
            }

            return bodyByteList.ToArray();
        }

        private byte[] CreateCsvFile(FileInfo fileInfo, BoundingBox boundingBox, string localPath, bool flashSolutionColumn)
        {
            string[] flashJson = File.ReadAllLines(fileInfo.FullName);

            using (var sw = new StreamWriter(localPath))
            {
                using (CsvWriter csvWriter = new CsvWriter(sw))
                {
                    if (flashSolutionColumn)
                    {
                        csvWriter.WriteHeader<LxCsvHeader>();
                    }
                    else
                    {
                        csvWriter.WriteHeader<LxCsvHeaderLite>();
                    }

                    foreach (string line in flashJson)
                    {
                        // Deserialize
                        Flash flash;
                        try
                        {
                            flash = JsonConvert.DeserializeObject<Flash>(line);
                        }
                        catch (Exception ex)
                        {
                            AppLogger.LogWarning(string.Format("Failed to deserialize \"{0}\" in {1}", line, fileInfo.FullName), ex);
                            continue;
                        }

                        if (boundingBox != null && !boundingBox.Contains(flash.Longitude, flash.Latitude))
                        {
                            continue;
                        }

                        csvWriter.WriteField(flash.TimeStamp);
                        csvWriter.WriteField(flash.Latitude);
                        csvWriter.WriteField(flash.Longitude);
                        csvWriter.WriteField(flash.Height);
                        // Change flash type 40 to 0
                        if (flash.Type == FlashType.FlashTypeGlobalCG)
                        {
                            flash.Type = FlashType.FlashTypeCG;
                        }
                        csvWriter.WriteField((int)flash.Type);
                        csvWriter.WriteField(flash.Amplitude);
                        if (flashSolutionColumn)
                        {
                            csvWriter.WriteField(flash.GetMetaDataDbString()); // Flash_Solution
                        }
                        csvWriter.WriteField(flash.Confidence);
                        csvWriter.NextRecord();
                    }
                }
            }

            return File.ReadAllBytes(localPath);
        }

        internal static byte[] EncryptBody(byte[] body, string encryptionKey, string encryptionIV)
        {
            byte[] encrypted;
            using (AesManaged aManaged = new AesManaged())
            {
                aManaged.BlockSize = 128;
                aManaged.KeySize = 128;
                aManaged.Key = Encoding.UTF8.GetBytes(encryptionKey);
                aManaged.IV = StringToByteArray(encryptionIV);
                using (var memoryStream = new MemoryStream())
                {
                    using (var cryptoStream = new CryptoStream(memoryStream, aManaged.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cryptoStream.Write(body, 0, body.Length);
                        cryptoStream.FlushFinalBlock();
                        encrypted = memoryStream.ToArray();
                    }
                }
            }
            return encrypted;
        }

        
        #region Inner Class: IntegerConverter
        public class IntegerConverter
        {
            #region Write to buffer methods
            public static void WriteUIntToBufferAtOffset(uint data, byte[] packageBytes, int offset, bool isMsbFirst)
            {
                ConvertBytes(BitConverter.GetBytes(data), packageBytes, offset, isMsbFirst);
            }

            public static void WriteUShortToBufferAtOffset(ushort data, byte[] packageBytes, int offset, bool isMsbFirst)
            {
                ConvertBytes(BitConverter.GetBytes(data), packageBytes, offset, isMsbFirst);
            }

            public static void WriteIntToBufferAtOffset(int data, byte[] packageBytes, int offset, bool isMsbFirst)
            {
                ConvertBytes(BitConverter.GetBytes(data), packageBytes, offset, isMsbFirst);
            }

            #region ConvertBytes
            private static void ConvertBytes(byte[] sourceBytes, byte[] destinationBytes, int offset, bool isMsbFirst)
            {
                if (isMsbFirst)
                {
                    Array.Reverse(sourceBytes);
                }
                sourceBytes.CopyTo(destinationBytes, offset);
            }
            #endregion
            #endregion

            #region Read from buffer methods
            public static uint? ReadUIntFromBufferAtOffset(byte[] packageBytes, int offset, bool isMsbFirst)
            {
                byte[] bytes = ReadBytes(packageBytes, offset, isMsbFirst, 4);
                return (bytes != null ? BitConverter.ToUInt32(bytes, 0) : (uint?)null);
            }

            public static ushort? ReadUShortFromBufferAtOffset(byte[] packageBytes, int offset, bool isMsbFirst)
            {
                byte[] bytes = ReadBytes(packageBytes, offset, isMsbFirst, 2);
                return (bytes != null ? BitConverter.ToUInt16(bytes, 0) : (ushort?)null);
            }

            public static int? ReadIntFromBufferAtOffset(byte[] packageBytes, int offset, bool isMsbFirst)
            {
                byte[] bytes = ReadBytes(packageBytes, offset, isMsbFirst, 4);
                return (bytes != null ? BitConverter.ToInt32(bytes, 0) : (int?)null);
            }

            #region ReadBytes
            private static byte[] ReadBytes(byte[] packageBytes, int offset, bool isMsbFirst, int dataTypeSize)
            {
                byte[] relevantBytes = null;

                if (packageBytes.Length > (offset + dataTypeSize - 1))
                {
                    relevantBytes = new byte[dataTypeSize];
                    Array.Copy(packageBytes, offset, relevantBytes, 0, dataTypeSize);
                    if (isMsbFirst)
                    {
                        Array.Reverse(relevantBytes);
                    }
                }

                return relevantBytes;
            }
            #endregion
            #endregion
        }
        #endregion

        #region GetIcHeight
        private static ushort GetIcHeight(Flash flashData)
        {
            return (flashData.Height > ushort.MaxValue ? ushort.MaxValue : (ushort)flashData.Height);
        }

        private static ushort GetIcHeight(Portion portion)
        {
            return (portion.Height > ushort.MaxValue ? ushort.MaxValue : (ushort)portion.Height);
        }
        #endregion

        #region GetMultiplicity
        private static byte GetMultiplicity(Flash flash)
        {
            return (flash.Portions.Count > byte.MaxValue ? byte.MaxValue : (byte)flash.Portions.Count);
        }
        #endregion

        #region CreateChecksum
        private static byte CreateChecksum(short packetLen, byte[] packageBytes)
        {
            byte bytesum = 0;
            for (int ct = 0; ct < (packetLen - 1); ++ct)
            {
                bytesum += packageBytes[ct];
            }
            return (byte)(256 - bytesum);
        }
        #endregion

        public List<FileReaderModel> FileReaderModels
        {
            get { return _fileReaderModels; }
        }

        public FileInfo LastFileProcessed
        {
            get { return _lastFileProcessed; }
        }

        public double LastFileProcessTime
        {
            get { return _lastFileProcessTime; }
        }

        public List<string> LastFilesCreated
        {
            get { return _lastFilesCreated.ToList(); }
        }

        public int FailedConsecutiveMinutes
        {
            get { return _failedConsecutiveMinutes; }
        }

        private static byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }
    }
}
