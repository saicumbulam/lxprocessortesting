﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using LxCommon;
using LxCommon.Models;
using LxDatafeedFileDelivery.Model;
using Newtonsoft.Json;

namespace LxDatafeedFileDelivery.Process
{
    public class NldnFormat
    {
        private static readonly byte[] Header = {
            0x53, //S
            0x46, //F
            0x55, //U
            0x53, //S
            0x39, //9
            0x39, //9
            0x20, //(sp)
            0x4B, //K
            0x57, //W
            0x42, //B
            0x43, //C
            0x20, //(sp)
            0x00, //Dm
            0x00, //Dl
            0x00, //Hm
            0x00, //Hl
            0x00, //Mm
            0x00, //Ml
            0x0D, // /r
            0x0D, // /r
            0x0D, // /r
            0x0A // /n
        };

        private static readonly DateTime UnixEpochUtc = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public static byte[] CreateOutput(FileInfo source, FileReaderModel fdr, string timeString)
        {
            byte[] body = CreateBody(source, fdr.BoundingBox, fdr.ExclusionBoundingBox);
            if (fdr.EncryptBody)
            {
                body = FileReader.EncryptBody(body, fdr.EncryptionKey, fdr.EncryptionIV);
            }
            
            byte[] header = CreateHeader(fdr.WmoHeaderId, timeString);
            byte[] output = new byte[header.Length + body.Length];
            Buffer.BlockCopy(header, 0, output, 0, header.Length);
            Buffer.BlockCopy(body, 0, output, header.Length, body.Length);
            return output;
        }

      
        private static byte[] CreateHeader(string wmoHeaderId, string timeString)
        {
            if (wmoHeaderId.Length != 6)
            {
                throw new ArgumentException("wmoHeaderId must be 6 characters long");
            }

            if (timeString.Length != 6)
            {
                throw new ArgumentException("timeString must be 6 characters long");
            }
            
            byte[] header = new byte[Header.Length];
            Buffer.BlockCopy(Header, 0, header, 0, Header.Length);

            byte[] ascii = Encoding.ASCII.GetBytes(wmoHeaderId);
            Buffer.BlockCopy(ascii, 0, header, 0, ascii.Length);

            //12 = Most significant digit of 2 digit UTC day represented as ASCII digit
            //13 = Least significant digit of 2 digit UTC day represented as ASCII digit
            //14 = Most significant digit of 2 digit UTC hour represented as ASCII digit
            //15 = Least significant digit of 2 digit UTC hour represented as ASCII digit
            //16 = Most significant digit of 2 digit UTC minute represented as ASCII digit
            //17 = Least significant digit of 2 digit UTC minute represented as ASCII digit
            
            ascii = Encoding.ASCII.GetBytes(timeString);
            Buffer.BlockCopy(ascii, 0, header, 12, ascii.Length);

            return header;
        }

        private static byte[] CreateBody(FileInfo source, BoundingBox boundingBox, BoundingBox exclusionBoundingBox)
        {
            byte[] ba = null;
            DateTime dt = DateTime.UtcNow;
            string[] lines = File.ReadAllLines(source.FullName);
            if (lines.Length > 0)
            {
                List<byte> body = new List<byte>();
                for (int i = 0; i < lines.Length; i++)
                {
                    Flash flash = JsonConvert.DeserializeObject<Flash>(lines[i]);
                    if (flash != null)
                    {
                        if (boundingBox == null || boundingBox.Contains(flash.Longitude, flash.Latitude))
                        {
                            if (exclusionBoundingBox == null || !exclusionBoundingBox.Contains(flash.Longitude, flash.Latitude))
                            {
                                //BYTE 31 Termination Character 
                                //0x00 if the element is NOT the last element in the record
                                //there was a previous record in this series so terminate the previous record
                                if (body.Count > 0)
                                {
                                    body.Add(0x00);
                                }

                                //BYTE 0-1 Record Type (16 bit Integer)
                                //record type value 0x00FF is used to indicate a lightning data record.
                                body.Add(0x00); body.Add(0xFF);

                                //BYTE 2-5 UTC Date/Time Hours/Minutes/Seconds (32 bit unsigned integer)
                                //BYTE 6-7 UTC Time Milliseconds (16 bit unsigned integer)
                                body.AddRange(GetTimeStamp(flash.TimeStamp, out dt));

                                //BYTE 8 - 11 Latitude(32 bit floating point)
                                byte[] lat = GetBigEndianBytes((float)Math.Round(flash.Latitude, 5));
                                //Swap16BitWordOrder(lat);
                                body.AddRange(lat);

                                //BYTE 12 - 15 Longitude(32 bit floating point)
                                byte[] lon = GetBigEndianBytes((float)Math.Round(flash.Longitude, 5));
                                //Swap16BitWordOrder(lon);
                                body.AddRange(lon);

                                //BYTE 16-17 Vendor Specific Data (16 bit Integer)
                                //This field is used for vendor - specific data relating to area of coverage used to report this lightning data. 
                                //Typical is use of Hexadecimal Values 0x0001 for CONUS source, and 0x0002 for long range source.
                                body.Add(0x00);
                                //This is sort of hack, we rely on the bounding box being CONUS and if we got here it is in the then it is the boudning box
                                if (boundingBox != null)
                                {
                                    body.Add(0x01);
                                }
                                else
                                {
                                    body.Add(0x02);
                                }
                                

                                //BYTE 18 - 19 Stroke Type (16 bit Integer)
                                //This field identifies whether this lightning stroke was cloud-to-ground or cloud-to-cloud.  
                                //Hexadecimal Values are 0x0000 for cloud-to-ground, 0x00FF for cloud - to - cloud, and 0xFFFF for Total Flash.
                                //We don't have a total flash, defaulting to cloud-to-ground
                                body.Add(0x00);
                                if (flash.Type == FlashType.FlashTypeIC || flash.Type == FlashType.FlashTypeGlobalIC)
                                {
                                    body.Add(0xFF);
                                }
                                else
                                {
                                    body.Add(0x00);
                                }

                                //BYTE 20 - 21 Stroke Kiloamps (16 bit Signed Integer)
                                //The stroke kiloamperes.  Note that in the case of a CG record type, the sign indicates direction.
                                //MIN: -254 MAX: 254
                                //To comply with the spec removing the sign from IC flashes and checking the min and max 
                                double kamp = flash.Amplitude / 1000;
                                if (flash.Type == FlashType.FlashTypeIC || flash.Type == FlashType.FlashTypeGlobalIC)
                                {
                                    kamp = Math.Abs(kamp);
                                }
                                if (kamp < -254) kamp = -254;
                                if (kamp > 254) kamp = 254;

                                short ka = (short)Math.Round(kamp, 0, MidpointRounding.AwayFromZero);
                                body.AddRange(GetBigEndianBytes(ka));

                                //BYTE 22 - 23 Stroke Multiplicity (16 bit Integer)
                                //The stroke multiplicity represented by an integer.
                                //MIN: 0 MAX: 15
                                //Capping our multiplicity, also setting a min of 1, not sure what a flash of multiplicity 0 would mean
                                int multi = flash.IcMultiplicity + flash.CgMultiplicity;
                                if (multi < 1) multi = 1;
                                if (multi > 15) multi = 15;
                                body.AddRange(GetBigEndianBytes((short)multi));

                                //BYTE 24 - 25 Stroke Duration (16 bit Integer)
                                //Stroke duration in microseconds.
                                //MIN: 0 MAX: 65,535
                                //convert from seconds to microseconds and capping
                                decimal dur = Math.Round(flash.DurationSeconds * 1000000, 0);
                                if (dur < 0) dur = 0;
                                if (dur > 65535) dur = 65535;
                                body.AddRange(GetBigEndianBytes((ushort)dur));

                                //BYTE 26 - 27 Reserved for future use (16 bit Integer)
                                //This field is reserved for future use and is ignored.
                                body.Add(0x00); body.Add(0x00);
                                //BYTE 28 - 30 Termination Characters 
                                //0x0D, 0x0D, 0x0A
                                //0x0D, 0x0D, 0x0A
                                body.Add(0x0D); body.Add(0x0D); body.Add(0x0A);
                            }
                        }
                    }
                }
                //BYTE 31 Termination Character 
                //0x03 if the element IS the last in the record.
                //we have written all the records so terminate the last record
                if (body.Count > 0)
                {
                    body.Add(0x03);
                }

                ba = body.ToArray();
            }

            if (ba.Length == null || ba.Length == 0)
            {
                //Create a keep alive record
                //A "Keep Alive" record shall of a two 8-bit byte entry, 0x0000, indicating keep-alive record type
                //followed by a four 8-bit byte termination sequence containing a 0x03 as the last entry. 
                ba = new byte[] { 0x00, 0x00, 0x0D, 0x0D, 0x0A, 0X03 };
            }

            return ba;
        }

        private static byte[] GetTimeStamp(string timeStamp, out DateTime dt)
        {
            dt = DateTime.UtcNow;
            if (DateTime.TryParse(timeStamp, out dt))
            {
                if (dt.Kind != DateTimeKind.Utc)
                {
                    dt = DateTime.SpecifyKind(dt, DateTimeKind.Utc);
                }
            }

            TimeSpan ts = dt - UnixEpochUtc;
            uint time = uint.MaxValue;
            if (ts.TotalSeconds < uint.MaxValue)
            {
                time = (uint)ts.TotalSeconds;
            }
            byte[] bt = GetBigEndianBytes(time);
            //Swap16BitWordOrder(bt);

            ushort milli = 999;
            if (dt.Millisecond < 999)
            {
                milli = (ushort)dt.Millisecond;
            }
            byte[] bm = GetBigEndianBytes(milli);

            byte[] ba = new byte[6];
            Buffer.BlockCopy(bt, 0, ba, 0, bt.Length);
            Buffer.BlockCopy(bm, 0, ba, bt.Length, bm.Length);

            return ba;
        }

        private static void Swap16BitWordOrder(byte[] ba)
        {
            if (ba.Length != 4)
            {
                throw new ArgumentException("byte array length must be 4 bytes");
            }

            byte b = ba[0];
            ba[0] = ba[2];
            ba[2] = b;

            b = ba[1];
            ba[1] = ba[3];
            ba[3] = b;
        }

        private static byte[] GetBigEndianBytes(ushort value)
        {
            byte[] ba = BitConverter.GetBytes(value);
            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(ba);
            }
            return ba;
        }

        private static byte[] GetBigEndianBytes(short value)
        {
            byte[] ba = BitConverter.GetBytes(value);
            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(ba);
            }
            return ba;
        }

        private static byte[] GetBigEndianBytes(uint value)
        {
            byte[] ba = BitConverter.GetBytes(value);
            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(ba);
            }
            return ba;
        }

        private static byte[] GetBigEndianBytes(float value)
        {
            byte[] ba = BitConverter.GetBytes(value);
            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(ba);
            }
            return ba;
        }

    }
}
