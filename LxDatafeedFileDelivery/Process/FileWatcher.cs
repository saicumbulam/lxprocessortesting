﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Aws.Core.Utilities;
using LxCommon.Utilities;

namespace LxDatafeedFileDelivery.Process
{
    public class FileWatcher : IDisposable
    {
        public delegate void FileReadyEventHandler(object sender, FileReadyEventArgs e);

        private readonly string _watchFolder;
        private readonly string _filePattern;
        private readonly DirectoryInfo _watchFolderInfo;
        private readonly ConcurrentDictionary<string, Tuple<FileInfo, DateTime>> _fileTracker;

        private readonly ReaderWriterLockSlim _fileTrackerLock;

        private const int MaxLockTries = 5;
        private const int MaxLockWaitMillseconds = 2;

        private FileSystemWatcher _watcher;
        private CancellationTokenSource _cancellationTokenSource;
        private FileReadyEventHandler _onFileReadyHandler;
        private bool _disposed;

        private bool _started;

        private Task _recurringFileTrackerCleanUpTask;

        public FileWatcher(string path, string filePattern)
        {
            _watchFolder = path;
            _filePattern = filePattern;

            _watchFolderInfo = new DirectoryInfo(_watchFolder);
            _fileTracker = new ConcurrentDictionary<string, Tuple<FileInfo, DateTime>>();
            _fileTrackerLock = new ReaderWriterLockSlim();
            _onFileReadyHandler = null;
            _disposed = false;
        }

        public void Start()
        {
            EventManager.LogInfo($"FileWatcher.Start() - Starting");

            if (_cancellationTokenSource?.Token != null)
            {
                _cancellationTokenSource.Cancel();
                _cancellationTokenSource.Dispose();
            }

            _cancellationTokenSource = new CancellationTokenSource();

            _recurringFileTrackerCleanUpTask = RecurringTask.Create(TimeSpan.FromMinutes(10), TimeSpan.FromMinutes(10), FileTrackerCleanUp, _cancellationTokenSource.Token);

            Task.Factory.StartNew(Process, _cancellationTokenSource.Token, TaskCreationOptions.LongRunning, TaskScheduler.Current);
            _started = true;

            EventManager.LogInfo($"FileWatcher.Start() - Started");
        }

        public void Stop()
        {
            EventManager.LogInfo($"FileWatcher.Stop() - Stopping");

            if (_watcher != null) _watcher.EnableRaisingEvents = false;

            _cancellationTokenSource?.Cancel();

            _fileTracker?.Clear();

            _started = false;

            EventManager.LogInfo($"FileWatcher.Stop() - Stopped");
        }


        public event FileReadyEventHandler FileReady
        {
            add { _onFileReadyHandler += value; }
            remove { if (_onFileReadyHandler != null) _onFileReadyHandler -= value; }
        }

        public void Dispose()
        {
            if (_disposed) return;

            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected void Dispose(bool disposing)
        {
            if (_disposed) return;

            try
            {
                if (disposing)
                {
                    if (_started) Stop();

                    _watcher?.Dispose();
                    _recurringFileTrackerCleanUpTask.Dispose();
                    _fileTrackerLock?.Dispose();
                    _cancellationTokenSource?.Dispose();

                    _onFileReadyHandler = null;
                }
            }
            finally
            {
                _disposed = true;
            }
        }

        private void Process()
        {
            var files = _watchFolderInfo.GetFiles(_filePattern, SearchOption.AllDirectories).Where(file => !_fileTracker.ContainsKey(file.FullName)).ToList();
            EventManager.LogDebug($"FileWatcher.Process() - Starting up. Processing files in watched folder. Folder: {_watchFolder}, Count: {files.Count}");

            foreach (var file in files)
            {
                _cancellationTokenSource.Token.ThrowIfCancellationRequested();

                var currentFile = file;
                ProcessFile(currentFile);
            }

            EventManager.LogDebug($"FileWatcher.Process() - Creating FileSystemWatcher. Folder: {_watchFolder}, File Pattern: {_filePattern}");
            _watcher = new FileSystemWatcher
            {
                Path = _watchFolder,
                NotifyFilter = NotifyFilters.FileName | NotifyFilters.LastWrite,
                Filter = _filePattern,
                EnableRaisingEvents = true
            };
            _watcher.Created += OnFileChanged;
            _watcher.Changed += OnFileChanged;
        }

        private void OnFileChanged(object source, FileSystemEventArgs e)
        {
            try
            {
                if (e.ChangeType == WatcherChangeTypes.Created || e.ChangeType == WatcherChangeTypes.Changed)
                    ProcessFile(new FileInfo(e.FullPath));
                else
                    EventManager.LogDebug($"FileWatcher.OnFileChanged - Ignoring unknown event type. File: {e.FullPath}, Event Type: {e.ChangeType}");
            }
            catch (Exception ex)
            {
                EventManager.LogError($"FileWatcher.OnFileChanged - Caught exception while processing event. File: {e.FullPath}, Error: {ex.Message}", ex);
            }
        }

        private async void ProcessFile(FileInfo file)
        {
            var readLockHeld = false;
            var writeLockHeld = false;

            EventManager.LogDebug($"FileWatcher.ProcessFile() - File processing requested. File: {file.FullName}");

            _cancellationTokenSource?.Token.ThrowIfCancellationRequested();

            if (!file.Exists)
            {
                EventManager.LogWarning($"FileWatcher.ProcessFile() - File requested to be processed, but it doesn't exist. File: {file.FullName}");
                return;
            }

            try
            {
                var tries = 0;
                var containsKey = false;

                while (!(readLockHeld = _fileTrackerLock.TryEnterReadLock(MaxLockWaitMillseconds)) && tries < MaxLockTries)
                    tries++;

                if (readLockHeld)
                {
                    containsKey = _fileTracker.ContainsKey(file.FullName);

                    _fileTrackerLock.ExitReadLock();
                    readLockHeld = false;
                }
                else
                {
                    EventManager.LogWarning($"FileWatcher.ProcessFile() - Failed to obtain read lock. Ignoring file. File: {file.FullName}");
                    return;
                }

                if (containsKey)
                {
                    EventManager.LogDebug($"FileWatcher.ProcessFile() - File requested to be processed, but already exists in the file tracker. File: {file.FullName}");
                    return;
                }

                tries = 0;
                while (!(writeLockHeld = _fileTrackerLock.TryEnterWriteLock(MaxLockWaitMillseconds)) && tries < MaxLockTries)
                    tries++;

                if (writeLockHeld)
                {
                    var tuple = new Tuple<FileInfo, DateTime>(file, DateTime.UtcNow);
                    _fileTracker.AddOrUpdate(file.FullName, tuple, (key, existingValue) => tuple);

                    _fileTrackerLock.ExitWriteLock();
                    writeLockHeld = false;
                }
                else
                {
                    EventManager.LogWarning($"FileWatcher.ProcessFile() - Failed to obtain write lock. Ignoring file. File: {file.FullName}");
                    return;
                }


                EventManager.LogDebug($"FileWatcher.ProcessFile() - Start processing file. File: {file.FullName}");

                var delayCount = 0;
                while (!IsFileReady(file) && delayCount < 10)
                {
                    _cancellationTokenSource?.Token.ThrowIfCancellationRequested();
                    await Task.Delay(TimeSpan.FromSeconds(30), _cancellationTokenSource?.Token ?? CancellationToken.None);
                    delayCount++;
                }

                if (File.Exists(file.FullName) && IsFileReady(file))
                {
                    EventManager.LogDebug($"FileWatcher.ProcessFile() - Raising FileReady event. File: {file.FullName}");
                    _onFileReadyHandler(this, new FileReadyEventArgs(new FileInfo(file.FullName)));
                }
                else
                {
                    tries = 0;
                    while (!(writeLockHeld = _fileTrackerLock.TryEnterWriteLock(MaxLockWaitMillseconds)) && tries < MaxLockTries)
                        tries++;

                    if (writeLockHeld)
                    {
                        var junkTuple = default(Tuple<FileInfo, DateTime>);
                        EventManager.LogWarning($"FileWatcher.ProcessFile() - File no longer exists or file still in use, so FileReady event will not be raised. File: {file.FullName}, Exists: {File.Exists(file.FullName)}", null);
                        _fileTracker.TryRemove(file.FullName, out junkTuple);

                        _fileTrackerLock.ExitWriteLock();
                        writeLockHeld = false;
                    }
                }

                EventManager.LogDebug($"FileWatcher.ProcessFile() - Finished processing file. File: {file.FullName}");

            }
            finally
            {
                if (readLockHeld) _fileTrackerLock.ExitReadLock();
                if (writeLockHeld) _fileTrackerLock.ExitWriteLock();
            }
        }

        private bool IsFileReady(FileInfo fileInfo)
        {
            try
            {
                using (var file = fileInfo.Open(FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    file.Close();
                    file.Dispose();
                    return true;
                }
            }
            catch (IOException)
            {
                return false;
            }
            catch (UnauthorizedAccessException ex)
            {
                EventManager.LogWarning($"FileWatcher.IsFileReady() - UnauthorizedAccessException. File: {fileInfo.FullName}, Error: {ex.Message}");
                return false;
            }
            catch (Exception ex)
            {
                EventManager.LogError($"FileWatcher.IsFileReady() - Caught exception. File: {fileInfo.FullName}, Error: {ex.Message}", ex);
                return false;
            }
        }

        private void FileTrackerCleanUp()
        {
            var writeLockHeld = false;
            var expiryTime = DateTime.UtcNow.AddMinutes(-10);

            EventManager.LogDebug($"FileWatcher.FileTrackerCleanUp() - Start cleaning expired entries from File Tracker. Expiry Date: {expiryTime}");

            try
            {
                var tries = 0;
                while (!(writeLockHeld = _fileTrackerLock.TryEnterWriteLock(MaxLockWaitMillseconds)) && tries < MaxLockTries)
                    tries++;

                if (writeLockHeld)
                {
                    foreach (var trackedFile in _fileTracker.Where(trackedFile => trackedFile.Value != null && trackedFile.Value.Item2 <= expiryTime))
                    {
                        EventManager.LogDebug($"FileWatcher.FileTrackerCleanUp() - Removing expired tracked file entry from File Tracker. File: {trackedFile.Key}, Time Added: {trackedFile.Value.Item2}");
                        var junkTuple = default(Tuple<FileInfo, DateTime>);
                        _fileTracker.TryRemove(trackedFile.Key, out junkTuple);
                    }
                }
                else
                {
                    EventManager.LogWarning($"FileWatcher.FileTrackerCleanUp() - Failed to obtain write lock. FileTracker will not be cleaned.");
                }
            }
            finally
            {
                if (writeLockHeld) _fileTrackerLock.ExitWriteLock();

                EventManager.LogDebug($"FileWatcher.FileTrackerCleanUp() - Finished cleaning expired entries from File Tracker.");
            }
        }
    }
}
