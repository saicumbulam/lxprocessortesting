﻿using System;
using System.IO;
using System.Threading;
using Aws.Core.Utilities;
using LxDatafeedFileDelivery.Common;

namespace LxDatafeedFileDelivery.Process
{
    public class FileCleaner
    {
        private readonly int _cleanupIntervalHours;
        private readonly int _cleanupKeepHours;
        private Timer _cleanupTimer;

        public FileCleaner(int cleanupIntervalHours, int cleanupKeepHours)
        {
            _cleanupIntervalHours = cleanupIntervalHours;
            _cleanupKeepHours = cleanupKeepHours;
        }

        public void Start()
        {
            _cleanupTimer = new Timer(Process, null, TimeSpan.FromMilliseconds(0), TimeSpan.FromHours(_cleanupIntervalHours));
        }

        public void Stop()
        {
            if (_cleanupTimer != null)
            {
                _cleanupTimer.Dispose();
            }
        }

        private void Process(object state)
        {
            foreach (var fileReaderModel in Config.FileReaderModels)
            {
                FileInfo[] files = fileReaderModel.LocalFolder.GetFiles();
                foreach (var fileInfo in files)
                {
                    // Clean files older than Config.FileCleanupKeepHours
                    if ((DateTime.UtcNow - fileInfo.CreationTimeUtc).TotalSeconds > new TimeSpan(_cleanupKeepHours, 0, 0).TotalSeconds)
                    {
                        try
                        {
                            fileInfo.Delete();
                        }
                        catch (Exception ex)
                        {
                            AppLogger.LogError(string.Format("Unable to delete file: {0}", fileInfo.FullName), ex);
                        }
                    }
                }
            }
        }
    }
}
