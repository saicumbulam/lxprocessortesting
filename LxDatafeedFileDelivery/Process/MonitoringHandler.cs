﻿using System;
using System.IO;
using LxDatafeedFileDelivery.Common;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using Aws.Core.Data.Common.Operations;
using Aws.Core.Utilities;

using LxDatafeedFileDelivery.Model;

namespace LxDatafeedFileDelivery.Process
{
    class MonitoringHandler
    {
        private FileReader _fileReader;

        public MonitoringHandler(FileReader fileReader)
        {
            _fileReader = fileReader;
        }

        public void Start()
        {
            try
            {
                OpsCommandHttpEndpointSvc.Start((cmd) => MonitorStatusCheck(cmd));
            }
            catch (Exception ex)
            {
                EventManager.LogError(0, "Unable to start ecv http endpoint", ex);
            }
        }

        private string MonitorStatusCheck(string cmd)
        {
            return GeneratePrtgStatus(_fileReader);
        }

        private string GeneratePrtgStatus(FileReader fileReader)
        {
            JObject j = new JObject();
            int count = 1;

            j.Add("isMaster", Config.MasterServer);
            j.Add("inputFolder", Config.FileReaderPickupFolder);
            j.Add("lastFileProcessed", fileReader.LastFileProcessed != null ? fileReader.LastFileProcessed.Name : null);
            j.Add("lastFileProcessTime", fileReader.LastFileProcessTime);

            foreach (FileReaderModel fileReaderModel in fileReader.FileReaderModels)
            {
                JObject j1 = new JObject();
                j1.Add(string.Format("outputFolder_{0}", count), fileReaderModel.OutputFolder.FullName);
                j1.Add(string.Format("boundingBox_{0}", count), fileReaderModel.BoundingBox != null ? JsonConvert.SerializeObject(fileReaderModel.BoundingBox) : null);

                j.Add(count.ToString(), j1);

                count++;
            }
            j.Add("lastFilesCreated", JToken.FromObject(fileReader.LastFilesCreated));
            j.Add("failedFolderFiles", Directory.GetFiles(Config.FileReaderFailedFolder).Length);

            int nwsFileCount = 0;
            int noaaFileCount = 0;
            foreach (FileReaderModel fileReaderModel in fileReader.FileReaderModels)
            {
                if (fileReaderModel.Name == "NWS")
                {
                    nwsFileCount += fileReaderModel.FilesCreatedLastTwoMinutes;
                }
                else if (fileReaderModel.Name == "NOAA")
                {
                    if (noaaFileCount == 0) // only check once
                    {
                        noaaFileCount += fileReaderModel.FilesCreatedLastTwoMinutes;
                    }
                }
            }
            j.Add("fileCreatedLastTwoMinutesNWS", nwsFileCount);
            j.Add("fileCreatedLastTwoMinutesNOAA", noaaFileCount);
            j.Add("serviceFailedConsecutiveMinutes", fileReader.FailedConsecutiveMinutes);

            return j.ToString();
        }

        public void Stop()
        {
            try
            {
                OpsCommandHttpEndpointSvc.Stop();
            }
            catch (Exception ex)
            {
                EventManager.LogError(1, "Unable to stop ecv http endpoint", ex);
            }

        }

    }
}
