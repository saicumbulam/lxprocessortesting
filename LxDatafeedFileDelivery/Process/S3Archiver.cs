﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Aws.Core.Utilities;
using En.Ods.Lib.Common.Dynamo;
using En.Ods.Lib.Common.Interface;
using En.Ods.Lib.Common.Serialization;
using LxCommon;
using LxDatafeedFileDelivery.Common;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace LxDatafeedFileDelivery.Process
{
    public class S3Archiver
    {
        private string _stagingFolder;
        private int _numFiles;

        private INoSql _noSql;
        private ICloudStorage _storage;
        private string _bucket;
        private const int PresignedUrlExpireMinutes = 5 * 24 * 60;

        private readonly FileWatcher _fileWatcher;

        private HttpClient _httpClient;
        private bool _masterError = false;

        public S3Archiver(string stagingFolder, int numFiles)
        {
            _stagingFolder = stagingFolder;
            _numFiles = numFiles;

            _noSql = new DynamoFactory().CreateProvider(new JsonOdsValue());
            _storage = new S3Factory().GetStorage(new JsonOdsValue());
            _bucket = Config.S3Bucket;

            _fileWatcher = new FileWatcher(stagingFolder, "*.csv");
            _httpClient = new HttpClient() { Timeout = new TimeSpan(0, 0, Config.MasterServerEcvTimeoutSeconds) };

            PublishProvider.Initialize();
        }

        public void Start()
        {
            if (_fileWatcher != null)
            {
                _fileWatcher.FileReady += OnFileReady;
                _fileWatcher.Start();
            }
        }

        public void Stop()
        {
            if (_fileWatcher != null)
            {
                _fileWatcher.Stop();
                _fileWatcher.FileReady -= OnFileReady;
            }
        }

        private void OnFileReady(object sender, FileReadyEventArgs e)
        {
            if (e?.File == null || !e.File.Exists) return;

            EventManager.LogDebug($"FileReader.OnFileReady() - Event raised. File: {e.FullName}");

            Process(null);
        }

        private void Process(object state)
        {
            if (!Config.MasterServer)
            {
                if (CheckMasterEcv())
                {
                    // master working, do nothing
                    if (_masterError)
                    {
                        _masterError = false;
                        AppLogger.LogWarning("Master output resumed", null);
                    }
                    return;
                }
                else
                {
                    _masterError = true;
                    AppLogger.LogWarning("Master output not working; Slave output", null);
                }
            }

            FileInfo[] files = new DirectoryInfo(_stagingFolder).GetFiles();
            if (files.Length >= _numFiles)
            {
                EventManager.LogInfo($"S3Archiver.Process() - more than {_numFiles} found");

                var guid = Guid.NewGuid().ToString();

                // Take most recent
                try
                {
                    var recentFiles = files.OrderByDescending(x => x.LastWriteTimeUtc).Take(_numFiles).ToList();
                    recentFiles.Reverse(); // from oldest to newest

                    List<string> combinedLines = new List<string>();
                    bool isFirst = true;
                    foreach (var inputFilePath in recentFiles)
                    {
                        var lines = File.ReadAllLines(inputFilePath.FullName);
                        if (!isFirst)
                            lines = lines.Skip(1).ToArray();
                        combinedLines.AddRange(lines);
                        isFirst = false;
                        Console.WriteLine("The file {0} has been processed.", inputFilePath);
                    }

                    File.WriteAllLines(Path.Combine(_stagingFolder, "combined.csv"), combinedLines);
                    var outputStream = File.ReadAllBytes(Path.Combine(_stagingFolder, "combined.csv"));

                    // Combine
                    //using (var outputStream = File.Create(Path.Combine(_stagingFolder, "combined.csv")))
                    //{
                    //    foreach (var inputFilePath in recentFiles)
                    //    {
                    //        using (var inputStream = File.OpenRead(inputFilePath.FullName))
                    //        {
                    //            // Buffer size can be passed as the second argument.
                    //            inputStream.CopyTo(outputStream);
                    //        }
                    //        Console.WriteLine("The file {0} has been processed.", inputFilePath);
                    //    }

                        // Put combined file in S3
                        string outputFileName = "Lx-" + DateTime.UtcNow.ToString("yyyy-MM-dd-HH-mm-ss");
                        _storage.PutObject(_bucket, Config.S3PathPrefix + "/" + outputFileName, outputStream);

                    EventManager.LogDebug($"S3Archiver.Process() - [{guid}] Combined file uploaded to S3 {Config.S3PathPrefix + "/" + outputFileName}");

                    // SNS
                    var presignedUrl = _storage.GetPresignedUrl(_bucket, Config.S3PathPrefix + "/" + outputFileName, PresignedUrlExpireMinutes);
                    var esbStatusTask = PublishProvider.TrySendNewFileMessage(presignedUrl, guid);
                    Task.WaitAll(esbStatusTask);

                    EventManager.LogDebug($"S3Archiver.Process() - [{guid}] SNS sent");

                    // Dynamo
                    if (Config.DynamoMonitoring)
                    {
                        JsonOdsValue store = new JsonOdsValue();
                        store["feedId"] = new JsonOdsValue(guid);
                        store["pointOfContact"] = new JsonOdsValue("Lightning-5MinFeed-Ingest");
                        store["dateTime"] = new JsonOdsValue(LtgTimeUtils.GetSecondsSince1970FromDateTime(DateTime.UtcNow));
                        store["message"] = new JsonOdsValue("Latest lightning file download url " + presignedUrl);
                        store["status"] = new JsonOdsValue("Success");
                        _noSql.SetValue(Config.DynamoMonitoringTable, new JsonOdsValue("feedId"), new JsonOdsValue("pointOfContact"), store);

                        EventManager.LogDebug($"S3Archiver.Process() - [{guid}] Logged to {Config.DynamoMonitoringTable}");
                    }
                    //}

                    // Cleanup
                    File.Delete(Path.Combine(_stagingFolder, "combined.csv"));

                    foreach (var fileInfo in files)
                    {
                        fileInfo.Delete();
                    }
                    EventManager.LogDebug($"S3Archiver.Process() - [{guid}] Staging files deleted");
                }
                catch (Exception ex)
                {
                    EventManager.LogError($"S3Archiver.Process() - Error", ex);
                    JsonOdsValue store = new JsonOdsValue();
                    store["feedId"] = new JsonOdsValue(guid);
                    store["pointOfContact"] = new JsonOdsValue("Lightning-5MinFeed-Ingest");
                    store["dateTime"] = new JsonOdsValue(LtgTimeUtils.GetSecondsSince1970FromDateTime(DateTime.UtcNow));
                    store["message"] = new JsonOdsValue("Latest lightning file error:" + ex.Message);
                    store["status"] = new JsonOdsValue("Failure");
                    _noSql.SetValue(Config.DynamoMonitoringTable, new JsonOdsValue("feedId"), new JsonOdsValue("pointOfContact"), store);
                }

            }
        }

        private bool CheckMasterEcv()
        {
            // return false if master ECV times out
            try
            {
                string url = Config.MasterServerEcvUrl;
                string json = _httpClient.GetStringAsync(url).Result;
                JObject jo = JObject.Parse(json);
                JToken token = jo.SelectToken("lastFilesCreated", false);
                List<string> list = JsonConvert.DeserializeObject<List<string>>(token.ToString());

                return true;
            }
            catch (Exception ex)
            {
                if (ex.InnerException.GetType() == typeof(TaskCanceledException))
                {
                    // Timeout
                    AppLogger.LogError("CheckMasterEcv times out: ", ex);
                    return false;
                }
                else
                {
                    AppLogger.LogError("CheckMasterEcv failed: ", ex);
                    return false;
                }
            }
        }
    }
}
