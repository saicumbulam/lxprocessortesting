﻿using System;
using System.IO;

namespace LxDatafeedFileDelivery.Model
{
    public class FileReaderModel
    {
        public string Name { get; set; }

        public DirectoryInfo LocalFolder { get; set; }

        public DirectoryInfo OutputFolder { get; set; }

        public bool OutputCsv { get; set; }

        public bool EncryptBody { get; set; }

        public bool HasSftpHeader { get; set; }
        
        public string EncryptionKey { get; set; }

        public string EncryptionIV { get; set; }

        public BoundingBox BoundingBox { get; set; }

        public BoundingBox ExclusionBoundingBox { get; set; }

        public UploadType UploadType { get; set; }

        public string UploadServer { get; set; }

        public string UploadPath { get; set; }

        public string UploadUserName { get; set; }

        public string UploadPassword { get; set; }

        public int FilesCreatedLastTwoMinutes
        {
            get
            {
                int i = 0;
                foreach (FileInfo fileInfo in LocalFolder.GetFiles())
                {
                    if ((DateTime.UtcNow - fileInfo.CreationTimeUtc).TotalMinutes <= 2)
                    {
                        i++;
                    }
                }
                return i;
            }
        }

        public int RetryCount { get; internal set; }
        public int RetryInitialIntervalSeconds { get; set; }
        public int RetryIncrementSeconds { get; set; }
        public bool NldnFormat { get; set; }
        public string WmoHeaderId { get; set; }
        
        public bool FlashSolutionColumn { get; set; }
    }

    public enum UploadType
    {
        Local,
        Ftp,
        SFtp,
        FTPS,
        S3
    };
}
