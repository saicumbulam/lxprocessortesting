﻿using System;

namespace LxDatafeedFileDelivery.Model
{
    public class BoundingBox
    {
        public double Top, Bottom, Left, Right; // boundary of the tree node, top/bottom: Latitude (-90 to 90), left/right: Longitude (-180 to 180)

        public BoundingBox()
        {
            Top = 0; Bottom = 0; Left = 0; Right = 0;
        }
       
        public BoundingBox(double l, double r, double b, double t)
        {
            Left = l; Right = r; Top = t; Bottom = b;
        }

        public double Area()
        {
            return (Right - Left) * (Top - Bottom);
        }

        public bool Contains(double x, double y)
        {
            if (x > Left && x < Right && y > Bottom && y < Top)
                return true;
            return false;
        }

        public bool Contains(BoundingBox rect)
        {
            if (Left < rect.Left && Right > rect.Right && Bottom < rect.Bottom && Top > rect.Top)
                return true;
            return false;

        }

        public bool IntersectsWith(BoundingBox rect)
        {
            double delta = 0;
            if (Left > rect.Right + delta || Top < rect.Bottom - delta || Bottom > rect.Top + delta || Right < rect.Left - delta)
                return false;
            return true;
        }
        public BoundingBox Expand(double offset)
        {
            BoundingBox newRect = new BoundingBox(this.Left - offset, this.Right + offset, this.Bottom - offset, this.Top + offset);
            return newRect;
        }

        public static double IntersectionArea(BoundingBox rect1, BoundingBox rect2)
        {
            double commonArea = 0.0;
            BoundingBox intersectRect = new BoundingBox();
            intersectRect.Left = Math.Max(rect1.Left, rect2.Left);
            intersectRect.Right = Math.Min(rect1.Right, rect2.Right);
            intersectRect.Bottom = Math.Max(rect1.Bottom, rect2.Bottom);
            intersectRect.Top = Math.Min(rect1.Top, rect2.Top);

            if (intersectRect.Top > intersectRect.Bottom && intersectRect.Right > intersectRect.Left)
            {
                commonArea = intersectRect.Area();
            }
            return commonArea;
        }

        public static BoundingBox Union(BoundingBox rect1, BoundingBox rect2)
        {
            BoundingBox unionRect = new BoundingBox();
            unionRect.Left = Math.Min(rect1.Left, rect2.Left);
            unionRect.Right = Math.Max(rect1.Right, rect2.Right);
            unionRect.Bottom = Math.Min(rect1.Bottom, rect2.Bottom);
            unionRect.Top = Math.Max(rect1.Top, rect2.Top);
            return unionRect;
        }
    }
}
