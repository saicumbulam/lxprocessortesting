﻿using System;

namespace LxQueryWorkerConsole
{
    public class Program
    {
        private static void Main(string[] args)
        {
            LxQueryWorker.Program.Start();

            Console.WriteLine("Press \'q\' to quit the program.");
            while (Console.Read() != 'q') { }

            LxQueryWorker.Program.Stop();
        }
    }
}
