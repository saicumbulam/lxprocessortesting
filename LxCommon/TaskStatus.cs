using System;
using Aws.Core.Utilities;

namespace LxCommon
{
	public enum TaskState
	{Err=0,Ok=1};

	/// <summary>
	/// Summary description for ThreadStatus.
	/// </summary>
	public class TaskStatus
	{
		private readonly ushort _taskId;
		private DateTime _dtErrTime;
		private string _lastErrMsg;
		private readonly int _reportErrInterval; //in Minutes;
	    private MsgType _msgType;

        public TaskStatus(ushort taskId, int reportErrInSec)
		{
			_taskId = taskId;
            _reportErrInterval = reportErrInSec;
			_lastErrMsg = "";
			_dtErrTime = DateTime.MinValue;
		}

		public void UpdateStatus(ThreadEventArgs args)
		{
            if (_lastErrMsg == "")
            {
                _msgType = args._msgType;
                _lastErrMsg = args._strMsg;
                _dtErrTime = DateTime.Now;
            }
		}

        public void ResetStatus()
        {
            _lastErrMsg = "";
            _dtErrTime = DateTime.MinValue;
        }
        public TaskState CheckStatusAndLogEvent()
		{
            TaskState rtnVal = TaskState.Ok;
			if(_lastErrMsg != "")
			{
                rtnVal = TaskState.Err;
				if(_dtErrTime > DateTime.MinValue
					&& _dtErrTime.AddSeconds(_reportErrInterval)<DateTime.Now)
				{
					//if this error continues for the specified interval, report it, and re-set status.
                    switch (_msgType)
                    {
                        case MsgType.Err:
                            EventManager.LogError(_taskId, LastErrMsg);
                            break;
                        case MsgType.Warning:
                            EventManager.LogWarning(_taskId, LastErrMsg);
                            break;
                        default:
                            EventManager.LogInfo(_taskId, LastErrMsg);
                            break;
                    }
                    
                    //reset after log
                    ResetStatus();

 				}
			}

            return rtnVal;
		}
		
		#region propertis
		public TaskState State
		{
			get
			{
				return (_lastErrMsg == "")?TaskState.Ok : TaskState.Err;
			}
		}

		public string LastErrMsg
		{
			get
			{
				return _lastErrMsg;
			}
		}

		#endregion

	}
}
