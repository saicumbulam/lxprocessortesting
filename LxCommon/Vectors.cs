﻿using System;

namespace LxCommon
{
    public class Rect
    {
        public double top, bottom, left, right; // boundary of the tree node, top/bottom: Latitude (-90 to 90), left/right: Longitude (-180 to 180)
        public Rect()
        {
            top = bottom = left = right = 0;
        }

        public Rect(double l, double r, double b, double t)
        {
            left = l; right = r; top = t; bottom = b;
        }
        public double MaxSizeInDegree()
        {
            return Math.Max(right - left, top - bottom);
        }
        public double MaxSizeInKM()
        {
            double horizontal = StationData.DistanceInKM(top, left, 0, top, right, 0);
            double vertical = StationData.DistanceInKM(top, left, 0, bottom, left, 0);
            return Math.Max(horizontal, vertical);
        }

        public bool IsInside(double x, double y)
        {
            return (x > left && x < right && y > bottom && y < top);
        }
    }


    public class Vector
    {
        public double x;
        public double y;
        public double z;
    }

    public class Vector2D
    {
        public double x;
        public double y;
        public Vector2D() { x = 0; y = 0; }
        public Vector2D(double vx, double vy)
        {
            x = vx; y = vy;
        }
    }


    public class VectorOps
    {
        public static Vector2D VectorAdd(Vector2D v1, Vector2D v2)
        {
            Vector2D v = new Vector2D(v1.x + v2.x, v1.y + v2.y);
            return v;
        }

        public static Vector2D ScalarMiltiplication(Vector2D v1, double constant)
        {
            Vector2D v = new Vector2D(v1.x * constant, v1.y * constant);
            return v;
        }

        public static Vector VectorNormalize(Vector vector)
        {
            double d = 0;
            d = vector.x * vector.x + vector.y * vector.y + vector.z * vector.z;
            d = Math.Sqrt(d);
            if (d == 0.0)
                return null;

            vector.x = vector.x / d;
            vector.y = vector.y / d;
            vector.z = vector.z / d;
            return vector;
        }

        public static Vector2D VectorNormalize(Vector2D vector)
        {
            double d = 0;
            d = vector.x * vector.x + vector.y * vector.y;
            d = Math.Sqrt(d);

            if (d == 0.0)
                return vector;

            vector.x = vector.x / d;
            vector.y = vector.y / d;
            return vector;
        }

        public static Vector ReverseVector(Vector vector)
        {
            vector.x *= -1;
            vector.y *= -1;
            vector.z *= -1;
            return vector;
        }

        public static Vector CrossProduct(Vector vector1, Vector vector2)
        {
            Vector vector = new Vector();
            vector.x = vector1.y * vector2.z - vector1.z * vector2.y;
            vector.y = vector1.z * vector2.x - vector1.x * vector2.z;
            vector.z = vector1.x * vector2.y - vector1.y * vector2.x;
            return vector;
        }

        public static double DotProduct(Vector vector1, Vector vector2)
        {
            return (vector1.x * vector2.x + vector1.y * vector2.y + vector1.z * vector2.z);
        }

        public static double DotProduct(Vector2D vector1, Vector2D vector2)
        {
            return (vector1.x * vector2.x + vector1.y * vector2.y);
        }

        public static double LengthOfVector(Vector vector)
        {
            return Math.Sqrt(vector.x * vector.x + vector.y * vector.y + vector.z * vector.z);
        }

        public static double LengthOfVector(Vector2D vector)
        {
            return Math.Sqrt(vector.x * vector.x + vector.y * vector.y);
        }

        // vector rotate around a normalized vector rotateVector with angle
        public static Vector VectorRotate(Vector vector, Vector rotateVector, double sine, double consine)
        {
            Vector rotatedVector = new Vector();

            //double sine = Math.Sin(angle);
            //double consine = Math.Cos(angle);
            double ux = rotateVector.x * vector.x; //u*x
            double uy = rotateVector.x * vector.y; //u*y
            double uz = rotateVector.x * vector.z; //uz#=u*z

            double vx = rotateVector.y * vector.x; //vx#=v*x
            double vy = rotateVector.y * vector.y; //vy#=v*y
            double vz = rotateVector.y * vector.z; //vz#=v*z
            double wx = rotateVector.z * vector.x; //wx#=w*x
            double wy = rotateVector.z * vector.y; //wy#=w*y
            double wz = rotateVector.z * vector.z; //wz#=w*z

            rotatedVector.x = rotateVector.x * (ux + vy + wz) + (vector.x * (rotateVector.y * rotateVector.y + rotateVector.z * rotateVector.z) - rotateVector.x * (vy + wz)) * consine + (-wy + vz) * sine;
            rotatedVector.y = rotateVector.y * (ux + vy + wz) + (vector.y * (rotateVector.x * rotateVector.x + rotateVector.z * rotateVector.z) - rotateVector.y * (ux + wz)) * consine + (wx - uz) * sine;
            rotatedVector.z = rotateVector.z * (ux + vy + wz) + (vector.z * (rotateVector.x * rotateVector.x + rotateVector.y * rotateVector.y) - rotateVector.z * (ux + vy)) * consine + (-vx + uy) * sine;

            return rotatedVector;
        }

        public static double AngleOfTwoVectors(Vector2D v1, Vector2D v2)
        {
            double l1 = LengthOfVector(v1);
            double l2 = LengthOfVector(v2);
            if (l1 == 0.0 || l2 == 0.0)
                return double.MaxValue;

            double arcAngle = DotProduct(v1, v2) / (l1 * l2);
            if (arcAngle > 1.0)
                return double.MaxValue;

            return Math.Acos(arcAngle);
        }

        public static Vector2D RotateVector2D(Vector2D v, double angleInRadian)
        {
            Vector2D finalVector = new Vector2D();
            finalVector.x = v.x * Math.Cos(angleInRadian) - v.y * Math.Sin(angleInRadian);
            finalVector.y = v.x * Math.Sin(angleInRadian) + v.y * Math.Cos(angleInRadian);
            return finalVector;
        }
    }

}
