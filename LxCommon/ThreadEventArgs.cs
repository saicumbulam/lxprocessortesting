using System;

namespace LxCommon
{
	public enum MsgType
	{Debug=-2,Fatal=-1, Err=0, Warning=1, Info=2, Verb=3, ThreadMonitor=4};

 
	public delegate void UpdateMsgDelegate(Object sender, ThreadEventArgs args);

	/// <summary>
	/// Summary description for ThreadEventArgs.
	/// </summary>
	public class ThreadEventArgs : EventArgs
	{
		public MsgType _msgType;
		public string _strMsg;
		public ushort _taskId;
        public const ushort TID_None = 0;
        public object _record; // used to record the thread informaiton
		public ThreadEventArgs(ushort srcId, MsgType type, string msg)
		{
			_taskId = srcId;
			_msgType = type;
			_strMsg = msg;
		}

        public ThreadEventArgs(ushort srcID, MsgType type,  object record, string msg)
        {
            _taskId = srcID;
            _msgType = type;
            _strMsg = msg;
            _record = record;
        }

		public ThreadEventArgs(MsgType type, string msg)
		{
            _taskId = TID_None;
			_msgType = type;
			_strMsg = msg;
		}
	
		#region properties
		public ushort TaskID
		{
			get
			{
				return _taskId;
			}
		}
		
		public MsgType TheType
		{
			get
			{
				return _msgType;
			}
		}

		public string Msg
		{
			get
			{
				return _strMsg;
			}
		}

		
		#endregion

	}
}
