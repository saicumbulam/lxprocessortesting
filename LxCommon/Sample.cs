using System;

namespace LxCommon
{
    public struct Sample
    {
        private UInt32 tick;
        private short amplitude;

        #region MyRegion
        public UInt32 Tick
        {
            get
            {
                return tick;
            }
            set
            {
                tick = value;
            }

        }
        public short E
        {
            get
            {
                return amplitude;
            }
            set
            {
                amplitude = value;
                if (amplitude <= short.MinValue)
                {
                    amplitude++;// make sure we can use Math.Abs on the amplitude
                }
            }

        }
        #endregion

    }
}
