﻿using System.Net;
using System.Net.Sockets;
using System.Text;

namespace LxCommon.DataFeed
{
    internal class SocketState
    {
        private int _bufferSize;
        private Socket _socket;
        public EndPoint RemoteEndPoint = null;

        public SocketState(Socket socket, int bufferSize)
            : this(socket, bufferSize, null)
        { }

        public SocketState()
            : this(null, 0, null)
        { }

        public SocketState(Socket socket, int bufferSize, string id)
        {
            _socket = socket;
            _bufferSize = bufferSize;
            Id = id;
            Buffer = new byte[_bufferSize];
            CacheBuffer = new byte[_bufferSize];
            BytesRead = 0;
            ReceivedMsg = new StringBuilder();
            CacheLen = 0;

            if (null != _socket && SocketType.Stream == _socket.SocketType)
                RemoteEndPoint = _socket.RemoteEndPoint;
            else
                RemoteEndPoint = new IPEndPoint(IPAddress.Any, 0);
        }

        #region Properties

        public byte[] Buffer { get; set; }

        public int CacheLen { get; set; }

        public byte[] CacheBuffer { get; set; }

        public int BufferSize
        {
            get { return _bufferSize; }
            set { _bufferSize = value; }
        }

        public int BytesRead { get; set; }

        public string Id { get; set; }

        public string RemoteIpAddress
        {
            get { return ((IPEndPoint)RemoteEndPoint).Address.ToString(); }
        }

        public StringBuilder ReceivedMsg { get; set; }

        public Socket Socket
        {
            get { return _socket; }
            set { _socket = value; }
        }

        #endregion
    }
}
