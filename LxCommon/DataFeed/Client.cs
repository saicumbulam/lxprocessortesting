﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Text;
using Aws.Core.Utilities;
using LxCommon.Config;
using LxCommon.Monitoring;

namespace LxCommon.DataFeed
{
    public delegate void OnFlashReceived(LTGFlash flash);

    public class Client
    {
        private TcpClient _tcpClient;
        private Stream _genericStream;
        private SocketState _socketState;
        private readonly object _lock;
        private DateTime _lastFlashReceivedUtc;
        private RateBuffer _rateBuffer;

        private const ushort EventId = TaskMonitor.DfClient;

        private byte[] _unUsedData;

        private readonly OnFlashReceived _onFlashReceived;

        private string _connectionString;
        private string _server;
        private int _port;
        private int _packetLength;
        private bool _isComboFeed;

        public Client(string connectionString, string server, int port, int packetLength, bool isComboFeed, OnFlashReceived onFlashRecived):
            this(onFlashRecived)
        {
            _connectionString = connectionString;
            _server = server;
            _port = port;
            _packetLength = packetLength;
            _isComboFeed = isComboFeed;
        }

        public Client(OnFlashReceived onFlashRecived)
        {
            _onFlashReceived = onFlashRecived;
            _lock = new object();
            _lastFlashReceivedUtc = DateTime.MinValue;
            _rateBuffer = new RateBuffer(AppConfig.DatafeedRateSampleSize);

            _connectionString = AppConfig.DataFeedClientConnectionString;
            _server = AppConfig.DataFeedClientServer;
            _port = AppConfig.DataFeedClientPort;
            _packetLength = AppConfig.DataFeedClientPacketLength;
            _isComboFeed = false;

        }

        public void DoDisconnect()
        {
            if (_tcpClient != null)
            {
                CleanupSocket();
                _socketState = null;
                _tcpClient = null;
            }
        }

        public void DoConnect()
        {
            if (_tcpClient != null)
            {
                CleanupSocket();
                _socketState = null;
                _tcpClient = null;
            }

            _unUsedData = null;

            Connecting();
        }

        private void Connecting()
        {
            try
            {
                byte[] buf = Encoding.ASCII.GetBytes(_connectionString);

                if (_tcpClient != null && _tcpClient.Client != null)
                {
                    try
                    {
                        _genericStream.Close();
                        _tcpClient.Client.Close();
                        _tcpClient.Close();
                    }
                    catch (Exception ex)
                    {
                        EventManager.LogWarning(EventId, "Error in closing the client socket.", ex);
                    }
                    finally
                    {
                        _tcpClient = null;
                    }
                }

                EventManager.LogInfo(EventId + 1, string.Format("Connecting to lightning datafeed. [server={0}:{1}; connectionString={2}]",
                    _server, _port, _connectionString));

                _tcpClient = new TcpClient(_server, _port);
                _genericStream = _tcpClient.GetStream();
                _genericStream.Write(buf, 0, buf.Length);
                _genericStream.Flush();

                _socketState = new SocketState(_tcpClient.Client, _packetLength);
                _genericStream.BeginRead(_socketState.Buffer, 0, _socketState.Buffer.Length, BeginReadCallback, _socketState);

            }
            catch (Exception e)
            {
                EventManager.LogError(EventId + 2, "Error connecting to the datafeed", e);
                _tcpClient = null;
            }
        }

        private void CleanupSocket()
        {
            try
            {
                if (_genericStream != null)
                {
                    _genericStream.Close();
                    _genericStream = null;
                }

                if (_tcpClient != null)
                {
                    Socket socket = _tcpClient.Client;
                    if (null != socket && socket.Connected)
                    {
                        socket.Shutdown(SocketShutdown.Both);
                        socket.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                EventManager.LogWarning(EventId + 3, "Failure cleaning up socket.", ex);
            }

        }

        private void BeginReadCallback(IAsyncResult result)
        {
            try
            {
                SocketState request = (SocketState)result.AsyncState;
                int count = _genericStream.EndRead(result);

                byte[] data = new byte[count + (_unUsedData == null ? 0 : _unUsedData.Length)];
                if (_unUsedData != null)
                {
                    Array.Copy(_unUsedData, 0, data, 0, _unUsedData.Length);
                    Array.Copy(request.Buffer, 0, data, _unUsedData.Length, count);
                    _unUsedData = null;
                }
                else
                {
                    Array.Copy(request.Buffer, 0, data, 0, count);
                }

                ReadBuffer(data);
            }
            catch (Exception e)
            {
                EventManager.LogError(EventId + 4, "Failure in BeginReadCallback for RespCallback.", e);
                CleanupSocket();
                _tcpClient = null;

            }
            finally
            {
                if (_tcpClient != null)
                {
                    try
                    {
                        _genericStream.BeginRead(_socketState.Buffer, 0, _socketState.Buffer.Length,
                                                        BeginReadCallback, _socketState);
                    }
                    catch (Exception ex)
                    {
                        _tcpClient = null;
                        EventManager.LogError(EventId + 5, "Failure in BeginRead for RespCallback.", ex);
                    }
                }
            }
        }

        private void ReadBuffer(byte[] data)
        {
            if (data.Length > 0)
            {
                int packageStartPos = 0;

                do
                {
                    ushort? len;
                    if (_isComboFeed)
                    {
                        len = ReadUShortFromBufferAtOffset(data, packageStartPos, true);
                    }
                    else
                    {
                        len = data[packageStartPos];
                    }

                    if (len.HasValue)
                    {
                        ushort packageByteSize = len.Value;

                        if ((packageStartPos + packageByteSize) <= data.Length)
                        {
                            byte[] package = new byte[packageByteSize];
                            Array.Copy(data, packageStartPos, package, 0, packageByteSize);
                            
                            LTGFlash ltgFlash;
                            if (_isComboFeed)
                            {
                                ltgFlash = ParseComboPackage(package);
                            }
                            else
                            {
                                ltgFlash = (LTGFlash) ParseFlashPackage(package, true);
                            }

                            if (ltgFlash != null)
                            {
                                _onFlashReceived(ltgFlash);
                                _rateBuffer.AddSample(DateTime.UtcNow);
                                lock (_lock)
                                {
                                    _lastFlashReceivedUtc = DateTime.UtcNow;
                                }
                            }

                            packageStartPos += packageByteSize;
                            if (packageStartPos == data.Length)
                            {
                                MoveBytesToBeginningOfBuffer(data, data.Length, packageStartPos);
                                break;
                            }
                        }
                        else
                        {   // We have already started reading the next package so setup the buffer to read the rest of the msg
                            MoveBytesToBeginningOfBuffer(data, data.Length, packageStartPos);
                            break;
                        }
                    }
                    else
                    {
                        MoveBytesToBeginningOfBuffer(data, data.Length, packageStartPos);
                        break;
                    }

                } while (packageStartPos < data.Length);
            }
        }

        public const byte NumberOfBytes = 0;
        public const byte FlashPacketOffset = 2;
        public const byte FlashPacketLength = 26;
        public const byte PulsePacketOffset = 28;
        public const byte PulsePacketLength = 26;

        private LTGFlash ParseComboPackage(byte[] package)
        {
            byte[] flashBytes = new byte[FlashPacketLength];
            Array.Copy(package, FlashPacketOffset, flashBytes, 0, FlashPacketLength);
            LTGFlash flash = (LTGFlash) ParseFlashPackage(flashBytes, true);

            //-3 == packet len + packet checksum
            int numberPortions = (package.Length - FlashPacketLength - 3) / PulsePacketLength;
            int nextStartingPosition = PulsePacketOffset;

            for (int i = 0; i < numberPortions; i++)
            {
                byte[] pulseBytes = new byte[PulsePacketLength];
                nextStartingPosition = PulsePacketOffset + (i * pulseBytes.Length);

                Array.Copy(package, nextStartingPosition, pulseBytes, 0, pulseBytes.Length);
                LTGFlashPortion portion = (LTGFlashPortion) ParseFlashPackage(pulseBytes, false);
                flash.AddFlashPortion(portion);
            }

            return flash;
        }

        private FlashData ParseFlashPackage(byte[] packageBytes, bool isFlash)
        {
            int type = packageBytes[PositionOffset.LxClassificationType];
            FlashType ft = ParseFlashType(type);

            uint? time = ReadUIntFromBufferAtOffset(packageBytes, PositionOffset.Time, true);
            ushort? millisec = ReadUShortFromBufferAtOffset(packageBytes, PositionOffset.Millisecond, true);
            DateTime? flashTime = null;
            if (time.HasValue && millisec.HasValue)
                flashTime = TimeSvc.GetTime((int)time.Value).AddMilliseconds(millisec.Value);

            int? lat = ReadIntFromBufferAtOffset(packageBytes, PositionOffset.Latitude, true);
            int? lon = ReadIntFromBufferAtOffset(packageBytes, PositionOffset.Longitude, true);
            int? amp = ReadIntFromBufferAtOffset(packageBytes, PositionOffset.Amplitude, true);
            int? height = ReadUShortFromBufferAtOffset(packageBytes, PositionOffset.IcHeight, true);

            FlashData ltgFlash = null;
            if (ft != FlashType.FlashTypeUnknown && flashTime.HasValue && lat.HasValue
                && lon.HasValue && amp.HasValue && height.HasValue)
            {
                if (isFlash)
                {
                    ltgFlash = new LTGFlash
                    {
                        FlashType = ft,
                        FlashTime = LtgTimeUtils.GetDateTimeString(flashTime.Value),
                        Latitude = lat.Value / Math.Pow(10, 7),
                        Longitude = lon.Value / Math.Pow(10, 7),
                        Amplitude = amp.Value,
                        Height = height.Value
                    };
                }
                else
                {
                    ltgFlash = new LTGFlashPortion
                    {
                        FlashType = ft,
                        FlashTime = LtgTimeUtils.GetDateTimeString(flashTime.Value),
                        Latitude = lat.Value / Math.Pow(10, 7),
                        Longitude = lon.Value / Math.Pow(10, 7),
                        Amplitude = amp.Value,
                        Height = height.Value
                    };
                }
            }

            return ltgFlash;
        }

        private static FlashType ParseFlashType(int type)
        {
            FlashType ft = FlashType.FlashTypeUnknown;

            switch (type)
            {
                case 0:
                    ft = FlashType.FlashTypeCG;
                    break;
                case 1:
                    ft = FlashType.FlashTypeIC;
                    break;
                case 2:
                    ft = FlashType.FlashTypeICNarrowBipolar;
                    break;
                case 40:
                    ft = FlashType.FlashTypeGlobalCG;
                    break;
                case 41:
                    ft = FlashType.FlashTypeGlobalIC;
                    break;
            }

            return ft;
        }


        private static uint? ReadUIntFromBufferAtOffset(byte[] packageBytes, int offset, bool isMsbFirst)
        {
            byte[] bytes = ReadBytes(packageBytes, offset, isMsbFirst, 4);
            return (bytes != null ? BitConverter.ToUInt32(bytes, 0) : (uint?)null);
        }

        private static ushort? ReadUShortFromBufferAtOffset(byte[] packageBytes, int offset, bool isMsbFirst)
        {
            byte[] bytes = ReadBytes(packageBytes, offset, isMsbFirst, 2);
            return (bytes != null ? BitConverter.ToUInt16(bytes, 0) : (ushort?)null);
        }

        private static int? ReadIntFromBufferAtOffset(byte[] packageBytes, int offset, bool isMsbFirst)
        {
            byte[] bytes = ReadBytes(packageBytes, offset, isMsbFirst, 4);
            return (bytes != null ? BitConverter.ToInt32(bytes, 0) : (int?)null);
        }

        private static byte[] ReadBytes(byte[] packageBytes, int offset, bool isMsbFirst, int dataTypeSize)
        {
            byte[] relevantBytes = null;

            if (packageBytes.Length > (offset + dataTypeSize - 1))
            {
                relevantBytes = new byte[dataTypeSize];
                Array.Copy(packageBytes, offset, relevantBytes, 0, dataTypeSize);
                if (isMsbFirst)
                {
                    Array.Reverse(relevantBytes);
                }
            }

            return relevantBytes;
        }

        private void MoveBytesToBeginningOfBuffer(byte[] data, int bytesRead, int packageStartPos)
        {
            _unUsedData = new byte[bytesRead - packageStartPos];
            Array.Copy(data, packageStartPos, _unUsedData, 0, _unUsedData.Length);
        }

        public DateTime LastFlashReceivedUtc
        {
            get
            {
                lock (_lock)
                {
                    return _lastFlashReceivedUtc;
                }
            }
        }

        public double FlashPerMinute
        {
            get { return _rateBuffer.GetRatePerMinute(DateTime.UtcNow); }
        }

        public class PositionOffset
        {
            public const byte NumberOfBytes = 0;
            public const byte LxClassificationType = 1;
            public const byte Time = 2;
            public const byte Millisecond = 6;
            public const byte Latitude = 8;
            public const byte Longitude = 12;
            public const byte Amplitude = 16;
            public const byte LocationError = 20;   // Publicly this is a reserved field; used by IAF
            public const byte IcHeight = 21;
            public const byte NumberOfSensors = 23;
            public const byte Multiplicity = 24;
            public const byte Checksum = 25;
        }

    } 
}
