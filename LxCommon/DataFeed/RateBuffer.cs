﻿using System;
using System.Threading;

namespace LxCommon.DataFeed
{
    public class RateBuffer
    {
        private int _sampleSize;
        private DateTime _startTime;

        private int[] _id;
        private int[] _sum;


        public RateBuffer(int sampleSize)
        {
            _sampleSize = sampleSize;
            _startTime = DateTime.UtcNow;

            _id = new int[sampleSize];
            
            for (int i = 0; i < sampleSize; i++) 
                _id[i] = -1;

            _sum = new int[sampleSize];
        }

        public void AddSample(DateTime time)
        {
            TimeSpan ts = time - _startTime;
            
            int index = (int)ts.TotalMinutes % _sampleSize;
            int id = (int)ts.TotalMinutes / _sampleSize;

            if (_id[index] == id)
            {
                Interlocked.Increment(ref _sum[index]);
            }
            else
            {
                Interlocked.CompareExchange(ref _id[index], id, _id[index]);
                Interlocked.CompareExchange(ref _sum[index], 1, _sum[index]);
            }
        }

        public double GetRatePerMinute(DateTime time)
        {
            double rate = 0.0D;
            double sum = 0.0D;

            TimeSpan ts = time - _startTime;

            int index = (int)ts.TotalMinutes % _sampleSize;
            int id = (int)ts.TotalMinutes / _sampleSize;
            int tm = (int)ts.TotalMinutes;
            int size = 0;

            for (int i = 0; i < _sampleSize; i++)
            {
                if (_id[i] >= 0 && (id - _id[i]) <= 1)
                {
                    if (i != index)
                    {
                        sum = sum + _sum[i];
                        size++;
                    }
                    else
                    {
                        double partial = ts.TotalMinutes - tm;
                        if (partial > 0)
                        {
                            sum = sum + (_sum[i] * (1/partial));
                            size++;
                        }
                    }
                }
            }

            if (size > 0)
                rate = sum / size;

            return rate;
        }
    }
}
