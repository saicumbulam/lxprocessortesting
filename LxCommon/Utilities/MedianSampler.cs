﻿using System;
using System.Collections.Generic;

namespace LxCommon.Utilities
{
    public class MedianSampler<T>
    {
        private readonly List<T> _array;
        private readonly List<T> _copy;
        private readonly int _capacity;
        private int _index;
        private readonly object _lock;
        private readonly Tuple<T, T, T> _defaultValue; 

        public MedianSampler(int capacity)
        {
            _capacity = capacity;
            _array = new List<T>(_capacity);
            _copy = new List<T>(_capacity);
            _index = 0;
            _lock = new object();
            _defaultValue = new Tuple<T, T, T>(default(T), default(T), default(T));
        }

        public void AddSample(T item)
        {
            lock (_lock)
            {

                if (_index > (_capacity - 1))
                {
                    _index = 0;
                }
                
                if (_array.Count < _capacity)
                {
                    _array.Add(item);
                }
                else
                {
                    _array[_index] = item;
                }
                
                _index++;
            }
        }

        public Tuple<T, T, T> GetMinMaxMedian()
        {
            return GetAtPercentInternal(.5);
        }

        public T GetAtPercent(double percent)
        {
            return GetAtPercentInternal(percent).Item3;
        }

        private Tuple<T, T, T> GetAtPercentInternal(double percent)
        {
            if (percent > 1)
            {
                throw new ArgumentOutOfRangeException("percent", "must be less than 1");
            }

            Tuple<T, T, T> minMaxMed = _defaultValue;
            int count;
            
            lock (_lock)
            {
                count = _array.Count;
                _copy.Clear();
                if (count > 0)
                {
                    _copy.AddRange(_array);
                    _copy.Sort();
                    int index = (int)(count * percent);
                    if (index >= count)
                    {
                        index = count - 1;
                    }
                    minMaxMed = new Tuple<T, T, T>(_copy[0], _copy[count - 1], _copy[index]);
                }
            }
            
            return minMaxMed;
        }

        public void Reset()
        {
            lock (_lock)
            {
                _array.Clear();
                _index = 0;
            }
        }

        public int Capacity
        {
            get { return _capacity; }
        }
    }
}
