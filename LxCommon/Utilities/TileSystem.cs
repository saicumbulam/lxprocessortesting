﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LxCommon.Utilities
{
    public static class TileSystem
    {
        private const double EarthRadius = 6378137;
        private const double MinLatitude = -85.05112878;
        private const double MaxLatitude = 85.05112878;
        private const double MinLongitude = -180;
        private const double MaxLongitude = 180;
        public const int LevelOfDetail = 5;

        /// <summary>
        /// Clips a number to the specified minimum and maximum values.
        /// </summary>
        /// <param name="n">The number to clip.</param>
        /// <param name="minValue">Minimum allowable value.</param>
        /// <param name="maxValue">Maximum allowable value.</param>
        /// <returns>The clipped value.</returns>
        private static double Clip(double n, double minValue, double maxValue)
        {
            return Math.Min(Math.Max(n, minValue), maxValue);
        }



        /// <summary>
        /// Determines the map width and height (in pixels) at a specified level
        /// of detail.
        /// </summary>
        /// <param name="levelOfDetail">Level of detail, from 1 (lowest detail)
        /// to 23 (highest detail). Default is 5</param>
        /// <returns>The map width and height in pixels.</returns>
        public static uint MapSize(int levelOfDetail = LevelOfDetail)
        {
            return (uint)256 << levelOfDetail;
        }



        /// <summary>
        /// Determines the ground resolution (in meters per pixel) at a specified
        /// latitude and level of detail.
        /// </summary>
        /// <param name="latitude">Latitude (in degrees) at which to measure the
        /// ground resolution.</param>
        /// <param name="levelOfDetail">Level of detail, from 1 (lowest detail)
        /// to 23 (highest detail). Default is 5</param>
        /// <returns>The ground resolution, in meters per pixel.</returns>
        public static double GroundResolution(double latitude, int levelOfDetail = LevelOfDetail)
        {
            latitude = Math.Round(Clip(latitude, MinLatitude, MaxLatitude), 5, MidpointRounding.ToEven);
            return Math.Cos(latitude * Math.PI / 180) * 2 * Math.PI * EarthRadius / MapSize(levelOfDetail);
        }



        /// <summary>
        /// Determines the map scale at a specified latitude, level of detail,
        /// and screen resolution.
        /// </summary>
        /// <param name="latitude">Latitude (in degrees) at which to measure the
        /// map scale.</param>
        /// <param name="screenDpi">Resolution of the screen, in dots per inch.</param>
        /// <returns>The map scale, expressed as the denominator N of the ratio 1 : N.</returns>
        /// <param name="levelOfDetail">Level of detail, from 1 (lowest detail)
        /// to 23 (highest detail). Default is 5</param>
        public static double MapScale(double latitude, int screenDpi, int levelOfDetail = LevelOfDetail)
        {
            return GroundResolution(latitude, levelOfDetail) * screenDpi / 0.0254;
        }

        /// <summary>
        /// Converts a point from latitude/longitude WGS-84 coordinates (in degrees)
        /// into a QuadKey at a specified level of detail.
        /// </summary>
        /// <param name="latitude">Latitude of the point, in degrees.</param>
        /// <param name="longitude">Longitude of the point, in degrees.</param>
        /// <param name="levelOfDetail">Level of detail, from 1 (lowest detail)
        /// to 23 (highest detail). Default is 5</param>
        /// <returns>A string containing the QuadKey.</returns>
        public static string LatLongToQuadKey(double latitude, double longitude, int levelOfDetail = LevelOfDetail)
        {
            var pixelX = 0;
            var pixelY = 0;
            LatLongToPixelXY(latitude, longitude, out pixelX, out pixelY, levelOfDetail);

            var tileX = 0;
            var tileY = 0;
            PixelXYToTileXY(pixelX, pixelY, out tileX, out tileY);

            return TileXYToQuadKey(tileX, tileY, levelOfDetail);
        }

        /// <summary>
        /// Converts a point from latitude/longitude WGS-84 coordinates (in degrees)
        /// into pixel XY coordinates at a specified level of detail.
        /// </summary>
        /// <param name="latitude">Latitude of the point, in degrees.</param>
        /// <param name="longitude">Longitude of the point, in degrees.</param>
        /// <param name="levelOfDetail">Level of detail, from 1 (lowest detail)
        /// to 23 (highest detail). Default is 5</param>
        /// <param name="pixelX">Output parameter receiving the X coordinate in pixels.</param>
        /// <param name="pixelY">Output parameter receiving the Y coordinate in pixels.</param>
        public static void LatLongToPixelXY(double latitude, double longitude, out int pixelX, out int pixelY, int levelOfDetail = LevelOfDetail)
        {
            latitude = Math.Round(Clip(latitude, MinLatitude, MaxLatitude), 5, MidpointRounding.ToEven);
            longitude = Math.Round(Clip(longitude, MinLongitude, MaxLongitude), 5, MidpointRounding.ToEven);

            var x = (longitude + 180) / 360;
            var sinLatitude = Math.Sin(latitude * Math.PI / 180);
            var y = 0.5 - Math.Log((1 + sinLatitude) / (1 - sinLatitude)) / (4 * Math.PI);

            var mapSize = MapSize(levelOfDetail);
            pixelX = (int)Clip(x * mapSize + 0.5, 0, mapSize - 1);
            pixelY = (int)Clip(y * mapSize + 0.5, 0, mapSize - 1);
        }



        /// <summary>
        /// Converts a pixel from pixel XY coordinates at a specified level of detail
        /// into latitude/longitude WGS-84 coordinates (in degrees).
        /// </summary>
        /// <param name="pixelX">X coordinate of the point, in pixels.</param>
        /// <param name="pixelY">Y coordinates of the point, in pixels.</param>
        /// <param name="levelOfDetail">Level of detail, from 1 (lowest detail)
        /// to 23 (highest detail). Default is 5</param>
        /// <param name="latitude">Output parameter receiving the latitude in degrees.</param>
        /// <param name="longitude">Output parameter receiving the longitude in degrees.</param>
        public static void PixelXYToLatLong(int pixelX, int pixelY, out double latitude, out double longitude, int levelOfDetail = LevelOfDetail)
        {
            double mapSize = MapSize(levelOfDetail);
            var x = (Clip(pixelX, 0, mapSize - 1) / mapSize) - 0.5;
            var y = 0.5 - (Clip(pixelY, 0, mapSize - 1) / mapSize);

            latitude = Math.Round((90 - 360 * Math.Atan(Math.Exp(-y * 2 * Math.PI)) / Math.PI), 5, MidpointRounding.ToEven);
            longitude = Math.Round(360 * x, 5, MidpointRounding.ToEven);
        }



        /// <summary>
        /// Converts pixel XY coordinates into tile XY coordinates of the tile containing
        /// the specified pixel.
        /// </summary>
        /// <param name="pixelX">Pixel X coordinate.</param>
        /// <param name="pixelY">Pixel Y coordinate.</param>
        /// <param name="tileX">Output parameter receiving the tile X coordinate.</param>
        /// <param name="tileY">Output parameter receiving the tile Y coordinate.</param>
        public static void PixelXYToTileXY(int pixelX, int pixelY, out int tileX, out int tileY)
        {
            tileX = pixelX / 256;
            tileY = pixelY / 256;
        }



        /// <summary>
        /// Converts tile XY coordinates into pixel XY coordinates of the upper-left pixel
        /// of the specified tile.
        /// </summary>
        /// <param name="tileX">Tile X coordinate.</param>
        /// <param name="tileY">Tile Y coordinate.</param>
        /// <param name="pixelX">Output parameter receiving the pixel X coordinate.</param>
        /// <param name="pixelY">Output parameter receiving the pixel Y coordinate.</param>
        public static void TileXYToPixelXY(int tileX, int tileY, out int pixelX, out int pixelY)
        {
            pixelX = tileX * 256;
            pixelY = tileY * 256;
        }



        /// <summary>
        /// Converts tile XY coordinates into a QuadKey at a specified level of detail.
        /// </summary>
        /// <param name="tileX">Tile X coordinate.</param>
        /// <param name="tileY">Tile Y coordinate.</param>
        /// <param name="levelOfDetail">Level of detail, from 1 (lowest detail)
        /// to 23 (highest detail). Default is 5</param>
        /// <returns>A string containing the QuadKey.</returns>
        public static string TileXYToQuadKey(int tileX, int tileY, int levelOfDetail = LevelOfDetail)
        {
            var quadKey = new StringBuilder();
            for (var i = levelOfDetail; i > 0; i--)
            {
                var digit = '0';
                var mask = 1 << (i - 1);
                if ((tileX & mask) != 0)
                {
                    digit++;
                }
                if ((tileY & mask) != 0)
                {
                    digit++;
                    digit++;
                }
                quadKey.Append(digit);
            }
            return quadKey.ToString();
        }



        /// <summary>
        /// Converts a QuadKey into tile XY coordinates.
        /// </summary>
        /// <param name="quadKey">QuadKey of the tile.</param>
        /// <param name="tileX">Output parameter receiving the tile X coordinate.</param>
        /// <param name="tileY">Output parameter receiving the tile Y coordinate.</param>
        /// <param name="levelOfDetail">Output parameter receiving the level of detail.</param>
        public static void QuadKeyToTileXY(string quadKey, out int tileX, out int tileY, out int levelOfDetail)
        {
            tileX = tileY = 0;
            levelOfDetail = quadKey.Length;
            for (var i = levelOfDetail; i > 0; i--)
            {
                var mask = 1 << (i - 1);
                switch (quadKey[levelOfDetail - i])
                {
                    case '0':
                        break;

                    case '1':
                        tileX |= mask;
                        break;

                    case '2':
                        tileY |= mask;
                        break;

                    case '3':
                        tileX |= mask;
                        tileY |= mask;
                        break;

                    default:
                        throw new ArgumentException("Invalid QuadKey digit sequence.");
                }
            }
        }

        public static void QuadKeyToBoundingBox(string quadKey, out double maxLatitude, out double maxLongitude, out double minLatitude, out double minLongitude)
        {
            int tileX, tileY, levelOfDetail;
            int pixelX, pixelY;
            double latitude1, longitude1;
            double latitude2, longitude2;

            QuadKeyToTileXY(quadKey, out tileX, out tileY, out levelOfDetail);

            // Get first lat/lon
            TileXYToPixelXY(tileX, tileY, out pixelX, out pixelY);
            PixelXYToLatLong(pixelX, pixelY, out latitude1, out longitude1, levelOfDetail);


            // Get second lat/lon
            TileXYToPixelXY(tileX + 1, tileY + 1, out pixelX, out pixelY);
            PixelXYToLatLong(pixelX, pixelY, out latitude2, out longitude2, levelOfDetail);

            // Convert to bounding box coords
            minLatitude = Math.Min(latitude1, latitude2);
            minLongitude = Math.Min(longitude1, longitude2);
            maxLatitude = Math.Max(latitude1, latitude2);
            maxLongitude = Math.Max(longitude1, longitude2);
        }

        public static List<string> BoundingBoxToQuadKeys(double maxLatitude, double maxLongitude, double minLatitude, double minLongitude, int levelOfDetail = LevelOfDetail)
        {
            var quadKeys = new List<string>();

            // 1 = upper left, 2 = lower right
            int pixelX1, pixelY1;
            LatLongToPixelXY(maxLatitude, minLongitude, out pixelX1, out pixelY1, levelOfDetail);
            int tileX1, tileY1;
            PixelXYToTileXY(pixelX1, pixelY1, out tileX1, out tileY1);


            int pixelX2, pixelY2;
            LatLongToPixelXY(minLatitude, maxLongitude, out pixelX2, out pixelY2, levelOfDetail);
            int tileX2, tileY2;
            PixelXYToTileXY(pixelX2, pixelY2, out tileX2, out tileY2);

            // for loop
            for (var x = tileX1; x <= tileX2; x++)
            {
                for (var y = tileY1; y <= tileY2; y++)
                {
                    quadKeys.Add(TileXYToQuadKey(x, y, levelOfDetail));
                }
            }

            return quadKeys;
        }
    }
}
