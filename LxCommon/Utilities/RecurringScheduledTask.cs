﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace LxCommon.Utilities
{
    public class RecurringScheduledTask
    {
        public static async Task CreateDaily(DateTime executionTime, Action action, CancellationToken cancellationToken)
        {
            await Create(executionTime, 1, action, cancellationToken);
        }

        public static async Task CreateWeekly(DateTime executionTime, Action action, CancellationToken cancellationToken)
        {
            await Create(executionTime, 7, action, cancellationToken);
        }

        private static async Task Create(DateTime executionTime, int daysToAdd, Action action, CancellationToken cancellationToken)
        {
            if (daysToAdd < 1) throw new InvalidOperationException("daysToAdd must be greater than zero");
            if (executionTime.Kind != DateTimeKind.Utc) executionTime = executionTime.ToUniversalTime();
            if (executionTime < DateTime.UtcNow) executionTime = DateTime.UtcNow.AddMinutes(1);

            cancellationToken.ThrowIfCancellationRequested();

            while (true)
            {
                cancellationToken.ThrowIfCancellationRequested();

                await Task.Delay(executionTime.Subtract(DateTime.UtcNow), cancellationToken);
                cancellationToken.ThrowIfCancellationRequested();

                Task.Run(() => action(), cancellationToken);


                executionTime = executionTime.AddDays(daysToAdd);
            }
        }
    }
}
