﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace LxCommon.Utilities
{
    public class ScheduledTask
    {
        public static async Task Create(DateTime executionTime, Action action, CancellationToken cancellationToken)
        {
            if (executionTime.Kind != DateTimeKind.Utc) executionTime = executionTime.ToUniversalTime();
            if (executionTime < DateTime.UtcNow) executionTime = DateTime.UtcNow.AddMinutes(1);

            cancellationToken.ThrowIfCancellationRequested();

            await Task.Delay(executionTime.Subtract(DateTime.UtcNow), cancellationToken);
            cancellationToken.ThrowIfCancellationRequested();
            action();
        }
    }
}
