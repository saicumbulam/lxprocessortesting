﻿using System;
using System.Linq;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;

namespace LxCommon.Utilities
{
    public static class DatabaseHelper
    {
        [SqlFunction]
        public static SqlInt32 ConvertOffsetToNumSensor(SqlString offsetsValue)
        {
            try
            {
                if (offsetsValue.IsNull)
                {
                    return new SqlInt32(0);
                }
                return new SqlInt32(offsetsValue.Value.Count(x => x == '='));

            }
            catch (Exception)
            {
                return new SqlInt32(0);
            }
        }
    }
}
