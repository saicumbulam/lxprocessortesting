﻿using System;
using System.Collections.Generic;

namespace LxCommon.Utilities
{
    public static class ListExtensions
    {
        public static void Resize<T>(this List<T> list, int size, Func<T> factory = null)
        {
            
            if (list.Count > size)
            {
                list.Clear();
            }

            if (list.Count < size)
            {
                while (list.Count < size)
                {
                    list.Add(factory == null ? default(T) : factory());
                }
            }

            list.CheckCapacity(list.Count);
        }


        private const int MinCapacityToShrink = 1000;
        private const int OverCapacityFactor = 10;
        private const int CapacityBufferFactor = 4;
        
        public static void CheckCapacity<T>(this List<T> list, int size)
        {
            //if (size != 0 && list.Capacity >= MinCapacityToShrink && list.Capacity > size * OverCapacityFactor)
            //{
            //    list.Capacity = size * CapacityBufferFactor;
            //}
        }


        public static void Repopulate<T>(this List<T> list, IEnumerable<T> values)
        {
            list.Clear();
            foreach (T value in values)
            {
                list.Add(value);
            }
        }

        public static void SetLengthAtLeast<T>(this List<T> list, int length)
        {
            while (list.Count < length)
            {
                list.Add(default(T));
            }

            list.CheckCapacity(list.Count);
        }

        public static List<T> Clone<T>(this List<T> list)
        {
            List<T> clone = new List<T>();
            foreach (T item in list)
            {
                clone.Add(item);
            }
            return clone;
        }
    }
}
