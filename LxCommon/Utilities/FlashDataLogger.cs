﻿using Aws.Core.Utilities;
using LxCommon.Models;
using LxCommon.Monitoring;
using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace LxCommon.Utilities
{
    public class FlashDataLogger
    {
        private const int MinimumClrThreads = 3;   //use this value to control the threads
        private const int MaximumClrThreads = 4;   //use this value to control the threads

        private static object locker = new Object();

        private const ushort EventManagerOffset = TaskMonitor.FileLogger;

        private string _folder;
        private string _baseFileName;
        private string _filechanged;
        private BlockingCollection<LogEntry> _bc;
        private ConcurrentQueue<LogEntry> _linesToWrite;
        private ConcurrentQueue<PacketEntry> _packetsToWrite;
        private int _writerLock;
        private DirectoryInfo _directory = null;

        private bool _isTlnSystem = true;
        private int _minutesPerFile;

        public FlashDataLogger(string folder, string baseFileName, bool isTlnSystem) : this(folder, baseFileName, isTlnSystem, 1)
        {
        }

        public FlashDataLogger(string folder, string baseFileName, bool isTlnSystem, int minutesPerFile)
        {
            _folder = folder;
            _baseFileName = baseFileName;
            _isTlnSystem = isTlnSystem;
            _bc = new BlockingCollection<LogEntry>(new ConcurrentQueue<LogEntry>(), 1100);;
            _linesToWrite = new ConcurrentQueue<LogEntry>();
            _packetsToWrite = new ConcurrentQueue<PacketEntry>();
            _directory = FileHelper.CreateDirectory(_folder);
            _writerLock = 0;

            if (minutesPerFile <= 0 || minutesPerFile > 60)
            {
                throw new ArgumentException("minutes per file must be > 0 and <= 60");
            }

            _minutesPerFile = minutesPerFile;
        }

        private void Write(LogEntry logEntry)
        {
            //_linesToWrite.Enqueue(logEntry);
            if (_filechanged == null)
            {
                _filechanged = logEntry.FileName;
            }
            if (_filechanged == logEntry.FileName && _bc.Count <= 1000)
            {
                _bc.Add(logEntry);
            }
            else
            {
                //_bc.CompleteAdding();
                _filechanged = logEntry.FileName;
                Tuple<int, int> threadpool1 = InitThreadPool();
                ThreadPool.SetMinThreads(threadpool1.Item1, threadpool1.Item2);
                ThreadPool.QueueUserWorkItem(WriteLines);
                
            }

            // This controls the thread that manages the queue, make sure we only launch one thread
            //if (0 == Interlocked.CompareExchange(ref _writerLock, 1, 0))
            //{
            //    Task.Factory.StartNew(WriteLines);
            //}
        }

        private void Write(PacketEntry packetEntry)
        {
            _packetsToWrite.Enqueue(packetEntry);
            //Tuple<int, int> threadpool1 = InitThreadPool();
            //ThreadPool.SetMinThreads(threadpool1.Item1, threadpool1.Item2);
            ThreadPool.QueueUserWorkItem(WritePackets);
            // This controls the thread that manages the queue, make sure we only launch one thread
            //if (0 == Interlocked.CompareExchange(ref _writerLock, 1, 0))
            //{
            //    Task.Factory.StartNew(WritePackets);
            //}
        }

        public void WriteFlash(LTGFlash ltgFlash, DateTime dt)
        {
            try
            {
                Flash flash = new Flash(ltgFlash, _isTlnSystem);
                WriteFlash(flash, dt);
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventManagerOffset, "Unable to write flash", ex);
            }
        }

        public void WriteFlash(Flash flash, DateTime dt)
        {
            try
            {
                string f = JsonConvert.SerializeObject(flash);
                string fullFileName = GetFullFileName(dt, _minutesPerFile);
                Write(new LogEntry(f, fullFileName));
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventManagerOffset + 1, "Unable to write flash", ex);
            }
        }

        public void WritePortion(LTGFlashPortion ltgPortion, Guid? flashId, DateTime dt)
        {
            try
            {
                Portion portion = new Portion(ltgPortion, flashId, _isTlnSystem);
                WritePortion(portion, flashId, dt);
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventManagerOffset + 2, "Unable to write portion", ex);
            }
        }

        public void WritePortion(Portion portion, Guid? flashId, DateTime dt)
        {
            try
            {
                portion.FlashId = flashId;
                string p = JsonConvert.SerializeObject(portion);
                string fullFileName = GetFullFileName(dt, _minutesPerFile);

                Write(new LogEntry(p, fullFileName));
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventManagerOffset + 3, "Unable to write portion", ex);
            }
        }

        private void WriteLines(Object stateInfo)
        {
            //codes are locked fully.need more efficient method
            lock (locker)
            {
                LogEntry logEntry;
                string currentFileName = null;
                TextWriter textWriter = null;
                try
                {
                    //while (_linesToWrite.TryDequeue(out logEntry))
                    while (_bc.TryTake(out logEntry))
                    {
                        if (textWriter == null || currentFileName != logEntry.FileName)
                        {
                            if (textWriter != null)
                            {
                                textWriter.Close();
                            }
                            textWriter = new StreamWriter(logEntry.FileName, true);
                            currentFileName = logEntry.FileName;
                        }

                        textWriter.WriteLine(logEntry.Line);
                        //textWriter.WriteLine(logEntry.Line);
                    }
                }
                catch (Exception ex)
                {
                    EventManager.LogError(EventManagerOffset + 4, "An error occured while writing the file in FlashDataLogger", ex);
                }
                finally
                {
                    if (textWriter != null)
                    {
                        textWriter.Close();
                    }
                    //Interlocked.Decrement(ref _writerLock);
                }
            }
        }

        private void WritePackets(Object stateInfo)
        {
            lock (locker)
            {
                PacketEntry packetEntry;
                string currentFileName = null;
                BinaryWriter binaryWriter = null;
                try
                {
                    while (_packetsToWrite.TryDequeue(out packetEntry))
                    {
                        if (binaryWriter == null || currentFileName != packetEntry.FileName)
                        {
                            if (binaryWriter != null)
                            {
                                binaryWriter.Close();
                            }

                            binaryWriter = new BinaryWriter(File.OpenWrite(packetEntry.FileName));
                            currentFileName = packetEntry.FileName;
                        }

                        binaryWriter.Write(packetEntry.Bytes);
                    }
                }
                catch (Exception ex)
                {
                    EventManager.LogError(EventManagerOffset + 4, "An error occured while writing the file in FlashDataLogger", ex);
                }
                finally
                {
                    if (binaryWriter != null)
                    {
                        binaryWriter.Close();
                    }
                    //Interlocked.Decrement(ref _writerLock);
                }
            }
        }

        private string GetFullFileName(DateTime utcTime, int minutesPerFile)
        {
            int min = (utcTime.Minute / minutesPerFile) * minutesPerFile;
            string f = string.Format("{0:D4}-{1:D2}-{2:D2}T{3:D2}-{4:D2}", utcTime.Year, utcTime.Month, utcTime.Day, utcTime.Hour, min);
            string fileName = string.Format(_baseFileName, f);
            fileName = Path.Combine(_directory.FullName, fileName);
            return fileName;
        }

        private class LogEntry
        {
            public LogEntry(string line, string fileName)
            {
                Line = line;
                FileName = fileName;
            }

            public string Line { get; private set; }
            public string FileName { get; private set; }
        }

        private class PacketEntry
        {
            public PacketEntry(byte[] bytes, string fileName)
            {
                Bytes = bytes;
                FileName = fileName;
            }

            public byte[] Bytes { get; private set; }
            public string FileName { get; private set; }
        }

        public void WritePacket(List<byte> ltgFlash, DateTime dt)
        {
            try
            {
                string fullFileName = GetFullFileName(dt, _minutesPerFile);
                Write(new PacketEntry(ltgFlash.ToArray(), fullFileName));
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventManagerOffset + 1, "Unable to write flash", ex);
            }
        }

        public int GetFileCount()
        {
            var count = 0;

            if (_directory != null && !string.IsNullOrWhiteSpace(_directory.FullName))
            {
                try
                {
                    if (_directory.Exists)
                    {
                        var fi = _directory.GetFiles("*", SearchOption.TopDirectoryOnly);
                        count = fi.Length;
                    }
                }
                catch (Exception ex)
                {
                    EventManager.LogError(EventManagerOffset + 5, "Unable to get file count", ex);
                }
            }

            return count;
        }

        private static Tuple<int, int> InitThreadPool()
        {
            int logicalProcessors = Environment.ProcessorCount;

            int minWorkerThreads = MinimumClrThreads * logicalProcessors;
            int minIoThreads = MinimumClrThreads * logicalProcessors;
            //int maxWorkerThreads = MaximumClrThreads * logicalProcessors;
            //int maxIoThreads = MaximumClrThreads * logicalProcessors;

            //set the minumum number of worker and IO threads
            //ThreadPool.SetMaxThreads(maxWorkerThreads, maxIoThreads);
            //ThreadPool.SetMinThreads(minWorkerThreads, minIoThreads);
            return Tuple.Create(minWorkerThreads, minIoThreads);
        }
    }
}