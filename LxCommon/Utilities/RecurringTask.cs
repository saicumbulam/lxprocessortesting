﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace LxCommon.Utilities
{
    public class RecurringTask
    {
        public static async Task Create(TimeSpan startDelay, TimeSpan interval, Action action, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (startDelay > TimeSpan.Zero) await Task.Delay(startDelay, cancellationToken);

            while (true)
            {
                cancellationToken.ThrowIfCancellationRequested();
                action();
                await Task.Delay(interval, cancellationToken);
            }
        }
    }
}
