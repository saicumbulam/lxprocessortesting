﻿using System;
using System.IO;

using Aws.Core.Utilities;

using LxCommon.Monitoring;

namespace LxCommon.Utilities
{
    public class FileHelper
    {
        private static readonly ushort EventManagerOffset = TaskMonitor.FileHelper;

        public static DirectoryInfo CreateDirectory(string folder)
        {
            if (folder == null)
            {
                throw new ArgumentNullException("folder");
            }

            DirectoryInfo di = new DirectoryInfo(folder);
            try
            {
                if (!di.Exists)
                    di.Create();
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventManagerOffset, string.Format("Unable to create directory: {0}", di.FullName), ex);
                di = null;
            }

            return di;
        }

        public static DateTime GetFileTimeFromName(string fileName, string filePattern)
        {
            if (fileName == null)
            {
                throw new ArgumentNullException("fileName");
            }

            if (filePattern == null)
            {
                throw new ArgumentNullException("filePattern");
            }

            DateTime fileTime = DateTime.MinValue;
            int start = filePattern.IndexOf('*');
            if (start > 0)
            {
                string left = filePattern.Substring(0, start);
                string right = filePattern.Substring(start + 1);
                string name = fileName.Replace(left, string.Empty);
                name = name.Replace(right, string.Empty);

                int end = name.LastIndexOf('-');
                if (end > 0)
                {
                    name = name.Remove(end, 1);
                    name = name.Insert(end, ":");
                }

                if (DateTime.TryParse(name, out fileTime))
                {
                    fileTime = DateTime.SpecifyKind(fileTime, DateTimeKind.Utc);
                }
            }

            return fileTime;
        }

    }
}
