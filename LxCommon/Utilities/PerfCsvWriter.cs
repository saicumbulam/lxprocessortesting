﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using Aws.Core.Utilities;
using LxCommon.Monitoring;

namespace LxCommon.Utilities
{
    public class PerformanceItem
    {
        public PerformanceCounter Counter { get; set; } 
        public string Header { get; set; }
        public string FormatString { get; set; }
        public Func<double, double> Converter { get; set; }
        public int Order { get; set; }
    }

    public class PerfCsvWriter
    {
        private IDictionary<string, PerformanceItem> _perfItems;
        private readonly CultureInfo _enUsCulture;
        private readonly string DefaultDateTimeFormat = "u";
        private const ushort EventId = TaskMonitor.PerfCsvWriter;
        private readonly DirectoryInfo _dirInfo;
        

        public PerfCsvWriter(IDictionary<string, PerformanceItem> perfItems, string outputFolder)
        {
            if (perfItems == null)
                throw new ArgumentNullException("perfItems");

            if (string.IsNullOrWhiteSpace(outputFolder))
                throw new ArgumentNullException("outputFolder");

            _dirInfo = FileHelper.CreateDirectory(outputFolder);
            
            _enUsCulture = new CultureInfo("en-US");
            _perfItems = perfItems;

        }


        public void RecordData(IDictionary<string, Tuple<DateTime, double>> perfValues)
        {
            //see http://blogs.msdn.com/b/bclteam/archive/2006/06/02/618156.aspx for why you call twice with sleep
            bool hadCounter = false;
            foreach (PerformanceItem item in _perfItems.Values)
            {
                if (item.Counter != null)
                {
                    item.Counter.NextValue();
                    hadCounter = true;
                }
            }

            if (hadCounter)
            {
                Thread.Sleep(1000);
            }

            DateTime now = DateTime.UtcNow;
            StringBuilder line = new StringBuilder(now.ToString(DefaultDateTimeFormat, _enUsCulture));
            IEnumerable<PerformanceItem> query = _perfItems.Values.OrderBy(item => item.Order);
            foreach (PerformanceItem item in query)
            {
                string sValue;
                if (item.Counter != null)
                {
                    sValue = FormatValue(item, item.Counter.NextValue());
                }
                else
                {
                    if (perfValues != null && perfValues.ContainsKey(item.Header))
                    {
                        Tuple<DateTime, double> t = perfValues[item.Header];
                        if (t.Item1 != DateTime.MinValue)
                        {
                            if (!string.IsNullOrWhiteSpace(item.FormatString))
                            {
                                sValue = t.Item1.ToString(item.FormatString, _enUsCulture);
                            }
                            else
                            {
                                sValue = t.Item1.ToString(DefaultDateTimeFormat, _enUsCulture);
                            }
                        }
                        else
                        {
                            sValue = FormatValue(item, t.Item2);
                        }
                    }
                    else
                    {
                        sValue = "0";
                    }
                }

                line.Append(",").Append(sValue);
                
            }
            Console.WriteLine(line);
            WriteToFile(now, line.ToString());
        }

        private string FormatValue(PerformanceItem item, double value)
        {
            string sValue;
            if (item.Converter != null)
            {
                value = item.Converter(value);
            }

            if (!string.IsNullOrWhiteSpace(item.FormatString))
            {
                sValue = value.ToString(item.FormatString);
            }
            else
            {
                sValue = value.ToString(_enUsCulture);
            }

            return sValue;
        }

        private void WriteToFile(DateTime dateTime, string line)
        {
            if (_dirInfo != null)
            {
                TextWriter textWriter = GetTextWriter(_dirInfo, dateTime);
                if (textWriter != null)
                {
                    using (textWriter)
                    {
                        try
                        {
                            textWriter.WriteLine(line);
                        }
                        catch (Exception ex)
                        {
                            EventManager.LogError(EventId + 1, "An error occured while writing the PerfCsvWriter", ex);
                        }
                    }
                }

                string fileName = string.Format("{0:D4}-{1:D2}-{2:D2}", dateTime.Year, dateTime.Month, dateTime.Day);

            }
        }

        private TextWriter GetTextWriter(DirectoryInfo dirInfo, DateTime dateTime)
        {
            string fileName = Path.Combine(dirInfo.FullName, string.Format("{0:D4}-{1:D2}-{2:D2}.csv", dateTime.Year, dateTime.Month, dateTime.Day));

            TextWriter textWriter = null;
            try
            {
                bool writeHeader = !File.Exists(fileName);
                textWriter = new StreamWriter(fileName, true);
                if (writeHeader)
                {
                    WriteHeader(textWriter);
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventId + 2, "Failure creating text writer for PerfCsvWriter", ex);
            }

            return textWriter;
        }

        private void WriteHeader(TextWriter textWriter)
        {
            textWriter.Write("Time Stamp");
            IEnumerable<PerformanceItem> query = _perfItems.Values.OrderBy(item => item.Order);
            foreach (PerformanceItem performanceItem in query)
            {
                textWriter.Write(",");
                textWriter.Write(performanceItem.Header);
            }
            textWriter.WriteLine();
        }
    }
}
