﻿using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace LxCommon.Utilities
{
    public static class StreamExtensions
    {
        public static IEnumerable<string> Lines(this Stream stream)
        {
            using (var reader = new StreamReader(stream))
            {
                while (!reader.EndOfStream)
                {
                    yield return reader.ReadLine();
                }
            }
        }

        public static IEnumerable<string> Lines(this Stream stream, CancellationToken cancellationToken)
        {
            using (var reader = new StreamReader(stream))
            {
                while (!reader.EndOfStream)
                {
                    cancellationToken.ThrowIfCancellationRequested();
                    yield return reader.ReadLine();
                }
            }
        }
    }
}
