using System;

namespace LxCommon
{
    public class StationDecibelParameter
    {
        public StationDecibelParameter()
        {
            DcOffsetInVolts = 0.0f;
            ExternalNoiseInVolts = 0.0f;
            InternalNoiseInVolts = 0.0f;
            EffectiveLengthInMeter = 0.0f;
            Decibel = 0;
            Valid = false;
        }

        public bool Valid { get; set; }

        public string StationId { get; set; }

        public int Decibel { get; set; }

        public DateTime CalibrationDate { get; set; }

        public float EffectiveLengthInMeter { get; set; }

        public float InternalNoiseInVolts { get; set; }

        public float ExternalNoiseInVolts { get; set; }

        public float DcOffsetInVolts { get; set; }

        public string Stats { get; set; }
    }

    public class StationCalibrationData
    {
        private int _activeParameterNum = 2;  // The current attenuation setting
        private readonly StationDecibelParameter[] _parameters = new StationDecibelParameter[4];  // for dB 0, -20, -40, -60

        public StationCalibrationData()
        {
            CalibrationInfoLoaded = false;
            for (int i = 0; i < 4; i++)
            {
                _parameters[i] = new StationDecibelParameter();
            }
        }

        //TODO delete this
        public void UpdateCalibrationData(string stationID, int dB, DateTime calibrationDate, float effectiveLengthInMeter,
            float internalNoiseInVolts, float externalNoiseInVolts, float dcOffsetInVolts)
        {
            StationDecibelParameter parameter = new StationDecibelParameter();
            parameter.StationId = stationID;
            parameter.Decibel = dB;
            parameter.CalibrationDate = calibrationDate;
            parameter.EffectiveLengthInMeter = effectiveLengthInMeter;
            parameter.InternalNoiseInVolts = internalNoiseInVolts;
            parameter.ExternalNoiseInVolts = externalNoiseInVolts;
            parameter.DcOffsetInVolts = dcOffsetInVolts;
            parameter.Valid = true;
            UpdateCalibrationData(parameter);
        }

        public void UpdateCalibrationData(StationDecibelParameter parameter)
        {
            if (parameter != null)
            {
                int nNum = GetParameterNum(parameter.Decibel);
                _parameters[nNum] = parameter;
            }
        }

        public void UpdateActiveCalirationNum(int decibel)
        {
            if (decibel < 0 || decibel > 60)
                return;

            _activeParameterNum = GetParameterNum(decibel);

        }

        private int GetParameterNum(int decibel)
        {
            int parameterNum = 0;
            switch (decibel)
            {
                case 0:
                    parameterNum = 0;
                    break;
                case -20:
                case 20:
                    parameterNum = 1;
                    break;
                case -40:
                case 40:
                    parameterNum = 2;
                    break;
                case -60:
                case 60:
                    parameterNum = 3;
                    break;
                default:
                    parameterNum = 0;
                    break;
            }
            return parameterNum;
        }

        public StationDecibelParameter GetActiveCalibrationData()
        {
            return _parameters[_activeParameterNum];
        }

        public bool CalibrationInfoLoaded { get; set; }

        public int ActiveParameterNum
        {
            get { return _activeParameterNum; }
            set { _activeParameterNum = value; }

        }
    }
}
