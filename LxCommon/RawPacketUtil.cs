using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Net;
using Aws.Core.Utilities;
using LxCommon.Models;
using LxCommon.Monitoring;

namespace LxCommon
{
    public class WaveformComparer : IComparer
    {
        // distance minus offset
        int IComparer.Compare(Object x, Object y)
        {
            return ((Waveform)x).TimeStamp.Nanoseconds.CompareTo(((Waveform)y).TimeStamp.Nanoseconds);
        }
    }

    public class RawPacketUtil
    {
        // Get the length from a packet header
        public static int GetPacketHeaderLength(PacketHeaderInfo header)
        {
            return GetPacketHeaderLength((byte)header.magicNum, (byte)header.MsgType, (byte)header.MajorVersion, (byte)header.MinorVersion);
        }

        // Return the length of the header
        // -1 indicate the packet is bad
        // For QPS the legnth is the total length of the packet
        public static int GetPacketHeaderLength(byte themagicNum, byte themsgType, byte themajorVersion, byte theminorVersion)
        {
            PacketHeaderLength headerLength = PacketHeaderLength.HeaderLengthUnknown; // -1;
            PacketType msgType = (PacketType)themsgType;
            PacketMagicNumber magicNum = (PacketMagicNumber)themagicNum;
            SensorVersion majorVersion = (SensorVersion)themajorVersion;
            SensorVersion minorVersion = (SensorVersion)theminorVersion;

            if (magicNum == PacketMagicNumber.MagicSensor8Packet) // Sensor 8
            {  // Sensor 8 dosen't have version
                if (msgType == PacketType.MsgTypeLtg)// Sensor 8: lightning
                {
                    headerLength = PacketHeaderLength.HeaderLengthLtg8; // 11;
                }
                else if (msgType == PacketType.MsgTypeGps)// Sensor 8: GPS
                {
                    headerLength = PacketHeaderLength.HeaderLengthGps8; //236;
                }
            }
            else if (magicNum == PacketMagicNumber.MagicSensor9Packet) // Sensor 9 and 10
            {
                if (msgType == PacketType.MsgTypeLtg)// Sensor 9: Lightning
                {
                    if (majorVersion == SensorVersion.ID_VERSION_SENSOR_9_MAJOR)
                    {
                        if (minorVersion == SensorVersion.ID_VERSION_SENSOR_9_MINOR0) // First sensor 9 raw daata
                        {
                            headerLength = PacketHeaderLength.HeaderLengthLtg90; //27;
                        }
                        else if (minorVersion == SensorVersion.ID_VERSION_SENSOR_9_MINOR1) // Version 9.1
                        {
                            headerLength = PacketHeaderLength.HeaderLengthLtg91; //38;
                        }
                        else if (minorVersion == SensorVersion.ID_VERSION_SENSOR_9_MINOR2) // Version9.2
                        {
                            headerLength = PacketHeaderLength.HeaderLengthLtg92; //50;
                        }
                        else if (minorVersion >= SensorVersion.ID_VERSION_SENSOR_9_MINOR3) // Version 9.3 and 9.4
                        {
                            headerLength = PacketHeaderLength.HeaderLengthLtg93; //58;
                        }
                    }
                    else if (majorVersion == SensorVersion.ID_VERSION_SENSOR_10_MAJOR)
                    {
                        if (minorVersion == SensorVersion.ID_VERSION_SENSOR_10_MINOR0)
                        {
                            headerLength = PacketHeaderLength.HeaderLengthLtg100;
                        }
                        if (minorVersion == SensorVersion.ID_VERSION_SENSOR_10_MINOR1)
                        {
                            headerLength = PacketHeaderLength.HeaderLengthLtg101;
                        }
                    }

                }
                else if (msgType == PacketType.MsgTypeGps)// Sensor 9: GPS
                {
                    headerLength = PacketHeaderLength.HeaderLengthGps; //246;
                }
                else if (msgType == PacketType.MsgTypeLog) // Sensor 9: Log
                {
                    headerLength = PacketHeaderLength.HeaderLengthLog; //16;
                }
                else if (msgType == PacketType.MsgTypeSpectra)
                {
                    headerLength = PacketHeaderLength.HeaderLengthSpectra; // 42
                }
                else
                {

                }
            }
            return (int)headerLength;
        }

        // Get the type and version information from the raw data
        public static bool GetPacketHeaderTypeVersion(byte[] data, PacketHeaderInfo packetHeaderInfo)
        {
            if (data == null || data.Length < 1)
                return false;

            // Check whether this is an WWLLN packet, which doesn't have a magic number. Check size and the fist byte
            if (data.Length == (int)PacketHeaderLength.HeaderLengthWWLLNUdp)
            {
                int sensorID = (int)data[0];
                if (sensorID > 0 && sensorID < 100)
                {
                    packetHeaderInfo.magicNum = PacketMagicNumber.MagicSensorWWLLN;
                    packetHeaderInfo.MsgType = PacketType.MsgTypeWWLLNLtg;
                    packetHeaderInfo.TotalLength = data.Length;
                    packetHeaderInfo.RawData = data;
                    return true;
                }
            }


            string sensorId = "";
            PacketType nMsgType = PacketType.MsgUnknow; //type in the UDP
            short nMajorVer = 0;
            short nMinorVer = 0;
            int nTotalLen = 0;

            byte[] b1b = new byte[1];
            byte[] b2b = new byte[2];
            byte[] b4b = new byte[4];
            byte[] b10b = new byte[10];

            int j = 0;
            b1b[0] = data[j++];
            PacketMagicNumber magic = (PacketMagicNumber)ByteArrayToInt(b1b, 1);
            packetHeaderInfo.magicNum = magic;

            if (magic == PacketMagicNumber.MagicKeepAlive) // A keep alive packet
            {
                nMsgType = PacketType.MsgTypeKeepAlive;
                nTotalLen = 1;
            }
            else if (magic == PacketMagicNumber.MagicFlashPacket) // A flash packet sent from processor
            {
                nMsgType = PacketType.MsgTypeFlash;
                nTotalLen = data.Length;
            }
            else if (magic == PacketMagicNumber.MagicSensor9Packet) // Sensor 9
            {
                //#region Sensor 9
                //first 14 bytes is accessed here for header info
                if (data.Length < 14)
                    return false;

                //msg trye
                b1b[0] = data[j++];
                nMsgType = (PacketType)ByteArrayToInt(b1b, 1);

                //packetHeaderInfo.MsgType = nMsgType;

                if (nMsgType != PacketType.MsgTypeLtg &&
                    nMsgType != PacketType.MsgTypeGps &&
                    nMsgType != PacketType.MsgTypeLog &&
                    nMsgType != PacketType.MsgTypeSpectra)
                {
                    return false; // Only handle the known data (lightnig, GPS data, log
                }

                // version 2 bytes
                b1b[0] = data[j++];
                nMajorVer = (short)ByteArrayToInt(b1b, 1);

                b1b[0] = data[j++];
                nMinorVer = (short)ByteArrayToInt(b1b, 1);

                //station id, 10 bytes
                for (int ii = 0; ii < 10; ii++)
                {
                    b10b[ii] = data[j++];
                }
                sensorId = System.Text.Encoding.ASCII.GetString(b10b);
                sensorId = sensorId.Trim();
                int k = sensorId.IndexOf('\0');
                if (k > 0)
                {
                    sensorId = sensorId.Substring(0, k);
                }
                packetHeaderInfo.SensorId = sensorId.ToUpper();
                //packetHeaderInfo.MsgType = nMsgType;
                packetHeaderInfo.MajorVersion = (SensorVersion)nMajorVer;
                packetHeaderInfo.MinorVersion = (SensorVersion)nMinorVer;
            }
            else if (magic == PacketMagicNumber.MagicSensor8Packet)
            {
                b1b[0] = data[1]; //only second byte meaningful
                nMsgType = (PacketType)ByteArrayToInt(b1b, 1);
                b2b[0] = data[2];
                b2b[1] = data[3];
                int nSensorId = IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2));
                sensorId = Convert.ToString(nSensorId);
                packetHeaderInfo.SensorId = sensorId.ToUpper();
                packetHeaderInfo.MajorVersion = SensorVersion.ID_VERSION_SENSOR_8;
                packetHeaderInfo.MinorVersion = SensorVersion.ID_VERSION_SENSOR_8_MINOR0;
            }
            else if (magic == PacketMagicNumber.MagicSensorWWLLN)
            {  // This is WWLLN sensor TOGA data
                nMsgType = PacketType.MsgTypeWWLLNLtg;
            }
            else
            {
                return false;
            }
            packetHeaderInfo.MsgType = nMsgType;
            packetHeaderInfo.TotalLength = nTotalLen;

            return CheckPacketLength(packetHeaderInfo);
        }

        public static bool GetWWLLNFlashData(string data, PacketHeaderInfo packetHeaderInfo)
        {
            try
            {
                //sample data format "W120,2012-03-06T15:23:18.195810,41.5694,-58.3138,0,8.1,5,7,38,40,41,60,323186" 
                //(W is the magic number, data may have the magic numbe removed)
                //format: version, time, lat, long, energy (not used), residual in millisec (not used), 
                //        num of sensor, [list of sensors], sequence number (for debug)
                if (string.IsNullOrEmpty(data))
                {
                    return false;
                }
                string[] s = data.Split(',');
                if (s.Length < 6)
                {
                    return false;
                }
                packetHeaderInfo.magicNum = PacketMagicNumber.MagicFlashWWLLN;
                packetHeaderInfo.MsgType = PacketType.MsgTypeFlash;
                packetHeaderInfo.MajorVersion = SensorVersion.ID_VERSION_UNKNOW;
                packetHeaderInfo.PacketTimeInSeconds = LtgTimeUtils.GetSecondsSince1970FromDateTime(DateTime.Parse(s[1]));
                LTGFlash flashData = new LTGFlash();
                flashData.Latitude = double.Parse(s[2]);
                flashData.Longitude = double.Parse(s[3]);
                flashData.FlashType = FlashType.FlashTypeGlobalCG;
                flashData.FlashTime = s[1];
                
                flashData.Confidence = 100;

                flashData.Description = "";

                flashData.FlashPortionList = new ArrayList();
                LTGFlashPortion pulse = new LTGFlashPortion();
                pulse.Amplitude = 0;

                pulse.Confidence = 100;
                pulse.Description = "";
                pulse.FlashTime = s[1];
                pulse.FlashType = FlashType.FlashTypeGlobalCG;
                pulse.Height = 0;
                pulse.Latitude = flashData.Latitude;
                pulse.Longitude = flashData.Longitude;
                
                int numSensor = int.Parse(s[6]);
                if (s.Length == 8 + numSensor)
                {
                    int endPivot = s.Length - 1;
                    for (int i = 7; i < endPivot; i++)
                    {
                        Offset offset = new Offset { StationId = s[i], Value = 0 };
                        pulse.OffsetList.Add(offset);
                    }
                }
                else
                {
                    pulse.OffsetList.Add(new Offset { StationId = "NumSensor", Value = numSensor });
                }
                flashData.AddFlashPortion(pulse);
                packetHeaderInfo.RawData = RawPacketUtil.EncodeLtgFlashToManager(flashData);
                packetHeaderInfo.TotalLength = packetHeaderInfo.RawData.Length;
                packetHeaderInfo.OtherObject = data;
                return true;
            }
            catch (Exception ie)
            {
                return false;
            }
        }

        // the data from WWLLN network
        public static bool GetWWLLNPacketHeaderInfo(byte[] data, PacketHeaderInfo packetHeaderInfo)
        {
            if (data == null || data.Length != (int)PacketHeaderLength.HeaderLengthWWLLNUdp)
                return false;

            byte[] b4 = new byte[4]; // 4 bytes
            byte[] b2 = new byte[2]; // 2 bytes
            int i = 0; // index of the buffer

            byte id = data[i++]; // 0: site ID
            byte version = data[i++]; // 1: site version
            // 2 -3: padding
            i = 4;

            // 4 - 7: time stamp
            for (int j = 0; j < 4; j++)
                b4[j] = data[i++];

            int timeStamp = ByteArrayToInt(b4, 4);
            // 8 - 11
            for (int j = 0; j < 4; j++)
                b4[j] = data[i++];

            int microsecond = ByteArrayToInt(b4, 4);

            // 12-13: toga 0
            for (int j = 0; j < 2; j++)
                b2[j] = data[i++];

            // 14-15: sequence number
            for (int j = 0; j < 2; j++)
                b2[j] = data[i++];
            int sequenceNum = ByteArrayToInt(b2, 2);

            // 16-17: energy
            for (int j = 0; j < 2; j++)
                b2[j] = data[i++];

            int energy = ByteArrayToInt(b2, 2);

            packetHeaderInfo.magicNum = PacketMagicNumber.MagicSensorWWLLN;
            packetHeaderInfo.MsgType = PacketType.MsgTypeWWLLNLtg;
            packetHeaderInfo.SensorId = id.ToString();  // First byte is sensor ID
            packetHeaderInfo.PacketTimeInSeconds = timeStamp;
            packetHeaderInfo.OffsetInNanoseconds = microsecond * 1000; // Convert to nanosecond
            packetHeaderInfo.PpsTickNumber = sequenceNum;
            packetHeaderInfo.HFThreshold = energy; //Borrow the field
            packetHeaderInfo.TotalLength = data.Length;
            packetHeaderInfo.RawData = data;
            return true;
        }

        // The data array should have at least the length of the header of the packet
        public static bool GetPacketHeaderInfo(byte[] data, PacketHeaderInfo packetHeaderInfo)
        {
            int nSecond = -1;
            int nTotalLen = 0;
            int j = 0;
            if (!GetPacketHeaderTypeVersion(data, packetHeaderInfo))
            {
                return false;
            }

            if (packetHeaderInfo.magicNum == PacketMagicNumber.MagicSensorWWLLN)
            {
                return GetWWLLNPacketHeaderInfo(data, packetHeaderInfo);
            }

            byte[] b1b = new byte[1];
            byte[] b2b = new byte[2];
            byte[] b4b = new byte[4];
            byte[] b10b = new byte[10];

            packetHeaderInfo.RawData = data;

            if (packetHeaderInfo.magicNum == PacketMagicNumber.MagicSensor9Packet) // Sensor 9
            {
                #region Sensor 9
                if (packetHeaderInfo.MsgType == PacketType.MsgTypeLtg)
                {
                    if (packetHeaderInfo.MajorVersion == SensorVersion.ID_VERSION_SENSOR_9_MAJOR)
                    {
                        if (packetHeaderInfo.MinorVersion == SensorVersion.ID_VERSION_SENSOR_9_MINOR0)
                        {
                            return GetPacketHeaderInfoFor9_0(data, packetHeaderInfo);
                        }
                        else if (packetHeaderInfo.MinorVersion == SensorVersion.ID_VERSION_SENSOR_9_MINOR1)
                        { // First lightning raw data with compression
                            return GetPacketHeaderInfoFor9_1(data, packetHeaderInfo);
                        }
                        else if (packetHeaderInfo.MinorVersion == SensorVersion.ID_VERSION_SENSOR_9_MINOR2)
                        { // LF dta added
                            return GetPacketHeaderInfoFor9_2(data, packetHeaderInfo);
                        }
                        else if (packetHeaderInfo.MinorVersion >= SensorVersion.ID_VERSION_SENSOR_9_MINOR3)
                        {   // LF dta added
                            // SensorVersion.ID_VERSION_SENSOR_9_MINOR4 has the same Header as ID_VERSION_SENSOR_9_MINOR3
                            return GetPacketHeaderInfoFor9_3(data, packetHeaderInfo);
                        }
                        else
                        { // Unknow minor version
                            return false;
                        }
                    }
                    else if (packetHeaderInfo.MajorVersion == SensorVersion.ID_VERSION_SENSOR_10_MAJOR)
                    {
                        // Version 10                    
                        if (packetHeaderInfo.MinorVersion == SensorVersion.ID_VERSION_SENSOR_10_MINOR0)
                        {
                            return GetPacketHeaderInfoFor10_0(data, packetHeaderInfo);
                        }
                        if (packetHeaderInfo.MinorVersion == SensorVersion.ID_VERSION_SENSOR_10_MINOR1)
                        {
                            return GetPacketHeaderInfoFor10_1(data, packetHeaderInfo);
                        }
                        else
                        {
                            // unknown minor version
                            return false;
                        }

                    }
                    else
                    { // Unknown Major version
                        return false;
                    }
                }
                else if (packetHeaderInfo.MsgType == PacketType.MsgTypeGps)
                {
                    if (data.Length < 14)
                        return false;

                    nTotalLen = 246;
                    packetHeaderInfo.TotalLength = nTotalLen;
                    packetHeaderInfo.RawData = data;
                    GetGpsData(packetHeaderInfo);
                }
                else if (packetHeaderInfo.MsgType == PacketType.MsgTypeLog)
                {
                    if (data.Length < 16)
                        return false;
                    j = 14;
                    b2b[0] = data[j++]; b2b[1] = data[j++];
                    nTotalLen = j + (UInt16)IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2));
                    packetHeaderInfo.TotalLength = nTotalLen;
                    packetHeaderInfo.RawData = data;
                }
                else if (packetHeaderInfo.MsgType == PacketType.MsgTypeSpectra)
                {
                    return GetPacketHeaderForSpectraData(data, packetHeaderInfo);

                }
                else
                {
                    return false; //  Will finish this later
                }
                #endregion Sensor 9
            }
            else if (packetHeaderInfo.magicNum == PacketMagicNumber.MagicSensor8Packet)// Sensor 8
            {
                #region sensor 8

                if (packetHeaderInfo.MsgType == PacketType.MsgTypeLtg)
                {
                    return GetPacketHeaderInforFor8_0(data, packetHeaderInfo);
                }
                else if (packetHeaderInfo.MsgType == PacketType.MsgTypeGps)
                {
                    nTotalLen = 4 + 154 + 78;
                    packetHeaderInfo.TotalLength = nTotalLen;
                    packetHeaderInfo.RawData = data;
                    GetGpsData(packetHeaderInfo);
                }
                #endregion Sensor 8

            }
            return CheckPacketLength(packetHeaderInfo);
        }

        private static bool CheckPacketLength(PacketHeaderInfo packetHeaderInfo)
        {
            if (packetHeaderInfo.MsgType == PacketType.MsgTypeLog &&
                (packetHeaderInfo.TotalLength > LtgConstants.MaxLogPacketSize || packetHeaderInfo.TotalLength < 0))
            {
                return false;

            }
            if (packetHeaderInfo.MsgType == PacketType.MsgTypeLtg &&
                (packetHeaderInfo.TotalLength > LtgConstants.MaxLightningPacketSize || packetHeaderInfo.TotalLength < 0))
            {
                return false;
            }
            return true;

        }

        private static bool GetPacketHeaderInforFor8_0(byte[] data, PacketHeaderInfo packetHeaderInfo)
        {
            int nSecond = -1;
            int nTotalLen = 0;
            int nOffset = -1;

            if (data.Length < 11)
                return false;

            byte[] b1b = new byte[1];
            byte[] b2b = new byte[2];
            byte[] b4b = new byte[4];
            byte[] b10b = new byte[10];

            int j = 4;
            //chech time
            j = 4;
            b4b[0] = data[j++]; b4b[1] = data[j++]; b4b[2] = data[j++]; b4b[3] = data[j++];

            //Offset in Nanoseconds
            b1b[0] = data[j++];
            nOffset = ByteArrayToInt(b1b, 1);

            j = 9;
            b2b[0] = data[j++]; b2b[1] = data[j++];
            nTotalLen = j + IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2));
            nSecond = IPAddress.NetworkToHostOrder(ByteArrayToInt(b4b, 4));

            packetHeaderInfo.PacketTimeInSeconds = nSecond;
            packetHeaderInfo.TotalLength = nTotalLen;
            packetHeaderInfo.OffsetInNanoseconds = nOffset;
            packetHeaderInfo.RawData = data;
            return CheckPacketLength(packetHeaderInfo);
        }

        private static bool GetPacketHeaderInfoFor9_0(byte[] data, PacketHeaderInfo packetHeaderInfo)
        {
            int nSecond = -1;
            int nTotalLen = 0;
            int nOffset = -1;
            int nSclkTicksPpsToFirstSample = -1;
            int nSclkTicksBetweenSamples = -1;
            int nSclkTicksInThisSec = -1;

            //first 27 bytes is accessed here for header info
            if (data.Length < 27)
                return false;

            byte[] b1b = new byte[1];
            byte[] b2b = new byte[2];
            byte[] b4b = new byte[4];
            byte[] b10b = new byte[10];

            int j = 14;
            //chech time
            b4b[0] = data[j++];
            b4b[1] = data[j++];
            b4b[2] = data[j++];
            b4b[3] = data[j++];
            nSecond = IPAddress.NetworkToHostOrder(ByteArrayToInt(b4b, 4));

            //Offset in Nanoseconds
            b1b[0] = data[j++];
            nOffset = ByteArrayToInt(b1b, 1) + LtgConstants.TickTimeOffsetInNanoseconds;  // the sensor had a bug with version 9.0, needs to subtract 785 nanoseconds from the time

            //SCLK ticks between pps and first sample
            b1b[0] = data[j++];
            nSclkTicksPpsToFirstSample = ByteArrayToInt(b1b, 1);

            //SCLK ticks between samples
            b1b[0] = data[j++];
            nSclkTicksBetweenSamples = ByteArrayToInt(b1b, 1);

            //SCLK ticks in this second
            b4b[0] = data[j++];
            b4b[1] = data[j++];
            b4b[2] = data[j++];
            b4b[3] = data[j++];
            nSclkTicksInThisSec = IPAddress.NetworkToHostOrder(ByteArrayToInt(b4b, 4));

            //total packet length
            b2b[0] = data[j++]; b2b[1] = data[j++];
            int ndataLen = IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2));
            nTotalLen = j + ndataLen;

            packetHeaderInfo.PacketTimeInSeconds = nSecond;
            packetHeaderInfo.TotalLength = nTotalLen;
            packetHeaderInfo.HFSclkTicksBetweenSamples = nSclkTicksBetweenSamples;
            packetHeaderInfo.HFSclkTicksInThisSec = nSclkTicksInThisSec;
            packetHeaderInfo.HFSclkTicksPpsToFirstSample = nSclkTicksPpsToFirstSample;
            packetHeaderInfo.OffsetInNanoseconds = nOffset;
            packetHeaderInfo.RawData = data;
            return CheckPacketLength(packetHeaderInfo);
        }

        private static bool GetPacketHeaderInfoFor9_1(byte[] data, PacketHeaderInfo packetHeaderInfo)
        {
            int nSecond = -1;
            int nTotalLen = 0;
            int nOffset = -1;
            int nSclkTicksPpsToFirstSample = -1;
            int nSclkTicksBetweenSamples = -1;
            int nSclkTicksInThisSec = -1;
            int j = 14;

            if (data.Length < (int)PacketHeaderLength.HeaderLengthLtg91)
                return false;

            byte[] b1b = new byte[1];
            byte[] b2b = new byte[2];
            byte[] b4b = new byte[4];
            byte[] b10b = new byte[10];

            //check time
            b4b[0] = data[j++];
            b4b[1] = data[j++];
            b4b[2] = data[j++];
            b4b[3] = data[j++];
            nSecond = IPAddress.NetworkToHostOrder(ByteArrayToInt(b4b, 4));

            //Offset in Nanoseconds
            b1b[0] = data[j++];
            nOffset = ByteArrayToInt(b1b, 1) + LtgConstants.TickTimeOffsetInNanoseconds;  // the sensor had a bug with version 9.1, needs to subtract 785 nanoseconds from the time

            //SCLK ticks between pps and first sample
            b1b[0] = data[j++];
            nSclkTicksPpsToFirstSample = ByteArrayToInt(b1b, 1);

            //SCLK ticks between samples
            b1b[0] = data[j++];
            nSclkTicksBetweenSamples = ByteArrayToInt(b1b, 1);

            //SCLK ticks in this second
            b4b[0] = data[j++];
            b4b[1] = data[j++];
            b4b[2] = data[j++];
            b4b[3] = data[j++];
            nSclkTicksInThisSec = IPAddress.NetworkToHostOrder(ByteArrayToInt(b4b, 4));

            // Attenuation
            j = 25;
            b1b[0] = data[j];
            byte nAttenuation = (byte)ByteArrayToInt(b1b, 1);

            j = 26;
            // bytes 26, 27 are threshold (debug code in little endian format)
            b2b[0] = data[j++]; b2b[1] = data[j++];
            int nThreshold = ByteArrayToInt(b2b, 2);

            //total packet length
            j = 36;
            b2b[0] = data[j++]; b2b[1] = data[j++];
            int ndataLen = IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2));
            nTotalLen = j + ndataLen;

            packetHeaderInfo.PacketTimeInSeconds = nSecond;
            packetHeaderInfo.TotalLength = nTotalLen;
            packetHeaderInfo.HFSampleNum = ndataLen / 6;
            packetHeaderInfo.HFSclkTicksBetweenSamples = nSclkTicksBetweenSamples;
            packetHeaderInfo.HFSclkTicksInThisSec = nSclkTicksInThisSec;
            packetHeaderInfo.HFSclkTicksPpsToFirstSample = nSclkTicksPpsToFirstSample;
            packetHeaderInfo.SensorAttenuation = nAttenuation;
            packetHeaderInfo.OffsetInNanoseconds = nOffset;
            packetHeaderInfo.HFThreshold = nThreshold;
            packetHeaderInfo.RawData = data;
            return CheckPacketLength(packetHeaderInfo);
        }

        // Short lived format, not used
        private static bool GetPacketHeaderInfoFor9_2(byte[] data, PacketHeaderInfo packetHeaderInfo)
        {
            int nSecond = -1;
            int nTotalLen = 0;
            int nOffset = -1;
            int nSclkTicksPpsToFirstSample = -1;
            int nSclkTicksBetweenSamples = -1;
            int nSclkTicksInThisSec = -1;
            int j = 14;

            if (data.Length < (int)PacketHeaderLength.HeaderLengthLtg92)
                return false;

            byte[] b1b = new byte[1];
            byte[] b2b = new byte[2];
            byte[] b4b = new byte[4];
            byte[] b10b = new byte[10];

            //check time
            b4b[0] = data[j++];
            b4b[1] = data[j++];
            b4b[2] = data[j++];
            b4b[3] = data[j++];
            nSecond = IPAddress.NetworkToHostOrder(ByteArrayToInt(b4b, 4));

            //Offset in Nanoseconds
            b1b[0] = data[j++];
            nOffset = ByteArrayToInt(b1b, 1);

            //byte: 19, SCLK ticks between pps and first sample
            b1b[0] = data[j++];
            nSclkTicksPpsToFirstSample = ByteArrayToInt(b1b, 1);

            //byte: 20,SCLK ticks between samples
            b1b[0] = data[j++];
            nSclkTicksBetweenSamples = ByteArrayToInt(b1b, 1);

            //bytes: 21-24, SCLK ticks in this second
            b4b[0] = data[j++];
            b4b[1] = data[j++];
            b4b[2] = data[j++];
            b4b[3] = data[j++];
            nSclkTicksInThisSec = IPAddress.NetworkToHostOrder(ByteArrayToInt(b4b, 4));

            // Byte: 25, Attenuation
            j = 25;
            b1b[0] = data[j];
            byte nAttenuation = (byte)ByteArrayToInt(b1b, 1);

            //HF data packet length
            j = 46;
            b2b[0] = data[j++]; b2b[1] = data[j++];
            int ndataLenHF = IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2));

            // LF data packet length
            b2b[0] = data[j++]; b2b[1] = data[j++];
            int ndataLenLF = IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2));

            // Total length including the header
            nTotalLen = (int)PacketHeaderLength.HeaderLengthLtg92 + ndataLenHF + ndataLenLF;

            packetHeaderInfo.PacketTimeInSeconds = nSecond;
            packetHeaderInfo.TotalLength = nTotalLen;
            packetHeaderInfo.HFSclkTicksBetweenSamples = nSclkTicksBetweenSamples;
            packetHeaderInfo.HFSclkTicksInThisSec = nSclkTicksInThisSec;
            packetHeaderInfo.HFSclkTicksPpsToFirstSample = nSclkTicksPpsToFirstSample;
            packetHeaderInfo.SensorAttenuation = nAttenuation;
            packetHeaderInfo.OffsetInNanoseconds = nOffset;
            packetHeaderInfo.RawData = data;
            return CheckPacketLength(packetHeaderInfo);
        }

        private static bool GetPacketHeaderInfoFor9_3(byte[] data, PacketHeaderInfo packetHeaderInfo)
        {
            int nSecond = -1;
            int nTotalLen = 0;
            int nOffset = -1;
            int nSclkTicksPpsToFirstSampleHF = -1;
            int nSclkTicksBetweenSamplesHF = -1;
            int nSclkTicksInThisSecHF = -1;
            int j = 14;

            if (data.Length < (int)PacketHeaderLength.HeaderLengthLtg93)
                return false;

            byte[] b1b = new byte[1];
            byte[] b2b = new byte[2];
            byte[] b4b = new byte[4];
            byte[] b10b = new byte[10];

            //check time
            b4b[0] = data[j++]; b4b[1] = data[j++]; b4b[2] = data[j++]; b4b[3] = data[j++];
            nSecond = IPAddress.NetworkToHostOrder(ByteArrayToInt(b4b, 4));

            //Offset in Nanoseconds
            b1b[0] = data[j++];
            nOffset = ByteArrayToInt(b1b, 1);

            //byte: 19, SCLK ticks between pps and first sample for HF
            b1b[0] = data[j++];
            nSclkTicksPpsToFirstSampleHF = ByteArrayToInt(b1b, 1);

            //byte: 20,SCLK ticks between samples for HF
            b1b[0] = data[j++];
            nSclkTicksBetweenSamplesHF = ByteArrayToInt(b1b, 1);

            //bytes: 21-24, SCLK ticks in this second for HF
            b4b[0] = data[j++];
            b4b[1] = data[j++];
            b4b[2] = data[j++];
            b4b[3] = data[j++];
            nSclkTicksInThisSecHF = IPAddress.NetworkToHostOrder(ByteArrayToInt(b4b, 4));

            // Byte: 25, Attenuation in dB
            j = 25;
            b1b[0] = data[j++];
            byte nAttenuation = (byte)ByteArrayToInt(b1b, 1);

            // Byte 26-27: HF threshold
            b2b[0] = data[j++]; b2b[1] = data[j++];
            int nthresholdHF = IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2));

            // LF data
            //byte: 28, SCLK ticks between pps and first sample for LF
            b1b[0] = data[j++];
            int nSclkTicksPpsToFirstSampleLF = ByteArrayToInt(b1b, 1);

            //byte: 29,SCLK ticks between samples for LF
            b1b[0] = data[j++];
            int nSclkTicksBetweenSamplesLF = ByteArrayToInt(b1b, 1);

            // byte: 30-31 LF threshold
            b2b[0] = data[j++]; b2b[1] = data[j++];
            int nthresholdLF = IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2));

            // byte: 32-33 HF DC offset
            b2b[0] = data[j++]; b2b[1] = data[j++];
            int dcOffsetHF = IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2));

            // byte: 34-35 LF DC offset
            b2b[0] = data[j++]; b2b[1] = data[j++];
            int dcOffsetLF = IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2));

            // Bytes 36-45 reserved

            // Byte 46-49 SCLK ticks between spectra samples
            j = 46;
            b4b[0] = data[j++]; b4b[1] = data[j++]; b4b[2] = data[j++]; b4b[3] = data[j++];
            int nSclkTicksBetweenSamplesSpectra = IPAddress.NetworkToHostOrder(ByteArrayToInt(b4b, 4));

            // Byte 50 reserved
            // Byte 51: flag 0=NoSpectra 1=Low 2=High, 3=dEdt

            //Byte 52-53: HF data packet length
            j = 52;
            b2b[0] = data[j++]; b2b[1] = data[j++];
            int ndataLenHF = IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2));

            // Byte 54-55: LF data packet length
            j = 54;
            b2b[0] = data[j++]; b2b[1] = data[j++];
            int ndataLenLF = IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2));

            // Byte 56-57: Spectra data packet length
            j = 56;
            b2b[0] = data[j++]; b2b[1] = data[j++];
            int ndataLenSpectra = IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2));

            // Total length including the header
            nTotalLen = (int)PacketHeaderLength.HeaderLengthLtg93 + ndataLenHF + ndataLenLF + ndataLenSpectra;

            packetHeaderInfo.PacketTimeInSeconds = nSecond;
            packetHeaderInfo.SensorAttenuation = nAttenuation;
            packetHeaderInfo.OffsetInNanoseconds = nOffset;

            packetHeaderInfo.HFSclkTicksPpsToFirstSample = nSclkTicksPpsToFirstSampleHF;
            packetHeaderInfo.HFSclkTicksBetweenSamples = nSclkTicksBetweenSamplesHF;
            packetHeaderInfo.HFSclkTicksInThisSec = nSclkTicksInThisSecHF;
            packetHeaderInfo.HFDCOffset = dcOffsetHF;
            packetHeaderInfo.HFSampleNum = ndataLenHF / 6;
            packetHeaderInfo.HFThreshold = nthresholdHF;

            packetHeaderInfo.LFSclkTicksPpsToFirstSample = nSclkTicksPpsToFirstSampleLF;
            packetHeaderInfo.LFSclkTicksBetweenSamples = nSclkTicksBetweenSamplesLF;
            packetHeaderInfo.LFThreshold = nthresholdLF;
            packetHeaderInfo.LFSampleNum = ndataLenLF / 6;
            packetHeaderInfo.LFDCOffset = dcOffsetLF;

            packetHeaderInfo.SpectraSclkTicksBetweenSamples = nSclkTicksBetweenSamplesSpectra;
            packetHeaderInfo.SpectraSampleNum = ndataLenSpectra / 6;

            packetHeaderInfo.TotalLength = nTotalLen;
            packetHeaderInfo.RawData = data;
            return CheckPacketLength(packetHeaderInfo);
        }

        private static bool GetPacketHeaderInfoFor10_0(byte[] data, PacketHeaderInfo packetHeaderInfo)
        {
            int nSecond = -1;
            int nTotalLen = 0;
            int nOffset = -1;
            int nSclkTicksPpsToFirstSampleHF = -1;
            int nSclkTicksBetweenSamplesHF = -1;
            int nSclkTicksInThisSecHF = -1;
            int nSclkTicksBetweenSamplesSpectra = -1;

            int j = 14;

            if (data.Length < (int)PacketHeaderLength.HeaderLengthLtg100)
                return false;

            byte[] b1b = new byte[1];
            byte[] b2b = new byte[2];
            byte[] b4b = new byte[4]; ;
            byte[] b10b = new byte[10];

            //check time
            b4b[0] = data[j++]; b4b[1] = data[j++]; b4b[2] = data[j++]; b4b[3] = data[j++];
            nSecond = IPAddress.NetworkToHostOrder(ByteArrayToInt(b4b, 4));

            //Offset in Nanoseconds
            b1b[0] = data[j++];
            nOffset = ByteArrayToInt(b1b, 1);

            //byte: 19, SCLK ticks between pps and first sample for HF
            b1b[0] = data[j++];
            nSclkTicksPpsToFirstSampleHF = ByteArrayToInt(b1b, 1);

            //byte: 20,SCLK ticks between samples for HF
            b1b[0] = data[j++];
            nSclkTicksBetweenSamplesHF = ByteArrayToInt(b1b, 1);

            //bytes: 21-24, SCLK ticks in this second for HF
            b4b[0] = data[j++];
            b4b[1] = data[j++];
            b4b[2] = data[j++];
            b4b[3] = data[j++];
            nSclkTicksInThisSecHF = IPAddress.NetworkToHostOrder(ByteArrayToInt(b4b, 4));

            // Byte: 25, Attenuation in dB
            j = 25;
            b1b[0] = data[j++];
            byte nAttenuation = (byte)ByteArrayToInt(b1b, 1);

            // Byte 26-27: HF threshold
            b2b[0] = data[j++]; b2b[1] = data[j++];
            int nthresholdHF = IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2));

            // LF data
            //byte: 28, SCLK ticks between pps and first sample for LF
            b1b[0] = data[j++];
            int nSclkTicksPpsToFirstSampleLF = ByteArrayToInt(b1b, 1);

            //byte: 29,SCLK ticks between samples for LF
            b1b[0] = data[j++];
            int nSclkTicksBetweenSamplesLF = ByteArrayToInt(b1b, 1);

            // byte: 30-31 LF threshold
            b2b[0] = data[j++]; b2b[1] = data[j++];
            int nthresholdLF = IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2));

            // byte: 32-33 HF DC offset
            b2b[0] = data[j++]; b2b[1] = data[j++];
            int dcOffsetHF = IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2));

            // byte: 34-35 LF DC offset
            b2b[0] = data[j++]; b2b[1] = data[j++];
            int dcOffsetLF = IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2));

            // Bytes 36-45 reserved

            // Byte 46-49 firmware version (hash)
            j = 46;
            b4b[0] = data[j++]; b4b[1] = data[j++]; b4b[2] = data[j++]; b4b[3] = data[j++];
            int firmwareVersion = IPAddress.NetworkToHostOrder(ByteArrayToInt(b4b, 4));

            // Byte 50 to 51: length in bytes of entire message
            b2b[0] = data[j++]; b2b[1] = data[j++];
            nTotalLen = IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2));

            //Byte 52-53: HF data packet length
            j = 52;
            b2b[0] = data[j++]; b2b[1] = data[j++];
            int ndataLenHF = IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2));

            // Byte 54-55: LF data packet length
            j = 54;
            b2b[0] = data[j++]; b2b[1] = data[j++];
            int ndataLenLF = IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2));

            // Byte 56-57: Spectra data packet length
            j = 56;
            b2b[0] = data[j++]; b2b[1] = data[j++];
            int ndataLenSpectra = IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2));


            packetHeaderInfo.PacketTimeInSeconds = nSecond;
            packetHeaderInfo.SensorAttenuation = nAttenuation;
            packetHeaderInfo.OffsetInNanoseconds = nOffset;

            packetHeaderInfo.HFSclkTicksPpsToFirstSample = nSclkTicksPpsToFirstSampleHF;
            packetHeaderInfo.HFSclkTicksBetweenSamples = nSclkTicksBetweenSamplesHF;
            packetHeaderInfo.HFSclkTicksInThisSec = nSclkTicksInThisSecHF;
            packetHeaderInfo.HFDCOffset = dcOffsetHF;
            packetHeaderInfo.HFSampleNum = ndataLenHF / 6;
            packetHeaderInfo.HFThreshold = nthresholdHF;

            packetHeaderInfo.LFSclkTicksPpsToFirstSample = nSclkTicksPpsToFirstSampleLF;
            packetHeaderInfo.LFSclkTicksBetweenSamples = nSclkTicksBetweenSamplesLF;
            packetHeaderInfo.LFThreshold = nthresholdLF;
            packetHeaderInfo.LFSampleNum = ndataLenLF / 6;
            packetHeaderInfo.LFDCOffset = dcOffsetLF;

            packetHeaderInfo.SpectraSclkTicksBetweenSamples = nSclkTicksBetweenSamplesSpectra;
            packetHeaderInfo.SpectraSampleNum = ndataLenSpectra / 6;

            packetHeaderInfo.TotalLength = nTotalLen;
            packetHeaderInfo.RawData = data;
            return CheckPacketLength(packetHeaderInfo);
        }

        private const int FirstSubsectionStart = 30;
        private const int SClockTicksBetweenSamples = 5;

        private static bool GetPacketHeaderInfoFor10_1(byte[] data, PacketHeaderInfo packetHeaderInfo)
        {
            bool valid = false;

            if (data.Length >= (int)PacketHeaderLength.HeaderLengthLtg101)
            {
                packetHeaderInfo.PacketTimeInSeconds = BigEndianBytesToInt(data, 14, 4);
                packetHeaderInfo.OffsetInNanoseconds = BigEndianBytesToInt(data, 18, 1);
                packetHeaderInfo.TotalLength = BigEndianBytesToInt(data, 19, 2);
                packetHeaderInfo.HFSclkTicksInThisSec = BigEndianBytesToInt(data, 21, 4);
                packetHeaderInfo.HFSclkTicksBetweenSamples = SClockTicksBetweenSamples;
                packetHeaderInfo.SensorAttenuation = data[25];
                packetHeaderInfo.FirmwareTime = BigEndianBytesToInt(data, 26, 4);

                int index = FirstSubsectionStart;
                int length = GetHighFrequencyHeader(packetHeaderInfo, FirstSubsectionStart);

                index = length + index;

                GetLowFrequencyHeader(packetHeaderInfo, index);

                valid = true;
            }

            return valid;
        }

        private enum SubsectionType { Unknown = 0, HighFrequency = 49, LowFrequency = 50, Gps = 51, Spectra = 52, Text = 53, Vlf = 54, Elf = 55 }

        public static int GetHighFrequencyHeader(PacketHeaderInfo packet, int offset)
        {
            int length = 0;
            byte[] data = packet.RawData;
            if (data.Length >= offset + 2)
            {
                length = BigEndianBytesToInt(data, offset, 2);

                if (data.Length > offset + length)
                {
                    int type = data[offset + 2];
                    if (type == (int)SubsectionType.HighFrequency)
                    {
                        packet.HFSclkTicksPpsToFirstSample = data[offset + 3];
                        packet.HFThreshold = BigEndianBytesToInt(data, offset + 4, 2);
                        packet.HFDCOffset = BigEndianBytesToInt(data, offset + 6, 2);
                    }
                }
                else
                {
                    length = 0;
                }
            }

            return length;
        }

        public static int GetLowFrequencyHeader(PacketHeaderInfo packet, int offset)
        {
            int length = 0;
            byte[] data = packet.RawData;
            if (data.Length >= offset + 2)
            {
                length = BigEndianBytesToInt(data, offset, 2);

                if (data.Length >= offset + length)
                {
                    int type = data[offset + 2];
                    if (type == (int)SubsectionType.LowFrequency)
                    {
                        packet.LFSclkTicksPpsToFirstSample = data[offset + 3];
                        packet.LFThreshold = BigEndianBytesToInt(data, offset + 4, 2);
                        packet.LFDCOffset = BigEndianBytesToInt(data, offset + 6, 2);
                    }
                }
                else
                {
                    length = 0;
                }
            }

            return length;
        }

        // Not used, because the spectra data is combined with 9.3 format
        private static bool GetPacketHeaderForSpectraData(byte[] data, PacketHeaderInfo packetHeaderInfo)
        {
            int nSecond = -1;
            int nTotalLen = 0;
            int nOffset = -1;
            int nSclkTicksPpsToFirstSample = -1;
            int nSclkTicksBetweenSamples = -1;
            int nSclkTicksInThisSec = -1;
            int j = 14;

            if (data.Length < (int)PacketHeaderLength.HeaderLengthSpectra)
                return false;

            byte[] b1b = new byte[1];
            byte[] b2b = new byte[2];
            byte[] b4b = new byte[4];
            byte[] b10b = new byte[10];

            //check time
            b4b[0] = data[j++];
            b4b[1] = data[j++];
            b4b[2] = data[j++];
            b4b[3] = data[j++];
            nSecond = IPAddress.NetworkToHostOrder(ByteArrayToInt(b4b, 4));

            //Offset in Nanoseconds
            b1b[0] = data[j++];
            nOffset = ByteArrayToInt(b1b, 1);

            //byte: 19-20, SCLK ticks between pps and first sample
            b2b[0] = data[j++]; b2b[1] = data[j++];
            nSclkTicksPpsToFirstSample = IPAddress.NetworkToHostOrder(ByteArrayToInt(b2b, 2));

            //byte: 21-24,SCLK ticks between samples
            b4b[0] = data[j++]; b4b[1] = data[j++]; b4b[2] = data[j++]; b4b[3] = data[j++];
            nSclkTicksBetweenSamples = IPAddress.NetworkToHostOrder(ByteArrayToInt(b4b, 4));

            //bytes: 25-28, SCLK ticks in this second
            b4b[0] = data[j++]; b4b[1] = data[j++]; b4b[2] = data[j++]; b4b[3] = data[j++];
            nSclkTicksInThisSec = IPAddress.NetworkToHostOrder(ByteArrayToInt(b4b, 4));

            // Byte: 29, Attenuation
            j = 29;
            b1b[0] = data[j];
            byte nAttenuation = (byte)ByteArrayToInt(b1b, 1);

            // Byte 30: Antenna mode or hardware info for the spectra data

            //Total data Length
            j = 31;
            b1b[0] = data[j++]; ;
            int log2Value = ByteArrayToInt(b1b, 1);
            nTotalLen = (int)PacketHeaderLength.HeaderLengthSpectra + (int)Math.Pow(2.0, log2Value);

            packetHeaderInfo.PacketTimeInSeconds = nSecond;
            packetHeaderInfo.TotalLength = nTotalLen;
            packetHeaderInfo.HFSclkTicksBetweenSamples = nSclkTicksBetweenSamples;
            packetHeaderInfo.HFSclkTicksInThisSec = nSclkTicksInThisSec;
            packetHeaderInfo.HFSclkTicksPpsToFirstSample = nSclkTicksPpsToFirstSample;
            packetHeaderInfo.SensorAttenuation = nAttenuation;
            packetHeaderInfo.OffsetInNanoseconds = nOffset;
            packetHeaderInfo.RawData = data;
            return CheckPacketLength(packetHeaderInfo);
        }

        public static bool GetSecondSampleInfo(byte[] data, PacketHeaderInfo header, ArrayList HFSamples, ArrayList LFSamples, ArrayList SpectraSamples)
        {
            if (HFSamples == null)
                return false;
            //throw new Exception("Object for samples must be created first");

            if (GetPacketHeaderInfo(data, header) == false)
                return false;
            //throw new Exception("failed to get header info in socond of sample UDP");
            if (header.MsgType != PacketType.MsgTypeLtg)
                return false;

            try
            {
                if (header.MajorVersion == SensorVersion.ID_VERSION_SENSOR_8)
                {
                    // return false; // TEMP
                    return GetSensor8Pulse(data, HFSamples);
                }
                else if (header.MajorVersion == SensorVersion.ID_VERSION_SENSOR_9_MAJOR)
                {
                    return GetSensor9PulseData(header, data, HFSamples, LFSamples, SpectraSamples);
                }
                else if (header.MajorVersion == SensorVersion.ID_VERSION_SENSOR_10_MAJOR)
                {
                    return GetSensor10PulseData(header, data, HFSamples, LFSamples, SpectraSamples);
                }

            }
            catch (Exception ex)
            {
                return false;
                string msg = string.Format("Warning, Sensor:{0}, Time:{1}, {2}",
                    header.SensorId, header.PacketTimeInSeconds, ex.Message);
                throw new Exception(msg);
            }
            return false;
        }

        private static bool GetSensor8Pulse(byte[] data, ArrayList alSamples)
        {
            //index for Initial Tick Number
            byte[] b1b = new byte[1];
            byte[] b2b = new byte[2];
            byte[] b4b = new byte[4];
            int j;
            int nDataBytes = 0;
            int nIndexData = 11;

            if (data.Length <= nIndexData)
                return false;

            //                   j = 8;
            //                   b1b[0] = data[j++];
            //                   nTimeOffset = ByteArrayToInt(b1b, 1);

            j = nIndexData - 2;
            b2b[0] = data[j++]; b2b[1] = data[j++];
            nDataBytes = IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2));

            if (nDataBytes < 7)
                return true;

            while (j + 1 < nIndexData + nDataBytes)
            {
                ArrayList alPulseSample = GetPulseData(data, ref j);
                if (alPulseSample.Count == 0)
                    return false;

                alSamples.AddRange(alPulseSample);
            }
            return true;
        }

        private static bool GetSensor9PulseData(PacketHeaderInfo header, byte[] data, ArrayList HFlSamples, ArrayList LFSamples, ArrayList SpectraSamples)
        {
            if (header.MajorVersion != SensorVersion.ID_VERSION_SENSOR_9_MAJOR) // Only handle sensor 9 data
                return false;

            if (header.MinorVersion == 0)
            {
                return GetSensor9_0PulseData(data, HFlSamples);
            }
            else if (header.MinorVersion == SensorVersion.ID_VERSION_SENSOR_9_MINOR1)
            {
                return GetSensor9_1PulseData(data, HFlSamples);

            }
            else if (header.MinorVersion == SensorVersion.ID_VERSION_SENSOR_9_MINOR2)
            {
                return GetSensor9_2PulseData(data, HFlSamples, LFSamples);

            }
            else if (header.MinorVersion == SensorVersion.ID_VERSION_SENSOR_9_MINOR3)
            {
                return GetSensor9_3PulseData(header, HFlSamples, LFSamples, SpectraSamples);
            }
            else if (header.MinorVersion == SensorVersion.ID_VERSION_SENSOR_9_MINOR4)
            {
                return GetSensor9_4PulseData(header, HFlSamples, LFSamples, SpectraSamples);
            }
            return false;
        }

        // First prototype of Sensor 9
        private static bool GetSensor9_0PulseData(byte[] data, ArrayList alSamples)
        {
            //index for Initial Tick Number
            int j = 0;
            byte[] b2b = new byte[2];
            int nDataBytes = 0;
            int nIndexData = 0;

            nIndexData = 27;
            if (data.Length <= nIndexData)
                return false;

            j = nIndexData - 2;
            b2b[0] = data[j++]; b2b[1] = data[j++];
            nDataBytes = IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2));

            if (nDataBytes < 7)
                return true;

            while (j + 1 < nIndexData + nDataBytes)
            {
                ArrayList alPulseSample = GetPulseData(data, ref j);
                if (alPulseSample.Count == 0)
                    return false; // Data is not good
                alSamples.AddRange(alPulseSample);
            }
            return true;
        }

        // First sensor 9 with compression
        private static bool GetSensor9_1PulseData(byte[] data, ArrayList alSamples)
        {
            //index for Initial Tick Number
            int j = 0;
            byte[] b2b = new byte[2];
            byte[] b4b = new byte[4];
            int nDataBytes = 0;
            int nIndexData = 0;

            nIndexData = 38;
            if (data.Length <= nIndexData)
                return false;

            j = nIndexData - 2;
            b2b[0] = data[j++]; b2b[1] = data[j++];
            nDataBytes = IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2)); // Total remaining bytes of data

            if (nDataBytes < 6)
                return true;

            int nTotalData = nIndexData + nDataBytes - 1;
            int nDataArraySize = nDataBytes / 6;
            Sample[] theSample = new Sample[nDataArraySize];
            /* Sample is a struct, so doesnt need initiate an instance
            for (int jj = 0; jj < nDataArraySize; jj++)
            {
                theSample[jj] = new Sample();
            }
             * */

            int i = 0;
            while (j < nTotalData && i < nDataArraySize)
            {
                // Sample theSample = new Sample();
                b4b[0] = data[j++]; b4b[1] = data[j++]; b4b[2] = data[j++]; b4b[3] = data[j++];
                theSample[i].Tick = (UInt32)IPAddress.NetworkToHostOrder(ByteArrayToInt(b4b, 4));
                b2b[0] = data[j++]; b2b[1] = data[j++];
                theSample[i].E = IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2));
                i++;
            }
            alSamples.AddRange(theSample);
            return true;
        }

        // LF is added
        private static bool GetSensor9_2PulseData(byte[] data, ArrayList HFSamples, ArrayList LFSamples)
        {
            //index for Initial Tick Number
            int j = 0;
            byte[] b2b = new byte[2];
            byte[] b4b = new byte[4];
            int nDataBytesHF = 0;
            int nDataBytesLF = 0;
            int nIndexData = 0;

            nIndexData = (int)PacketHeaderLength.HeaderLengthLtg92;
            if (data.Length <= nIndexData)
                return false;

            // Get sample data size
            j = 46;
            b2b[0] = data[j++]; b2b[1] = data[j++];
            nDataBytesHF = IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2)); // Total HF bytes of data

            b2b[0] = data[j++]; b2b[1] = data[j++];
            nDataBytesLF = IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2)); // Total HF bytes of data

            // Get HF data
            int nDataArraySize = nDataBytesHF / 6;
            Sample[] theSample = new Sample[nDataArraySize];
            int i = 0;
            j = (int)PacketHeaderLength.HeaderLengthLtg92; // Start byte of HF data
            while (i < nDataArraySize)
            {
                // Sample theSample = new Sample();
                b4b[0] = data[j++]; b4b[1] = data[j++]; b4b[2] = data[j++]; b4b[3] = data[j++];
                theSample[i].Tick = (UInt32)IPAddress.NetworkToHostOrder(ByteArrayToInt(b4b, 4));
                b2b[0] = data[j++]; b2b[1] = data[j++];
                theSample[i].E = IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2));
                i++;
            }
            HFSamples.AddRange(theSample);

            // Get LF data
            nDataArraySize = nDataBytesLF / 6;
            theSample = new Sample[nDataArraySize];
            i = 0;
            j = (int)PacketHeaderLength.HeaderLengthLtg92 + nDataBytesHF; // Start byte of LF data
            if (LFSamples != null)
            {
                while (i < nDataArraySize)
                {
                    b4b[0] = data[j++]; b4b[1] = data[j++]; b4b[2] = data[j++]; b4b[3] = data[j++];
                    theSample[i].Tick = (UInt32)IPAddress.NetworkToHostOrder(ByteArrayToInt(b4b, 4));
                    b2b[0] = data[j++]; b2b[1] = data[j++];
                    theSample[i].E = IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2));
                    i++;
                }
                LFSamples.AddRange(theSample);
            }
            return true;
        }

        // LF and Spectra data is added
        private static bool GetSensor9_3PulseData(PacketHeaderInfo header, ArrayList HFSamples, ArrayList LFSamples, ArrayList SpectraSamples)
        {
            //index for Initial Tick Number
            byte[] data = header.RawData;

            int j = 0;
            byte[] b2b = new byte[2];
            byte[] b4b = new byte[4];
            int nDataBytesHF = 0;
            int nDataBytesLF = 0;
            int nDataBytesSpectra = 0;
            int nIndexData = 0;

            nIndexData = (int)PacketHeaderLength.HeaderLengthLtg93;

            try
            {

                if (data.Length <= nIndexData)
                    return false;

                // Get HF sample data size
                j = 52;
                b2b[0] = data[j++]; b2b[1] = data[j++];
                nDataBytesHF = IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2)); // Total HF bytes of data

                // Get LF sample data size
                j = 54;
                b2b[0] = data[j++]; b2b[1] = data[j++];
                nDataBytesLF = IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2)); // Total LF bytes of data

                // Get LF sample data size
                j = 56;
                b2b[0] = data[j++]; b2b[1] = data[j++];
                nDataBytesSpectra = IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2)); // Total Spectra bytes of data


                // Get HF data
                int nDataArraySize = header.HFSampleNum;
                Sample[] theSample = new Sample[nDataArraySize];
                int i = 0;
                j = (int)PacketHeaderLength.HeaderLengthLtg93; // Start byte of HF data
                if (HFSamples != null)
                {
                    while (i < nDataArraySize)
                    {
                        // Sample theSample = new Sample();
                        b4b[0] = data[j++]; b4b[1] = data[j++]; b4b[2] = data[j++]; b4b[3] = data[j++];
                        theSample[i].Tick = (UInt32)IPAddress.NetworkToHostOrder(ByteArrayToInt(b4b, 4));
                        b2b[0] = data[j++]; b2b[1] = data[j++];
                        theSample[i].E = IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2));
                        i++;
                    }
                    HFSamples.AddRange(theSample);
                }

                // Get LF data
                nDataArraySize = header.LFSampleNum;
                theSample = new Sample[nDataArraySize];
                i = 0;
                j = (int)PacketHeaderLength.HeaderLengthLtg93 + nDataBytesHF; // Start byte of LF data
                if (LFSamples != null)
                {
                    while (i < nDataArraySize)
                    {
                        b4b[0] = data[j++]; b4b[1] = data[j++]; b4b[2] = data[j++]; b4b[3] = data[j++];
                        theSample[i].Tick = (UInt32)IPAddress.NetworkToHostOrder(ByteArrayToInt(b4b, 4));
                        b2b[0] = data[j++]; b2b[1] = data[j++];
                        theSample[i].E = IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2));
                        i++;
                    }
                    LFSamples.AddRange(theSample);
                }

                // Get Spectra Data
                nDataArraySize = header.SpectraSampleNum;
                theSample = new Sample[nDataArraySize];
                i = 0;
                j = (int)PacketHeaderLength.HeaderLengthLtg93 + nDataBytesHF + nDataBytesLF; // Start byte of Spectra data
                if (SpectraSamples != null)
                {

                    while (i < nDataArraySize)
                    {
                        b4b[0] = data[j++]; b4b[1] = data[j++]; b4b[2] = data[j++]; b4b[3] = data[j++];
                        theSample[i].Tick = (UInt32)IPAddress.NetworkToHostOrder(ByteArrayToInt(b4b, 4));
                        b2b[0] = data[j++]; b2b[1] = data[j++];
                        theSample[i].E = IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2));
                        i++;
                    }
                    SpectraSamples.AddRange(theSample);
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        // New compression data based on 9.4
        private static bool GetSensor9_4PulseData(PacketHeaderInfo header, ArrayList HFSamples, ArrayList LFSamples, ArrayList SpectraSamples)
        {

            //index for Initial Tick Number
            byte[] data = header.RawData;

            int j = 0;
            byte[] b2b = new byte[2];
            byte[] b4b = new byte[4];
            int nDataBytesHF = 0;
            int nDataBytesLF = 0;
            int nDataBytesSpectra = 0;
            int nIndexData = 0;
            byte[] theSample;
            ArrayList tempList;
            RawdataDecompressor decompressor = new RawdataDecompressor();
            try
            {
                // Get HF sample data size
                j = 52;
                b2b[0] = data[j++]; b2b[1] = data[j++];
                nDataBytesHF = IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2)); // Total HF bytes of data

                // Get LF sample data size
                j = 54;
                b2b[0] = data[j++]; b2b[1] = data[j++];
                nDataBytesLF = IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2)); // Total LF bytes of data

                // Get Spectra data size
                j = 56;
                b2b[0] = data[j++]; b2b[1] = data[j++];
                nDataBytesSpectra = IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2)); // Total Spectra bytes of data

                // Get HF data
                if (nDataBytesHF != 0 && HFSamples != null)
                {
                    nIndexData = (int)PacketHeaderLength.HeaderLengthLtg94;
                    theSample = new byte[nDataBytesHF];
                    Array.Copy(data, nIndexData, theSample, 0, nDataBytesHF);
                    // decompressor = new RawdataDecompressor();
                    // New compression for the byte 50 in header
                    decompressor.setMantissas(data[50]);
                    decompressor.SetRawData(theSample);
                    tempList = decompressor.DecodeCompress();
                    if (tempList != null && tempList.Count > 0)
                        HFSamples.AddRange(tempList);
                    header.HFSampleNum = HFSamples.Count;
                }



                if (nDataBytesLF != 0 && LFSamples != null)
                {
                    // Get LF data
                    nIndexData = (int)PacketHeaderLength.HeaderLengthLtg94 + nDataBytesHF;
                    theSample = new byte[nDataBytesLF];
                    Array.Copy(data, nIndexData, theSample, 0, nDataBytesLF);
                    decompressor.setMantissas(data[50]);
                    decompressor.SetRawData(theSample);

                    tempList = decompressor.DecodeCompress();
                    if (tempList != null && tempList.Count > 0)
                        LFSamples.AddRange(tempList);
                    header.LFSampleNum = LFSamples.Count;
                }

                // Spectra data is not compressed
                // Spectra data is not used here so we can skip it
                return true;
                if (nDataBytesSpectra != 0 && SpectraSamples != null)
                {
                    int nDataArraySize = header.SpectraSampleNum;
                    Sample[] theSpectraSample = new Sample[nDataArraySize];
                    int i = 0;
                    j = (int)PacketHeaderLength.HeaderLengthLtg93 + nDataBytesHF + nDataBytesLF; // Start byte of Spectra data
                    while (i < nDataArraySize)
                    {
                        b4b[0] = data[j++]; b4b[1] = data[j++]; b4b[2] = data[j++]; b4b[3] = data[j++];
                        theSpectraSample[i].Tick = (UInt32)IPAddress.NetworkToHostOrder(ByteArrayToInt(b4b, 4));
                        b2b[0] = data[j++]; b2b[1] = data[j++];
                        theSpectraSample[i].E = IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2));
                        i++;
                    }
                    SpectraSamples.AddRange(theSpectraSample);
                }

                return true;
            }
            catch (Exception ex)
            {

                return false;
            }
        }

        private static bool GetSensor10PulseData(PacketHeaderInfo header, byte[] data, ArrayList HFlSamples, ArrayList LFSamples, ArrayList SpectraSamples)
        {
            if (header.MajorVersion != SensorVersion.ID_VERSION_SENSOR_10_MAJOR) // Only handle sensor 10 data
                return false;

            if (header.MinorVersion == 0)
            {
                return GetSensor10_0PulseData(header, HFlSamples, LFSamples, SpectraSamples); ;
            }
            return false;
        }


        // New compression data based on 10.0
        private static bool GetSensor10_0PulseData(PacketHeaderInfo header, ArrayList HFSamples, ArrayList LFSamples, ArrayList SpectraSamples)
        {

            //index for Initial Tick Number
            byte[] data = header.RawData;

            int j = 0;
            byte[] b2b = new byte[2];
            byte[] b4b = new byte[4];
            int nDataBytesHF = 0;
            int nDataBytesLF = 0;
            int nDataBytesSpectra = 0;
            int nIndexData = 0;
            byte[] theSample;
            ArrayList tempList;
            RawdataDecompressor decompressor = new RawdataDecompressor();
            try
            {
                // Get HF sample data size
                j = 52;
                b2b[0] = data[j++]; b2b[1] = data[j++];
                nDataBytesHF = IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2)); // Total HF bytes of data

                // Get LF sample data size
                j = 54;
                b2b[0] = data[j++]; b2b[1] = data[j++];
                nDataBytesLF = IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2)); // Total LF bytes of data

                // Get Spectra data size
                j = 56;
                b2b[0] = data[j++]; b2b[1] = data[j++];
                nDataBytesSpectra = IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2)); // Total Spectra bytes of data

                // Get HF data
                if (nDataBytesHF != 0 && HFSamples != null)
                {
                    nIndexData = (int)PacketHeaderLength.HeaderLengthLtg100;
                    theSample = new byte[nDataBytesHF];
                    Array.Copy(data, nIndexData, theSample, 0, nDataBytesHF);
                    // decompressor = new RawdataDecompressor();
                    // New compression for the byte 50 in header
                    decompressor.setMantissas(0x03);  // Always set this to 3 for sensor 10
                    decompressor.SetRawData(theSample);
                    tempList = decompressor.DecodeCompress();
                    if (tempList != null && tempList.Count > 0)
                        HFSamples.AddRange(tempList);
                    header.HFSampleNum = HFSamples.Count;
                }

                if (nDataBytesLF != 0 && LFSamples != null)
                {
                    // Get LF data
                    nIndexData = (int)PacketHeaderLength.HeaderLengthLtg100 + nDataBytesHF;
                    theSample = new byte[nDataBytesLF];
                    Array.Copy(data, nIndexData, theSample, 0, nDataBytesLF);
                    decompressor.setMantissas(0x03); // Always set this to 3 for sensor 10
                    decompressor.SetRawData(theSample);

                    tempList = decompressor.DecodeCompress();
                    if (tempList != null && tempList.Count > 0)
                        LFSamples.AddRange(tempList);
                    header.LFSampleNum = LFSamples.Count;
                }

                // Spectra data is not compressed
                // Spectra data is not used so we can skip here
                return true;
                if (nDataBytesSpectra != 0 && SpectraSamples != null)
                {
                    int nDataArraySize = header.SpectraSampleNum;
                    Sample[] theSpectraSample = new Sample[nDataArraySize];
                    int i = 0;
                    j = (int)PacketHeaderLength.HeaderLengthLtg93 + nDataBytesHF + nDataBytesLF + 8; // Start byte of Spectra data
                    while (i < nDataArraySize)
                    {
                        b4b[0] = data[j++]; b4b[1] = data[j++]; b4b[2] = data[j++]; b4b[3] = data[j++];
                        theSpectraSample[i].Tick = (UInt32)IPAddress.NetworkToHostOrder(ByteArrayToInt(b4b, 4));
                        b2b[0] = data[j++]; b2b[1] = data[j++];
                        theSpectraSample[i].E = IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2));
                        i++;
                    }
                    SpectraSamples.AddRange(theSpectraSample);
                }
                return true;
            }
            catch (Exception ex)
            {

                return false;
            }
        }

        //version 8 format: Messagetype (2 bytes), Sensor id (2 bytes), position data(154 bytes), time raim status(78 byte)
        //version 9 :
        //Position Data:
        //@@HamdyyhmsffffaaaaoooohhhhmmmmaaaaoooohhhhmmmmVVvvhddttimsidd (repeat imsidd for remaining 11 channels) ssrrccooooTTushmvvvvvvC<CR><LF>
        //m month 1..12
        //d day 1..31
        //yy year 1998..2079
        //h hours 0..23
        //m minutes 00..59
        //s seconds 0..60
        //ffff fractional second 0..999,999,999 nanoseconds
        //Position (Filtered or Unfiltered following Filter Select)
        //aaaa latitude in mas -324,000,000..324,000,000 (-90�..+90�)
        //oooo longitude in mas -648,000,000..648,000,000 (-180�..+180�)
        //hhhh GPS height in cm - -100,000..+1,800,000 (-1000..+18,000 m)
        public static bool GetGpsData(PacketHeaderInfo gpsHeaderInfo)
        {
            if (!gpsHeaderInfo.IsValid())
                return false;

            int rawDataIndex = 0;
            if (gpsHeaderInfo.MajorVersion >= SensorVersion.ID_VERSION_SENSOR_9_MAJOR)
            {
                rawDataIndex = 14;
            }
            else if (gpsHeaderInfo.MajorVersion == SensorVersion.ID_VERSION_SENSOR_8)
            {
                rawDataIndex = 4;
            }
            else
            {
                return false;
            }

            byte[] b1b = new byte[1];
            byte[] b2b = new byte[2];
            byte[] b4b = new byte[4];
            byte[] b10b = new byte[10];

            byte[] rawGPSData = new byte[gpsHeaderInfo.RawData.Length - rawDataIndex + 1];
            Array.Copy(gpsHeaderInfo.RawData, rawDataIndex, rawGPSData, 0, gpsHeaderInfo.RawData.Length - rawDataIndex);

            rawDataIndex = 4;
            int month = (int)rawGPSData[rawDataIndex++];
            int day = (int)rawGPSData[rawDataIndex++];
            b2b[0] = rawGPSData[rawDataIndex++];
            b2b[1] = rawGPSData[rawDataIndex++];
            int year = IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2));
            int hour = (int)rawGPSData[rawDataIndex++];
            int minute = (int)rawGPSData[rawDataIndex++];
            int second = (int)rawGPSData[rawDataIndex++];

            DateTime dataTime = new DateTime(year, month, day, hour, minute, second);
            int unixTime = LtgTimeUtils.GetSecondsSince1970FromDateTime(dataTime);

            // Location of the sensor
            rawDataIndex = 15;
            int nLat, nLon, nHeight;
            double fLat, fLon, fHeight;
            b4b[0] = rawGPSData[rawDataIndex++]; b4b[1] = rawGPSData[rawDataIndex++];
            b4b[2] = rawGPSData[rawDataIndex++]; b4b[3] = rawGPSData[rawDataIndex++];

            nLat = IPAddress.NetworkToHostOrder(ByteArrayToInt(b4b, 4));
            fLat = (double)90.0 * nLat / 324000000;

            b4b[0] = rawGPSData[rawDataIndex++]; b4b[1] = rawGPSData[rawDataIndex++];
            b4b[2] = rawGPSData[rawDataIndex++]; b4b[3] = rawGPSData[rawDataIndex++];

            nLon = IPAddress.NetworkToHostOrder(ByteArrayToInt(b4b, 4));
            fLon = (double)90.0 * nLon / 324000000;

            //height in meter
            b4b[0] = rawGPSData[rawDataIndex++]; b4b[1] = rawGPSData[rawDataIndex++];
            b4b[2] = rawGPSData[rawDataIndex++]; b4b[3] = rawGPSData[rawDataIndex++];
            nHeight = IPAddress.NetworkToHostOrder(ByteArrayToInt(b4b, 4));
            fHeight = (double)nHeight / 100.0;

            // Satellite information
            rawDataIndex = 55;
            int nVisibleSatellites = rawGPSData[rawDataIndex++];
            int nTrackedStallites = rawGPSData[rawDataIndex++];

            GpsData gpsData = new GpsData();
            gpsData.SensorId = gpsHeaderInfo.SensorId;
            gpsData.Latitude = fLat;
            gpsData.Longtitude = fLon;
            gpsData.Height = fHeight;
            gpsData.SecondsSinceEpic = unixTime;
            gpsData.VisibleSatellites = nVisibleSatellites;
            gpsData.TrackedSatellites = nTrackedStallites;
            gpsData.SensorVersion = string.Format("{0}.{1}", (int)gpsHeaderInfo.MajorVersion, (int)gpsHeaderInfo.MinorVersion);
            gpsData.Rawdata = rawGPSData;
            gpsHeaderInfo.PacketTimeInSeconds = unixTime;
            gpsHeaderInfo.OtherObject = gpsData;

            return true;
        }

        public static byte[] BuildSensor9PacketFrom8(byte[] data)
        {
            byte[] b2b = new byte[2];

            //sensor 9 and 8 have the same packet struture from "bytes in the message after the header"
            int nIndex8 = 9;
            int nIndex9 = 25;
            int nSize = nIndex9 + data.Length - nIndex8;
            byte[] buf9 = new byte[nSize];
            Array.Copy(data, nIndex8, buf9, nIndex9, data.Length - nIndex8);

            int j = 0;

            //magic
            buf9[j++] = 48;

            //Msg Type 
            buf9[j++] = data[1];

            //version
            buf9[j++] = 9; buf9[j++] = 0;

            //sensor ID
            b2b[0] = data[2];
            b2b[1] = data[3];
            int nSensorId = IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2));
            string sensorId = Convert.ToString(nSensorId);
            for (int i = 0; i < 10; i++)
            {
                if (i < sensorId.Length)
                    buf9[j++] = (byte)sensorId[i];
                else
                    buf9[j++] = (byte)' ';
            }


            //time
            buf9[j++] = data[4]; buf9[j++] = data[5]; buf9[j++] = data[6]; buf9[j++] = data[7];

            //Pps tick number
            //buf9[j++] = 0; buf9[j++] = 0; buf9[j++] = 0; buf9[j++] = 0;

            //Offset in Nanoseconds
            buf9[j++] = 0;

            //SCLK ticks between pps and first sample
            buf9[j++] = 2;

            //SCLK ticks between samples
            buf9[j++] = 5;

            //SCLK ticks in this second
            int nSTick = 120000000;
            nSTick = IPAddress.HostToNetworkOrder(nSTick);
            byte[] b4b = BitConverter.GetBytes(nSTick);
            Array.Copy(b4b, 0, buf9, j, 4);
            j += 4;


            return buf9;
        }

        // Encode flash to packet
        public static PacketHeaderInfo EncodeFlashToPacket(LTGFlash flashData)
        {
            PacketHeaderInfo header = new PacketHeaderInfo();
            byte[] packet = RawPacketUtil.EncodeLtgFlashToManager((LTGFlash)flashData);
            if (packet == null)
                return null;

            if (!RawPacketUtil.GetPacketHeaderInfo(packet, header))
                return null;
            return header;
        }

        //build packet for client based on TOA TCP packet format spec
        //public static byte[] EncodeLtgFlashToClient(FlashData flash)
        //{
        //    int system = 0;
        //    int data_type = (flash.FlashType == FlashType.FlashTypeIC) ? 3 : 0;
        //    DateTime flashTime = LtgTimeUtils.GetUtcDateTimeFromString(flash.FlashTime);

        //    int theYear = flashTime.Year;
        //    int theMonth = flashTime.Month;
        //    int theDay = flashTime.Day;
        //    DateTime midNightTime = new DateTime(theYear, theMonth, theDay);
        //    TimeSpan ts = flashTime - midNightTime;
        //    int minutes_since_midnight = (int)ts.TotalMinutes;
        //    ts = flashTime - midNightTime.AddMinutes(minutes_since_midnight);
        //    int milliseconds_since_minute_start = (int)ts.TotalMilliseconds;
        //    Int32 Longitude = (Int32)(flash.Longitude * Math.Pow(10, 7));
        //    Int32 Latitude = (Int32)(flash.Latitude * Math.Pow(10, 7));
        //    int Magnitude_Polarity = (int)flash.Amplitude;

        //    int random1 = 0;
        //    int Stroke_Solution = 1;
        //    int random2 = 9, random3 = 1, random4 = 14, checksum = 0;
        //    return null;


        //    return PackLightningData(system,
        //        data_type,
        //            minutes_since_midnight,
        //             milliseconds_since_minute_start,
        //            theYear, theMonth, theDay,
        //            Longitude, random1, Magnitude_Polarity,
        //             Stroke_Solution, Latitude, random2,
        //             random3, random4, checksum);

        //}

        public static byte[] CreateHeartbeatForClient()
        {
            return null;
            // return PackLightningData(1, 1, 12,
            //                 12, 2006, 6, 22, 37, 12,
            //                1, 1, 85, 12,
            //                12, 12, 0);
        }

        //Build packet sent to lightning manager
        public static byte[] EncodeLtgFlashToManager(LTGFlash flash)
        {
            int nTotalSize = 0;

            //encode flash portions
            ArrayList alFp = new ArrayList();
            int index = 0;
            while (index < flash.FlashPortionList.Count)
            {
                LTGFlashPortion fp = flash.FlashPortionList[index] as LTGFlashPortion;
                if (fp != null)
                {
                    byte[] bfp = EncodeFlashPortion(fp);
                    alFp.Add(bfp);
                    nTotalSize += bfp.Length;
                }
                index++;
            }



            byte[] b2b = null;
            int nIndex = 0;

            //size for the flash part: 50 fixed plus the bytes in description
            int nFlashSize = 52 + flash.Description.Length;

            //total size: nFlashSize + flash portion size + # of flash portion + heade + tail
            nTotalSize += (nFlashSize + 3);

            byte[] buf = new byte[nTotalSize];

            // First byte is the magic number
            // Second and third byte are the total length of the packet
            nIndex = 0;
            buf[0] = (byte)PacketMagicNumber.MagicFlashPacket;
            //packet size without 4 bytes header and tail
            nIndex += 1;

            //b2b = BitConverter.GetBytes((ushort)(nTotalSize - 4));
            //Array.Copy(b2b, 0, buf, nIndex, 2);
            //nIndex += 2;

            //flash data base object
            byte[] fd = EncodeFlashData(flash);
            Array.Copy(fd, 0, buf, nIndex, fd.Length);
            nIndex += fd.Length;

            //# of flash portions
            b2b = BitConverter.GetBytes(alFp.Count);
            Array.Copy(b2b, 0, buf, nIndex, 2);
            nIndex += 2;

            foreach (byte[] fp in alFp)
            {
                Array.Copy(fp, 0, buf, nIndex, fp.Length);
                nIndex += fp.Length;
            }

            //end indicator
            //Array.Copy(LtgConstants.TcpEndMarker, 0, buf, nIndex, 2);
            //nIndex += 2;

            return buf;
        }

        private static byte[] EncodeFlashPortion(LTGFlashPortion fp)
        {
            byte[] b2b = null;
            int nIndex = 0;

            //size: 52 fixed bytes plus the bytes in stroke solution & offsets
            string amplitudes = FormatOffsetAbsPeakAmplitude(fp.OffsetList);
            ushort nBufSize = (ushort)(54 + fp.Description.Length + amplitudes.Length);
            byte[] buf = new byte[nBufSize];

            //encode the base object first
            byte[] flash = EncodeFlashData(fp);
            Array.Copy(flash, 0, buf, 0, flash.Length);

            nIndex = 0;

            //update the size 
            b2b = BitConverter.GetBytes(nBufSize);
            Array.Copy(b2b, 0, buf, nIndex, 2);
            nIndex = flash.Length;

            //size of amplitudes 
            b2b = BitConverter.GetBytes(amplitudes.Length);
            Array.Copy(b2b, 0, buf, nIndex, 2);
            nIndex += 2;

            //offsets
            if (amplitudes.Length > 0)
            {
                byte[] b = Encoding.ASCII.GetBytes(amplitudes);
                Array.Copy(b, 0, buf, nIndex, amplitudes.Length);
                nIndex += amplitudes.Length;
            }

            return buf;
        }

        private static byte[] EncodeFlashData(FlashData flash)
        {
            byte[] b2b = null;
            byte[] b4b = null;
            int nIndex = 0;
            float f;
            int int32bit = 0;

            //size: 50 fixed bytes plus the bytes in description
            ushort nBufSize = (ushort)(52 + flash.Description.Length);
            byte[] buf = new byte[nBufSize];

            nIndex = 0;

            //size
            b2b = BitConverter.GetBytes(nBufSize);
            Array.Copy(b2b, 0, buf, nIndex, 2);
            nIndex += 2;

            //type
            byte ntype = (byte)flash.FlashType;
            buf[nIndex++] = ntype; //c-g strike

            //flash.FlashTime format: YYYY-MM-DD-hh:mm:ss.xxxxxxxxx, up to 29 bytes
            StringBuilder sb = new StringBuilder(flash.FlashTime);
            while (sb.Length < 29)
                sb.Append(' ');
            byte[] b0 = Encoding.ASCII.GetBytes(sb.ToString());
            Array.Copy(b0, 0, buf, nIndex, sb.Length);
            nIndex += sb.Length;

            //latitude
            int32bit = (int)(flash.Latitude * 10000000);
            b4b = BitConverter.GetBytes(int32bit);
            Array.Copy(b4b, 0, buf, nIndex, 4);
            nIndex += 4;

            //longitude
            int32bit = (int)(flash.Longitude * 10000000);
            b4b = BitConverter.GetBytes(int32bit);
            Array.Copy(b4b, 0, buf, nIndex, 4);
            nIndex += 4;

            //height
            int32bit = (int)(flash.Height * 100);
            b4b = BitConverter.GetBytes(int32bit);
            Array.Copy(b4b, 0, buf, nIndex, 4);
            nIndex += 4;

            //amp
            b4b = BitConverter.GetBytes((int)flash.Amplitude);
            Array.Copy(b4b, 0, buf, nIndex, 4);
            nIndex += 4;

            //Confidence
            b2b = BitConverter.GetBytes(flash.Confidence);
            Array.Copy(b2b, 0, buf, nIndex, 2);
            nIndex += 2;

            //size of solution
            b2b = BitConverter.GetBytes(flash.Description.Length);
            Array.Copy(b2b, 0, buf, nIndex, 2);
            nIndex += 2;

            //stroke solution
            if (flash.Description.Length > 0)
            {
                byte[] b = Encoding.ASCII.GetBytes(flash.Description);
                Array.Copy(b, 0, buf, nIndex, flash.Description.Length);
                nIndex += flash.Description.Length;
            }

            return buf;
        }

        public static LTGFlash DecodeFlashPacket(byte[] data)
        {
            //   data: header and tail have been removed already
            // packet at least 25 bytes long
            if (data.Length < 25)
                return null;

            LTGFlash flash = new LTGFlash();

            int nIndex = 1;
            FlashData fd = DecodeFlashData(data, ref nIndex, false);


            //# of flash portions
            byte[] b2b = new byte[2];
            b2b[0] = data[nIndex++]; b2b[1] = data[nIndex++];
            int nFP = ByteArrayToInt(b2b, 2);

            for (int i = 0; i < nFP; i++)
            {
                LTGFlashPortion fp = DecodeFlashPortion(data, ref nIndex);
                flash.AddFlashPortion(fp);
            }

            flash.Amplitude = fd.Amplitude;
            flash.Description = fd.Description;
            flash.FlashTime = fd.FlashTime;
            flash.Height = fd.Height;
            flash.FlashType = fd.FlashType;
            flash.Latitude = fd.Latitude;
            flash.Longitude = fd.Longitude;
            flash.Confidence = fd.Confidence;

            return flash;
        }

        private static LTGFlashPortion DecodeFlashPortion(byte[] data, ref int nIndex)
        {
            //LTGFlashPortion fp = new LTGFlashPortion();

            byte[] b2b = new byte[2];
            int nFlashSize;

            int j = nIndex;

            //size of this flash portion
            b2b[0] = data[j++]; b2b[1] = data[j++];
            nFlashSize = ByteArrayToInt(b2b, 2);

            //decode to get base object
            LTGFlashPortion fp = (DecodeFlashData(data, ref nIndex, true)) as LTGFlashPortion;

            if (fp != null)
            {
                //offsets amplitudes
                b2b[0] = data[nIndex++];
                b2b[1] = data[nIndex++];
                int nLen = ByteArrayToInt(b2b, 2);

                if (nLen > 0)
                {
                    byte[] b = new byte[nLen];
                    for (int i = 0; i < nLen; i++)
                        b[i] = data[nIndex++];

                    string amps = Encoding.ASCII.GetString(b);
                    var amplitudes = ParseOffsetAbsPeakAmplitude(amps);
                    fp.SetOffsetAmplitudes(amplitudes);
                }
            }

            return fp;

        }

        private static FlashData DecodeFlashData(byte[] data, ref int nIndex, bool isPortion)
        {
            //   data: header and tail have been removed already
            // packet at least 48 bytes long
            if (data.Length < 48)
                return null;

            byte[] b1b = new byte[1];
            byte[] b2b = new byte[2];
            byte[] b4b = new byte[4];
            int j = nIndex;

            int nType;
            float fAmp;
            double fLat, fLon, fHeight;
            string sSolution = "";
            string sTime;
            int nFlashSize;
            int nConfidence;

            //size of flash portion
            b2b[0] = data[j++]; b2b[1] = data[j++];
            nFlashSize = ByteArrayToInt(b2b, 2);

            //type
            b1b[0] = data[j++];
            nType = ByteArrayToInt(b1b, 1);  //c-g strike

            //time, second since 1970
            //            b4b[0] = data[j++]; b4b[1] = data[j++]; b4b[2] = data[j++]; b4b[3] = data[j++];
            //            nSecond = ByteArrayToInt(b4b, 4);

            //nanoseconds            
            //            b4b[0] = data[j++]; b4b[1] = data[j++]; b4b[2] = data[j++]; b4b[3] = data[j++];
            //            nNonosecond = ByteArrayToInt(b4b, 4);

            //time as string
            byte[] b0 = new byte[29];
            for (int i = 0; i < 29; i++)
                b0[i] = data[j++];
            sTime = Encoding.ASCII.GetString(b0);

            //latitude
            b4b[0] = data[j++]; b4b[1] = data[j++]; b4b[2] = data[j++]; b4b[3] = data[j++];
            fLat = (double)(ByteArrayToInt(b4b, 4) / 10000000.0);

            //longitude
            b4b[0] = data[j++]; b4b[1] = data[j++]; b4b[2] = data[j++]; b4b[3] = data[j++];
            fLon = (double)(ByteArrayToInt(b4b, 4) / 10000000.0);

            //height
            b4b[0] = data[j++]; b4b[1] = data[j++]; b4b[2] = data[j++]; b4b[3] = data[j++];
            int temp = ByteArrayToInt(b4b, 4);
            fHeight = (double)(temp / 100.0);

            //amp
            b4b[0] = data[j++]; b4b[1] = data[j++]; b4b[2] = data[j++]; b4b[3] = data[j++];
            fAmp = (float)ByteArrayToInt(b4b, 4);

            //size of flash portion
            b2b[0] = data[j++]; b2b[1] = data[j++];
            nConfidence = ByteArrayToInt(b2b, 2);

            //size of stroke solution
            b2b[0] = data[j++]; b2b[1] = data[j++];
            int nLen = ByteArrayToInt(b2b, 2);

            //stroke solution
            //          int nLen =nFlashSize -j ;
            if (nLen > 0)
            {
                byte[] b = new byte[nLen];
                for (int i = 0; i < nLen; i++)
                    b[i] = data[j++];

                sSolution = Encoding.ASCII.GetString(b);
            }



            nIndex = j;

            FlashData flash = null;
            if (isPortion)
            {
                flash = new LTGFlashPortion(sTime, fLat, fLon, fHeight, fAmp, sSolution, (FlashType)nType, nConfidence);
            }
            else
            {
                flash = new LTGFlash(sTime, fLat, fLon, fHeight, fAmp, sSolution, (FlashType)nType, nConfidence);
            }

            return flash;
        }

        //format offsets to string for TCP
        public static string FormatOffsetAbsPeakAmplitude(List<Offset> offsetList)
        {
            string val = string.Empty;

            if (offsetList != null && offsetList.Count > 0)
            {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < offsetList.Count; i++)
                {
                    sb.Append(offsetList[i].StationId).Append("=").Append(offsetList[i].AbsPeakAmplitude);
                    if (i < offsetList.Count - 1)
                    {
                        sb.Append(",");
                    }
                }
                val = sb.ToString();
            }

            return val;
        }

        private static List<Tuple<string, short>> ParseOffsetAbsPeakAmplitude(string value)
        {
            var offsetAmplitudes = new List<Tuple<string, short>>();
            string[] offsets = value.Split(',');
            foreach (string str in offsets)
            {
                string[] stationOffset = str.Split('=');
                if (stationOffset.Length == 2)
                {
                    short amp;
                    if (short.TryParse(stationOffset[1], out amp))
                    {
                        offsetAmplitudes.Add(new Tuple<string, short>(stationOffset[0], amp));
                    }
                }
            }

            return offsetAmplitudes;
        }

        public static int ByteArrayToInt(byte[] buffer, int nSize)
        {
            int n = 0;
            switch (nSize)
            {
                case 1:
                    //					n = (int)BitConverter.ToInt16(buffer, 0);
                    n = Convert.ToInt32(buffer[0]);
                    break;
                case 2:
                    n = BitConverter.ToUInt16(buffer, 0);
                    break;
                case 4:
                    n = BitConverter.ToInt32(buffer, 0);
                    break;
            }
            return n;
        }

        public static int BigEndianBytesToInt(byte[] data, int offset, int size)
        {
            if (size > 4)
            {
                throw new ArgumentOutOfRangeException("size", "size cannot be greater than 4");
            }
            if (data.Length < offset + size - 1)
            {
                throw new ArgumentOutOfRangeException("size", "size extends beyond array");
            }

            int value = 0;
            if (size == 1)
            {
                value = data[offset];
            }
            else if (size == 2)
            {
                value = (data[offset] << 8) + data[offset + 1];
            }
            else if (size == 3)
            {
                value = (data[offset] << 16) + (data[offset + 1] << 8) + data[offset + 2];
            }
            else if (size == 4)
            {
                value = (data[offset] << 24) + (data[offset + 1] << 16) + (data[offset + 2] << 8) + data[offset + 3];
            }


            return value;
        }

        public static bool IsValidTime(int nSecond)
        {
            DateTime dt = new System.DateTime(1970, 1, 1).AddSeconds(nSecond);
            TimeSpan ts = DateTime.Now - dt;

            //playback may play very old file
            if (Math.Abs(ts.TotalDays) > 3000)
                return false;

            return true;
        }

        private static ArrayList GetPulseData(byte[] data, ref int nPos)
        {
            ArrayList alSample = new ArrayList();
            if (nPos + 7 > data.Length)
                return alSample;

            byte[] b1b = new byte[1];
            byte[] b2b = new byte[2];
            byte[] b4b = new byte[4];
            int j = nPos;

            UInt32 initTick;
            short initE;
            short prevE;
            int nRemainBytes = 0;

            //pulse section header
            b4b[0] = data[j++]; b4b[1] = data[j++]; b4b[2] = data[j++]; b4b[3] = data[j++];
            initTick = (UInt32)IPAddress.NetworkToHostOrder(ByteArrayToInt(b4b, 4));
            b2b[0] = data[j++]; b2b[1] = data[j++];
            initE = IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2));
            prevE = initE;
            b1b[0] = data[j++];
            nRemainBytes = ByteArrayToInt(b1b, 1);
            Sample sample0 = new Sample();

            sample0.E = initE;
            sample0.Tick = initTick;
            alSample.Add(sample0);

            //Delt Es
            for (int i = 0; i < nRemainBytes; i++)
            {
                Sample sample = new Sample();
                sample.Tick = (uint)(initTick + i + 1);

                if (j >= data.Length)
                {
                    string msg = string.Format("Partial packet found, length:{0}, bytes missing:{1}", data.Length, nRemainBytes - j);
                    throw new Exception(msg);
                }

                b1b[0] = data[j++];
                sbyte c = (sbyte)(b1b[0]);
                sample.E = (short)(prevE + c);
                //if (sample.E > 511)
                //    Console.WriteLine("Wrong");
                alSample.Add(sample);

                prevE = sample.E;

            }
            nPos = j;

            return alSample;
        }

        public static byte[] GetStreamingBuffer(PacketHeaderInfo dataHeader)
        {
            byte[] buffer = null;

            if (dataHeader.TotalLength > UInt16.MaxValue)
            {
                EventManager.LogError(TaskMonitor.ManagerArchiveProcessor, string.Format("Error getting streaming buffer, packet to large: {0} bytes", dataHeader.TotalLength));
            }
            else
            {
                UInt16 length = (UInt16)dataHeader.TotalLength;
                byte[] len = BitConverter.GetBytes(length);
                buffer = new byte[length + 4];

                Array.Copy(len, 0, buffer, 0, 2);
                Array.Copy(dataHeader.RawData, 0, buffer, 2, length);
                Array.Copy(LtgConstants.TcpEndMarker, 0, buffer, length + 2, 2);
            }

            return buffer;
        }

        public static PacketHeaderInfo GetAKeepAlivePacket()
        {
            try
            {
                byte[] buf = new byte[1];
                buf[0] = (byte)PacketMagicNumber.MagicKeepAlive;
                PacketHeaderInfo header = new PacketHeaderInfo();
                if (RawPacketUtil.GetPacketHeaderInfo(buf, header))
                {
                    return header;
                }
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        //#region PackLightningData
        //public static unsafe Byte[] PackLightningData(int system,
        //    int data_type,
        //    int minutes_since_midnight,
        //    int milliseconds_since_minute_start,
        //    int theYear, int theMonth, int theDay,
        //    int Longitude, int random1, int Magnitude_Polarity,
        //    int Stroke_Solution, int Latitude, int random2,
        //    int random3, int random4, int checksum)
        //{
        //    Byte[] data = new Byte[24];
        //    Byte aByte = 0;
        //    Byte* aByte_ptr;
        //    ushort ushort_val;

        //    //BYTE 0
        //    //System

        //    try
        //    {
        //        PosBoundCheck(15, system, "system", false);
        //        aByte = checked((Byte)system);
        //        aByte <<= 4;
        //    }
        //    catch (Exception e)
        //    {
        //        //this.pLogFile.WriteLine("Error Packing  [{0}], value = [{1}], {2} ", "system", system, e);
        //    }

        //    //Lightning Type
        //    try
        //    {
        //        PosBoundCheck(15, data_type, "data_type", false);

        //        aByte |= checked((Byte)data_type);
        //        data[0] = aByte;
        //    }
        //    catch (Exception e)
        //    {
        //        //this.pLogFile.WriteLine("Error Packing  [{0}], value = [{1}], {2} ", "data_type", data_type, e);
        //    }

        //    //BYTE 1 - High, Minutes Since Midnight
        //    //BYTE 2 - Low, Minutes Since Midnight

        //    try
        //    {
        //        ushort_val = checked((ushort)minutes_since_midnight);

        //        aByte_ptr = (byte*)&ushort_val;
        //        data[1] = aByte_ptr[1];
        //        data[2] = aByte_ptr[0];

        //    }
        //    catch (Exception e)
        //    {
        //        //this.pLogFile.WriteLine("Error Packing  [{0}], value = [{1}], {2} ", "minutes_since_midnight", minutes_since_midnight, e);
        //    }

        //    //BYTE 3 - Low, milliseconds_since_minute_start
        //    //BYTE 4 - High, milliseconds_since_minute_start
        //    try
        //    {
        //        ushort_val = checked((ushort)milliseconds_since_minute_start);
        //        aByte_ptr = (byte*)&ushort_val;
        //        data[4] = aByte_ptr[1];
        //        data[3] = aByte_ptr[0];

        //    }
        //    catch (Exception e)
        //    {
        //        //this.pLogFile.WriteLine("Error Packing  [{0}], value = [{1}], {2} ", "milliseconds_since_minute_start", milliseconds_since_minute_start, e);
        //    }

        //    //BYTE 5 - High, Middle Bits of Year
        //    //BYTE 5 - Low, Low Bits of Year JVT BEGIN HERE
        //    try
        //    {
        //        PosBoundCheck(4096, theYear, "theYear", false);

        //        aByte_ptr = (Byte*)&theYear;
        //        data[5] = aByte_ptr[0];

        //    }
        //    catch (Exception e)
        //    {
        //        //this.pLogFile.WriteLine("Error Packing  [{0}], value = [{1}], {2} ", "theYear", theYear, e);
        //    }

        //    //BYTE 6 - High, Month of Year (1-12)
        //    //BYTE 6 - Low, High Bits of Day of Month
        //    try
        //    {
        //        PosBoundCheck(12, theMonth, "theMonth", false);
        //        aByte_ptr = (Byte*)&theMonth;
        //        data[6] = aByte_ptr[0];
        //        data[6] <<= 4;

        //        aByte_ptr = (Byte*)&theDay;
        //        aByte = aByte_ptr[0];
        //        aByte >>= 4;  //Put upper 4 bits in lower end of byte
        //        data[6] |= aByte;

        //    }
        //    catch (Exception e)
        //    {
        //        //this.pLogFile.WriteLine("Error Packing  [{0}], value = [{1}], {2} ", "theMonth", theMonth, e);
        //    }


        //    //BYTE 7 - High, Low Bitsof Day of Month
        //    //BYTE 7 - Low, High Bits of Year
        //    try
        //    {
        //        PosBoundCheck(31, theDay, "theDay", false);
        //        aByte_ptr = (Byte*)&theDay;
        //        aByte = aByte_ptr[0];
        //        aByte <<= 4;
        //        data[7] = aByte;

        //        aByte_ptr = (Byte*)&theYear;
        //        aByte = aByte_ptr[1];
        //        aByte &= 0x0F;  //Clear out upper 4 bits for following OR
        //        data[7] |= aByte;

        //    }
        //    catch (Exception e)
        //    {
        //        //this.pLogFile.WriteLine("Error Packing  [{0}], value = [{1}], {2} ", "system", system, e);
        //    }


        //    //BYTE 8 - 11  Longitude
        //    try
        //    {
        //        PosBoundCheck(4294967295, Longitude, "Longitude", true);
        //        aByte_ptr = (Byte*)&Longitude;
        //        data[8] = aByte_ptr[0];
        //        data[9] = aByte_ptr[1];
        //        data[10] = aByte_ptr[2];
        //        data[11] = aByte_ptr[3];

        //    }
        //    catch (Exception e)
        //    {
        //        //this.pLogFile.WriteLine("Error Packing  [{0}], value = [{1}], {2} ", "Longitude", Longitude, e);
        //    }

        //    //BYTE 12 - Random
        //    try
        //    {
        //        PosBoundCheck(255, random1, "random1", false);
        //        aByte_ptr = (Byte*)&random1;
        //        data[12] = aByte_ptr[0];
        //    }
        //    catch (Exception e)
        //    {
        //        //this.pLogFile.WriteLine("Error Packing  [{0}], value = [{1}], {2} ", "random1", random1, e);
        //    }

        //    //BYTE 13 - Amplitude LSB
        //    try
        //    {
        //        PosBoundCheck(65535, Magnitude_Polarity, "Magnitude_Polarity", false);
        //        aByte_ptr = (Byte*)&Magnitude_Polarity;
        //        data[13] = aByte_ptr[0];

        //        //BYTE 14 - Amplitude MSB
        //        data[14] = aByte_ptr[1];

        //    }
        //    catch (Exception e)
        //    {
        //        //this.pLogFile.WriteLine("Error Packing  [{0}], value = [{1}], {2} ", "Magnitude_Polarity", Magnitude_Polarity, e);
        //    }

        //    //BYTE 15 - 18  Latitude
        //    try
        //    {
        //        PosBoundCheck(2147483647, Latitude, "Latitude", true);
        //        aByte_ptr = (Byte*)&Latitude;
        //        data[15] = aByte_ptr[0];
        //        data[16] = aByte_ptr[1];
        //        data[17] = aByte_ptr[2];
        //        data[18] = aByte_ptr[3];

        //    }
        //    catch (Exception e)
        //    {
        //        //this.pLogFile.WriteLine("Error Packing  [{0}], value = [{1}], {2} ", "Latitude", Latitude, e);
        //    }


        //    //BYTE 19 - Stroke Solution
        //    try
        //    {
        //        PosBoundCheck(255, Stroke_Solution, "Stroke_Solution", false);
        //        aByte_ptr = (Byte*)&Stroke_Solution;
        //        data[19] = aByte_ptr[0];

        //    }
        //    catch (Exception e)
        //    {
        //        //this.pLogFile.WriteLine("Error Packing  [{0}], value = [{1}], {2} ", "Stroke_Solution", Stroke_Solution, e);
        //    }

        //    //BYTE 20 - 22 - Random
        //    try
        //    {
        //        PosBoundCheck(255, random2, "random2", false);
        //        PosBoundCheck(255, random3, "random3", false);
        //        PosBoundCheck(255, random4, "random4", false);

        //        aByte_ptr = (Byte*)&random2;
        //        data[20] = aByte_ptr[0];
        //        aByte_ptr = (Byte*)&random3;
        //        data[21] = aByte_ptr[0];
        //        aByte_ptr = (Byte*)&random4;
        //        data[22] = aByte_ptr[0];
        //    }
        //    catch (Exception e)
        //    {
        //        //this.pLogFile.WriteLine("Error Packing  [{0}], value = [{1}], {2} ", "RANDOM234", random2, e);
        //    }


        //    //Byte 23 - Checksum
        //    //Sum 1st 23 bytes
        //    byte bytesum = 0;

        //    for (int ct = 0; ct < 23; ++ct)
        //    {
        //        bytesum += data[ct];
        //    }

        //    data[23] = (byte)(256 - bytesum);
        //    //pLogFile.WriteLine("ByteSum = {0}, Checksum = {1}", bytesum, data[23]);
        //    //this.pLogFile.Flush();

        //    /* Ignore the DB value for now			
        //    PosBoundCheck ( 255, checksum, "checksum" );
        //    aByte_ptr = (Byte*) &checksum;
        //    data[23] = aByte_ptr[0];
        //    */
        //    return data;


        //}//PackLightningData



        //static public void PosBoundCheck(long bound, long val, string name, bool can_be_negative)
        //{


        //    if ((!can_be_negative) && val < 0)
        //    {
        //        throw new Exception("[" + name + "] Negative Value [" + val + "] not allowed.");
        //    }

        //    if (val > bound)
        //    {
        //        throw new Exception("[" + name + "] Value [" + val + "] too high. Limit is [" + bound + "]");
        //    }
        //} //end posboundcheck


        //#endregion

        public static List<Waveform> ConvertTickToTime(SecondOfSample sample)
        {
            List<Waveform> waveforms = new List<Waveform>();
            ConvertTickToTime(sample, waveforms);
            if (waveforms.Count == 0)
            {
                waveforms = null;
            }
            return waveforms;
        }

        public static void ConvertTickToTime(SecondOfSample sample, List<Waveform> waveforms)
        {
            if (sample.MajorVersion == SensorVersion.ID_VERSION_SENSOR_9_MAJOR)
            {
                if (sample.MinorVersion == SensorVersion.ID_VERSION_SENSOR_9_MINOR0)
                {
                    ConvertSensor90TickToTime(sample, waveforms);
                }
                else if (sample.MinorVersion >= SensorVersion.ID_VERSION_SENSOR_9_MINOR1
                    && sample.MinorVersion <= SensorVersion.ID_VERSION_SENSOR_9_MINOR3)
                {
                    ConvertSensor91TickToTime(sample, waveforms);
                }
                else if (sample.MinorVersion <= SensorVersion.ID_VERSION_SENSOR_9_MINOR4)
                {
                    ConvertSensor91TickToTime(sample, waveforms);
                }
            }
            else if (sample.MajorVersion == SensorVersion.ID_VERSION_SENSOR_10_MAJOR)
            {
                if (sample.MinorVersion <= SensorVersion.ID_VERSION_SENSOR_10_MINOR0)
                {
                    ConvertSensor91TickToTime(sample, waveforms);
                }
            }

        }


        private static void ConvertSensor90TickToTime(SecondOfSample sample, List<Waveform> waveforms)
        {
            if (sample.NumberOfHfSamples > 0)
            {
                for (int i = 0; i < sample.NumberOfHfSamples; i++)
                {
                    Sample oneSample = (Sample)sample.HfSamples[i];
                    uint sampleNumber = oneSample.Tick;

                    int nanoseconds = (int)(1e9 * (float)(sample._nHFSclkTicksPpsToFirstSample + sampleNumber * sample._nHFSclkTicksBetweenSamples)
                            / sample._nHFSclkTicksInThisSec) + sample.ppsOffsetInNanoseconds;

                    if (nanoseconds < 0) continue;
                    if (nanoseconds > 999999999) continue;

                    Waveform waveform = new Waveform
                    {
                        TimeStamp = new DateTimeUtcNano(sample.UTCDateTime, nanoseconds),
                        Amplitude = oneSample.E
                    };
                    waveforms.Add(waveform);
                }
            }
        }

        private static void ConvertSensor91TickToTime(SecondOfSample sample, List<Waveform> waveforms)
        {
            if (sample.NumberOfHfSamples > 0)
            {
                Waveform prewaveform = new Waveform();

                for (int i = 0; i < sample.NumberOfHfSamples; i++)
                {
                    Sample oneSample = (Sample)sample.HfSamples[i];
                    uint sampleNumber = oneSample.Tick;

                    int nanoseconds = (int)(1e9 * (float)(sample._nHFSclkTicksPpsToFirstSample + sampleNumber * sample._nHFSclkTicksBetweenSamples)
                            / sample._nHFSclkTicksInThisSec) + sample.ppsOffsetInNanoseconds;

                    if (nanoseconds < 0) continue;
                    if (nanoseconds > 999999999) continue;

                    Waveform waveform = new Waveform();
                    DateTimeUtcNano timeStamp = new DateTimeUtcNano(sample.UTCDateTime, nanoseconds);

                    waveform.TimeStamp = timeStamp;
                    waveform.Amplitude = oneSample.E;

                    if (waveform.TimeStamp > prewaveform.TimeStamp)
                    {
                        waveforms.Add(waveform);
                        prewaveform = waveform;
                    }
                }
            }
        }

        public static byte[] ConvertRawToWaveformBuffer(PacketHeaderInfo header)
        {
            byte[] buffer = null;
            ArrayList alSamples = new ArrayList();
            ArrayList LFSamples = new ArrayList();
            ArrayList SpectraSamples = new ArrayList();

            try
            {
                if (GetSecondSampleInfo(header.RawData, header, alSamples, LFSamples, SpectraSamples))
                {
                    SecondOfSample secondOfSample = new SecondOfSample();
                    secondOfSample.SetSamples(header, alSamples, LFSamples, SpectraSamples);

                    List<Waveform> waveforms = ConvertTickToTime(secondOfSample);
                    if (waveforms != null && waveforms.Count > 0)
                    {
                        int i = 0, j = 0;

                        int length = 1 + 10 + 4 + 2 + waveforms.Count * 6;
                        buffer = new byte[length];

                        buffer[i++] = 88; // Magic
                        for (j = 0; j < header.SensorId.Length && j < 10; j++)
                        {
                            buffer[i++] = (byte)header.SensorId[j];
                        }
                        i = 11;
                        int second = header.PacketTimeInSeconds;

                        DateTime baseTime = LtgTimeUtils.GetUtcDateTimeFrom1970Seconds(second);
                        DateTimeUtcNano startTime = new DateTimeUtcNano(baseTime, 0);

                        //string startTime = LtgTimeUtils.GetDateTimeStringFrom1970Seconds(second);
                        int secondNetwork = IPAddress.HostToNetworkOrder(second);
                        byte[] bytes = BitConverter.GetBytes(secondNetwork);
                        for (j = 0; j < 4; j++)
                        {
                            buffer[i++] = bytes[j];
                        }

                        short number = (short)waveforms.Count;
                        short numberNetwork = IPAddress.HostToNetworkOrder(number);
                        bytes = BitConverter.GetBytes(numberNetwork);
                        for (j = 0; j < 2; j++)
                        {
                            buffer[i++] = bytes[j];
                        }


                        foreach (Waveform waveform in waveforms)
                        {
                            if (waveform.TimeStamp > startTime)
                            {
                                int time = waveform.TimeStamp.NanosecondsFromBaseTime;
                                int timeNetwork = IPAddress.HostToNetworkOrder(time);
                                bytes = BitConverter.GetBytes(timeNetwork);
                                for (j = 0; j < 4; j++)
                                {
                                    buffer[i++] = bytes[j];
                                }

                                short amp = waveform.Amplitude;
                                short ampNetwork = IPAddress.HostToNetworkOrder(amp);
                                bytes = BitConverter.GetBytes(ampNetwork);
                                for (j = 0; j < 2; j++)
                                {
                                    buffer[i++] = bytes[j];
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                buffer = null;
            }

            return buffer;
        }
    }
}
