namespace LxCommon
{
    public enum ProcessorType
    {
        ProcessorTypeHF = 0,
        ProcessorTypeLF = 1 // long distance processor considering earth curvature
    }

    // Some useful information
    // Light speed: 299,792,458 m/s, or 299,792km/s, 299km/ms, 0.299km/us

    // Constants of the raw data

    public enum PacketMagicNumber
    {
        MagicUnkow = -1,
        MagicSensor9Packet = 48,     //identify packet from Sersor 9, 0x30
        MagicSensor8Packet = 0xAA,   // First byte in Sensor 8 data
        MagicFlashPacket = 26,        //identify UDP sent from another Lightning Manager
        MagicKeepAlive = 71,            // Keep alive packet
        MagicSensorWWLLN = 33,
        MagicFlashWWLLN = 87     //WWLLN Flash Magic Number "W"
    }

    public enum PacketType
    {
        MsgUnknow = -1,
        MsgTypeLtg = 1, //Lightning waveform Data
        MsgTypeSpectra = 3,
        MsgTypeGps = 4, //GPS Data
        MsgTypeLog = 7, // Sensor 9 Log file
        MsgTypeFlash = 100, // Flash data
        MsgTypeKeepAlive = 101, // Keep alive data
        MsgTypeWWLLNLtg = 15 //Lightning WWLLN Data

    }

    public enum PacketHeaderLength
    {
        HeaderLengthUnknown = -1,
        HeaderLengthLtg8 = 11,
        HeaderLengthLtg90 = 27,
        HeaderLengthLtg91 = 38,
        HeaderLengthLtg92 = 50,
        HeaderLengthLtg93 = 58,
        HeaderLengthLtg94 = 58,
        HeaderLengthLtg100 = 62,
        HeaderLengthLtg101 = 30,
        HeaderLengthGps8 = 236,
        HeaderLengthGps = 246,
        HeaderLengthLog = 16,
        HeaderLengthSpectra = 42,
        HeaderLengthWWLLNUdp = 20
    }

    // Flash Type
    public enum FlashType
    {
        FlashTypeCG = 0,
        FlashTypeIC = 1,
        FlashTypeICNarrowBipolar = 2,
        FlashTypeUnknown = 3,
        FlashTypeGlobalCG = 40,   // From the global network, need to be converted to FlashTypeIC
        FlashTypeGlobalIC = 41   // From the global network, need to be converted to FlashTypeCG
    }


    public enum SensorVersion
    {
        ID_VERSION_UNKNOW = -1,
        ID_VERSION_SENSOR_8 = 8,
        ID_VERSION_SENSOR_8_MINOR0 = 0,
        ID_VERSION_SENSOR_9_MAJOR = 9, // Sensor 9 major version
        ID_VERSION_SENSOR_9_MINOR0 = 0, // First version of sensor 9: Ltg data is uncompressed
        ID_VERSION_SENSOR_9_MINOR1 = 1, // Second version of sensor 9: Ltg data is compressed
        ID_VERSION_SENSOR_9_MINOR2 = 2, // Low frequency data is added: this one is short lived, not used
        ID_VERSION_SENSOR_9_MINOR3 = 3,  // Low frequency and Spectrum data added
        ID_VERSION_SENSOR_9_MINOR4 = 4,  // New compression 
        ID_VERSION_SENSOR_10_MAJOR = 10, // Sensor 10 with TOGA info added
        ID_VERSION_SENSOR_10_MINOR0 = 0,  // First version of sensor 10
        ID_VERSION_SENSOR_10_MINOR1 = 1  // First version of sensor 10
    }

    // Some of the followings willb be moved to config file
    public abstract class LtgConstants
    {
        // Constants of service config
        /*/ original
        public const int NumberOfTcpHandlerThreads = 5;
        public const int NumberOfConverterThreads = 5;
        public const int NumberOfCorrelatorThreads = 20;
        public const int NumberOfLocatorThreads = 25;
        public const int NumberOfPostLocatorThreads = 25;
        // * */

        /*/ TEMP TEMP debugger
        public const int NumberOfTcpHandlerThreads = 1;
        public const int NumberOfConverterThreads = 1;
        public const int NumberOfCorrelatorThreads = 1;
        public const int NumberOfLocatorThreads = 1;
        public const int NumberOfPostLocatorThreads = 1;
        //*/
        //public const int TotalNumOfValidationThreads = 3;
        //public const int NumberOfCorrelatorThreadsLF = 15;
        //public const int NumberOfLocatorThreadsLF = 35;
        //public const int TotalNumOfGlobalFlashMergerThreads = 3;
        //public const int TotalNumOfGrouperThreads = 5;
        //public const int TotalNumOfSensorThreads = 50;


        public const double LightspeedInMetersPerMicrosecond = 299.792458 * 0.99; // light is slower in air than in vacuum
        public const double LightspeedInKMPerMicrosecond = 0.299792458 * 0.99;
        public const double EarthRadiusEquatorialInLightMicroseconds = 21281.5575; // earth centered light microseconds
        public const double EarthRadiusPolarInMeters = 6356752.31424518; //#defined by WGS84
        public const double EarthRadiusEquatorialInMeters = 6378137; // #defined by WGS84, do not replace with more accurate value

        // Communication setup
        public const int TimeOutSensorToReceiver = 5;  // If no data from the sensor for this time, close the socket
        public const int TimeOutPermanentConnection = 20; // A permanent connection, if no data for more than this time, close the socket
        public const int MaxPacketSize = 1048576; //1 megabyte
        public const int MaxClientSocketNumber = 2000;
        public static readonly byte[] TcpEndMarker = { (byte)'|', (byte)'|' }; // This is the separator for the packets in a TCP stream
        public const int MaxLogPacketSize = 64000;
        public const int MaxLightningPacketSize = 10000;

        // Constants for data quality
        public const int MinSamplesInOneShortPortion = 3;   // One short portion must have more than this samples to be considered in the detection
        public const int MinAmplitueFluctuationInOneShortPortion = 5; // There should be peaks   100 * (max_amp - min_amp)/average_amp
        public const int MinVisibleSatellites = 6; // The GPS data considered to be valid
        public const int MaxPacketArriveTimeTooEarlyOrTooLateInSeconds = 60; // Packet is considered too late or too early
        public const double WaveformNoiseThresholdCompareToPeak = 0.01; // If the waveform sample amplitude is less than WaveformNoiseThrsholdCompareToPeak*PeakAmplitude,it is considered noise 

        public const int MinSamplesInOneShortPortionLF = 20;   // One short portion must have more than this samples to be considered in the detection
        public const int MinAmplitueFluctuationInOneShortPortionLF = 30; // There should be peaks   100 * (max_amp - min_amp)/average_amp


        public const int MaxStationNum = 3000; // 
        public const int MaxCorrelationArraySize = 30000;  // microsecond
        public const int MaxTcpDataQueueSizeForProcessing = 5;
        public const int MaxSampleDataQueueSizeForProcessing = 1;
        public const int MaxFlashQueueSize = 10000;
        public const int MaxPortionsInQueueTobeProcessed = 2000;
        public const int MaxSampleDataQueueSize = 5000;
        public const int MaxTcpRawDataQueueSize = 10000;
        public const int MaxSecForDataQueue = 10;
        public const int MaxTcpSenderDataQueue = 5000; // This is the queue for the TCP sender for Receiver->Processor, Processor->LtgManager
        public const int MaxWaveformListSizeOfOneSensor = 90;  // 60 seconds for each sensor to store the waveformOfSecond
        public const int MaxWaveformListSizeOfAllSensors = 10000; // the total number of waveformOfSeconds for all sensors

        // Constants for the Correlator
        // public const int minSensorNumberOfValidDetections = 10;  // Minimu number of sensors to agree on one detection, this will be changed by the data in App.config
        public const int MaxSensorNumberOfValidDetections = 12; // If more than this number of sensors agree on one detection, the locator will stop further calculation in order to save time
        public const double MaxResidualErrorsInLocatorPrelocation = 4.0; // Microseconds, each sensor meets this value against the location from the cone intersection
        public const double MaxResidualErrorsInLocator = 4.0; // Microseconds, each sensor needs to have the error less than this value in order to be considered in the detection
        public const double MaxResidualErrorsInLocatorLF = 5.0; // Microseconds, each sensor needs to have the error less than this value in order to be considered in the detection
        //public const double MaxGlobalLocatorBadnessInSeconds = 5.0e-4; //1.5e-5; // Global locator Badness in seconds
        public const int TimeSlotSizeOfEachThreadInSecond = 1; // in seconds, each corellator thread will process these many seconds of data
        //public const int MinTimeSlotSizeOfEachThreadInSecond = 1; // must have at least one second to process
        public const int ProcessWaveformTimeDelayInSecondHF = 4; // Max wait to  process in seconds
        public const int ProcessWaveformTimeDelayInSecondLF = 20; // Max wait to  process in seconds
        public const int GroupFlashportionTimeDelayInMillisecondLF = 10000; // 
        public const int GroupFlashportionTimeDelayInMillisecondHF = 5000; // 
        public const int TimeslotPacketOverlappingTimeInSecond = 1; //seconds
        public const int TimeslotWaveformOverlappingTimeInMicrosecond = 1000; //Microsecond
        public const double TimeslotWaveformOverlappingTimeInMicrosecondLF = 60000; //Microsecond
        public const int MaxCommonPortionLengthInMicrosecond = 4000; // Microseconds, if the portion is longer than this length, it should be divided into more portions
        public const int MaxNumOfShortportionsInCommonPortion = 30; // Max sensors used in a common short portion
        public const int MaxSensorNumberInPostProcessingLocator = 20; // Max sensors used in a common short portion
        public const double MinGapOfCommonPortionInSecond = 0.002; // second, the waveforms are grouped in one common portion from all sensors
        public const double MinGapOfShortPortionInSecond = 0.00035; // second, the waveforms are grouped in one short portion for a single sensor
        public const double MinGapOfShortPortionInMicrosecond = MinGapOfShortPortionInSecond * 1e6; // second, the waveforms are grouped in one short portion for a single sensor
        public const double MinGapOfShortPortionInNanoseconds = MinGapOfShortPortionInMicrosecond * 1000; // second, the waveforms are grouped in one short portion for a single sensor
        public const double MinGapOfFlashPortionInSecond = 0.0004; // Two flash portions should be not too close to each other, avoid duplicate due to different sensors
        public const double MinGapOfFlashPortionInMicroseconds = MinGapOfFlashPortionInSecond * 1000000; // Two flash portions should be not too close to each other, avoid duplicate due to different sensors
        public const double MinGapOfCommonPortionInSecondLF = 0.030; // second, the waveforms are grouped in one common portion from all sensors
        public const double MinGapOfShortPortionInSecondLF = 0.002; // second, the waveforms are grouped in one short portion for a single sensor
        public const int MaxOffsetValueInLocatorInMicrosecond = 4000; // microsecond, the sensor is too far away from the lightning beyond this value
        public const int MaxLinearFittingDistanceInWaveformInMicrosecond = 50; // Microseconds, if the distance of two continuous points in the waveform is larger than this, the amplitudes will be 0 for all points in between
        public const int MaxTimeSpentInLocatorInMillisecond = 80; // milliseconds spent in the lcoator, so we don't spend too much time  to halt the program
        public const int MinNearestSensorConfidenceLevelForPreLocator = 30; // 0 to 100
        public const int MinNearestSensorConfidenceLevelForPostLocator = 70; // 0 to 100
        public const int TimeRangeForCheckMaxSensorsInMillisecond = 1; // Check the max number of sensors in this range, all the waveform should be considered from a singlg stroke
        public const int MinSensorAroundFlashAngle = 35; // 0 to 360
        public const int MinSensorBinDistribution = 3;
        public const int MaxBadToasInDetectionRange = 500;
        public const int MaxToaStandardDeviation = 100;         // If the standard devition of flash time based on TOA is greater this value, the stroke is invalid
        public const int SaturationValueForDetection = 28000;   // when the peak of a waveform excceeds this value, it is considered saturated
        //public const int SaturationValueForPeakcurrent = 17000;   // when the peak of a waveform excceeds this value, it is considered saturated
        public const int SaturationValueForPeakcurrent = 28000;   // when the peak of a waveform excceeds this value, it is considered saturated
        public const int MinStrokesInACluster = 5; // The number of strokes detected from a group of sensors that agree with each
        // we may be able to remove these, visit later
        public const double MaxDistanceOfGroupSensorsInMircrosecond = 5000; // in microsecond, sensors farther than this value can not be used together (1500km)
        public const double MaxDistanceOfSensorToFlashInMicrosecond = 20000; // in microsecond, sensors farther than this is considered too far from the flash (3000km), light speed 0.3km/microsecond
        public const double MaxDistanceOfSensorToFlashInMicrosecondLF = 26000; // in microsecond, sensors farther than this is considered too far from the flash (8000km), light speed 0.3km/microsecond
        public const int MinDistanceOfTwoSensorsInMicrosecond = 100; // in microsecond, sensors closer than this value can not be used together (30KM)
        public const double MinDistanceOfTwoSensorsInMicrosecondLF = 1500; // in microsecond, sensors closer than this value can not be used together
        public const double MaxDistanceOfGroupSensorsInMircrosecondLF = 30000; // 9000 KM
        public const double MaxDistanceForICTravelInMicrosecond = 5000; // 5ms
        public const double MinPeakCurrentForNegativeCg = 3500;
        public const double MinPeakCurrentForPositiveCg = 15000;
        public const double MaxPeakCurrentForIc = 30000;
        public const double MaxAllowedPeakCurrent = 500000;
        public const double MaxAllowedErrorEllipseMajorAxis = 6;

        public const int MaxSizeOfBinsForCrossCorrelation = 300;  // The array size of the bin (each bin is one microsecond)
        public const int MaxErrorsInTOAInMicrosecond = 100; // used in comparison of the difference of time of arrivals with the distance of two sensors
        public const int MaxErrorsInTOAInMicrosecondLF = 60; // used in comparison of the difference of time of arrivals with the distance of two sensors
        public const int MinSensorDistanceMatchPercentage = 40; // The percentage sensors have to meet the distance check in grouping the common portions and offsets
        public const double MachineEpsilon = 5e-16;

        public const double MinTimeInMicroSecondBetweenTwoStrokesInSameLocation = 1500; // This will prevent duplicate strokes

        public const int MaxNumberOfNearestSensorsForClassification = 5;  // Use the nearest sensors to detect the return stroke
        public const double MinDistanceForClassificationInMicrosecond = 5000; // If the farthest sensor is greater than this, it is CG 900km
        public const int TickTimeOffsetInNanoseconds = -785;  // All sensors with version 9.0 and 9.1 should have this correction, added at 6/3/2009


        // Lightning Manager constants
        //public const int MaxCheckBackTimeForGlobalInSeconds = 600;
        //public const short MaxCheckbackDistanceForGlobalInKM = 80;
        public const short MaxGlobalTreeSizeInKM = 800;
        public const double MaxQuadtreeNodeSizeInDegree = 1;

        public static FlashType ConvertWwllnTypeToTlnType(FlashType type)
        {
            FlashType ft = type;
            switch (ft)
            {
                case FlashType.FlashTypeGlobalCG:
                    ft = FlashType.FlashTypeCG;
                    break;
                case FlashType.FlashTypeGlobalIC:
                    ft = FlashType.FlashTypeIC;
                    break;
            }
            return ft;
        }
    }

}
