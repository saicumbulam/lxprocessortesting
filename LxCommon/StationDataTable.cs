﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aws.Core.Utilities;
using LxCommon.Config;
using LxCommon.Database;

namespace LxCommon
{
    public class StationDataTable
    {
        private const int ConcurrencyLevel = 10;
        private const int InitialCapacity = 1500;
        private readonly ConcurrentDictionary<string, StationData> _data = new ConcurrentDictionary<string, StationData>(ConcurrencyLevel, InitialCapacity);
        private DateTime _lastRefreshTime = DateTime.MinValue;
        private ConcurrentDictionary<string, int> _networkSensorCount = new ConcurrentDictionary<string, int>(); 

        public DateTime LastRefreshTime
        {
            get { return _lastRefreshTime; }
        }

        public void InitalLoad()
        {
            UpdateStationGpsData();

            ICollection<string> keys = _networkSensorCount.Keys;
            foreach (string key in keys)
            {
                int count;
                if (!_networkSensorCount.TryGetValue(key, out count))
                {
                    count = -1;
                }
                EventManager.LogInfo(string.Format("StationDataTable: updating Network Sensor Count: NetworkID = {0}; New Sensor Count = {1} ", key, count));
            }

            if (_data != null)
            {
                UpdateStationCalibrationData();
                BuildStationDistanceHash();
            }

            _lastRefreshTime = DateTime.UtcNow;
        }

        public void Update(object state)
        {
            UpdateStationQcData();

            ICollection<string> keys = _networkSensorCount.Keys;
            foreach (string key in keys)
            {
                int count;
                if (!_networkSensorCount.TryGetValue(key, out count))
                {
                    count = -1;
                }
                EventManager.LogInfo(string.Format("StationDataTable: updating Network Sensor Count: NetworkID = {0}; New Sensor Count = {1} ", key, count));
            }

            if (_data != null)
            {
                UpdateStationCalibrationData();
                BuildStationDistanceHash();
            }

            _lastRefreshTime = DateTime.UtcNow;
        }

        public void Load(object state)
        {
            // Load station GPS data only once, then use the live data from the sensors
            if (_data.Count < 1)
            {
                UpdateStationGpsData();
            }
            else
            {
                UpdateStationQcData();
            }

            ICollection<string> keys = _networkSensorCount.Keys;
            foreach (string key in keys)
            {
                int count;
                if (!_networkSensorCount.TryGetValue(key, out count))
                {
                    count = -1;
                }
                EventManager.LogInfo(string.Format("StationDataTable: updating Network Sensor Count: NetworkID = {0}; New Sensor Count = {1} ", key, count));
            }
            if (_data != null)
            {
                UpdateStationCalibrationData();
                BuildStationDistanceHash();

                //if (AppConfig.ProcessorType == ProcessorType.ProcessorTypeLF)
                //    UpdateGlobalStations(); 
            }
            _lastRefreshTime = DateTime.UtcNow;
        }

        private void UpdateStationGpsData()
        {
            GetStationDataDbCmdText dbCmd = new GetStationDataDbCmdText();
            List<StationData> lsd = dbCmd.Execute();
            _networkSensorCount.Clear();
            if (lsd != null && lsd.Count > 0)
            {
                Parallel.ForEach(lsd, s =>
                    {
                        _data.AddOrUpdate(s.StationID, s, (key, existingValue) => s);
                        if (s.Networks != null)
                        {
                            foreach (string network in s.Networks)
                            {
                                _networkSensorCount.AddOrUpdate(network, 1, (key, existingValue) =>
                                {
                                    existingValue++;
                                    return existingValue;
                                });
                            }
                        }
                       
                    });
            }

            _networkSensorCount.AddOrUpdate("Total", _data.Count, (key, existingValue) => _data.Count);
        }

        private void UpdateStationQcData()
        {
            GetStationDataDbCmdText dbCmd = new GetStationDataDbCmdText();
            List<StationData> lsd = dbCmd.Execute();
            _networkSensorCount.Clear();
            if (lsd != null && lsd.Count > 0)
            {
                Parallel.ForEach(lsd, s =>
                    {
                        _data.AddOrUpdate(s.StationID,
                            (key) =>
                            {
                                EventManager.LogInfo("New sensor added from database: " + s.StationID);
                                return s;
                            },
                            (key, existingValue) =>
                            {
                                existingValue.NetworkType = s.NetworkType;
                                existingValue.QcFlag = s.QcFlag;
                                existingValue.Networks = s.Networks;
                                return existingValue;
                            });
                        
                        if (s.Networks != null)
                        {
                            foreach (string network in s.Networks)
                            {
                                _networkSensorCount.AddOrUpdate(network, 1, (key, existingValue) =>
                                {
                                    existingValue++;
                                    return existingValue;
                                });
                            }
                        }
                    });
            }

            _networkSensorCount.AddOrUpdate("Total", _data.Count, (key, existingValue) => _data.Count);
        }

        // load the global network sensors and mark the sensor active for LF/global detection
        private void UpdateGlobalStations()
        {
            // Sensors excluded from LF detector
            Rect excludedSensorRectrect = new Rect(-117, -79, 33, 44);
            foreach (StationData sd in _data.Values)
            {
                if (sd.Longitude > excludedSensorRectrect.left && sd.Longitude < excludedSensorRectrect.right &&
                    sd.Latitude > excludedSensorRectrect.bottom && sd.Latitude < excludedSensorRectrect.top)
                    sd.QcFlag = 0;
            }    
        }

        private void UpdateStationCalibrationData()
        {
            GetStationCalibrationDbCmdText dbCmd = new GetStationCalibrationDbCmdText();
            List<StationDecibelParameter> lsdp = dbCmd.Execute();

            if (lsdp != null && lsdp.Count > 0)
            {
                Parallel.ForEach(lsdp, sdp =>
                {
                    StationData sd;
                    if (_data.TryGetValue(sdp.StationId, out sd))
                    {
                        sd.CalibrationData.UpdateCalibrationData(sdp);
                        sd.CalibrationData.CalibrationInfoLoaded = true;
                    }
                });
            }
        }

        private void BuildStationDistanceHash()
        {
            if (_data != null)
            {
                List<string> stationIds = new List<string>(_data.Keys);
                if (stationIds.Count > 0)
                    Parallel.ForEach(stationIds, BuildStationDistanceHash);
            }
        }

        private void BuildStationDistanceHash(string stationId)
        {
            StationData sd;
            try
            {
                if (_data.TryGetValue(stationId, out sd))
                {
                    foreach (StationData station in _data.Values)
                    {
                        double distance;

                        if (AppConfig.ProcessorType == ProcessorType.ProcessorTypeLF)
                            distance = StationData.Wgs84DistanceInMicrosecond(sd.Latitude, sd.Longitude, station.Latitude, station.Longitude);
                        else
                            distance = sd.DistanceFromStationInMicrosecond(station);

                        if (sd.DistanceInMicrosecondTable.ContainsKey(station.StationID))
                            sd.DistanceInMicrosecondTable[station.StationID] = distance;
                        else
                            sd.DistanceInMicrosecondTable.Add(station.StationID, distance);
                    }
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(string.Format("Error updating station data hash for {0}", stationId), ex);
            }
        }

        public void UpdateStationDistance(string stationId)
        {
            StationData sd;
            if (_data.TryGetValue(stationId, out sd))
            {
                foreach (StationData station in _data.Values)
                {
                    double distance;

                    if (AppConfig.ProcessorType == ProcessorType.ProcessorTypeLF)
                        distance = StationData.Wgs84DistanceInMicrosecond(sd.Latitude, sd.Longitude, station.Latitude, station.Longitude);
                    else
                        distance = sd.DistanceFromStationInMicrosecond(station);

                    if (sd.DistanceInMicrosecondTable.ContainsKey(station.StationID))
                        sd.DistanceInMicrosecondTable[station.StationID] = distance;
                    else
                        sd.DistanceInMicrosecondTable.Add(station.StationID, distance);

                    if (station.DistanceInMicrosecondTable.ContainsKey(sd.StationID))
                        station.DistanceInMicrosecondTable[sd.StationID] = distance;
                    else
                        station.DistanceInMicrosecondTable.Add(sd.StationID, distance);
                }
            }
        }
        
        public ConcurrentDictionary<string, StationData> Data
        {
            get { return _data; }
        }

        public ConcurrentDictionary<string, int> NetworkSensorCount
        {
            get { return _networkSensorCount; }
        }
    }
}