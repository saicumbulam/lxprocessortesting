﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;

namespace LxCommon.TcpCommunication
{
    public class ListenerSocketState
    {
        public const int DefaultBufferSize = 1024;
        private const int DefaultPacketSize = 10240;
        private const int DefaultSampleSize = 30;
        private readonly byte[] _buffer;
        private readonly List<Byte> _packet;
        private readonly int _bufferSize;
        private Socket _workSocket;
        private string _remoteIp;
        private RateCalculator _receiveRate;
        private int _receiveRateSampleSize;

        public ListenerSocketState() : this(DefaultBufferSize, DefaultPacketSize, DefaultSampleSize) { }

        public ListenerSocketState(int receiveRateSampleSize) : this(DefaultBufferSize, DefaultPacketSize, receiveRateSampleSize) { }
        
        public ListenerSocketState(int bufferSize, int packetSize, int sampleSize)
        {
            _bufferSize = bufferSize;
            _buffer = new byte[bufferSize];
            _packet = new List<byte>(packetSize);
            _workSocket = null;
            _remoteIp = string.Empty;
            _receiveRate = new RateCalculator(sampleSize);
        }

        public Socket WorkSocket 
        {
            get { return _workSocket; }
            set
            {
                _workSocket = value;
                if (_workSocket != null && _workSocket.RemoteEndPoint != null)
                {
                    IPEndPoint remoteEndPoint = _workSocket.RemoteEndPoint as IPEndPoint;
                    if (remoteEndPoint != null && !string.IsNullOrWhiteSpace(remoteEndPoint.Address.ToString()))
                    {
                        _remoteIp = remoteEndPoint.Address.ToString();
                    }
                    else
                    {
                        _remoteIp = string.Empty;
                    }
                }
            }
        }

        public void AddRateSample(DateTime utcTime)
        {
            _receiveRate.AddSample(utcTime);
        }

        public double GetRatePerSecond(DateTime utcTime)
        {
            return _receiveRate.GetRatePerSecond(utcTime);
        }

        public byte[] SocketBuffer
        {
            get { return _buffer; }
        }

        public List<Byte> PacketBuffer
        {
            get { return _packet; }
        }

        public string RemoteIp
        {
            get { return _remoteIp; }
        }
        
        public void CopySocketBufferToPacketBuffer(int count)
        {
            for (int i = 0; i < count; i++)
                _packet.Add(_buffer[i]);
        }

        public int BufferSize { get { return _bufferSize; } }
    }
}
