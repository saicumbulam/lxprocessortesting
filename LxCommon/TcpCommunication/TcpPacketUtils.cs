﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Aws.Core.Utilities;
using LxCommon.Monitoring;

namespace LxCommon.TcpCommunication
{
    public enum DecodeResult { FoundPacket, NoPacket, Error };

    public static class TcpPacketUtils
    {
        private const ushort EventManagerOffset = TaskMonitor.TcpPacketUtils;

        public static short? ReadShortFromBufferAtOffset(List<byte> packageBytes, int offset)
        {
            byte[] bytes = ReadBytes(packageBytes, offset, 2);
            return (bytes != null ? BitConverter.ToInt16(bytes, 0) : (short?)null);
        }

        public static int? ReadUInt16FromBufferAtOffset(List<byte> packageBytes, int offset)
        {
            byte[] bytes = ReadBytes(packageBytes, offset, 2);
            return (bytes != null ? BitConverter.ToUInt16(bytes, 0) : (int?)null);
        }

        public static byte[] ReadBytes(List<byte> packageBytes, int offset, int dataTypeSize)
        {
            byte[] relevantBytes = null;

            if (packageBytes.Count > (offset + dataTypeSize - 1))
            {
                relevantBytes = new byte[dataTypeSize];
                packageBytes.CopyTo(offset, relevantBytes, 0, dataTypeSize);
            }

            return relevantBytes;
        }
        
        public static DecodeResult TryTcpDecode(List<byte> packetBuffer, string remoteIp, out PacketHeaderInfo phi)
        {
            DecodeResult result = DecodeResult.NoPacket;
            phi = null;
            int? len = ReadUInt16FromBufferAtOffset(packetBuffer, 0);
            if (packetBuffer.Count >= 2)
            {
                if (len.HasValue && len > 0 && len <= LtgConstants.MaxPacketSize)
                {
                    if (packetBuffer.Count >= len.Value + 4)
                    {
                        byte[] endMarker = ReadBytes(packetBuffer, 2 + len.Value, 2);
                        if (endMarker[0] == LtgConstants.TcpEndMarker[0] && endMarker[1] == LtgConstants.TcpEndMarker[1])
                        {
                            byte[] data = ReadBytes(packetBuffer, 2, len.Value);
                            phi = new PacketHeaderInfo();
                            if (RawPacketUtil.GetPacketHeaderInfo(data, phi))
                            {
                                packetBuffer.RemoveRange(0, 2 + len.Value + 2);
                                phi.RemoteIP = remoteIp;
                                result = DecodeResult.FoundPacket;
                            }
                            else
                            {
                                phi = null;
                                result = DecodeResult.Error;
                                EventManager.LogWarning(EventManagerOffset, "Error in tcp decode, unable to get packet header");
                            }
                        }
                        else
                        {
                            result = DecodeResult.Error;
                            EventManager.LogWarning(EventManagerOffset, "Error in tcp decode, unable to find end marker");
                        }
                    }
                }
                else
                {
                    result = DecodeResult.Error;
                    EventManager.LogWarning(EventManagerOffset, string.Format("Error in tcp decode, invalid length: {0}", 
                        (len.HasValue) ? len.Value.ToString(CultureInfo.InvariantCulture) : "null"));
                }
            }

            return result;
        }

        private const int SensorPacketHeaderLength = 4;
        private const int SensorMaxPacketLength = 100000;
        public static DecodeResult TryTcpSensorDecode(List<byte> packetBuffer, string remoteIp, out PacketHeaderInfo phi)
        {
            DecodeResult result = DecodeResult.NoPacket;
            phi = null;
            if (packetBuffer.Count >= SensorPacketHeaderLength)
            {
                int headerLength = RawPacketUtil.GetPacketHeaderLength(packetBuffer[0], packetBuffer[1], packetBuffer[2], packetBuffer[3]);
                if (headerLength > 0)
                {
                    if (packetBuffer.Count >= headerLength)
                    {
                        phi = new PacketHeaderInfo();
                        byte[] header = new byte[headerLength];
                        packetBuffer.CopyTo(0, header, 0, headerLength);
                        if (RawPacketUtil.GetPacketHeaderInfo(header, phi))
                        {
                            if (phi.TotalLength >= 0 && phi.TotalLength <= SensorMaxPacketLength)
                            {
                                if (packetBuffer.Count >= phi.TotalLength)
                                {
                                    //Console.WriteLine(phi.TotalLength);
                                    phi.RawData = new byte[phi.TotalLength];
                                    packetBuffer.CopyTo(0, phi.RawData, 0, phi.TotalLength);
                                    if (phi.MsgType == PacketType.MsgTypeGps)
                                    {
                                        RawPacketUtil.GetGpsData(phi);
                                    }
                                    packetBuffer.RemoveRange(0, phi.TotalLength);
                                    phi.RemoteIP = remoteIp;
                                    result = DecodeResult.FoundPacket;
                                }
                            }
                            else
                            {
                                phi = null;
                                result = DecodeResult.Error;
                            }
                        }
                        else
                        {
                            phi = null;
                            result = DecodeResult.Error;
                        }
                    }
                }
                else
                {
                    result = DecodeResult.Error;
                }
            }

            return result;
        }

        public static byte[] TcpEncodeWithInt32Length(PacketHeaderInfo packet)
        {
            if (packet == null || packet.RawData == null)
            {
                throw new ArgumentNullException("packet");
            }

            //4 bytes for length + payload + 2 bytes for end marker
            byte[] buffer = new byte[4 + packet.RawData.Length + 2];

            byte[] len = BitConverter.GetBytes(packet.RawData.Length);
            Array.Copy(len, 0, buffer, 0, 4);
            Array.Copy(packet.RawData, 0, buffer, 4, packet.RawData.Length);
            Array.Copy(LtgConstants.TcpEndMarker, 0, buffer, packet.RawData.Length + 4, 2);
            
            return buffer;
        }

        public static DecodeResult TryTcpDecodeWithInt32Length(List<byte> packetBuffer, string remoteIp, out PacketHeaderInfo phi)
        {
            DecodeResult result = DecodeResult.NoPacket;
            phi = null;
            if (packetBuffer.Count >= 4)
            {
                byte[] len = new byte[4];
                packetBuffer.CopyTo(0, len, 0, 4);
                int length = BitConverter.ToInt32(len, 0);
                
                if (length > 0 && length <= LtgConstants.MaxPacketSize)
                {
                    //4 bytes for length + payload + 2 bytes for end marker
                    int totalLength = 4 + length + 2;

                    if (packetBuffer.Count >= totalLength)
                    {
                        if (packetBuffer[totalLength - 2] == LtgConstants.TcpEndMarker[0] && packetBuffer[totalLength - 1] == LtgConstants.TcpEndMarker[1])
                        {
                            byte[] payload = new byte[length];
                            packetBuffer.CopyTo(4, payload, 0, length);

                            phi = new PacketHeaderInfo();
                            if (RawPacketUtil.GetPacketHeaderInfo(payload, phi))
                            {
                                packetBuffer.RemoveRange(0, totalLength);
                                phi.RemoteIP = remoteIp;
                                result = DecodeResult.FoundPacket;
                            }
                            else
                            {
                                phi = null;
                                result = DecodeResult.Error;
                                EventManager.LogWarning(EventManagerOffset, "Error in tcp decode, unable to get packet header");
                            }
                        }
                        else
                        {
                            result = DecodeResult.Error;
                            EventManager.LogWarning(EventManagerOffset, "Error in tcp decode, unable to find end marker");
                        }
                        
                    }
                }
                else
                {
                    result = DecodeResult.Error;
                    EventManager.LogWarning(EventManagerOffset, string.Format("Error in tcp decode, invalid length: {0}", length));
                }
            }

            return result;
        }
    }
}
