﻿using System;
using System.Threading;

namespace LxCommon.TcpCommunication
{
    public class RateCalculator
    {
        private int _sampleSize;
        private DateTime _startTime;

        private int[] _id;
        private int[] _sum;

        public RateCalculator(int sampleSize) : this(sampleSize, DateTime.UtcNow)
        {
        }

        public RateCalculator(int sampleSize, DateTime startTime)
        {
            _sampleSize = sampleSize;
            _startTime = startTime;

            _id = new int[sampleSize];

            for (int i = 0; i < sampleSize; i++)
                _id[i] = -1;

            _sum = new int[sampleSize];
        }

        public void AddSample(DateTime time)
        {
            TimeSpan ts = time - _startTime;

            int index = (int)ts.TotalSeconds % _sampleSize;
            int id = (int)ts.TotalSeconds / _sampleSize;

            
            //see if this slot is the same time period
            if (id == Interlocked.Exchange(ref _id[index], id))
            {
                Interlocked.Increment(ref _sum[index]);
            }
            else
            {
                Interlocked.Exchange(ref _sum[index], 1);
            }
        }

        public double GetRatePerSecond(DateTime time)
        {
            double rate = 0.0D;
            double sum = 0.0D;

            TimeSpan tspan = time - _startTime;

            int index = (int)tspan.TotalSeconds % _sampleSize;
            int id = (int)tspan.TotalSeconds / _sampleSize;
            int ts = (int)tspan.TotalSeconds;
            int size = ts >= _sampleSize ? _sampleSize : ts;
            
            for (int i = 0; i < _sampleSize; i++)
            {
                if (_id[i] >= 0)
                {
                    if (i != index && (id - _id[i]) <= 1)
                    {
                        sum = sum + _sum[i];
                    }
                    else if (i == index && (id - _id[i]) == 0)
                    {
                        double partial = tspan.TotalSeconds - ts;
                        if (partial > 0)
                        {
                            sum = sum + (_sum[i] * (1 / partial));
                        }
                    }
                }
            }

            if (size > 0)
                rate = sum / size;

            return rate;
        }

        public int GetTotalCount(DateTime time)
        {   
            int sum = 0;

            TimeSpan tspan = time - _startTime;

            int index = (int)tspan.TotalSeconds % _sampleSize;
            int id = (int)tspan.TotalSeconds / _sampleSize;
            
            for (int i = 0; i < _sampleSize; i++)
            {
                if (_id[i] >= 0)
                {
                    if (i != index && (id - _id[i]) <= 1)
                    {
                        sum = sum + _sum[i];
                    }
                    else if (i == index && (id - _id[i]) == 0)
                    {
                        sum = sum + _sum[i];
                    }
                }
            }

            return sum;
        }

        public void Reset()
        {
            for (int i = 0; i < _sampleSize; i++)
            {
                _id[i] = -1;
                _sum[i] = 0;
            }   
        }
    }
}
