﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using Aws.Core.Utilities;
using LxCommon.Monitoring;

namespace LxCommon.TcpCommunication
{
    public class Responder
    {
        private const ushort EventManagerOffset = TaskMonitor.Responder;
        private readonly TcpListener _tcpListener;
        private readonly int _maxPendingConnections;
        private readonly ConcurrentDictionary<IPEndPoint, SendWorker> _clients;
        private bool _isRunning;
        private readonly string _name;
        private readonly ManualResetEventSlim _stopEvent;
        private readonly RateCalculator _connectionRate;

        public Responder(string name, IPAddress ipAdd, int port, int maxPendingConnections, int connectionRateSampleSize)
        {
            _name = name;
            _maxPendingConnections = maxPendingConnections;
            _stopEvent = new ManualResetEventSlim(false);
            _isRunning = false;
            _clients = new ConcurrentDictionary<IPEndPoint, SendWorker>();
            _connectionRate = new RateCalculator(connectionRateSampleSize);
            
            IPEndPoint ipLocalEndPoint = new IPEndPoint(ipAdd, port);
            try
            {
                _tcpListener = new TcpListener(ipLocalEndPoint);
                EventManager.LogInfo(EventManagerOffset, string.Format("{0} created tcp listener at {1}:{2}", name, ipLocalEndPoint.Address, ipLocalEndPoint.Port));
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventManagerOffset + 1, "Unable to create TcpListener", ex);
            }
        }

        public void Open()
        {
            IPEndPoint endPoint = _tcpListener.LocalEndpoint as IPEndPoint;
            if (endPoint != null)
            {
                try
                {
                    _tcpListener.Start(_maxPendingConnections);
                    _isRunning = true;

                    EventManager.LogInfo(EventManagerOffset + 2, string.Format("{0} started listening at {1}:{2}", _name, endPoint.Address, endPoint.Port));

                    Listen();
                }
                catch (Exception ex)
                {
                    EventManager.LogError(EventManagerOffset + 3, string.Format("{0} was unable to start listening at {1}:{2}", _name, endPoint.Address, endPoint.Port), ex);
                }
            }
            
        }

        private async void Listen()
        {
            while (_isRunning)
            {
                try
                {
                    TcpClient tcpClient = await _tcpListener.AcceptTcpClientAsync();
                    IPEndPoint localEndPoint = tcpClient.Client.LocalEndPoint as IPEndPoint;
                    IPEndPoint remoteEndPoint = tcpClient.Client.RemoteEndPoint as IPEndPoint;
                    if (localEndPoint != null && remoteEndPoint != null)
                    {
                        EventManager.LogInfo(EventManagerOffset + 4, string.Format("{0} accepted connection on {1}:{2} from {3}:{4}", _name, localEndPoint.Address, localEndPoint.Port, remoteEndPoint.Address, remoteEndPoint.Port));

                        _connectionRate.AddSample(DateTime.UtcNow);

                        SendWorker client = new SendWorker(tcpClient, remoteEndPoint, _stopEvent);
                        
                        _clients.AddOrUpdate(remoteEndPoint, client,
                            (key, existing) =>
                            {
                                existing.Close();
                                return client;
                            });
                    }
                    
                }
                catch (Exception ex)
                {
                    EventManager.LogError(EventManagerOffset + 5, string.Format("{0} error accepting a connection", _name), ex);
                }
            }
        }

        public void AddPacket(byte[] packet)
        {
            List<IPEndPoint> keys = new List<IPEndPoint>(_clients.Keys);

            try
            {
                Parallel.ForEach(keys, (key) =>
                    {
                        SendWorker sendWorker;
                        if (_clients.TryGetValue(key, out sendWorker))
                        {
                            //this will fail when the connection has been closed
                            if (!sendWorker.TryAdd(packet))
                            {
                                sendWorker.Close();
                                _clients.TryRemove(key, out sendWorker);
                            }
                        }
                    });
            }
            catch (AggregateException aex)
            {
                aex.Handle((x) =>
                    {
                        EventManager.LogError(EventManagerOffset + 6, string.Format("{0} error adding a packet", _name), x);
                        return true;
                    });
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventManagerOffset + 7, string.Format("{0} error adding a packet", _name), ex);
            }
        }

        public void Close()
        {   
            _isRunning = false;
            _stopEvent.Set();

            if (_tcpListener != null)
            {
                _tcpListener.Stop();
            }
            List<IPEndPoint> keys = new List<IPEndPoint>(_clients.Keys);

            Parallel.ForEach(keys, (key) =>
                {
                    SendWorker sendWorker;
                    if (_clients.TryRemove(key, out sendWorker))
                    {
                        sendWorker.Close();
                    }
                });    
        }

        public double GetConnectionRatePerSecond(DateTime now)
        {
            return _connectionRate.GetRatePerSecond(now);
        }

        public string Name { get { return _name; } }

        public int Connections { get { return _clients.Count; } }
    }
}
