﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using Aws.Core.Utilities;

using LxCommon.Monitoring;

namespace LxCommon.TcpCommunication
{
    public class Listener
    {
        private const ushort EventManagerOffset = TaskMonitor.Listener;
        private readonly TcpListener _tcpListener;
        private readonly int _maxPendingConnections;
        private readonly Func<List<byte>, string, DecodeResult> _decodePacket;
        private ConcurrentDictionary<IPEndPoint, ListenerSocketState> _sockets;
        private readonly RateCalculator _receiveRate;
        private readonly RateCalculator _connectionRate;
        private readonly string _name;
        private readonly int _receiveRateSampleSize;
        private readonly int? _expectedConnections;
        private readonly Timer _idleCheckTimer;

        public Listener(string name, IPAddress ipAdd, int port, int maxPendingConnections, Func<List<byte>, string, DecodeResult> decodePacket, int receiveRateSampleSize,
            int connectionRateSampleSize, int idleCheckIntervalSeconds, int? expectedConnections)
        {
            _name = name;
            _maxPendingConnections = maxPendingConnections;
            _decodePacket = decodePacket;
            _sockets = new ConcurrentDictionary<IPEndPoint, ListenerSocketState>();
            _receiveRateSampleSize = receiveRateSampleSize;
            _receiveRate = new RateCalculator(receiveRateSampleSize);
            _connectionRate = new RateCalculator(connectionRateSampleSize);
            _expectedConnections = expectedConnections;
            _idleCheckTimer = new Timer(CloseIdleConnections, null, TimeSpan.FromSeconds(idleCheckIntervalSeconds), TimeSpan.FromSeconds(idleCheckIntervalSeconds));
            
            IPEndPoint ipLocalEndPoint = new IPEndPoint(ipAdd, port);
            try
            {
                _tcpListener = new TcpListener(ipLocalEndPoint);
                EventManager.LogInfo(EventManagerOffset + 0, string.Format("{0} created tcp listener at {1}:{2}", name, ipLocalEndPoint.Address, ipLocalEndPoint.Port));
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventManagerOffset + 1, "Unable to create TcpListener", ex);
            }
        }

        public void Open()
        {
            IPEndPoint endPoint = _tcpListener.LocalEndpoint as IPEndPoint;
            try
            {
                _tcpListener.Start(_maxPendingConnections);
            }
            catch (SocketException socketEx)
            {
                EventManager.LogError(EventManagerOffset + 2, string.Format("Unable to start tcp listener; Socket Error Code: {0}", socketEx.ErrorCode), socketEx);
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventManagerOffset + 3, "Unable to start tcp listener", ex);
            }
            

            if (endPoint != null)
                EventManager.LogInfo(EventManagerOffset + 4, string.Format("{0} started listening {1}:{2}", _name, endPoint.Address, endPoint.Port));

            BeginAccept(_tcpListener);
        }

        private void BeginAccept(TcpListener tcpListener)
        {
            try
            {
                tcpListener.BeginAcceptTcpClient(AcceptCallback, _tcpListener);
            }
            catch (SocketException socketEx)
            {
                EventManager.LogError(EventManagerOffset + 1, string.Format("Socket Exception on BeginAcceptTcpClient Socket Error Code: {0}", socketEx.ErrorCode), socketEx);
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventManagerOffset + 1, "Error on BeginAcceptTcpClient", ex);
            }
        }

        private void AcceptCallback(IAsyncResult ar)
        {
            // Get the listener that handles the client request.
            TcpListener listener = ar.AsyncState as TcpListener;

            _connectionRate.AddSample(DateTime.UtcNow);

            // End the operation
            if (listener != null)
            {
                Socket socket = null;
                try
                {
                    socket = listener.EndAcceptSocket(ar);
                }
                catch (SocketException socketEx)
                {
                    EventManager.LogError(EventManagerOffset + 1, string.Format("Socket Exception in AcceptCallback; Socket Error Code: {0}", socketEx.ErrorCode), socketEx);
                }
                catch (Exception ex)
                {
                    EventManager.LogError(EventManagerOffset + 1, "Error on BeginAcceptTcpClient", ex);
                }

                BeginAccept(listener);

                ProcessSocket(socket);
            }
        }

        private void ProcessSocket(Socket socket)
        {
            if (socket != null)
            {
                // Create the state object.
                ListenerSocketState state = new ListenerSocketState(_receiveRateSampleSize) { WorkSocket = socket };
                IPEndPoint remoteEndPoint = null;
                try
                {
                    IPEndPoint localEndPoint = socket.LocalEndPoint as IPEndPoint;
                    remoteEndPoint = socket.RemoteEndPoint as IPEndPoint;
                    if (localEndPoint != null && remoteEndPoint != null)
                    {
                        //remember the socket so we can clean it up later, also close any connection from duplicate remote end points
                        _sockets.AddOrUpdate(remoteEndPoint, state,
                            (key, existing) =>
                            {
                                existing.WorkSocket.Close();
                                return state;
                            });

                        // Begin receiving the data from the remote device.
                        socket.BeginReceive(state.SocketBuffer, 0, state.BufferSize, SocketFlags.None, ReceiveCallback, state);

                        EventManager.LogInfo(EventManagerOffset + 5, string.Format("{0} accepted connection on {1}:{2} from {3}:{4}", _name, localEndPoint.Address, localEndPoint.Port, remoteEndPoint.Address, remoteEndPoint.Port));

                    }
                }
                catch (SocketException socketEx)
                {
                    EventManager.LogError(EventManagerOffset + 6, string.Format("Socket Error in AcceptCallback Socket Error Code: {0}", socketEx.ErrorCode), socketEx);
                    CleanupSocket(remoteEndPoint, state);

                }
                catch (Exception ex)
                {
                    EventManager.LogError(EventManagerOffset + 7, "Error in AcceptCallback", ex);
                    CleanupSocket(remoteEndPoint, state);
                }
            }
        }

        private void ReceiveCallback(IAsyncResult ar)
        {
            ListenerSocketState state = ar.AsyncState as ListenerSocketState;
            Socket socket = null;
            IPEndPoint remoteEndPoint = null;
            try
            {
                // Retrieve the state object and the client socket from the asynchronous state object.
                if (state != null)
                {
                    socket = state.WorkSocket;
                    // Read data from the remote device.
                    if (socket != null)
                    {
                        remoteEndPoint = socket.RemoteEndPoint as IPEndPoint;
                        int bytesRead = socket.EndReceive(ar);
                        if (bytesRead > 0)
                        {
                            // There might be more data, so store the data received so far.
                            state.CopySocketBufferToPacketBuffer(bytesRead);

                            DecodeResult result = DecodeResult.FoundPacket;
                            while (state.PacketBuffer.Count > 0 && result == DecodeResult.FoundPacket)
                            {
                                result = _decodePacket(state.PacketBuffer, state.RemoteIp);
                                if (result == DecodeResult.FoundPacket)
                                {
                                    _receiveRate.AddSample(DateTime.UtcNow);
                                    state.AddRateSample(DateTime.UtcNow);
                                }  
                            }

                            if (result == DecodeResult.Error)
                            {
                                
                                EventManager.LogError(EventManagerOffset + 12, string.Format("Malformed packet recieved from {0}:{1}, closing socket",
                                    (remoteEndPoint != null) ? remoteEndPoint.Address.ToString() : "?", (remoteEndPoint != null) ? remoteEndPoint.Port.ToString(CultureInfo.InvariantCulture) : "?"));
                                CleanupSocket(remoteEndPoint, state);
                            }
                            else
                            {
                                //  Get the rest of the data.
                                socket.BeginReceive(state.SocketBuffer, 0, state.BufferSize, SocketFlags.None, ReceiveCallback, state);
                            }
                        }
                        else
                        {
                            // if the remote host shuts down the connection and all available data has been received, EndReceive method 
                            // will complete immediately and return zero bytes, we need to keep reading till no more packets are found

                            DecodeResult result = DecodeResult.FoundPacket;
                            while (state.PacketBuffer.Count > 0 && result == DecodeResult.FoundPacket)
                            {
                                result = _decodePacket(state.PacketBuffer, state.RemoteIp);
                                if (result == DecodeResult.FoundPacket)
                                {
                                    _receiveRate.AddSample(DateTime.UtcNow);
                                    state.AddRateSample(DateTime.UtcNow);
                                }  
                            }

                            CleanupSocket(remoteEndPoint, state);
                        }
                    }
                }
            }
            catch (SocketException socketEx)
            {
                EventManager.LogError(EventManagerOffset + 8, string.Format("Socket Error in ReceiveCallback Socket Error Code: {0}", socketEx.ErrorCode), socketEx);
                if (socket != null)
                {
                    CleanupSocket(remoteEndPoint, state);
                }

            }
            catch (Exception ex)
            {
                EventManager.LogError(EventManagerOffset + 9, "Error in ReceiveCallback", ex);
                if (socket != null)
                {
                    CleanupSocket(remoteEndPoint, state);
                }
            }
        }

        private void CloseIdleConnections(object state)
        {
            if (!_expectedConnections.HasValue || _expectedConnections.Value < ActiveConnections)
            {
                DateTime utcNow = DateTime.UtcNow;
                try
                {
                    List<KeyValuePair<IPEndPoint, ListenerSocketState>> kvps = new List<KeyValuePair<IPEndPoint, ListenerSocketState>>(_sockets);
                    foreach (KeyValuePair<IPEndPoint, ListenerSocketState> kvp in kvps)
                    {
                        if (kvp.Value.GetRatePerSecond(utcNow) <= 0.0)
                        {
                            CleanupSocket(kvp.Key, kvp.Value);
                            EventManager.LogInfo(EventManagerOffset + 12, string.Format("{0} cleaned up idle connection from {1}:{2}", _name, kvp.Key.Address, kvp.Key.Port));
                        }
                    }
                }
                catch(Exception ex)
                {
                    EventManager.LogError(EventManagerOffset + 12, "Error on cleaning up idle connections.", ex);
                }
            }
        }

        public void Close()
        {
            if (_idleCheckTimer != null)
            {
                _idleCheckTimer.Dispose();
            }

            if (_tcpListener != null)
            {
                _tcpListener.Stop();
            }

            if (_sockets != null)
            {
                List<KeyValuePair<IPEndPoint, ListenerSocketState>> kvps = new List<KeyValuePair<IPEndPoint, ListenerSocketState>>(_sockets);
                foreach (KeyValuePair<IPEndPoint, ListenerSocketState> kvp in kvps)
                {
                    CleanupSocket(kvp.Key, kvp.Value);
                }
            }
        }

        private void CleanupSocket(IPEndPoint ipep, ListenerSocketState state)
        {
            if (state.WorkSocket != null)
            {
                try
                {
                    if (state.WorkSocket.Connected)
                    {
                        state.WorkSocket.Shutdown(SocketShutdown.Both);
                    }
                    state.WorkSocket.Close();
                }
                catch (SocketException socketEx)
                {
                    EventManager.LogError(EventManagerOffset + 10, string.Format("Socket Exception on Shutdown Socket Error Code: {0}", socketEx.ErrorCode), socketEx);
                }
                catch (Exception ex)
                {
                    EventManager.LogError(EventManagerOffset + 11, "Error on Shutdown", ex);
                }
            }

            ListenerSocketState trash;
            if (ipep != null) _sockets.TryRemove(ipep, out trash);
        }

        public double GetIncomingRatePerSecond(DateTime now)
        {
            return _receiveRate.GetRatePerSecond(now);
        }

        public double GetConnectionRatePerSecond(DateTime now)
        {
            return _connectionRate.GetRatePerSecond(now);
        }

        public int Connections
        {
            get { return _sockets.Count; }
        }

        public int ActiveConnections
        {
            get 
            {
                int count = 0;
                if (_sockets != null)
                {
                    foreach (KeyValuePair<IPEndPoint, ListenerSocketState> keyValuePair in _sockets)
                    {
                        if (keyValuePair.Value.WorkSocket.Connected)
                        {
                            count++;
                        }
                    }
                }
                return count;
            }
        }

        public string Name { get { return _name; } }
    }
}
