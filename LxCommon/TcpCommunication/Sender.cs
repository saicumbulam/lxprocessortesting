﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net;
using System.Threading;

namespace LxCommon.TcpCommunication
{
    public interface ISendWorker
    {
        ManualResetEventSlim StopEvent { set; }

        void Add(PacketHeaderInfo phi);

        void Add(byte[] packet);

        void Close();

        bool IsActive { get; }
    }

    public class Sender
    {
        private ConcurrentDictionary<IPEndPoint, ISendWorker> _clients;
        private readonly string _name;
        private readonly ManualResetEventSlim _stopEvent;

        public Sender(string name, ConcurrentDictionary<IPEndPoint, ISendWorker> clients)
        {
            _name = name;
            _stopEvent = new ManualResetEventSlim(false);

            foreach (ISendWorker sw in clients.Values)
            {
                sw.StopEvent = _stopEvent;
            }

            _clients = clients;

        }

        public void AddPacket(PacketHeaderInfo phi)
        {
            if (_clients != null)
            {
                foreach (KeyValuePair<IPEndPoint, ISendWorker> kvp in _clients)
                {
                    kvp.Value.Add(phi);
                }
            }
        }

        public void AddPacket(byte[] packet)
        {
            if (_clients != null)
            {
                foreach (KeyValuePair<IPEndPoint, ISendWorker> kvp in _clients)
                {
                    kvp.Value.Add(packet);
                }
            }
        }

        public void Close()
        {
            _stopEvent.Set();

            foreach (KeyValuePair<IPEndPoint, ISendWorker> keyValuePair in _clients)
            {
                keyValuePair.Value.Close();
            }    
        }

        public string Name { get { return _name; } }

        public int Connections { get { return _clients.Count; } }

        public ManualResetEventSlim StopEvent { get { return _stopEvent; } }
    
        public int ActiveConnections 
        { 
            get 
            {
                int count = 0;
                foreach (KeyValuePair<IPEndPoint, ISendWorker> keyValuePair in _clients)
                {
                    if (keyValuePair.Value.IsActive) count++;
                }
                return count;
            } 
        }
    }
}
