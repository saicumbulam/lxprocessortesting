﻿using System;
using System.Collections.Concurrent;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using Aws.Core.Utilities;
using LxCommon.Config;
using LxCommon.Monitoring;

namespace LxCommon.TcpCommunication
{
    public class SendWorker : ISendWorker
    {
        private const ushort EventManagerOffset = TaskMonitor.SendWorker;

        private TcpClient _tcpClient;
        private ConcurrentQueue<byte[]> _sendQueue;
        private int _processLock;
        private ManualResetEventSlim _stopEvent;
        protected IPEndPoint _remoteEndPoint;
        private bool _connectClient;
        private RateCalculator _rateCalculator;
        private const int RateCalculatorSampleSize = 5;

        private static readonly int MaxQueueSize = AppConfig.MaxSendWorkerQueueSize;

        public SendWorker(TcpClient tcpClient, IPEndPoint remoteEndPoint, ManualResetEventSlim stopEvent) 
            : this(stopEvent)
        {
            _tcpClient = tcpClient;
            _remoteEndPoint = remoteEndPoint;
            _connectClient = false;
        }
    
        public SendWorker(IPEndPoint remoteEndPoint)
            : this()
        {
            _connectClient = true;
            _remoteEndPoint = remoteEndPoint;
        }

        private SendWorker(ManualResetEventSlim stopEvent)
            : this()
        {
            _stopEvent = stopEvent;
        }

        private SendWorker()
        {
            _sendQueue = new ConcurrentQueue<byte[]>();
            _rateCalculator = new RateCalculator(RateCalculatorSampleSize);
        }

        public bool TryAdd(byte[] packet)
        {
            bool isConnected = false;

            if (_tcpClient != null && _tcpClient.Connected)
            {
                isConnected = true;
                Add(packet);
            }
            else
            {
                if (_tcpClient != null && _tcpClient.Client != null && _tcpClient.Client.RemoteEndPoint != null)
                {
                    IPEndPoint remoteEndPoint = _tcpClient.Client.RemoteEndPoint as IPEndPoint;
                    if (remoteEndPoint != null)
                    {
                        EventManager.LogWarning(EventManagerOffset, string.Format("Cannot send packet to {0}:{1} because the connection has been closed", 
                            remoteEndPoint.Address, remoteEndPoint.Port));
                    }
                }
            }

            return isConnected;
        }

        public virtual void Add(PacketHeaderInfo phi)
        {
            if (phi != null)
            {
                byte[] packet = RawPacketUtil.GetStreamingBuffer(phi);
                Add(packet);
            }
        }

        public void Add(byte[] packet)
        {
            if (_sendQueue.Count > MaxQueueSize)
            {
                double maxSize = MaxQueueSize - (MaxQueueSize * 0.1);

                EventManager.LogWarning(EventManagerOffset + 1, string.Format("{0}:{1} send queue greater than max size, dumping packets from queue", _remoteEndPoint.Address, _remoteEndPoint.Port));

                byte[] trash;

                while (_sendQueue.Count > maxSize)
                {
                    _sendQueue.TryDequeue(out trash);
                }
            }

            _sendQueue.Enqueue(packet);
            // This controls the task that manages the queue, make sure we only launch one task 
            if (0 == Interlocked.CompareExchange(ref _processLock, 1, 0))
            {
                ProcessSendQueue();
            }
        }

        private async void ProcessSendQueue()
        {
            byte[] packet;
            try
            {
                while (_sendQueue.TryDequeue(out packet))
                {
                    if (IsValidPacketToSend(packet))
                    {
                        await SendPacket(packet);
                    }

                    if (!_connectClient && (_tcpClient == null || !_tcpClient.Connected))
                        break;

                    if (_stopEvent.Wait(0))
                        break;
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventManagerOffset + 2, string.Format("Error processing {0}:{1} send queue", _remoteEndPoint.Address, _remoteEndPoint.Port), ex);
            }
            finally
            {
                Interlocked.Decrement(ref _processLock);
            }
        }

        protected virtual bool IsValidPacketToSend(byte[] packet)
        {
            return true;
        }

        protected virtual async Task SendPacket(byte[] packet)
        {
            if (_connectClient)
            {
                while ((_tcpClient == null || !_tcpClient.Connected) && !_stopEvent.Wait(0))
                {
                    try
                    {
                        if (_tcpClient == null)
                            _tcpClient = new TcpClient(AddressFamily.InterNetwork);

                        await _tcpClient.ConnectAsync(_remoteEndPoint.Address, _remoteEndPoint.Port);
                        EventManager.LogInfo(EventManagerOffset + 3, string.Format("Successfully connected to {0}:{1}", _remoteEndPoint.Address, _remoteEndPoint.Port));
                    }
                    catch (SocketException socketEx)
                    {
                        EventManager.LogError(EventManagerOffset + 4, string.Format("Error connecting to {0}:{1}; Socket Error Code: {2}", _remoteEndPoint.Address, _remoteEndPoint.Port, socketEx.ErrorCode), socketEx);
                        Close();
                    }
                    catch (Exception ex)
                    {
                        EventManager.LogError(EventManagerOffset + 5, "Error in AcceptCallback", ex);
                        Close();
                    }
                }
            }

            if (_tcpClient != null && _tcpClient.Connected)
            {
                try
                {
                    NetworkStream ns = _tcpClient.GetStream();
                    await ns.WriteAsync(packet, 0, packet.Length);
                    _rateCalculator.AddSample(DateTime.UtcNow);
                }
                catch (Exception ex)
                {
                    EventManager.LogError(EventManagerOffset + 6, "Error writing to network stream", ex);
                    Close();
                }
            }
        }

   
        public void Close()
        {
            if (_tcpClient != null)
            {
                try
                {
                    if (_tcpClient.Connected)
                    {
                        NetworkStream ns = _tcpClient.GetStream();
                        ns.Close();
                    }
                }
                catch (Exception ex)
                {
                    EventManager.LogError(EventManagerOffset + 7, "Error closing network stream", ex);
                }

                _tcpClient.Close();

                _tcpClient = null;
            }
        }

        public double GetOutgoingRatePerSecond(DateTime now)
        {
            return _rateCalculator.GetRatePerSecond(now);
        }

        public bool IsActive
        {
            get { return _tcpClient != null && _tcpClient.Connected; } 
        }

        public ManualResetEventSlim StopEvent
        {
            set { _stopEvent = value; }
        }
    }
}
