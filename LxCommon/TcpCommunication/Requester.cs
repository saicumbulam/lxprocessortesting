﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using Aws.Core.Utilities;
using LxCommon.Monitoring;

namespace LxCommon.TcpCommunication
{
    public class Requester
    {
        private string _name;
        private IPEndPoint _remoteEndPoint;
        private Func<List<byte>, string, DecodeResult> _decodePacket;
        private RateCalculator _receiveRate;
        private RateCalculator _connectionRate;
        private RateCalculator _errorRate;
        private TcpClient _tcpClient;
        private ManualResetEventSlim _stopEvent;
        private byte[] _connectionPacket;

        private const ushort EventManagerOffset = TaskMonitor.Requester;

        public Requester(string name, IPEndPoint remoteEndPoint, Func<List<byte>, string, DecodeResult> decodePacket, int receiveRateSampleSize, 
            int connectionRateSampleSize, ManualResetEventSlim stopEvent)
        {
            _name = name ?? string.Empty;
            _remoteEndPoint = remoteEndPoint;
            _decodePacket = decodePacket;
            _receiveRate = new RateCalculator(receiveRateSampleSize);
            _connectionRate = new RateCalculator(connectionRateSampleSize);
            _errorRate = null;
            _stopEvent = stopEvent;
        }

        public Requester(string name, IPEndPoint remoteEndPoint, byte[] connectionPacket, Func<List<byte>, string, DecodeResult> decodePacket,
            int receiveRateSampleSize, int connectionRateSampleSize, int errorRateSampleSize, ManualResetEventSlim stopEvent)
            : this(name, remoteEndPoint, decodePacket, receiveRateSampleSize, connectionRateSampleSize, stopEvent)
        {
            _connectionPacket = connectionPacket;
            _errorRate = new RateCalculator(errorRateSampleSize);
        }

        public async void Open()
        {
            while ((_tcpClient == null || !_tcpClient.Connected) && !_stopEvent.Wait(0))
            {
                try
                {
                    if (_tcpClient != null)
                    {
                        _tcpClient.Close();
                    }

                    _tcpClient = new TcpClient(AddressFamily.InterNetwork);
                    await _tcpClient.ConnectAsync(_remoteEndPoint.Address, _remoteEndPoint.Port);
                    EventManager.LogInfo(EventManagerOffset + 3, string.Format("{0} successfully connected to {1}:{2}", _name, _remoteEndPoint.Address, _remoteEndPoint.Port));
                    if (_connectionPacket != null)
                    {
                        await SendConnectionPacket();
                    }
                    if (_tcpClient != null && _tcpClient.Connected)
                    {
                        _connectionRate.AddSample(DateTime.UtcNow);
                    }
                }
                catch (SocketException socketEx)
                {
                    EventManager.LogError(EventManagerOffset + 4, string.Format("{0} error connecting to {1}:{2}, socket error code: {3}", _name, _remoteEndPoint.Address, 
                        _remoteEndPoint.Port, socketEx.ErrorCode), socketEx);
                    Close();
                    if (_errorRate != null) _errorRate.AddSample(DateTime.UtcNow);
                    
                }
                catch (Exception ex)
                {
                    EventManager.LogError(EventManagerOffset + 5, string.Format("{0} error in open", _name), ex);
                    Close();
                    if (_errorRate != null) _errorRate.AddSample(DateTime.UtcNow);
                }
            }


            if (_tcpClient != null)
            {
                BeginReceive(_tcpClient.Client);
            }
        }

        private async Task SendConnectionPacket()
        {
            if (_tcpClient.Connected)
            {
                try
                {
                    NetworkStream stream = _tcpClient.GetStream();
                    await stream.WriteAsync(_connectionPacket, 0, _connectionPacket.Length);
                }
                catch (SocketException socketEx)
                {
                    EventManager.LogError(EventManagerOffset + 4, string.Format("{0} error sending connection packet to {1}:{2}, socket error code: {3}", _name, _remoteEndPoint.Address,
                        _remoteEndPoint.Port, socketEx.ErrorCode), socketEx);
                    Close();
                    if (_errorRate != null) _errorRate.AddSample(DateTime.UtcNow);
                }
                catch (Exception ex)
                {
                    EventManager.LogError(EventManagerOffset + 5, string.Format("{0} error sending connection packet", _name), ex);
                    Close();
                    if (_errorRate != null) _errorRate.AddSample(DateTime.UtcNow);
                }
            }
        }

        private void BeginReceive(Socket socket)
        {
            try
            {
                // Create the state object.
                ListenerSocketState state = new ListenerSocketState { WorkSocket = socket };

                // Begin receiving the data from the remote device.
                socket.BeginReceive(state.SocketBuffer, 0, state.BufferSize, SocketFlags.None, ReceiveCallback, state);
            }
            catch (SocketException socketEx)
            {
                EventManager.LogError(EventManagerOffset + 6, string.Format("{0} socket error in begin receive, socket error code: {1}", _name, socketEx.ErrorCode), socketEx);
                CleanupSocket(socket);
                if (_errorRate != null) _errorRate.AddSample(DateTime.UtcNow);
                Open();

            }
            catch (Exception ex)
            {
                EventManager.LogError(EventManagerOffset + 7, string.Format("{0} error in begin receive", _name), ex);
                CleanupSocket(socket);
                if (_errorRate != null) _errorRate.AddSample(DateTime.UtcNow);
                Open();
            }
            
        }

        private void ReceiveCallback(IAsyncResult ar)
        {
            ListenerSocketState state = ar.AsyncState as ListenerSocketState;
            Socket socket = null;
            IPEndPoint remoteEndPoint = null;
            try
            {
                // Retrieve the state object and the client socket from the asynchronous state object.
                if (state != null)
                {
                    socket = state.WorkSocket;
                    // Read data from the remote device.
                    if (socket != null)
                    {
                        remoteEndPoint = socket.RemoteEndPoint as IPEndPoint;
                        int bytesRead = socket.EndReceive(ar);
                        if (bytesRead > 0)
                        {
                            // There might be more data, so store the data received so far.
                            state.CopySocketBufferToPacketBuffer(bytesRead);

                            DecodeResult result = DecodeResult.FoundPacket;
                            while (state.PacketBuffer.Count > 0 && result == DecodeResult.FoundPacket)
                            {
                                result = _decodePacket(state.PacketBuffer, state.RemoteIp);
                                if (result == DecodeResult.FoundPacket)
                                {
                                    _receiveRate.AddSample(DateTime.UtcNow);
                                }
                            }

                            if (result == DecodeResult.Error)
                            {

                                EventManager.LogError(EventManagerOffset + 12, string.Format("{0} malformed packet recieved from {1}:{2}, closing socket", _name,
                                    (remoteEndPoint != null) ? remoteEndPoint.Address.ToString() : "?", (remoteEndPoint != null) ? remoteEndPoint.Port.ToString(CultureInfo.InvariantCulture) : "?"));
                                CleanupSocket(socket);
                                if (_errorRate != null) _errorRate.AddSample(DateTime.UtcNow);
                                Open();
                            }
                            else
                            {
                                //  Get the rest of the data.
                                socket.BeginReceive(state.SocketBuffer, 0, state.BufferSize, SocketFlags.None, ReceiveCallback, state);
                            }
                        }
                        else
                        {
                            // if the remote host shuts down the connection and all available data has been received, EndReceive method 
                            // will complete immediately and return zero bytes, we need to keep reading the buffer till no more packets are found

                            DecodeResult result = DecodeResult.FoundPacket;
                            while (state.PacketBuffer.Count > 0 && result == DecodeResult.FoundPacket)
                            {
                                result = _decodePacket(state.PacketBuffer, state.RemoteIp);
                                if (result == DecodeResult.FoundPacket)
                                {
                                    _receiveRate.AddSample(DateTime.UtcNow);
                                }
                            }

                            CleanupSocket(socket);
                            Open();
                        }
                    }
                }
            }
            catch (SocketException socketEx)
            {
                EventManager.LogError(EventManagerOffset + 8, string.Format("{0} socket error in receive callback, socket error code: {1}", _name, socketEx.ErrorCode), socketEx);
                if (socket != null)
                {
                    CleanupSocket(socket);
                    if (_errorRate != null) _errorRate.AddSample(DateTime.UtcNow);
                    Open();
                }

            }
            catch (Exception ex)
            {
                EventManager.LogError(EventManagerOffset + 9, string.Format("{0} error in receive callback", _name), ex);
                if (socket != null)
                {
                    CleanupSocket(socket);
                    if (_errorRate != null) _errorRate.AddSample(DateTime.UtcNow);
                    Open();
                }
            }
        }


        private void CleanupSocket(Socket socket)
        {
            if (socket != null)
            {
                try
                {
                    if (socket.Connected)
                    {
                        socket.Shutdown(SocketShutdown.Both);
                    }
                    socket.Close();
                }
                catch (SocketException socketEx)
                {
                    EventManager.LogError(EventManagerOffset + 10, string.Format("{0} socket exception on cleanup, socket error code: {1}", _name, socketEx.ErrorCode), socketEx);
                }
                catch (Exception ex)
                {
                    EventManager.LogError(EventManagerOffset + 11, string.Format("{0} error on Shutdown", _name), ex);
                }
            }
        }

        public void Close()
        {
            if (_tcpClient != null)
            {
                try
                {
                    if (_tcpClient.Connected)
                    {
                        NetworkStream ns = _tcpClient.GetStream();
                        ns.Close();
                    }
                }
                catch (Exception ex)
                {
                    EventManager.LogError(EventManagerOffset + 7, string.Format("{0} error closing network stream", _name), ex);
                }

                _tcpClient.Close();

                _tcpClient = null;
            }
        }

        public double GetIncomingRatePerSecond(DateTime now)
        {
            return _receiveRate.GetRatePerSecond(now);
        }

        public double GetConnectionRatePerSecond(DateTime now)
        {
            return _connectionRate.GetRatePerSecond(now);
        }

        public double GetErrorRatePerSecond(DateTime now)
        {
            double rate = 0.0;
            if (_errorRate != null)
            {
                rate = _errorRate.GetRatePerSecond(now);
            }
            return rate;
        }

        public int GetErrorCount(DateTime now)
        {
            int errors = 0;
            if (_errorRate != null)
            {
                errors = _errorRate.GetTotalCount(now);
            }
            return errors;
        }

        public string Address
        {
            get { return _remoteEndPoint.Address.ToString(); }
        }

        public string Name
        {
            get { return _name; }
        }
    }
}
