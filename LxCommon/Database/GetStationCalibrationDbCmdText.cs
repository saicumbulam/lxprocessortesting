﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using Aws.Core.Data;
using Aws.Core.Utilities;
using LxCommon.Config;

namespace LxCommon.Database
{
    internal class GetStationCalibrationDbCmdText : DbCommandText
    {
        private static readonly int DbCommandTimeout = AppConfig.DbCommandTimeout;
        private const ushort EventId = 100;
        private const int InitalCapacity = 1000;

        public GetStationCalibrationDbCmdText()
        {
            InitSP();
        }

        public override string BuildCmdText()
        {
            //return "dbo.LtgGetStationCalibrationData_pr";
            return "LtgGetStationCalibrationData_pr";
        }

        public List<StationDecibelParameter> Execute()
        {
            List<StationDecibelParameter> stationList = null;

            SqlConnection conn = null;
            DbCommand dbCmd = null;
            SqlDataReader dr = null;
            try
            {
                conn = DbConnectionFactory.Build(ConfigurationManager.ConnectionStrings["Lightning"].ConnectionString);
                dbCmd = new DbCommand(conn, this);
                dbCmd.Command.CommandTimeout = DbCommandTimeout;
                dr = dbCmd.ExecuteReader();
                StationDecibelParameter s;
                if (dr.HasRows)
                {
                    stationList = new List<StationDecibelParameter>(InitalCapacity);
                    while (dr.Read())
                    {
                        s = ParseCalibrationRow(dr);
                        if (s != null)
                            stationList.Add(s);
                    }
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventId, "Unable to get station calibration data from db.", ex);
            }
            finally
            {
                if (null != dr) dr.Close();
                if (null != dbCmd) dbCmd.Close();
                if (null != conn) conn.Close();
            }

            return stationList;
        }

        private StationDecibelParameter ParseCalibrationRow(SqlDataReader dr)
        {
            StationDecibelParameter sdp = new StationDecibelParameter
            {
                StationId = GetDataString(dr, "Station_ID"),
                Decibel = GetDataInt(dr, "dB"),
                CalibrationDate = GetDataDateTime(dr, "CalibrationDate"),
                EffectiveLengthInMeter = Convert.ToSingle(GetDataDecimal(dr, "EffectiveLengthInMeter")),
                InternalNoiseInVolts = Convert.ToSingle(GetDataDecimal(dr, "InternalNoiseInVolts")),
                ExternalNoiseInVolts = Convert.ToSingle(GetDataDecimal(dr, "ExternalNoiseInVolts")),
                DcOffsetInVolts = Convert.ToSingle(GetDataDecimal(dr, "DcOffsetInVolts")),
                Valid = true
            };

            return sdp;
        }


    }
}
