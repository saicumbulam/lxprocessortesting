﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using Aws.Core.Data;
using Aws.Core.Utilities;
using LxCommon.Config;

namespace LxCommon.Database
{
    internal class GetStationDataDbCmdText : DbCommandText
    {
        private static readonly int DbCommandTimeout = AppConfig.DbCommandTimeout;
        private const ushort EventId = 100;
        private const int InitalCapacity = 1000;

        public GetStationDataDbCmdText()
        {
            InitSP();
        }

        public override string BuildCmdText()
        {
            //return "dbo.LtgGetStationData_pr";
            return "LtgGetStationData_pr";
        }

        public List<StationData> Execute()
        {
            List<StationData> stationList = null;

            SqlConnection conn = null;
            DbCommand dbCmd = null;
            SqlDataReader dr = null;
            try
            {
                conn = DbConnectionFactory.Build(ConfigurationManager.ConnectionStrings["Lightning"].ConnectionString);
                dbCmd = new DbCommand(conn, this);
                dbCmd.Command.CommandTimeout = DbCommandTimeout;
                dr = dbCmd.ExecuteReader();
                StationData s;
                StationData prev = null;
                if (dr.HasRows)
                {
                    stationList = new List<StationData>(InitalCapacity);
                    
                    while (dr.Read())
                    {
                        s = ParseStationRow(dr);
                        if (s != null)
                        {
                            if (prev != null && prev.StationID == s.StationID && !string.IsNullOrWhiteSpace(s.NetworkType))
                            {
                                prev.Networks.Add(s.NetworkType);
                            }
                            else
                            {
                                if (!string.IsNullOrWhiteSpace(s.NetworkType))
                                {
                                    s.Networks.Add(s.NetworkType);
                                }
                                stationList.Add(s);
                                prev = s;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventId, "Unable to get station data from db.", ex);
            }
            finally
            {
                if (null != dr) dr.Close();
                if (null != dbCmd) dbCmd.Close();
                if (null != conn) conn.Close();
            }

            return stationList;
        }

        private StationData ParseStationRow(SqlDataReader dr)
        {
            string id = GetDataString(dr, "Station_ID");
            double lat = Convert.ToDouble(GetDataDecimal(dr, "Latitude"));
            double lon = Convert.ToDouble(GetDataDecimal(dr, "Longitude"));
            double alt = Convert.ToDouble(GetDataDecimal(dr, "Altitude"));
            short qc = GetDataShort(dr, "QCFlag");
            string networkType = GetDataString(dr, "LtgNetworkID", null);
            if (networkType != null)
            {
                networkType = networkType.ToUpper().Trim();
            }
            
            StationData s = new StationData(id, lat, lon, alt, qc);

            s.NetworkType = networkType;


            //if (AppConfig.ProcessorType == ProcessorType.ProcessorTypeLF)
            //    s.UpdateCartesianWithRoundEarth();
            //else
            //    s.UpdateCartesian();
            

            return s;
        }

        
    }
}
