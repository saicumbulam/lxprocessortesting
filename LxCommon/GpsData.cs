namespace LxCommon
{
    public class GpsData
    {
        private string _sensorId;
        private double _lat;
        private double _lon;
        private double _height;  //GPS height in cm,  -100,000..+1,800,000
        private string _version;  // antenna version
        private int secondsSinceEpic = 0;
        private int visibleSatellites = 0;
        private int trackedSatellites = 0;
        private byte[] _rawdata = null;

        public bool IsValid()
        {
            if (_lat == 0.0 && _lon == 0.0 && _height == 0)
                return false;
            if (secondsSinceEpic == 0)
                return false;
            if (visibleSatellites < LtgConstants.MinVisibleSatellites)
                return false;
            return true;
        }

        public int VisibleSatellites
        {
            get { return visibleSatellites; }
            set { visibleSatellites = value; }
        }

        public int TrackedSatellites
        {
            get { return trackedSatellites; }
            set { trackedSatellites = value; }
        }


        public int SecondsSinceEpic
        {
            get { return secondsSinceEpic; }
            set { secondsSinceEpic = value; }
        }

        public double Latitude
        {
            get
            {
                return _lat;
            }
            set
            {
                _lat = value;
            }
        }
        public double Longtitude
        {
            get
            {
                return _lon;
            }
            set
            {
                _lon = value;
            }
        }
        public double Height
        {
            get
            {
                return _height;
            }
            set
            {
                _height = value;
            }
        }
        public string SensorId
        {
            get
            {
                return _sensorId;
            }
            set
            {
                _sensorId = value;
            }
        }

        public string SensorVersion
        {
            get
            {
                return _version;
            }
            set
            {
                _version = value;
            }
        }

        public byte[] Rawdata
        {
            get
            {
                return _rawdata;
            }
            set
            {
                _rawdata = value;
            }
        }
    }
}
