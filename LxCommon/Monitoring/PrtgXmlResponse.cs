﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace LxCommon.Monitoring
{
    [XmlRoot("prtg")]
    public class PrtgXmlResponse
    {
        private List<PrtgXmlResult> _results = null;
     
        public PrtgXmlResponse()
        {
            IncludeErrors = true;
            IncludeWarnings = true;
        }
     
        [XmlElement("error")]
        public string Error { get; set; }

        [XmlElement("text")]
        public string Text { get; set; }

        [XmlIgnore]
        public bool IncludeErrors { get; set; }

        [XmlIgnore]
        public bool IncludeWarnings { get; set; }

        [XmlElement("result")]
        public List<PrtgXmlResult> Results
        {
            get { return _results; }
            set { _results = value; }
        }
     
        #region Public Methods
        public void SetError(string message)
        {
            if (IncludeErrors)
            {
                Error = "1";
                Text = message;
                Results = null;
            }
        }

        public void AddResult<T>(string channelName, T value, UnitType unit, bool isFloat, string customUnit, string warningMsg = null)
        {
            AddResult(new PrtgXmlResult() { 
                Channel = channelName, 
                Value = value.ToString(), 
                Unit = unit.ToString(), 
                IsFloat = (isFloat) ? "1" : "0",
                CustomUnit = customUnit,
                Warning = IncludeWarnings ? warningMsg : null
            });
        }

        public void AddResult(PrtgXmlResult result)
        {
            if (_results == null)
            {
                _results = new List<PrtgXmlResult>();
            }

            _results.Add(result);
        }
        #endregion
    }

    // these values come from https://mercury.earthnetworks.com/api.htm?tabid=7
    public enum UnitType
    {
        BytesBandwidth,
        BytesMemory,
        BytesDisk,
        Temperature,
        Percent,
        TimeResponse,
        TimeSeconds,
        Custom,
        Count,
        CPU,
        BytesFile,
        SpeedDisk,
        SpeedNet,
        TimeHours
    };  
}
