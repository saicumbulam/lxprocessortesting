using System;
using System.Net;
using System.Net.Sockets;

namespace LxCommon.Monitoring
{
	/// <summary>
	/// Summary description for StatusTracking.
	/// </summary>
	public class StatusTracking
	{
		//event to fire message to main thread
		public event UpdateMsgDelegate  SyncProcEvent;

        private string _appName;
		private string _strStatsManIP;
		private int _nStatsManPort;
		private int _nStatsFlag;
		
		private const int TRANCKING_INTERVAL_SECONDS = 300;
		private DateTime _dtLastTrack;

		public StatusTracking(string appName, string statsManIP, int statsManPort, int statsFlag)
		{
            _appName = appName;
			_strStatsManIP = statsManIP;
			_nStatsManPort = statsManPort;
			_nStatsFlag = statsFlag; //255

			_dtLastTrack = DateTime.Now.AddSeconds(-TRANCKING_INTERVAL_SECONDS);
		}

		public void UdpStatsMan()
		{
			if(ShouldSendTracking()==false)
				return;

			if(_strStatsManIP=="")
				return;

			string hostName = _appName = " on " + Dns.GetHostName();
			UdpClient client = new UdpClient();

			try
			{
				//Send DataGram		
				byte[] sendData = new byte[255];
				sendData[0] = Convert.ToByte(_nStatsFlag);
				sendData[1] = Convert.ToByte(hostName.Length) ;
				int i;
				for(i=0; i<hostName.Length; i++)
					sendData[i+2] = (byte)hostName[i];
				//ThreadEvent(MsgType.Info, "Rending tracking info...");
				//Send to Server 
				client.Send(sendData, i+2, _strStatsManIP, _nStatsManPort);

				if(SyncProcEvent!=null)
					SyncProcEvent(this, new ThreadEventArgs(MsgType.Verb, "\tSent UDP tracking info to " + _strStatsManIP));

			}
			catch(Exception ex)
			{
				if(SyncProcEvent!=null)
					SyncProcEvent(this, new ThreadEventArgs(MsgType.Err, "Failed to send tracking info to StatsMan, " + ex.Message));	
			}
			finally
			{
				//Close Connection
				client.Close();
				_dtLastTrack = DateTime.Now;
			}
		}

		private bool ShouldSendTracking()
		{
			if( _dtLastTrack <= DateTime.Now.AddSeconds(-TRANCKING_INTERVAL_SECONDS) )
				return true;

			return false;

		}

	}
}
