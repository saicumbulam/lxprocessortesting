﻿using System.Xml.Serialization;

namespace LxCommon.Monitoring
{
    [XmlRoot("result")]
    public class PrtgXmlResult
    {     
        [XmlElement("channel")]
        public string Channel { get; set; }

        [XmlElement("value")]
        public string Value { get; set; }

        [XmlElement("float")]
        public string IsFloat { get; set; }

        [XmlElement("unit")]
        public string Unit { get; set; }

        [XmlElement("warning")]
        public string Warning { get; set; }

        [XmlElement("customUnit")]
        public string CustomUnit { get; set; }
        
    }
}
