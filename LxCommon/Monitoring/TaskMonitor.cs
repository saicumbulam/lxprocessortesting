using System.Collections;

namespace LxCommon.Monitoring
{
    public class TaskMonitor
    {
        //task IDs that need to be monitored
        public const int TID_TCP_TO_MANAGER = 13; // Send lightning flash data to Lightning manager
        public const int TID_UDP_PUSH_LTG_DATA = 15;  // Error in pushing lightning UDP data
        public const int TID_WWLLN_FLASH_UDP_RATE = 21; //Error in receiving too few wwlln flash data
        
        //Common Task Monitor ID
        public const ushort TidNone = 0;//"Not-monitored task";
        public const ushort TidServiceStarted = 65000;
        public const ushort TidServiceStopped = 65001;
        public const ushort TidServiceStartupFail = 65002;
        public const ushort TidServiceStoppedFail = 65003;

        public const ushort TidUpstreamDataUnavailable = 11000;
        public const ushort TidMonitorPageUnavailable = 11001;
        public const ushort TidTcpSenderFail = 11002; // Error in sending TCP data to another server: Processor or Manager
        public const ushort TidServiceConfigLoadFail = 11004;
        public const ushort TidUdpSocketFail = 11006; // sensor no longer sent UDPs
        public const ushort TidTcpSocketFail = 11007; 
        public const ushort TidArchiveLtgFlashFail = 11008; //Error in archiving the flash data
        public const ushort TidOutboundDataQueueFull = 11009;
        public const ushort TidQuerySensorDBFail = 11015;
        public const ushort TidUpdateGlobalStationFail = 11016;
        public const ushort TidReadCalibrationvalueFail = 11017;
        public const ushort TidBuildStationDistanceFail = 11018;
        public const ushort TidECVPageFail = 11019;

        //common library
        public const ushort DfClient = 11600;
        public const ushort ParallelQueueProcessor = 11700;
        public const ushort ParallelTaskProcessor = 11710;
        public const ushort FileExport = 11720;
        public const ushort Listener = 11730;
        public const ushort Responder = 11740;
        public const ushort Sender = 11750;
        public const ushort SendWorker = 11760;
        public const ushort FileLogger = 11770;
        public const ushort Requester = 11780;
        public const ushort FileHelper = 11790;
        public const ushort TcpPacketUtils = 11800;

        //Receiver Specific Task Monitor ID
        public const ushort TidUpdateGpsDbFail = 11001;  // Update GSP database when a new GPS data from sensor is received
        public const ushort TidArchiveLtgRawDataFail = 11003; // Error in archiving the raw lightning data from sensors
        public const ushort TidUpdateLogDbFail = 11005; // Update the LOG file DB
        public const ushort TidUpdateStatsDbFail = 11006; // Update the LOG file DB
        public const ushort TidGpsDataQueueFull = 11010;
        public const ushort TidLogDataQueueFull = 11011;
        public const ushort TidArchiveDataQueueFull = 11012;
        public const ushort TidRawDataArchiverDirAccessFail = 11013;
        public const ushort TidRawDataArchiverFileProcessFail = 11014;
        public const ushort FileWriterProcessor = 11100;
        public const ushort FileArchiver = 11110;
        public const ushort WwllnTogaSenderWorker = 11130;
        public const ushort IncomingPacketProcessor = 11140;
        public const ushort TlnSendWorker = 11150;
        

        //WaveformQuery 
        public const ushort ReadCacheFile = 11100;
        public const ushort WriteCacheFile = 11101;
        public const ushort ReadStationsDb = 11102;
        public const ushort LoadCacheFile = 11103;
        public const ushort SaveCacheFile = 11104;
        public const ushort LoadWaveform = 11105;

        //LxPortal.Api
        public const ushort ApiStationUpdate = 11200;
        public const ushort ApiStationGet = 11201;
        public const ushort ApiStationListGet = 11202;
        public const ushort ApiStationAdd = 11203;
        public const ushort ApiStationDelete = 11204;
        public const ushort ApiCurrentSensorStatusGet = 11205;
        public const ushort ApiSensorPacketHistoryGet = 11206;
        public const ushort ApiCurrentSensorDiagnosticGet = 11207;
        public const ushort ApiSensorDetectionGet = 11208;
        public const ushort ApiFlashGet = 11209;
        public const ushort ApiFlashGetRadius = 11210;
        public const ushort ApiCellAlertGet = 11211;
        public const ushort ApiCellTrackGet = 11212;
        public const ushort ApiStormGet = 11213;
        public const ushort ApiNwsAlertGet = 11214;
        public const ushort ApiNwsAlertSql = 11215;
        public const ushort ApiSystemEcv = 11216;
        public const ushort ApiStickyFailover = 11217;
        
        //Calibrator Specific Task Monitor ID

        //Processor Specific Task Monitor ID
        //public const ushort ProcRestartThread = 11400;
        //public const ushort ProcStartThread = 11401;
        //public const ushort ProcStartMonitorThread = 11402;
        //public const ushort ProcMonitorthreadShutdown = 11403;
        //public const ushort ProcStartFlashSender = 11404;
        //public const ushort ProcStartLocatorThread = 11405;
        //public const ushort ProcStartGrouperThread = 11406;
        //public const ushort ProcTCPData  = 11407;
        //public const ushort ProcPreprocessTimeOut = 11408;
        //public const int TID_THREAD_RESTART = 11409; // Thread is restarted in Processor, information
        
        //Data Manager Specific Task Monitor ID
        public const ushort TidSaveLxFlashToDBFail = 11301;
        public const ushort TidSaveLxFlashPortionToDBFail = 11302;
        public const ushort TidSaveLxFlashProcessFail = 11303;
        public const ushort TidSaveLxFlashStatFail = 11304;
        public const ushort TidClosestSensorLookupFail = 11306;
        public const ushort TidDataManagerInternalError = 11307;
        public const ushort TidCacheAddFail = 11308;
        public const ushort TidCacheRemoveFail = 11309;
        public const ushort TidDataManagerShutdownSequenceIssue = 11310;
        public const ushort TidRejectedFlashLogFail = 11311;
        public const ushort TidBufferFull = 11312;
        public const ushort TidConvertPacketFail = 11313;
        public const ushort TidFindQuadtreenodeFail = 11314;
        public const ushort TidRejectedGlobalData = 11315;
        public const ushort TidDecodePacketFail = 11316;
        public const ushort ManagerFlashPortionGrouperProcessor = 11320;
        public const ushort ManagerEtlDatabaseGateway = 11330;
        public const ushort ManagerArchiveProcessor = 11340;
        public const ushort ManagerRejectedFlashLogger = 11350;


        //Data Feed Specific Task Monitor ID
        public const ushort DfLogEventFail = 11500;
        public const ushort DfTcpConnectFail = 11501;
        public const ushort DfSendDatatoTcp = 11502;
        public const ushort DfLoadPartnerIdFailed = 11503;
        public const ushort DfDataControllerFailedStartup = 11504;
        public const ushort DfConnServerManStartup = 11505;
        public const ushort DfInternalError = 11506;
        public const ushort DfInternalConnFailed = 11507;
        public const ushort DfSendResponsePackFail = 11508;
        public const ushort DfGetIntConfigFail = 11509;
        public const ushort DfConnectFailtoLxDm = 11510;
        public const ushort DfFailedTcpListnerStartUp = 11511;
        public const ushort DfFailedAsyncTcpConn = 11512;
        public const ushort DfFailedAccepSocketStartUp = 11513;
        public const ushort DfFailedReceiveAsyncMsg = 11514;
        public const ushort DfQueMaxSizeReached = 11515;
        public const ushort DfGetDataDatetime = 11516;
        public const ushort DfGetDataDouble = 11517;
        public const ushort DfGetDataFloat = 11518;
        public const ushort DfGetDataInt = 11519;
        public const ushort DfGetDataString = 11520;
        public const ushort DfDebugLogToolFailedInit = 11521;
        public const ushort DfLxDataFeedCrashing = 11522;
        public const ushort DfFailedNetworkTypeConn = 11523;
        public const ushort DfFailedAcceptSocketSetKeepAlives = 11524;
        public const ushort DfIncomingPacketProcessor = 11530;
        public const ushort DfFlashFileWriter = 11540;

        public const ushort PerfCsvWriter = 11800;

        //Cell Tracker Specific Task Monitor ID

        private Hashtable _tasks = new Hashtable();
        
        
        public void AddTask(ushort taskId, int reportErrInSec)
        {
            _tasks.Add(taskId, new TaskStatus(taskId, reportErrInSec));
        }


        public void UpdateStatus(ThreadEventArgs args)
        {
            if (_tasks.ContainsKey(args._taskId) == false)
                return;

            TaskStatus ts = (TaskStatus)_tasks[args._taskId];
            ts.UpdateStatus(args);

        }

        public bool CheckStatusForTasks()
        {
            bool hasFail = false;
            foreach (TaskStatus ts in _tasks.Values)
            {
                if (ts.CheckStatusAndLogEvent() == TaskState.Err)
                {
                    //also write to log file
                    //SyncProcEvent(this, new ThreadEventArgs(MsgType.Err, "Error, " + ts.LastErrMsg));

                    hasFail = true;
                }
            }

            return hasFail;
        }

    }
}
