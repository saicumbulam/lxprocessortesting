﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace LxCommon.Monitoring
{

    public class PrtgResponseBuilder
    {
        private PrtgXmlResponse _response;
        private bool _includeWarnings, _includeErrors;
        
        public PrtgResponseBuilder(bool includeErrors = true, bool includeWarnings = true)
        {
            
            _includeErrors = includeErrors;
            _includeWarnings = includeWarnings;
            _response = new PrtgXmlResponse()
            {
                IncludeErrors = _includeErrors,
                IncludeWarnings = _includeWarnings
            };
        }

        public void AddSample(string channelName, double value, UnitType unit, string customUnit = null, string warningMsg = null, string errorMsg = null)
        {
            _response.AddResult(channelName, value, unit, true, customUnit, warningMsg);
        }

        public void AddSample(string channelName, int value, UnitType unit, string customUnit = null, string warningMsg = null, string errorMsg = null)
        {
            _response.AddResult(channelName, value, unit, false, customUnit, warningMsg);
        }

        public void AddSample(string channelName, DateTime value, UnitType unit, string customUnit = null, string warningMsg = null, string errorMsg = null)
        {
            _response.AddResult(channelName, value, unit, false, customUnit, warningMsg);
        }

        public string Build()
        {
            return Serialize(_response);
        }
        
        private static string Serialize(PrtgXmlResponse result)
        {
            using (StringWriter stringWriter = new StringWriter())
            {
                XmlWriterSettings settings = new XmlWriterSettings
                {
                    OmitXmlDeclaration = true,
                    Indent = true
                };

                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                ns.Add(String.Empty, String.Empty);

                using (XmlWriter writer = XmlWriter.Create(stringWriter, settings))
                {
                    XmlSerializer xmlSerializer = new XmlSerializer(result.GetType());
                    xmlSerializer.Serialize(writer, result, ns);
                }

                return stringWriter.ToString();
            }
        }

       
    }
}
