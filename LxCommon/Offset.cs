using System;
using System.Collections.Generic;

namespace LxCommon
{
    public class Offset : IComparable<Offset>
    {
        private double _value;
        private double _distanceToFlash; // Distance to the flash from the station

        // waveforms used for classification
        public object OriginalShortPortion { get; set; }


        public object Clone()
        {
            return MemberwiseClone();
        }

        public Offset()
        {
            WaveFormforOffset = null;
            StationId = string.Empty;
            _value = 0;
            _distanceToFlash = -1;
        }

        public Offset(string stationId, double value) : this()
        {
            StationId = stationId;
            _value = value;
        }

        public static int CompareOffsetByResidualError(Offset x, Offset y)
        {
            return x.ResidualError.CompareTo(y.ResidualError);
        }

        public static int CompareOffsetByDistanceToFlash(Offset x, Offset y)
        {
            int distCompare = x._distanceToFlash.CompareTo(y._distanceToFlash);
            if (distCompare == 0)
                return x.Value.CompareTo(y.Value);
            return distCompare;
        }

        public double ResidualError { get; set; }

        public object WaveFormforOffset { get; set; }

        public string StationId { get; set; }

        public double Value
        {
            get { return _value; }
            set { _value = value; }
        }

        public int InSolutionCount { get; set; }

        public double X { get; set; }

        public double Y { get; set; }

        public double Z { get; set; }

        public double T { get; set; }

        public double DistanceToFlash
        {
            get { return _distanceToFlash; }
            set { _distanceToFlash = value; }
        }

        public int CompareTo(Offset other)
        {
            return _value.CompareTo(other.Value);
        }

        public override string ToString()
        {
            return StationId + ":" + Value + ",";
        }

        public short AbsPeakAmplitude
        {
            get; set;
        }
    }

    public class OffsetCompareDistance : IComparer<Offset>
    {
        public int Compare(Offset x, Offset y)
        {
            return x.DistanceToFlash.CompareTo(y.DistanceToFlash);
        }
    }
}
