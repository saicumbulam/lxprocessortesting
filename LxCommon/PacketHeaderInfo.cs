using System;

namespace LxCommon
{
    public class PacketHeaderInfo
    {
        public PacketMagicNumber magicNum = PacketMagicNumber.MagicUnkow;
        private PacketType _msgType = PacketType.MsgUnknow;
        private string _sensorId = "";
        private SensorVersion _majorVer = SensorVersion.ID_VERSION_UNKNOW;
        private SensorVersion _minorVer = SensorVersion.ID_VERSION_UNKNOW;

        private int _totalLength = -1; // the packet size

        // Sensor 9 information
        private int _packetSecondsFrom1970 = 0;
        private int _nPpsTickNumber = 0;  // not used
        private int _nOffsetInNanoseconds = 0;  
        private byte _attenuation = 0; // for 9.1 and up

        // HF data information
        private int _nHFSclkTicksPpsToFirstSample = 0;
        private int _nHFSclkTicksBetweenSamples = 0;
        private int _nHFSclkTicksInThisSec = 0;
        private int _nHFThreshold = 0;
        private int _nHFDCOffset = 0;
        private int _nHFSampleNum = 0;

        // LF data information
        private int _nLFSclkTicksPpsToFirstSample = 0;
        private int _nLFSclkTicksBetweenSamples = 0;
        private int _nLFThreshold = 0;
        private int _nLFDCOffset = 0;
        private int _nLFSampleNum = 0;

        // Spectrum data information
        private int _nSpectraSclkTicksBetweenSamples = 0;
        private int _nSpectraSampleNum = 0;

        private byte[] _rawData = null; // hold the raw packet
        private string remoteIP = "";  // IP address where this packet was sent
        private string remoteMAC = ""; // not used
        private bool _bValidPacket = false;

        private object otherObject = null;  // Hold GpsData or other information 


        // MISC
        public string errorMessage = ""; // Debug purpose


        public PacketHeaderInfo()
        {

        }

        public static bool IsValidSensorID(string stationID)
        {
            bool valid = true;
            //char c;
            try
            {
                if (stationID.Length <= 0)
                    return false;
                foreach (char c in stationID)
                {
                    if (!char.IsLetterOrDigit(c))
                        return false;
                }
            }
            catch
            {
                valid = false;
            }

            return valid;
        }

        #region Properties

        public int HFThreshold
        {
            get { return _nHFThreshold; }
            set { _nHFThreshold = value; }
        }

        public int HFDCOffset
        {
            get { return _nHFDCOffset; }
            set { _nHFDCOffset = value; }
        }

        public int HFSampleNum
        {
            get { return _nHFSampleNum; }
            set { _nHFSampleNum = value; }
        }


        public int LFSclkTicksPpsToFirstSample
        {
            get { return _nLFSclkTicksPpsToFirstSample; }
            set { _nLFSclkTicksPpsToFirstSample = value; }
        }

        public int LFSclkTicksBetweenSamples
        {
            get { return _nLFSclkTicksBetweenSamples; }
            set { _nLFSclkTicksBetweenSamples = value; }
        }

        public int LFThreshold
        {
            get { return _nLFThreshold; }
            set { _nLFThreshold = value; }
        }

        public int LFDCOffset
        {
            get { return _nLFDCOffset; }
            set { _nLFDCOffset = value; }
        }

        public int LFSampleNum
        {
            get { return _nLFSampleNum; }
            set { _nLFSampleNum = value; }
        }



        public int SpectraSclkTicksBetweenSamples
        {
            get { return _nSpectraSclkTicksBetweenSamples; }
            set { _nSpectraSclkTicksBetweenSamples = value; }
        }

        public int SpectraSampleNum
        {
            get { return _nSpectraSampleNum; }
            set { _nSpectraSampleNum = value; }
        }


        public string RemoteMAC
        {
            get { return remoteMAC; }
            set { remoteMAC = value; }
        }

        public string RemoteIP
        {
            get { return remoteIP; }
            set { remoteIP = value; }
        }

        public int TotalLength
        {
            get
            {
                return _totalLength;
            }
            set
            {
                _totalLength = value;
            }
        }
        
        public int PacketTimeInSeconds
        {
            get
            {
                return _packetSecondsFrom1970;
            }
            set
            {
                _packetSecondsFrom1970 = value;
                TimeStamp = LtgTimeUtils.GetUtcDateTimeFrom1970Seconds(value);
            }
        }

        public DateTime TimeStamp { get; private set; }

        public byte SensorAttenuation
        {
            get { return _attenuation; }
            set { _attenuation = value; }
        }
        public SensorVersion MajorVersion
        {
            get
            {
                return _majorVer;
            }
            set
            {
                _majorVer = value;
            }
        }
        public SensorVersion MinorVersion
        {
            get
            {
                return _minorVer;
            }
            set
            {
                _minorVer = value;
            }
        }
        public PacketType MsgType
        {
            get
            {
                return _msgType;
            }
            set
            {
                _msgType = value;
            }
        }
        public string SensorId
        {
            get
            {
                return _sensorId;
            }
            set
            {
                _sensorId = value;
            }
        }

        public int PpsTickNumber
        {
            get
            {
                return _nPpsTickNumber;
            }
            set
            {
                _nPpsTickNumber = value;
            }
        }
        public int OffsetInNanoseconds
        {
            get
            {
                return _nOffsetInNanoseconds;
            }
            set
            {
                _nOffsetInNanoseconds = value;
            }
        }
        public int HFSclkTicksPpsToFirstSample
        {
            get
            {
                return _nHFSclkTicksPpsToFirstSample;
            }
            set
            {
                _nHFSclkTicksPpsToFirstSample = value;
            }
        }
        public int HFSclkTicksBetweenSamples
        {
            get
            {
                return _nHFSclkTicksBetweenSamples;
            }
            set
            {
                _nHFSclkTicksBetweenSamples = value;
            }
        }
        public int HFSclkTicksInThisSec
        {
            get
            {
                return _nHFSclkTicksInThisSec;
            }
            set
            {
                _nHFSclkTicksInThisSec = value;
            }
        }

        public byte[] RawData
        {
            get
            {
                return _rawData;
            }
            set
            {
                _rawData = value;
                CheckData();
            }
        }


        public object OtherObject
        {
            get { return otherObject; }
            set { otherObject = value; }
        }

        public int? FirmwareTime { get; set; }

        public bool IsValid()
        {
            return _bValidPacket;
        }

        private void CheckData()
        {
            _bValidPacket = false;
            errorMessage = "";
            if (_msgType == PacketType.MsgUnknow)
            {
                errorMessage = SensorId + ": Invalid Message Type: ";
                return;
            }

            if (_msgType == PacketType.MsgTypeWWLLNLtg)
            {
                if (!PacketHeaderInfo.IsValidSensorID(SensorId))
                {
                    errorMessage = SensorId + ": Invalid Sensor ID";
                    return;
                }
                _bValidPacket = true;
                return;
            }

            if (_msgType == PacketType.MsgTypeLtg || _msgType == PacketType.MsgTypeGps || _msgType == PacketType.MsgTypeLog)
            {

                if (!PacketHeaderInfo.IsValidSensorID(SensorId))
                {
                    errorMessage = SensorId + ": Invalid Sensor ID";
                    return;
                }


                if (this.TotalLength != _rawData.Length)
                {
                    errorMessage = SensorId + ":Expected data length=" + TotalLength.ToString() + ", Actual data length=" + _rawData.Length.ToString();
                    return;
                }
                if (_msgType == PacketType.MsgTypeLtg && !RawPacketUtil.IsValidTime(_packetSecondsFrom1970))
                {
                    errorMessage = SensorId + ":Bad time for waveform packet, the total second=" + _packetSecondsFrom1970.ToString();
                    return;
                }

                if (_majorVer < SensorVersion.ID_VERSION_SENSOR_8 || _majorVer > SensorVersion.ID_VERSION_SENSOR_10_MAJOR)
                {
                    errorMessage = SensorId + ": Bad version, major=" + _majorVer.ToString() + "minor=" + _minorVer.ToString();
                    return;
                }
            }
            _bValidPacket = true;
        }
        #endregion
    }

}
