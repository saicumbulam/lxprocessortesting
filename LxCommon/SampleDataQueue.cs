using System;
using System.Collections;

// SampleData represents the data sent from the sensor in tick/amplitue format
namespace LxCommon
{
    public class SecondOfSample
    {
        private string stationID;
        private SensorVersion majorVersion;
        private SensorVersion minorVersion;
        private int time; // seconds since 1970
        private short nanoSeconds;
        private int nHfSamples;
        private int nLfSamples;
        private int nSpectraSamples;



        private ArrayList hfsamples = null; // HF sampes list of Sample
        private ArrayList lfSamples = null; // Low frequency samples
        private ArrayList spectraSamples = null; // Spectra Samples

        private byte attenuation = 0;

        private int _nPpsTickNumber; // only for Sensor 9: not used

        public int _nHFSclkTicksPpsToFirstSample;
        public int _nHFSclkTicksBetweenSamples;
        public int _nHFSclkTicksInThisSec;


        private int _nLFSclkTicksPpsToFirstSample;
        private int _nLFSclkTicksBetweenSamples;

        public int ppsOffsetInNanoseconds;

        // TEMP: should be disabled later
        //public bool __reversePolarity = false;

        public SecondOfSample()
        {
            stationID = "";
            nHfSamples = 0;
            nanoSeconds = 0;
            hfsamples = null;
        }

        public void SetSamples(PacketHeaderInfo header, ArrayList HFSample, ArrayList LFSample, ArrayList SpectraSample)
        {
            try
            {
                this.StationID = header.SensorId;
                this.Time = header.PacketTimeInSeconds;
                this.MajorVersion = header.MajorVersion;
                this.MinorVersion = header.MinorVersion;

                if (this.MajorVersion == SensorVersion.ID_VERSION_SENSOR_8)
                {
                    hfsamples = HFSample; // (Sample[])HFSample.ToArray(typeof(Sample));
                    nHfSamples = HFSample.Count;
                }
                else if (this.MajorVersion == SensorVersion.ID_VERSION_SENSOR_9_MAJOR)
                {
                    if (this.MinorVersion == SensorVersion.ID_VERSION_SENSOR_9_MINOR0)
                    {
                        SetSecondSampeFor9_0(header, HFSample);

                    }
                    else if (this.MinorVersion == SensorVersion.ID_VERSION_SENSOR_9_MINOR1)
                    {
                        SetSecondSampeFor9_1(header, HFSample);
                    }
                    else if (this.MinorVersion == SensorVersion.ID_VERSION_SENSOR_9_MINOR2)
                    {
                        SetSecondSampeFor9_2(header, HFSample, LFSample);
                    }
                    else if (this.MinorVersion >= SensorVersion.ID_VERSION_SENSOR_9_MINOR3)
                    {
                        SetSecondSampeFor9_3(header, HFSample, LFSample, SpectraSample);
                    }
                }
                else if (this.MajorVersion == SensorVersion.ID_VERSION_SENSOR_10_MAJOR)
                {
                    if (this.MinorVersion <= SensorVersion.ID_VERSION_SENSOR_10_MINOR0)
                    {
                        SetSecondSampeFor9_3(header, HFSample, LFSample, SpectraSample);
                    }
                }

            }
            catch (Exception ex)
            {
                //AppLogger.LogMessage(ex.ToString());
            }
        }

        private void SetSecondSampeFor9_0(PacketHeaderInfo header, ArrayList alSample)
        {
            hfsamples = alSample; // (Sample[])alSample.ToArray(typeof(Sample));
            nHfSamples = alSample.Count;
            this._nPpsTickNumber = header.PpsTickNumber;
            this.ppsOffsetInNanoseconds = header.OffsetInNanoseconds;
            this._nHFSclkTicksPpsToFirstSample = header.HFSclkTicksPpsToFirstSample;
            this._nHFSclkTicksBetweenSamples = header.HFSclkTicksBetweenSamples;
            this._nHFSclkTicksInThisSec = header.HFSclkTicksInThisSec;
        }

        private void SetSecondSampeFor9_1(PacketHeaderInfo header, ArrayList alSample)
        {
            hfsamples = alSample; // (Sample[])alSample.ToArray(typeof(Sample));
            nHfSamples = alSample.Count;
            this._nPpsTickNumber = header.PpsTickNumber;
            this.ppsOffsetInNanoseconds = header.OffsetInNanoseconds;
            this._nHFSclkTicksPpsToFirstSample = header.HFSclkTicksPpsToFirstSample;
            this._nHFSclkTicksBetweenSamples = header.HFSclkTicksBetweenSamples;
            this._nHFSclkTicksInThisSec = header.HFSclkTicksInThisSec;
            this.attenuation = header.SensorAttenuation;
        }

        private void SetSecondSampeFor9_2(PacketHeaderInfo header, ArrayList alSample, ArrayList alfSample)
        {
            hfsamples = alSample; // (Sample[])alSample.ToArray(typeof(Sample));
            lfSamples = alfSample; // (Sample[])alfSample.ToArray(typeof(Sample));
            nHfSamples = alSample.Count;
            this._nPpsTickNumber = header.PpsTickNumber;
            this.ppsOffsetInNanoseconds = header.OffsetInNanoseconds;
            this._nHFSclkTicksPpsToFirstSample = header.HFSclkTicksPpsToFirstSample;
            this._nHFSclkTicksBetweenSamples = header.HFSclkTicksBetweenSamples;
            this._nHFSclkTicksInThisSec = header.HFSclkTicksInThisSec;
            this.attenuation = header.SensorAttenuation;
        }

        private void SetSecondSampeFor9_3(PacketHeaderInfo header, ArrayList HFlSamples, ArrayList LFSamples, ArrayList SpectraSamples)
        {
            hfsamples = HFlSamples; // (Sample[])HFlSamples.ToArray(typeof(Sample));
            lfSamples = LFSamples; // (Sample[])LFSamples.ToArray(typeof(Sample));
            spectraSamples = SpectraSamples; // (Sample[])SpectraSamples.ToArray(typeof(Sample));

            //nHfSamples = header.HFSampleNum;
            nHfSamples = HFlSamples.Count;
            this._nPpsTickNumber = header.PpsTickNumber;
            this.ppsOffsetInNanoseconds = header.OffsetInNanoseconds;
            this._nHFSclkTicksPpsToFirstSample = header.HFSclkTicksPpsToFirstSample;
            this._nHFSclkTicksBetweenSamples = header.HFSclkTicksBetweenSamples;
            this._nHFSclkTicksInThisSec = header.HFSclkTicksInThisSec;
            this.attenuation = header.SensorAttenuation;

            this._nLFSclkTicksPpsToFirstSample = header.LFSclkTicksPpsToFirstSample;
            this._nLFSclkTicksBetweenSamples = header.LFSclkTicksBetweenSamples;
            nLfSamples = LFSamples.Count;

            nSpectraSamples = SpectraSamples.Count;
        }



        #region MyRegion
        public string StationID
        {
            get
            {
                return stationID;
            }
            set
            {
                stationID = value;
            }
        }

        public SensorVersion MajorVersion
        {
            get
            {
                return majorVersion;
            }
            set
            {
                majorVersion = value;
            }

        }

        public SensorVersion MinorVersion
        {
            get
            {
                return minorVersion;
            }
            set
            {
                minorVersion = value;
            }

        }

        public int Time
        {
            get
            {
                return time;
            }
            set
            {
                time = value;
            }

        }

        public DateTime UTCDateTime
        {
            get
            {
                return LtgTimeUtils.GetUtcDateTimeFrom1970Seconds(time);
            }
        }
        public int NumberOfHfSamples
        {
            get
            {
                return nHfSamples;
            }
            set
            {
                nHfSamples = value;
            }
        }
        public int NumberOfLFSamples
        {
            get { return nLfSamples; }
            set { nLfSamples = value; }
        }

        public int NumberOfSpectraSamples
        {
            get { return nSpectraSamples; }
            set { nSpectraSamples = value; }
        }
        public ArrayList HfSamples
        {
            get
            {
                return hfsamples;
            }
            set
            {
                hfsamples = value;
            }
        }

        public ArrayList LfSamples
        {
            get { return lfSamples; }
            set { lfSamples = value; }
        }

        public byte Attenuation
        {
            get { return attenuation; }
            set { attenuation = value; }
        }
        public ArrayList SpectraSamples
        {
            get { return spectraSamples; }
            set { spectraSamples = value; }
        }

        public int LFSclkTicksPpsToFirstSample
        {
            get { return _nLFSclkTicksPpsToFirstSample; }
            set { _nLFSclkTicksPpsToFirstSample = value; }
        }

        public int LFSclkTicksBetweenSamples
        {
            get { return _nLFSclkTicksBetweenSamples; }
            set { _nLFSclkTicksBetweenSamples = value; }
        }

        #endregion
    }
}
