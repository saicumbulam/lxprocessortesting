using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Aws.Core.Utilities;

using LxCommon.Models;

namespace LxCommon
{
    public abstract class FlashData : IComparable
    {
        private string _flashTime;
        private FlashType _flashtype; //IC/CG
        private double _longitude;
        private double _latitude;
        private double _height;
        private float _amplitude;  // Peak current (A)
        private int _confidence = 0;  // Composite confidence
        private double[] _solution; // Solution (x,y,z,t) that the flashportion is calculated
        private FlashConfidenceData _confidenceData = new FlashConfidenceData();

        // Order based on the offset value
        public int CompareTo(Object obj)
        {
            return TimeStamp.CompareTo(((FlashData)obj).TimeStamp);
        }
        
        // Compare the location in KM
        public int LocationCompareTo(FlashData flashData)
        {
            var distanceInKm = StationData.DistanceInKM(this.Latitude, this.Longitude, 0, flashData.Latitude, flashData.Longitude, 0);
            return (int)distanceInKm;
        }

        public float LocationError
        {
            get { return 0; }
        }

        public override string ToString()
        {
            return string.Format("{0},{1},{2},{3},{4}", FlashTime, _latitude, _longitude, _amplitude, FlashType);
        }

        public string FlashTime
        {
            get { return TimeStamp.ToString(); }
            set
            {
                TimeStamp = new DateTimeUtcNano(value);
            }
        }

        public double Longitude
        {
            get { return _longitude; }
            set { _longitude = Math.Round(value, 5, MidpointRounding.AwayFromZero); }
        }

        public double Latitude
        {
            get { return _latitude; }
            set { _latitude = Math.Round(value, 5, MidpointRounding.AwayFromZero); }
        }

        public double Height
        {
            get { return _height; }
            set { _height = Math.Round(value, 0, MidpointRounding.AwayFromZero); }
        }

        public float Amplitude
        {
            get { return _amplitude; }
            set { _amplitude = value; }
        }

        public abstract string Description
        {
            get;
            set;
        }

        public FlashType FlashType
        {
            get { return _flashtype; }
            set { _flashtype = value; }
        }

        public int Confidence
        {
            get { return _confidence; }
            set { _confidence = value; }
        }

        public FlashConfidenceData ConfidenceData
        {
            get { return _confidenceData; }
            set { _confidenceData = value; }
        }

        public double[] Solution
        {
            get { return _solution; }
            set { _solution = value; }
        }

        public DateTimeUtcNano TimeStamp { get; set; }
    }

    public class LTGFlash : FlashData
    {
        private const string WwllnSource = "wwlln";
        private const string TlnSource = "tln";

        private ArrayList _flashPortionList = null;
        private FlashExtendedData _flashExtendedData;

        public LTGFlash()
        {
            _flashExtendedData = new FlashExtendedData();
        }

        public LTGFlash(string time, double latitude, double longitude, double height, float amplitude, string solution, FlashType type, int confidence)
        {
            FlashTime = time;
            Latitude = latitude;
            Longitude = longitude;
            Height = height;
            Amplitude = amplitude;
            FlashType = type;
            Confidence = confidence;

            _flashExtendedData = FlashExtendedData.Deserialize(solution) ?? new FlashExtendedData();

        }

        public void AddFlashPortion(FlashData flashPortion)
        {
            if (_flashPortionList == null)
                _flashPortionList = new ArrayList();
            _flashPortionList.Add(flashPortion);
        }

        public ArrayList FlashPortionList
        {
            get { return _flashPortionList; }
            set { _flashPortionList = value; }
        }

        public void FinalizeFlash(ConcurrentDictionary<string, StationData> stationTable, bool isWwlln)
        {
            if (!isWwlln)
            {
                SanityCheck(stationTable);
            }

            SetFlashData();
        }

        public override string ToString()
        {
            return FlashPortionList.Cast<object>().Aggregate("", (current, portion) => current + (portion.ToString() + "\r\n"));
        }

        private void SanityCheck(ConcurrentDictionary<string, StationData> stationTable)
        {
            try
            {
                int i = 0, numOfCGPortions = 0;
                LTGFlashPortion curPortion = null, bestCGportion = null;

                _flashPortionList.Sort();

                var closestFirstSensorDistance = int.MaxValue;
                var closestLastSensorDistance = int.MaxValue;
                // Calculate the distance from the first and last sensor to the flash 
                foreach (LTGFlashPortion portion in _flashPortionList)
                {
                    if (!portion.IsWwlln)
                    {
                        // First sensor
                        var theOffset = (Offset)portion.OffsetList[0];
                        if (!stationTable.ContainsKey(theOffset.StationId))
                            continue;
                        var stationData = (StationData)stationTable[theOffset.StationId];
                        theOffset.DistanceToFlash = (float)stationData.DistanceFromLatLongHeightInMicrosecond(portion.Latitude, portion.Longitude, 0);
                        if (theOffset.DistanceToFlash < closestFirstSensorDistance)
                            closestFirstSensorDistance = (int)theOffset.DistanceToFlash;

                        // Last sensor
                        theOffset = (Offset)portion.OffsetList[portion.OffsetList.Count - 1];
                        if (!stationTable.ContainsKey(theOffset.StationId))
                            continue;
                        stationData = (StationData)stationTable[theOffset.StationId];
                        theOffset.DistanceToFlash = (float)stationData.DistanceFromLatLongHeightInMicrosecond(portion.Latitude, portion.Longitude, 0);
                        if (theOffset.DistanceToFlash < closestLastSensorDistance)
                            closestLastSensorDistance = (int)theOffset.DistanceToFlash;
                    }
                }

                // Check the time and distance 
                i = 0;
                while (i < _flashPortionList.Count - 1)
                {
                    curPortion = (LTGFlashPortion)_flashPortionList[i];

                    if (!curPortion.IsWwlln)
                    {
                        // If all the sensors are from long distant sensors, the location is not reliable
                        if (curPortion.OffsetList[0].DistanceToFlash > 3.0 * closestLastSensorDistance)
                        {
                            _flashPortionList.RemoveAt(i);
                            continue;
                        }

                        if (curPortion.FlashType == FlashType.FlashTypeCG)
                        {
                            numOfCGPortions++;
                        }
                    }

                    i++;
                }

                // Check polarity of the CGs
                if (numOfCGPortions < 2)
                    return;

                i = 0;
                numOfCGPortions = 0;  // The number may not reliable since some of the CGs maybe removed, so recount it
                while (i < _flashPortionList.Count)
                {
                    curPortion = (LTGFlashPortion)_flashPortionList[i++];
                    if (curPortion.FlashType == FlashType.FlashTypeCG && !curPortion.IsWwlln)
                    {
                        numOfCGPortions++;
                        if (bestCGportion == null)
                        {
                            bestCGportion = curPortion;
                        }
                        else if (bestCGportion.OffsetList[0].DistanceToFlash > curPortion.OffsetList[0].DistanceToFlash)
                        {
                            bestCGportion = curPortion;
                        }
                    }
                }

                if (numOfCGPortions < 2)
                    return;

                while (i < _flashPortionList.Count)
                {
                    curPortion = (LTGFlashPortion)_flashPortionList[i++];
                    if (curPortion.FlashType == FlashType.FlashTypeCG)
                    {
                        if (bestCGportion != null && (curPortion != bestCGportion && Math.Sign(curPortion.Amplitude) != Math.Sign(bestCGportion.Amplitude)))
                        {
                            curPortion.Amplitude *= -1;  // Reverse the polarity
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void SetFlashData()
        {
            Latitude = 0;
            Longitude = 0;
            Height = 0;
            CgMultiplicity = 0;
            IcMultiplicity = 0;
            DurationSeconds = 0;
            FlashType = FlashType.FlashTypeIC;
            LTGFlashPortion brightestTlnIc = null;
            LTGFlashPortion brightestTlnCg = null;
            LTGFlashPortion brightestWwllnIc = null;
            LTGFlashPortion brightestWwllnCg = null;

            var uniqueSensors = new HashSet<string>();
            LTGFlashPortion startPortion = null;
            LTGFlashPortion endPortion = null;

            if (_flashPortionList != null)
            {
                var initialized = false;
                var random = new Random();
                var count = 0;
                while (count < _flashPortionList.Count)
                {
                    var portion = _flashPortionList[count] as LTGFlashPortion;
                    if (portion != null)
                    {
                        
                        if (!initialized)
                        {
                            startPortion = portion;
                            endPortion = portion;
                            MinLatitude = portion.Latitude;
                            MaxLatitude = portion.Latitude;
                            MinLongitude = portion.Longitude;
                            MaxLongitude = portion.Longitude;
                            Version = portion.Version;
                            initialized = true;
                        }
                        else
                        {
                            if (portion.TimeStamp < startPortion.TimeStamp) startPortion = portion;
                            if (portion.TimeStamp > endPortion.TimeStamp) endPortion = portion;
                            if (portion.Latitude < MinLatitude) MinLatitude = portion.Latitude;
                            if (portion.Latitude > MaxLatitude) MaxLatitude = portion.Latitude;
                            if (portion.Longitude < MinLongitude) MinLongitude = portion.Longitude;
                            if (portion.Longitude > MaxLongitude) MaxLongitude = portion.Longitude;
                        }

                        if (portion.FlashType == FlashType.FlashTypeGlobalCG || portion.FlashType == FlashType.FlashTypeCG)
                        {
                            CgMultiplicity = CgMultiplicity + 1;
                            portion.Height = 0;
                            if (portion.FlashType == FlashType.FlashTypeGlobalCG)
                            {
                                if (brightestWwllnCg == null || (Math.Abs(portion.Amplitude) > Math.Abs(brightestWwllnCg.Amplitude)))
                                {
                                    brightestWwllnCg = portion;
                                }
                            }
                            else
                            {
                                if (brightestTlnCg == null || (Math.Abs(portion.Amplitude) > Math.Abs(brightestTlnCg.Amplitude)))
                                {
                                    brightestTlnCg = portion;
                                }
                            }
                        }
                        else if (portion.FlashType == FlashType.FlashTypeGlobalIC || portion.FlashType == FlashType.FlashTypeIC)
                        {
                            IcMultiplicity = IcMultiplicity + 1;
                            if (portion.Height < 0)
                            {
                                portion.Height *= -1;
                            }
                            
                            if (portion.FlashType == FlashType.FlashTypeGlobalIC)
                            {
                                if (brightestWwllnIc == null || (Math.Abs(portion.Amplitude) > Math.Abs(brightestWwllnIc.Amplitude)))
                                {
                                    brightestWwllnIc = portion;
                                }
                            }
                            else
                            {
                                // Bad height due to the network shape, 
                                while (portion.Height > 20000 * random.Next(random.Next(93, 100), 100) / 98.0)
                                {
                                    portion.Height *= random.Next(random.Next(26, 60), 100) / 100.0;
                                }

                                if (brightestTlnIc == null || (Math.Abs(portion.Amplitude) > Math.Abs(brightestTlnIc.Amplitude)))
                                {
                                    brightestTlnIc = portion;
                                }
                            }
                        }

                        if (portion.OffsetList != null)
                        {
                            foreach (var offset in portion.OffsetList)
                            {
                                uniqueSensors.Add(offset.StationId);
                            }
                        }
                    }

                    count++;
                }

                if (startPortion != null)
                {
                    if (IcMultiplicity + CgMultiplicity > 1)
                    {
                        StartTime = startPortion.FlashTime;
                        EndTime = endPortion.FlashTime;
                        DurationSeconds = endPortion.TimeStamp.Seconds - startPortion.TimeStamp.Seconds;
                    }
                    else
                    {
                        StartTime = startPortion.FlashTime;
                        EndTime = endPortion.FlashTime;
                    }
                }

                NumberSensors = uniqueSensors.Count;
                
                //location and amplitude should always come from TLN portions
                if (brightestTlnCg != null)
                {
                    Latitude = brightestTlnCg.Latitude;
                    Longitude = brightestTlnCg.Longitude;
                    Height = 0;
                    Amplitude = brightestTlnCg.Amplitude;
                    FlashTime = brightestTlnCg.FlashTime;
                    Confidence = brightestTlnCg.Confidence;
                    FlashType = FlashType.FlashTypeCG;
                }
                else if (brightestWwllnCg != null)
                {
                    if (brightestTlnIc != null)
                    {
                        Latitude = brightestTlnIc.Latitude;
                        Longitude = brightestTlnIc.Longitude;
                        Height = 0;
                        Amplitude = brightestTlnIc.Amplitude;
                        FlashTime = brightestTlnIc.FlashTime;
                        Confidence = brightestTlnIc.Confidence;
                        FlashType = FlashType.FlashTypeCG;
                    }
                    else
                    {
                        Latitude = brightestWwllnCg.Latitude;
                        Longitude = brightestWwllnCg.Longitude;
                        Height = 0;
                        Amplitude = brightestWwllnCg.Amplitude;
                        FlashTime = brightestWwllnCg.FlashTime;
                        Confidence = brightestWwllnCg.Confidence;
                        FlashType = FlashType.FlashTypeGlobalCG;
                    }
                }
                else if (brightestTlnIc != null)
                {
                    Latitude = brightestTlnIc.Latitude;
                    Longitude = brightestTlnIc.Longitude;
                    Height = brightestTlnIc.Height;
                    Amplitude = brightestTlnIc.Amplitude;
                    FlashTime = brightestTlnIc.FlashTime;
                    Confidence = brightestTlnIc.Confidence;
                    FlashType = FlashType.FlashTypeIC;
                }
                else if (brightestWwllnIc != null)
                {
                    Latitude = brightestWwllnIc.Latitude;
                    Longitude = brightestWwllnIc.Longitude;
                    Height = brightestWwllnIc.Height;
                    Amplitude = brightestWwllnIc.Amplitude;
                    FlashTime = brightestWwllnIc.FlashTime;
                    Confidence = brightestWwllnIc.Confidence;
                    FlashType = FlashType.FlashTypeGlobalIC;
                }
            }
        }

        public override string Description
        {
            get { return _flashExtendedData.ToJsonString(); }
            set { _flashExtendedData = FlashExtendedData.Deserialize(value) ?? new FlashExtendedData(); }
        }

        public string Version
        {
            get { return _flashExtendedData.Version; }
            private set { _flashExtendedData.Version = value; }
        }

        public string StartTime
        {
            get { return _flashExtendedData.StartTime; }
            private set { _flashExtendedData.StartTime = value; }
        }

        public string EndTime
        {
            get { return _flashExtendedData.EndTime; }
            private set { _flashExtendedData.EndTime = value; }
        }

        public decimal DurationSeconds
        {
            get { return _flashExtendedData.DurationSeconds; }
            private set { _flashExtendedData.DurationSeconds = value; }
        }

        public int NumberSensors
        {
            get { return _flashExtendedData.NumberSensors; }
            private set { _flashExtendedData.NumberSensors = value; }
        }

        public int IcMultiplicity
        {
            get { return _flashExtendedData.IcMultiplicity; }
            private set { _flashExtendedData.IcMultiplicity = value; }
        }

        public int CgMultiplicity
        {
            get { return _flashExtendedData.CgMultiplicity; }
            private set { _flashExtendedData.CgMultiplicity = value; }
        }

        public string Source
        {
            get { return _flashExtendedData.Source; }
            private set { _flashExtendedData.Source = value; }
        }

        public double MaxLatitude
        {
            get { return _flashExtendedData.MaxLatitude; }
            private set { _flashExtendedData.MaxLatitude = value; }
        }

        public double MinLatitude
        {
            get { return _flashExtendedData.MinLatitude; }
            private set { _flashExtendedData.MinLatitude = value; }
        }

        public double MaxLongitude
        {
            get { return _flashExtendedData.MaxLongitude; }
            private set { _flashExtendedData.MaxLongitude = value; }
        }

        public double MinLongitude
        {
            get { return _flashExtendedData.MinLongitude; }
            private set { _flashExtendedData.MinLongitude = value; }
        }
    }

    public class LTGFlashPortion : FlashData
    {
        private List<Offset> _offsetList; // Good offsets which meet the residual errors
        private object _commonPortionRef;  // Record all the sensors that saw the flash portion, used in post processing
        private object _activeSensorTable; // Record all the active sensors during the second, used in flase location removal
        private List<Offset> _closestSensorOffsetForClassification;  // Record 5 offsets of the closest sensors
        private PortionExtendedData _portionExtendedData;
        private bool _isWwlln;

        public LTGFlashPortion()
        {
            _portionExtendedData = new PortionExtendedData();
            _offsetList = new List<Offset>();
            _commonPortionRef = null;
            _activeSensorTable = null;
            _closestSensorOffsetForClassification = null;
            IsWwlln = false;
            RejectedReason = string.Empty;
        }

        public LTGFlashPortion(string time, double latitude, double longitude, double height, float amplitude, string solution, FlashType type, int confidence) : this()
        {
            FlashTime = time;
            Latitude = latitude;
            Longitude = longitude;
            Height = height;
            Amplitude = amplitude;
            FlashType = type;
            if (FlashType == FlashType.FlashTypeGlobalCG || FlashType == FlashType.FlashTypeGlobalIC)
            {
                IsWwlln = true;
            }
            Confidence = confidence;
            _portionExtendedData = PortionExtendedData.Deserialize(solution) ?? new PortionExtendedData();
        }

        public void UpdateSensorToFlashDistance(ConcurrentDictionary<string, StationData> stationTable)
        {
            foreach (var offset in _offsetList)
            {
                StationData station;
                if (stationTable.TryGetValue(offset.StationId, out station))
                {
                    offset.DistanceToFlash = station.DistanceFromLatLongHeightInMicrosecond(Latitude, Longitude, 0);
                }
            }

            _offsetList.Sort(new OffsetCompareDistance());
        }

        public object CommonPortionRef
        {
            get { return _commonPortionRef; }
            set { _commonPortionRef = value; }
        }

        public List<Offset> OffsetList
        {
            get { return _offsetList; }
            set
            {
                _offsetList = value;
                if (_offsetList != null && _offsetList.Count > 0)
                {
                    _portionExtendedData.StationOffsets = new Dictionary<string, double>(_offsetList.Count);
                    foreach (var offset in _offsetList)
                    {
                        if (!string.IsNullOrWhiteSpace(offset.StationId) && !_portionExtendedData.StationOffsets.ContainsKey(offset.StationId))
                        {
                            _portionExtendedData.StationOffsets.Add(offset.StationId, Math.Round(offset.Value, 0));
                        }
                    }
                    _portionExtendedData.NumberStations = _portionExtendedData.StationOffsets.Count;
                }
            }
        }

        public object ActiveSensorTable
        {
            get { return _activeSensorTable; }
            set { _activeSensorTable = value; }
        }

        public bool CheckConfidence(FlashConfidenceData minConfidenceData)
        {
            if (ConfidenceData.CompareTo(minConfidenceData) >= 0)
                return true;
            return false;
        }

        private const int NAdc2VAdc = 32768;
        private const double Beta = 0.3;
        private const double Z0 = 377;


        public static float CalculateFlashPortionCurrent(double peakVolts, double distance, StationDecibelParameter calibData)
        {
            float i = 0;
            try
            {
                var vadc = peakVolts / NAdc2VAdc;
                var vout = vadc - calibData.DcOffsetInVolts - calibData.InternalNoiseInVolts * Math.Sign(vadc);
                var vb = (vout - calibData.ExternalNoiseInVolts) / 28.8 + 1.25 * Math.Sign(vout) * Math.Exp(13.9 * (Math.Abs(vout) - 1));
                var e = vb / calibData.EffectiveLengthInMeter;
                i = (float)(2 * Math.PI * e * distance / (Beta * Z0));

            }
            catch (Exception ex)
            {
                EventManager.LogInfo("A problem occured while calculating Current Flash Portion for Amperes in Ligtning Core Service. \r\nReason: " + ex.Message);
                i = 0;
            }
            return i;
        }

        public override string Description
        {
            get { return _portionExtendedData.ToJsonString(); }
            set { _portionExtendedData = PortionExtendedData.Deserialize(value) ?? new PortionExtendedData(); }
        }

        public ErrorEllipse ErrorEllipse
        {
            get { return _portionExtendedData.ErrorEllipse; }
            set { _portionExtendedData.ErrorEllipse = value; }
        }

        public string Version
        {
            get { return _portionExtendedData.Version; }
            set { _portionExtendedData.Version = value; }
        }

        public int NumberSensors
        {
            get { return _portionExtendedData.NumberStations; }

        }

        public List<Offset> ClosestSensorOffsetForClassification
        {
            get { return _closestSensorOffsetForClassification; }
            set { _closestSensorOffsetForClassification = value; }
        }

        public bool IsWwlln
        {
            get; set;
        }
        
        public string RejectedReason
        {
            get; set;
        }

        public override string ToString()
        {
            return base.ToString() + " Sensors:" + NumberSensors;
        }

        internal void SetOffsetAmplitudes(List<Tuple<string, short>> amplitudes)
        {
            _offsetList.Clear();
            if (_portionExtendedData != null && _portionExtendedData.StationOffsets != null)
            {
                double offsetValue;
                foreach (Tuple<string, short> amplitude in amplitudes)
                {
                    _portionExtendedData.StationOffsets.TryGetValue(amplitude.Item1, out offsetValue);
                    _offsetList.Add
                        (
                            new Offset()
                            {
                                StationId = amplitude.Item1,
                                AbsPeakAmplitude = amplitude.Item2,
                                Value = offsetValue
                            }
                        );
                }
                _offsetList.Sort();
            }
        }
    }


    public class FlashConfidenceData : IComparable
    {
        private short _nearSensorConfidence = 0; // nearest agreed sensor/nearest sensors
        private short _bigestAngle = 0; // biggest angle of the sensors
        private short _sensorDistribution = 0; // number of 10 degree sections occupied by sensors

        public short NearSensorConfidence
        {
            get { return _nearSensorConfidence; }
            set { _nearSensorConfidence = value; }
        }

        public short BiggestAngle
        {
            get { return _bigestAngle; }
            set { _bigestAngle = value; }
        }

        public short SensorDistribution
        {
            get { return _sensorDistribution; }
            set { _sensorDistribution = value; }
        }

        public int CompareTo(object obj)
        {
            var minConfidence = (FlashConfidenceData)obj;
            if (_nearSensorConfidence >= minConfidence.NearSensorConfidence
                && _bigestAngle >= minConfidence.BiggestAngle
                && _sensorDistribution >= minConfidence.SensorDistribution)
                return 1;
            return -1;
        }
    }
}

