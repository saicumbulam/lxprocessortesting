﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace LxCommon.DataStructures
{
    public class ConcurrentSortedSet<T> : IDisposable
    {
        private readonly SortedSet<T> _sortedSet;
        private readonly IComparer<T> _comparer; 
        private readonly ReaderWriterLockSlim _lock;

        public ConcurrentSortedSet()
            : this(null)
        { }

        public ConcurrentSortedSet(IComparer<T> comparer)
        {
            if (comparer != null)
            {
                _comparer = comparer;
                _sortedSet = new SortedSet<T>(_comparer);
            }
            else
                _sortedSet = new SortedSet<T>();

            _lock = new ReaderWriterLockSlim();
        }

        public bool TryAdd(T item)
        {
            bool success = false;
            _lock.EnterWriteLock();
            try
            {
                success = _sortedSet.Add(item);
            }
            finally
            {
                _lock.ExitWriteLock();
            }
            return success;
        }

        public bool TryAddRange(List<T> items)
        {
            bool success = true;
            _lock.EnterWriteLock();
            try
            {
                foreach (T item in items)
                {
                    success = _sortedSet.Add(item) && success;
                }
            }
            finally
            {
                _lock.ExitWriteLock();
            }
            return success;
        }

        public int RemoveWhere(Predicate<T> match)
        {
            int count = 0;
            _lock.EnterWriteLock();
            try
            {
                if (_sortedSet.Count > 0)
                {
                    count = _sortedSet.RemoveWhere(match);
                }
            }
            finally
            {
                _lock.ExitWriteLock();
            }
            return count;
        }

        public int RemoveBetween(T lowerValue, T upperValue)
        {
            int count = 0;
            _lock.EnterWriteLock();
            try
            {
                if (_sortedSet.Count > 0)
                {
                    SortedSet<T> between = _sortedSet.GetViewBetween(lowerValue, upperValue);
                    if (between.Count > 0)
                    {
                        //since this a "view" it will remove the entries from the _sortedSet as well
                        between.Clear();
                    }   
                }
            }
            finally
            {
                _lock.ExitWriteLock();
            }
            return count;
        }

        public SortedSet<T> GetBetween(T lowerValue, T upperValue)
        {
            SortedSet<T> retBetween = null;
            _lock.EnterReadLock();
            try
            {
                SortedSet<T> between = _sortedSet.GetViewBetween(lowerValue, upperValue);
                T[] copy = new T[between.Count];
                if (between.Count > 0)
                {
                    between.CopyTo(copy);                                    
                }

                if (_comparer != null)
                {
                    retBetween = new SortedSet<T>(copy, _comparer);
                }
                else
                {
                    retBetween = new SortedSet<T>(copy);
                }
            }
            finally
            {
                _lock.ExitReadLock();
            }

            return retBetween;
        }

        public void Dispose()
        {
            _lock.Dispose();
        }

        public int Count 
        {
            get
            {
                int count;
                _lock.EnterReadLock();
                try
                {
                    count = _sortedSet.Count;
                }
                finally
                {
                    _lock.ExitReadLock();
                }
                return count;
            }
        }

       
    }
}
