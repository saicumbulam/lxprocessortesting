﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using Aws.Core.Utilities;
using LxCommon.Monitoring;

namespace LxCommon.DataStructures
{
    public abstract class ParallelQueueProcessor<T>
    {
        protected readonly ConcurrentQueue<T> _queue;
        private int _activeCommands;
        private int _threadLock;
        protected readonly ManualResetEventSlim _stopEvent;
        private readonly int _maxActiveWorkItems;

        private const ushort EventId = TaskMonitor.ParallelQueueProcessor;
        
        protected ParallelQueueProcessor(ManualResetEventSlim stopEvent, int maxActiveWorkItems)
        {
            _queue = new ConcurrentQueue<T>();
            _activeCommands = 0;
            _threadLock = 0;
            _stopEvent = stopEvent;
            _maxActiveWorkItems = maxActiveWorkItems;
        }
        //called for processing of the packet
        public virtual void Add(T workItem)
        {
            _queue.Enqueue(workItem);

            // This controls the thread that manages the queue, make sure we only launch one thread 
            if (0 == Interlocked.CompareExchange(ref _threadLock, 1, 0))
            {
                Task.Factory.StartNew(ProcessQueue);
            }
        }

        private void ProcessQueue()
        {
           try
            {
                do
                {
                    T workItem;
                    SpinWait sw = new SpinWait();
                    // Try to pull a work item off the queue
                    while (IsSafeToProcess() && _queue.TryDequeue(out workItem))
                    {   // We have a cmd so increment active and queue worker thread
                        Interlocked.Increment(ref _activeCommands);

                        T item = workItem;
                        //sent for preprocessing and post processing at the same time
                        Task.Factory.StartNew(() => ProcessWorkItem(DoWork, item));

                        
                        while (Interlocked.CompareExchange(ref _activeCommands, _activeCommands, _maxActiveWorkItems) == _maxActiveWorkItems)
                        {   // Wait for the number of active commands to drop below the Max allowed.
                            if (_stopEvent.Wait(0)) 
                            {
                                Shutdown();
                                break;
                            }

                            sw.SpinOnce();
                        }

                        if (_stopEvent.Wait(0))
                        {
                            Shutdown();
                            break;
                        }
                    }

                    while (_queue.Count <= 0 && !_stopEvent.Wait(0))
                    {
                        sw.SpinOnce();
                    }

                } while (_activeCommands > 0 || _queue.Count > 0);
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventId, "Failure getting work item from ParallelQueueProcessor.", ex);
            }
            finally
            {
                Interlocked.Decrement(ref _threadLock);
            }
        }

        protected virtual bool IsSafeToProcess()
        {
            return true;
        }

        private void ProcessWorkItem(Action<T> action, T workItem)
        {
            try
            {
                action(workItem);
            }
            catch (Exception ex)
            {
                EventManager.LogWarning(EventId + 1, "Failure trying to act on work item in ParallelQueueProcessor.", ex);
            }
            finally
            {
                Interlocked.Decrement(ref _activeCommands);
            }
        }

        protected abstract void DoWork(T workItem);

        protected virtual void Shutdown()
        {
        }

        public int Count { get { return _queue.Count; } }

        public int ActiveCommands { get { return _activeCommands; } }

        public int MaxActiveWorkItems { get { return _maxActiveWorkItems; } } 
    }
}
