﻿using System;
using System.Collections.Generic;
using System.Threading;

using Aws.Core.Utilities;

using LxCommon.Utilities;

namespace LxCommon.DataStructures
{
    public class FluxList<T> : List<T>
    {
        private static readonly Random _random;
        private static readonly object _randLock;

        static FluxList()
        {
            _random = new Random();
            _randLock = new object();
        }

        private readonly MedianSampler<int> _sampler;
        private int _usages;
        private int _checkAt;

        private const int DefaultSampleSize = 100;
        private const int SampleSizeCheckAtMultiplier = 5000;

        public FluxList() : this(0, DefaultSampleSize) {}

        public FluxList(int capacity) : this(capacity, DefaultSampleSize) {}

        public FluxList(int capacity, int sampleSize) : base(capacity)
        {
            _sampler = new MedianSampler<int>(sampleSize);
            _usages = 0;
            _checkAt = GetRandomCheckAt(sampleSize);
        }

        public FluxList(IEnumerable<T> collection)
            : this(collection, DefaultSampleSize)
            {}

        public FluxList(IEnumerable<T> collection, int sampleSize) : base(collection)
        {
            _sampler = new MedianSampler<int>(sampleSize);
            _usages = 0;
            _checkAt = GetRandomCheckAt(sampleSize);
        }


        private int GetRandomCheckAt(int sampleSize)
        {
            sampleSize = sampleSize * SampleSizeCheckAtMultiplier;
            int checkAt;
            int quarter = (sampleSize / 4);
            int lower = sampleSize - quarter;
            int upper = sampleSize + quarter;
            lock (_randLock)
            {
                checkAt = _random.Next(lower, upper);
            }
            return checkAt;
        }
        // This function returns the this variable to the size of the size vabriable
        public void Resize(int size, Func<T> factory = null)
        {
            if (Count > size)
            {
                Clear();
            }

            if (Count < size)
            {
                while (Count < size)
                {
                    Add(factory == null ? default(T) : factory());
                }
            }

        }

        private void CheckCapacity()
        {
            if (Interlocked.CompareExchange(ref _usages, 0, _checkAt) == _checkAt)
            {
                int capacity = _sampler.GetAtPercent(1);
                if (capacity > 0 && capacity != Capacity)
                {
                    Capacity = capacity;
                }
                _checkAt = GetRandomCheckAt(_sampler.Capacity);
            }
        }

        public void Repopulate(IEnumerable<T> values)
        {
            Clear();
            foreach (T value in values)
            {
                Add(value);
            }
        }

        public void SetLengthAtLeast(int length, Func<T> factory = null)
        {
            while (Count < length)
            {
                Add(factory == null ? default(T) : factory());
            }
        }

        public new void Clear()
        {
            _sampler.AddSample(Count);
            Interlocked.Increment(ref _usages);
            base.Clear();
            CheckCapacity();
        }

    }
}
