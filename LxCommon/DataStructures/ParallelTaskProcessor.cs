﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Aws.Core.Utilities;
using LxCommon.Monitoring;

namespace LxCommon.DataStructures
{
    public abstract class ParallelTaskProcessor<T>
    {
        private int _activeCommands;
        private int _threadLock;
        private readonly ManualResetEventSlim _stopEvent;
        private readonly int _maxActiveWorkItems;

        private const ushort EventId = TaskMonitor.ParallelTaskProcessor;

        protected ParallelTaskProcessor(ManualResetEventSlim stopEvent, int maxActiveWorkItems)
        {
            _activeCommands = 0;
            _threadLock = 0;
            _stopEvent = stopEvent;
            _maxActiveWorkItems = maxActiveWorkItems;
        }

       
        public void Start()
        {
            // This controls the thread that manages the queue, make sure we only launch one thread 
            if (0 == Interlocked.CompareExchange(ref _threadLock, 1, 0))
            {
                Task.Factory.StartNew(ProcessWorkItems);
            }
        }

        protected abstract bool TryGetWorkItem(out T workItem);

        private void ProcessWorkItems()
        {
            try
            {
                SpinWait sw = new SpinWait();
                while (true)
                {
                    do
                    {
                        if (_stopEvent.Wait(0))
                        {
                            break;
                        } 
                        try
                        {
                            T workItem;
                            // Try to get a work item 
                            while (TryGetWorkItem(out workItem))
                            {   // We have a cmd so increment active and start task
                                Interlocked.Increment(ref _activeCommands);

                                T item = workItem;
                                Task.Factory.StartNew(() => ProcessWorkItem(DoWork, item));

                                
                                while (Interlocked.CompareExchange(ref _activeCommands, _activeCommands, _maxActiveWorkItems) == _maxActiveWorkItems)
                                {   // Wait for the number of active commands to drop below the Max allowed.
                                    if (_stopEvent.Wait(0))
                                        break;

                                    sw.SpinOnce();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            EventManager.LogWarning(EventId, "Failure getting a work items", ex);
                        }
                    } while (IsWaitingForActiveCommands());

                    if (_stopEvent.Wait(0))
                        break;

                    sw.SpinOnce();
                }
            }
            catch (Exception ex)
            {
                EventManager.LogError(EventId + 1, "Failure processing work items.", ex);
            }
            finally
            {
                Interlocked.Decrement(ref _threadLock);
            }
        }

        private bool IsWaitingForActiveCommands()
        {
            return Interlocked.CompareExchange(ref _activeCommands, _activeCommands, _maxActiveWorkItems) > 0;
        }

        private void ProcessWorkItem(Action<T> action, T workItem)
        {
            try
            {
                action(workItem);
            }
            catch (Exception ex)
            {
                EventManager.LogWarning(EventId + 2, "Failure trying to act on work item.", ex);
            }
            finally
            {
                Interlocked.Decrement(ref _activeCommands);
            }
        }

        protected abstract void DoWork(T workItem);

        public int ActiveCommands
        {
            get { return _activeCommands; }
        }

        public int MaxActiveWorkItems
        {
            get { return _maxActiveWorkItems; }
        }

    }
}
