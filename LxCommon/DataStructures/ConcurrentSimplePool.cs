﻿using System;
using System.Collections.Generic;

using Aws.Core.Utilities;

namespace LxCommon.DataStructures
{
    public class ConcurrentSimplePool<T> where T : class
    {
        protected Queue<T> _items;
        protected int _size = 0;
        protected Func<T> _factory;
        protected Action<T> _cleanForReuse;
        private readonly object _lock;
        private string _name;

        public ConcurrentSimplePool(int size, Func<T> factory, string name = null)
        {
            _size = size;
            _items = new Queue<T>(_size);
            _factory = factory;
            _cleanForReuse = null;
            _lock = new object();

            _name = name ?? string.Empty;

            while (_items.Count < _size)
            {
                _items.Enqueue(_factory());
            }

        }

        public ConcurrentSimplePool(int size, Func<T> factory, Action<T> cleanForReuse, string name = null)
            : this(size, factory, name)
        {
            _cleanForReuse = cleanForReuse;
        }

        public virtual T Take()
        {
            T item = null;

            lock (_lock)
            {
                if (_items.Count > 0)
                {
                    item = _items.Dequeue();
                }
            }

            if (item == null)
            {
                item = _factory();
                if (EventManager.IsTypeDebugEnabled)
                {
                    EventManager.LogDebug(string.Format("Pool empty for {0} Capacity: {1}", _name, _size));
                }
            }

            return item;
        }

        public virtual bool Return(T item)
        {
            bool wasReturned = false;

            if (_cleanForReuse != null)
            {
                _cleanForReuse(item);
            }

            lock (_lock)
            {
                if (_items.Count < _size)
                {
                    _items.Enqueue(item);
                    wasReturned = true;
                }
            }

            return wasReturned;
        }
    }
}
