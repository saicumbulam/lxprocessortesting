using System;

namespace LxCommon.Models
{
    public struct Waveform : IEquatable<Waveform>, IComparable<Waveform>
    {
        public DateTimeUtcNano TimeStamp { get; set; }
        public short Amplitude { get; set; }

        public static bool operator ==(Waveform wave1, Waveform wave2)
        {
            return wave1.TimeStamp.Nanoseconds == wave2.TimeStamp.Nanoseconds && wave1.Amplitude == wave2.Amplitude;
        }

        public static bool operator !=(Waveform wave1, Waveform wave2)
        {
            return wave1.TimeStamp.Nanoseconds != wave2.TimeStamp.Nanoseconds || wave1.Amplitude != wave2.Amplitude;
        }

        public bool Equals(Waveform other)
        {
            return TimeStamp.Equals(other.TimeStamp) && Amplitude == other.Amplitude;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is Waveform && Equals((Waveform)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (TimeStamp.GetHashCode() * 397) ^ Amplitude.GetHashCode();
            }
        }

        public int CompareTo(Waveform other)
        {
            return TimeStamp.CompareTo(other.TimeStamp);
        }
    }
}