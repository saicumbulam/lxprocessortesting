﻿using System;

namespace LxCommon.Models
{
    public struct DateTimeUtcNano : IComparable<DateTimeUtcNano>, IEquatable<DateTimeUtcNano>
    {
        private static readonly DateTime MinDateTimeValue; // 2000-01-01 00:00:00
        private static readonly DateTime MaxDateTimeValue; // 2292-04-10 23:47:15

        public static readonly DateTimeUtcNano MinValue;
        public static readonly DateTimeUtcNano MaxValue;

        public const long NanosecondsPerSecond = 1000000000L;
        public const long NanosecondsPerTick = NanosecondsPerSecond / TimeSpan.TicksPerSecond;
        public const int NanosecondsPerMicrosecond = 1000;
        public const int MicrosecondsPerSecond = 1000000;
        
        private readonly long _nanoseconds;
        private long _microseconds;
        private decimal _seconds;
        private readonly DateTime _baseTime;
        private string _timeString;
        private int _nanosecondsFromBaseTime;
        
        private const long MaxEpochNanoseconds = long.MaxValue - NanosecondsPerSecond;
        
        static DateTimeUtcNano()
        {
            MinDateTimeValue = new DateTime(2000, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            long maxTicks = MaxEpochNanoseconds / NanosecondsPerTick;
            maxTicks = maxTicks + MinDateTimeValue.Ticks;
            MaxDateTimeValue = new DateTime(maxTicks, DateTimeKind.Utc);

            MinValue = new DateTimeUtcNano(MinDateTimeValue, 0);
            MaxValue = new DateTimeUtcNano(MaxDateTimeValue, 0);
        }

        public DateTimeUtcNano(DateTime baseTime, int nanoseconds)
        {
            if (baseTime.Kind != DateTimeKind.Utc)
            {
                baseTime = DateTime.SpecifyKind(baseTime, DateTimeKind.Utc);
            }
            
            if (baseTime < MinDateTimeValue || baseTime > MaxDateTimeValue)
            {
                throw new ArgumentOutOfRangeException("baseTime");
            }

            if (nanoseconds < 0 || nanoseconds > 999999999)
            {
                throw new ArgumentOutOfRangeException("nanoseconds");
            }

            _baseTime = baseTime;

            long baseEpochTicks = (baseTime - MinDateTimeValue).Ticks;
            _nanoseconds = baseEpochTicks * NanosecondsPerTick + nanoseconds;
            
            _microseconds = long.MinValue;
            _seconds = decimal.MinValue;
            _timeString = null;
            _nanosecondsFromBaseTime = int.MinValue;
        }

        public DateTimeUtcNano(long epochNanoseconds)
        {
            if (epochNanoseconds < 0 || epochNanoseconds > MaxEpochNanoseconds)
            {
                throw new ArgumentOutOfRangeException("epochNanoseconds");
            }

            long epochSeconds = epochNanoseconds / NanosecondsPerSecond;
            
            _baseTime = MinDateTimeValue.AddSeconds(epochSeconds);
            _nanoseconds = epochNanoseconds;

            _microseconds = long.MinValue;
            _seconds = decimal.MinValue;
            _timeString = null;
            _nanosecondsFromBaseTime = int.MinValue;
        }

        public DateTimeUtcNano(string value)
        {
            DateTime baseTime;
            decimal subseconds;
            string[] vals = value.Split('.');

            if (vals.Length != 2 || !DateTime.TryParse(vals[0], out baseTime) || !decimal.TryParse("." + vals[1], out subseconds))
            {
                throw new ArgumentOutOfRangeException("value");
            }

            if (baseTime.Kind != DateTimeKind.Utc)
            {
                baseTime = DateTime.SpecifyKind(baseTime, DateTimeKind.Utc);
            }

            if (baseTime < MinDateTimeValue || baseTime > MaxDateTimeValue)
            {
                throw new ArgumentOutOfRangeException("value");
            }

            _baseTime = baseTime;

            long nanoseconds = (long)Math.Round(subseconds * NanosecondsPerSecond, 0, MidpointRounding.AwayFromZero);
            if (nanoseconds < 0 || nanoseconds > 999999999)
            {
                throw new ArgumentOutOfRangeException("value");
            }

            
            long baseEpochTicks = (baseTime - MinDateTimeValue).Ticks;
            _nanoseconds = baseEpochTicks * NanosecondsPerTick + nanoseconds;

            _microseconds = long.MinValue;
            _seconds = decimal.MinValue;
            _timeString = null;
            _nanosecondsFromBaseTime = int.MinValue;
        }

        public int NanosecondsFromBaseTime
        {
            get
            {
                if (_nanosecondsFromBaseTime == int.MinValue)
                {
                    long baseEpochNanoseconds = (_baseTime - MinDateTimeValue).Ticks * NanosecondsPerTick;
                    _nanosecondsFromBaseTime = (int) (_nanoseconds - baseEpochNanoseconds);
                }

                return _nanosecondsFromBaseTime;
            }
        }

        public long Nanoseconds
        {
            get { return _nanoseconds; }
        }

        public decimal Seconds
        {
            get 
            { 
                if (_seconds == decimal.MinValue)
                {
                    _seconds = Math.Round(_nanoseconds / (decimal)NanosecondsPerSecond, 9, MidpointRounding.AwayFromZero);
                }

                return _seconds; 
            }
        }

        public DateTime BaseTime
        {
            get { return _baseTime; }
        }

        public long Microseconds
        {
            get 
            {
                if (_microseconds == long.MinValue)
                {
                    _microseconds = (long)Math.Round(_nanoseconds / (decimal)NanosecondsPerMicrosecond, 0, MidpointRounding.AwayFromZero);
                }
                return _microseconds; 
            }
        }

        public int CompareTo(DateTimeUtcNano other)
        {
            return _nanoseconds.CompareTo(other.Nanoseconds);
        }

        public static bool operator >(DateTimeUtcNano nano1, DateTimeUtcNano nano2)
        {
            return nano1.Nanoseconds > nano2.Nanoseconds;
        }

        public static bool operator <(DateTimeUtcNano nano1, DateTimeUtcNano nano2)
        {
            return nano1.Nanoseconds < nano2.Nanoseconds;
        }

        public static bool operator <=(DateTimeUtcNano nano1, DateTimeUtcNano nano2)
        {
            return nano1.Nanoseconds <= nano2.Nanoseconds;
        }

        public static bool operator >=(DateTimeUtcNano nano1, DateTimeUtcNano nano2)
        {
            return nano1.Nanoseconds >= nano2.Nanoseconds;
        }

        public string TimeString 
        {
            get 
            {
                if (_timeString == null)
                {
                    decimal subseconds = Seconds - (long)Seconds;
                    _timeString = String.Format("{0:yyyy-MM-ddTHH:mm:ss}{1:.000000000}", _baseTime, subseconds);
                }
                return _timeString;
            }
        }

        public static bool operator ==(DateTimeUtcNano nano1, DateTimeUtcNano nano2)
        {
            return nano1.Nanoseconds == nano2.Nanoseconds;
        }

        public static bool operator !=(DateTimeUtcNano nano1, DateTimeUtcNano nano2)
        {
            return nano1.Nanoseconds != nano2.Nanoseconds;
        }

        public bool Equals(DateTimeUtcNano other)
        {
            return _nanoseconds == other.Nanoseconds;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is DateTimeUtcNano && Equals((DateTimeUtcNano)obj);
        }

        public override int GetHashCode()
        {
            return _nanoseconds.GetHashCode();
        }

        public override string ToString()
        {
            return TimeString;
        }
    }
}
