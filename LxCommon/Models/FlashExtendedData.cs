﻿using System;

using Newtonsoft.Json;

namespace LxCommon.Models
{
    [JsonObject("fextd")]
    public class FlashExtendedData
    {
        private double _minLatitude;
        private double _minLongitude;
        private double _maxLatitude;
        private double _maxLongitude;
        private decimal _durationSeconds;

        [JsonProperty(PropertyName = "st")]
        public string StartTime { get; set; }

        [JsonProperty(PropertyName = "et")]
        public string EndTime { get; set; }
        
        [JsonProperty(PropertyName = "v")]
        public string Version { get; set; }
        
        [JsonProperty(PropertyName = "ns")]
        public int NumberSensors { get; set; }

        [JsonProperty(PropertyName = "im")]
        public int IcMultiplicity { get; set; }

        [JsonProperty(PropertyName = "cm")]
        public int CgMultiplicity { get; set; }

        [JsonProperty(PropertyName = "aa")]
        public double MaxLatitude
        {
            get { return _maxLatitude; }
            set { _maxLatitude = Math.Round(value, 5, MidpointRounding.ToEven); }
        }

        [JsonProperty(PropertyName = "ia")]
        public double MinLatitude {
            get { return _minLatitude; }
            set { _minLatitude = Math.Round(value, 5, MidpointRounding.ToEven); }
        }

        [JsonProperty(PropertyName = "ao")]
        public double MaxLongitude
        {
            get { return _maxLongitude; }
            set { _maxLongitude = Math.Round(value, 5, MidpointRounding.ToEven); }
        }

        [JsonProperty(PropertyName = "io")]
        public double MinLongitude
        {
            get { return _minLongitude; }
            set { _minLongitude = Math.Round(value, 5, MidpointRounding.ToEven); }
        }

        [JsonProperty(PropertyName = "d")]
        public decimal DurationSeconds
        {
            get { return _durationSeconds; }
            set { _durationSeconds = Math.Round(value, 9, MidpointRounding.ToEven); }
        }

        [JsonProperty(PropertyName = "s")]
        public string Source { get; set; }

        public string ToJsonString()
        {
            return JsonConvert.SerializeObject(this);
        }

        public static FlashExtendedData Deserialize(string data)
        {
            FlashExtendedData fed = null;
            try
            {
                fed = JsonConvert.DeserializeObject<FlashExtendedData>(data);
            }
            catch (JsonReaderException jre)
            {
                //just swallowing invalid json exceptions
            }

            return fed;
        }
    }
}
