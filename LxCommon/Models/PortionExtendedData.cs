﻿using System.Collections.Generic;

using Newtonsoft.Json;

namespace LxCommon.Models
{
    [JsonObject("pextd")]
    public class PortionExtendedData
    {
        [JsonProperty(PropertyName = "ee")]
        public ErrorEllipse ErrorEllipse { get; set; }
        [JsonProperty(PropertyName = "v")]
        public string Version { get; set; }
        [JsonProperty(PropertyName = "ns")]
        public int NumberStations { get; set; }
        [JsonProperty(PropertyName = "so")]
        public Dictionary<string, double> StationOffsets { get; set; }

        public string ToJsonString()
        {
            return JsonConvert.SerializeObject(this);
        }

        public static PortionExtendedData Deserialize(string data)
        {
            PortionExtendedData ped = null;
            try
            {
                ped = JsonConvert.DeserializeObject<PortionExtendedData>(data);
            }
            catch (JsonReaderException jre)
            {
                //just swallowing invalid json exceptions
            }
             
            return ped;
        }
    }
}
