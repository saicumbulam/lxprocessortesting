﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace LxCommon.Models
{
    /// <summary>
    /// Portion is meant to be a light weight class for easy serialization and deserialization of LtgFlashPortion data
    /// </summary>
    public class Portion : Lightning
    {
        public Portion() { }

        public Portion(LTGFlashPortion ltgPortion, Guid? flashId, bool isTlnSystem)
        {
            TimeStamp = ltgPortion.FlashTime;
            Longitude = ltgPortion.Longitude;
            Latitude = ltgPortion.Latitude;
            Height = ltgPortion.Height;
            Amplitude = ltgPortion.Amplitude;
            Type = ltgPortion.FlashType;
            Confidence = ltgPortion.Confidence;
            ErrorEllipse = ltgPortion.ErrorEllipse;
            Version = ltgPortion.Version;
            RejectedReason = ltgPortion.RejectedReason;
            if (isTlnSystem)
            {
                if (ltgPortion.OffsetList != null && ltgPortion.OffsetList.Count > 0)
                {
                    StationOffsets = new Dictionary<string, double>(ltgPortion.OffsetList.Count);
                    StationAmplitudes = new Dictionary<string, double>(ltgPortion.OffsetList.Count);
                    foreach (var offset in ltgPortion.OffsetList)
                    {
                        if (!string.IsNullOrWhiteSpace(offset.StationId) && !StationOffsets.ContainsKey(offset.StationId))
                        {
                            StationOffsets.Add(offset.StationId, offset.Value);
                            StationAmplitudes.Add(offset.StationId, offset.AbsPeakAmplitude);
                        }
                    }
                }
            }
            else
            {
                Offsets = RawPacketUtil.FormatOffsetAbsPeakAmplitude(ltgPortion.OffsetList);
            }
            FlashId = flashId;
        }

        [JsonProperty(PropertyName = "timeStamp")]
        public string TimeStamp { get; set; }

        [JsonProperty(PropertyName = "amplitude")]
        public double Amplitude { get; set; }

        [JsonProperty(PropertyName = "type")]
        public FlashType Type { get; set; }

        [JsonProperty(PropertyName = "confidence")]
        public int Confidence { get; set; }

        [JsonProperty(PropertyName = "solution", NullValueHandling = NullValueHandling.Ignore)]
        public string Solution { get; set; }

        [JsonProperty(PropertyName = "errorEllipse", NullValueHandling = NullValueHandling.Ignore)]
        public ErrorEllipse ErrorEllipse { get; set; }

        [JsonProperty(PropertyName = "offsets", NullValueHandling = NullValueHandling.Ignore)]
        public string Offsets { get; set; }

        [JsonProperty(PropertyName = "stationOffsets", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, double> StationOffsets { get; set; }

        [JsonProperty(PropertyName = "stationAmplitudes", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, double> StationAmplitudes { get; set; }

        [JsonProperty(PropertyName = "flashId", NullValueHandling = NullValueHandling.Include)]
        public Guid? FlashId { get; set; }

        [JsonIgnore]
        public Guid? FlashGUID { get; set; }

        [JsonProperty(PropertyName = "version", NullValueHandling = NullValueHandling.Include)]
        public string Version { get; set; }

        [JsonProperty(PropertyName = "rejectedReason", NullValueHandling = NullValueHandling.Ignore)]
        public string RejectedReason { get; set; }

        public string GetMetaDataDbString()
        {
            var num = (StationOffsets == null) ? 0 : StationOffsets.Count;

            var extd = new PortionExtendedData
            {
                ErrorEllipse = ErrorEllipse,
                StationOffsets = StationOffsets,
                NumberStations = num,
                Version = Version
            };

            return extd.ToJsonString();
        }
    }
}
