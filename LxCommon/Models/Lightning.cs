﻿using System;
using Newtonsoft.Json;

namespace LxCommon.Models
{
    public abstract class Lightning
    {
        private double _latitude;
        private double _longitude;
        private int _height;

        [JsonProperty(PropertyName = "longitude")]
        public double Longitude
        {
            get { return _longitude; }
            set { _longitude = Math.Round(value, 5, MidpointRounding.ToEven); }
        }

        [JsonProperty(PropertyName = "latitude")]
        public double Latitude
        {
            get { return _latitude; }
            set { _latitude = Math.Round(value, 5, MidpointRounding.ToEven); }
        }

        [JsonProperty(PropertyName = "height")]
        public double Height
        {
            get { return _height; }
            set { _height = Convert.ToInt32(Math.Truncate(value)); }
        }
    }
}
