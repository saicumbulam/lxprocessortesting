﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LxCommon.Models
{
    /// <summary>
    /// Flash is meant to be a light weight class for easy serialization and deserialization of LtgFlash data
    /// </summary>
    public class Flash : Lightning
    {
        private double _minLatitude;
        private double _minLongitude;
        private double _maxLatitude;
        private double _maxLongitude;

        private decimal _durationSeconds;

        public Flash() { }

        public Flash(LTGFlash ltgFlash, bool isTlnSystem)
        {
            TimeStamp = ltgFlash.FlashTime;
            Longitude = ltgFlash.Longitude;
            Latitude = ltgFlash.Latitude;
            Height = ltgFlash.Height;
            Amplitude = ltgFlash.Amplitude;
            Type = ltgFlash.FlashType;
            Confidence = ltgFlash.Confidence;
            Version = ltgFlash.Version;
            NumberSensors = ltgFlash.NumberSensors;
            MinLatitude = ltgFlash.MinLatitude;
            MinLongitude = ltgFlash.MinLongitude;
            MaxLatitude = ltgFlash.MaxLatitude;
            MaxLongitude = ltgFlash.MaxLongitude;
            Source = ltgFlash.Source;
            StartTime = ltgFlash.StartTime;
            EndTime = ltgFlash.EndTime;
            DurationSeconds = ltgFlash.DurationSeconds;
            IcMultiplicity = ltgFlash.IcMultiplicity;
            CgMultiplicity = ltgFlash.CgMultiplicity;
            AgeAtLogTimeSeconds = null;

            if (ltgFlash.FlashPortionList != null)
            {
                Portions = new List<Portion>();
                foreach (LTGFlashPortion ltgPortion in ltgFlash.FlashPortionList)
                {
                    Portions.Add(new Portion(ltgPortion, null, isTlnSystem));
                }
            }
        }

        public Flash(LTGFlash ltgFlash, bool isTlnSystem, DateTime logTime)
            : this(ltgFlash, isTlnSystem)
        {
            var flashTime = LtgTimeUtils.GetUtcDateTimeFromString(TimeStamp);
            AgeAtLogTimeSeconds = (int)(logTime - flashTime).TotalSeconds;
        }


        [JsonProperty(PropertyName = "timeStamp")]
        public string TimeStamp { get; set; }
        
        [JsonProperty(PropertyName = "ageAtLogTimeSeconds", NullValueHandling = NullValueHandling.Ignore)]
        public int? AgeAtLogTimeSeconds { get; set; }

        [JsonProperty(PropertyName = "amplitude")]
        public double Amplitude { get; set; }

        [JsonProperty(PropertyName = "type")]
        public FlashType Type { get; set; }

        [JsonProperty(PropertyName = "confidence")]
        public int Confidence { get; set; }

        [JsonProperty(PropertyName = "portions")]
        public List<Portion> Portions { get; set; }

        [JsonProperty(PropertyName = "id", NullValueHandling = NullValueHandling.Ignore)]
        public long? Id { get; set; }

        [JsonProperty(PropertyName = "version", NullValueHandling = NullValueHandling.Include)]
        public string Version { get; set; }

        [JsonProperty(PropertyName = "numberSensors")]
        public int NumberSensors { get; set; }

        [JsonProperty(PropertyName = "minLatitude")]
        public double MinLatitude
        {
            get { return _minLatitude; }
            set { _minLatitude = Math.Round(value, 5, MidpointRounding.ToEven); }
        }

        [JsonProperty(PropertyName = "minLongitude")]
        public double MinLongitude
        {
            get { return _minLongitude; }
            set { _minLongitude = Math.Round(value, 5, MidpointRounding.ToEven); }
        }

        [JsonProperty(PropertyName = "maxLatitude")]
        public double MaxLatitude
        {
            get { return _maxLatitude; }
            set { _maxLatitude = Math.Round(value, 5, MidpointRounding.ToEven); }
        }

        [JsonProperty(PropertyName = "maxLongitude")]
        public double MaxLongitude
        {
            get { return _maxLongitude; }
            set { _maxLongitude = Math.Round(value, 5, MidpointRounding.ToEven); }
        }

        [JsonProperty(PropertyName = "source")]
        public string Source { get; set; }

        [JsonProperty(PropertyName = "startTime")]
        public string StartTime { get; set; }

        [JsonProperty(PropertyName = "endTime")]
        public string EndTime { get; set; }

        [JsonProperty(PropertyName = "durationSeconds")]
        public decimal DurationSeconds
        {
            get { return _durationSeconds; }
            set { _durationSeconds = Math.Round(value, 9, MidpointRounding.ToEven); }
        }

        [JsonProperty(PropertyName = "icMultiplicity")]
        public int IcMultiplicity { get; set; }

        [JsonProperty(PropertyName = "cgMultiplicity")]
        public int CgMultiplicity { get; set; }

        public string GetMetaDataDbString()
        {
            var fextd = new FlashExtendedData
            {
                CgMultiplicity = CgMultiplicity,
                IcMultiplicity = IcMultiplicity,
                StartTime = StartTime,
                EndTime = EndTime,
                DurationSeconds = DurationSeconds,
                MinLatitude = MinLatitude,
                MinLongitude = MinLongitude,
                MaxLatitude = MaxLatitude,
                MaxLongitude = MaxLongitude,
                NumberSensors = NumberSensors,
                Source = Source,
                Version = Version
            };

            return fextd.ToJsonString();
        }
    }
}
