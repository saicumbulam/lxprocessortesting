﻿using System.Collections.Generic;

using Newtonsoft.Json;

namespace LxCommon.Models
{
    [JsonObject("ee")]
    public class ErrorEllipse
    {
        [JsonProperty(PropertyName = "maj")]
        public double MajorAxis { get; set; }

        [JsonProperty(PropertyName = "min")]
        public double MinorAxis { get; set; }

        [JsonProperty(PropertyName = "b")]
        public double MajorAxisBearing { get; set; }

        public string ToJsonString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
