﻿namespace LxCommon.Config
{
    internal class AppConfig : Aws.Core.Utilities.Config.AppConfig
    {
        public static int DbCommandTimeout
        {
            get { return Get("DbCommandTimeout", 5); }
        }

        public static ProcessorType ProcessorType
        {
            get { return (ProcessorType)Get<int>("ProcessorType", 0); }
        }

        public static string DataFeedClientConnectionString
        {
            get { return Get("DataFeed.Client.ConnectionString", "{\"p\":\"endev\",\"v\":2,\"f\":2,\"t\":1,\"class\":2}"); }
        }

        public static string DataFeedClientServer
        {
            get { return Get("DataFeed.Client.Server", "lx.datamart.earthnetworks.com"); }
        }

        public static int DataFeedClientPort
        {
            get { return Get("DataFeed.Client.Port", 80); }
        }

        public static int DataFeedClientPacketLength
        {
            get { return Get("DataFeed.Client.PacketLength", 26); }
        }

        public static int DataFeedMaxSecondsSinceLastFlash
        {
            get { return Get("DataFeed.Client.MaxSecondsSinceLastFlash", 30); }
        }

        public static int DatafeedRateSampleSize
        {
            get { return Get("DataFeed.Client.DatafeedRateSampleSize", 5); }
        }

        public static string ExportFileDirectory
        {
            get { return Get<string>("ExportFileDirectory", null); }
        }

        public static int MaxSendWorkerQueueSize 
        {
            get { return Get<int>("TcpCommunication.MaxSendWorkerQueueSize", 10000); }
        }
    }
}
