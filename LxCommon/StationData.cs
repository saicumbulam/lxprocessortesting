﻿using System;
using System.Collections.Generic;

namespace LxCommon
{
    public class StationData
    {
        private string _stationId = string.Empty;
        private double _latitude = 0.0;
        private double _longitude = 0.0;
        private double _altitude;  // m
        private short _qcFlag = 0;
        private bool _active = true; // Whether this station will be used in the process, in LF only the sensors in the global network is used
        private bool _isGpsValid = true;
        private string _networkType = "";  
        private HashSet<string> _networks;

        private SensorVersion _majorVersion = SensorVersion.ID_VERSION_SENSOR_9_MAJOR;
        private SensorVersion _minorVersion = SensorVersion.ID_VERSION_SENSOR_9_MINOR0;

        private string _antennaVersion = string.Empty;
        private StationCalibrationData _calibrationData = new StationCalibrationData();
        private Dictionary<string, double> _distanceTable = new Dictionary<string, double>(); // Table for distances to other sensors

        public Dictionary<string, double> DistanceInMicrosecondTable
        {
            get { return _distanceTable; }
            set { _distanceTable = value; }
        }

        private static double AngleToRadian = Math.PI / 180.0;

        // in earth centerd light microseconds
        private double[] _xyz = new double[3];

        public StationData()
        {
            Attenuation = 0;
            _networks = new HashSet<string>();
        }

        public StationData(string id, double lat, double lon, double alt, short qcflag)
        {
            Attenuation = 0;
            if (id == null)
                return;

            _networks = new HashSet<string>();

            _stationId = id.Trim();
            _latitude = lat;
            _longitude = lon;
            _altitude = alt;
            _qcFlag = qcflag;
            _isGpsValid = HasGoodGpsData();
            _xyz = XyzFromLatLonHeight(lat, lon, alt);
        }

        public void UpdateCartesianWithRoundEarth()
        {
            _xyz = XyzFromLatLonHeightRoundEarth(_latitude, _longitude, _altitude);
        }

        public void UpdateCartesian()
        {
            _xyz = XyzFromLatLonHeight(_latitude, _longitude, _altitude);
        }

        // Check whether the GPS data is good
        private bool HasGoodGpsData()
        {
            // Make sure it is active
            if (_latitude == 0.0 && _longitude == 0.0)
                return false;
            return true;

        }

        // height in meters, lat lon in degrees, xyz in earth centered light microseconds
        private static double[] XyzFromLatLonHeight(double lat, double lon, double height)
        {
            double x, y, z;
            XyzFromLatLonHeight(lat, lon, height, out x, out y, out z);
            double[] xyz = null;
            if (x != double.MinValue && y != double.MinValue && z != double.MinValue)
            {
                xyz = new[] { x, y, z };
            }
            return xyz;
        }

        public static void XyzFromLatLonHeight(double lat, double lon, double height, out double x, out double y, out double z)
        {
            try
            {
                lat *= Math.PI / 180;
                lon *= Math.PI / 180;
                // height *= 1000;

                double earthRadiusEquatorial = 6378137; // #defined by WGS84, do not replace with more accurate value
                double earthRadiusPolar = 6356752.31424518; //#defined by WGS84
                //double LtgConstants.LightspeedInMetersPerMicrosecond = 299.792458;
                double a = earthRadiusEquatorial / LtgConstants.LightspeedInMetersPerMicrosecond;
                double b = earthRadiusPolar / LtgConstants.LightspeedInMetersPerMicrosecond;
                double rr2 = (b / a) * (b / a);
                double e = Math.Sqrt(1 - rr2);
                double h = height / LtgConstants.LightspeedInMetersPerMicrosecond;
                double tmp = e * Math.Sin(lat);
                double tmp2 = Math.Sqrt(1 - tmp * tmp);
                double n = 100000000000.0;
                if (tmp2 > LtgConstants.MachineEpsilon)
                    n = a / tmp2;

                // n, h and rr2 are all constants, which will be added here so to avoid the calculations
                x = (n + h) * Math.Cos(lat) * Math.Cos(lon);
                y = (n + h) * Math.Cos(lat) * Math.Sin(lon);
                z = (rr2 * n + h) * Math.Sin(lat);
            }
            catch (Exception)
            {
                x = double.MinValue;
                y = double.MinValue;
                z = double.MinValue;
            }
        }

        //This function takes the xyz coordinates and converts that to lat, lon and height values.
        //height in meters, lat lon in degrees, xyz in earth centered light microseconds"
        public static double[] LatLonHeightFromXyz(double[] xyz)
        {
            double x = xyz[0];
            double y = xyz[1];
            double z = xyz[2];

            double earthRadiusEquatorial = 6378137;// #defined by WGS84, do not replace with more accurate value
            double earthRadiusPolar = 6356752.31424518;// #defined by WGS84
            //double LtgConstants.LightspeedInMetersPerMicrosecond = 299.792458;
            double a = earthRadiusEquatorial / LtgConstants.LightspeedInMetersPerMicrosecond;
            double b = earthRadiusPolar / LtgConstants.LightspeedInMetersPerMicrosecond;
            double rr2 = (b / a) * (b / a);
            double e = Math.Sqrt(1 - rr2);
            double lon = Math.Atan2(y, x);
            double rxy = Math.Sqrt(x * x + y * y);
            double lat = Math.Atan2(z, rxy / rr2);

            double v, lastlat;
            while (true)
            {
                lastlat = lat;
                double tm = e * Math.Sin(lat);
                v = a / Math.Sqrt(1 - tm * tm);
                lat = Math.Atan2(Math.Sin(lat) * v * e * e + z, rxy);
                if (Math.Abs(lastlat - lat) < 1.0e-7)
                    break;
            }

            double height = rxy / Math.Cos(lat) - v; //Microsecond
            //height *= (LtgConstants.LightspeedInMetersPerMicrosecond / 1000);
            height *= LtgConstants.LightspeedInMetersPerMicrosecond;

            lat *= (180 / Math.PI);
            lon *= (180 / Math.PI);
            double[] latlonhi = { lat, lon, height };
            return latlonhi;
        }

        // The earth is round here height in meters, lat lon in degrees, xyz in earth centered light microseconds
        public static double[] XyzFromLatLonHeightRoundEarth(double lat, double lon, double height)
        {
            lat *= Math.PI / 180;
            lon *= Math.PI / 180;

            double earthRadiusEquatorial = 6378137; // #defined by WGS84, do not replace with more accurate value
            double earthRadiusPolar = 6356752.31424518; //#defined by WGS84
            //double LtgConstants.LightspeedInMetersPerMicrosecond = 299.792458;
            double a = earthRadiusEquatorial / LtgConstants.LightspeedInMetersPerMicrosecond;
            double b = a; // earthRadiusPolar / LtgConstants.LightspeedInMetersPerMicrosecond;
            double rr2 = (b / a) * (b / a);
            double e = Math.Sqrt(1 - rr2);
            double h = height / LtgConstants.LightspeedInMetersPerMicrosecond;
            double tmp = e * Math.Sin(lat);
            double tmp2 = Math.Sqrt(1 - tmp * tmp);
            double n = 100000000000.0;
            if (tmp2 > LtgConstants.MachineEpsilon)
                n = a / tmp2;

            // n, h and rr2 are all constants, which will be added here so to avoid the calculations
            double[] xyz = new double[3];
            xyz[0] = (n + h) * Math.Cos(lat) * Math.Cos(lon);
            xyz[1] = (n + h) * Math.Cos(lat) * Math.Sin(lon);
            xyz[2] = (rr2 * n + h) * Math.Sin(lat);

            //double[] test = LatLonHeightFromXyz(xyz);
            return xyz;
        }

        //lat lon in degrees, xyz in earth centered light microseconds"
        public static double[] LatLonFromXyz(Vector xyz)
        {
            double x = xyz.x;
            double y = xyz.y;
            double z = xyz.z;

            double lon = Math.Atan2(y, x);
            double rxy = Math.Sqrt(x * x + y * y);
            double lat = Math.Atan2(z, rxy);

            lat *= (180 / Math.PI);
            lon *= (180 / Math.PI);
            double[] latlonhi = { lat, lon, 0 };
            return latlonhi;
        }



        // Calculate the distance for this station to the location in microsecond
        public double DistanceFromLatLongHeightInMicrosecond(double lat, double lon, double height)
        {
            double x, y, z;
            XyzFromLatLonHeight(lat, lon, height, out x, out y, out z);

            double distance = 0;

            double value = x - _xyz[0];
            distance += value * value;

            value = y - _xyz[1];
            distance += value * value;

            value = z - _xyz[2];
            distance += value * value;

            distance = Math.Sqrt(distance);

            return distance;
        }


        // Calculate the distance for this station to the location in km?
        public double DistanceFromLatLongHeightInKM(double lat, double lon, double alt)
        {
            return (DistanceFromLatLongHeightInMicrosecond(lat, lon, alt) * LtgConstants.LightspeedInKMPerMicrosecond);
        }

        // microsecond
        public static double DistanceInMicrosecond(double lat1, double lon1, double alt1, double lat2, double lon2, double alt2)
        {
            double[] location1 = XyzFromLatLonHeight(lat1, lon1, alt1);
            double[] location2 = XyzFromLatLonHeight(lat2, lon2, alt2);
            double distance = 0, value = 0;
            for (int i = 0; i < 3; i++)
            {
                value = location1[i] - location2[i];
                distance += value * value;
            }
            return Math.Sqrt(distance);

        }

        // distance between two points on earch, in microsecond, considering the curvature of the earth
        public static double DistanceWithCurvatureInMicrosecond(double lat1, double lon1, double lat2, double lon2)
        {
            double lat_r1 = lat1 * Math.PI / 180;
            double lon_r1 = lon1 * Math.PI / 180;
            double lat_r2 = lat2 * Math.PI / 180;
            double lon_r2 = lon2 * Math.PI / 180;
            double dist = LtgConstants.EarthRadiusEquatorialInLightMicroseconds *
                Math.Acos(Math.Sin(lat_r1) * Math.Sin(lat_r2) + Math.Cos(lat_r1) * Math.Cos(lat_r2) * Math.Cos(lon_r1 - lon_r2));
            return dist;
        }
        public static double DistanceWithCurvatureInKM(double lat1, double lon1, double lat2, double lon2)
        {
            return (DistanceWithCurvatureInMicrosecond(lat1, lon1, lat2, lon2) * 0.299792458);
        }

        // Km
        public static double DistanceInKM(double lat1, double lon1, double alt1, double lat2, double lon2, double alt2)
        {
            return (DistanceInMicrosecond(lat1, lon1, alt1, lat2, lon2, alt2) * 0.299792458);
        }

        public double DistanceFromStationInMicrosecond(StationData station)
        {
            return DistanceFromLatLongHeightInMicrosecond(station.Latitude, station.Longitude, station.Altitude);
        }

        public double DistanceFromStationInKM(StationData station)
        {
            return DistanceFromLatLongHeightInKM(station.Latitude, station.Longitude, station.Altitude);
        }


        // Return the sweep angle from one point on great circle
        public static double LatitudeAngleFromDistance(double orginLat, double originLon, double distanceInMeter)
        {
            return (180.0 * distanceInMeter) / (LtgConstants.EarthRadiusPolarInMeters * Math.PI);
        }

        // Return the sweep angle from one point on small circle
        public static double LongitudeAngleFromDistance(double orginLat, double originLon, double distanceInMeter)
        {
            double smallCirclRadius = Math.Cos(orginLat * Math.PI / 180) * LtgConstants.EarthRadiusEquatorialInMeters;

            return (180.0 * distanceInMeter) / (smallCirclRadius * Math.PI);
        }

        // Calculate the initial bering from (lat,lon) to the sensor
        public double BeringFromALocation(double lat, double lon)
        {
            try
            {
                double lat1 = lat * AngleToRadian;
                double lon1 = lon * AngleToRadian;
                double lat2 = Latitude * AngleToRadian;
                double lon2 = Longitude * AngleToRadian;
                double dLon = lon2 - lon1;
                double bering = Math.Atan2(Math.Sin(dLon) * Math.Cos(lat2), Math.Cos(lat1) * Math.Sin(lat2) - Math.Sin(lat1) * Math.Cos(lat2) * Math.Cos(dLon));
                if (bering < 0)
                    bering += 2 * Math.PI;
                return bering;

            }
            catch (Exception)
            {
                return 0.0;
            }
        }


        // Return the angle from 0 to 2PI
        public double AngleFromALocation(double lat, double lon, double alt)
        {
            return BeringFromALocation(lat, lon);
            /*
            double[] location = XyzFromLatLonHeight(lat, lon, alt);
            double dx = _xyz[0] - location[0];
            double dy = _xyz[1] - location[1];
            if (dx == 0.0)
                return 0.0;

            double angleInRadiant = Math.Atan2(dy, dx);
            if (angleInRadiant < 0)
                angleInRadiant += 2 * Math.PI;
            return angleInRadiant;
             */
        }



        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
        /* Vincenty Inverse Solution of Geodesics on the Ellipsoid (c) Chris Veness 2002-2010             */
        /*                                                                                                */
        /* from: Vincenty inverse formula - T Vincenty, "Direct and Inverse Solutions of Geodesics on the */
        /*       Ellipsoid with application of nested equations", Survey Review, vol XXII no 176, 1975    */
        /*       http://www.ngs.noaa.gov/PUBS_LIB/inverse.pdf                                             */
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */

        /**
         * Calculates geodetic distance between two points specified by latitude/longitude using 
         * Vincenty inverse formula for ellipsoids
         *
         * @param   {Number} lat1, lon1: first point in decimal degrees
         * @param   {Number} lat2, lon2: second point in decimal degrees
         * @returns (Number} distance in metres between points
         */
        public static double VincentyDistanceInMeter(double lat1, double lon1, double lat2, double lon2)
        {
            double a = 6378137, b = 6356752.314245, f = 1 / 298.257223563;  // WGS-84 ellipsoid params
            // Convert to Radian from degrees
            double degreeToRadian = Math.PI / 180.0;
            lat1 *= degreeToRadian;
            lat2 *= degreeToRadian;
            lon1 *= degreeToRadian;
            lon2 *= degreeToRadian;

            double L = lon2 - lon1;
            double U1 = Math.Atan((1 - f) * Math.Tan(lat1));
            double U2 = Math.Atan((1 - f) * Math.Tan(lat2));
            double sinU1 = Math.Sin(U1), cosU1 = Math.Cos(U1);
            double sinU2 = Math.Sin(U2), cosU2 = Math.Cos(U2);

            double lambda = L, lambdaP, iterLimit = 100;
            double sinLambda, cosLambda, sinSigma, cosSigma, sigma, sinAlpha, cosSqAlpha, cos2SigmaM;
            do
            {
                sinLambda = Math.Sin(lambda);
                cosLambda = Math.Cos(lambda);
                sinSigma = Math.Sqrt((cosU2 * sinLambda) * (cosU2 * sinLambda) +
                  (cosU1 * sinU2 - sinU1 * cosU2 * cosLambda) * (cosU1 * sinU2 - sinU1 * cosU2 * cosLambda));
                if (sinSigma == 0) return 0;  // co-incident points
                cosSigma = sinU1 * sinU2 + cosU1 * cosU2 * cosLambda;
                sigma = Math.Atan2(sinSigma, cosSigma);
                sinAlpha = cosU1 * cosU2 * sinLambda / sinSigma;
                cosSqAlpha = 1 - sinAlpha * sinAlpha;
                cos2SigmaM = cosSigma - 2 * sinU1 * sinU2 / cosSqAlpha;
                if (cos2SigmaM == double.NaN)
                    cos2SigmaM = 0;  // equatorial line: cosSqAlpha=0 (§6)
                double C = f / 16 * cosSqAlpha * (4 + f * (4 - 3 * cosSqAlpha));
                lambdaP = lambda;
                lambda = L + (1 - C) * f * sinAlpha *
                  (sigma + C * sinSigma * (cos2SigmaM + C * cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM)));
            } while (Math.Abs(lambda - lambdaP) > 1e-7 && --iterLimit > 0);

            //if (iterLimit == 0)
            //    return double.NaN; // formula failed to converge

            double uSq = cosSqAlpha * (a * a - b * b) / (b * b);
            double A = 1 + uSq / 16384 * (4096 + uSq * (-768 + uSq * (320 - 175 * uSq)));
            double B = uSq / 1024 * (256 + uSq * (-128 + uSq * (74 - 47 * uSq)));
            double deltaSigma = B * sinSigma * (cos2SigmaM + B / 4 * (cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM) -
              B / 6 * cos2SigmaM * (-3 + 4 * sinSigma * sinSigma) * (-3 + 4 * cos2SigmaM * cos2SigmaM)));
            double s = b * A * (sigma - deltaSigma);
            return s;
        }

        // use the no-vacuum light speed
        public static double VincentyDistanceInMicrosecond(double lat1, double lon1, double lat2, double lon2)
        {
            double distanceInMeter = VincentyDistanceInMeter(lat1, lon1, lat2, lon2);
            return distanceInMeter / LtgConstants.LightspeedInMetersPerMicrosecond;
        }

        // Simpified Ellipsoidal distance calculation
        public static double Wgs84DistanceInMeter(double lat1, double lon1, double lat2, double lon2) 
        {
            double earthRadiusEquatorial = 6378137;// #defined by WGS84, do not replace with more accurate value
            double earthRadiusPolar = 6356752.31424518;// #defined by WGS84
            double a = earthRadiusEquatorial;
            double b = earthRadiusPolar;
            double tmp = (a - b) / (a + b);
            double zz = 3 * tmp * tmp;
            double poleToPoleDistance = Math.PI * (a + b) * (1 + zz / (10 + Math.Sqrt(4 - zz))) / 2; // #approximate, good to about 6 places
            double hf = (1 - b / a) / 2;
            double degree_to_radian = Math.PI / 180.0;
            double y0 = lat1 * degree_to_radian;
            double y1 = lat2 * degree_to_radian;
            double x0 = lon1 * degree_to_radian;
            double x1 = lon2 * degree_to_radian;
            tmp = Math.Sin((y0 + y1) / 2);
            double sf = tmp * tmp;
            double cf = 1 - sf;
            tmp = Math.Sin((y0 - y1) / 2);
            double sg = tmp * tmp;
            double cg = 1 - sg;
            tmp = Math.Sin((x0 - x1) / 2);
            double sh = tmp * tmp;
            double ch = 1 - sh;
            double ss = sg * ch + cf * sh;
            double cc = cg * ch + sf * sh;
            double oo = Math.Atan2(Math.Sqrt(ss), Math.Sqrt(cc));
            double aa = (sg * cf) / ss + (sf * cg) / cc;
            double bb = (sg * cf) / ss - (sf * cg) / cc;
            double dd = oo / Math.Sqrt(ss * cc);
            double d2 = ss - cc;
            double dp = dd + 6 / dd;
            double e1 = hf * (-aa - 3.0 * bb / dd);
            double e2 = hf * hf * ((-dp * bb + (-3.75 + d2 * (dd + 3.75 / dd)) * aa + 4.0 - d2 * dd) * aa - (7.5 * d2 * bb / dd - dp) * bb);
            return 2 * oo * earthRadiusEquatorial * (1 + e1 + e2);
        }

        public static double Wgs84DistanceInMicrosecond(double lat1, double lon1, double lat2, double lon2)
        {
            double distanceInMeter = Wgs84DistanceInMeter(lat1, lon1, lat2, lon2);
            return distanceInMeter / LtgConstants.LightspeedInMetersPerMicrosecond;
        }


        #region Properties

        public string NetworkType
        {
            get { return _networkType; }
            set { _networkType = value; }
        }

        public string StationID
        {
            get { return _stationId; }
            set { _stationId = value; }
        }

        public string AntennaVersion
        {
            get
            {
                return _antennaVersion;
            }
            set
            {
                _antennaVersion = value;
            }
        }

        public double Latitude
        {
            get { return _latitude; }
            set { _latitude = value; }
        }

        public double Longitude
        {
            get { return _longitude; }
            set { _longitude = value; }
        }

        public double Altitude
        {
            get { return _altitude; }
            set { _altitude = value; }
        }

        public short QcFlag
        {
            get
            {
                //if (!HasGoodGpsData())
                //    return 0;

                return _qcFlag;

            }
            set { _qcFlag = value; }
        }
        public double[] Xyz
        {
            get
            {
                return _xyz;
            }
        }

        public void SetVersion(string versionString)
        {
            if (versionString.Contains("."))
            {
                _majorVersion = (SensorVersion)Convert.ToInt16(versionString.Substring(0, versionString.IndexOf('.')));
                _minorVersion = (SensorVersion)Convert.ToInt16(versionString.Substring(versionString.IndexOf('.') + 1));

            }
            else
            {
                _majorVersion = (SensorVersion)Convert.ToInt16(versionString);
                _minorVersion = (SensorVersion)0;
            }
        }

        public StationCalibrationData CalibrationData
        {
            get { return _calibrationData; }
            set { _calibrationData = value; }
        }

        public int Attenuation { get; set; }

        public bool Active
        {
            get { return _active; }
            set { _active = value; }
        }

        public SensorVersion MajorVersion
        {
            get { return _majorVersion; }
            set { _majorVersion = value; }
        }

        public SensorVersion MinorVersion
        {
            get { return _minorVersion; }
            set { _minorVersion = value; }
        }

        public bool IsGpsValid
        {
            get { return _isGpsValid; }
            set { _isGpsValid = value; }
        }

        public HashSet<string> Networks
        {
            get { return _networks; }
            set { _networks = value; }
        }

        #endregion
    }
}
