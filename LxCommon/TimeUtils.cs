using System;

namespace LxCommon
{
    public abstract class LtgTimeUtils
    {
        private static readonly DateTime UnixEpochStart = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);

        // Get the DateTime object from YYYY-MM-DDThh:mm:ss
        static public DateTime GetUtcDateTimeFromString(string sampleTime)
        {
            DateTime dt;
            DateTime.TryParse(sampleTime, out dt);
            if (dt.Kind != DateTimeKind.Utc)
                dt = DateTime.SpecifyKind(dt, DateTimeKind.Utc);
            
            return dt;
        }

        // Calculate the total microseconds from mm:ss.xxxxxxxxx
        static public Int64 GetMicrosecondsFromString(string sampleTime)
        {
            try
            {
                Int64 seconds = //60 * Convert.ToInt64(sampleTime.Substring(14, 2)) + 
                    Convert.ToInt32(sampleTime.Substring(17, 2));
                string subString = sampleTime.Substring(20, 6);
                Int64 microsecond = Convert.ToInt64(subString);
                Int64 totalMs = (seconds * 1000000) + microsecond;
                return totalMs;
            }
            catch (Exception ex)
            {
                return 0;

            }
        }

        // Calculate the total nanoseconds in the fraction of the second from 0.xxxxxxxxx
        // 
        static public int GetNanosecondsFromString(string sampleTime)
        {
            try
            {
                string subString = sampleTime.Substring(20, 9);
                int nanoseconds = Convert.ToInt32(subString);
                return nanoseconds;
            }
            catch (Exception ex)
            {
                return 0;

            }
        }

        static public DateTime GetUtcDateTimeFrom1970Seconds(int secondsFrom1970)
        {
            return UnixEpochStart.AddSeconds(secondsFrom1970);
        }

        static public string GetDateTimeStringFrom1970Seconds(int secondsFrom1970)
        {
            DateTime dateTime = GetUtcDateTimeFrom1970Seconds(secondsFrom1970);
            return GetDateTimeString(dateTime);
        }


        static public string GetStringFrom1970Seconds(int secondsFrom1970)
        {
            DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dateTime = dateTime.AddSeconds(secondsFrom1970);
            return dateTime.ToString("s");
        }

        static public string GetTimeStringFromSecondsSince1970(double seconds)
        {
            DateTime dateTime1970 = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            int second = (int)seconds;
            DateTime dateTime = dateTime1970.AddSeconds(second);
            int nanoSeconds = (int)(1e9 * (seconds - second));
            string timeString = string.Format("{0:D4}-{1:D2}-{2:D2}T{3:D2}:{4:D2}:{5:D2}.{6:D9}",
                dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute, dateTime.Second, nanoSeconds);
            return timeString;
        }

      

        static public string GetDateTimeString(DateTime dateTime)
        {
            string timeString = "";
            timeString = string.Format("{0:D4}-{1:D2}-{2:D2}T{3:D2}:{4:D2}:{5:D2}.{6:D3}000000",
                dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute, dateTime.Second, dateTime.Millisecond);
            return timeString;
        }


        
        static public int GetSecondsSince1970FromDateTime(DateTime dateTime)
        {

            TimeSpan span = dateTime - UnixEpochStart;
            return (int)span.TotalSeconds;
        }

        private static readonly DateTime StartTime2006 = new DateTime(2006, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
        static public int GetSecondsSince2006FromDateTime(DateTime dateTime)
        {

            TimeSpan span = dateTime - StartTime2006;
            return (int)span.TotalSeconds;
        }

        // YYYY-MM-DDThh:mm:ss.xxxxxxxxx
        // return the value of .xxxxxxxxx
        static private double GetSubSecondsFromTimeString(string timeString)
        {
            string subString = timeString.Substring(19);
            double subSeconds = Convert.ToDouble(subString);
            return subSeconds;
        }

        // Get the seconds with 9 digits after the decimal points
        static public double GetSecondsSince1970FromTimeString(string timeString)
        {
            DateTime utcDateTime = LtgTimeUtils.GetUtcDateTimeFromString(timeString);
            int seconds = LtgTimeUtils.GetSecondsSince1970FromDateTime(utcDateTime);
            double subSeconds = LtgTimeUtils.GetSubSecondsFromTimeString(timeString);
            subSeconds += seconds;
            return subSeconds;
        }


        static public int GetSecondsSince1970FromTimeStringYYYYMMDDhhmm(string timeString)
        {
            DateTime utcDateTime = LtgTimeUtils.GetUtcDateTimeFromStringYYYYMMDDhhmm(timeString);
            int seconds = LtgTimeUtils.GetSecondsSince1970FromDateTime(utcDateTime);
            return seconds;
        }

        static public int GetSecondsSince1970FromTimeStringYYYYMMDDhh(string timeString)
        {
            DateTime utcDateTime = LtgTimeUtils.GetUtcDateTimeFromStringYYYYMMDDhh(timeString);
            int seconds = LtgTimeUtils.GetSecondsSince1970FromDateTime(utcDateTime);
            return seconds;
        }

        static public DateTime GetUtcDateTimeFromStringYYYYMMDDhh(string timeString)
        {
            DateTime dateTime = new DateTime(
            Convert.ToInt32(timeString.Substring(0, 4)),
            Convert.ToInt32(timeString.Substring(4, 2)),
            Convert.ToInt32(timeString.Substring(6, 2)),
            Convert.ToInt32(timeString.Substring(8, 2)),
            0, // Minute
            0, 0);

            return dateTime;
        }

        static public DateTime GetUtcDateTimeFromStringYYYYMMDDhhmm(string timeString)
        {
            DateTime dateTime = new DateTime(
            Convert.ToInt32(timeString.Substring(0, 4)),
            Convert.ToInt32(timeString.Substring(4, 2)),
            Convert.ToInt32(timeString.Substring(6, 2)),
            Convert.ToInt32(timeString.Substring(8, 2)),
            Convert.ToInt32(timeString.Substring(10, 2)),
            0, 0);

            return dateTime;


        }


        // Get the seconds with 9 digits after the decimal points
        // the second is the second from 2006.1.1 00:00 UTC
        // the subseconds are the 9 digital decimal points from the time string
        static public double GetSecondsSince2006FromTimeString(string timeString)
        {
            DateTime utcDateTime = LtgTimeUtils.GetUtcDateTimeFromString(timeString);
            int seconds = LtgTimeUtils.GetSecondsSince2006FromDateTime(utcDateTime);
            double subSeconds = LtgTimeUtils.GetSubSecondsFromTimeString(timeString);
            subSeconds += seconds;
            return subSeconds;
        }

        static public string GetTimeStringFromSecondsSince2006(double seconds)
        {
            int second = (int)seconds;
            DateTime dateTime = StartTime2006.AddSeconds(second);
            int nanoSeconds = (int)(1e9 * (seconds - second));
            string timeString = string.Format("{0:D4}-{1:D2}-{2:D2}T{3:D2}:{4:D2}:{5:D2}.{6:D9}",
                dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute, dateTime.Second, nanoSeconds);
            return timeString;
        }

        // Subtract some microseconds from a time string
        static public string GetTimeStringAddMicroseconds(string timeString, double microseconds)
        {
            double seconds = LtgTimeUtils.GetSecondsSince2006FromTimeString(timeString) + 1.0e-6 * microseconds;
            return LtgTimeUtils.GetTimeStringFromSecondsSince2006(seconds);
        }

        // t1 - t2, the difference is in microsecond
        public static double SubtractTimeStringInMicroseconds(string t1, string t2)
        {
            double t1_seconds = LtgTimeUtils.GetSecondsSince2006FromTimeString(t1);
            double t2_seconds = LtgTimeUtils.GetSecondsSince2006FromTimeString(t2);
            return (t1_seconds - t2_seconds) * 1e6;
        }

        // t1 - t2, the difference is in microsecond
        public static double SubtractTimeStringInSeconds(string t1, string t2)
        {
            double t1_seconds = LtgTimeUtils.GetSecondsSince2006FromTimeString(t1);
            double t2_seconds = LtgTimeUtils.GetSecondsSince2006FromTimeString(t2);
            return (t1_seconds - t2_seconds);
        }

        public static bool PacketTooNewOrTooOld(long secondFrom1970)
        {
            int curTime = GetSecondsSince1970FromDateTime(DateTime.UtcNow);
            if (secondFrom1970 < curTime - LtgConstants.MaxPacketArriveTimeTooEarlyOrTooLateInSeconds ||
                secondFrom1970 > curTime + LtgConstants.MaxPacketArriveTimeTooEarlyOrTooLateInSeconds)
                return true;
            return false;
        }

        public static bool PacketGoodForProcessing(DateTime utcTimeStamp)
        {
            bool isGood = false;

            TimeSpan span = DateTime.UtcNow - utcTimeStamp;
            if (Math.Abs(span.TotalSeconds) < LtgConstants.MaxPacketArriveTimeTooEarlyOrTooLateInSeconds)
                isGood = true;

            return isGood;
        }
    }
}