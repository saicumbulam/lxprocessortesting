﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace LxCommon
{
    public class RawdataDecompressor
    {
        //mantissas = 4 is old mathos
        // I suggest putting the extra header information in byte 50 (currently reserved).
        // I suggest that this byte be "3" for 3 bit mantissas (new) and "0" for 4 bit mantissas (old).
        // In the decompressor, we change 12 to 13 in floating(), and we change 8 to 4 and 15 to 7 in nextThreshold().
        // Charlie yu need to read rad byte 50 of the header
        //int byte_50 = 3; //0 is old. 3 is new   
        private byte _byte50 = 0;
        private short _lastThresholdCrossed = 0;
        private short _goingDown = 0;
        private short _amplitude = 0;
        private int _index = 0;
        bool _isFinishedProcessing = false;
        private byte[] _rawData = null;

        public RawdataDecompressor()
        {
            _lastThresholdCrossed = 0;
            _goingDown = 0;
            _amplitude = 0;
            _index = 0;
            _isFinishedProcessing = false;
        }

        public void SetRawData(byte[] rawData)
        {
            _isFinishedProcessing = false;
            _lastThresholdCrossed = 0;
            _goingDown = 0;
            _amplitude = 0;
            _index = 0;
            _rawData = rawData;
        }

        public void setMantissas(byte byte50)
        {
            _byte50 = byte50;
        }


        public RawdataDecompressor(byte[] rawData)
        {
            _lastThresholdCrossed = 0;
            _goingDown = 0;
            _amplitude = 0;
            _index = 0;
            _isFinishedProcessing = false;
            _rawData = rawData;
            _byte50 = 0x03;
        }


        public ArrayList DecodeCompress()
        {
            byte b;  // One byte data
            uint tick = 0;
            byte da = 0;
            uint dt = 0;
            ArrayList sampleList = new ArrayList();

            if (_rawData.Length == 0)
                return sampleList;

            while (!_isFinishedProcessing)
            {
                try
                {
                    b = ReadOneByte();
                    int nType = LeadingZeroCount(b);  // leading zero for a byte
                    switch (nType)
                    {
                        case 0:
                            tick += (uint)(b & 0x7f);
                            _amplitude = NextThreshold();
                            _lastThresholdCrossed = _amplitude;
                            break;
                        case 1:
                            da = (byte)(b & 0x3f);
                            dt = ReadOneByte();
                            tick += dt;
                            _amplitude = Peak(da, 6);
                            _lastThresholdCrossed = ComputeLast();
                            _goingDown = (short)(1 - _goingDown);
                            break;
                        case 2:
                            da = (byte)(b & 0x1f);
                            dt = ReadTwoBytes();
                            tick += dt;
                            _amplitude = Peak(da, 5);
                            _lastThresholdCrossed = ComputeLast();
                            _goingDown = (short)(1 - _goingDown);
                            break;
                        case 3:
                            dt = (ushort)(((b & 0x0f) << 8) + ReadOneByte());
                            tick += dt;
                            _amplitude = NextThreshold();
                            _lastThresholdCrossed = _amplitude;
                            break;
                        case 4:
                            dt = (ushort)(((b & 0x7) << 8) + ReadOneByte());
                            tick += dt;
                            _amplitude = (short)-NextThreshold();
                            _lastThresholdCrossed = _amplitude;
                            break;
                        case 5:
                            dt = (ushort)((b & 0x3) + 1);
                            tick += dt;
                            _amplitude = ReadTwoBytesSigned();
                            break;
                        case 6:
                            _goingDown = (byte)(b & 1);
                            _lastThresholdCrossed = ComputeLast();
                            break;
                        default: //7 
                            tick = (uint)((b << 24) + ReadThreeBytes());
                            _amplitude = ReadTwoBytesSigned();
                            break;
                    }

                    if (nType != 6)
                    { //6 is direction
                        Sample sample = new Sample();
                        sample.Tick = tick;
                        sample.E = _amplitude;
                        sampleList.Add(sample);
                    }
                }
                catch (Exception ex)
                {
                    break;
                }
            }
            return sampleList;
        }

        public void Decompress(IList<Sample> sampleList)
        {
            byte b;  // One byte data
            uint tick = 0;
            byte da = 0;
            uint dt = 0;


            if (_rawData.Length != 0)
            {
                while (!_isFinishedProcessing)
                {
                    try
                    {
                        b = ReadOneByte();
                        int nType = LeadingZeroCount(b); // leading zero for a byte
                        switch (nType)
                        {
                            case 0:
                                tick += (uint) (b & 0x7f);
                                _amplitude = NextThreshold();
                                _lastThresholdCrossed = _amplitude;
                                break;
                            case 1:
                                da = (byte) (b & 0x3f);
                                dt = ReadOneByte();
                                tick += dt;
                                _amplitude = Peak(da, 6);
                                _lastThresholdCrossed = ComputeLast();
                                _goingDown = (short) (1 - _goingDown);
                                break;
                            case 2:
                                da = (byte) (b & 0x1f);
                                dt = ReadTwoBytes();
                                tick += dt;
                                _amplitude = Peak(da, 5);
                                _lastThresholdCrossed = ComputeLast();
                                _goingDown = (short) (1 - _goingDown);
                                break;
                            case 3:
                                dt = (ushort) (((b & 0x0f) << 8) + ReadOneByte());
                                tick += dt;
                                _amplitude = NextThreshold();
                                _lastThresholdCrossed = _amplitude;
                                break;
                            case 4:
                                dt = (ushort) (((b & 0x7) << 8) + ReadOneByte());
                                tick += dt;
                                _amplitude = (short) -NextThreshold();
                                _lastThresholdCrossed = _amplitude;
                                break;
                            case 5:
                                dt = (ushort) ((b & 0x3) + 1);
                                tick += dt;
                                _amplitude = ReadTwoBytesSigned();
                                break;
                            case 6:
                                _goingDown = (byte) (b & 1);
                                _lastThresholdCrossed = ComputeLast();
                                break;
                            default: //7 
                                tick = (uint) ((b << 24) + ReadThreeBytes());
                                _amplitude = ReadTwoBytesSigned();
                                break;
                        }

                        if (nType != 6)
                        {
                            //6 is direction
                            Sample sample = new Sample();
                            sample.Tick = tick;
                            sample.E = _amplitude;
                            sampleList.Add(sample);
                        }
                    }
                    catch (Exception ex)
                    {
                        break;
                    }
                }
                
            }
        }

        // Leading zero count for a byte
        private byte LeadingZeroCount(byte b)
        {
            if ((b & 0x80) != 0)
                return 0;
            if ((b & 0x40) != 0)
                return 1;
            if ((b & 0x20) != 0)
                return 2;
            if ((b & 0x10) != 0)
                return 3;
            if ((b & 0x08) != 0)
                return 4;
            if ((b & 0x04) != 0)
                return 5;
            if ((b & 0x02) != 0)
                return 6;
            return 7;
        }


        // Leading zero count for a ushort
        private byte LeadingZeroCount(ushort a)
        {
            if ((a & 0x8000) != 0)
                return 0;
            if ((a & 0x4000) != 0)
                return 1;
            if ((a & 0x2000) != 0)
                return 2;
            if ((a & 0x1000) != 0)
                return 3;
            if ((a & 0x800) != 0)
                return 4;
            if ((a & 0x400) != 0)
                return 5;
            if ((a & 0x200) != 0)
                return 6;
            if ((a & 0x100) != 0)
                return 7;
            if ((a & 0x80) != 0)
                return 8;
            if ((a & 0x40) != 0)
                return 9;
            if ((a & 0x20) != 0)
                return 10;
            return 11;
        }

        private byte ReadOneByte()
        {
            if (_isFinishedProcessing)
                return 0;

            byte r = _rawData[_index];
            _index++;
            if (_index >= _rawData.Length)
                _isFinishedProcessing = true;

            return r;
        }
        private ushort ReadTwoBytes()
        {
            if (_isFinishedProcessing)
                return 0;
            ushort a;
            byte b;
            a = (ushort)(ReadOneByte() << 8);
            b = ReadOneByte();
            ushort r = (ushort)(a + b);
            return r;
        }
        private uint ReadThreeBytes()
        {
            if (_isFinishedProcessing)
                return 0;
            byte b;
            uint a;
            a = (uint)(ReadTwoBytes() << 8);
            b = ReadOneByte();
            uint r = (uint)(a + b);
            return r;
        }
        private short ReadTwoBytesSigned()
        {
            if (_isFinishedProcessing)
                return 0;
            short a;
            a = (short)ReadTwoBytes();
            return a;
        }

        private void Floating(short a, ref short mantissa, ref short exponent)
        {
            // Convert a to ushort
            ushort tmp;

            if (a > 0)
                tmp = (ushort)a;
            else
                tmp = (ushort)(-a);

            //Ben Chnages 08/11/11
            if (_byte50 == 0)
                exponent = (short)(12 - LeadingZeroCount(tmp));
            else
                exponent = (short)(13 - LeadingZeroCount(tmp));

            if (exponent < 0)
                exponent = 0;

            mantissa = (short)(a >> exponent);
        }


        private short Peak(byte da, byte width)
        {
            short exponent = 0, mantissa = 0;
            short nextthreshold = NextThreshold();
            nextthreshold -= _goingDown;
            
            Floating(nextthreshold, ref mantissa, ref exponent);
            
            return (short)((mantissa << width + 1) + (da + da + 1) << exponent >> width + 1);
        }

        private short ComputeLast()
        {
            short a = 0, b = 0;
            Floating((short)(_amplitude - _goingDown), ref a, ref b);
            return (short)((a + _goingDown) << b);
        }
        
        private short NextThreshold()
        {
            short a = 0, b = 0;
            Floating(_lastThresholdCrossed, ref a, ref b);

            // New format
            if (_byte50 == 0x03)
            {
                if (_goingDown != 0)
                {
                    if (a == 4)
                        return (short)(7 << b - 1);
                    else
                        return (short)(a - 1 << b);
                }
                else
                {
                    if (a == -4)
                        return (short)(-7 << b - 1);
                    else
                        return (short)(a + 1 << b);
                }
            }
            else
            { // Old format before 8/23/2011
                if (_goingDown != 0)
                {
                    if (a == 8)
                        return (short)(15 << (b - 1));
                    else
                        return (short)((a - 1) << b);
                }
                else
                {
                    if (a == -8)
                        return (short)(-15 << (b - 1));
                    else
                        return (short)((a + 1) << b);
                }
            }
        }
    }
}
