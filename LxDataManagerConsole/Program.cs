﻿using System;

namespace LxDataManagerConsole
{
    class Program
    {
        private static void Main(string[] args)
        {
            LxDataManager.Program.Start();

            Console.WriteLine("Press \'q\' to quit the program.");
            while (Console.Read() != 'q') { }

            LxDataManager.Program.Stop();
        }
    }
}
