﻿using System;
using LxProcessor.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LxProcessorUnitTest
{
    [TestClass]
    public class MedianSampleFinderUnitTest
    {
        [TestMethod]
        public void TestMethod()
        {
            MedianSampleFinder msf = new MedianSampleFinder(10);
            msf.AddSample(8);
            msf.AddSample(15);
            msf.AddSample(89);
            msf.AddSample(45);
            msf.AddSample(66);

            Assert.AreEqual(msf.Median, 0);
        }

        [TestMethod]
        public void TestMethod2()
        {
            MedianSampleFinder msf = new MedianSampleFinder(10);
            msf.AddSample(8);
            msf.AddSample(15);
            msf.AddSample(89);
            msf.AddSample(45);
            msf.AddSample(66);
            msf.AddSample(78);
            msf.AddSample(100);
            msf.AddSample(1456);
            msf.AddSample(1786);
            msf.AddSample(0);
            msf.AddSample(1);
            
            Assert.AreEqual(msf.Median, 66);
        }


        [TestMethod]
        public void TestMethod3()
        {
            MedianSampleFinder msf = new MedianSampleFinder(10);
            msf.AddSample(8);
            msf.AddSample(15);
            msf.AddSample(89);
            msf.AddSample(45);
            msf.AddSample(66);
            msf.AddSample(78);
            msf.AddSample(100);
            msf.AddSample(1456);
            msf.AddSample(1786);
            msf.AddSample(0);
            msf.AddSample(1);
            msf.AddSample(2);
            Assert.AreEqual(msf.Median, 66);
        }

        [TestMethod]
        public void TestMethod4()
        {
            MedianSampleFinder msf = new MedianSampleFinder(5);
            msf.AddSample(8);
            msf.AddSample(15);
            msf.AddSample(89);
            msf.AddSample(45);
            msf.AddSample(66);
            msf.AddSample(78);
            msf.AddSample(100);
            msf.AddSample(1456);
            msf.AddSample(1786);
            msf.AddSample(0);
            msf.AddSample(1);
            msf.AddSample(2);
            Assert.AreEqual(msf.Median, 100);
        }
    }
}
