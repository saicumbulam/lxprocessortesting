﻿using System;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using LxProcessor.Common;

namespace LxProcessorUnitTest
{
    [TestClass]
    public class ConcurrentSortedDictionaryUnitTest
    {
        [TestMethod]
        public void GetBetweenTestMethod1()
        {
            ConcurrentSortedDictionary<int, string> data = CreateSampleData();

            object result = Helper.RunInstanceMethod(typeof(ConcurrentSortedDictionary<int, string>), "GetBetween", data, new object[] { 0, 32 });

            SortedDictionary<int, Tuple<string, object>> vals = result as SortedDictionary<int, Tuple<string, object>>;

            Assert.IsFalse(vals.ContainsKey(-100));
            Assert.IsFalse(vals.ContainsKey(-50));
            Assert.IsFalse(vals.ContainsKey(-25));
            Assert.IsTrue(vals.ContainsKey(0));
            Assert.IsTrue(vals.ContainsKey(10));
            Assert.IsTrue(vals.ContainsKey(15));
            Assert.IsTrue(vals.ContainsKey(20));
            Assert.IsTrue(vals.ContainsKey(25));
            Assert.IsTrue(vals.ContainsKey(30));
            Assert.IsFalse(vals.ContainsKey(35));
            Assert.IsFalse(vals.ContainsKey(40));
            Assert.IsFalse(vals.ContainsKey(100));
        }

        [TestMethod]
        public void GetBetweenTestMethod2()
        {

            ConcurrentSortedDictionary<int, string> data = CreateSampleData();

            object result = Helper.RunInstanceMethod(typeof(ConcurrentSortedDictionary<int, string>), "GetBetween", data, new object[] { 1, 32 });

            SortedDictionary<int, Tuple<string, object>> vals = result as SortedDictionary<int, Tuple<string, object>>;

            Assert.IsFalse(vals.ContainsKey(-100));
            Assert.IsFalse(vals.ContainsKey(-50));
            Assert.IsFalse(vals.ContainsKey(-25));
            Assert.IsFalse(vals.ContainsKey(0));
            Assert.IsTrue(vals.ContainsKey(10));
            Assert.IsTrue(vals.ContainsKey(15));
            Assert.IsTrue(vals.ContainsKey(20));
            Assert.IsTrue(vals.ContainsKey(25));
            Assert.IsTrue(vals.ContainsKey(30));
            Assert.IsFalse(vals.ContainsKey(35));
            Assert.IsFalse(vals.ContainsKey(40));
            Assert.IsFalse(vals.ContainsKey(100));
        }

        [TestMethod]
        public void GetBetweenTestMethod3()
        {

            ConcurrentSortedDictionary<int, string> data = CreateSampleData();

            object result = Helper.RunInstanceMethod(typeof(ConcurrentSortedDictionary<int, string>), "GetBetween", data, new object[] { -101, 101 });

            SortedDictionary<int, Tuple<string, object>> vals = result as SortedDictionary<int, Tuple<string, object>>;

            Assert.IsTrue(vals.ContainsKey(-100));
            Assert.IsTrue(vals.ContainsKey(-50));
            Assert.IsTrue(vals.ContainsKey(-25));
            Assert.IsTrue(vals.ContainsKey(0));
            Assert.IsTrue(vals.ContainsKey(10));
            Assert.IsTrue(vals.ContainsKey(15));
            Assert.IsTrue(vals.ContainsKey(20));
            Assert.IsTrue(vals.ContainsKey(25));
            Assert.IsTrue(vals.ContainsKey(30));
            Assert.IsTrue(vals.ContainsKey(35));
            Assert.IsTrue(vals.ContainsKey(40));
            Assert.IsTrue(vals.ContainsKey(100));
        }

        [TestMethod]
        public void GetBetweenTestMethod4()
        {

            ConcurrentSortedDictionary<int, string> data = CreateSampleData();

            object result = Helper.RunInstanceMethod(typeof(ConcurrentSortedDictionary<int, string>), "GetBetween", data, new object[] { -300, -140 });

            SortedDictionary<int, Tuple<string, object>> vals = result as SortedDictionary<int, Tuple<string, object>>;

            Assert.IsFalse(vals.ContainsKey(-100));
            Assert.IsFalse(vals.ContainsKey(-50));
            Assert.IsFalse(vals.ContainsKey(-25));
            Assert.IsFalse(vals.ContainsKey(0));
            Assert.IsFalse(vals.ContainsKey(10));
            Assert.IsFalse(vals.ContainsKey(15));
            Assert.IsFalse(vals.ContainsKey(20));
            Assert.IsFalse(vals.ContainsKey(25));
            Assert.IsFalse(vals.ContainsKey(30));
            Assert.IsFalse(vals.ContainsKey(35));
            Assert.IsFalse(vals.ContainsKey(40));
            Assert.IsFalse(vals.ContainsKey(100));
        }

        [TestMethod]
        public void GetBetweenTestMethod5()
        {

            ConcurrentSortedDictionary<int, string> data = CreateSampleData();

            object result = Helper.RunInstanceMethod(typeof(ConcurrentSortedDictionary<int, string>), "GetBetween", data, new object[] { 100, 100 });

            SortedDictionary<int, Tuple<string, object>> vals = result as SortedDictionary<int, Tuple<string, object>>;

            Assert.IsFalse(vals.ContainsKey(-100));
            Assert.IsFalse(vals.ContainsKey(-50));
            Assert.IsFalse(vals.ContainsKey(-25));
            Assert.IsFalse(vals.ContainsKey(0));
            Assert.IsFalse(vals.ContainsKey(10));
            Assert.IsFalse(vals.ContainsKey(15));
            Assert.IsFalse(vals.ContainsKey(20));
            Assert.IsFalse(vals.ContainsKey(25));
            Assert.IsFalse(vals.ContainsKey(30));
            Assert.IsFalse(vals.ContainsKey(35));
            Assert.IsFalse(vals.ContainsKey(40));
            Assert.IsTrue(vals.ContainsKey(100));
        }

        [TestMethod]
        public void GetBetweenTestMethod6()
        {

            ConcurrentSortedDictionary<int, string> data = CreateSampleData();

            object result = Helper.RunInstanceMethod(typeof(ConcurrentSortedDictionary<int, string>), "GetBetween", data, new object[] { 5, 34 });

            SortedDictionary<int, Tuple<string, object>> vals = result as SortedDictionary<int, Tuple<string, object>>;

            Assert.IsFalse(vals.ContainsKey(-100));
            Assert.IsFalse(vals.ContainsKey(-50));
            Assert.IsFalse(vals.ContainsKey(-25));
            Assert.IsFalse(vals.ContainsKey(0));
            Assert.IsTrue(vals.ContainsKey(10));
            Assert.IsTrue(vals.ContainsKey(15));
            Assert.IsTrue(vals.ContainsKey(20));
            Assert.IsTrue(vals.ContainsKey(25));
            Assert.IsTrue(vals.ContainsKey(30));
            Assert.IsFalse(vals.ContainsKey(35));
            Assert.IsFalse(vals.ContainsKey(40));
            Assert.IsFalse(vals.ContainsKey(100));
        }

        private ConcurrentSortedDictionary<int, string> CreateSampleData()
        {
            ConcurrentSortedDictionary<int, string> data = new ConcurrentSortedDictionary<int, string>();

            data.AddOrUpdate(-100, "value -100", TryMerge);
            data.AddOrUpdate(-50, "value -50", TryMerge);
            data.AddOrUpdate(-25, "value -25", TryMerge);
            data.AddOrUpdate(0, "value 0", TryMerge);
            data.AddOrUpdate(1, "value 1", TryMerge);
            data.AddOrUpdate(10, "value 10", TryMerge);
            data.AddOrUpdate(15, "value 15", TryMerge);
            data.AddOrUpdate(20, "value 20", TryMerge);
            data.AddOrUpdate(25, "value 25", TryMerge);
            data.AddOrUpdate(30, "value 30", TryMerge);
            data.AddOrUpdate(35, "value 35", TryMerge);
            data.AddOrUpdate(40, "value 40", TryMerge);
            data.AddOrUpdate(100, "value 100", TryMerge);

            return data;
        }

        private string TryMerge(int key, string addValue, string exisitingValue)
        {
            return exisitingValue + " " + addValue;
        }
    }
}
