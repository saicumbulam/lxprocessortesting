﻿using System;
using System.Reflection;
using System.Linq;
using System.Collections.Generic;

namespace LxProcessorUnitTest
{
    public class Helper
    {
        private Helper()
        {

        }

        #region Run Method

        /// <summary>
        ///		Runs a method on a type, given its parameters. This is useful for
        ///		calling private methods.
        /// </summary>
        /// <param name="t"></param>
        /// <param name="strMethod"></param>
        /// <param name="aobjParams"></param>
        /// <returns>The return value of the called method.</returns>
        public static object RunStaticMethod(System.Type t, string strMethod, object[] aobjParams)
        {
            BindingFlags eFlags = BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic;
            return RunMethod(t, strMethod, null, aobjParams, eFlags);
        }

        public static object RunInstanceMethod(System.Type t, string strMethod, object objInstance, object[] aobjParams)
        {
            BindingFlags eFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
            return RunMethod(t, strMethod, objInstance, aobjParams, eFlags);
        }

        public static object RunInstanceMethod(System.Type t, string strMethod, object objInstance, object[] aobjParams, Type[] types)
        {
            BindingFlags eFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
            return RunMethod(t, strMethod, objInstance, aobjParams, types, eFlags);
        }

        private static object RunMethod(System.Type t, string strMethod, object objInstance, object[] objParams, BindingFlags eFlags)
        {
            MethodInfo m;
            try
            {
                // add parameter type list
                Type[] paramTypes = new Type[objParams.Length];
                if (objParams != null &&
                    objParams.Length > 0)
                {
                    for (int i = 0; i < objParams.Length; i++)
                    {
                        // if parameter is null, use object as the params, seems to work so far -ACN
                        if (objParams[i] == null)
                        {
                            paramTypes[i] = typeof(Object);
                        }
                        else
                        {
                            paramTypes[i] = objParams[i].GetType();
                        }
                    }
                }

                m = t.GetMethod(strMethod, eFlags, null, paramTypes, null);
                if (m == null)
                {
                    throw new ArgumentException("There is no method '" + strMethod + "' for type '" + t.ToString() + "'.");
                }

                object objRet = m.Invoke(objInstance, objParams);
                return objRet;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        private static object RunMethod(System.Type t, string strMethod, object objInstance, object[] objParams, Type[] types, BindingFlags eFlags)
        {
            MethodInfo m;
            try
            {
                // add parameter type list
                Type[] paramTypes = types.ToArray();

                m = t.GetMethod(strMethod, eFlags, null, paramTypes, null);
                if (m == null)
                {
                    throw new ArgumentException("There is no method '" + strMethod + "' for type '" + t.ToString() + "'.");
                }

                object objRet = m.Invoke(objInstance, objParams);
                return objRet;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        #endregion

    }
}
