﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Types;

namespace LxDatafeed.Data.Tests
{
    [TestClass()]
    public class BoundingBoxTests
    {
        [TestMethod()]
        public void ContainsTest()
        {
            SqlGeography geo = SqlGeography.STGeomFromText(new SqlChars("POLYGON ((115 -7, 115 -42, 161 -42, 161 -7, 115 -7))"), 4326);

            BoundingBox bb = BoundingBox.Convert(geo, false);
            bool contains = bb.Contains(160.97225, -16.097);

            Assert.AreEqual(contains, false);
        }

        [TestMethod()]
        public void ContainsTest2()
        {
            SqlGeography geo = SqlGeography.STGeomFromText(new SqlChars("POLYGON ((115 -7, 115 -42, 161 -42, 161 -7, 115 -7))"), 4326);

            BoundingBox bb = BoundingBox.Convert(geo, false);
            bool contains = bb.Contains(-160.97225, -16.097);

            Assert.AreEqual(contains, true);
        }


        [TestMethod()]
        public void ContainsTest3()
        {
            SqlGeography geo = SqlGeography.STGeomFromText(new SqlChars("POLYGON ((115 -7, 115 -42, 161 -42, 161 -7, 115 -7))"), 4326);

            BoundingBox bb = BoundingBox.Convert(geo, true);
            bool contains = bb.Contains(160.97225, -16.097);

            Assert.AreEqual(contains, true);
        }
    }
}