﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using LxCommon;
using LxCommon.Models;

namespace LxDataManagerRetry
{
    class Program
    {
        static void Main(string[] args)
        {

            CancellationTokenSource cts = new CancellationTokenSource();
            

            if (args.Length > 0  && Directory.Exists(args[0]))
            {
                RetryFilesAsync(args[0], cts.Token);
            }
            else
            {
                Console.WriteLine("Invalid folder specified");
            }

            Console.WriteLine("Press \'q\' to quit the program.");
            while (Console.Read() != 'q') { }
            cts.Cancel();


        }

        private static async Task RetryFilesAsync(string folderName, CancellationToken cancellationToken)
        {
            DirectoryInfo folder = new DirectoryInfo(folderName);
            FileInfo[] files = folder.GetFiles("*.json");
            Console.WriteLine("Found {0} files", files.Length);
            if (files.Length > 0)
            {
                foreach (FileInfo file in files)
                {
                    await ProcessFileAync(file, cancellationToken);
                }
            }
        }

        private static async Task ProcessFileAync(FileInfo file, CancellationToken cancellationToken)
        {
            using (StreamReader reader = file.OpenText())
            {
                string line = await reader.ReadLineAsync();
                if (line.IndexOf("portions", StringComparison.Ordinal) > 0)
                {
                    //flash
                    Flash flash = JsonConvert.DeserializeObject<Flash>(line);
                }
                else if (line.IndexOf("errorEllipse", StringComparison.Ordinal) > 0)
                {
                    //portion
                    Portion portion = JsonConvert.DeserializeObject<Portion>(line);
                }
            }
        }
    }
}
