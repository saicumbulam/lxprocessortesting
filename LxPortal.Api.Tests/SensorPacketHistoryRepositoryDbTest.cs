﻿using LxPortal.Api.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using LxPortalLib.Models;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using Aws.Core.Utilities;
using LxCommon; 

namespace LxPortal.Api.Tests
{
    
    [TestClass()]
    public class SensorPacketHistoryRepositoryDbTest
    {


        private TestContext testContextInstance;

        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        [TestMethod()]
        public void GetTest()
        {

            //test the Get function using null start/end values  
            string id = "ABLWC";
            Nullable<DateTime> start = null;
            Nullable<DateTime> end = null;
            string connString = ConfigurationManager.ConnectionStrings["Lightning"].ConnectionString;
            string query = "exec GetSensorPacket_pr @Sensor_ID = '" + id + "'"; 

            DataTable ds = new DataTable();

            SqlConnection thisConn = new SqlConnection(connString);
            SqlCommand comm = new SqlCommand(query, thisConn);
            SqlDataAdapter readConn = new SqlDataAdapter(comm);
            thisConn.Open();
            readConn.Fill(ds); 
            thisConn.Close();

            int expected = ds.Rows.Count;

            SensorPacketHistoryRepositoryDb target = new SensorPacketHistoryRepositoryDb();
            List<PacketRecord> actual = target.Get(id, start, end);
            Assert.AreEqual(expected, actual.Count);
        }
    }
}
