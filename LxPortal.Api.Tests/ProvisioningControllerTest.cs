﻿using Aws.Core.Utilities;

using System;
using System.Text;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using LxPortal.Api.Controllers;
using LxPortal.Api.Helper;
using LxPortal.Api.Models;
using LxPortal.Api.Models.Database;

using LxPortalLib.Models;
using LxPortalLib.Common;

namespace LxPortal.Api.Tests
{
    /// <summary>
    /// Summary description for ProvisioningControllerTest
    /// </summary>
    [TestClass]
    public class ProvisioningControllerTest
    {
        private static PartnerRecord _partner;
        private static PartnerRecord _updatePartner;
        private static ProvisioningController _provisioningController;

        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            _provisioningController = new ProvisioningController();

            _partner = new PartnerRecord
            {
                //PartnerId = Guid.NewGuid().ToString(),
                PartnerName = "HQ Test",
                IsActive = true,
                ResponseFormatTypeId = 1, //can only be 1 at a time 
                ResponsePackageTypeId = 2, //can only be 1 at a time 
                IsCloudToGroundEnabled = true,
                IsInCloudEnabled = false,
                BoundingArea = "29,-130;-55,-23", //split each point go through sqlgeography check (N,E;S,W)
                IpAddress = { "1.1.1.1", "2.2.2.22222","34.56.67.8" },
                LastModifiedDateUtc = new DateTime(2003, 11, 15),
                LastModifiedBy = "1",
                NetworkTypeId = 1
            };

            _updatePartner = new PartnerRecord
            {
                PartnerId = "47A2AA75-CCC5-4CB6-9121-2D88B3C85353",
                PartnerName = "HQ Test 43",
                IsActive = true,
                ResponseFormatTypeId = 1, //can only be 1 at a time 
                ResponsePackageTypeId = 1, //can only be 1 at a time 
                IsCloudToGroundEnabled = false,
                IsInCloudEnabled = false,
                BoundingArea = "29,-130;-55,-23", //split each point go through sqlgeography check (N,E;S,W)
                IpAddress = { "1.1.1.1", "2.2.2.22222", "34.56.67.8" },
                LastModifiedDateUtc = new DateTime(2003, 11, 15),
                LastModifiedBy = "jchung",
                NetworkTypeId = 1
            };

        }

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void PutPartner()
        {
            try
            {
                var test = _provisioningController.PutPartner(_updatePartner);
            }
            catch (Exception ex)
            {
                Assert.Fail(string.Format("Message: {0}", ex.Message));
            }


        }

        [TestMethod]
        public void PostPartner()
        {
            try
            {
                var test = _provisioningController.PostPartner(_partner);
            }
            catch (Exception ex)
            {
                Assert.Fail(string.Format("Message: {0}", ex.Message));
            }


        }

        [TestMethod]
        public void GetPartner()
        {

            try
            {
                string partnerId = "FFD20395-12FF-45CB-8BDF-5616DEB6451F";
                var test = _provisioningController.GetPartner(partnerId);
            }
            catch (Exception ex)
            {
                Assert.Fail(string.Format("Message: {0}", ex.Message));
            }

        }

        [TestMethod]
        public void GetAllPartners()
        {
            try
            {
                var test = _provisioningController.GetPartner(null);
            }
            catch (Exception ex)
            {
                Assert.Fail(string.Format("Message: {0}", ex.Message));
            }
        }

        [TestMethod]
        public void GetEventLog()
        {
            try
            {
                string id = "0288f2dd-a9a2-43cb-aa15-657594afe277";
                var test = _provisioningController.GetEventLog(id);
            }
            catch (Exception ex)
            {
                Assert.Fail(string.Format("Message: {0}", ex.Message));
            }
        }
    }
}