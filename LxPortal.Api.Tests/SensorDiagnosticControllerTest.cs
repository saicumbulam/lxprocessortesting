﻿using LxPortal.Api.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using LxPortalLib.Models;
using System.Configuration;
using System.Data.SqlClient; 
using System.Data; 

namespace LxPortal.Api.Tests
{
    
    [TestClass()]
    public class SensorDiagnosticControllerTest
    {

        private TestContext testContextInstance;

        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        //check if a diagnostic is returned 
        [TestMethod()]
        public void GetTest()
        {
            //1. get db connection
            //2. compare the number of counts that actual has vs expected 
            //3. can we compare the last modifieddate value and to ensure it's the same?
            string id = "ADWLL";
            Nullable<DateTime> start = null;
            Nullable<DateTime> end = null;
            string connStringArc = ConfigurationManager.ConnectionStrings["LightningDM"].ConnectionString;
            string queryArc = "exec GetSensorLog_pr @Sensor_ID ='" + id + "'";  

            DataTable ds = new DataTable();

            SqlConnection conn = new SqlConnection(connStringArc);

            SqlCommand comm = new SqlCommand(queryArc, conn);
            SqlDataAdapter readConn = new SqlDataAdapter(comm);
            conn.Open();
            readConn.Fill(ds);
            conn.Close();

            int expected = ds.Rows.Count; 

            SensorDiagnosticController target = new SensorDiagnosticController();
            SensorDiagnostics actual = target.Get(id, start, end);
            Assert.AreEqual(expected, actual.DiagnosticList.Count);


            
        }
    }
}
