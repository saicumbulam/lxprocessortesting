﻿using System.Collections.Generic;
using LxPortal.Api.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using LxPortalLib.Models;
using LxPortal.Api.Models.Database;
using LxCommon;
using Aws.Core.Utilities; 
using Aws.Core.Data;
using System.Data.SqlClient;
using System.Configuration; 
using System.Data; 


namespace LxPortal.Api.Tests
{
    
    
    /// <summary>
    ///This is a test class for SensorDetectionRepositoryDbTest and is intended
    ///to contain all SensorDetectionRepositoryDbTest Unit Tests
    ///</summary>
    [TestClass()]
    public class SensorDetectionRepositoryDbTest
    {

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Get
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [DeploymentItem("LxPortal.Api.dll")]
        public void GetTest()
        {
            
            try
            {
                //create instance of database connection
                //expected to see same on both DM and Lx

                //1. pick an id (and you should how many record that station has (expected)
                string id = "ADWLL";
                string start = "2012-09-15 00:36:45.207";
                string end = "2012-10-04 05:43:42.243"; 
                string connectionString = ConfigurationManager.ConnectionStrings["LightningDM"].ConnectionString;
                string query = "SELECT Sensor_ID FROM [TLSDM].[dbo].[SensorStatistics] WHERE LastModifiedDate >='" + start + "' and LastModifiedDate < '" + end + "' and Sensor_ID = '"+ id + "'";   //count, id with a datetime 

                DataTable ds = new DataTable(); 

                SqlConnection thisConn = new SqlConnection(connectionString);
                SqlCommand comm = new SqlCommand(query, thisConn); //create a query 
                SqlDataAdapter readConn = new SqlDataAdapter(comm);//read from the query
                thisConn.Open(); //open up connection to db 
                readConn.Fill(ds); 
                thisConn.Close(); //close connection from db 

                int expected = ds.Rows.Count; 
                
                //2. use that id + start time + end time to test the function (actual)
                SensorDetectionRepositoryDb target = new SensorDetectionRepositoryDb();
                SensorDetection actual = target.Get(id, Convert.ToDateTime(start), Convert.ToDateTime(end));
                int now = actual.DetectionList.Count; 


                //3. compare the expected record count with actual record count, they should be the same object type  
                Assert.AreEqual(expected, now);

            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
                throw; 
            }
            

        }
    }
}
