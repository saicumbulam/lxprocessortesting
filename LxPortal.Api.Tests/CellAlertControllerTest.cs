﻿using System.Collections.Generic;
using System.Net.Http;
using LxPortal.Api.Controllers;
using LxPortalLib.Api;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using LxPortalLib.Models;
using System.Configuration;
using System.Data.SqlClient; 
using System.Data; 

namespace LxPortal.Api.Tests
{
    
    [TestClass]
    public class CellAlertControllerTest
    {

        private TestContext _testContextInstance;

        public TestContext TestContext
        {
            get
            {
                return _testContextInstance;
            }
            set
            {
                _testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        //check if a diagnostic is returned 
        [TestMethod]
        public void GetTest()
        {
            //1. get db connection
            //2. compare the number of counts that actual has vs expected 
            //3. can we compare the last modifieddate value and to ensure it's the same?
            string connStringArc = ConfigurationManager.ConnectionStrings["Lightning"].ConnectionString;
            string queryArc = "SELECT TOP 2 [CellTimeUtc] FROM [Lightning].[dbo].[CellAlerts] WHERE [AlertLevel] = 3 order by celltimeutc desc";  

            DataTable ds = new DataTable();
            SqlConnection conn = new SqlConnection(connStringArc);
            SqlCommand comm = new SqlCommand(queryArc, conn);
            SqlDataAdapter readConn = new SqlDataAdapter(comm);
            conn.Open();
            readConn.Fill(ds);
            conn.Close();
            DateTime start, end;

            int expected = ds.Rows.Count;
            switch (expected)
            {
                case 0:
                    Assert.Inconclusive("There is no cell alert in database");
                    return;
                case 1:
                    start = DateTime.Parse(ds.Rows[0]["CellTimeUtc"].ToString());
                    end = DateTime.Parse(ds.Rows[0]["CellTimeUtc"].ToString()).AddMinutes(1);
                    break;
                default:
                    start = DateTime.Parse(ds.Rows[1]["CellTimeUtc"].ToString());
                    end = DateTime.Parse(ds.Rows[0]["CellTimeUtc"].ToString()).AddMinutes(1);
                    break;
            }

            conn = new SqlConnection(connStringArc);
            comm = new SqlCommand("SELECT COUNT(*) FROM [Lightning].[dbo].[CellAlerts] WHERE AlertLevel = '3' AND CellTimeUtc between '" + start.ToString("yyyy-MM-dd HH:mm:ss") + "' AND '" + end.ToString("yyyy-MM-dd HH:mm:ss") + "'", conn);
            readConn = new SqlDataAdapter(comm);
            ds = new DataTable();
            conn.Open();
            readConn.Fill(ds);
            conn.Close();
            expected = int.Parse(ds.Rows[0][0].ToString());

            string alertType = "3";
            var responseCont = new ResponseContainer<List<CellAlertRecord>>(); 
            responseCont = CellAlertClient.GetBoxCellAlert(start, end, alertType, 51, -153, -41, -16);
            Assert.AreEqual(expected, responseCont.r.Count);
        }
    }
}
