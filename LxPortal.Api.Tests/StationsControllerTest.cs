﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using LxPortalLib.Models;

using LxPortal.Api.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;

namespace LxPortal.Api.Tests
{


    /// <summary>
    ///This is a test class for StationsControllerTest and is intended
    ///to contain all StationsControllerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class StationsControllerTest
    {
        private static IEnumerable<Station> _stations;
        private static StationsController _stationController;
        private static Station _newStation;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            _stationController = new StationsController();
        }

        [TestMethod]
        public void GetAllStations()
        {
            try
            {
                _stations = _stationController.GetAllStations();
                Assert.IsTrue(_stations.Any(), "GetAllStations: fail to get any stations");
            }
            catch (Exception ie)
            {
                Assert.Fail("GetAllStations: fail because " + ie.Message);
            }
        }

        [TestMethod]
        public void GetStation()
        {
            try
            {
                if (_stations == null)
                {
                    GetAllStations();
                }
                if (_stations != null && _stations.Any())
                {
                    Station referenceStation = _stations.First();
                    Station station = _stationController.GetStation(referenceStation.Id);
                    Assert.AreEqual(referenceStation.Id, station.Id, "GetStation: Station Id is not the same as the reference");
                }
                else
                {
                    Assert.Fail("GetStation: The test cannot locate a station for testing");
                }
            }
            catch (Exception ie)
            {
                Assert.Fail("GetStation: fail because " + ie.Message);
            }
        }

        [TestMethod]
        public void PostStation()
        {
            _newStation = new Station();
            _newStation.Name = "test station name";
            _newStation.StreetAddress = "test station name";
            _newStation.Country = "USA";
            _newStation.Province = "Maryland";
            _newStation.Region = "North America";
            _newStation.City = "Germantown";
            _newStation.PostalCode = "20904";
            _newStation.SerialNumber = Guid.NewGuid().ToString().Substring(0, 14);
            _newStation.PocName = "Weatherbug";
            _newStation.PocPhone = "0987654321";
            _newStation.PocEmail = "weatherbug@weatherbug.com";
            _newStation.PrimaryDeliveryIp = "127.0.0.2";
            _newStation.SecondaryDeliveryIp = "127.0.0.3";
            _newStation.SendLogRateMinutes = 10;
            _newStation.SendGpsRateMinutes = 10;
            _newStation.SendAntennaGainRateMinutes = 10;
            _newStation.AntennaMode = 9;
            _newStation.AntennaAttenuation = 40;
            _newStation.Note = "";

            SqlConnection conn = null;
            SqlCommand command;
            SqlDataReader reader = null;
            try
            {
                HttpResponseMessage responseMsg = _stationController.PostStation(_newStation);
                if (!string.IsNullOrEmpty(responseMsg.ReasonPhrase))
                {
                    _newStation.Id = responseMsg.ReasonPhrase;
                }
                else
                {
                    Assert.Fail("PostStation:there is no id prsented for the new station");
                }
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Lightning"].ConnectionString);
                conn.Open();
                command = conn.CreateCommand();
                command.CommandText = string.Format("SELECT count(*) as NumRec from dbo.ltgcalibration where station_id = '{0}'",
                                        _newStation.Id);
                reader = command.ExecuteReader();
                if (reader.Read())
                {
                    if (int.Parse(reader["NumRec"].ToString()) <= 0)
                        Assert.Fail("PostStation: fail because the default calibration value is not set.");
                }
                else
                    Assert.Fail("PostStation: fail because the default calibration value is not set.");
            }
            catch (Exception ie)
            {
                Assert.Fail("PostStation: fail because " + ie.Message);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                if (conn != null)
                    conn.Close();
            }
        }

        [TestMethod]
        public void PostStationWithSameSerial()
        {
            _newStation = new Station();
            _newStation.Name = "test station name";
            _newStation.StreetAddress = "test station name";
            _newStation.Country = "USA";
            _newStation.Province = "Maryland";
            _newStation.Region = "North America";
            _newStation.City = "Germantown";
            _newStation.PostalCode = "20904";
            _newStation.SerialNumber = "123456";
            _newStation.PocName = "Weatherbug";
            _newStation.PocPhone = "0987654321";
            _newStation.PocEmail = "weatherbug@weatherbug.com";
            _newStation.PrimaryDeliveryIp = "127.0.0.2";
            _newStation.SecondaryDeliveryIp = "127.0.0.3";
            _newStation.SendLogRateMinutes = 10;
            _newStation.SendGpsRateMinutes = 10;
            _newStation.SendAntennaGainRateMinutes = 10;
            _newStation.AntennaMode = 9;
            _newStation.AntennaAttenuation = 40;
            _newStation.Note = "";

            try
            {
                HttpResponseMessage responseMsg = _stationController.PostStation(_newStation);
                Assert.IsFalse(responseMsg.StatusCode == HttpStatusCode.Created, "PostStationWithSameSerial: Post fail with the same serial number");

            }
            catch (Exception ie)
            {
                Assert.Fail("PostStationWithSameSerial: fail because " + ie.Message);
            }
        }

        [TestMethod]
        public void PutStation()
        {
            try
            {
                if (_newStation != null && string.IsNullOrEmpty(_newStation.Id))
                {
                    PostStation();
                }
                else
                {
                    if (_newStation != null)
                    {
                        _newStation.Name = "test station name MODIFIED";
                        _stationController.PutStation(_newStation.Id, _newStation);
                        Station modifiedStation = _stationController.GetStation(_newStation.Id);
                        Assert.AreEqual(_newStation.Name, modifiedStation.Name);
                    }
                    else
                    {
                        Assert.Fail("PutStation: fail because no station for update");
                    }
                }
            }
            catch (Exception ie)
            {
                Assert.Fail("PutStation: fail because " + ie.Message);
            }
        }

        [TestMethod]
        public void DeleteStation()
        {
            try
            {
                if (_newStation != null && string.IsNullOrEmpty(_newStation.Id))
                {
                    PostStation();
                }
                else
                {
                    if (_newStation != null)
                    {
                        _stationController.DeleteStation(_newStation.Id);
                        _stationController.GetStation(_newStation.Id);
                    }
                    Assert.Fail("DeleteStation: fail because this line should never be reached");
                }
            }
            catch (Exception ie)
            {
                if (!(ie is HttpResponseException))
                {
                    Assert.Fail("DeleteStation: fail because " + ie.Message);
                }
            }
        }

        
        [TestMethod()]
        [DeploymentItem("LxPortal.Api.dll")]
        public void VerifyStationDataTest()
        {
            StationsController_Accessor target = new StationsController_Accessor(); // TODO: Initialize to an appropriate value
            Station station = new Station();
            station.Name = "a";
            station.SerialNumber = "a";
            station.SendGpsRateMinutes = 10;
            station.SendAntennaGainRateMinutes = 10;
            station.SendLogRateMinutes = 10;
            station.AntennaMode = 9;
            station.AntennaAttenuation = 40;
            StringBuilder strBuilder = new StringBuilder(260);
            for (int i = 0; i < 1001; i++)
                strBuilder.Append("a");
            string testString = strBuilder.ToString();
            station.PrimaryDeliveryIp = testString;
            Assert.IsFalse(target.VerifyStationData(station), "VerifyStationDataTest: fail to invalidate primary delivery ip");
            station.PrimaryDeliveryIp = "a";
            station.SecondaryDeliveryIp = testString;
            Assert.IsFalse(target.VerifyStationData(station), "VerifyStationDataTest: fail to invalidate secondary delivery ip");
            station.SecondaryDeliveryIp = "a";
            station.Name = testString;
            Assert.IsFalse(target.VerifyStationData(station), "VerifyStationDataTest: fail to invalidate Name");
            station.Name = "a";
            station.SerialNumber = testString;
            Assert.IsFalse(target.VerifyStationData(station), "VerifyStationDataTest: fail to invalidate SerialNumber");
            station.SerialNumber = "a";
            station.StreetAddress = testString;
            Assert.IsFalse(target.VerifyStationData(station), "VerifyStationDataTest: fail to invalidate StreetAddress");
            station.StreetAddress = "a";
            station.Country = testString;
            Assert.IsFalse(target.VerifyStationData(station), "VerifyStationDataTest: fail to invalidate Country");
            station.Country = "a";
            station.Province = testString;
            Assert.IsFalse(target.VerifyStationData(station), "VerifyStationDataTest: fail to invalidate Province");
            station.Province = "a";
            station.Region = testString;
            Assert.IsFalse(target.VerifyStationData(station), "VerifyStationDataTest: fail to invalidate Region");
            station.Region = "a";
            station.City = testString;
            Assert.IsFalse(target.VerifyStationData(station), "VerifyStationDataTest: fail to invalidate City");
            station.City = "a";
            station.PostalCode = testString;
            Assert.IsFalse(target.VerifyStationData(station), "VerifyStationDataTest: fail to invalidate PostalCode");
            station.PostalCode = "a";
            station.PocName = testString;
            Assert.IsFalse(target.VerifyStationData(station), "VerifyStationDataTest: fail to invalidate PocName");
            station.PocName = "a";
            station.PocPhone = testString;
            Assert.IsFalse(target.VerifyStationData(station), "VerifyStationDataTest: fail to invalidate PocPhone");
            station.PocPhone = "a";
            station.PocEmail = testString;
            Assert.IsFalse(target.VerifyStationData(station), "VerifyStationDataTest: fail to invalidate PocEmail");
            station.PocEmail = "a";
            station.Note = testString;
            Assert.IsFalse(target.VerifyStationData(station), "VerifyStationDataTest: fail to invalidate Note");
            station.Note = "a";
            station.SendGpsRateMinutes = null;
            Assert.IsFalse(target.VerifyStationData(station), "VerifyStationDataTest: fail to invalidate SendGpsRateMinutes");
            station.SendGpsRateMinutes = 10;

            station.SendLogRateMinutes = null;
            Assert.IsFalse(target.VerifyStationData(station), "VerifyStationDataTest: fail to invalidate SendLogRateMinutes");
            station.SendLogRateMinutes = 10;
            station.SendAntennaGainRateMinutes = null;
            Assert.IsFalse(target.VerifyStationData(station), "VerifyStationDataTest: fail to invalidate SendAntennaGainRateMinutes");
            station.SendAntennaGainRateMinutes = 10;
            station.AntennaMode = null;
            Assert.IsFalse(target.VerifyStationData(station), "VerifyStationDataTest: fail to invalidate AntennaMode");
            station.AntennaMode = 0;
            station.AntennaMode = 11;
            Assert.IsFalse(target.VerifyStationData(station), "VerifyStationDataTest: fail to invalidate AntennaMode");
            station.AntennaMode = 8;
            Assert.IsFalse(target.VerifyStationData(station), "VerifyStationDataTest: fail to invalidate AntennaMode");
            station.AntennaMode = 10;
            station.SendLogRateMinutes = 1;
            Assert.IsFalse(target.VerifyStationData(station), "VerifyStationDataTest: fail to invalidate SendLogRateMinutes");
            station.SendLogRateMinutes = 1000;
            Assert.IsFalse(target.VerifyStationData(station), "VerifyStationDataTest: fail to invalidate SendLogRateMinutes");
            station.SendLogRateMinutes = 10;
            station.SendGpsRateMinutes = 1;
            Assert.IsFalse(target.VerifyStationData(station), "VerifyStationDataTest: fail to invalidate SendGpsRateMinutes");
            station.SendGpsRateMinutes = 1000;
            Assert.IsFalse(target.VerifyStationData(station), "VerifyStationDataTest: fail to invalidate SendGpsRateMinutes");
            station.SendGpsRateMinutes = 10;
            station.SendAntennaGainRateMinutes = 1;
            Assert.IsFalse(target.VerifyStationData(station), "VerifyStationDataTest: fail to invalidate SendAntennaGainRateMinutes");
            station.SendAntennaGainRateMinutes = 1000;
            Assert.IsFalse(target.VerifyStationData(station), "VerifyStationDataTest: fail to invalidate SendAntennaGainRateMinutes");
        }

        
    }
}
