﻿using LxPortal.Api.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using LxPortalLib.Models;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient; 

namespace LxPortal.Api.Tests
{
    
    
    /// <summary>
    ///This is a test class for StormRepositoryDbTest and is intended
    ///to contain all StormRepositoryDbTest Unit Tests
    ///</summary>
    [TestClass()]
    public class StormDataControllerTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        [TestMethod()]
        //[HostType("ASP.NET")]
        //[AspNetDevelopmentServerHost("C:\\Earth\\Lightning\\TLN\\Release 2\\Services\\LxPortal.Api\\Workspace\\LxPortal.Api", "/")]
        //[UrlToTest("http://localhost:12539/")]
        public void GetAllTest()
        {
            //1. get db connection
            //2. compare the number of counts that actual has vs expected 
            //3. can we compare the last modifieddate value and to ensure it's the same?

            StormRepositoryDb target = new StormRepositoryDb(); 
            DateTime start;
            DateTime.TryParse("2011-08-05 13:50:00.000", out start); 
            DateTime end;
            DateTime.TryParse("2011-08-06 23:00:00.000", out end);
            Decimal ullat = 38;
            Decimal ullon = -103;
            Decimal lrlat = 46;
            Decimal lrlon = -89;

            string connString = ConfigurationManager.ConnectionStrings["StormPager"].ConnectionString;
            //replicate the db call in stored proc 
            //change the query to pull fewer or more to test comparison 
            string query = "SELECT  lsr.[MsgID],lsr.[Event] FROM [StormPager].[dbo].[NBCSAS_LSR] as lsr LEFT OUTER JOIN stormpager.dbo.nbcsas_lu_lsr as lu on lsr.event = lu.event WHERE lu.LsrId in ( 15, 37, 39, 40, 41) AND LSR.ReportedTime >= '" + start + "' AND LSR.ReportedTime <= '" + end + "' AND Latitude >= "+ ullat +" AND Latitude <= " + lrlat + " AND Longitude >= "+ ullon +" AND Longitude <= "+ lrlon +" "; 

            DataTable ds = new DataTable();
            SqlConnection conn = new SqlConnection(connString);
            SqlCommand comm = new SqlCommand(query, conn);
            SqlDataAdapter readConn = new SqlDataAdapter(comm);
            conn.Open();
            readConn.Fill(ds); 
            conn.Close();

            int expected = ds.Rows.Count;
            List<StormRecord> actual = target.GetAll(start, end, ullat, ullon, lrlat, lrlon);
            Assert.AreEqual(expected, actual.Count);
            
            //compare the local time vs. utc time?
        }
    }
}
