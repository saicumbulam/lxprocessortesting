﻿using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;

using LxPortal.Api.Models;
using LxPortalLib.Common;

namespace LxPortal.Api.Tests
{
    [TestClass()]
    public class QuerySourceTest
    {
        [TestMethod()]
        [DeploymentItem("LxPortal.Api.dll")]
        public void GetQuerySourceTest1()
        {
            DateTime start;
            DateTime.TryParse("2012-09-24 03:00", out start);
            DateTime end ;
            DateTime.TryParse("2012-09-26 03:00", out end);
            DateTime utcNow;
            DateTime.TryParse("2012-09-27 03:16", out utcNow);

            DateTime liveStart;
            DateTime.TryParse("2012-09-25 03:30", out liveStart);
            int dartMartAgeHours = 48;

            DateTime dataMartEnd = utcNow.AddHours(-dartMartAgeHours);

            QuerySource expected = new QuerySource
            {

                DataMartStartKey = Utilities.GetDateKey(start),
                DataMartEndKey = Utilities.GetDateKey(dataMartEnd),
                UseDataMart = true,
            };

            QuerySource actual = QuerySource.GetQuerySource(start, end, utcNow, dartMartAgeHours, false, 0, 0);
            Assert.AreEqual(expected, actual);
            
        }

        [TestMethod()]
        [DeploymentItem("LxPortal.Api.dll")]
        public void GetQuerySourceTest2()
        {
            DateTime start;
            DateTime.TryParse("2011-12-29 03:00", out start);
            DateTime end;
            DateTime.TryParse("2012-01-01 03:00", out end);
            DateTime utcNow;
            DateTime.TryParse("2012-01-02 23:47", out utcNow);

            DateTime liveStart;
            DateTime.TryParse("2012-01-01 00:00", out liveStart);
            int dartMartAgeHours = 48;

            DateTime dataMartEnd = utcNow.AddHours(-dartMartAgeHours);

            QuerySource expected = new QuerySource
            {

                DataMartStartKey = Utilities.GetDateKey(start),
                DataMartEndKey = Utilities.GetDateKey(dataMartEnd),
                UseDataMart = true
            };

            QuerySource actual = QuerySource.GetQuerySource(start, end, utcNow, dartMartAgeHours, false, 0, 0);
            Assert.AreEqual(expected, actual);

        }

        [TestMethod()]
        [DeploymentItem("LxPortal.Api.dll")]
        public void GetQuerySourceTest3()
        {
            DateTime start;
            DateTime.TryParse("2011-12-29 03:00", out start);
            DateTime end;
            DateTime.TryParse("2012-01-01 03:00", out end);
            DateTime utcNow;
            DateTime.TryParse("2012-01-02 23:47", out utcNow);

            DateTime liveStart;
            DateTime.TryParse("2012-01-01 00:00", out liveStart);
            int dartMartAgeHours = 48;
            long dataMartStartKey = 20111231001; //2011 12 31 00 1
            long dataMartArchiveEndKey = 20111230234; //2011 12 31 00 1

            DateTime dataMartEnd = utcNow.AddHours(-dartMartAgeHours);

            QuerySource expected = new QuerySource
            {

                DataMartStartKey = dataMartStartKey,
                DataMartEndKey = Utilities.GetDateKey(dataMartEnd),
                UseDataMart = true,
                UseDataMartArchive = true,
                DataMartArchiveStartKey = Utilities.GetDateKey(start),
                DataMartArchiveEndKey = dataMartArchiveEndKey
            };

            QuerySource actual = QuerySource.GetQuerySource(start, end, utcNow, dartMartAgeHours, true, dataMartArchiveEndKey, dataMartStartKey);
            Assert.AreEqual(expected, actual);

        }

        [TestMethod()]
        [DeploymentItem("LxPortal.Api.dll")]
        public void GetQuerySourceTest4()
        {
            DateTime start;
            DateTime.TryParse("2011-12-29 03:00", out start);
            DateTime end;
            DateTime.TryParse("2012-01-01 03:00", out end);
            DateTime utcNow;
            DateTime.TryParse("2012-01-02 23:47", out utcNow);

            DateTime liveStart;
            DateTime.TryParse("2012-01-01 00:00", out liveStart);
            int dartMartAgeHours = 0;

            long dataMartStartKey = 20111231001; //2011 12 31 00 1
            long dataMartArchiveEndKey = 20111230234; //2011 12 31 00 1

            QuerySource expected = new QuerySource
            {
                DataMartStartKey = dataMartStartKey,
                DataMartEndKey = Utilities.GetDateKey(end.AddMinutes(-1)),
                UseDataMart = true,
                UseDataMartArchive = true,
                DataMartArchiveStartKey = Utilities.GetDateKey(start),
                DataMartArchiveEndKey = dataMartArchiveEndKey
            };

            QuerySource actual = QuerySource.GetQuerySource(start, end, utcNow, dartMartAgeHours, true, dataMartArchiveEndKey, dataMartStartKey);
            Assert.AreEqual(expected, actual);

        }

        [TestMethod()]
        [DeploymentItem("LxPortal.Api.dll")]
        public void GetQuerySourceTest5()
        {
            DateTime start;
            DateTime.TryParse("2011-12-28 03:00", out start);
            DateTime end;
            DateTime.TryParse("2011-12-29 03:00", out end);
            DateTime utcNow;
            DateTime.TryParse("2012-01-02 23:47", out utcNow);

            int dartMartAgeHours = 0;
            
            long dataMartStartKey = 20111231001; //2011 12 31 00 1
            long dataMartArchiveEndKey = 20111230234; //2011 12 30 23 4

            QuerySource expected = new QuerySource
            {
                UseDataMart = false,
                UseDataMartArchive = true,
                DataMartArchiveStartKey = Utilities.GetDateKey(start),
                DataMartArchiveEndKey = Utilities.GetDateKey(end.AddMinutes(-1))
            };

            QuerySource actual = QuerySource.GetQuerySource(start, end, utcNow, dartMartAgeHours, true, dataMartArchiveEndKey, dataMartStartKey);
            Assert.AreEqual(expected.UseDataMart, actual.UseDataMart);
            Assert.AreEqual(expected.UseDataMartArchive, actual.UseDataMartArchive);
            Assert.AreEqual(expected.DataMartArchiveEndKey, actual.DataMartArchiveEndKey);
            Assert.AreEqual(expected.DataMartArchiveStartKey, actual.DataMartArchiveStartKey);
        }

        [TestMethod()]
        [DeploymentItem("LxPortal.Api.dll")]
        public void GetQuerySourceTest6()
        {
            DateTime start;
            DateTime.TryParse("2011-12-31 03:00", out start);
            DateTime end;
            DateTime.TryParse("2012-01-01 22:59", out end);
            DateTime utcNow;
            DateTime.TryParse("2012-01-02 23:47", out utcNow);

            int dartMartAgeHours = 0;

            long dataMartStartKey = 20111231001; //2011 12 31 00 1
            long dataMartArchiveEndKey = 20111230234; //2011 12 30 23 4

            QuerySource expected = new QuerySource
            {
                UseDataMart = true,
                UseDataMartArchive = false,
                DataMartStartKey = Utilities.GetDateKey(start),
                DataMartEndKey = Utilities.GetDateKey(end.AddMinutes(-1))
            };

            QuerySource actual = QuerySource.GetQuerySource(start, end, utcNow, dartMartAgeHours, true, dataMartArchiveEndKey, dataMartStartKey);
            Assert.AreEqual(expected.UseDataMart, actual.UseDataMart);
            Assert.AreEqual(expected.UseDataMartArchive, actual.UseDataMartArchive);
            Assert.AreEqual(expected.DataMartEndKey, actual.DataMartEndKey);
            Assert.AreEqual(expected.DataMartStartKey, actual.DataMartStartKey);
        }

    }
}
